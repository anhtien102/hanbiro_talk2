import {
  take,
  put,
  call,
  fork,
  select,
  takeEvery,
  all
} from 'redux-saga/effects';
import { ipcRenderer } from 'electron';
import * as actionTypes from '../actions/actionTypes';

function* processBeforeNavigateHome(action) {}

// use them in parallel
export default function* authSagas() {
  yield takeEvery(actionTypes.NAVIGATE_HOME_PAGE, processBeforeNavigateHome);
}
