import {
  take,
  put,
  call,
  fork,
  select,
  takeEvery,
  all
} from 'redux-saga/effects';
import * as actions from '../actions';
import authSagas from './auth';

export default function* root() {
  yield all([fork(authSagas)]);
}
