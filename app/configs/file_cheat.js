export const ensureFilePath = path => {
  if (path) {
    return path.replace(/#/gi, '%23');
  }
  return path;
};
