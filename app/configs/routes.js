import React from 'react';

import UserView from '../screens/home/common/UserView';

import CommonHeader from '../screens/home/common/header';

import CompanyOrgList from '../screens/home/messenger/CompanyOrgList';
import FavoriteOrgList from '../screens/home/messenger/FavoriteOrgList';
import RoomListContainer from '../containers/RoomListContainer';
import ChatContainer from '../containers/ChatContainer';

import WhisperInfor from '../screens/home/whisper/WhisperInfor';
import WhisperList from '../screens/home/whisper/WhisperList';
import WhisperDetail from '../screens/home/whisper/WhisperDetail';
import ChatRightPannel from '../screens/home/messenger/ChatRightPannel';
import BoardRightPanel from '../screens/home/messenger/BoardRightPanel';

export const routes = [
  {
    path: '/',
    exact: true,
    topLeft: UserView,
    topRight: CommonHeader,
    bottomLeft: CompanyOrgList,
    bottomRight: ChatContainer,
    rightPannel: ChatRightPannel,
    boardRightPannel: BoardRightPanel,
    commonKey: 'Chat_Container'
  },
  {
    path: '/favorite',
    topLeft: UserView,
    topRight: CommonHeader,
    bottomLeft: FavoriteOrgList,
    bottomRight: ChatContainer,
    rightPannel: ChatRightPannel,
    boardRightPannel: BoardRightPanel,
    commonKey: 'Chat_Container'
  },
  {
    path: '/chat',
    topLeft: UserView,
    topRight: CommonHeader,
    bottomLeft: RoomListContainer,
    bottomRight: ChatContainer,
    rightPannel: ChatRightPannel,
    boardRightPannel: BoardRightPanel,
    commonKey: 'Chat_Container'
  },
  {
    path: '/whisper',
    topLeft: UserView,
    topRight: WhisperInfor,
    bottomLeft: WhisperList,
    bottomRight: WhisperDetail,
    rightPannel: () => null,
    boardRightPannel: () => null,
    commonKey: 'Whisper_Container'
  },
  {
    path: '/history',
    topLeft: UserView,
    topRight: () => <div>history topRight</div>,
    bottomLeft: () => <div>history bottomLeft!</div>,
    bottomRight: () => <div>history bottomRight</div>,
    rightPannel: () => null,
    boardRightPannel: () => null,
    commonKey: 'History_Container'
  }
];
