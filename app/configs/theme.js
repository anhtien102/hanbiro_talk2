import { createMuiTheme } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';

const MODE = 'light';
const DEFAULT_FONT = 'Helvetica';
const DEFAULT_FONT_SIZE = 14;
const DEFAULT_PRIMARY_THEME_ID = 2;

export const ThemeSupport = [
  {
    themeId: 1,
    primary: {
      main: '#0671b7',
      light: '#dae9ff',
      dark: '#004687',
      contrastText: '#fff'
    },
    hanbiroColor: {
      backgroundOther: '#efefef',
      backgroundMe: '#0671b7',
      textMsgColorMe: '#ffffff',
      textMsgColorFileNameMe: '#ffffff',
      textDescMe: 'rgba(255, 255, 255, 0.6)',
      textMsgLinkMe: '#ffffff',
      textMsgLinkOther: 'rgba(0, 0, 0, 0.87)',
      dialogTitle: '#0671b7',
      quoteTitleColorMe: '#fdb64e',
      quoteTitleColor: '#0671b7',

      quoteTitleColorMyName: '#add8e6'
    }
  },
  {
    themeId: 2,
    primary: {
      main: '#438eb9',
      light: '#78beeb',
      dark: '#006189',
      contrastText: '#fff'
    },
    hanbiroColor: {
      backgroundOther: '#efefef',
      backgroundMe: 'rgba(67,142,185,0.25)',
      textMsgColorMe: 'rgba(0, 0, 0, 0.87)',
      textMsgColorFileNameMe: 'rgba(0, 0, 0, 0.87)',
      textDescMe: 'rgba(255, 255, 255, 0.6)',
      textMsgLinkMe: 'rgba(0, 0, 0, 0.87)',
      textMsgLinkOther: 'rgba(0, 0, 0, 0.87)',
      dialogTitle: '#438eb9',
      quoteTitleColorMe: '#0671b7',
      quoteTitleColor: '#0671b7',

      quoteTitleColorMyName: '#30819c'
    }
  },
  {
    themeId: 3,
    primary: {
      main: '#b39ddb',
      light: '#e6ceff',
      dark: '#836fa9',
      contrastText: '#fff'
    },
    hanbiroColor: {
      backgroundOther: '#efefef',
      backgroundMe: 'rgba(179,157,219,0.3)',
      textMsgColorMe: 'rgba(0, 0, 0, 0.87)',
      textMsgColorFileNameMe: 'rgba(0, 0, 0, 0.87)',
      textDescMe: 'rgba(255, 255, 255, 0.6)',
      textMsgLinkMe: 'rgba(0, 0, 0, 0.87)',
      textMsgLinkOther: 'rgba(0, 0, 0, 0.87)',
      dialogTitle: '#b39ddb',
      quoteTitleColorMe: '#0671b7',
      quoteTitleColor: '#0671b7',

      quoteTitleColorMyName: '#30819c'
    }
  },
  {
    themeId: 4,
    primary: {
      main: '#9a6284',
      light: '#cc90b4',
      dark: '#6b3657',
      contrastText: '#fff'
    },
    hanbiroColor: {
      backgroundOther: '#efefef',
      backgroundMe: '#9a6284',
      textMsgColorMe: '#ffffff',
      textMsgColorFileNameMe: '#ffffff',
      textDescMe: 'rgba(255, 255, 255, 0.6)',
      textMsgLinkMe: '#ffffff',
      textMsgLinkOther: 'rgba(0, 0, 0, 0.87)',
      dialogTitle: '#9a6284',
      quoteTitleColorMe: '#fdb64e',
      quoteTitleColor: '#0671b7',

      quoteTitleColorMyName: '#add8e6'
    }
  },
  {
    themeId: 5,
    primary: {
      main: '#80cbc4',
      light: '#b2fef7',
      dark: '#4f9a94',
      contrastText: '#fff'
    },
    hanbiroColor: {
      backgroundOther: '#efefef',
      backgroundMe: 'rgba(128,203,196,0.4)',
      textMsgColorMe: 'rgba(0, 0, 0, 0.87)',
      textMsgColorFileNameMe: 'rgba(0, 0, 0, 0.87)',
      textDescMe: 'rgba(255, 255, 255, 0.6)',
      textMsgLinkMe: 'rgba(0, 0, 0, 0.87)',
      textMsgLinkOther: 'rgba(0, 0, 0, 0.87)',
      dialogTitle: '#80cbc4',
      quoteTitleColorMe: '#0671b7',
      quoteTitleColor: '#0671b7',

      quoteTitleColorMyName: '#30819c'
    }
  },
  {
    themeId: 6,
    primary: {
      main: '#e57373',
      light: '#ffa4a2',
      dark: '#af4448',
      contrastText: '#fff'
    },
    hanbiroColor: {
      backgroundOther: '#efefef',
      backgroundMe: '#e57373',
      textMsgColorMe: '#ffffff',
      textMsgColorFileNameMe: '#ffffff',
      textDescMe: 'rgba(255, 255, 255, 0.6)',
      textMsgLinkMe: '#ffffff',
      textMsgLinkOther: 'rgba(0, 0, 0, 0.87)',
      dialogTitle: '#e57373',
      quoteTitleColorMe: '#6d2a8b',
      quoteTitleColor: '#0671b7',

      quoteTitleColorMyName: '#18414e'
    }
  }
];

const darkTheme = {
  primary: {
    main: '#505050',
    light: '#888888',
    dark: '#303030',
    contrastText: '#fff'
  }
};

export const FontSupport = [
  'Open Sans',
  'Roboto',
  'Sans-Serif',
  'Verdana',
  'Arial',
  'Courier New',
  'Georgia',
  'Charcoal',
  'Monaco',
  'Geneva',
  'Helvetica'
];

export const FontSizeSupport = [12, 13, 14, 15, 16];

export const createTheme = (
  font = DEFAULT_FONT,
  fontSize = DEFAULT_FONT_SIZE,
  type = MODE
) => {
  const storageDarkTheme = useSelector(state => state.setting.darkmode);
  const storageFont = useSelector(state => state.setting.font);
  const storageFontSize = useSelector(state => state.setting.fontSize);
  const storagePrimaryColor = useSelector(state => state.setting.primaryTheme);
  const themeId = storagePrimaryColor?.themeId ?? DEFAULT_PRIMARY_THEME_ID;
  const primaryTheme = ThemeSupport.find(item => item.themeId == themeId);

  const themeType = storageDarkTheme ?? type;
  const isLight = themeType == 'light';

  const datePicker = isLight
    ? {}
    : {
        MuiPickersDay: {
          day: {
            '&:focus&.MuiPickersDay-daySelected ': {
              backgroundColor: '#505050'
            }
          },
          daySelected: {
            backgroundColor: '#505050',
            '&:hover': {
              backgroundColor: '#303030'
            }
          }
        },
        MuiPickersDateRangeDay: {
          rangeIntervalDayHighlight: {
            backgroundColor: 'rgba(220, 192, 192, 0.87)'
          }
        }
      };

  return createMuiTheme({
    typography: {
      fontFamily: storageFont ?? font,
      fontSize: storageFontSize ?? fontSize
    },
    palette: {
      type: themeType,
      primary: isLight ? primaryTheme.primary : primaryTheme.primary,
      background: {
        paper: isLight ? '#fff' : '#424242',
        default: isLight ? '#fff' : '#303030',
        tabBarContainer: isLight ? '#efefef' : '#424242'
      },
      divider: isLight ? '#e5e5e5' : 'rgba(255, 255, 255, 0.12)',
      hanbiroColor: primaryTheme.hanbiroColor,
      dividerTabBar: isLight ? '#707070' : '#303030'
    },
    overrides: datePicker
  });
};
