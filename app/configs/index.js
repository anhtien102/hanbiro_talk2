import { routes } from './routes';
import { LanguageSetting } from './language';
import {
  createTheme,
  FontSupport,
  FontSizeSupport,
  ThemeSupport
} from './theme';
export {
  routes,
  createTheme,
  FontSupport,
  FontSizeSupport,
  ThemeSupport,
  LanguageSetting
};
