import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import crypto from 'crypto';
import path from 'path';
import * as Actions from './actions';
import React, { Component } from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import { withTranslation } from 'react-i18next';
import talkApi from './core/service/talk.api.render';
import MainOneOnePage from './janus_client/src/app/main_one_one';
import withStyles from './app_call_styles';
import { ipcRenderer, remote } from 'electron';
import * as constantsApp from './configs/constant';
import {
  SIP_HANGUP_REASON,
  SIP_RESULT_STATUS,
  buildMyNumber
} from './janus_client/src/app/singlecall/sipstatus';

import simpleRequest from './janus_client/src/app/api/server-config-request';

import { openLinkWithURL, remoteAskMediaAccess } from './utils/electron.utils';

const USE_SIP_CHECK_LOOP = true;

let sipDataReceived = {
  sipNumber: null,
  sipKey: null
};

window.onerror = function(error, url, line) {
  console.log(error, url, line);
  remote.getCurrentWindow().close();
};

const requireCheckDeviceCapture = process.platform == 'darwin' ? true : false;
class AppCallPage extends Component {
  constructor(props) {
    super(props);
    this.delayQuit = this.delayQuit.bind(this);
    this.delayQuit3000 = this.delayQuit3000.bind(this);
    this.showNotification = this.showNotification.bind(this);
    this.sendSIPWithAction = this.sendSIPWithAction.bind(this);
    this.startSipCheck = this.startSipCheck.bind(this);
    this.stopSipCheck = this.stopSipCheck.bind(this);
    this.playSoundCall = this.playSoundCall.bind(this);
    this.state = {
      openSnackbar: false,
      errorSnackbar: null,
      audio: null,
      userData: null,
      direction: null,
      myUserKey: null,
      roomKey: null,
      sipKey: null,
      doCallOp: null,
      playSound: null,
      callNumber: null
    };
    this.sentHangup = false;
    this.isShowNotification = false;

    this.sipCheckObject = {
      timer: null,
      cancelToken: null,
      stopped: false
    };
  }

  haveWebRTCInformation() {
    const webRTCInfo = talkApi.webrtcInformation();
    return (
      webRTCInfo && webRTCInfo.rtcserver != null && webRTCInfo.rtcserver != ''
    );
  }

  ensureWebRTCInformation() {
    const webRTCInfo = talkApi.webrtcInformation();
    return {
      useWebSocket: webRTCInfo.method == 'https' ? false : true,
      serverRTC: webRTCInfo.rtcserver,
      httpPortRTC: webRTCInfo.httpsport,
      wsPortRTC: webRTCInfo.wssport,
      restPort: webRTCInfo.api,
      restProtocol: 'https',
      platform: 'electron',
      version: webRTCInfo.version
    };
  }

  handleClosed() {
    this.setState({
      openSnackbar: false
    });
  }

  showAlert(msg) {
    this.setState({
      errorSnackbar: msg,
      openSnackbar: true
    });
  }

  componentDidMount() {
    const { actions } = this.props;

    const urlParams = new URLSearchParams(global.location.search);
    const audio = urlParams.get('audio');
    const userKey = urlParams.get('userkey');
    const myUserKey = urlParams.get('myuserkey');
    const displayName = urlParams.get('displayname');
    const direction = urlParams.get('direction');
    const roomKey = urlParams.get('roomkey');
    const sipKey = urlParams.get('sipkey');

    const userData = ipcRenderer.sendSync(
      constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
      constantsApp.ACTION_SYNC_GET_USER_BY_USER_KEY,
      { userKey: userKey }
    );

    let data = { ...userData, userKey: userKey, displayName: displayName };

    this.setState({
      userKey: userKey,
      audio: audio,
      userData: data,
      direction: direction,
      myUserKey: myUserKey,
      roomKey: roomKey,
      sipKey: sipKey
    });

    ipcRenderer.send(constantsApp.SEND_EVENT_LOAD_APP_SETTING, '');
    ipcRenderer.on(
      constantsApp.REPLY_EVENT_LOAD_APP_SETTING,
      (event, setting, alreadyLogin, languageMap) => {
        // update domain for API
        talkApi.transferSettingInfoFromElectronToBrowser();
        actions.requestInitAppSetting(setting, {}, alreadyLogin);
      }
    );

    ipcRenderer.on(constantsApp.MAIN_TO_RENDER_EVENT, (event, action, args) => {
      if (action == constantsApp.ACTION_EXTRA_LOGIN_CHANGED) {
        talkApi.updateAllAccountInfo();
        actions.requestUpdateExtraLoginInfo(args);
        return;
      }

      if (action == constantsApp.ACTION_SIP_RESULT) {
        const { sipKey } = this.state;
        let result = args.result;
        let sipNumber = args.sipNumber;

        if (
          sipKey &&
          sipKey == args.sipKey &&
          (sipDataReceived.sipNumber == null ||
            sipDataReceived.sipNumber == sipNumber)
        ) {
          sipDataReceived.sipKey = sipKey;
          sipDataReceived.sipNumber = sipNumber;

          if (result == SIP_RESULT_STATUS.success) {
            //success
            this.setState({
              doCallOp: { time: Date.now(), target: sipNumber }
            });
          } else if (result == SIP_RESULT_STATUS.busy) {
            //busy
            this.showNotification('call_busy');
            this.delayQuit();
          } else if (result == SIP_RESULT_STATUS.error) {
            //failed connect or error
            this.showNotification('call_error');
            this.delayQuit();
          }
        }

        return;
      }

      if (action == constantsApp.ACTION_SIP_END) {
        const { sipKey } = this.state;
        if (sipKey && sipKey == args.sipKey) {
          if (args.apiID == 'SIPHANGUP') {
            let from = args.fromKey;
            let reason = args.reason;
            let sipNumber = args.sipNumber;

            if (
              sipDataReceived.sipNumber == null ||
              sipDataReceived.sipNumber == sipNumber
            ) {
              if (from != myUserKey) {
                if (reason == SIP_HANGUP_REASON.busy) {
                  this.showNotification('call_busy');
                } else if (reason == SIP_HANGUP_REASON.missed_call) {
                  this.showNotification('missed_call');
                }
              }
              this.delayQuit();
            }
          } else {
            // sip closed
            this.delayQuit();
          }
        }
      }
    });

    ipcRenderer.sendSync(
      constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
      constantsApp.ACTION_SYNC_VIDEO_ONE_ONE_IS_LOADED,
      true
    );
  }

  componentWillUnmount() {
    sipDataReceived.sipNumber = null;
    ipcRenderer.removeAllListeners(constantsApp.REPLY_EVENT_LOAD_APP_SETTING);
    ipcRenderer.removeAllListeners(constantsApp.MAIN_TO_RENDER_EVENT);
  }

  setFlashBadgeIcon() {
    if (process.platform == 'win32') {
      ipcRenderer.sendSync(
        constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
        constantsApp.ACTION_SYNC_SET_MAIN_FLASH_FRAME,
        null
      );
    }
  }

  showNotification(message) {
    if (this.isShowNotification) {
      return;
    }
    this.isShowNotification = true;
    const { t } = this.props;
    const { userData, audio } = this.state;
    let title = `[${audio != 1 ? t('video call') : t('audio call')}] ${
      userData.displayName
    }`;

    let msg = message.includes(':') ? message : t(message);

    const url = talkApi.gwUserPhotoURL(
      userData.userKey,
      false,
      50,
      50,
      userData.userPhotoTime
    );
    const fileName = crypto
      .createHash('sha1')
      .update(url)
      .digest('hex');
    const filePath = path.join(talkApi.photoUserDir(), fileName);

    new Notification(title, {
      body: msg,
      icon: filePath
    });
    this.setFlashBadgeIcon();
  }

  delayQuit() {
    setTimeout(() => {
      this.quit();
    }, 1000);
  }

  delayQuit3000() {
    setTimeout(() => {
      this.quit();
    }, 3000);
  }

  quit() {
    console.log('app quit');
    window.isUserQuiting = true;
    remote.getCurrentWindow().close();
  }

  sendSIPWithAction({ mode, duration, result, reason, inputCallNumber }) {
    if (mode == 'hangup' || mode == 'close') {
      if (this.sentHangup) {
        return;
      }
    }

    let iDuration;
    if (duration) {
      iDuration = parseInt(duration);
    }

    const {
      userData,
      direction,
      myUserKey,
      roomKey,
      sipKey,
      audio
    } = this.state;
    let from;
    let to;

    let callNumber = buildMyNumber(talkApi.domain, myUserKey);
    if (mode == 'result') {
      if (direction == constantsApp.CALL_DIRECTION.INBOUND) {
        //incomming
        from = userData.userKey;
        to = myUserKey;
      } else {
        from = myUserKey;
        to = userData.userKey;
      }
    } else {
      from = myUserKey;
      to = userData.userKey;
    }

    ipcRenderer.send(
      constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
      constantsApp.ACTION_SEND_SIP_HTTP_REQUEST,
      {
        mode: mode,
        roomKey: roomKey,
        fromKey: from,
        toKey: to,
        sipKey: sipKey,
        duration: iDuration,
        reason: reason,
        video: audio != 1 ? 1 : 0,
        result: result,
        sipNumber: inputCallNumber ?? callNumber
      }
    );

    if (mode == 'hangup' || mode == 'close') {
      this.sentHangup = true;
    }
  }

  /**
   * @description start sip check
   */

  startSipCheck = d => {
    if (USE_SIP_CHECK_LOOP) {
      const { myUserKey, roomKey, sipKey } = this.state;

      this.sipCheckObject.timer = setTimeout(() => {
        if (this.sipCheckObject.stopped) {
          return;
        }
        this.sipCheckObject.cancelToken = simpleRequest.createCancelToken();
        simpleRequest
          .sipCheckValid({
            domain: talkApi.domain,
            roomKey: roomKey,
            loginKey: talkApi.autoLoginKey,
            sipKey: sipKey,
            myKey: myUserKey,
            authKey: talkApi.newAutoLoginKey,
            cancelToken: this.sipCheckObject.cancelToken
          })
          .then(result => {
            this.sipCheckObject.cancelToken = null;
            if (result == 0) {
              this.delayQuit3000();
              return;
            }
            this.startSipCheck(5000);
          })
          .catch(error => {
            this.sipCheckObject.cancelToken = null;
            this.startSipCheck(5000);
          });
      }, d);
    }
  };

  stopSipCheck = () => {
    if (USE_SIP_CHECK_LOOP) {
      clearTimeout(this.sipCheckObject.timer);
      this.sipCheckObject.stopped = true;
      if (this.sipCheckObject.cancelToken) {
        this.sipCheckObject.cancelToken.cancel();
        this.sipCheckObject.cancelToken = null;
      }
    }
  };

  playSoundCall = playing => {
    this.setState({
      playSound: playing
    });
  };

  render() {
    const { classes, t } = this.props;
    const {
      openSnackbar,
      errorSnackbar,
      userData,
      audio,
      direction,
      myUserKey,
      doCallOp,
      playSound
    } = this.state;

    return (
      <div className={classes.main}>
        {this.haveWebRTCInformation() &&
          userData?.userKey &&
          talkApi.autoLoginKey &&
          talkApi.domain &&
          myUserKey &&
          direction && (
            <MainOneOnePage
              t={t}
              onCloseWindows={this.quit}
              incomingCall={
                direction == constantsApp.CALL_DIRECTION.INBOUND ? true : false
              }
              talkApi={talkApi}
              audio={audio}
              userData={userData}
              myUserKey={myUserKey}
              showWindows={() => {
                remote.getCurrentWindow().show();
              }}
              configsInformation={this.ensureWebRTCInformation()}
              sendSIPWithAction={this.sendSIPWithAction}
              doCallOp={doCallOp}
              showNotification={this.showNotification}
              startSipCheck={this.startSipCheck}
              stopSipCheck={this.stopSipCheck}
              playSoundCall={this.playSoundCall}
              openLinkWithURL={openLinkWithURL}
              remoteAskMediaAccess={remoteAskMediaAccess}
              requireCheckDeviceCapture={requireCheckDeviceCapture}
            />
          )}

        {playSound && (
          <audio
            style={{ position: 'absolute' }}
            src="./images/calling6.wav"
            autoPlay
            loop
          ></audio>
        )}

        <Snackbar
          autoHideDuration={2000}
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={openSnackbar}
          onClose={() => this.handleClosed()}
          message={errorSnackbar}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    logged_user: state.auth.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation()(withStyles(AppCallPage)));
