import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import RoomList from '../screens/home/messenger/RoomList';
import * as Actions from '../actions';

function mapStateToProps(state) {
  return {
    appUI: state.appUI,
    org_time_card: state.company.org_time_card,
    listActualyFav: state.company.listActualyFav,
    rooms: state.room_list.rooms,
    user: state.auth.user.account_info,
    user_cached: state.company.user_cached,
    search_text: state.search_result.search_text,
    allSeachResult: state.search_result.allSeachResult,
    commonSettings: state.setting.common
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomList);
