import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Home from '../screens/home';
import * as Actions from '../actions';

function mapStateToProps(state) {
  return {
    selected_room: state.appUI.selected_room,
    selected_whisper: state.appUI.selected_whisper,
    logged_user: state.auth.user,
    group_cached: state.company.group_cached,
    org: state.company.org,
    rooms: state.room_list.rooms,
    room_cached: state.room_list.room_cached,
    user_cached: state.company.user_cached,
    room_info: state.message_list.roomInfo,
    show_search_result: state.appUI.show_search_result,
    showRightPannel: state.appUI.showRightPannel,
    showRightBoardPannel: state.appUI.showRightBoardPannel,
    search_text: state.search_result.search_text,
    allUnreadMessage: state.room_list.allUnreadMessage,
    allUnreadMessageBadgeIcon: state.room_list.allUnreadMessageBadgeIcon,
    groupwareUnreadCount: state.appUI.groupwareUnreadCount,
    commonSettings: state.setting.common
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
