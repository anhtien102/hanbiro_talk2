import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ChatWindowsContent from '../screens/home/messenger/ChatWindowContent';
import * as Actions from '../actions';

function mapStateToProps(state) {
  return {
    appUI: state.appUI,
    message_list: state.message_list,
    user_logged: state.auth.user,
    logged_user: state.auth.user,
    user_cached: state.company.user_cached,
    room_info: state.message_list.roomInfo
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatWindowsContent);
