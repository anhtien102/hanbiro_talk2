import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ChatContent from '../screens/home/messenger/ChatContent';
import * as Actions from '../actions';

function mapStateToProps(state) {
  return {
    appUI: state.appUI,
    message_list: state.message_list,
    user_logged: state.auth.user,
    userCached: state.company.user_cached,
    commonSettings: state.setting.common
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatContent);
