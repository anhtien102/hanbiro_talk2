import React, { Fragment, useEffect } from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { Provider, useSelector, useDispatch } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { hot } from 'react-hot-loader/root';
import { createTheme, LanguageSetting } from './configs';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Login from './screens/login';
import HomePage from './containers/HomeContainer';
import CapturePage from 'capture.js';
import ChatPage from 'chat.js';
import VideoPage from 'video_room.js';
import CallPage from './app_call.js';
import RemoteControlPage from './remote_control';
import { configureStore, history } from './store';
import * as constantsApp from './configs/constant';
import * as Actions from './actions';
import * as Utils from './utils';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { ipcRenderer } from 'electron';
import './app.global.css';
import { LocalizationProvider } from '@material-ui/pickers';
import MomentUtils from '@material-ui/pickers/adapter/moment';

const store = configureStore();
const Container = process.env.PLAIN_HMR ? Fragment : AppContainer;
const Reload = hot(Main);

function Main(props) {
  const { store, history } = props;

  //language code;
  const lang = ipcRenderer.sendSync(
    constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
    constantsApp.ACTION_SYNC_GET_LANGUAGE
  );

  i18n.use(initReactI18next).init({
    resources: LanguageSetting.resourcesLanguage,
    lng: lang,
    fallbackLng: LanguageSetting.defaultLanguage,
    interpolation: {
      escapeValue: false
    }
  });

  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </Provider>
  );
}

function App() {
  const dispatch = useDispatch();
  const auth = useSelector(state => state.auth);
  const theme = createTheme();

  useEffect(() => {
    const storageFont = Utils.getDataFromStorage(constantsApp.STORAGE_FONT);
    const storageFontSize = Utils.getDataFromStorage(
      constantsApp.STORAGE_FONT_SIZE
    );
    const storagePrimaryTheme = Utils.getDataFromStorage(
      constantsApp.STORAGE_PRIMARY_THEME
    );
    const storageSecondaryTheme = Utils.getDataFromStorage(
      constantsApp.STORAGE_SECONDARY_THEME
    );
    const storageDarkTheme = Utils.getDataFromStorage(
      constantsApp.STORAGE_DARK_MODE
    );

    const storageCommonSetting = Utils.getDataFromStorage(
      constantsApp.STORAGE_COMMON_SETTING
    );

    if (storageFont) {
      dispatch(Actions.onChangeFont(storageFont));
    }
    if (storageFontSize) {
      dispatch(Actions.onChangeFontSize(storageFontSize));
    }
    if (storagePrimaryTheme) {
      dispatch(Actions.changePrimaryTheme(storagePrimaryTheme));
    }
    if (storageSecondaryTheme) {
      dispatch(Actions.changeSecondaryTheme(storageSecondaryTheme));
    }
    if (storageDarkTheme) {
      dispatch(Actions.changeDarkTheme(storageDarkTheme));
    }
    if (storageCommonSetting) {
      dispatch(Actions.changeCommonSetting(storageCommonSetting));
    }
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <LocalizationProvider dateAdapter={MomentUtils}>
        {auth.authentication ? <HomePage /> : <Login />}
      </LocalizationProvider>
    </ThemeProvider>
  );
}

function ChatApp() {
  const dispatch = useDispatch();
  const theme = createTheme();

  useEffect(() => {
    const storageFont = Utils.getDataFromStorage(constantsApp.STORAGE_FONT);
    const storageFontSize = Utils.getDataFromStorage(
      constantsApp.STORAGE_FONT_SIZE
    );
    const storagePrimaryTheme = Utils.getDataFromStorage(
      constantsApp.STORAGE_PRIMARY_THEME
    );
    const storageSecondaryTheme = Utils.getDataFromStorage(
      constantsApp.STORAGE_SECONDARY_THEME
    );
    const storageDarkTheme = Utils.getDataFromStorage(
      constantsApp.STORAGE_DARK_MODE
    );

    if (storageFont) {
      dispatch(Actions.onChangeFont(storageFont));
    }
    if (storageFontSize) {
      dispatch(Actions.onChangeFontSize(storageFontSize));
    }
    if (storagePrimaryTheme) {
      dispatch(Actions.changePrimaryTheme(storagePrimaryTheme));
    }
    if (storageSecondaryTheme) {
      dispatch(Actions.changeSecondaryTheme(storageSecondaryTheme));
    }
    if (storageDarkTheme) {
      dispatch(Actions.changeDarkTheme(storageDarkTheme));
    }
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <LocalizationProvider dateAdapter={MomentUtils}>
        <ChatPage />
      </LocalizationProvider>
    </ThemeProvider>
  );
}

function VideoApp() {
  const dispatch = useDispatch();
  const theme = createTheme();

  useEffect(() => {
    const storageFont = Utils.getDataFromStorage(constantsApp.STORAGE_FONT);
    const storageFontSize = Utils.getDataFromStorage(
      constantsApp.STORAGE_FONT_SIZE
    );
    const storagePrimaryTheme = Utils.getDataFromStorage(
      constantsApp.STORAGE_PRIMARY_THEME
    );
    const storageSecondaryTheme = Utils.getDataFromStorage(
      constantsApp.STORAGE_SECONDARY_THEME
    );
    const storageDarkTheme = 'dark'; //Utils.getDataFromStorage(constantsApp.STORAGE_DARK_MODE);

    if (storageFont) {
      dispatch(Actions.onChangeFont(storageFont));
    }
    if (storageFontSize) {
      dispatch(Actions.onChangeFontSize(storageFontSize));
    }
    if (storagePrimaryTheme) {
      dispatch(Actions.changePrimaryTheme(storagePrimaryTheme));
    }
    if (storageSecondaryTheme) {
      dispatch(Actions.changeSecondaryTheme(storageSecondaryTheme));
    }
    if (storageDarkTheme) {
      dispatch(Actions.changeDarkTheme(storageDarkTheme));
    }
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <LocalizationProvider dateAdapter={MomentUtils}>
        <VideoPage />
      </LocalizationProvider>
    </ThemeProvider>
  );
}

function CallApp() {
  const dispatch = useDispatch();
  const theme = createTheme();

  useEffect(() => {
    const storageFont = Utils.getDataFromStorage(constantsApp.STORAGE_FONT);
    const storageFontSize = Utils.getDataFromStorage(
      constantsApp.STORAGE_FONT_SIZE
    );
    const storagePrimaryTheme = Utils.getDataFromStorage(
      constantsApp.STORAGE_PRIMARY_THEME
    );
    const storageSecondaryTheme = Utils.getDataFromStorage(
      constantsApp.STORAGE_SECONDARY_THEME
    );
    const storageDarkTheme = 'light'; //Utils.getDataFromStorage(constantsApp.STORAGE_DARK_MODE);

    if (storageFont) {
      dispatch(Actions.onChangeFont(storageFont));
    }
    if (storageFontSize) {
      dispatch(Actions.onChangeFontSize(storageFontSize));
    }
    if (storagePrimaryTheme) {
      dispatch(Actions.changePrimaryTheme(storagePrimaryTheme));
    }
    if (storageSecondaryTheme) {
      dispatch(Actions.changeSecondaryTheme(storageSecondaryTheme));
    }
    if (storageDarkTheme) {
      dispatch(Actions.changeDarkTheme(storageDarkTheme));
    }
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <LocalizationProvider dateAdapter={MomentUtils}>
        <CallPage />
      </LocalizationProvider>
    </ThemeProvider>
  );
}

function RemoteControlApp() {
  const dispatch = useDispatch();
  const theme = createTheme();

  useEffect(() => {
    const storageFont = Utils.getDataFromStorage(constantsApp.STORAGE_FONT);
    const storageFontSize = Utils.getDataFromStorage(
      constantsApp.STORAGE_FONT_SIZE
    );
    const storagePrimaryTheme = Utils.getDataFromStorage(
      constantsApp.STORAGE_PRIMARY_THEME
    );
    const storageSecondaryTheme = Utils.getDataFromStorage(
      constantsApp.STORAGE_SECONDARY_THEME
    );
    const storageDarkTheme = 'dark'; //Utils.getDataFromStorage(constantsApp.STORAGE_DARK_MODE);

    if (storageFont) {
      dispatch(Actions.onChangeFont(storageFont));
    }
    if (storageFontSize) {
      dispatch(Actions.onChangeFontSize(storageFontSize));
    }
    if (storagePrimaryTheme) {
      dispatch(Actions.changePrimaryTheme(storagePrimaryTheme));
    }
    if (storageSecondaryTheme) {
      dispatch(Actions.changeSecondaryTheme(storageSecondaryTheme));
    }
    if (storageDarkTheme) {
      dispatch(Actions.changeDarkTheme(storageDarkTheme));
    }
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <RemoteControlPage />
    </ThemeProvider>
  );
}

let content;
let mainElement = document.getElementById('root');
const isRoot = mainElement != null;

if (isRoot) {
  content = <Reload store={store} history={history} />;
} else {
  mainElement = document.getElementById('root_chat');
  if (mainElement) {
    function ChatMain(props) {
      const { store, history } = props;

      //language code;
      const lang = ipcRenderer.sendSync(
        constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
        constantsApp.ACTION_SYNC_GET_LANGUAGE
      );

      i18n.use(initReactI18next).init({
        resources: LanguageSetting.resourcesLanguage,
        lng: lang,
        fallbackLng: LanguageSetting.defaultLanguage,
        interpolation: {
          escapeValue: false
        }
      });
      return (
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <ChatApp />
          </ConnectedRouter>
        </Provider>
      );
    }
    const ChatReload = hot(ChatMain);

    content = <ChatReload store={store} history={history} />;
  } else {
    mainElement = document.getElementById('root_video');
    if (mainElement) {
      function ChatMain(props) {
        const { store, history } = props;

        //language code;
        const lang = ipcRenderer.sendSync(
          constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
          constantsApp.ACTION_SYNC_GET_LANGUAGE
        );

        i18n.use(initReactI18next).init({
          resources: LanguageSetting.resourcesLanguage,
          lng: lang,
          fallbackLng: LanguageSetting.defaultLanguage,
          interpolation: {
            escapeValue: false
          }
        });
        return (
          <Provider store={store}>
            <ConnectedRouter history={history}>
              <VideoApp />
            </ConnectedRouter>
          </Provider>
        );
      }
      const ChatReload = hot(ChatMain);

      content = <ChatReload store={store} history={history} />;
    } else if (mainElement) {
      function ChatMain(props) {
        const { store, history } = props;

        //language code;
        const lang = ipcRenderer.sendSync(
          constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
          constantsApp.ACTION_SYNC_GET_LANGUAGE
        );

        i18n.use(initReactI18next).init({
          resources: LanguageSetting.resourcesLanguage,
          lng: lang,
          fallbackLng: LanguageSetting.defaultLanguage,
          interpolation: {
            escapeValue: false
          }
        });
        return (
          <Provider store={store}>
            <ConnectedRouter history={history}>
              <VideoApp />
            </ConnectedRouter>
          </Provider>
        );
      }
      const ChatReload = hot(ChatMain);

      content = <ChatReload store={store} history={history} />;
    } else {
      mainElement = document.getElementById('app_call');
      if (mainElement) {
        function ChatMain(props) {
          const { store, history } = props;

          //language code;
          const lang = ipcRenderer.sendSync(
            constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
            constantsApp.ACTION_SYNC_GET_LANGUAGE
          );

          i18n.use(initReactI18next).init({
            resources: LanguageSetting.resourcesLanguage,
            lng: lang,
            fallbackLng: LanguageSetting.defaultLanguage,
            interpolation: {
              escapeValue: false
            }
          });
          return (
            <Provider store={store}>
              <ConnectedRouter history={history}>
                <CallApp />
              </ConnectedRouter>
            </Provider>
          );
        }
        const ChatReload = hot(ChatMain);

        content = <ChatReload store={store} history={history} />;
      }
      mainElement = document.getElementById('remote_control');
      if (mainElement) {
        function RemoteControlMain(props) {
          const { store, history } = props;

          //language code;
          const lang = ipcRenderer.sendSync(
            constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
            constantsApp.ACTION_SYNC_GET_LANGUAGE
          );

          i18n.use(initReactI18next).init({
            resources: LanguageSetting.resourcesLanguage,
            lng: lang,
            fallbackLng: LanguageSetting.defaultLanguage,
            interpolation: {
              escapeValue: false
            }
          });
          return (
            <Provider store={store}>
              <ConnectedRouter history={history}>
                <RemoteControlApp />
              </ConnectedRouter>
            </Provider>
          );
        }
        const RemoteControlReload = hot(RemoteControlMain);

        content = <RemoteControlReload store={store} history={history} />;
      } else {
        mainElement = document.getElementById('capture');
        function CaptureMain(props) {
          return <CapturePage />;
        }
        const CaptureReload = hot(CaptureMain);
        content = <CaptureReload />;
      }
    }
  }
}

render(<Container>{content}</Container>, mainElement);
