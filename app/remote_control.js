import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from './actions';
import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import talkApi from './core/service/talk.api.render';
// import MainRemoteControlPage from './janus_client/src/app/remote-control/main_remote_control';
import withStyles from './remote_control_styles';
import { ipcRenderer, remote } from 'electron';
import * as constantsApp from './configs/constant';

window.onerror = function(error, url, line) {
  console.log(error, url, line);
};

window.platform = process.platform;

class AppRemoteControlPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      remoteId: null
    };
  }

  haveWebRTCInformation() {
    const webRTCInfo = talkApi.webrtcInformation();
    return (
      webRTCInfo && webRTCInfo.rtcserver != null && webRTCInfo.rtcserver != ''
    );
  }

  ensureWebRTCInformation() {
    const webRTCInfo = talkApi.webrtcInformation();
    return {
      useWebSocket: webRTCInfo.method == 'https' ? false : true,
      serverRTC: webRTCInfo.rtcserver,
      httpPortRTC: webRTCInfo.httpsport,
      wsPortRTC: webRTCInfo.wssport,
      restPort: webRTCInfo.api,
      restProtocol: 'https',
      platform: 'electron',
      version: webRTCInfo.version
    };
  }

  componentDidMount() {
    const { actions } = this.props;

    const urlParams = new URLSearchParams(global.location.search);
    const remoteId = urlParams.get('remoteId') ?? '';
    this.setState({
      remoteId: remoteId
    });

    ipcRenderer.send(constantsApp.SEND_EVENT_LOAD_APP_SETTING, '');
    ipcRenderer.on(
      constantsApp.REPLY_EVENT_LOAD_APP_SETTING,
      (event, setting, alreadyLogin, languageMap) => {
        // update domain for API
        talkApi.transferSettingInfoFromElectronToBrowser();
        actions.requestInitAppSetting(setting, {}, alreadyLogin);
      }
    );

    ipcRenderer.on(constantsApp.MAIN_TO_RENDER_EVENT, (event, action, args) => {
      if (action == constantsApp.ACTION_EXTRA_LOGIN_CHANGED) {
        talkApi.updateAllAccountInfo();
        actions.requestUpdateExtraLoginInfo(args);
        return;
      }
      if (action == constantsApp.ACTION_REMOTE_UPDATE_ID) {
        if (args.remoteId) {
          this.setState({
            remoteId: args.remoteId
          });
        }
      }
    });
  }

  componentWillUnmount() {
    ipcRenderer.removeAllListeners(constantsApp.REPLY_EVENT_LOAD_APP_SETTING);
    ipcRenderer.removeAllListeners(constantsApp.MAIN_TO_RENDER_EVENT);
  }

  robotControlAction(data) {
    ipcRenderer.send(
      constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
      constantsApp.ACTION_ROBOT_CONTROL,
      data
    );
  }

  quit() {
    console.log('app quit');
    remote.getCurrentWindow().close();
  }

  render() {
    const { classes, t, user_cached } = this.props;
    const { remoteId } = this.state;

    const myUserKey = talkApi.shareObj?.account_info?.user_key;
    const contact = myUserKey ? user_cached[myUserKey] : null;
    const displayName = contact
      ? contact.displayName
      : talkApi.shareObj?.account_info?.user_id;

    return (
      <div className={classes.main}>
        {this.haveWebRTCInformation() &&
          talkApi.autoLoginKey &&
          talkApi.domain &&
          remoteId != null && (
            // <MainRemoteControlPage
            //   t={t}
            //   remoteId={remoteId}
            //   robotControlAction={this.robotControlAction}
            //   onCloseWindows={this.quit}
            //   talkApi={talkApi}
            //   configsInformation={this.ensureWebRTCInformation()}
            //   displayName={displayName}
            //   myUserKey={myUserKey}
            // />
            <div></div>
          )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    logged_user: state.auth.user,
    user_cached: state.company.user_cached
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation()(withStyles(AppRemoteControlPage)));
