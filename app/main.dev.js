/* eslint global-require: off */

/**
 * This module executes inside of electron's main process. You can start
 * electron renderer process from here and communicate with the other processes
 * through IPC.
 *
 * When running `yarn build` or `yarn build-main`, this file is compiled to
 * `./app/main.prod.js` using webpack. This gives us some performance wins.
 *
 * @flow
 */
import {
  app,
  BrowserWindow,
  ipcMain,
  Tray,
  nativeImage,
  Menu,
  screen,
  powerMonitor
} from 'electron';

import sudoComd from 'sudo-prompt';

import { autoUpdater } from 'electron-updater';
import log from 'electron-log';
import MenuBuilder from './menu';
import path from 'path';
import fs from 'fs';
import moment from 'moment';
import * as constantsApp from './configs/constant';
import { ensureFilePath } from './configs/file_cheat';

import {
  MessengerCenter,
  CALL_STATE_CALL
} from './core/service/messenger-center';
import myLogger from './core/logger/debug';
import appPrefs from './core/utils/pref';
import allTranslation from './language/electron.lang';
import * as talkDB from './core/service/database/db';
import sipController from './core/service/sipcontroller';
import remoteController from './core/service/remotecontroller';
import Badge from './core/utils/badge';

import windowStateKeeper from './core/utils/window.state.management';
var isQuiting = false;
var forceClosedVideoConferenceWindows = false;
var networkOnline = true;

var currentBadgeNumberGlobal = 0;

var isBasicUpdater = true;

const WINDOWS_BG_COLOR = '#333333';

const USE_DEVELOP_UPDATER_URL = false;

var ignoreSendOutRoom = false;

// import robot from 'robotjs';

var mainWindowsEnterHtmlFullscreenTimer;
var mainWindowsEnterHtmlFullscreen = false;

const messengerCenter = new MessengerCenter();

app.commandLine.appendSwitch('webrtc-max-cpu-consumption-percentage=100');

// app.commandLine.appendSwitch('allow-insecure-localhost', 'true');
//IGNORE ERR_CERT_COMMON_NAME_INVALID
app.commandLine.appendSwitch('ignore-certificate-errors', 'true');

app.disableHardwareAcceleration();
app.commandLine.appendSwitch('disable-software-rasterizer');

var myDirName = ensureFilePath(__dirname);

log.transports.console.format = '[{y}-{m}-{d} {h}:{i}:{s}] {text}';
log.transports.file.archiveLog = function(oldFile) {
  oldFile = oldFile.toString();
  const info = path.parse(oldFile);
  try {
    let files = fs.readdirSync(info.dir);
    let count = 0;

    for (var i = files.length - 1; i >= 0; i--) {
      let f = files[i];
      if (f.includes(messengerCenter.domain)) {
        if (count <= 4) {
          count++;
        } else {
          if (f != messengerCenter.domain + '.log') {
            let fPath = path.join(info.dir, f);
            fs.unlinkSync(fPath);
          }
        }
      }
    }

    fs.renameSync(
      oldFile,
      path.join(
        info.dir,
        info.name + '_' + moment().format('YYYY-MM-DD HH_mm_ss') + info.ext
      )
    );
  } catch (e) {
    console.warn('Could not rotate log', e);
  }
};

export default class AppUpdater {
  constructor() {
    autoUpdater.logger = log;
  }
}

function runAutoUpdate(domain, basicUpdate) {
  isBasicUpdater = basicUpdate;
  if (domain == null || domain == '') {
    return;
  }

  const updateDomain = domain;

  let url = '';
  if (basicUpdate) {
    url = `https://${updateDomain}/winapp/${updateDomain}/messenger/`;
    if (USE_DEVELOP_UPDATER_URL) {
      url = 'http://global3.hanbiro.com/nhan.nt/mobileteam/hanbirotalk/';
    }
  } else {
    url = `https://${updateDomain}/winapp/`;
    if (USE_DEVELOP_UPDATER_URL) {
      url =
        'http://global3.hanbiro.com/nhan.nt/mobileteam/hanbirotalk/serverpath';
    }
  }

  autoUpdater.setFeedURL({
    provider: 'generic',
    url: url
  });
  // autoUpdater
  //   .checkForUpdatesAndNotify()
  //   .then(r => {
  //     console.log('r', r);
  //   })
  //   .catch(err => {
  //     console.log(err);
  //   });
  autoUpdater
    .checkForUpdates()
    .then(r => {
      console.log('r', r);
    })
    .catch(err => {
      console.log(err);
    });
}

let mainWindowRequireClosed = false;
let mainWindow = null;

let captureWindows = [];
let videoConferenceWindows = null;
let currentVideoConferenceId = null;
let currentVideoConferenceRoomKey = null;
let currentVideoConferenceOutURL = null;

let tray = null;

let videoConferenceResizeTimer = null;
const DEFAULT_VIDEO_CONFERENCE_WINDOWS_RATIO = 2;
const DEFAULT_TITLE_BAR_HEIGH = 24;

const iconNameNormal = process.platform == 'linux' ? 'icon_32.png' : 'icon.ico';
const iconNormal = nativeImage.createFromPath(
  path.join(__dirname, 'images', iconNameNormal)
);
const iconNameUnread =
  process.platform == 'linux' ? 'icon_32_unread.png' : 'icon_unread.ico';
const iconUnread = nativeImage.createFromPath(
  path.join(__dirname, 'images', iconNameUnread)
);

let chatWindowsList = {};

function destroyTray() {
  if (tray) {
    tray.destroy();
    tray = null;
  }
}

const isPrimaryInstance = app.requestSingleInstanceLock();
if (!isPrimaryInstance) {
  console.log('Second instance => quit');
  app.quit();
} else {
  if (process.env.NODE_ENV === 'production') {
    const sourceMapSupport = require('source-map-support');
    sourceMapSupport.install();
  }

  if (
    process.env.NODE_ENV === 'development' ||
    process.env.DEBUG_PROD === 'true'
  ) {
    require('electron-debug')(); // disable hot reload here opendevtools
  }

  function createTray() {
    if (process.platform === 'darwin') {
    } else {
      tray = new Tray(iconNormal);
      const contextMenu = Menu.buildFromTemplate([
        {
          label: allTranslation.text('Open'),
          click: () => {
            if (mainWindow != null) {
              mainWindow.show();
            }
          }
        },
        {
          label: allTranslation.text('Exit'),
          click: () => {
            destroyTray();
            app.quit();
          }
        }
      ]);
      if (process.platform === 'win32') {
        tray.on('click', () => {
          if (mainWindow != null) {
            mainWindow.show();
          }
        });
      }
      if (currentBadgeNumberGlobal <= 0) {
        tray.setToolTip(allTranslation.text('HanbiroTalk'));
      } else {
        let text = allTranslation
          .text('unread conversation')
          .replace('{1}', currentBadgeNumberGlobal);
        tray.setToolTip(text);
        tray.setImage(iconUnread);
      }

      tray.setContextMenu(contextMenu);
    }
  }

  function updateTrayIconAndTooltip() {
    if (process.platform === 'win32' || process.platform === 'linux') {
      if (tray && !tray.isDestroyed()) {
        if (currentBadgeNumberGlobal <= 0) {
          tray.setToolTip(allTranslation.text('HanbiroTalk'));
          tray.setImage(iconNormal);
        } else {
          let text = allTranslation
            .text('unread conversation')
            .replace('{1}', currentBadgeNumberGlobal);
          tray.setToolTip(text);
          tray.setImage(iconUnread);
        }
      }
    }
  }

  const installExtensions = async () => {
    const installer = require('electron-devtools-installer');
    const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
    const extensions = ['REACT_DEVELOPER_TOOLS', 'REDUX_DEVTOOLS'];

    return Promise.all(
      extensions.map(name => installer.default(installer[name], forceDownload))
    ).catch(console.log);
  };

  function setupMenuBar() {
    const menuBuilder = new MenuBuilder(mainWindow);
    menuBuilder.buildMenu();
  }

  const createWindow = async () => {
    if (
      process.env.NODE_ENV === 'development' ||
      process.env.DEBUG_PROD === 'true'
    ) {
      await installExtensions();
    }

    let mainWindowState = windowStateKeeper({
      defaultWidth: 1444,
      defaultHeight: 728
    });

    let options = {
      show: false,
      x: mainWindowState.x,
      y: mainWindowState.y,
      width: mainWindowState.width,
      height: mainWindowState.height,
      webPreferences: {
        nodeIntegrationInWorker: true,
        nodeIntegration: true,
        webviewTag: true
      },
      minWidth: 800,
      minHeight: 800
    };

    if (process.platform == 'linux') {
      options = {
        ...options,
        icon: path.join(__dirname, 'images', 'icon_linux.png')
      };
    } else if (process.platform == 'darwin') {
      options = {
        ...options,
        backgroundColor: WINDOWS_BG_COLOR
      };
    } else {
      options = {
        ...options,
        backgroundColor: WINDOWS_BG_COLOR,
        icon: path.join(__dirname, 'images', 'icon.ico')
      };
    }

    mainWindow = new BrowserWindow(options);
    const badgeOptions = { radius: 15 };
    new Badge({
      win: mainWindow,
      opts: badgeOptions,
      callback: badgeCount => {
        currentBadgeNumberGlobal = badgeCount;
        updateTrayIconAndTooltip();
      }
    });

    mainWindowState.manage(mainWindow);

    mainWindow.loadURL(`file://${myDirName}/app.html`);

    // @TODO: Use 'ready-to-show' event
    //        https://github.com/electron/electron/blob/master/docs/api/browser-window.md#using-ready-to-show-event
    mainWindow.webContents.on('did-finish-load', () => {
      if (!mainWindow) {
        throw new Error('"mainWindow" is not defined');
      }
      if (process.env.START_MINIMIZED) {
        mainWindow.minimize();
      } else {
        mainWindow.show();
        mainWindow.focus();
      }
    });

    mainWindow.webContents.on('crashed', function() {
      console.log('app crashed');
      destroyTray();
      app.quit();
    });

    mainWindow.on('unresponsive', function() {
      console.log('app unresponsive');
      destroyTray();
      app.quit();
    });

    mainWindow.on('enter-html-full-screen', () => {
      clearTimeout(mainWindowsEnterHtmlFullscreenTimer);
      mainWindowsEnterHtmlFullscreen = true;
    });

    mainWindow.on('leave-html-full-screen', () => {
      mainWindowsEnterHtmlFullscreenTimer = setTimeout(() => {
        mainWindowsEnterHtmlFullscreen = false;
      }, 1000);
    });

    mainWindow.addListener('leave-full-screen', () => {
      if (mainWindowRequireClosed) {
        mainWindowRequireClosed = false;
        if (mainWindow != null) {
          mainWindow.hide();
        }
      }
    });

    mainWindow.addListener('focus', () => {
      if (process.platform == 'win32' && mainWindow) {
        mainWindow.flashFrame(false);
      }

      if (!global.ShareGlobalObject.inLoginPage) {
        resumeMessengerCenterService(true);
        sendEventToBrowserWindow(
          constantsApp.MAIN_TO_RENDER_EVENT,
          constantsApp.ACTION_GOT_FOCUSED_WINDOWS_AGAIN,
          null
        );

        if (messengerCenter.isAuthenticated) {
          const curRoomKey = global.ShareKeepValue.CURRENT_ROOM_KEY;
          if (
            curRoomKey &&
            messengerCenter.userKey &&
            messengerCenter.userKey != ''
          ) {
            messengerCenter.sendAPIToSever(
              constantsApp.API_MARK_ALL_MESSAGE_AS_READ,
              {
                roomKey: curRoomKey,
                userKey: messengerCenter.userKey
              }
            );
          }
        }
      } else {
        closeVideoConfenceWindows();
        closeAllChatWindows();
      }
      closeAllWindows();
    });

    app.on('before-quit', function() {
      isQuiting = true;
    });

    mainWindow.on('close', event => {
      if (!isQuiting && !global.ShareGlobalObject.inLoginPage) {
        event.preventDefault();
        if (!mainWindowsEnterHtmlFullscreen) {
          if (mainWindow != null) {
            if (mainWindow.isFullScreen()) {
              mainWindowRequireClosed = true;
              mainWindow.setFullScreen(false);
            } else {
              mainWindowRequireClosed = false;
              mainWindow.hide();
            }
          }
        }

        event.returnValue = false;
      }
    });

    mainWindow.on('closed', () => {
      closeAllWindows();
      closeVideoConfenceWindows();
      closeAllChatWindows();
      mainWindowsEnterHtmlFullscreen = false;
      mainWindow = null;
    });

    // init language
    const lang = appPrefs.getLanguage();

    if (lang == null || lang == '') {
      const locale = app.getLocale();
      appPrefs.setLanguage(locale, langCode => {
        allTranslation.setLanguage(__dirname, langCode);
      });
    } else {
      appPrefs.updateLanguageCode(lang, langCode => {
        allTranslation.setLanguage(__dirname, langCode);
      });
    }

    //create after init language
    createTray();

    setupMenuBar();

    // Remove this if your app does not use auto updates
    // eslint-disable-next-line
    new AppUpdater();
  };

  const openCallOneOneWindows = async (
    roomKey,
    sipKey,
    displayName,
    direction,
    audio
  ) => {
    const videoConferenceInfo = messengerCenter.serverInfo.videoConferenceInfo;
    if (
      videoConferenceInfo.server2 == null ||
      videoConferenceInfo.server2 == '' ||
      videoConferenceInfo.rtcserver == null ||
      videoConferenceInfo.rtcserver == '' ||
      videoConferenceInfo.callOneOne == 0
    ) {
      return;
    }

    const BACKGROUND_WINDOWS = '#000';
    let mainWindowState = windowStateKeeper({
      file: 'video_call_one_one.json',
      maximize: false,
      fullScreen: false,
      defaultWidth: 900,
      defaultHeight:
        900 / DEFAULT_VIDEO_CONFERENCE_WINDOWS_RATIO + DEFAULT_TITLE_BAR_HEIGH
    });

    let options = {
      title: 'Call',
      show: false,
      x: mainWindowState.x,
      y: mainWindowState.y,
      width: mainWindowState.width,
      height: mainWindowState.height,
      minWidth: 300,
      minHeight:
        300 / DEFAULT_VIDEO_CONFERENCE_WINDOWS_RATIO + DEFAULT_TITLE_BAR_HEIGH,
      webPreferences: {
        nodeIntegrationInWorker: true,
        nodeIntegration: true
      }
    };

    if (process.platform == 'linux') {
      options = {
        ...options,
        icon: path.join(__dirname, 'images', 'icon_linux.png')
      };
    } else if (process.platform == 'darwin') {
      options = {
        ...options,
        backgroundColor: BACKGROUND_WINDOWS
      };
    } else {
      options = {
        ...options,
        backgroundColor: BACKGROUND_WINDOWS,
        icon: path.join(__dirname, 'images', 'icon.ico')
      };
    }

    const w = new BrowserWindow(options);
    sipController.view = w;
    mainWindowState.manage(w);
    const wUrl = `file://${myDirName}/app_call.html?displayname=${encodeURI(
      displayName
    )}&myuserkey=${
      messengerCenter.userKey
    }&roomkey=${roomKey}&sipkey=${sipKey ??
      ''}&direction=${direction}&userkey=${sipController.userKey ??
      ''}&audio=${audio ?? ''}`;

    w.loadURL(wUrl);

    w.webContents.on('did-finish-load', () => {
      if (!w) {
        throw new Error('"call one one" is not defined');
      }

      if (process.env.START_MINIMIZED) {
        w.minimize();
      }

      if (sipController.cachedRequest) {
        sendEventToBrowserWindow(
          constantsApp.MAIN_TO_RENDER_EVENT,
          sipController.cachedRequest.action,
          sipController.cachedRequest.data
        );
        sipController.cachedRequest = null;
      }

      messengerCenter.resetCallState(CALL_STATE_CALL.showCall);
    });

    w.on('closed', () => {
      if (sipController.extraURLs.length > 0) {
        messengerCenter.sendUnNeedResult(sipController.extraURLs);
      }

      sipController.reset();

      messengerCenter.resetCallState(CALL_STATE_CALL.init);

      messengerCenter.sendBothRoomListAndGettalkAPI(roomKey);
    });

    w.webContents.on('crashed', function() {
      console.log('openCallOneOneWindows app crashed');
      messengerCenter.resetCallState(CALL_STATE_CALL.init);
      closeVideoConfenceWindows();
    });

    w.on('unresponsive', function() {
      console.log('openCallOneOneWindows app unresponsive');
      messengerCenter.resetCallState(CALL_STATE_CALL.init);
      closeVideoConfenceWindows();
    });

    if (
      process.env.NODE_ENV === 'development' ||
      process.env.DEBUG_PROD === 'true'
    ) {
      w.openDevTools();
    }
  };

  const openRemoteControlWindows = async remoteId => {
    const videoConferenceInfo = messengerCenter.serverInfo.videoConferenceInfo;
    if (
      videoConferenceInfo.server2 == null ||
      videoConferenceInfo.server2 == '' ||
      videoConferenceInfo.rtcserver == null ||
      videoConferenceInfo.rtcserver == ''
    ) {
      return;
    }

    let options = {
      title: 'Remote Control',
      show: false,
      width: 500,
      height: 500,
      minWidth: 500,
      minHeight: 500,
      webPreferences: {
        nodeIntegrationInWorker: true,
        nodeIntegration: true
      }
    };

    if (process.platform == 'linux') {
      options = {
        ...options,
        icon: path.join(__dirname, 'images', 'icon_linux.png')
      };
    } else if (process.platform == 'darwin') {
      options = {
        ...options
      };
    } else {
      options = {
        ...options,
        icon: path.join(__dirname, 'images', 'icon.ico')
      };
    }

    const w = new BrowserWindow(options);
    remoteController.view = w;
    const wUrl = `file://${myDirName}/remote_control.html?remoteId=${remoteId ??
      ''}`;

    w.loadURL(wUrl);

    w.webContents.on('did-finish-load', () => {
      if (!w) {
        throw new Error('"call one one" is not defined');
      }

      if (process.env.START_MINIMIZED) {
        w.minimize();
      } else {
        w.show();
        w.focus();
      }
    });

    w.on('closed', () => {
      remoteController.reset();
    });

    w.webContents.on('crashed', function() {
      console.log('remotecontrol window app crashed');
      closeVideoConfenceWindows();
    });

    w.on('unresponsive', function() {
      console.log('remotecontrol window app unresponsive');
      closeVideoConfenceWindows();
    });

    if (
      process.env.NODE_ENV === 'development' ||
      process.env.DEBUG_PROD === 'true'
    ) {
      w.openDevTools();
    }
  };

  const openVideoConferenceWindows = async (url, roomKey, audio) => {
    let userLegacyWebRTC = false;
    const videoConferenceInfo = messengerCenter.serverInfo.videoConferenceInfo;
    if (
      videoConferenceInfo.server2 == null ||
      videoConferenceInfo.server2 == '' ||
      videoConferenceInfo.rtcserver == null ||
      videoConferenceInfo.rtcserver == ''
    ) {
      userLegacyWebRTC = true;
    }

    if (userLegacyWebRTC || !constantsApp.USE_NEW_LOGIN_KEY) {
      messengerCenter.sendAPIToSever(constantsApp.API_WEB_AUTO_LOGIN, {});
    }

    const BACKGROUND_WINDOWS = '#000';
    let mainWindowState = windowStateKeeper({
      file: 'openVideoConferenceWindows.json',
      defaultWidth: 900,
      defaultHeight:
        900 / DEFAULT_VIDEO_CONFERENCE_WINDOWS_RATIO + DEFAULT_TITLE_BAR_HEIGH
    });

    let options = {
      title: 'Video Conference',
      show: false,
      x: mainWindowState.x,
      y: mainWindowState.y,
      width: mainWindowState.width,
      height: mainWindowState.height,
      minWidth: 300,
      minHeight:
        300 / DEFAULT_VIDEO_CONFERENCE_WINDOWS_RATIO + DEFAULT_TITLE_BAR_HEIGH,
      webPreferences: {
        nodeIntegrationInWorker: true,
        nodeIntegration: true
      }
    };

    if (process.platform == 'linux') {
      options = {
        ...options,
        icon: path.join(__dirname, 'images', 'icon_linux.png')
      };
    } else if (process.platform == 'darwin') {
      if (userLegacyWebRTC) {
        options = {
          ...options,
          backgroundColor: BACKGROUND_WINDOWS,
          maximizable: false,
          fullscreen: false
        };
      } else {
        options = {
          ...options,
          backgroundColor: BACKGROUND_WINDOWS
        };
      }
    } else {
      if (userLegacyWebRTC) {
        options = {
          ...options,
          backgroundColor: BACKGROUND_WINDOWS,
          icon: path.join(__dirname, 'images', 'icon.ico'),
          maximizable: false,
          fullscreen: false
        };
      } else {
        options = {
          ...options,
          backgroundColor: BACKGROUND_WINDOWS,
          icon: path.join(__dirname, 'images', 'icon.ico')
        };
      }
    }

    videoConferenceWindows = new BrowserWindow(options);
    mainWindowState.manage(videoConferenceWindows);

    if (userLegacyWebRTC) {
      videoConferenceWindows.loadURL(url);
    } else {
      videoConferenceWindows.loadURL(
        `file://${myDirName}/video_room.html?roomkey=${roomKey}&audio=${audio ??
          ''}`
      );
    }

    videoConferenceWindows.webContents.on('did-finish-load', () => {
      if (!videoConferenceWindows) {
        throw new Error('"videoConferenceWindows" is not defined');
      }
      if (userLegacyWebRTC) {
        if (process.platform == 'darwin') {
          videoConferenceWindows.setAspectRatio(
            DEFAULT_VIDEO_CONFERENCE_WINDOWS_RATIO,
            {
              width: 0,
              height: DEFAULT_TITLE_BAR_HEIGH
            }
          );
        } else if (process.platform == 'linux') {
          videoConferenceWindows.on('maximize', () => {
            if (videoConferenceWindows != null) {
              videoConferenceWindows.unmaximize();
            }
          });

          videoConferenceWindows.on('resize', () => {
            clearTimeout(videoConferenceResizeTimer);
            videoConferenceResizeTimer = setTimeout(() => {
              if (videoConferenceWindows != null) {
                const curDisplay = screen.getDisplayMatching(
                  videoConferenceWindows.getBounds()
                );
                const workAreaSize = curDisplay
                  ? curDisplay.workAreaSize
                  : null;
                const workAreaWidth = workAreaSize ? workAreaSize.width : 0;
                const workAreaHeight = workAreaSize ? workAreaSize.height : 0;

                const size = videoConferenceWindows.getSize();
                let curWidth = size[0];
                if (curWidth < 500) {
                  curWidth = 500;
                }
                const curHeight = size[1];

                let newHeight =
                  curWidth / DEFAULT_VIDEO_CONFERENCE_WINDOWS_RATIO +
                  DEFAULT_TITLE_BAR_HEIGH;
                newHeight = parseInt(newHeight);

                let newWidth =
                  curHeight * DEFAULT_VIDEO_CONFERENCE_WINDOWS_RATIO;
                newWidth = parseInt(newWidth);

                if (newHeight > workAreaHeight) {
                  newHeight = curHeight;
                } else {
                  newWidth = curWidth;
                }

                if (curHeight != newHeight || newWidth != curWidth) {
                  videoConferenceWindows.setSize(newWidth, newHeight, false);
                }
              }
            }, 500);
          });
        } else if (process.platform == 'win32') {
          videoConferenceWindows.on('will-resize', (e, newBounds) => {
            if (videoConferenceWindows != null) {
              let newHeight =
                newBounds.width / DEFAULT_VIDEO_CONFERENCE_WINDOWS_RATIO +
                DEFAULT_TITLE_BAR_HEIGH;
              if (newBounds.height != newHeight) {
                e.preventDefault();
                newBounds.height = parseInt(newHeight);
                videoConferenceWindows.setBounds(newBounds);
              }
            }
          });
        }
      }

      if (process.env.START_MINIMIZED) {
        videoConferenceWindows.minimize();
      } else {
        videoConferenceWindows.show();
        videoConferenceWindows.focus();
      }

      sendVideoConferenceMessage({ isEnter: true, audio: audio });
    });

    videoConferenceWindows.on('closed', () => {
      if (currentVideoConferenceOutURL) {
        messengerCenter.sendUnNeedResult(currentVideoConferenceOutURL);
        currentVideoConferenceOutURL = null;
      }
      if (!ignoreSendOutRoom) {
        sendVideoConferenceMessage({ isEnter: false, audio: audio });
      }
      ignoreSendOutRoom = false;

      videoConferenceWindows = null;
      currentVideoConferenceId = null;
      currentVideoConferenceRoomKey = null;
      messengerCenter.videoConferenceRoomKey = null;
    });

    videoConferenceWindows.webContents.on('crashed', function() {
      console.log('videoConferenceWindows app crashed');
      closeVideoConfenceWindows();
    });

    videoConferenceWindows.on('unresponsive', function() {
      console.log('videoConferenceWindows app unresponsive');
      closeVideoConfenceWindows();
    });

    if (
      process.env.NODE_ENV === 'development' ||
      process.env.DEBUG_PROD === 'true'
    ) {
      videoConferenceWindows.openDevTools();
    }
  };

  const sendVideoConferenceMessage = param => {
    if (currentVideoConferenceRoomKey) {
      let msg = null;
      if (param.audio == 1) {
        msg = allTranslation.text(
          param.isEnter
            ? 'I participated in a audio conference.'
            : 'I went out at the audio conference.'
        );
      } else {
        msg = allTranslation.text(
          param.isEnter
            ? 'I participated in a video conference.'
            : 'I went out at the video conference.'
        );
      }

      const clientID = appPrefs.generateRandomUUID();
      messengerCenter.sendMessagePacket({
        roomKey: currentVideoConferenceRoomKey,
        clientId: clientID,
        userKey: messengerCenter.userKey,
        emoticon: '',
        message: msg
      });
    }
  };

  const createCaptureWindow = async (data, roomKey) => {
    if (captureWindows.length > 0) {
      captureWindows.forEach(window => {
        window.show();
      });
      return;
    }

    const zeroSource = data.find(obj => {
      return obj.sourceId == 0;
    });

    let displays = screen.getAllDisplays();
    if (zeroSource) {
      showCaptureWindowsWithDisplay(displays[0], zeroSource, roomKey);
    } else {
      for (let i = 0; i < displays.length; i++) {
        let display = displays[i];
        const displayID = display.id;
        const findSource = data.find(obj => {
          return obj.sourceId == displayID;
        });
        showCaptureWindowsWithDisplay(display, findSource, roomKey);
      }
    }
  };

  function showCaptureWindowsWithDisplay(display, findSource, roomKey) {
    if (!findSource) {
      return;
    }

    //win32
    let alwaysOnTop = false;
    if (process.platform == 'darwin') {
      alwaysOnTop = true;
    }

    const { width, height } = display.bounds;
    const capture = new BrowserWindow({
      width: width,
      height: height,
      x: display.bounds.x,
      y: display.bounds.y,
      minWidth: width,
      minHeight: height,
      alwaysOnTop: alwaysOnTop,
      show: false,
      skipTaskbar: true,
      resizable: false,
      maximizable: true,
      enableLargerThanScreen: true,
      transparent: true,
      frame: false,
      webPreferences: {
        nodeIntegration: true
      }
    });
    captureWindows.push(capture);

    let filePathThumb = encodeURIComponent(findSource.filePath);
    capture.loadURL(
      `file://${myDirName}/capture.html?path=${filePathThumb}&roomkey=${roomKey}`
    );

    capture.webContents.on('did-finish-load', () => {
      if (!capture) {
        throw new Error('"captureWindows" is not defined');
      }

      capture.show();
    });

    capture.on('closed', () => {
      const index = captureWindows.indexOf(capture);
      if (index > -1) {
        captureWindows.splice(index, 1);
      }
      captureWindows.forEach(window => {
        window.close();
      });
      captureWindows = [];
    });
  }

  const openChatWindows = async data => {
    let options = {
      title: 'Chat',
      show: false,
      width: 1024,
      height: 1024,
      minWidth: 600,
      minHeight: 600,
      webPreferences: {
        nodeIntegrationInWorker: true,
        nodeIntegration: true
      }
    };

    if (process.platform == 'linux') {
      options = {
        ...options,
        icon: path.join(__dirname, 'images', 'icon_linux.png')
      };
    } else if (process.platform == 'darwin') {
      options = {
        ...options,
        backgroundColor: WINDOWS_BG_COLOR
      };
    } else {
      options = {
        ...options,
        backgroundColor: WINDOWS_BG_COLOR,
        icon: path.join(__dirname, 'images', 'icon.ico')
      };
    }

    const chatWindows = new BrowserWindow(options);

    chatWindows.loadURL(
      `file://${myDirName}/chat.html?roomkey=${data.roomKey}`
    );

    chatWindows.webContents.on('did-finish-load', () => {
      if (!chatWindows) {
        throw new Error('"chatWindows" is not defined');
      }

      if (process.env.START_MINIMIZED) {
        chatWindows.minimize();
      } else {
        chatWindows.show();
        chatWindows.focus();
      }
    });

    chatWindows.on('closed', () => {
      chatWindowsList[data.roomKey] = null;
    });

    chatWindows.webContents.on('crashed', function() {
      console.log('chatWindows app crashed');
      chatWindows.close();
    });

    chatWindows.on('unresponsive', function() {
      console.log('chatWindows app unresponsive');
      chatWindows.close();
    });

    chatWindowsList[data.roomKey] = chatWindows;
  };

  function closeAllWindows() {
    captureWindows.forEach(window => {
      window.close();
    });
    captureWindows = [];
  }

  function closeVideoConfenceWindows() {
    if (videoConferenceWindows) {
      forceClosedVideoConferenceWindows = true;
      videoConferenceWindows.close();
      videoConferenceWindows = null;
      currentVideoConferenceId = null;
      currentVideoConferenceRoomKey = null;
      messengerCenter.videoConferenceRoomKey = null;
    }

    remoteController.closeRemoteControl();
    sipController.closeCall();
  }
  function closeAllChatWindows() {
    for (let key in chatWindowsList) {
      const window = chatWindowsList[key];
      if (window) {
        window.close();
      }
    }
    chatWindowsList = {};
  }

  /**
   * Add event listeners...
   */

  app.on('will-quit', () => {
    talkDB.closeDB();
    messengerCenter.disconnectSocket();
  });

  app.on('window-all-closed', () => {
    // Respect the OSX convention of having the application in memory even
    // after all windows have been closed
    console.log('window-all-closed');
    // if (process.platform !== 'darwin') {
    talkDB.closeDB();
    destroyTray();
    app.quit();
    // }
  });

  // autoUpdater.on('update-available', info => {
  //   console.log('update-available', info);
  // });

  autoUpdater.on('update-downloaded', (event, info) => {
    sendEventToBrowserWindow(
      constantsApp.MAIN_TO_RENDER_EVENT,
      constantsApp.ACTION_NEW_UPDATE_VERSION_DOWNLOADED,
      {
        version: event.version,
        releaseNotes: event.releaseNotes
      }
    );
  });

  autoUpdater.on('error', error => {
    if (isBasicUpdater) {
      const status = error.message;
      if (
        status &&
        (status.includes('status 404: Not Found') ||
          status.includes('HttpError: 404 Not Found'))
      ) {
        appPrefs.loadAllAppSetting((setting, alreadyLogin) => {
          const domain =
            setting && setting.account_info
              ? setting.account_info.domain
              : null;
          runAutoUpdate(domain, false);
        });
      }
    }
  });

  app.on('second-instance', (event, commandLine, workingDirectory) => {
    if (mainWindow) {
      if (mainWindow.isMinimized()) {
        mainWindow.restore();
      } else {
        mainWindow.show();
      }
      mainWindow.focus();
    }
  });

  app.on('ready', () => {
    app.setAppUserModelId('com.hanbiro.hanbirotalkmac');
    powerMonitor.on('suspend', () => {
      console.log('suspend');
      stopMessengerCenterService();
    });
    powerMonitor.on('lock-screen', () => {
      console.log('lock-screen');
      stopMessengerCenterService();
    });
    powerMonitor.on('resume', () => {
      console.log('resume');
      resumeMessengerCenterService(true);
    });
    powerMonitor.on('unlock-screen', () => {
      console.log('unlock-screen');
      resumeMessengerCenterService(true);
    });
    createWindow();
  });

  app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
      createWindow();
    } else {
      mainWindow.show();
      if (process.platform == 'darwin') {
        if (videoConferenceWindows) {
          videoConferenceWindows.moveTop();
        } else if (sipController.view) {
          sipController.view.moveTop();
        } else if (remoteController.view) {
          remoteController.moveToTop();
        }
      }
    }
  });

  function sendEventToBrowserWindow(
    event,
    actionName,
    arg,
    setting = null,
    ...extra
  ) {
    sendEventToMainBrowserWindow(event, actionName, arg, setting, ...extra);
    sendEventToExtraBrowserWindows(event, actionName, arg, setting, ...extra);
  }

  function sendEventToMainBrowserWindow(
    event,
    actionName,
    arg,
    setting = null,
    ...extra
  ) {
    if (mainWindow && mainWindow.webContents) {
      mainWindow.webContents.send(event, actionName, arg, setting, extra);
    }
  }

  function sendEventToExtraBrowserWindows(
    event,
    actionName,
    arg,
    setting = null,
    ...extra
  ) {
    if (videoConferenceWindows && videoConferenceWindows.webContents) {
      videoConferenceWindows.webContents.send(
        event,
        actionName,
        arg,
        setting,
        extra
      );
    }

    if (sipController.view) {
      sipController.view.webContents.send(
        event,
        actionName,
        arg,
        setting,
        extra
      );
    }

    for (let key in chatWindowsList) {
      const browser = chatWindowsList[key];
      if (browser && browser.webContents) {
        browser.webContents.send(event, actionName, arg, setting, extra);
      }
    }
  }

  // EVENT FROM SOCKET

  app.on('msgcenter_resolve_dns_error', (actionName, arg) => {
    myLogger.log('resolveDNSError, actionName=', actionName, 'args=', arg);
    if (actionName === constantsApp.ACTION_LOGIN_RESPONSE) {
      appPrefs.setAttempDisableAutoLogin(true); // for safe never loop
      sendEventToBrowserWindow(constantsApp.REPLY_LOGIN_EVENT, actionName, arg);
    } else {
      myLogger.log('attemp call logout for clear all setting');
      messengerCenter.logout();
    }
  });

  app.on('msgcenter_authenticated', (actionName, arg) => {
    myLogger.log('Socket authenticated, actionName=', actionName, 'args=', arg);
    if (actionName === constantsApp.ACTION_LOGIN_RESPONSE) {
      appPrefs.loadAllAppSetting((setting, alreadyLogin) => {
        const domain =
          setting && setting.account_info ? setting.account_info.domain : null;
        runAutoUpdate(domain, true);
        sendEventToBrowserWindow(
          constantsApp.REPLY_LOGIN_EVENT,
          actionName,
          arg,
          setting
        );
      });
    } else {
      myLogger.log('relogin ok');
      stopAllRequest();
      sendShowDisconectStatus(true, false);
    }
  });

  app.on('msgcenter_authenticated_failed', (actionName, arg) => {
    myLogger.log(
      'Socket authenticated failed , actionName=',
      actionName,
      'args=',
      arg
    );
    if (actionName === constantsApp.ACTION_LOGIN_RESPONSE) {
      appPrefs.setAttempDisableAutoLogin(true); // for safe never loop
      sendEventToBrowserWindow(constantsApp.REPLY_LOGIN_EVENT, actionName, arg);
    } else {
      myLogger.log('attemp call logout for clear all setting');
      messengerCenter.logout();
    }
  });

  app.on('msgcenter_connection_closed_and_logout', (actionName, arg) => {
    myLogger.log(
      'connectionClosedAndLogout, actionName=',
      actionName,
      'args=',
      arg
    );
    myLogger.log(
      'relogin failed need close all child window and go to login page'
    );
    appPrefs.setAttempDisableAutoLogin(true); // for safe never loop

    closeAllWindows();
    closeVideoConfenceWindows();
    closeAllChatWindows();

    stopAllRequest();

    sendEventToBrowserWindow(
      constantsApp.MAIN_TO_RENDER_EVENT,
      actionName,
      arg
    );
  });

  app.on('msgcenter_connection_closed_on_error', (actionName, arg) => {
    myLogger.log(
      'connectionClosedOnError, actionName=',
      actionName,
      'args=',
      arg
    );
    stopAllRequest();
    sendEventToBrowserWindow(
      constantsApp.MAIN_TO_RENDER_EVENT,
      actionName,
      arg
    );

    sendShowDisconectStatus(false, true);

    if (networkOnline) {
      messengerCenter.relogin(false);
    } else {
      //   myLogger.log('attemp call logout for clear all setting');
      //   messengerCenter.logout();
    }
  });

  app.on('msgcenter_data_changed', (actionName, arg) => {
    myLogger.log(
      'msgcenter_data_changed, actionName=',
      actionName,
      'args=',
      arg
    );

    if (process.platform == 'linux' && tray && tray.isDestroyed()) {
      createTray();
    }

    sendEventToBrowserWindow(
      constantsApp.MAIN_TO_RENDER_EVENT,
      actionName,
      arg
    );
  });

  // EVENT NETWORK
  ipcMain.on('online-status-changed', (event, status) => {
    myLogger.log(status);
    if (status === 'offline') {
      // console.log('prepare stopMessengerCenterService network changed');
      if (networkOnline == true) {
        // console.log('stopMessengerCenterService is process');
        stopAllRequest();
        networkOnline = false;
        messengerCenter.resetNumberRetry();
        messengerCenter.disconnectSocket();
        sendShowDisconectStatus(true, true);
      }
    } else {
      // console.log('prepare resumeMessengerCenterService network changed');
      if (networkOnline == false) {
        // console.log('resumeMessengerCenterService is process');
        networkOnline = true;
        messengerCenter.relogin(true);
      }
    }
  });

  function sendShowDisconectStatus(force, show) {
    sendEventToBrowserWindow(
      constantsApp.MAIN_TO_RENDER_EVENT,
      constantsApp.ACTION_SHOW_DISCONNECT_SOCKET,
      { force: force, show: show }
    );
  }

  function stopMessengerCenterService() {
    // console.log('stopMessengerCenterService');
    stopAllRequest();
    messengerCenter.resetNumberRetry();
    messengerCenter.disconnectSocket();
  }

  function resumeMessengerCenterService(reset) {
    // console.log('resumeMessengerCenterService');
    messengerCenter.relogin(reset);
  }

  function isMainWidowsFocused() {
    return (
      mainWindow &&
      mainWindow.isFocused() &&
      mainWindow.isVisible() &&
      !mainWindow.isMinimized()
    );
  }

  // EVENT FROM RENDDER

  ipcMain.on(
    constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
    (event, action, data) => {
      if (action == constantsApp.ACTION_SYNC_GET_LANGUAGE) {
        event.returnValue = appPrefs.getLanguageDef();
      } else if (action == constantsApp.ACTION_SYNC_SET_LANGUAGE) {
        appPrefs.setLanguage(data, langCode => {
          allTranslation.setLanguage(__dirname, langCode);
          destroyTray();
          createTray();
          setupMenuBar();
          event.returnValue = true;
        });
      } else if (action == constantsApp.ACTION_SYNC_GET_ROOM_BY_ROOM_KEY) {
        event.returnValue = talkDB.findRoomActiveByRoomKey(
          data.roomKey,
          messengerCenter.accountID
        );
      } else if (action == constantsApp.ACTION_SYNC_GET_USER_BY_USER_KEY) {
        event.returnValue = talkDB.findUserByUserKey(data.userKey);
      } else if (
        action ==
        constantsApp.ACTION_SYNC_CHECK_ROOM_AND_WINDOWS_FOCUS_WITH_ROOM_KEY
      ) {
        let result = false;
        if (
          videoConferenceWindows &&
          currentVideoConferenceRoomKey == data.roomKey &&
          videoConferenceWindows.isFocused() &&
          videoConferenceWindows.isVisible() &&
          !videoConferenceWindows.isMinimized()
        ) {
          result = true;
        }
        event.returnValue = result;
      } else if (
        action == constantsApp.ACTION_SYNC_UPDATE_VIDEO_CONFERENCE_DATA
      ) {
        currentVideoConferenceOutURL = data;
        event.returnValue = true;
      } else if (
        action == constantsApp.ACTION_SYNC_GET_UNREAD_MESSAGE_POSITION
      ) {
        const result = messengerCenter.markPositionUnreadMessageCached.getUnreadMessageWithRoomKey(
          data.roomKey
        );
        event.returnValue = result;
      } else if (action == constantsApp.ACTION_SYNC_CHECK_TALK_CACHE) {
        const result = messengerCenter.canUseGetTalkCache(data.roomKey);
        event.returnValue = result;
      } else if (action == constantsApp.ACTION_SYNC_SET_RESET_CAMERA) {
        var options = {
          name: 'HanbiroTalk2',
          icns:
            '/Applications/HanbiroTalk2.app/Contents/Resources/HanbiroTalk2.icns' // (optional)
        };
        sudoComd.exec(
          'killall VDCAssistant;killall AppleCameraAssistant',
          options,
          function(error, stdout, stderr) {
            if (error) {
              console.log('error: ' + error);
            }
            console.log('stdout: ' + stdout);
          }
        );
        event.returnValue = true;
      } else if (action == constantsApp.ACTION_SYNC_GET_OPEN_AT_LOGIN) {
        const result = app.getLoginItemSettings().openAtLogin;
        event.returnValue = result;
      } else if (action == constantsApp.ACTION_SYNC_SET_OPEN_AT_LOGIN) {
        app.setLoginItemSettings({ openAtLogin: data });
        event.returnValue = true;
      } else if (action == constantsApp.ACTION_SYNC_VIDEO_ONE_ONE_IS_LOADED) {
        sipController.loaded = true;
        event.returnValue = true;
      } else if (action == constantsApp.ACTION_SYNC_SET_MAIN_FLASH_FRAME) {
        if (mainWindow) {
          mainWindow.flashFrame(true);
        }
        event.returnValue = true;
      } else if (action == constantsApp.ACTION_SYNC_GET_CAPTURE_LANGUAGE) {
        const help1 = allTranslation.text(
          'Click and drag the mouse to select the area of the screen to be captured'
        );
        const help2 = allTranslation.text('Press ESC key to exit');
        const help3 = allTranslation.text('Clear');
        const help4 = allTranslation.text('Save');
        const help5 = allTranslation.text('Draw');
        const help6 = allTranslation.text('Undo');
        const help7 = allTranslation.text('Back');
        const help8 = allTranslation.text('Size');
        const help9 = allTranslation.text('Color');
        const help10 = allTranslation.text('send_capture');
        event.returnValue = {
          help1,
          help2,
          help3,
          help4,
          help5,
          help6,
          help7,
          help8,
          help9,
          help10
        };
      } else if (action == constantsApp.ACTION_SYNC_RESIZE_WINDOWS_IF_NEED) {
        const extraWidth = data ?? constantsApp.FILE_LIST_PANNEL_WIDTH;
        if (mainWindow) {
          const curDisplay = screen.getDisplayMatching(mainWindow.getBounds());
          const workAreaSize = curDisplay ? curDisplay.workAreaSize : null;
          const workAreaWidth = workAreaSize ? workAreaSize.width : 0;
          const size = mainWindow.getSize();
          const lastWidth = size[0];
          const lastHeight = size[1];
          const newWidth = lastWidth + extraWidth;
          if (newWidth < workAreaWidth) {
            mainWindow.setSize(newWidth, lastHeight, false);
          }
        }
        event.returnValue = true;
      } else if (
        action == constantsApp.ACTION_SYNC_OPEN_REMOTE_CONTROL_WINDOWS
      ) {
        if (videoConferenceWindows) {
          let msg = allTranslation.text(
            'You already entered other video conference room.'
          );
          sendEventToBrowserWindow(
            constantsApp.MAIN_TO_RENDER_EVENT,
            constantsApp.ACTION_SHOW_MSG_INFO_TO_USER,
            msg
          );
          event.returnValue = false;
          return;
        }

        if (sipController.view) {
          let msg = allTranslation.text(
            'You already entered other video conference room.'
          );
          sendEventToBrowserWindow(
            constantsApp.MAIN_TO_RENDER_EVENT,
            constantsApp.ACTION_SHOW_MSG_INFO_TO_USER,
            msg
          );
          event.returnValue = false;
          return;
        }

        if (remoteController.existRemoteControlPage()) {
          remoteController.sendData(
            constantsApp.MAIN_TO_RENDER_EVENT,
            constantsApp.ACTION_REMOTE_UPDATE_ID,
            data
          );
          remoteController.moveToTop();
          event.returnValue = false;
        } else {
          openRemoteControlWindows(data.remoteId);
          event.returnValue = true;
        }
      }
    }
  );

  ipcMain.on(constantsApp.SEND_EVENT_LOAD_APP_SETTING, (event, arg) => {
    appPrefs.loadAllAppSetting((setting, alreadyLogin) => {
      event.reply(
        constantsApp.REPLY_EVENT_LOAD_APP_SETTING,
        setting,
        alreadyLogin,
        allTranslation.LANG_MAP
      );
    });
  });

  ipcMain.on(constantsApp.SEND_LOGIN_EVENT, (event, arg) => {
    appPrefs.loadAllAppSetting((setting, alreadyLogin) => {
      let account_info = setting.account_info;
      if (account_info) {
        let domain = account_info.domain;
        let userId = account_info.user_id;
        let password = account_info.password;

        if (domain != arg.domain) {
          //new domain delete all user and room
          talkDB.deleteAllUserGroupAndRoom();
          appPrefs.eraseSetting();
        } else if (userId != arg.userid) {
          //new user delete all room
          talkDB.deleteAllRoomMessage();
          appPrefs.eraseSetting();
        }
      } else {
        talkDB.deleteAllUserGroupAndRoom();
        appPrefs.eraseSetting();
      }

      appPrefs.setDomainUserIdPasswordAutoLogin(
        arg.domain,
        arg.userid,
        arg.password,
        arg.autologin,
        arg.mobilelogin,
        () => {
          messengerCenter.loginWithDomainUserIdPassword(
            arg.domain,
            arg.userid,
            arg.password,
            arg.encryptUserId,
            arg.encryptPassword,
            arg.otpCode
          );
        }
      );
    });
  });

  ipcMain.on(constantsApp.CANCEL_LOGIN_EVENT, (event, arg) => {
    messengerCenter.disconnectSocket();
    event.reply(
      constantsApp.REPLY_LOGIN_EVENT,
      constantsApp.ACTION_LOGIN_USER_CANCELED
    );
  });

  ipcMain.on(constantsApp.SEND_LOGOUT_EVENT, (event, arg) => {
    const clearAndLogout = arg ? arg.clearAndLogout : false;
    messengerCenter.logout(() => {
      if (clearAndLogout) {
        talkDB.deleteAllRoomMessage();
        appPrefs.eraseSetting();
        appPrefs.eraseAccountSetting();
      } else {
        talkDB.deleteAllTalkInfo();
      }
    });
  });

  ipcMain.on(constantsApp.SEND_SOCKET_API_EVENT, (event, apiID, info) => {
    messengerCenter.sendAPIToSever(apiID, info);
  });

  // DATA FOR RENDER

  ipcMain.on(constantsApp.REQUEST_FULL_QUERY_ORG_TREE, (event, arg) => {
    event.reply(
      constantsApp.REPLY_FULL_QUERY_ORG_TREE,
      talkDB.getFullORGTree()
    );
  });

  ipcMain.on(
    constantsApp.REQUEST_FULL_QUERY_ORG_TREE_FOR_STATE,
    (event, arg) => {
      event.reply(
        constantsApp.REPLY_FULL_QUERY_ORG_TREE_FOR_STATE,
        talkDB.getFullORGTree()
      );
    }
  );

  ipcMain.on(constantsApp.REQUEST_FULL_QUERY_ROOM_LIST, (event, arg) => {
    event.reply(
      constantsApp.REPLY_FULL_QUERY_ROOM_LIST,
      talkDB.getRoomList(messengerCenter.accountID)
    );
  });

  ipcMain.on(
    constantsApp.REQUEST_QUERY_MESSAGE_LIST_ROOM_DETAIL,
    (event, arg) => {
      let messageList = {};
      let roomDetail = {
        room: {},
        details: []
      };
      try {
        roomDetail = talkDB.getFullRoomDetailListByRoomID(arg.roomID);
        if (!arg.queryRoomDetailOnly) {
          messageList = talkDB.getMessageList(
            arg,
            isMainWidowsFocused(),
            constantsApp.GETTALK_COUNT
          );
        }
      } catch (e) {}
      event.reply(
        constantsApp.REPLY_QUERY_MESSAGE_LIST_ROOM_DETAIL,
        messageList,
        roomDetail,
        {
          queryRoomDetailOnly: arg.queryRoomDetailOnly
        }
      );
    }
  );

  ipcMain.on(
    constantsApp.REQUEST_COMMON_UPLOAD_DOWNLOAD_STATUS,
    (event, args) => {
      const action = args.action;
      if (action == constantsApp.ACTION_DOWNLOAD_UPLOAD_MAIN_TO_EXTRA) {
        sendEventToExtraBrowserWindows(
          constantsApp.REQUEST_COMMON_UPLOAD_DOWNLOAD_STATUS,
          action,
          args.obj
        );
      } else if (action == constantsApp.ACTION_DOWNLOAD_UPLOAD_EXTRA_TO_MAIN) {
        sendEventToMainBrowserWindow(
          constantsApp.MAIN_TO_RENDER_EVENT,
          action,
          args.obj
        );
      }
    }
  );

  ipcMain.on(
    constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
    (event, action, data) => {
      if (
        action ==
        constantsApp.ACTION_QUERY_ROOM_WITH_USER_LIST_AND_CREATE_ROOM_IF_NEED
      ) {
        appPrefs.loadAllAppSetting((setting, alreadyLogin) => {
          const account_info = setting.account_info;
          const extra_server_info = setting.extra_server_info;
          const extra_login_info = setting.extra_login_info;
          let loggedName = account_info.user_id;
          if (extra_login_info && extra_login_info.login) {
            loggedName =
              extra_login_info.login.username ?? account_info.user_id;
          }

          messengerCenter.keepRoomCreateQueue = null;

          let maxNumberContactInRoom = extra_server_info.maxContactInRoom;
          const category = data.category;

          const users = data.users;

          if (!users || users.length <= 0) {
            return;
          }

          const room = talkDB.findRoomActiveByListUser(
            users,
            account_info,
            messengerCenter.accountID
          );
          let userKeyList = [];
          users.forEach(user => {
            userKeyList.push(user.userKey);
          });
          if (room) {
            talkDB.updateAllMessageAsReadedWithRoomId(room.rID);
            messengerCenter.sendAPIToSever(
              constantsApp.API_MARK_ALL_MESSAGE_AS_READ,
              {
                roomKey: room.rRoomKey,
                userKey: account_info.user_key
              }
            );

            sendEventToBrowserWindow(
              constantsApp.MAIN_TO_RENDER_EVENT,
              constantsApp.ACTION_OPEN_ROOM_SELECTED,
              {
                category: category,
                primaryKey: room.rRoomKey,
                room: room,
                users: userKeyList,
                forceChangeChatTab: data.changeTab,
                newTab: data.newTab
              }
            );

            return;
          }

          const priviledge = extra_server_info.priviledge;
          let allowRoomCreate = true;
          if (priviledge && priviledge.roomcreate) {
            allowRoomCreate = priviledge.roomcreate > 0;
          }
          if (!allowRoomCreate) {
            let msg = allTranslation.text(
              'You do not have create room permission'
            );
            sendEventToBrowserWindow(
              constantsApp.MAIN_TO_RENDER_EVENT,
              constantsApp.ACTION_SHOW_MSG_INFO_TO_USER,
              msg
            );
            return;
          }

          if (users.length > maxNumberContactInRoom) {
            let msg = allTranslation.text(
              'Unable to create room contains more than $1 contacts'
            );
            msg = msg.replace('$1', maxNumberContactInRoom);
            sendEventToBrowserWindow(
              constantsApp.MAIN_TO_RENDER_EVENT,
              constantsApp.ACTION_SHOW_MSG_INFO_TO_USER,
              msg
            );
            return;
          }

          let roomTagName = appPrefs.generateRandomUUID();

          let userCreateList = [];
          users.forEach(user => {
            userCreateList.push({ key: user.userKey, name: user.displayName });
          });
          if (!users.includes(account_info.user_key)) {
            userCreateList.push({
              key: account_info.user_key,
              name: loggedName
            });
          }

          messengerCenter.keepRoomCreateQueue = {
            category: category,
            primaryKey: roomTagName,
            room: null,
            forceChangeChatTab: data.changeTab,
            users: userKeyList,
            newTab: data.newTab
          };

          messengerCenter.sendAPIToSever(constantsApp.API_ROOM_CREATE, {
            roomTag: roomTagName,
            roomAlias: '', // Auto change room name to private alias => difficult to undertand behavior roomAlias: data.roomName ?? '',
            userList: userCreateList
          });
        });
      } else if (action == constantsApp.ACTION_OPEN_SELECTED_ROOM_KEY_IF_CAN) {
        appPrefs.loadAllAppSetting((setting, alreadyLogin) => {
          if (messengerCenter.isAuthenticated) {
            const account_info = setting.account_info;
            const room = talkDB.findRoomActiveByRoomKey(
              data.roomKey,
              messengerCenter.accountID
            );
            if (room) {
              talkDB.updateAllMessageAsReadedWithRoomId(room.rID);
              messengerCenter.sendAPIToSever(
                constantsApp.API_MARK_ALL_MESSAGE_AS_READ,
                {
                  roomKey: room.rRoomKey,
                  userKey: account_info.user_key
                }
              );

              if (mainWindow) {
                mainWindow.show();
                mainWindow.moveTop();
                mainWindow.focus();
              }

              sendEventToBrowserWindow(
                constantsApp.MAIN_TO_RENDER_EVENT,
                constantsApp.ACTION_REPLY_OPEN_SELECTED_ROOM_KEY_IF_CAN,
                {
                  room: room
                }
              );
            }
          }
        });
      } else if (
        action == constantsApp.ACTION_UPDATE_ALL_MESSAGE_AS_READ_AND_SEND_API
      ) {
        let roomId = data.roomId;
        let roomKey = data.roomKey;
        appPrefs.loadAllAppSetting((setting, alreadyLogin) => {
          const account_info = setting.account_info;
          talkDB.updateAllMessageAsReadedWithRoomId(roomId);
          messengerCenter.sendAPIToSever(
            constantsApp.API_MARK_ALL_MESSAGE_AS_READ,
            {
              roomKey: roomKey,
              userKey: account_info.user_key
            }
          );
          sendEventToBrowserWindow(
            constantsApp.MAIN_TO_RENDER_EVENT,
            constantsApp.ACTION_ROOM_LIST_CHANGED,
            null
          );
        });
      } else if (action == constantsApp.ACTION_SEND_MSG_TO_SERVER) {
        if (messengerCenter.userKey == null || messengerCenter.userKey == '') {
          return;
        }
        const clientID = appPrefs.generateRandomUUID();
        let msgId = talkDB.insertSendingMessage(
          messengerCenter.isAuthenticated,
          data.roomId,
          data.roomKey,
          clientID,
          messengerCenter.userKey,
          data.emojMessage,
          data.message
        );

        sendEventToBrowserWindow(
          constantsApp.MAIN_TO_RENDER_EVENT,
          constantsApp.ACTION_RELOAD_ALL_ROOM_CONTENT,
          {
            roomKey: data.roomKey
          }
        );

        messengerCenter.sendAPIToSever(constantsApp.API_SEND_MESSAGE, {
          roomKey: data.roomKey,
          clientId: clientID,
          userKey: messengerCenter.userKey,
          emoticon: data.emojMessage,
          message: data.message
        });
      } else if (
        action == constantsApp.ACTION_SEND_ALL_READ_FROM_MESSAGE_VIEW
      ) {
        if (messengerCenter.roomListPacketResponse) {
          let roomKey = data.roomKey;
          appPrefs.loadAllAppSetting((setting, alreadyLogin) => {
            const account_info = setting.account_info;
            talkDB.updateAllMessageAsReadedAndSendAPIWithRoomKey(
              roomKey,
              messengerCenter.accountID
            );
            messengerCenter.sendAPIToSever(
              constantsApp.API_MARK_ALL_MESSAGE_AS_READ,
              {
                roomKey: roomKey,
                userKey: account_info.user_key
              }
            );
          });
        }
      } else if (action == constantsApp.ACTION_SEND_UPLOAD_FILES) {
        appPrefs.loadAllAppSetting((setting, alreadyLogin) => {
          const serverInfo = setting.extra_server_info;
          const maxFileSizeUpload = serverInfo
            ? serverInfo.maxFileSize
            : 4294967296;

          let files = [];
          if (data.files.length == 1) {
            const file = data.files[0];
            if (file.size <= 0) {
              return;
            }

            if (file.size > maxFileSizeUpload) {
              let msg = allTranslation.text(
                'Can only send files with a maximum size of $1 MB'
              );
              const mb = maxFileSizeUpload / 1024 / 1024;
              msg = msg.replace('$1', mb);
              sendEventToBrowserWindow(
                constantsApp.MAIN_TO_RENDER_EVENT,
                constantsApp.ACTION_SHOW_MSG_INFO_TO_USER,
                msg
              );
              return;
            }
            files.push(file);
          } else {
            for (let i = 0; i < data.files.length; i++) {
              const file = data.files[i];
              if (file.size <= 0 || file.size > maxFileSizeUpload) {
                continue;
              }
              files.push(file);
            }
          }

          files.forEach(element => {
            messengerCenter.sendAPIToSever(
              constantsApp.API_SEND_FILE_HTTP_FAST,
              {
                roomKey: data.roomKey,
                file: element
              }
            );
          });
        });
      } else if (action == constantsApp.ACTION_UPDATE_FILE_STATUS) {
        talkDB.updateFileStatusWithData(data.roomKey, data.numberKey, data);
      } else if (action == constantsApp.ACTION_RESEND_MESSAGE) {
        if (messengerCenter.isAuthenticated) {
          talkDB.updateMessagePendingToSending(data.msgID);

          messengerCenter.sendAPIToSever(constantsApp.API_SEND_MESSAGE, {
            roomKey: data.msgRoomKey,
            clientId: data.msgClientId,
            userKey: data.msgUserKey,
            emoticon: data.msgEmotion,
            message: data.msgBody
          });

          sendEventToBrowserWindow(
            constantsApp.MAIN_TO_RENDER_EVENT,
            constantsApp.ACTION_RELOAD_ALL_ROOM_CONTENT,
            {
              roomKey: data.msgRoomKey,
              msgOnly: true
            }
          );
        }
      } else if (action == constantsApp.ACTION_REUPLOAD_FILE_MESSAGE) {
        if (messengerCenter.isAuthenticated) {
          console.log(data);
          talkDB.deleteMessage(data.msgID);

          messengerCenter.sendAPIToSever(constantsApp.API_SEND_FILE_HTTP_FAST, {
            roomKey: data.roomKey,
            file: data.file
          });

          sendEventToBrowserWindow(
            constantsApp.MAIN_TO_RENDER_EVENT,
            constantsApp.ACTION_RELOAD_ALL_ROOM_CONTENT,
            {
              roomKey: data.roomKey,
              msgOnly: true
            }
          );
        }
      } else if (action == constantsApp.ACTION_DELETE_MESSAGE) {
        talkDB.deleteMessage(data.msgID);
        sendEventToBrowserWindow(
          constantsApp.MAIN_TO_RENDER_EVENT,
          constantsApp.ACTION_RELOAD_ALL_ROOM_CONTENT,
          {
            roomKey: data.msgRoomKey,
            msgOnly: true
          }
        );
      } else if (action == constantsApp.ACTION_QUERY_ORG_ROOM_WITH_TEXT) {
        const result = talkDB.queryContactAndRoomWithSearchText(
          data,
          messengerCenter.accountID
        );
        sendEventToBrowserWindow(
          constantsApp.MAIN_TO_RENDER_EVENT,
          constantsApp.ACTION_REPLY_ORG_ROOM_WITH_TEXT,
          result
        );
      } else if (action == constantsApp.ACTION_QUERY_ORG_ONLY_WITH_TEXT) {
        const result = talkDB.queryContactAndRoomWithSearchText(
          data,
          messengerCenter.accountID
        );
        sendEventToBrowserWindow(
          constantsApp.MAIN_TO_RENDER_EVENT,
          constantsApp.ACTION_REPLY_QUERY_ORG_ONLY_WITH_TEXT,
          result
        );
      } else if (action == constantsApp.ACTION_OPEN_CAPTURE_DESKTOP_WINDOWS) {
        createCaptureWindow(data.result, data.roomKey);
      } else if (action == constantsApp.ACTION_OPEN_VIDEO_CONFERENCE_WINDOWS) {
        const videoId = data.videoId;
        const roomKey = data.roomKey;
        const audio = data.audio;
        if (videoConferenceWindows) {
          if (videoId == currentVideoConferenceId) {
            videoConferenceWindows.show();
            videoConferenceWindows.moveTop();
            videoConferenceWindows.focus();
          } else {
            let msg = allTranslation.text(
              'You already entered other video conference room.'
            );
            sendEventToBrowserWindow(
              constantsApp.MAIN_TO_RENDER_EVENT,
              constantsApp.ACTION_SHOW_MSG_INFO_TO_USER,
              msg
            );
          }
        } else {
          currentVideoConferenceId = videoId;
          currentVideoConferenceRoomKey = roomKey;
          messengerCenter.videoConferenceRoomKey = roomKey;

          openVideoConferenceWindows(data.url, roomKey, audio);
        }
      } else if (action == constantsApp.ACTION_OPEN_CHAT_WINDOWS) {
        openChatWindows(data);
      } else if (action == constantsApp.ACTION_CREATE_OR_INVITE_USER_TO_ROOM) {
        appPrefs.loadAllAppSetting((setting, alreadyLogin) => {
          const account_info = setting.account_info;
          const extra_server_info = setting.extra_server_info;
          const extra_login_info = setting.extra_login_info;
          let loggedName = account_info.user_id;
          if (extra_login_info && extra_login_info.login) {
            loggedName =
              extra_login_info.login.username ?? account_info.user_id;
          }

          let maxNumberContactInRoom = extra_server_info.maxContactInRoom;
          const category = data.category;
          const users = data.users;
          const allUserInRoom = data.listAllUserInRoom ?? [];
          const roomKey = data.roomKey;
          if (!users || users.length <= 0 || !roomKey || roomKey == '') {
            return;
          }

          const nextUserCountInRoom = allUserInRoom.length + users.length;

          if (nextUserCountInRoom > maxNumberContactInRoom) {
            let msg = allTranslation.text(
              'Unable to create room contains more than $1 contacts'
            );
            msg = msg.replace('$1', maxNumberContactInRoom);
            sendEventToBrowserWindow(
              constantsApp.MAIN_TO_RENDER_EVENT,
              constantsApp.ACTION_SHOW_MSG_INFO_TO_USER,
              msg
            );
            return;
          }

          if (allUserInRoom.length > 2) {
            let userList = [];
            users.forEach(element => {
              userList.push({
                key: element.userKey,
                name: element.displayName
              });
            });

            messengerCenter.sendAPIToSever(constantsApp.API_ROOM_INVITE, {
              roomKey: roomKey,
              userList: userList
            });
            return;
          }

          messengerCenter.keepRoomCreateQueue = null;
          const nextUsers = [...allUserInRoom, ...users];

          const room = talkDB.findRoomActiveByListUser(
            nextUsers,
            account_info,
            messengerCenter.accountID
          );

          let userKeyList = [];
          nextUsers.forEach(user => {
            userKeyList.push(user.userKey);
          });
          if (room) {
            talkDB.updateAllMessageAsReadedWithRoomId(room.rID);
            messengerCenter.sendAPIToSever(
              constantsApp.API_MARK_ALL_MESSAGE_AS_READ,
              {
                roomKey: room.rRoomKey,
                userKey: account_info.user_key
              }
            );
            sendEventToBrowserWindow(
              constantsApp.MAIN_TO_RENDER_EVENT,
              constantsApp.ACTION_OPEN_ROOM_SELECTED,
              {
                category: category,
                primaryKey: room.rRoomKey,
                room: room,
                users: userKeyList,
                forceChangeChatTab: true
              }
            );
            return;
          }

          const priviledge = extra_server_info.priviledge;
          let allowRoomCreate = true;
          if (priviledge && priviledge.roomcreate) {
            allowRoomCreate = priviledge.roomcreate > 0;
          }
          if (!allowRoomCreate) {
            let msg = allTranslation.text(
              'You do not have create room permission'
            );
            sendEventToBrowserWindow(
              constantsApp.MAIN_TO_RENDER_EVENT,
              constantsApp.ACTION_SHOW_MSG_INFO_TO_USER,
              msg
            );
            return;
          }

          let roomTagName = appPrefs.generateRandomUUID();

          let userCreateList = [];
          nextUsers.forEach(user => {
            userCreateList.push({ key: user.userKey, name: user.displayName });
          });
          if (!nextUsers.includes(account_info.user_key)) {
            userCreateList.push({
              key: account_info.user_key,
              name: loggedName
            });
          }
          messengerCenter.keepRoomCreateQueue = {
            category: category,
            primaryKey: roomTagName,
            forceChangeChatTab: true,
            room: null,
            users: userKeyList
          };

          messengerCenter.sendAPIToSever(constantsApp.API_ROOM_CREATE, {
            roomTag: roomTagName,
            roomAlias: '',
            userList: userCreateList
          });
        });
      } else if (
        action == constantsApp.ACTION_REQUEST_QUIT_AND_INSTALL_NEW_APP
      ) {
        app.removeAllListeners('window-all-closed');
        const browserWindows = BrowserWindow.getAllWindows();
        browserWindows.forEach(function(browserWindow) {
          browserWindow.removeAllListeners('close');
        });
        autoUpdater.quitAndInstall(true, true);
      } else if (
        action ==
        constantsApp.ACTION_QUERY_ROOM_OR_CREATE_ROOM_IF_NEED_AND_SHARE_MESSAGE
      ) {
        appPrefs.loadAllAppSetting((setting, alreadyLogin) => {
          const account_info = setting.account_info;
          const extra_server_info = setting.extra_server_info;
          const extra_login_info = setting.extra_login_info;
          let loggedName = account_info.user_id;
          if (extra_login_info && extra_login_info.login) {
            loggedName =
              extra_login_info.login.username ?? account_info.user_id;
          }

          if (data.targetData.length <= 0) {
            return;
          }

          const selectedData = data.targetData[0];
          const shareMsgObj = data.shareMsg;
          const shareMsg = shareMsgObj.data;

          let targetData = null;
          if (selectedData.rRoomKey) {
            targetData = selectedData;
          } else {
            targetData = talkDB.findRoomActiveByListUser(
              [selectedData],
              account_info,
              messengerCenter.accountID
            );
          }

          if (targetData) {
            // send message only
            if (shareMsgObj.filePath && shareMsgObj.filePath != '') {
              messengerCenter.sendAPIToSever(
                constantsApp.API_SEND_FILE_HTTP_FAST,
                {
                  roomKey: targetData.rRoomKey,
                  file: {
                    name: shareMsg.msgdtFileName,
                    path: shareMsgObj.filePath,
                    size: shareMsg.msgdtFileSize
                  }
                }
              );
            } else {
              const clientID = appPrefs.generateRandomUUID();
              let msgId = talkDB.insertSendingMessage(
                messengerCenter.isAuthenticated,
                targetData.rID,
                targetData.rRoomKey,
                clientID,
                messengerCenter.userKey,
                shareMsg.msgEmotion,
                shareMsg.msgBody
              );

              sendEventToBrowserWindow(
                constantsApp.MAIN_TO_RENDER_EVENT,
                constantsApp.ACTION_RELOAD_ALL_ROOM_CONTENT,
                {
                  roomKey: targetData.rRoomKey
                }
              );

              messengerCenter.sendAPIToSever(constantsApp.API_SEND_MESSAGE, {
                roomKey: targetData.rRoomKey,
                clientId: clientID,
                userKey: messengerCenter.userKey,
                emoticon: shareMsg.msgEmotion,
                message: shareMsg.msgBody
              });
            }
            return;
          }

          const priviledge = extra_server_info.priviledge;
          let allowRoomCreate = true;
          if (priviledge && priviledge.roomcreate) {
            allowRoomCreate = priviledge.roomcreate > 0;
          }
          if (!allowRoomCreate) {
            return;
          }

          let roomTagName = appPrefs.generateRandomUUID();

          let userCreateList = [
            { key: selectedData.userKey, name: selectedData.displayName }
          ];

          if (selectedData.userKey != account_info.user_key) {
            userCreateList.push({
              key: account_info.user_key,
              name: loggedName
            });
          }

          messengerCenter.keepForwardMessageList[roomTagName] = shareMsgObj;

          messengerCenter.sendAPIToSever(constantsApp.API_ROOM_CREATE, {
            roomTag: roomTagName,
            roomAlias: '', // Auto change room name to private alias => difficult to undertand behavior roomAlias: data.roomName ?? '',
            userList: userCreateList
          });
        });
      } else if (
        action == constantsApp.ACTION_REQUEST_QUERY_FILE_LIST_WITH_FILTER
      ) {
        sendEventToBrowserWindow(
          constantsApp.MAIN_TO_RENDER_EVENT,
          constantsApp.ACTION_REPLY_QUERY_FILE_LIST_WITH_FILTER,
          talkDB.getFileListWithFilterType(
            data.roomID,
            data.roomKey,
            data.filterType,
            data.firstTimeStamp,
            constantsApp.GETFILES_COUNT
          )
        );
      } else if (action == constantsApp.ACTION_CLOSE_VIDEO_CONFERENCE_WINDOWS) {
        ignoreSendOutRoom = data.ignoreSendOutRoom;
        if (videoConferenceWindows) {
          forceClosedVideoConferenceWindows = true;
          videoConferenceWindows.close();
        }
      } else if (action == constantsApp.ACTION_SEND_SIP_HTTP_REQUEST) {
        messengerCenter.sendSipAPI(data);
      } else if (action == constantsApp.ACTION_OPEN_CALL_ONE_ONE_WINDOWS) {
        if (videoConferenceWindows) {
          let msg = allTranslation.text(
            'You already entered other video conference room.'
          );
          sendEventToBrowserWindow(
            constantsApp.MAIN_TO_RENDER_EVENT,
            constantsApp.ACTION_SHOW_MSG_INFO_TO_USER,
            msg
          );
          return;
        }

        const userKey = data.userKey;
        const audio = data.audio;
        const displayName = data.displayName;
        const direction = data.direction;
        const sipKey = data.sipKey;
        const roomKey = data.roomKey;
        if (sipController.view) {
          if (sipKey == sipController.id) {
            sipController.view.show();
            sipController.view.moveTop();
            sipController.view.focus();
          }
        } else {
          sipController.id = sipKey;
          sipController.userKey = userKey;

          openCallOneOneWindows(roomKey, sipKey, displayName, direction, audio);
        }
      } else if (action == constantsApp.ACTION_ROBOT_CONTROL) {
        // if (data.type == 'tap') {
        //   if (data.modifiers.length > 0) {
        //     robot.keyTap(data.vkey, data.modifiers);
        //   } else {
        //     robot.keyTap(data.vkey);
        //   }
        // } else if (data.type == 'toggle') {
        //   robot.keyToggle(data.vkey, data.down ? 'down' : 'up');
        // } else if (data.type == 'moveMouse') {
        //   robot.moveMouse(data.x, data.y);
        // }else if (data.type == 'mouseToggle') {
        //   robot.mouseToggle(data.action1, data.action2);
        // }
      }
    }
  );

  function stopAllRequest() {
    sendEventToBrowserWindow(
      constantsApp.MAIN_TO_RENDER_EVENT,
      constantsApp.ACTION_STOP_ALL_REQUEST,
      {}
    );
    talkDB.updateAllFileUploadToFailed();
  }
}
