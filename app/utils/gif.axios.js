const GifAxios_intervalMs = 100;
const GifAxios_maxRequest = 1;
let GifAxios_penddingDownload = 0;
import axios from 'axios';
import fs from 'fs';
import path from 'path';
export default class GifAxios {
  constructor() {
    this.api = axios.create({});
    /**
     * Axios Request Interceptor
     */
    this.api.interceptors.request.use(function(config) {
      return new Promise((resolve, reject) => {
        let interval = setInterval(() => {
          if ((GifAxios_penddingDownload < this, GifAxios_maxRequest)) {
            GifAxios_penddingDownload++;
            clearInterval(interval);
            resolve(config);
          }
        }, GifAxios_intervalMs);
      });
    });
    /**
     * Axios Response Interceptor
     */
    this.api.interceptors.response.use(
      function(response) {
        GifAxios_penddingDownload = Math.max(0, GifAxios_penddingDownload - 1);
        return Promise.resolve(response);
      },
      function(error) {
        GifAxios_penddingDownload = Math.max(0, GifAxios_penddingDownload - 1);
        return Promise.reject(error);
      }
    );
  }

  fetch(url, rootPath) {
    return new Promise((resolse, reject) => {
      let fileName = url.replace(/[^a-z0-9]/gi, '');
      console.log(fileName);
      const urlEncode = fileName + '.gif';
      const filePath = path.join(rootPath, urlEncode);
      if (fs.existsSync(filePath)) {
        const stats = fs.statSync(filePath);
        let item = { name: urlEncode, path: filePath, size: stats.size };
        resolse(item);
        return;
      }
      this.api
        .get(url, {
          responseType: 'arraybuffer'
        })
        .then(response => {
          if (response.status == 200) {
            const buffer = Buffer.from(response.data);
            fs.writeFileSync(filePath, buffer);
            let item = { name: urlEncode, path: filePath, size: buffer.length };
            resolse(item);
          } else {
            reject(null);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
  }
}
