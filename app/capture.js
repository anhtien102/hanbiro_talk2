import React, { Component, Fragment } from 'react';
import { remote, ipcRenderer } from 'electron';
import ReactCrop from './ReactCrop';
import SaveIcon from '@material-ui/icons/Save';
import RefreshIcon from '@material-ui/icons/Refresh';
import ReplayIcon from '@material-ui/icons/Replay';
import GestureIcon from '@material-ui/icons/Gesture';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import CanvasDraw from 'react-canvas-draw';
import CheckIcon from '@material-ui/icons/Check';
import {
  REQUEST_COMMON_ACTION_FROM_RENDER,
  ACTION_SEND_UPLOAD_FILES,
  COMMON_SYNC_ACTION_FROM_RENDER,
  ACTION_SYNC_GET_CAPTURE_LANGUAGE
} from './configs/constant';
import fs from 'fs';
import path from 'path';
import {
  ButtonGroup,
  IconButton,
  Button,
  Tooltip,
  Typography,
  Slider
} from '@material-ui/core';
import { withTheme } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import { ThemeSupport } from './configs';
import { ensureFilePath } from './configs/file_cheat';

const styles = theme => ({
  main: {
    display: 'flex',
    width: '100%',
    height: '100vh',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    backgroundColor: '#303030'
  },
  drawContent: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative'
  },
  line: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8
  },
  drawMenuContent: {
    position: 'absolute',
    bottom: -120,
    flexDirection: 'column',
    display: 'flex',
    alignItems: 'center'
  },
  iconColor: {
    width: 24,
    height: 24,
    borderRadius: '50%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

const withStyle = withStyles(styles);

function generateFilePath(name) {
  let tmpPath = remote.app.getPath('appData');
  tmpPath = path.join(tmpPath, 'HanbiroTalk');
  tmpPath = path.join(tmpPath, 'temp');
  if (!fs.existsSync(tmpPath)) {
    fs.mkdirSync(tmpPath);
  }
  return path.join(tmpPath, name);
}

function ValueLabelComponent(props) {
  const { children, open, value } = props;
  return (
    <Tooltip open={open} enterTouchDelay={0} placement="top" title={value}>
      {children}
    </Tooltip>
  );
}

class CapturePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      drawMode: false,
      penColor: ThemeSupport[0].primary.main,
      lineWeight: 1,
      helpsString: {},
      fileName: 'capture' + Date.now() + '.png',
      drawName: 'draw' + Date.now() + '.png',
      imagePath: null,
      drawPath: null,
      roomKey: null,
      src: null,
      crop: {
        x: 0,
        y: 0,
        unit: 'px',
        width: 0,
        height: 0
      },
      statusDrag: 'end'
    };
    this.saveableCanvas;
    this.keyDownEvent = this.keyDownEvent.bind(this);
    this.onCropChange = this.onCropChange.bind(this);
    this.onDone = this.onDone.bind(this);
    this.onDraw = this.onDraw.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
    this.renderSubmenu = this.renderSubmenu.bind(this);
  }

  keyDownEvent(event) {
    switch (event.key) {
      case 'Escape':
        remote.getCurrentWindow().close();
        break;
    }
  }

  componentDidMount() {
    const helpsString = ipcRenderer.sendSync(
      COMMON_SYNC_ACTION_FROM_RENDER,
      ACTION_SYNC_GET_CAPTURE_LANGUAGE,
      null
    );

    document.addEventListener('keydown', this.keyDownEvent);
    let paramQuery = global.location.search;

    paramQuery = paramQuery.split('?')[1];

    let params = paramQuery.split('&');
    console.log(paramQuery, params);
    let imagePath = params[0].split('=')[1];
    if (imagePath) {
      imagePath = decodeURIComponent(imagePath);
    }

    let roomKey = params[1].split('=')[1];
    this.setState({
      imagePath: imagePath,
      roomKey: roomKey,
      helpsString: helpsString
    });
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.keyDownEvent);
  }

  sendFiles(fileData) {
    const { roomKey } = this.state;
    if (roomKey) {
      let files = [fileData];
      ipcRenderer.send(
        REQUEST_COMMON_ACTION_FROM_RENDER,
        ACTION_SEND_UPLOAD_FILES,
        {
          roomKey: roomKey,
          files: files
        }
      );
    }
  }

  sendFileUrl(fileUrl, fileName) {
    if (fileUrl && fileUrl != '') {
      const stats = fs.statSync(fileUrl);
      if (stats.size > 0) {
        this.sendFiles({ name: fileName, path: fileUrl, size: stats.size });
      }
    }
  }

  onRefresh() {
    this.setState({
      crop: {
        x: 0,
        y: 0,
        unit: 'px',
        width: 0,
        height: 0
        // aspect: 16 / 9
      }
    });
  }

  onDone() {
    const { crop, imagePath, fileName } = this.state;
    if (!crop || crop.width <= 0) {
      this.sendFileUrl(imagePath, fileName);
      remote.getCurrentWindow().close();
    } else {
      this.makeClientCrop(crop, fileName).then(fileUrl => {
        this.sendFileUrl(fileUrl, fileName);
        remote.getCurrentWindow().close();
      });
    }
  }

  onDraw() {
    const { crop, drawName } = this.state;
    this.makeClientCrop(crop, drawName).then(fileUrl => {
      this.setState({
        drawMode: true,
        drawPath: fileUrl
      });
    });
  }

  // If you setState the crop in here you should return false.
  onImageLoaded = image => {
    this.imageRef = image;
    return false; // Return false if you set crop state in here.
  };

  onCropComplete = crop => {};

  onCropChange = (crop, percentCrop) => {
    // You could also use percentCrop:
    // this.setState({ crop: percentCrop });
    this.setState({ crop });
  };

  async makeClientCrop(crop, fileName) {
    if (this.imageRef && crop.width && crop.height) {
      return await this.getCroppedImg(this.imageRef, crop, fileName);
    }
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );

    return new Promise((resolve, reject) => {
      const dataURL = canvas.toDataURL('image/png');
      const src = dataURL.replace(/^data:image\/(png|jpg);base64,/, '');

      const filePath = generateFilePath(fileName);
      fs.writeFile(filePath, src, 'base64', err => {
        if (err) {
          resolve(null);
          return;
        }
        resolve(filePath);
      });
    });
  }

  drawDone = () => {
    const { drawName } = this.state;
    const canvas = this.saveableCanvas.canvas['grid'];
    const canvasDrawing = this.saveableCanvas.canvas['drawing'];
    const context = this.saveableCanvas.ctx['grid'];
    context.drawImage(canvasDrawing, 0, 0);
    const dataURL = canvas.toDataURL('image/png');
    const src = dataURL.replace(/^data:image\/(png|jpg);base64,/, '');

    const filePath = generateFilePath(drawName);
    fs.writeFile(filePath, src, 'base64', err => {
      if (err) {
        return;
      }
      this.sendFileUrl(filePath, drawName);
      remote.getCurrentWindow().close();
    });
  };

  onChangeLineWeight = (event, value) => {
    this.setState({ lineWeight: value });
  };

  renderSubmenu() {
    const { helpsString } = this.state;
    return this.state.statusDrag == 'end' ? (
      <div style={{ position: 'absolute', bottom: 0, right: 0 }}>
        <ButtonGroup
          size="small"
          variant="contained"
          color="primary"
          aria-label="contained primary button group"
        >
          <Button
            onClick={this.onRefresh}
            variant="contained"
            color="default"
            size="small"
            startIcon={<RefreshIcon />}
          >
            {helpsString.help3}
          </Button>
          <Button
            onClick={this.onDraw}
            variant="contained"
            color="default"
            size="small"
            startIcon={<GestureIcon />}
          >
            {helpsString.help5}
          </Button>
          <Button
            onClick={this.onDone}
            variant="contained"
            color="default"
            size="small"
            startIcon={<SaveIcon />}
          >
            {helpsString.help10}
          </Button>
        </ButtonGroup>
      </div>
    ) : null;
  }

  renderDrawMenu = () => {
    const { lineWeight, penColor, helpsString } = this.state;
    const { classes } = this.props;
    return (
      <div className={classes.drawMenuContent}>
        <ButtonGroup
          size="small"
          variant="contained"
          color="primary"
          aria-label="contained primary button group"
        >
          <Button
            onClick={() => {
              this.setState({ drawMode: false });
            }}
            variant="contained"
            color="default"
            size="small"
            startIcon={<ArrowBackIcon />}
          >
            {helpsString.help7}
          </Button>
          <Button
            onClick={() => {
              this.saveableCanvas?.undo?.();
            }}
            variant="contained"
            color="default"
            size="small"
            startIcon={<ReplayIcon />}
          >
            {helpsString.help6}
          </Button>
          <Button
            onClick={this.drawDone}
            variant="contained"
            color="default"
            size="small"
            startIcon={<SaveIcon />}
          >
            {helpsString.help4}
          </Button>
        </ButtonGroup>
        <div className={classes.line} style={{ width: 250 }}>
          <Typography
            variant="body2"
            style={{ paddingRight: 16, color: 'white' }}
            className={classes.text}
          >
            {helpsString.help8}
          </Typography>
          <Slider
            value={lineWeight}
            min={1}
            max={5}
            step={1}
            marks={true}
            ValueLabelComponent={ValueLabelComponent}
            onChange={this.onChangeLineWeight}
            aria-labelledby="continuous-slider"
            style={{ display: 'flex', flex: 1 }}
          />
        </div>
        <div className={classes.line} style={{ width: 250 }}>
          <Typography
            variant="body2"
            style={{ paddingRight: 8, color: 'white' }}
            className={classes.text}
          >
            {helpsString.help9}
          </Typography>
          <div style={{ display: 'flex', flex: 1 }}>
            {ThemeSupport.map(item => {
              return (
                <IconButton
                  key={item.themeId}
                  size="small"
                  onClick={() => this.setState({ penColor: item.primary.main })}
                >
                  <div
                    className={classes.iconColor}
                    style={{
                      backgroundColor: item.primary.main
                    }}
                  >
                    {item.primary.main == penColor && (
                      <CheckIcon fontSize="small" style={{ color: 'white' }} />
                    )}
                  </div>
                </IconButton>
              );
            })}
          </div>
        </div>
      </div>
    );
  };

  renderContent = () => {
    const {
      imagePath,
      drawPath,
      helpsString,
      crop,
      drawMode,
      penColor,
      lineWeight
    } = this.state;

    if (drawMode) {
      return (
        <div className={this.props.classes.drawContent}>
          <CanvasDraw
            ref={canvasDraw => (this.saveableCanvas = canvasDraw)}
            lazyRadius={1}
            brushRadius={lineWeight}
            brushColor={penColor}
            imgSrc={ensureFilePath(drawPath)}
            canvasWidth={crop.width}
            canvasHeight={crop.height}
            style={{ padding: 1 }}
          />
          {this.renderDrawMenu()}
        </div>
      );
    } else if (imagePath) {
      return (
        <ReactCrop
          style={{ overflow: 'hidden' }}
          src={ensureFilePath(imagePath)}
          crop={crop}
          ruleOfThirds
          onImageLoaded={this.onImageLoaded}
          onComplete={this.onCropComplete}
          onChange={this.onCropChange}
          onDragStart={() => {
            this.setState({ statusDrag: 'start' });
          }}
          onDragEnd={() => {
            this.setState({ statusDrag: 'end' });
          }}
          componentInit={
            <div
              style={{
                position: 'absolute',
                boxShadow: '0 0 0 9999em rgba(0, 0, 0, 0.5)',
                left: 'calc(50% - 400px)',
                top: 10,
                textAlign: 'center',
                borderRadius: 20
              }}
            >
              <div
                style={{
                  margin: 'auto',
                  width: 800,
                  backgroundColor: 'rgba(0, 0, 0, 0.8)',
                  color: 'white',
                  fontSize: 20,
                  padding: 20,
                  borderRadius: 20,
                  border: '1px solid #ccc!important'
                }}
              >
                {helpsString.help1} <pre />
                {helpsString.help2}
              </div>
            </div>
          }
          subMenu={this.renderSubmenu()}
        />
      );
    }
    return <div style={{ backgroundColor: 'white' }}></div>;
  };

  render() {
    return (
      <div className={this.props.classes.main}>{this.renderContent()}</div>
    );
  }
}

export default withStyle(withTheme(CapturePage));
