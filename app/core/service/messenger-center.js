import parser from 'xml-js';
import TCPConnection from '../socket/tcp-connection';
import myLogger from '../logger/debug';
import * as MesengerPacket from './packet/messenger-packet';
import * as TalkConstants from './talk-constants';
import TextUtils from '../utils/text.util';
import cj4dxUtils from '../utils/cj4dx.utils';
import Utils from '../utils/utils';
import talkPath from '../utils/folder-manage/talk-folder';
import * as talkDB from './database/db';
import appPrefs from '../utils/pref';
import { app } from 'electron';
import * as constantsApp from '../../configs/constant';
import { getAudioDurationInSeconds } from 'get-audio-duration';
import fs from 'fs';
import axios from 'axios';
import https from 'https';
import log from 'electron-log';

import {
  SIP_RESULT_STATUS,
  buildMyNumber
} from '../../janus_client/src/app/singlecall/sipstatus';

import GetTalkCached from './gettalk.cache';
import MarkPositionUnreadMessageCached from './message.unread.cache';
import ApiSupportList, {
  CUSTOM_SUPPORT_CHAT_BOARD,
  CUSTOM_SUPPORT_CLOUDDISK,
  CUSTOM_SUPPORT_WHISPER
} from './api.support';
import {
  base64FromImagePath,
  resizeImageWithPathAndSave
} from '../utils/image.process';
import allTranslation from '../../language/electron.lang';

import sipController from './sipcontroller';

import macaddress from 'macaddress';

const TAG = 'MessengerCenter';

const DISABLE_THUMBNAIL = true;

const MAX_NUMBER_RETRY = 20;
const SAFE_MAX_NUMBER_RETRY = 100;

const USE_CACHE_GETTALK = true;

var safeRetryLiveTimeout = 0;

export const CALL_STATE_CALL = {
  init: 0,
  makingCall: 1,
  prepareShow: 2,
  showCall: 3
};

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function phpToMoment(str) {
  let replacements = {
    d: 'DD',
    D: 'ddd',
    j: 'D',
    l: 'dddd',
    N: 'E',
    S: 'o',
    w: 'e',
    z: 'DDD',
    W: 'W',
    F: 'MMMM',
    m: 'MM',
    M: 'MMM',
    n: 'M',
    t: '', // no equivalent
    L: '', // no equivalent
    o: 'YYYY',
    Y: 'YYYY',
    y: 'YY',
    a: 'a',
    A: 'A',
    B: '', // no equivalent
    g: 'h',
    G: 'H',
    h: 'hh',
    H: 'HH',
    i: 'mm',
    s: 'ss',
    u: 'SSS',
    e: 'zz', // deprecated since Moment.js 1.6.0
    I: '', // no equivalent
    O: '', // no equivalent
    P: '', // no equivalent
    T: '', // no equivalent
    Z: '', // no equivalent
    c: '', // no equivalent
    r: '', // no equivalent
    U: 'X'
  };

  if (str) {
    return str
      .split('')
      .map(chr => (chr in replacements ? replacements[chr] : chr))
      .join('');
  }

  return null;
}

export class MessengerCenter {
  constructor() {
    this.initServerInfo();
    this.isConnecting = false;
    this.isAuthenticated = false;
    this.tcpConnection = null;
    this.encryptUserId = '';
    this.encryptPassword = '';
    this.userKey = '';
    this.accountID = -1;
    this.liveTimer = null;
    this.timeCardTimer = null;
    this.cancelTokenDNSSource = null;
    this.cancelTokenLoginGroupwareSource = null;
    this.cancelTokenHTTPOrgSource = null;

    this.useLoginForRelogin = false;

    this.delayReloadAllRoomListAndGettalkAPI = null;

    this.apiSupportList = new ApiSupportList();

    this.typeLogin = true;

    this.numberOfRetry = 0;

    this.lastReloadFullContentNotify = 0;

    this.lastMsgNotify = 0;
    this.lastMsgStatusNotify = 0;

    this.curUserListFavoriteUpdate = 0;
    this.curUserListUpdate = 0;

    this.roomListPacketResponse = false;

    this.timeoutRelogin = null;

    this.safeRetryRelogin = 0;

    this.timeoutPingList = [];

    this.delayRequestUserList = null;

    this.getTalkCached = new GetTalkCached();
    this.markPositionUnreadMessageCached = new MarkPositionUnreadMessageCached();

    this.keepForwardMessageList = {};

    this.clouddiskSupportInOTP = -1;
    this.whisperSupportInOTP = -1;

    this.videoConferenceRoomKey = null;

    this.axiosIgnoreSSL = axios.create({
      httpsAgent: new https.Agent({
        rejectUnauthorized: false
      })
    });

    // 1  make sip, 2 started call
    this.makingCallState = {
      callStatus: CALL_STATE_CALL.init,
      timer: null,
      sipKey: null
    };
  }

  addPingTimeout() {
    const pingDelay = (this.serverInfo.liveConfigInfo.liveTimeout ?? 10) * 1000;
    const maxRetry = this.serverInfo.liveConfigInfo.retry ?? 3;
    const timer = setTimeout(() => {
      if (safeRetryLiveTimeout >= maxRetry) {
        safeRetryLiveTimeout = 0;
        this.logoutWithStatus({ code: 3, msg: 'Server restart' });
        return;
      }
      safeRetryLiveTimeout++;
      this.disconnectSocket();
      this._sendConnectionCloseOnErrorToApp(
        `Ping timeout in ${pingDelay} milliseconds`
      );
    }, pingDelay);
    this.timeoutPingList.push(timer);
    // console.log('addPingTimeout', this.timeoutPingList);
  }

  removeEarlyPingTimeout() {
    // console.log('removeEarlyPingTimeout before', this.timeoutPingList);
    const timer = this.timeoutPingList.shift();
    clearTimeout(timer);
    // console.log('removeEarlyPingTimeout after', this.timeoutPingList);
  }

  stopAllPingTimeoutList() {
    this.timeoutPingList.forEach(timer => {
      clearTimeout(timer);
    });
    this.timeoutPingList = [];
    // console.log('stopAllPingTimeoutList', this.timeoutPingList);
  }

  getCurrentRoomKeyActive() {
    return global.ShareKeepValue.CURRENT_ROOM_KEY;
  }

  initServerInfo() {
    this.serverInfo = {
      address: '',
      title: '',
      port: 0,
      version: 1,
      zip: 0,
      ssl: 0,
      sslPort: 0,
      maxContactInRoom: 0,
      maxFileSize: 0,
      canDownloadFileValue: 1,
      transferFileByWeb: 0,
      supportTrackGps: 0,
      clouddiskSupport: 1,
      groupCallSupport: 0,
      chatBoardSupport: 0,
      whisperSupport: 1,
      otpLogin: 0,
      useHTTPOrgAPI: false,
      voiceIpServer: {
        sipIp: '',
        sipPort: '',
        sipProtocol: '',
        isActiveSip: 0,
        sipSupportVideo: false
      },
      videoConference: 0,
      videoConferenceInfo: {
        method: 'wss',
        server: '',
        server2: '',
        rtcserver: '',
        httpsport: 17005,
        wssport: 8989,
        api: 0,
        version: 1,
        audioroom: 0,
        callOneOne: 0
      },
      liveConfigInfo: {
        liveTimeout: 10,
        liveTime: 30,
        retry: 3
      },
      translate: {
        useTranslate: false
      },
      saveLogger: false,
      allowFileTransfer: true
    };
  }

  _sendAuthenticatedToApp(isLogin, errorCode) {
    this.isConnecting = false;
    this.isAuthenticated = true;
    app.emit(
      'msgcenter_authenticated',
      isLogin
        ? constantsApp.ACTION_LOGIN_RESPONSE
        : constantsApp.ACTION_RELOGIN_RESPONSE,
      {
        code: errorCode
      }
    );
  }

  _sendAuthenticationFailedToApp(isLogin, errorCode) {
    this.isConnecting = false;
    this.isAuthenticated = false;
    app.emit(
      'msgcenter_authenticated_failed',
      isLogin
        ? constantsApp.ACTION_LOGIN_RESPONSE
        : constantsApp.ACTION_RELOGIN_RESPONSE,
      {
        code: errorCode
      }
    );
  }

  _sendDataChangedToApp(actionName, arg) {
    app.emit('msgcenter_data_changed', actionName, arg);
  }

  _sendResolveDNSErrorToApp(isLogin, errorCode, errorMessage = '') {
    this.isConnecting = false;
    this.isAuthenticated = false;
    app.emit(
      'msgcenter_resolve_dns_error',
      isLogin
        ? constantsApp.ACTION_LOGIN_RESPONSE
        : constantsApp.ACTION_RELOGIN_RESPONSE,
      {
        code: errorCode,
        errorMessage: errorMessage
      }
    );
  }

  _sendConnectionCloseAndLogoutToApp(arg) {
    this.isConnecting = false;
    this.isAuthenticated = false;
    app.emit(
      'msgcenter_connection_closed_and_logout',
      constantsApp.ACTION_MOVE_TO_LOGIN_EVENT,
      arg
    );
  }

  _sendConnectionCloseOnErrorToApp(arg) {
    this.isConnecting = false;
    this.isAuthenticated = false;
    app.emit(
      'msgcenter_connection_closed_on_error',
      constantsApp.ACTION_SOCKET_ERROR,
      arg
    );
  }

  _sendORGChangedNotify() {
    if (this.changeOrgDelay) {
      clearTimeout(this.changeOrgDelay);
    }
    this.changeOrgDelay = setTimeout(() => {
      this._sendDataChangedToApp(constantsApp.ACTION_ORG_CHANGED, null);
      this.changeOrgDelay = null;
    }, 1000);
  }

  _sendORGStatusChangedNotify() {
    if (this.changeOrgStatusDelay) {
      clearTimeout(this.changeOrgStatusDelay);
    }
    this.changeOrgStatusDelay = setTimeout(() => {
      this._sendDataChangedToApp(constantsApp.ACTION_ORG_STATUS_CHANGED, null);
      this.changeOrgStatusDelay = null;
    }, 1000);
  }

  _sendExtraLoginChanged(data) {
    this._sendDataChangedToApp(constantsApp.ACTION_EXTRA_LOGIN_CHANGED, data);
  }

  _sendRoomChangedNotify(data) {
    this._sendDataChangedToApp(constantsApp.ACTION_ROOM_LIST_CHANGED, data);
  }

  _sendGetTalkChangedNotify(data) {
    if (this.changeMsgDelay) {
      clearTimeout(this.changeMsgDelay);
    }
    let ts = Date.now();
    if (ts > this.lastMsgNotify + 200) {
      this._sendDataChangedToApp(constantsApp.ACTION_MSG_GETTALK_CHANGED, data);
    } else {
      this.changeMsgDelay = setTimeout(() => {
        this._sendDataChangedToApp(
          constantsApp.ACTION_MSG_GETTALK_CHANGED,
          data
        );
        this.changeMsgDelay = null;
      }, 100);
    }
    this.lastMsgNotify = ts;
  }

  _sendReloadFullContentChangedNotify(data) {
    if (this.reloadFullContentDelay) {
      clearTimeout(this.reloadFullContentDelay);
    }
    let ts = Date.now();
    if (ts > this.lastReloadFullContentNotify + 200) {
      this._sendDataChangedToApp(
        constantsApp.ACTION_RELOAD_ALL_ROOM_CONTENT,
        data
      );
    } else {
      this.reloadFullContentDelay = setTimeout(() => {
        this._sendDataChangedToApp(
          constantsApp.ACTION_RELOAD_ALL_ROOM_CONTENT,
          data
        );
        this.reloadFullContentDelay = null;
      }, 100);
    }
    this.lastReloadFullContentNotify = ts;
  }

  _sendMsgListStatusChangedNotify() {
    if (this.changeMsgStatusDelay) {
      clearTimeout(this.changeMsgStatusDelay);
    }
    let ts = Date.now();
    if (ts > this.lastMsgStatusNotify + 200) {
      this._sendDataChangedToApp(constantsApp.ACTION_MSG_STATUS_CHANGED, null);
    } else {
      this.changeMsgStatusDelay = setTimeout(() => {
        this._sendDataChangedToApp(
          constantsApp.ACTION_MSG_STATUS_CHANGED,
          null
        );
        this.changeMsgStatusDelay = null;
      }, 100);
    }
    this.lastMsgStatusNotify = ts;
  }

  disconnectSocket() {
    if (this.cancelTokenDNSSource) {
      this.cancelTokenDNSSource.cancel();
      this.cancelTokenDNSSource = null;
    }
    if (this.cancelTokenLoginGroupwareSource) {
      this.cancelTokenLoginGroupwareSource.cancel();
      this.cancelTokenLoginGroupwareSource = null;
    }
    if (this.cancelTokenHTTPOrgSource) {
      this.cancelTokenHTTPOrgSource.cancel();
      this.cancelTokenHTTPOrgSource = null;
    }
    this.releaseTask();
    if (this.tcpConnection) {
      this.tcpConnection.disconnectSocket(true);
    }
    this.isConnecting = false;
    this.isAuthenticated = false;
    this.roomListPacketResponse = false;

    if (this.timeoutRelogin) {
      clearTimeout(this.timeoutRelogin);
      this.timeoutRelogin = null;
    }
  }

  releaseTask() {
    clearTimeout(this.delayRequestUserList);
    this.stopAllPingTimeoutList();
    this.stopLiveTimer();
    this.stopTimeCardTimer();
    this.keepForwardMessageList = {};
  }

  resetNumberRetry() {
    this.numberOfRetry = 0;
  }

  loginWithDomainUserIdPassword(
    domain,
    userId,
    password,
    encryptUserId,
    encryptPassword,
    otpCode
  ) {
    const isOnRequest = this.isConnecting || this.isAuthenticated;
    if (isOnRequest) {
      return;
    }
    this.isConnecting = true;
    this.domain = domain;
    this.userId = userId;
    this.password = password;
    this.encryptUserId = encryptUserId;
    this.encryptPassword = encryptPassword;
    safeRetryLiveTimeout = 0;

    this._startMessengerCenter(true, true, otpCode);
  }

  logout(callBack) {
    this.logoutWithStatus({ code: 0 }, callBack);
  }

  logoutWithStatus(status, callBack) {
    this.markPositionUnreadMessageCached.clearAllMark();
    this.resetNumberRetry();
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.LogoutPacket(appPrefs.getDeviceCode())
      );

      setTimeout(() => {
        appPrefs.clearAllSetting();
        this.disconnectSocket();
        this._sendConnectionCloseAndLogoutToApp(status);
        if (callBack) {
          callBack();
        }
      }, 500);
    } else {
      appPrefs.clearAllSetting();
      this.disconnectSocket();
      this._sendConnectionCloseAndLogoutToApp(status);
      if (callBack) {
        callBack();
      }
    }
  }

  relogin(resetRetryNumber) {
    const isOnRequest = this.isConnecting || this.isAuthenticated;
    if (isOnRequest) {
      myLogger.log('has connection already, ignore retry login ');

      return;
    }

    appPrefs.loadAllAppSetting((setting, alreadyLogin) => {
      if (alreadyLogin) {
        this.userKey = setting.account_info.user_key;
        this.domain = setting.account_info.domain;
        this.userId = setting.account_info.user_id;
        this.password = setting.account_info.password;
        if (resetRetryNumber) {
          this.resetNumberRetry();
        }

        if (
          this.numberOfRetry < MAX_NUMBER_RETRY &&
          this.safeRetryRelogin < SAFE_MAX_NUMBER_RETRY
        ) {
          const timeout = this.numberOfRetry * 5000;

          myLogger.log(
            'prepare reconnect socket retry = ',
            this.numberOfRetry,
            timeout,
            'safeRetryRelogin',
            this.safeRetryRelogin
          );

          if (this.timeoutRelogin) {
            clearTimeout(this.timeoutRelogin);
            this.timeoutRelogin = null;
          }

          this.timeoutRelogin = setTimeout(() => {
            this.safeRetryRelogin++;
            this.numberOfRetry++;
            myLogger.log(
              'start reconnect socket retry = ',
              this.numberOfRetry,
              timeout
            );
            this.isConnecting = true;
            this._startMessengerCenter(false, false, null);
          }, timeout);
        } else {
          myLogger.log(
            'reach max number of retry ',
            this.numberOfRetry,
            this.safeRetryRelogin
          );
          this.resetNumberRetry();
          this.safeRetryRelogin = 0;
          this.logout();
        }
      }
    });
  }

  _startMessengerCenter(isLogin, serverXmlRequest, otpCode) {
    if (serverXmlRequest) {
      this._resolveDNSFromHost('http://', this.domain, (response, error) => {
        if (error != null) {
          this._resolveDNSFromHost('https://', this.domain, (res, err) => {
            if (err != null) {
              this._sendResolveDNSErrorToApp(
                isLogin,
                constantsApp.SOCKET_ERROR_CODE.dns_error
              );
            } else {
              if (this._processDNSXML(res.data)) {
                //connect tcp socket
                this._connectToGroupwareIfNeed(isLogin, otpCode);
              } else {
                this._sendResolveDNSErrorToApp(
                  isLogin,
                  constantsApp.SOCKET_ERROR_CODE.dns_error
                );
              }
            }
          });
        } else {
          if (this._processDNSXML(response.data)) {
            //connect tcp socket
            this._connectToGroupwareIfNeed(isLogin, otpCode);
          } else {
            this._resolveDNSFromHost('https://', this.domain, (res, err) => {
              if (err != null) {
                this._sendResolveDNSErrorToApp(
                  isLogin,
                  constantsApp.SOCKET_ERROR_CODE.dns_error
                );
              } else {
                if (this._processDNSXML(res.data)) {
                  //connect tcp socket
                  this._connectToGroupwareIfNeed(isLogin, otpCode);
                } else {
                  this._sendResolveDNSErrorToApp(
                    isLogin,
                    constantsApp.SOCKET_ERROR_CODE.dns_error
                  );
                }
              }
            });
          }
        }
      });
    } else {
      this._connectTCPClient(isLogin);
    }
  }

  _processDNSXML(dataResponse) {
    try {
      this.initServerInfo();
      const rawJsonData = parser.xml2json(dataResponse, { compact: false });
      const jsonData = JSON.parse(rawJsonData);
      const childXML = jsonData.elements[0].elements;
      childXML.forEach(element => {
        if (element.name == 'serverinfo') {
          const attrs = element.attributes;
          this.serverInfo.address = attrs['ip'];
          this.serverInfo.port = attrs['port'];
          this.serverInfo.showMobileLogin = attrs['mobileloginshow'] ?? 0;
        } else if (element.name == 'title') {
          if (element.elements) {
            this.serverInfo.title = element.elements[0].text ?? '';
          }
        } else if (element.name == 'newver') {
          if (element.elements) {
            this.serverInfo.version = element.elements[0].text ?? 1;
          }
        } else if (element.name == 'zip') {
          if (element.elements) {
            this.serverInfo.zip = element.elements[0].text ?? 0;
          }
        } else if (element.name == 'chatroom') {
          const attrs = element.attributes;
          if (attrs) {
            this.serverInfo.maxContactInRoom = attrs['onelimit'] ?? 0;
          }
        } else if (element.name == 'fileconfig') {
          const attrs = element.attributes;
          if (attrs) {
            this.serverInfo.maxFileSize = attrs['maxfilesize'] ?? 0;
          }
        } else if (element.name == 'savebutton') {
          const attrs = element.attributes;
          if (attrs) {
            this.serverInfo.canDownloadFileValue = attrs['mobile'] ?? 1;
          }
        } else if (element.name == 'transferfilebyweb') {
          if (element.elements) {
            this.serverInfo.transferFileByWeb = element.elements[0].text ?? 0;
          }
        } else if (element.name == 'trackgps') {
          if (element.elements) {
            this.serverInfo.supportTrackGps = element.elements[0].text ?? 0;
          }
        } else if (element.name == 'webdisk') {
          if (element.elements) {
            this.serverInfo.clouddiskSupport = element.elements[0].text ?? 0;
          }
        } else if (element.name == 'translate') {
          if (element.elements) {
            const text = element.elements[0].text;
            this.serverInfo.translate.useTranslate = text == '1' ? true : false;
          }
        } else if (element.name == 'videoconf') {
          const attrs = element.attributes;
          if (attrs) {
            this.serverInfo.videoConferenceInfo = {
              method: attrs['method'] ?? 'wss',
              server: attrs['server'] ?? '',
              server2: attrs['server2'] ?? '',
              rtcserver: attrs['rtcserver'] ?? '',
              httpsport: attrs['httpsport'] ?? 17005,
              wssport: attrs['wssport'] ?? 8989,
              api: attrs['api'] ?? 0,
              version: attrs['v'] ?? 1,
              audioroom: attrs['audioroom'] ?? 0,
              callOneOne: constantsApp.IGNORE_SIP_CALl ? 0 : attrs['sip'] ?? 0
            };
          }
          if (element.elements) {
            this.serverInfo.videoConference = element.elements[0].text ?? 0;
          }
        } else if (element.name == 'memo') {
          if (element.elements) {
            this.serverInfo.whisperSupport = element.elements[0].text ?? 1;
          }
        } else if (element.name == 'chatboard') {
          if (element.elements) {
            this.serverInfo.chatBoardSupport = element.elements[0].text ?? 0;
          }
        } else if (element.name == 'weblogin') {
          if (element.elements) {
            this.serverInfo.otpLogin = element.elements[0].text ?? 0;
          }
        } else if (element.name == 'servercon') {
          const attrs = element.attributes;
          if (attrs) {
            this.serverInfo.liveConfigInfo.liveTimeout =
              attrs['LIVETIMEOUT'] ?? 10;
            this.serverInfo.liveConfigInfo.liveTime = attrs['LIVETIME'] ?? 30;
            this.serverInfo.liveConfigInfo.retry = attrs['RETRY'] ?? 3;
          }
        } else if (element.name == 'ssl') {
          const attrs = element.attributes;
          if (attrs) {
            this.serverInfo.sslPort = attrs['port'] ?? 0;
          }
          if (element.elements) {
            this.serverInfo.ssl = element.elements[0].text ?? 0;
          }
        } else if (element.name == 'sip') {
          const attrs = element.attributes;
          if (attrs) {
            this.serverInfo.voiceIpServer.sipIp = attrs['ip'] ?? '';
            this.serverInfo.voiceIpServer.sipPort = attrs['port'] ?? '';
            this.serverInfo.voiceIpServer.sipProtocol = attrs['protocol'] ?? '';
            this.serverInfo.voiceIpServer.sipSupportVideo =
              (attrs['video'] ?? 0) > 0;
            this.serverInfo.groupCallSupport = attrs['video'] ?? 0;
            if (element.elements) {
              this.serverInfo.voiceIpServer.isActiveSip =
                element.elements[0].text ?? 0;
            }
          }
        } else if (element.name == 'SAVELOG') {
          if (element.elements) {
            this.serverInfo.saveLogger =
              element.elements[0].text == 1 ? true : false;
          }
        } else if (element.name == 'orghttp') {
          if (element.elements) {
            this.serverInfo.useHTTPOrgAPI =
              element.elements[0].text == 1 ? true : false;
          }
        } else if (element.name == 'filetrans') {
          if (element.elements) {
            this.serverInfo.allowFileTransfer =
              element.elements[0].text != 0 ? true : false;
          }
        }
      });

      if (this.serverInfo.sslPort == 0) {
        this.serverInfo.sslPort = this.serverInfo.port;
      }

      if (this.serverInfo.maxFileSize == 0) {
        this.serverInfo.maxFileSize = 4294967296;
      }

      if (this.serverInfo.maxContactInRoom == 0) {
        this.serverInfo.maxContactInRoom = 50;
      }

      appPrefs.setExtraServerInfo(this.serverInfo);
      myLogger.log('serverinfo:', this.serverInfo);
      return true;
    } catch (error) {
      myLogger.log(error);
      return false;
    }
  }

  async _resolveDNSFromHost(scheme: string, domain: string, callback) {
    var url = scheme + domain + '/winapp/' + domain + '/messenger/server.xml';
    myLogger.logTag(TAG, 'start request:' + url);

    try {
      const CancelToken = axios.CancelToken;
      this.cancelTokenDNSSource = CancelToken.source();
      const response = await this.axiosIgnoreSSL.get(url, {
        timeout: 30000,
        cancelToken: this.cancelTokenDNSSource.token
      });
      this.cancelTokenDNSSource = null;
      callback(response, null);
    } catch (error) {
      this.cancelTokenDNSSource = null;
      if (axios.isCancel(error)) {
        console.log('Request canncelled');
        return;
      }
      callback(null, error);
    }
  }

  async _loginGroupware(
    scheme: string,
    domain: string,
    otpCode: String,
    callback
  ) {
    var url = scheme + domain + '/ngw/sign/auth';
    if (otpCode) {
      url = url + '?step=2&is_checking_otp=1';
    }

    myLogger.log('_loginGroupware', url);

    try {
      const CancelToken = axios.CancelToken;
      this.cancelTokenLoginGroupwareSource = CancelToken.source();

      const response = await axios.post(
        url,
        {
          gw_id: this.encryptUserId,
          gw_pass: this.encryptPassword,
          device_id: appPrefs.getPCUUid(),
          model: appPrefs.getDeviceName(),
          token: '',
          method: appPrefs.getMethodLogin(),
          usb: '',
          mac: '',
          code: otpCode ?? '',
          ver: 1,
          auto_save_id: 0,
          dont_ask: 0
        },
        {
          headers: {
            PRODUCT: 'hanbirotalk',
            'User-Agent': 'TalkElectron',
            APP_TYPE: 'messenger',
            'APP-TYPE': 'messenger',
            device: 'app'
          },

          timeout: 30000,
          cancelToken: this.cancelTokenLoginGroupwareSource.token
        }
      );
      this.cancelTokenLoginGroupwareSource = null;
      callback(response, null);
    } catch (error) {
      myLogger.log('_loginGroupware', error);
      this.cancelTokenLoginGroupwareSource = null;
      if (axios.isCancel(error)) {
        console.log('Request canncelled');
        return;
      }
      callback(null, error);
    }
  }

  async sendUnNeedResult(urls = []) {
    // myLogger.logTag(TAG, 'start request:' + url);
    // console.log(urls);

    try {
      for (let i = 0; i < urls.length; i++) {
        const url = urls[i];
        const response = await axios.get(url, {
          timeout: 10000
        });
        // console.log(response);
      }
    } catch (error) {
      // console.log(error);
    }
  }

  async sendSipResultAPI({
    roomKey,
    fromKey,
    toKey,
    sipKey,
    result,
    video,
    sipNumber
  }) {
    await this.sendSipAPI({
      mode: 'result',
      roomKey: roomKey,
      fromKey: fromKey,
      toKey: toKey,
      sipKey: sipKey,
      result: result,
      video: video,
      sipNumber: sipNumber
    });
  }

  async sendSipAPI({
    mode,
    roomKey,
    fromKey,
    toKey,
    sipKey,
    result,
    duration,
    reason,
    video,
    sipNumber
  }) {
    let authKey = global.ShareGlobalObject.account_info.auto_login_key;
    let loginKey = global.ShareGlobalObject.account_info.web_auto_login_key;
    let url = 'https://' + this.domain + '/winapp/hcsong/sipcheck.php';

    let data = {
      mode: mode,
      loginkey: loginKey,
      roomkey: roomKey,
      fromkey: fromKey,
      tokey: toKey,
      userkey: this.userKey,
      sipkey: sipKey,
      mykey: this.userKey,
      authkey: authKey,
      video: video
    };
    if (result != null) {
      data = { ...data, result: result };
    }
    if (duration != null) {
      data = { ...data, duration: duration };
    }
    if (reason != null) {
      data = { ...data, reason: reason };
    }

    if (sipNumber != null) {
      data = { ...data, sipnumber: sipNumber };
    }

    if (this.tcpConnection.saveLogger) {
      log.log('sendSipAPI', url, data);
    } else {
      console.log('sendSipAPI', url, data);
    }

    try {
      const response = await axios.get(url, {
        params: data
      });
      if (this.tcpConnection.saveLogger) {
        log.log('receivedSipAPI', url, response?.data);
      } else {
        console.log('receivedSipAPI', url, response?.data);
      }
    } catch (error) {
      if (this.tcpConnection.saveLogger) {
        log.log('receivedSipAPI error ', error);
      } else {
        console.log('receivedSipAPI error ', error);
      }
    }
  }

  // ----------------------------------------------------------------MAIN SOCKET--------------------------------------------------------------------------------------

  isRequireOTP(data) {
    if (!data.success && data.code == 'otp') {
      return true;
    }
    return false;
  }

  isNotNeedOTP(data) {
    if (
      data.success &&
      TextUtils.isNotEmpty(data.cookie) &&
      TextUtils.isNotEmpty(data.hmail_key) &&
      TextUtils.isNotEmpty(data.u_no) &&
      TextUtils.isNotEmpty(data.u_cn)
    ) {
      return true;
    }
    return false;
  }

  isOTPNotRegisterYet(data) {
    if (!data.success && data.code == 'force') {
      return true;
    }
    return false;
  }

  updateMenuSupport(menu) {
    if (menu) {
      menu.forEach(item => {
        if (item.name == 'disk') {
          this.clouddiskSupportInOTP = 1;
        } else if (item.name == 'postit') {
          this.whisperSupportInOTP = 1;
        }
      });
      if (this.clouddiskSupportInOTP != 1) {
        this.clouddiskSupportInOTP = 0;
      }
      if (this.whisperSupportInOTP != 1) {
        this.whisperSupportInOTP = 0;
      }
    } else {
      //not exist menu
      this.clouddiskSupportInOTP = -1;
      this.whisperSupportInOTP = -1;
    }
  }

  _connectToGroupwareIfNeed(isLogin, otpCode) {
    if (isLogin && this.serverInfo.otpLogin > 0) {
      console.log('start OTP login');
      this._loginGroupware(
        'https://',
        this.domain,
        otpCode,
        (response, error) => {
          if (error || !response.data) {
            this._sendResolveDNSErrorToApp(
              isLogin,
              constantsApp.SOCKET_ERROR_CODE.dns_error
            );
            return;
          }

          // console.log(response.data);

          if (this.isNotNeedOTP(response.data)) {
            this.updateMenuSupport(response.data ? response.data.menu : []);
            this._connectTCPClient(isLogin);
            return;
          }

          if (this.isRequireOTP(response.data)) {
            this._sendResolveDNSErrorToApp(
              isLogin,
              constantsApp.SOCKET_ERROR_CODE.require_otp,
              response.data.msg
            );
            return;
          }

          if (this.isOTPNotRegisterYet(response.data)) {
            this._sendResolveDNSErrorToApp(
              isLogin,
              constantsApp.SOCKET_ERROR_CODE.customError,
              allTranslation.text(
                'Your account has not registered OTP. Please register OTP on web browser'
              )
            );
            return;
          }

          if (!response.data.success) {
            this._sendResolveDNSErrorToApp(
              isLogin,
              constantsApp.SOCKET_ERROR_CODE.customError,
              response.data.msg
            );
            return;
          }
        }
      );
    } else {
      this._connectTCPClient(isLogin);
    }
  }

  _setupLog() {
    log.transports.file.level = 'info';
    log.transports.file.fileName = `${this.domain}.log`;
    log.transports.file.maxSize = 5242880;
  }

  _connectTCPClient(isLogin) {
    myLogger.logTag(
      TAG,
      '_connectTCPClient With ip',
      this.serverInfo.address,
      'port',
      this.serverInfo.port,
      ' zip',
      this.serverInfo.zip,
      ' ssl',
      this.serverInfo.ssl,
      isLogin ? 'type login' : 'type relogin'
    );

    this.typeLogin = isLogin;

    const sslSupport = this.serverInfo.ssl > 0;
    const zipSupport = this.serverInfo.zip > 0;

    this._setupLog();

    this.tcpConnection = new TCPConnection();
    this.tcpConnection.saveLogger = this.serverInfo.saveLogger;
    this.tcpConnection.connectWithIpPort(
      this.serverInfo.address,
      this.serverInfo.port,
      sslSupport,
      this.serverInfo.sslPort,
      zipSupport,
      {
        socketConnected: () => {
          myLogger.log('Socket connected');
          let packet = null;
          if (isLogin || this.useLoginForRelogin) {
            this.tcpConnection.isLogin = isLogin;
            let loginMode;
            if (this.useLoginForRelogin) {
              loginMode = 'SILENT';
            }
            packet = new MesengerPacket.LoginPacket(
              this.domain,
              this.userId,
              this.password,
              appPrefs.getPCUUid(),
              zipSupport ? 1 : 0,
              appPrefs.getDeviceName(),
              loginMode
            );
          } else {
            packet = new MesengerPacket.ReLoginPacket(
              this.userKey,
              appPrefs.getPCUUid(),
              zipSupport ? 1 : 0
            );
          }
          this.tcpConnection.sendPacket(packet);
        },
        socketDisconnectWithError: error => {
          if (this.useLoginForRelogin) {
            this.useLoginForRelogin = false;
            this.logoutWithStatus({ code: 3, msg: 'Server restart' });
          } else {
            this.releaseTask();
            this._sendConnectionCloseOnErrorToApp(error);
          }
        },
        onReceivedPacket: xmlString => {
          const raw = xmlString.toString('utf8').trim();
          let i = raw.lastIndexOf('>');
          let rawXmlData = raw.substring(0, i + 1);
          this._parserXMLAllAPI(rawXmlData);
        }
      }
    );
  }

  _parserXMLAllAPI(xmlString: string) {
    try {
      const jsonData = parser.xml2js(xmlString, { compact: false });
      // const rawJsonData = parser.xml2json(xmlString, { compact: false });
      // const jsonData = JSON.parse(rawJsonData);
      const childXML = jsonData.elements[0].elements;
      childXML.forEach(element => {
        if (element.name == 'USER') {
          const childUser = element.elements;
          XMLProcess.processUser(this, childUser);
        } else if (element.name == 'ALARM') {
          const childAlarm = element.elements;
          XMLProcess.processAlarm(this, childAlarm);
        } else if (element.name == 'CHAT') {
          const childChat = element.elements;
          XMLProcess.processChat(this, childChat);
        } else if (element.name == 'FILE') {
          const childFile = element.elements;
          XMLProcess.processFile(this, childFile);
        } else if (element.name == 'CLOUD') {
          const childUser = element.elements;
          XMLProcess.processCloud(this, childUser);
        }
      });
    } catch (err) {
      this.disconnectSocket();
      this._sendConnectionCloseOnErrorToApp(err);
    }
  }

  async sendHTTPUserListRequest() {
    var url = `https://${this.domain}/winapp/hcsong/messenger_org.php`;
    log.log('sendHTTPUserListRequest', url);

    try {
      const CancelToken = axios.CancelToken;
      this.cancelTokenHTTPOrgSource = CancelToken.source();

      let authKey = global.ShareGlobalObject.account_info.auto_login_key;
      let httpOrgTime = appPrefs.getLastHTTPUserListUpdate();
      console.log('httpOrgTime', httpOrgTime);
      const response = await axios.post(url, null, {
        headers: {
          PRODUCT: 'hanbirotalk',
          'User-Agent': 'TalkElectron',
          APP_TYPE: 'messenger',
          'APP-TYPE': 'messenger',
          device: 'app',
          HANBIRO_SORT: 'JOBTITLE',
          'HANBIRO-SORT': 'JOBTITLE',
          HANBIRO_USERKEY: this.userKey,
          'HANBIRO-USERKEY': this.userKey,
          HANBIRO_AUTHKEY: authKey,
          'HANBIRO-AUTHKEY': authKey,
          HANBIRO_USERLISTTIME: httpOrgTime,
          'HANBIRO-USERLISTTIME': httpOrgTime
        },

        timeout: 60000,
        cancelToken: this.cancelTokenHTTPOrgSource.token
      });
      this.cancelTokenHTTPOrgSource = null;

      const responseData = response?.data;
      if (responseData) {
        log.log('sendHTTPUserListRequest', responseData);
        try {
          const jsonData = parser.xml2js(responseData, { compact: false });
          const childXML = jsonData.elements[0].elements;
          let response = {};
          childXML.forEach(element => {
            if (element.name == 'USER') {
              const childUser = element.elements;

              childUser.forEach(element => {
                if (element.name == 'USERLIST') {
                  const attrs = element.attributes;
                  if (attrs) {
                    response.createdTime = attrs['CREATEDTIME'] ?? 0;
                  }
                  let listGroup = [];
                  let listUser = [];
                  let listNickName = [];
                  let listPhoto = [];
                  const userListElements = element.elements;
                  if (userListElements) {
                    userListElements.forEach(userElement => {
                      if (userElement.name == 'DEPT') {
                        const deptListElement = userElement.elements;
                        if (deptListElement) {
                          let branchDept = null;
                          let manageDept = null;
                          let deptList = [];
                          deptListElement.forEach(dept => {
                            if (dept.name == 'MYDEPT') {
                              const attrs = dept.attributes;
                              if (attrs) {
                                const key = attrs['KEY'];
                                if (key) {
                                  deptList.push(key);
                                }
                              }
                            } else if (dept.name == 'MANAGEDEPT') {
                              const attrs = dept.attributes;
                              if (attrs) {
                                manageDept = attrs['KEY'] ?? '';
                              }
                            } else if (dept.name == 'BRANCHDEPT') {
                              const attrs = dept.attributes;
                              if (attrs) {
                                branchDept = attrs['KEY'] ?? '';
                              }
                            }
                          });
                          response.deptList = deptList;
                          response.manageDept = manageDept;
                          response.branchDept = branchDept;
                        }
                      } else if (userElement.name == 'FAV') {
                        const attrs = userElement.attributes;
                        if (attrs) {
                          let group = {
                            groupKey: attrs['KEY'],
                            groupAlias: attrs['NAME'],
                            groupShortName: attrs['SHORTNAME'],
                            groupFullName: attrs['LONGNAME'],
                            groupParentKey: attrs['PKEY'] ?? '',
                            groupLang: attrs['COUNTRY'] ?? '',
                            groupType: TalkConstants.GROUP_TYPE_USER,
                            favorite: true
                          };
                          listGroup.push(group);
                        }
                      } else if (
                        userElement.name == 'DIR' ||
                        userElement.name == 'DIRFAV'
                      ) {
                        const attrs = userElement.attributes;
                        if (attrs) {
                          let group = {
                            groupHeader: attrs['HEADER'] ?? '',
                            groupKey: attrs['KEY'] ?? '',
                            groupAlias: attrs['NAME'],
                            groupShortName: attrs['SHORTNAME'],
                            groupFullName: attrs['LONGNAME'],
                            groupParentKey: attrs['PKEY'] ?? '',
                            groupLang: attrs['COUNTRY'] ?? '',
                            groupType: TalkConstants.GROUP_TYPE_ADMIN,
                            favorite:
                              userElement.name == 'DIRFAV' ? true : false
                          };
                          listGroup.push(group);
                        }
                      } else if (
                        userElement.name == 'ITEM' ||
                        userElement.name == 'ITEMFAV'
                      ) {
                        const attrs = userElement.attributes;
                        if (attrs) {
                          const localPhone = attrs['LOCALPHONE'] ?? '';
                          const phone = attrs['PHONE'] ?? '';
                          let userLocalPhoneValue = phone;
                          if (TextUtils.isNotEmpty(localPhone)) {
                            userLocalPhoneValue =
                              userLocalPhoneValue + ' (' + localPhone + ')';
                          }

                          let contact = {
                            userEmail: attrs['EMAIL'] ?? '',
                            userKey: attrs['KEY'],
                            userLocalPhone: userLocalPhoneValue,
                            loginType: TalkConstants.loginTypeFromString(
                              attrs['LOGINTYPE'] ?? ''
                            ),
                            mobilephone: attrs['MOBILEPHONE'] ?? '',
                            userAlias: attrs['NAME'] ?? '',
                            nickname: attrs['NICKNAME'] ?? '',
                            shortname: attrs['SHORTNAME'] ?? '',
                            order: parseInt(attrs['ORDER']) ?? 0,
                            phototime: parseInt(attrs['PHOTOTIME']) ?? 0,
                            parentKey: attrs['PKEY'] ?? '',
                            position: attrs['POSITION'] ?? '',
                            sex: parseInt(attrs['SEX']) ?? 0,
                            status: parseInt(attrs['STATUS']) ?? 0,
                            fullname: attrs['LONGNAME'] ?? '',
                            sipphone: attrs['SIP'] ?? '',
                            userLang: '',
                            hiddenUser: false,
                            favorite:
                              userElement.name == 'ITEMFAV' ? true : false
                          };

                          let chosunText =
                            Utils.decomposeSylLabels(contact.userAlias) +
                            Utils.decomposeSylLabels(contact.shortname) +
                            Utils.decomposeSylLabels(contact.fullname);
                          contact.chosunText = chosunText;
                          listUser.push(contact);
                        }
                      } else if (userElement.name == 'NICKNAME') {
                        const attrs = userElement.attributes;
                        if (attrs) {
                          let nickNameObj = {
                            userKey: attrs['USERKEY'] ?? '',
                            nickName: attrs['NICKNAME'] ?? ''
                          };
                          listNickName.push(nickNameObj);
                        }
                      } else if (userElement.name == 'PHOTO') {
                        const attrs = userElement.attributes;
                        if (attrs) {
                          let photoObj = {
                            userKey: attrs['USERKEY'] ?? '',
                            timeStamp: parseInt(attrs['TIMESTAMP']) ?? 0
                          };
                          listPhoto.push(photoObj);
                        }
                      }
                    });
                  }
                  response.listGroup = listGroup;
                  response.listUser = listUser;
                  response.listNickName = listNickName;
                  response.listPhoto = listPhoto;
                }
              });
            }
          });

          let previousCreateTime = appPrefs.getLastHTTPUserListUpdate();
          if (previousCreateTime != response.createdTime) {
            let result = cj4dxUtils.storeBranchUserOnly(
              response.manageDept,
              response.branchDept,
              response.deptList,
              response.listUser,
              response.listGroup
            );

            if (result.hasBranchInMyKey) {
              talkDB.storeContactAndGroup(
                result.newListGroup,
                result.newListContact
              );
              this._sendORGChangedNotify();
              this.sendLivePacket();
            } else {
              talkDB.storeContactAndGroup(
                response.listGroup,
                response.listUser
              );
              this._sendORGChangedNotify();
              this.sendLivePacket();
            }

            talkDB.updateNickNameAndPhotoTimeListFromORGHTTP(
              response.listNickName,
              response.listPhoto
            );

            appPrefs.setLastHTTPUserListUpdate(response.createdTime);
          }
        } catch (err) {
          log.log('sendHTTPUserListRequest', err);
        }
      }
    } catch (error) {
      log.log('sendHTTPUserListRequest', error);
      this.cancelTokenHTTPOrgSource = null;
      if (axios.isCancel(error)) {
        console.log('Request canncelled');
        return;
      }
    }
  }

  sendDelayRequestUserList(useDefaultDelay) {
    clearTimeout(this.delayRequestUserList);
    let randomDelay = getRandomInt(60);
    randomDelay = randomDelay * 60 * 1000;
    let delay = useDefaultDelay ? 30000 : randomDelay;
    this.delayRequestUserList = setTimeout(() => {
      if (this.serverInfo.useHTTPOrgAPI) {
        this.sendHTTPUserListRequest();
      } else {
        this.sendUserListPacket();
      }
    }, delay);
  }

  sendUserListPacket() {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.UserList(
          this.domain,
          this.userKey,
          appPrefs.getLanguageDef()
        )
      );
    }
  }

  sendUserModInfoPacket(time) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(new MesengerPacket.UserModInfoPacket(time));
    }
  }

  sendRoomListPacket() {
    if (this.isAuthenticated) {
      this.roomListPacketResponse = false;
      this.tcpConnection.sendPacket(
        new MesengerPacket.RoomListPacket(appPrefs.getLastUpdateRoomList())
      );
    }
  }

  sendTimeCardPacket() {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(new MesengerPacket.TimeCardPacket());
    }
  }

  sendTimeCardPacketTimer() {
    const timeCardDelay = 30 * 60 * 1000;
    this.sendTimeCardPacket();
    this.stopTimeCardTimer();
    this.timeCardTimer = setInterval(() => {
      this.sendTimeCardPacket();
    }, timeCardDelay);
  }

  stopTimeCardTimer() {
    if (this.timeCardTimer) {
      clearInterval(this.timeCardTimer);
      this.timeCardTimer = null;
    }
  }

  sendLivePacket() {
    if (this.isAuthenticated) {
      this.addPingTimeout();
      this.tcpConnection.sendPacket(new MesengerPacket.LivePacket());
    }
  }

  stopLiveTimer() {
    if (this.liveTimer) {
      clearInterval(this.liveTimer);
      this.liveTimer = null;
    }
  }

  sendLivePacketTimer() {
    const liveDelay = (this.serverInfo.liveConfigInfo.liveTime ?? 30) * 1000;
    this.sendLivePacket();
    this.stopLiveTimer();
    this.liveTimer = setInterval(() => {
      this.sendLivePacket();
    }, liveDelay);
  }

  sendAPIToSever(apiID, packetInfo) {
    if (apiID == constantsApp.API_GETTALK) {
      this.sendGetTalkPacket(packetInfo);
    } else if (apiID == constantsApp.API_ROOM_CREATE) {
      this.sendRoomCreatePacket(packetInfo);
    } else if (apiID == constantsApp.API_ROOM_INVITE) {
      this.sendRoomInvitePacket(packetInfo);
    } else if (apiID == constantsApp.API_ROOM_ONE) {
      this.sendRoomOnePacket(packetInfo.roomKey, packetInfo.forceCheck);
    } else if (apiID == constantsApp.API_MARK_ALL_MESSAGE_AS_READ) {
      this.sendAllMessageReadPacket(packetInfo.roomKey, packetInfo.userKey);
    } else if (apiID == constantsApp.API_SEND_MESSAGE) {
      this.sendMessagePacket(packetInfo);
    } else if (apiID == constantsApp.API_SEND_FILE_HTTP_FAST) {
      this.sendFileHttpFastPacket(packetInfo);
    } else if (apiID == constantsApp.API_SEND_FILE_HTTP_FINISHED) {
      this.sendFileHttpFinishedPacket(packetInfo);
    } else if (
      apiID == constantsApp.API_SEND_FILE_HTTP_SENDING_COMPLETED_FINISHED
    ) {
      this.sendFileHttpSendingCompletedFinishedPacket(packetInfo);
    } else if (apiID == constantsApp.API_SEND_FILE_CANCEL) {
      this.sendFileCancelPacket(packetInfo);
    } else if (apiID == constantsApp.API_MSG_TYPING) {
      this.sendAPITypingPacket(packetInfo);
    } else if (apiID == constantsApp.API_USER_CHANGE_STATUS) {
      this.sendAPIChangeStatusPacket(packetInfo);
    } else if (apiID == constantsApp.API_USER_CHANGE_NICKNAME) {
      this.sendAPIChangeNickNamePacket(packetInfo);
    } else if (apiID == constantsApp.API_CHANGE_ALERT_ROOM) {
      this.sendAPIChangeAlertRoomPacket(packetInfo);
    } else if (apiID == constantsApp.API_ROOM_OUT) {
      this.sendAPIRoomOutPacket(packetInfo);
    } else if (apiID == constantsApp.API_CHANGE_ROOM_NAME) {
      this.sendAPIRoomNamePacket(packetInfo);
    } else if (apiID == constantsApp.API_CHANGE_ROOM_FAVORITE) {
      this.sendAPIChangeRoomFavoritePacket(packetInfo);
    } else if (apiID == constantsApp.API_CHANGE_ORG_FAVORITE) {
      this.sendAPIChangeOrgFavoritePacket(packetInfo);
    } else if (apiID == constantsApp.API_CANCEL_MESSAGE) {
      this.sendAPICancelMessagePacket(packetInfo);
    } else if (apiID == constantsApp.API_SAVE_FILE_CLOUDDISK) {
      this.sendAPISaveFileClouddiskPacket(packetInfo);
    } else if (apiID == constantsApp.API_FOLDER_CREATE) {
      this.sendAPIFolderCreatePacket(packetInfo);
    } else if (apiID == constantsApp.API_FOLDER_DELETE) {
      this.sendAPIFolderDeletePacket(packetInfo);
    } else if (apiID == constantsApp.API_FOLDER_UPDATE) {
      this.sendAPIFolderUpdatePacket(packetInfo);
    } else if (apiID == constantsApp.API_GET_FILES) {
      this.sendApiGetFiles(packetInfo);
    } else if (apiID == constantsApp.API_WEB_AUTO_LOGIN) {
      this.sendApiWebAutoLogin(packetInfo);
    } else if (apiID == constantsApp.API_SIP_MAKE_CALL) {
      this.sendApiSipMakeCall(packetInfo);
    } else if (apiID == constantsApp.API_BOARD_ALARM_NOTIFY) {
      this.sendApiBoardAlarmNotify(packetInfo);
    }
  }

  sendApiBoardAlarmNotify(packetInfo) {
    console.log('sendApiBoardAlarmNotify:', packetInfo);
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.AlarmBoardNotificationPacket(packetInfo)
      );
    }
  }

  clearCallStateTimer() {
    this.makingCallState.callStatus = CALL_STATE_CALL.prepareShow; //prepate show call page
    clearTimeout(this.makingCallState.timer);
    this.makingCallState.timer = null;

    setTimeout(() => {
      if (this.makingCallState.callStatus == CALL_STATE_CALL.prepareShow) {
        this.makingCallState.callStatus = CALL_STATE_CALL.init;
      }
    }, 5000);
  }

  sendApiSipMakeCall(packetInfo) {
    if (this.makingCallState.callStatus == CALL_STATE_CALL.init) {
      if (this.isAuthenticated) {
        this.makingCallState.callStatus = CALL_STATE_CALL.makingCall; // send sip
        this.makingCallState.timer = setTimeout(() => {
          sipController.makeCallClientKey = null;
          this.makingCallState.callStatus = CALL_STATE_CALL.init; //end call
          this.makingCallState.timer = null;
        }, 5000);
        sipController.makeCallClientKey = packetInfo.clientKey;
        this.tcpConnection.sendPacket(
          new MesengerPacket.SIPMakeCallPacket(packetInfo)
        );
      }
    } else {
      if (sipController.view && sipController.isLoaded()) {
        sipController.view.moveTop();
      }
    }
  }

  resetCallState(status = CALL_STATE_CALL.init) {
    clearTimeout(this.makingCallState.timer);
    this.makingCallState.callStatus = status;
    this.makingCallState.timer = null;
  }

  sendApiWebAutoLogin(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(new MesengerPacket.WebAutoLoginPackage());
    }
  }

  sendApiGetFiles(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.GetFilesPacket(
          packetInfo.roomKey,
          packetInfo.lastTime,
          packetInfo.filterType == 0 ? 'IMAGE' : 'OTHERS',
          constantsApp.GETFILES_COUNT
        )
      );
    }
  }

  sendAPIFolderUpdatePacket(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.FolderUpdateChildPacket(
          packetInfo.folderKey,
          packetInfo.userKeyList
        )
      );
    }
  }

  sendAPIFolderDeletePacket(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.FolderDelPacket(packetInfo.folderKey)
      );
    }
  }

  sendAPIFolderCreatePacket(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.FolderCreatePacket(
          packetInfo.folderName,
          packetInfo.folderKey,
          ''
        )
      );
    }
  }

  sendAPISaveFileClouddiskPacket(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.SaveFileClouddiskPacket(
          packetInfo.roomKey,
          packetInfo.numberKey,
          packetInfo.timestamp
        )
      );
    }
  }

  sendAPICancelMessagePacket(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.CancelMessagePacket(
          packetInfo.roomKey,
          this.userKey,
          packetInfo.numberKey,
          packetInfo.timestamp
        )
      );
    }
  }

  sendAPIChangeOrgFavoritePacket(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.OrgFavoritePacket(
          packetInfo.key,
          packetInfo.isGroup,
          packetInfo.isAdd
        )
      );
    }
  }

  sendAPIChangeRoomFavoritePacket(packetInfo) {
    if (this.isAuthenticated) {
      if (this.apiSupportList.hasSupportRoomFavorite()) {
        this.tcpConnection.sendPacket(
          new MesengerPacket.RoomFavoritePacket(
            packetInfo.roomKey,
            packetInfo.isAdd
          )
        );
      }
    }
  }

  sendAPIRoomNamePacket(packetInfo) {
    if (this.isAuthenticated) {
      if (this.apiSupportList.hasSupportChangeRoomName()) {
        this.tcpConnection.sendPacket(
          new MesengerPacket.RoomNamePacket(
            packetInfo.roomKey,
            packetInfo.isPublic,
            packetInfo.name
          )
        );
      }
    }
  }

  sendAPIRoomOutPacket(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.RoomOutPacket(packetInfo.roomKey, this.userKey)
      );
    }
  }

  sendAPIChangeAlertRoomPacket(packetInfo) {
    if (this.isAuthenticated) {
      if (this.apiSupportList.hasSupportRoomPushAlertApi()) {
        this.tcpConnection.sendPacket(
          new MesengerPacket.AlertRoomPacket(
            packetInfo.roomKey,
            packetInfo.enable
          )
        );
      }
    }
  }

  sendAPIChangeStatusPacket(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.ChangeStatus(this.userKey, packetInfo.status)
      );
    }
  }

  sendAPIChangeNickNamePacket(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.ChangeNickName(this.userKey, packetInfo.name)
      );
    }
  }

  sendAPITypingPacket(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.MsgTypingPacket(
          packetInfo.roomKey,
          packetInfo.userKey,
          packetInfo.name,
          packetInfo.status
        )
      );
    }
  }

  sendFileCancelPacket(packetInfo) {
    if (this.isAuthenticated) {
      if (this.apiSupportList.hasSupportMFileCancelApi())
        this.tcpConnection.sendPacket(
          new MesengerPacket.HTTPFileCancelPacket(
            packetInfo.roomKey,
            packetInfo.fileKey
          )
        );
    }
  }

  sendFileHttpSendingCompletedFinishedPacket(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.HTTPFileSendingPacket(
          'COMPLETED',
          packetInfo.number,
          packetInfo.fileKey,
          packetInfo.roomKey,
          packetInfo.fileType,
          packetInfo.filePath,
          packetInfo.fileSize,
          packetInfo.fileSize,
          packetInfo.fileName,
          packetInfo.receiver,
          packetInfo.time
        )
      );
      this.tcpConnection.sendPacket(
        new MesengerPacket.HTTPFileFinishedPacket(
          packetInfo.sender,
          packetInfo.fileKey,
          packetInfo.number,
          packetInfo.fileSize,
          packetInfo.roomKey,
          packetInfo.fileType,
          packetInfo.filePath,
          packetInfo.fileName,
          packetInfo.receiver,
          packetInfo.serverFilePath,
          packetInfo.time,
          packetInfo.serverFileName,
          packetInfo.type,
          packetInfo.recordTime
        )
      );
    }
  }

  sendFileHttpFinishedPacket(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.HTTPFileFinishedPacket(
          packetInfo.sender,
          packetInfo.fileKey,
          packetInfo.number,
          packetInfo.fileSize,
          packetInfo.roomKey,
          packetInfo.fileType,
          packetInfo.filePath,
          packetInfo.fileName,
          packetInfo.receiver,
          packetInfo.serverFilePath,
          packetInfo.time,
          packetInfo.serverFileName,
          packetInfo.type,
          packetInfo.recordTime
        )
      );
    }
  }

  sendFileHttpFastPacket(packetInfo) {
    if (this.isAuthenticated) {
      const onlineUser = talkDB.getOnlineUserWithRoomKey(
        packetInfo.roomKey,
        this.accountID
      );
      const onlineUserWithOutMe = onlineUser.filter(
        item => item.rdtUKey != this.userKey
      );

      const filePath = packetInfo.file.path;
      const fileSize = packetInfo.file.size;
      const fileName = packetInfo.file.name;
      const fileExt = TalkConstants.getFileExtension(fileName);
      if (fileExt == TalkConstants.MessageFileEXT.RECORD_EXT) {
        getAudioDurationInSeconds(filePath)
          .then(duration => {
            this.tcpConnection.sendPacket(
              new MesengerPacket.HTTPFileRequestPacket(
                packetInfo.roomKey,
                fileExt,
                filePath,
                fileSize,
                fileName,
                '',
                onlineUserWithOutMe,
                Math.round(duration),
                ''
              )
            );
          })
          .catch(error => {
            this.tcpConnection.sendPacket(
              new MesengerPacket.HTTPFileRequestPacket(
                packetInfo.roomKey,
                fileExt,
                filePath,
                fileSize,
                fileName,
                '',
                onlineUserWithOutMe,
                0,
                ''
              )
            );
          });
      } else {
        if (fileExt == TalkConstants.MessageFileEXT.IMAGE_EXT) {
          base64FromImagePath(
            filePath,
            constantsApp.THUMBNAIL_SIZE_SOCKET_REQUEST,
            constantsApp.THUMBNAIL_SIZE_SOCKET_REQUEST_QUALITY,
            fileName,
            base64 => {
              this.tcpConnection.sendPacket(
                new MesengerPacket.HTTPFileRequestPacket(
                  packetInfo.roomKey,
                  fileExt,
                  filePath,
                  fileSize,
                  fileName,
                  '',
                  onlineUserWithOutMe,
                  0,
                  base64
                )
              );
            }
          );
        } else {
          const thumbnailBase64 = '';
          this.tcpConnection.sendPacket(
            new MesengerPacket.HTTPFileRequestPacket(
              packetInfo.roomKey,
              fileExt,
              filePath,
              fileSize,
              fileName,
              '',
              onlineUserWithOutMe,
              0,
              thumbnailBase64
            )
          );
        }
      }
    }
  }

  sendAllMessageReadPacket(roomKey, userKey) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.AllMessageAsReadPacket(roomKey, userKey)
      );
    }
  }

  sendRoomCreatePacket(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.RoomCreatePacket(
          packetInfo.roomTag,
          packetInfo.roomAlias,
          packetInfo.userList
        )
      );
    }
  }

  sendRoomInvitePacket(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.RoomInvitePacket(
          packetInfo.roomKey,
          this.userKey,
          packetInfo.userList
        )
      );
    }
  }

  sendRoomOnePacket(roomKey) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(new MesengerPacket.RoomOnePacket(roomKey));
    }
  }

  sendMessagePacket(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.MessagePacket(
          packetInfo.roomKey,
          packetInfo.clientId,
          packetInfo.userKey,
          packetInfo.emoticon,
          packetInfo.message
        )
      );
    }
  }

  sendMessageResultPacket(roomKey, numberKey) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.MessageResultPacket(roomKey, numberKey)
      );
    }
  }

  sendGetTalkPacketNoCache(packetInfo) {
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.RoomTalkPacket(
          packetInfo.roomKey,
          packetInfo.lastTime,
          packetInfo.count,
          packetInfo.clientKey
        )
      );
    }
  }

  sendBothRoomListAndGettalkAPI(roomKey) {
    clearTimeout(this.delayReloadAllRoomListAndGettalkAPI);
    this.delayReloadAllRoomListAndGettalkAPI = setTimeout(() => {
      this.sendRoomListPacket();
      this.sendGetTalkPacketNoCache({
        roomKey: roomKey,
        lastTime: 0,
        count: constantsApp.GETTALK_COUNT,
        clientKey: 'IGNORE_UPDATE'
      });
    }, 2000);
  }

  sendGetTalkPacket(packetInfo) {
    if (this.sendGetTalkLocalIfPosible(packetInfo)) {
      return;
    }
    if (this.isAuthenticated) {
      this.tcpConnection.sendPacket(
        new MesengerPacket.RoomTalkPacket(
          packetInfo.roomKey,
          packetInfo.lastTime,
          packetInfo.count,
          packetInfo.clientKey
        )
      );
    }
  }

  sendLoginResAccessControlPacket() {
    macaddress.all((err, all) => {
      let macaddressList = [];
      if (!err) {
        const values = Object.values(all);
        if (values) {
          values.forEach(item => {
            if (item && item.mac) {
              macaddressList.push(item.mac);
            }
          });
        }
      }

      if (macaddressList.length <= 0) {
        this.initServerInfo();
        this.disconnectSocket();
        this._sendAuthenticationFailedToApp(
          this.typeLogin,
          constantsApp.SOCKET_ERROR_CODE.access_control
        );
        return;
      }

      this.tcpConnection.sendPacket(
        new MesengerPacket.LoginResAccessControlPacket(macaddressList.join(','))
      );
    });
  }

  sendGetThumbnailList(roomKey, fileList: []) {
    if (DISABLE_THUMBNAIL) {
      return;
    }
    if (this.isAuthenticated) {
      // fileList.forEach(item => {
      //   this.tcpConnection.sendPacket(
      //     new MesengerPacket.GetThumbnailPacket(roomKey, null, item)
      //   );
      // });
      this.tcpConnection.sendPacket(
        new MesengerPacket.GetThumbnailPacket(roomKey, fileList, null)
      );
    }
  }

  canUseGetTalkCache(roomKey) {
    if (USE_CACHE_GETTALK) {
      let data = null;
      let roomInfo = this.getTalkCached.infoCachedFromRoomKeyRequestTime(
        roomKey,
        0
      );
      if (roomInfo) {
        data = talkDB.getBeginTimeFromMessageList(
          this.accountID,
          roomKey,
          '',
          roomInfo.oldTime,
          0,
          constantsApp.GETTALK_COUNT
        );
      }

      if (data && data.firstTimeStamp >= 0) {
        return true;
      }
    }
    return false;
  }

  sendGetTalkLocalIfPosible(packetInfo) {
    if (USE_CACHE_GETTALK) {
      let data = null;
      let roomInfo = this.getTalkCached.infoCachedFromRoomKeyRequestTime(
        packetInfo.roomKey,
        packetInfo.lastTime
      );
      if (roomInfo) {
        data = talkDB.getBeginTimeFromMessageList(
          this.accountID,
          packetInfo.roomKey,
          packetInfo.clientKey,
          roomInfo.oldTime,
          packetInfo.lastTime,
          packetInfo.count
        );
      }

      if (data && data.firstTimeStamp >= 0) {
        // TODO send fake gettalk ?
        if (packetInfo.lastTime == 0) {
          this.sendGetTalkPacketNoCache(packetInfo);
        }

        this._sendGetTalkChangedNotify(data);
        return true;
      }
    }
    return false;
  }

  updateMarkPosition(roomKey, userKey, createDate) {
    const curRoomKey = this.getCurrentRoomKeyActive();
    const myKey = this.userKey;
    if (curRoomKey != roomKey && myKey != userKey) {
      this.markPositionUnreadMessageCached.markUnreadMessagePosition(
        roomKey,
        createDate
      );
    }
  }

  updateTalkInfo(roomKey, createDate) {
    if (USE_CACHE_GETTALK) {
      this.getTalkCached.updateTalkInfo(roomKey, createDate, talkDB);
    }
  }

  addTalkInfo(roomKey, oldTime, latestTime) {
    if (USE_CACHE_GETTALK) {
      this.getTalkCached.addTalkInfo(roomKey, oldTime, latestTime, talkDB);
    }
  }

  removeTalkInfo(roomKey) {
    if (USE_CACHE_GETTALK) {
      this.getTalkCached.clearWithRoomKey(roomKey);
    }
  }

  transferCacheAfterLogin() {
    if (USE_CACHE_GETTALK) {
      talkDB.transferCurrentToPrevTalkInfo();
      this.getTalkCached.clearAll();
    }
  }

  sendAlarmBoardNotification(boardInfo) {}
}

function checkPhotoThumbnailExist(roomKey, fileMessage) {
  // check thumbnail exist
  if (DISABLE_THUMBNAIL) {
    return true;
  }
  if (fileMessage.messageType == TalkConstants.MessageType.MESSAGE_TYPE_PHOTO) {
    let fileName = TalkConstants.generateThumbFileNameMessage(
      fileMessage.numberKey
    );
    let filePath = talkPath.filePathThumbWithName(roomKey, fileName);
    return talkPath.fileExistAtPath(filePath);
  }
  return true; //default other type is empty thumbnail, not need download
}

function saveThumbnailIfHave(messageElement, roomKey, numberKey) {
  if (messageElement.elements) {
    let thumbnailElement = messageElement.elements[0];
    if (thumbnailElement && thumbnailElement.name == 'THUMBNAIL') {
      if (thumbnailElement.elements) {
        let thumbnailBase64 = thumbnailElement.elements[0].text;
        if (thumbnailBase64) {
          let filePath = talkPath.filePathThumbWithName(
            roomKey,
            TalkConstants.generateThumbFileNameMessage(numberKey)
          );
          if (!talkPath.fileExistAtPath(filePath)) {
            talkPath.writeBase64Image(filePath, thumbnailBase64);
          }
        }
      }
    }
  }
}

const XMLProcess = {
  processCloud(messengerCenter: MessengerCenter, rootElement) {
    let response = {};
    rootElement.forEach(element => {
      if (element.name == 'MFILECLOUD') {
        response.apiID = 'MFILECLOUD';
        const attrs = element.attributes;
        if (attrs) {
          response.name = attrs['NAME'] ?? '';

          response.numberKey = attrs['NUMBER'] ?? '';
          response.receiver = attrs['RECVER'] ?? '';
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.userKey = attrs['SENDER'] ?? '';
          response.size = attrs['SIZE'] ?? 0;
          response.createDate = TalkConstants.timeFromTimeStamp(
            attrs['TIME'] ?? ''
          );
          (response.fileAddress = attrs['URL'] ?? ''), (response.filePort = 0);
          response.clouddisk = 1;
          response.fileKey = '';

          response.fileType = attrs['FILETYPE'];
          if (TextUtils.isNotEmpty(response.name)) {
            response.fileType = TalkConstants.getFileExtension(response.name);
          }
          response.messageType = TalkConstants.typeMessageFromFileType(
            response.fileType
          );

          response.hostName = '';

          response.path = '';

          response.serverFileName = '';
          response.serverFilePath = '';

          response.recordTime = 0;

          response.messageThirdParty =
            TalkConstants.MessageThirdParty.MESSAGE_CLOUDDISK_ID;
        }
      }
    });

    if (response.apiID == 'MFILECLOUD') {
      PacketListener.onReceivedFileCloudPacket(messengerCenter, response);
    }
  },
  processUser(messengerCenter: MessengerCenter, rootElement) {
    let response = {};
    rootElement.forEach(element => {
      if (element.name == 'LOGIN') {
        response.apiID = 'LOGIN';
        const attrs = element.attributes;
        let login = null;
        let statusCode = attrs['RESULT'] ?? 0;
        if (statusCode == 1) {
          login = {
            result: statusCode,
            userKey: attrs['USERKEY'] ?? '',
            nickname: attrs['NICKNAME'] ?? '',
            username: attrs['USERNAME'] ?? '',
            loginid: attrs['ID'] ?? '',
            lasmodifiedfavuserlist: attrs['LASTMODIFIED_FAVLIST'] ?? 0,
            lasmodifieduserlist: attrs['LASTMODIFIED_USERLIST'] ?? 0,
            lastusermodtime: attrs['LAST_USERMODTIME'] ?? 0,
            sex: attrs['SEX'] ?? 1,
            authkey: attrs['AUTHKEY'] ?? '',
            roomadmin: attrs['ROOMADMIN'] ?? 0,
            mysip: attrs['MYSIP'] ?? '',
            mysippass: attrs['MYSIPPASS'] ?? '',
            mobilephone: attrs['MOBILEPHONE'] ?? '',
            localphone: attrs['LOCALPHONE'] ?? '',
            autologinkey: attrs['AUTOLOGINKEY'] ?? '',
            whispercount: attrs['MEMO'] ?? 0,
            setpassword: attrs['SETPASSWORD'] ?? 0,
            enablepush: attrs['ENABLEPUSH'] ?? 0,
            newchatlog: attrs['NEWCHATLOG'] ?? 0,
            lastandroidver: attrs['LATEST_ANDROID_VER'] ?? '',
            gwdateformat: attrs['DATE_FORMAT']
          };
        } else {
          login = {
            result: statusCode
          };
        }
        response.login = login;
      } else if (element.name == 'RELOGIN') {
        response.apiID = 'RELOGIN';
        const attrs = element.attributes;
        let relogin = null;
        let statusCode = attrs['RESULT'] ?? 0;
        if (statusCode == 1) {
          relogin = {
            result: statusCode,
            authkey: attrs['AUTHKEY'] ?? '',
            lasmodifiedfavuserlist: attrs['LASTMODIFIED_FAVLIST'] ?? 0,
            lasmodifieduserlist: attrs['LASTMODIFIED_USERLIST'] ?? 0,
            lastusermodtime: attrs['LAST_USERMODTIME'] ?? 0,
            nickname: attrs['NICKNAME'] ?? '',
            setpassword: attrs['SETPASSWORD'] ?? 0,
            enablepush: attrs['ENABLEPUSH'] ?? 0
          };
        } else {
          relogin = {
            result: statusCode
          };
        }
        response.relogin = relogin;
      } else if (element.name == 'LOGINREQ') {
        response.apiID = 'LOGINREQ';
        const attrs = element.attributes;
        let loginReq = {
          card: attrs['CARD'] ?? 0,
          cardKey: attrs['CARDKEY'] ?? '',
          mac: attrs['MAC'] ?? 0,
          usb: attrs['USB'] ?? 0
        };
        response.loginReq = loginReq;
      } else if (element.name == 'MENU') {
        const attrs = element.attributes;
        if (attrs) {
          let menu = {
            webdisk: attrs['WEBDISK'] ?? 1,
            whisperSupport: attrs['POSTIT'] ?? 1
          };
          response.menu = menu;
        }
      } else if (element.name == 'AGREEITEM') {
        const agreeElements = element.elements;
        if (agreeElements) {
          let agreeList = [];
          agreeElements.forEach(agree => {
            if (agree.name == 'ITEM') {
              if (agree.elements) {
                const attrs = agree.attributes;
                let item = {
                  enable: attrs['ENABLE'],
                  name: agree.elements[0].text ?? ''
                };
                agreeList.push(item);
              }
            }
          });
          response.agrees = agreeList;
        }
      } else if (element.name == 'SUPPORT') {
        if (element.elements) {
          response.supportAPIList = element.elements[0].text ?? '';
        }
      } else if (element.name == 'PRIVILEDGE') {
        const attrs = element.attributes;
        if (attrs) {
          let priviledge = {
            roomcreate: attrs['ROOMCREATE'] ?? 1,
            roominvite: attrs['ROOMINVITE'] ?? 1,
            roomout: attrs['ROOMOUT'] ?? 1
          };
          response.priviledge = priviledge;
        }
      } else if (element.name == 'USERLIST') {
        response.apiID = 'USERLIST';
        let listGroup = [];
        let listUser = [];
        const userListElements = element.elements;
        if (userListElements) {
          userListElements.forEach(userElement => {
            if (userElement.name == 'DEPT') {
              const deptListElement = userElement.elements;
              if (deptListElement) {
                let branchDept = null;
                let manageDept = null;
                let deptList = [];
                deptListElement.forEach(dept => {
                  if (dept.name == 'MYDEPT') {
                    const attrs = dept.attributes;
                    if (attrs) {
                      const key = attrs['KEY'];
                      if (key) {
                        deptList.push(key);
                      }
                    }
                  } else if (dept.name == 'MANAGEDEPT') {
                    const attrs = dept.attributes;
                    if (attrs) {
                      manageDept = attrs['KEY'] ?? '';
                    }
                  } else if (dept.name == 'BRANCHDEPT') {
                    const attrs = dept.attributes;
                    if (attrs) {
                      branchDept = attrs['KEY'] ?? '';
                    }
                  }
                });
                response.deptList = deptList;
                response.manageDept = manageDept;
                response.branchDept = branchDept;
              }
            } else if (userElement.name == 'FAV') {
              const attrs = userElement.attributes;
              if (attrs) {
                let group = {
                  groupKey: attrs['KEY'],
                  groupAlias: attrs['NAME'],
                  groupShortName: attrs['SHORTNAME'],
                  groupFullName: attrs['LONGNAME'],
                  groupParentKey: attrs['PKEY'] ?? '',
                  groupLang: '',
                  groupType: TalkConstants.GROUP_TYPE_USER,
                  favorite: true
                };
                listGroup.push(group);
              }
            } else if (
              userElement.name == 'DIR' ||
              userElement.name == 'DIRFAV'
            ) {
              const attrs = userElement.attributes;
              if (attrs) {
                let group = {
                  groupHeader: attrs['HEADER'] ?? '',
                  groupKey: attrs['KEY'] ?? '',
                  groupAlias: attrs['NAME'],
                  groupShortName: attrs['SHORTNAME'],
                  groupFullName: attrs['LONGNAME'],
                  groupParentKey: attrs['PKEY'] ?? '',
                  groupLang: '',
                  groupType: TalkConstants.GROUP_TYPE_ADMIN,
                  favorite: userElement.name == 'DIRFAV' ? true : false
                };
                listGroup.push(group);
              }
            } else if (
              userElement.name == 'ITEM' ||
              userElement.name == 'ITEMFAV'
            ) {
              const attrs = userElement.attributes;
              if (attrs) {
                const localPhone = attrs['LOCALPHONE'] ?? '';
                const phone = attrs['PHONE'] ?? '';
                let userLocalPhoneValue = phone;
                if (TextUtils.isNotEmpty(localPhone)) {
                  userLocalPhoneValue =
                    userLocalPhoneValue + ' (' + localPhone + ')';
                }

                let contact = {
                  userEmail: attrs['EMAIL'] ?? '',
                  userKey: attrs['KEY'],
                  userLocalPhone: userLocalPhoneValue,
                  loginType: TalkConstants.loginTypeFromString(
                    attrs['LOGINTYPE'] ?? ''
                  ),
                  mobilephone: attrs['MOBILEPHONE'] ?? '',
                  userAlias: attrs['NAME'] ?? '',
                  nickname: attrs['NICKNAME'] ?? '',
                  shortname: attrs['SHORTNAME'] ?? '',
                  order: parseInt(attrs['ORDER']) ?? 0,
                  phototime: parseInt(attrs['PHOTOTIME']) ?? 0,
                  parentKey: attrs['PKEY'] ?? '',
                  position: attrs['POSITION'] ?? '',
                  sex: parseInt(attrs['SEX']) ?? 0,
                  status: parseInt(attrs['STATUS']) ?? 0,
                  fullname: attrs['LONGNAME'] ?? '',
                  sipphone: attrs['SIP'] ?? '',
                  userLang: '',
                  hiddenUser: false,
                  favorite: userElement.name == 'ITEMFAV' ? true : false
                };

                let chosunText =
                  Utils.decomposeSylLabels(contact.userAlias) +
                  Utils.decomposeSylLabels(contact.shortname) +
                  Utils.decomposeSylLabels(contact.fullname);
                contact.chosunText = chosunText;
                listUser.push(contact);
              }
            }
          });
        }
        response.listGroup = listGroup;
        response.listUser = listUser;
      } else if (element.name == 'USERMODINFO') {
        response.apiID = 'USERMODINFO';
        response.nicknames = [];
        response.photos = [];
        const attrs = element.attributes;
        if (attrs) {
          const last1 = attrs['LAST_MODTIME'];
          const last2 = attrs['LAST_USERMODTIME'];
          let realLast = last1 ? last1 : last2;
          response.lastModTime = TalkConstants.timeFromTimeStamp(realLast);
        }
      } else if (element.name == 'NICKNAMES') {
        if (response.apiID == 'USERMODINFO') {
          const nickNameList = element.elements;
          if (nickNameList) {
            nickNameList.forEach(item => {
              if (item.name == 'ITEM') {
                const iAttrs = item.attributes;
                if (iAttrs) {
                  response.nicknames.push({
                    userKey: iAttrs['USERKEY'],
                    value: iAttrs['NICKNAME']
                  });
                }
              }
            });
          }
        }
      } else if (element.name == 'PHOTOTIMES') {
        if (response.apiID == 'USERMODINFO') {
          const photoTimeList = element.elements;
          if (photoTimeList) {
            photoTimeList.forEach(item => {
              if (item.name == 'ITEM') {
                const iAttrs = item.attributes;
                if (iAttrs) {
                  response.photos.push({
                    userKey: iAttrs['USERKEY'],
                    value: iAttrs['TIMESTAMP']
                  });
                }
              }
            });
          }
        }
      } else if (element.name == 'OLDPROTOCOL') {
        response.apiID = 'OLDPROTOCOL';
      } else if (element.name == 'CLOSE') {
        response.apiID = 'SOCKET_CLOSED';
        const attrs = element.attributes;
        if (attrs) {
          response.status = attrs['STATUS'] ?? 1;
        } else {
          response.status = 0;
        }
      } else if (element.name == 'STATUS') {
        response.apiID = 'STATUS';
        const attrs = element.attributes;
        if (attrs) {
          response.loginType = TalkConstants.loginTypeFromString(
            attrs['LOGINTYPE']
          );
          response.status = attrs['STATE'];
          response.userKey = attrs['USERKEY'];
        }
      } else if (element.name == 'NICKNAME') {
        response.apiID = 'NICKNAME';
        const attrs = element.attributes;
        if (attrs) {
          response.nickName = attrs['NAME'];
          response.userKey = attrs['USERKEY'];
        }
      } else if (element.name == 'ALERTROOM') {
        response.apiID = 'ALERTROOM';
        const attrs = element.attributes;
        if (attrs) {
          response.enable = attrs['ENABLE'] ?? 1;
          response.result = attrs['RESULT'];
          response.roomKey = attrs['ROOMKEY'];
        }
      } else if (element.name == 'FOLDERDEL') {
        response.apiID = 'FOLDERDEL';
        const attrs = element.attributes;
        if (attrs) {
          response.result = attrs['RESULT'] ?? 0;
          response.folderKey = attrs['FOLDERKEY'] ?? '';
        }
      } else if (element.name == 'FOLDERCREATE') {
        response.apiID = 'FOLDERCREATE';
        const attrs = element.attributes;
        if (attrs) {
          response.result = attrs['RESULT'] ?? 0;
          response.group = {
            header: '',
            groupKey: attrs['FOLDERKEY'] ?? '',
            name: attrs['NAME'] ?? '',
            shortName: '',
            pKey: attrs['PFOLDERKEY'] ?? ''
          };
        }
      } else if (element.name == 'USERDEL') {
        response.apiID = 'USERDEL';
        const attrs = element.attributes;
        if (attrs) {
          response.result = attrs['RESULT'] ?? 0;
          response.folderKey = attrs['FOLDERKEY'] ?? '';
          response.userKeyList = attrs['USERKEY'] ?? '';
          response.groupKeyList = attrs['GROUPKEY'] ?? '';
        }
      } else if (element.name == 'USERUPDATE') {
        response.apiID = 'USERUPDATE';
        const attrs = element.attributes;
        if (attrs) {
          response.result = attrs['RESULT'] ?? 0;
          response.folderKey = attrs['FOLDERKEY'] ?? '';
          response.userKeyList = attrs['USERKEY'] ?? '';
          response.groupKeyList = attrs['GROUPKEY'] ?? '';
        }
      } else if (element.name == 'USERADD') {
        response.apiID = 'USERADD';
        const attrs = element.attributes;
        if (attrs) {
          response.result = attrs['RESULT'] ?? 0;
          response.folderKey = attrs['FOLDERKEY'] ?? '';
          response.userKeyList = attrs['USERKEY'] ?? '';
          response.groupKeyList = attrs['GROUPKEY'] ?? '';
        }
      } else if (element.name == 'WEBAUTOLOGIN') {
        response.apiID = 'WEBAUTOLOGIN';
        const attrs = element.attributes;
        if (attrs) {
          response.result = attrs['RESULT'] ?? 0;
          response.webLoginKey = attrs['KEY'] ?? '';
        }
      }
    });

    if (response.apiID == 'LOGIN') {
      PacketListener.onReceivedLoginPacket(messengerCenter, response);
    } else if (response.apiID == 'RELOGIN') {
      PacketListener.onReceivedReLoginPacket(messengerCenter, response);
    } else if (response.apiID == 'LOGINREQ') {
      PacketListener.onReceivedLoginRequestAccessControlPacket(
        messengerCenter,
        response
      );
    } else if (response.apiID == 'USERLIST') {
      PacketListener.onReceivedUserListPacket(messengerCenter, response);
    } else if (response.apiID == 'OLDPROTOCOL') {
      PacketListener.onReceivedOldProtocolPacket(messengerCenter, response);
    } else if (response.apiID == 'SOCKET_CLOSED') {
      PacketListener.onReceivedSocketClosedPacket(messengerCenter, response);
    } else if (response.apiID == 'STATUS') {
      PacketListener.onReceivedUserStatusPacket(messengerCenter, response);
    } else if (response.apiID == 'NICKNAME') {
      PacketListener.onReceivedUserNickNamePacket(messengerCenter, response);
    } else if (response.apiID == 'USERMODINFO') {
      PacketListener.onReceivedUserModInfoPacket(messengerCenter, response);
    } else if (response.apiID == 'ALERTROOM') {
      PacketListener.onReceivedAlertRoomPacket(messengerCenter, response);
    } else if (response.apiID == 'FOLDERDEL') {
      PacketListener.onReceivedFolderDelPacket(messengerCenter, response);
    } else if (response.apiID == 'FOLDERCREATE') {
      PacketListener.onReceivedFolderCreatePacket(messengerCenter, response);
    } else if (response.apiID == 'USERDEL') {
      PacketListener.onReceivedUserDelPacket(messengerCenter, response);
    } else if (response.apiID == 'USERUPDATE') {
      PacketListener.onReceivedUserUpdatePacket(messengerCenter, response);
    } else if (response.apiID == 'USERADD') {
      PacketListener.onReceivedUserAddPacket(messengerCenter, response);
    } else if (response.apiID == 'WEBAUTOLOGIN') {
      PacketListener.onReceivedWebAutoLoginPacket(messengerCenter, response);
    }
  },
  processAlarm(messengerCenter: MessengerCenter, rootElement) {
    let response = {};
    rootElement.forEach(element => {
      if (element.name == 'LIVE') {
        response.apiID = 'LIVE';
        response.result = 0;
        const attrs = element.attributes;
        if (attrs) {
          const statusCode = attrs['RESULT'] ?? 0;
          if (statusCode >= 0) {
            response.statusList = TalkConstants.splitStatusLoginVer2(
              attrs['STATUS'] ?? ''
            );
          }
          response.result = statusCode;
        }
      } else if (element.name == 'TIMECARD') {
        response.apiID = 'TIMECARD';
        const attrs = element.attributes;
        if (attrs) {
          response.statusStr = attrs['STATUS'] ?? '';
        } else {
          response.statusStr = '';
        }
      } else if (element.name == 'FAVORITE') {
        response.apiID = 'FAVORITE';
        response.statusCode = 0;
        const attrs = element.attributes;
        if (attrs) {
          const statusCode = attrs['RESULT'] ?? 0;
          response.statusCode = statusCode;
          response.key = attrs['KEY'] ?? '';
          response.target = attrs['TARGET'] ?? '';
          response.mode = attrs['MODE'] ?? '';
        }
      } else if (element.name == 'COMPANYGROUP') {
        response.apiID = 'COMPANYGROUP';
        const attrs = element.attributes;
        if (attrs) {
          response.mode = attrs['MODE'] ?? '';
          if (response.mode == 'ADD') {
            response.group = {
              header: attrs['HEADER'] ?? '',
              groupKey: attrs['KEY'] ?? '',
              name: attrs['NAME'] ?? '',
              shortName: attrs['SHORTNAME'] ?? '',
              pKey: attrs['PKEY'] ?? ''
            };
          } else if (response.mode == 'DEL') {
            response.groupKey = attrs['KEY'] ?? '';
          } else if (response.mode == 'MOD') {
            let group = {
              groupHeader: attrs['HEADER'] ?? '',
              groupKey: attrs['KEY'] ?? '',
              groupAlias: attrs['NAME'],
              groupShortName: attrs['SHORTNAME'],
              groupFullName: attrs['LONGNAME'],
              groupParentKey: attrs['PKEY'] ?? '',
              groupLang: attrs['COUNTRY'] ?? '',
              groupType: TalkConstants.GROUP_TYPE_ADMIN,
              favorite: false
            };
            response.group = group;
          }
        }
      } else if (element.name == 'COMPANYUSER') {
        response.apiID = 'COMPANYUSER';
        const attrs = element.attributes;
        if (attrs) {
          response.mode = attrs['MODE'] ?? '';
          if (response.mode == 'ADD') {
            const localPhone = attrs['LOCALPHONE'] ?? '';
            const phone = attrs['PHONE'] ?? '';
            let userLocalPhoneValue = phone;
            if (TextUtils.isNotEmpty(localPhone)) {
              userLocalPhoneValue =
                userLocalPhoneValue + ' (' + localPhone + ')';
            }

            const user = {
              userKey: attrs['KEY'] ?? '',
              name: attrs['NAME'] ?? '',
              email: attrs['EMAIL'] ?? '',
              localPhone: userLocalPhoneValue,
              nickName: attrs['NICKNAME'] ?? '',
              shortName: attrs['SHORTNAME'] ?? '',
              order: attrs['ORDER'] ?? 0,
              pKey: attrs['PKEY'] ?? '',
              position: attrs['POSITION'] ?? '',
              sex: attrs['SEX'] ?? 0,
              status: attrs['STATUS'] ?? 0,
              photoTime: attrs['PHOTOTIME'] ?? 0,
              sip: attrs['SIP'] ?? ''
            };

            let chosunText =
              Utils.decomposeSylLabels(user.name) +
              Utils.decomposeSylLabels(user.shortname);
            user.chosunText = chosunText;
            response.user = user;
          } else if (response.mode == 'DEL') {
            response.userKey = attrs['KEY'] ?? '';
          } else if (response.mode == 'MOD') {
            const localPhone = attrs['LOCALPHONE'] ?? '';
            const phone = attrs['PHONE'] ?? '';
            let userLocalPhoneValue = phone;
            if (TextUtils.isNotEmpty(localPhone)) {
              userLocalPhoneValue =
                userLocalPhoneValue + ' (' + localPhone + ')';
            }

            let contact = {
              userEmail: attrs['EMAIL'] ?? '',
              userKey: attrs['KEY'],
              userLocalPhone: userLocalPhoneValue,
              loginType: TalkConstants.loginTypeFromString(
                attrs['LOGINTYPE'] ?? ''
              ),
              mobilephone: attrs['MOBILEPHONE'] ?? '',
              userAlias: attrs['NAME'] ?? '',
              nickname: attrs['NICKNAME'] ?? '',
              shortname: attrs['SHORTNAME'] ?? '',
              order: parseInt(attrs['ORDER']) ?? 0,
              phototime: parseInt(attrs['PHOTOTIME']) ?? 0,
              parentKey: attrs['PKEY'] ?? '',
              position: attrs['POSITION'] ?? '',
              sex: parseInt(attrs['SEX']) ?? 0,
              status: attrs['STATUS'] ?? 0,
              fullname: attrs['LONGNAME'] ?? '',
              sipphone: attrs['SIP'] ?? '',
              userLang: attrs['COUNTRY'] ?? '',
              hiddenUser: false,
              favorite: false
            };

            let chosunText =
              Utils.decomposeSylLabels(contact.userAlias) +
              Utils.decomposeSylLabels(contact.shortname) +
              Utils.decomposeSylLabels(contact.fullname);
            contact.chosunText = chosunText;
            response.user = contact;
          }
        }
      } else if (element.name == 'CHATROOMBOARDALARM') {
        response.apiID = 'CHATROOMBOARDALARM';
        const attrs = element.attributes;
        if (attrs) {
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.message = {
            userKey: attrs['USERKEY'] ?? '',
            numberKey: attrs['NUMBER'] ?? '0',
            rawCreateDate: attrs['REGTIME'],
            createDate: TalkConstants.timeFromTimeStampSafe(attrs['REGTIME']),
            clientKey: attrs['BOARDKEY'] ?? '',
            emoticon: '',
            msgBody: attrs['CONTENT'] ?? '',
            msgStyle: attrs['NOTI'] ?? 0
          };
        }
      } else if (element.name == 'SIPHANGUP') {
        response.apiID = 'SIPHANGUP';
        const attrs = element.attributes;
        if (attrs) {
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.fromKey = attrs['FROM_USERKEY'] ?? '';
          response.sipKey = attrs['SIPKEY'] ?? '';
          response.toKey = attrs['TO_USERKEY'] ?? '';
          response.reason = attrs['REASON'] ?? '';
          response.sipNumber = attrs['SIPNUMBER'];
        }
      } else if (element.name == 'SIPCLOSE') {
        response.apiID = 'SIPCLOSE';
        const attrs = element.attributes;
        if (attrs) {
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.fromKey = attrs['FROM_USERKEY'] ?? '';
          response.sipKey = attrs['SIPKEY'] ?? '';
          response.toKey = attrs['TO_USERKEY'] ?? '';
        }
      } else if (element.name == 'SIPANSWER') {
        response.apiID = 'SIPANSWER';
        const attrs = element.attributes;
        if (attrs) {
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.fromKey = attrs['FROM_USERKEY'] ?? '';
          response.sipKey = attrs['SIPKEY'] ?? '';
          response.toKey = attrs['TO_USERKEY'] ?? '';
          response.sipNumber = attrs['SIPNUMBER'];
        }
      } else if (element.name == 'SIPRES') {
        response.apiID = 'SIPRES';
        const attrs = element.attributes;
        if (attrs) {
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.fromKey = attrs['FROM_USERKEY'] ?? '';
          response.numberKey = attrs['NUMBER'] ?? '';
          response.sipKey = attrs['SIPKEY'] ?? '';
          response.timeStamp = attrs['TIMESTAMP'] ?? '';
          response.toKey = attrs['TO_USERKEY'] ?? '';
          response.video = attrs['VIDEO'] ?? 0;
          response.clientKey = attrs['CLIENT_KEY'];
        }
      } else if (element.name == 'SIPRESULT') {
        response.apiID = 'SIPRESULT';
        const attrs = element.attributes;
        if (attrs) {
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.fromKey = attrs['FROM_USERKEY'] ?? '';
          response.numberKey = attrs['NUMBER'] ?? '';
          response.sipKey = attrs['SIPKEY'] ?? '';
          response.timeStamp = attrs['TIMESTAMP'] ?? '';
          response.toKey = attrs['TO_USERKEY'] ?? '';
          response.video = attrs['VIDEO'] ?? 0;
          response.result = attrs['RESULT'] ?? 0;
          response.sipNumber = attrs['SIPNUMBER'];
        }
      } else if (element.name == 'SIP') {
        response.apiID = 'SIP';
        const attrs = element.attributes;
        if (attrs) {
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.fromKey = attrs['FROM_USERKEY'] ?? '';
          response.numberKey = attrs['NUMBER'] ?? '';
          response.sipKey = attrs['SIPKEY'] ?? '';
          response.timeStamp = attrs['TIMESTAMP'] ?? '';
          response.toKey = attrs['TO_USERKEY'] ?? '';
          response.video = attrs['VIDEO'] ?? 0;
        }
      } else if (element.name == 'DOC') {
        if (!response.apiID) {
          response.apiID = 'DOC';
          response.whisper = {};
          response.mail = {};
          response.approval = {};
          response.task = {};
          response.todo = {};
          response.circular = {};
          response.board = {};
        }
        if (response.whisper) {
          const attrs = element.attributes;
          if (attrs) {
            const name = attrs['NAME'] ?? '';
            if (name == 'WHISPER') {
              response.whisper.date = TalkConstants.timeFromTimeStampSafe(
                attrs['DATE'] ?? ''
              );
              response.whisper.docId = attrs['DOCUID'] ?? '';
              response.whisper.newCount = attrs['NEW'] ?? 0;
            } else if (name == 'MAIL') {
              response.mail.newCount = attrs['NEW'] ?? 0;
            } else if (name == 'APPROVAL') {
              response.approval.newCount = attrs['NEW'] ?? 0;
            } else if (name == 'TASK') {
              response.task.newCount = attrs['NEW'] ?? 0;
            } else if (name == 'TODO') {
              response.todo.newCount = attrs['NEW'] ?? 0;
            } else if (name == 'CIRCULAR') {
              response.circular.newCount = attrs['NEW'] ?? 0;
            } else if (name == 'BOARD') {
              response.board.newCount = attrs['NEW'] ?? 0;
            }
          }
        }
      } else if (element.name == 'MEMOREVDATA') {
        response.apiID = 'MEMOREVDATA';
        const attrs = element.attributes;
        if (attrs) {
          response.contentMemo = attrs['MEMO'] ?? '';
          response.contentType = attrs['CONTENT_TYPE'] ?? '';
          response.cId = attrs['INDEX'] ?? '';
          response.sendTime = attrs['SENDTIME'] ?? '';
          const date = new Date(response.sendTime);
          response.date = date;
          response.userKey = attrs['USERKEY'] ?? '';
        }
      }
    });

    if (response.apiID == 'LIVE') {
      PacketListener.onReceivedLivePacket(messengerCenter, response);
    } else if (response.apiID == 'TIMECARD') {
      PacketListener.onReceivedTimeCardPacket(messengerCenter, response);
    } else if (response.apiID == 'FAVORITE') {
      PacketListener.onReceivedFavoritePacket(messengerCenter, response);
    } else if (response.apiID == 'COMPANYGROUP') {
      PacketListener.onReceivedCompanyGroupPacket(messengerCenter, response);
    } else if (response.apiID == 'COMPANYUSER') {
      PacketListener.onReceivedCompanyUserPacket(messengerCenter, response);
    } else if (response.apiID == 'CHATROOMBOARDALARM') {
      PacketListener.onReceivedChatBoardRoomPacket(messengerCenter, response);
    } else if (response.apiID == 'SIPHANGUP' || response.apiID == 'SIPCLOSE') {
      PacketListener.onReceivedSipEndPacket(messengerCenter, response);
    } else if (response.apiID == 'SIPRES') {
      PacketListener.onReceivedSipResPacket(messengerCenter, response);
    } else if (response.apiID == 'SIPANSWER') {
      PacketListener.onReceivedSipAnswerPacket(messengerCenter, response);
    } else if (response.apiID == 'SIPRESULT') {
      PacketListener.onReceivedSipResultPacket(messengerCenter, response);
    } else if (response.apiID == 'SIP') {
      PacketListener.onReceivedIncomingPacket(messengerCenter, response);
    } else if (response.apiID == 'DOC') {
      PacketListener.onReceivedGroupwareCountPacket(messengerCenter, response);
    } else if (response.apiID == 'MEMOREVDATA') {
      PacketListener.onReceivedWhisperPacket(messengerCenter, response);
    }
  },
  processChat(messengerCenter: MessengerCenter, rootElement) {
    let response = {};
    rootElement.forEach(element => {
      if (element.name == 'ROOMLIST') {
        response.apiID = 'ROOMLIST';
        const attrs = element.attributes;
        if (attrs) {
          const oldRoomTime = attrs['OLDROOMTIME'] ?? 0;
          response.oldRoomTime = oldRoomTime;
        } else {
          response.oldRoomTime = 0;
        }

        response.lastRoomListUpdate = 0;
        const roomList = [];
        const roomListElements = element.elements;
        if (roomListElements) {
          roomListElements.forEach(roomElement => {
            if (roomElement.name == 'ROOM') {
              const attrs = roomElement.attributes;
              if (attrs) {
                let room = {
                  leftRoom: false,
                  roomKey: attrs['ROOMKEY'],
                  name: attrs['NAME'] ?? '',
                  users: TalkConstants.splitUserKey(attrs['USERS']),
                  lastMsgNumber: attrs['LAST_MESSAGE_NUMBER'] ?? '',
                  lastSenderUserKey: attrs['SENDERKEY'] ?? '',
                  lastTime: TalkConstants.timeFromTimeStamp(attrs['LASTTIME']),
                  lastMsgBody: attrs['MSG'] ?? '',
                  iRead: attrs['IREAD'] ?? 0,
                  unreadMessageServer: attrs['UNREAD'] ?? 0,
                  liveChat: attrs['LIVECHAT'] ?? 0,
                  guestChat: attrs['GUESTCHAT'] ?? 0,
                  alias: attrs['ALIAS'] ?? '',
                  aliasPrivate: attrs['PRIVATEALIAS'] ?? '',
                  aliasPublic: attrs['PUBLICALIAS'] ?? '',
                  roomTypeGU: attrs['TYPE'] ?? '',
                  pushAlert: attrs['ALERTROOM'] ?? 0,
                  fireRoom: attrs['FIRECHAT'] ?? 0,
                  favoriteRoom: attrs['FAVORITEROOM'] ?? 0,
                  groupCallKey: attrs['GROUPSIPNUMBER'] ?? '',
                  fireRoomCode: attrs['FIREROOMCODE'] ?? '',
                  fireRoomGPS: attrs['FIREROOMGPS'] ?? '',
                  roomTag: attrs['TAG'] ?? '',
                  lastMsgDeleted: attrs['IS_DELETED'] ?? 0
                };

                roomList.push(room);

                let currentStatusTime = response.lastRoomListUpdate;
                let statusTime = attrs['STATUSTIME'] ?? 0;
                if (currentStatusTime < statusTime) {
                  response.lastRoomListUpdate = statusTime;
                }
              }
            } else if (roomElement.name == 'LEAVED') {
              const attrs = roomElement.attributes;
              if (attrs) {
                let room = {
                  leftRoom: true,
                  roomKey: attrs['ROOMKEY']
                };
                roomList.push(room);
                let currentStatusTime = response.lastRoomListUpdate;
                let statusTime = attrs['STATUSTIME'] ?? 0;
                if (currentStatusTime < statusTime) {
                  response.lastRoomListUpdate = statusTime;
                }
              }
            }
          });
        }
        response.roomList = roomList;
      } else if (element.name == 'GETTALK') {
        response.apiID = 'GETTALK';
        const attrs = element.attributes;
        if (attrs) {
          response.clientKey = attrs['CLIENTKEY'] ?? '';
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.talkCount = attrs['TALKCOUNT'] ?? 0;
        } else {
          response.clientKey = '';
          response.roomKey = '';
          response.talkCount = 0;
        }

        let firstTimeStamp = '';
        const needRequestThumbnailList = [];
        const messageList = [];
        const messageListElements = element.elements;
        if (messageListElements) {
          messageListElements.forEach(messageElement => {
            if (messageElement.name == 'TALK') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let messageReadedKey = attrs['READED'] ?? '';
                if (TextUtils.isNotEmpty(messageReadedKey)) {
                  messageReadedKey = '|' + messageReadedKey + '|';
                }

                let timeStamp = attrs['TIME'] ?? '';
                if (TextUtils.isEmpty(firstTimeStamp)) {
                  firstTimeStamp = timeStamp;
                }

                let message = {
                  userKey: attrs['USERKEY'] ?? '',
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
                  messageType: TalkConstants.MessageType.MESSAGE_TYPE_TEXT,
                  numberKey: attrs['NUMBER'] ?? '',
                  messageReadedKey: messageReadedKey,
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  clientKey: attrs['CLIENTKEY'] ?? '',
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp),
                  messageEmoticon: attrs['EMOTICON'] ?? '',
                  isBookmark: attrs['BOOKMARK'] ?? 0,
                  bookmarkKey: attrs['BOOKMARKKEY'] ?? '',
                  isDeleted: attrs['IS_DELETED'] ?? 0
                };

                if (messageElement.elements) {
                  message.messageBody = messageElement.elements[0].text;
                } else {
                  message.messageBody = '';
                }
                messageList.push(message);
              }
            } else if (messageElement.name == 'NEWBOARD') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let message = {
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
                  numberKey: attrs['NUMBER'] ?? '',
                  clientKey: attrs['BOARDKEY'] ?? ''
                };

                if (
                  TextUtils.isNotEmpty(message.numberKey) &&
                  message.numberKey != 'null' &&
                  TextUtils.isNotEmpty(message.clientKey) &&
                  message.clientKey != 'null'
                ) {
                  message.userKey = attrs['USERKEY'] ?? '';
                  let timeStamp = attrs['TIME'] ?? '';
                  if (TextUtils.isEmpty(firstTimeStamp)) {
                    firstTimeStamp = timeStamp;
                  }
                  message.createDate = TalkConstants.timeFromTimeStamp(
                    timeStamp
                  );
                  message.messageType =
                    TalkConstants.MessageType.MESSAGE_TYPE_BOARD_INFO;
                  message.messageStyle = attrs['NOTI'] ?? '0';
                  if (messageElement.elements) {
                    message.messageBody = messageElement.elements[0].text;
                  } else {
                    message.messageBody = '';
                  }
                  messageList.push(message);
                }
              }
            } else if (messageElement.name == 'SIPSEND') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let messageReadedKey = attrs['READED'] ?? '';
                if (TextUtils.isNotEmpty(messageReadedKey)) {
                  messageReadedKey = '|' + messageReadedKey + '|';
                }

                let timeStamp = attrs['TIME'] ?? '';
                if (TextUtils.isEmpty(firstTimeStamp)) {
                  firstTimeStamp = timeStamp;
                }

                let sipType = attrs['SIPTYPE'] ?? '';
                let sipDuration = attrs['DURATION'] ?? 0;
                let sipStatus = TalkConstants.getMessageSipStatusFromString(
                  sipType,
                  false,
                  sipDuration
                );
                let useOtherUserKey = false;
                if (
                  sipStatus ==
                  TalkConstants.MessageSipConstant.SIP_STATUS_CALLING
                ) {
                  useOtherUserKey = false;
                }

                let message = {
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
                  sipDuration: sipDuration,
                  sipStatus: sipStatus,
                  sipType: TalkConstants.getMessageSipTypeFromString(
                    attrs['VIDEO'] ?? ''
                  ),
                  sipKey: attrs['SIPKEY'] ?? '',
                  sipOtherUserKey: attrs['REVUSERKEY'] ?? '',
                  messageType: TalkConstants.MessageType.MESSAGE_TYPE_CALL_LOG,
                  useOtherUserKey: useOtherUserKey,
                  numberKey: attrs['NUMBER'] ?? '',
                  messageReadedKey: messageReadedKey,
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  clientKey: attrs['CLIENTKEY'] ?? '',
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp)
                };

                if (messageElement.elements) {
                  message.messageBody = messageElement.elements[0].text;
                } else {
                  message.messageBody = '';
                }
                messageList.push(message);
              }
            } else if (messageElement.name == 'SIPRECV') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let messageReadedKey = attrs['READED'] ?? '';
                if (TextUtils.isNotEmpty(messageReadedKey)) {
                  messageReadedKey = '|' + messageReadedKey + '|';
                }

                let timeStamp = attrs['TIME'] ?? '';
                if (TextUtils.isEmpty(firstTimeStamp)) {
                  firstTimeStamp = timeStamp;
                }

                let sipType = attrs['SIPTYPE'] ?? '';
                let sipDuration = attrs['DURATION'] ?? 0;
                let sipStatus = TalkConstants.getMessageSipStatusFromString(
                  sipType,
                  true,
                  sipDuration
                );

                let useOtherUserKey = false;
                if (
                  sipStatus ==
                  TalkConstants.MessageSipConstant.SIP_STATUS_CALLING
                ) {
                  useOtherUserKey = true;
                }

                let message = {
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
                  sipDuration: sipDuration,
                  sipStatus: sipStatus,
                  sipType: TalkConstants.getMessageSipTypeFromString(
                    attrs['VIDEO'] ?? ''
                  ),
                  sipKey: attrs['SIPKEY'] ?? '',
                  sipOtherUserKey: attrs['SENDUSERKEY'] ?? '',
                  messageType: TalkConstants.MessageType.MESSAGE_TYPE_CALL_LOG,
                  useOtherUserKey: useOtherUserKey,
                  numberKey: attrs['NUMBER'] ?? '',
                  messageReadedKey: messageReadedKey,
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  clientKey: attrs['CLIENTKEY'] ?? '',
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp)
                };

                if (messageElement.elements) {
                  message.messageBody = messageElement.elements[0].text;
                } else {
                  message.messageBody = '';
                }
                messageList.push(message);
              }
            } else if (messageElement.name == 'GROUPSIP') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let messageReadedKey = attrs['READED'] ?? '';
                if (TextUtils.isNotEmpty(messageReadedKey)) {
                  messageReadedKey = '|' + messageReadedKey + '|';
                }

                let timeStamp = attrs['TIME'] ?? '';
                if (TextUtils.isEmpty(firstTimeStamp)) {
                  firstTimeStamp = timeStamp;
                }

                let sipType = attrs['GROUPSIPMODE'] ?? '';
                let sipStatus = TalkConstants.getMessageGroupSipStatusFromString(
                  sipType
                );

                let message = {
                  userKey: attrs['USERKEY'] ?? '',
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
                  sipStatus: sipStatus,
                  sipKey: attrs['GROUPSIPNUMBER'] ?? '',
                  sipType: TalkConstants.getMessageSipTypeFromString(
                    attrs['VIDEO'] ?? ''
                  ),
                  messageType:
                    TalkConstants.MessageType.MESSAGE_TYPE_GROUP_CALL_LOG,
                  numberKey: attrs['NUMBER'] ?? '',
                  messageReadedKey: messageReadedKey,
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp)
                };

                if (messageElement.elements) {
                  message.messageBody = messageElement.elements[0].text;
                } else {
                  message.messageBody = '';
                }
                messageList.push(message);
              }
            } else if (messageElement.name == 'ROOMALIAS') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let timeStamp = attrs['TIME'] ?? '';
                if (TextUtils.isEmpty(firstTimeStamp)) {
                  firstTimeStamp = timeStamp;
                }

                let message = {
                  userKey: attrs['USERKEY'] ?? '',
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
                  messageType:
                    TalkConstants.MessageType.MESSAGE_TYPE_ROOM_ALIAS,
                  messageBody: attrs['ALIAS'] ?? '',
                  messageFormat: attrs['NAME'] ?? '',
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp)
                };
                messageList.push(message);
              }
            } else if (messageElement.name == 'ROOMIN') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let timeStamp = attrs['TIME'] ?? '';
                if (TextUtils.isEmpty(firstTimeStamp)) {
                  firstTimeStamp = timeStamp;
                }

                let message = {
                  userKey: attrs['USERKEY'] ?? '',
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
                  messageType: TalkConstants.MessageType.MESSAGE_TYPE_INVITE,
                  messageFormat: attrs['INVITER'] ?? '',
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp)
                };
                messageList.push(message);
              }
            } else if (messageElement.name == 'ROOMOUT') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let timeStamp = attrs['TIME'] ?? '';
                if (TextUtils.isEmpty(firstTimeStamp)) {
                  firstTimeStamp = timeStamp;
                }

                let message = {
                  userKey: attrs['USERKEY'] ?? '',
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
                  messageType: TalkConstants.MessageType.MESSAGE_TYPE_OUT,
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp)
                };
                messageList.push(message);
              }
            } else if (messageElement.name == 'FILESEND') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let messageReadedKey = attrs['READED'] ?? '';
                if (TextUtils.isNotEmpty(messageReadedKey)) {
                  messageReadedKey = '|' + messageReadedKey + '|';
                }

                let timeStamp = attrs['TIME'] ?? '';
                if (TextUtils.isEmpty(firstTimeStamp)) {
                  firstTimeStamp = timeStamp;
                }

                let hasFile = true;
                let strHasFile = attrs['HASFILE'] ?? '';
                if (TextUtils.isEmpty(strHasFile)) {
                  hasFile = true;
                } else {
                  hasFile = parseInt(strHasFile) > 0;
                }

                let fileMessage = {
                  isFileMsg: true,
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp),
                  isBookmark: attrs['BOOKMARK'] ?? 0,
                  bookmarkKey: attrs['BOOKMARKKEY'] ?? '',
                  messageBody: attrs['FILENAME'] ?? '',
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  numberKey: attrs['NUMBER'] ?? '',
                  messageFailed: 1,
                  messageType: TalkConstants.typeMessageFromFileType(
                    attrs['FILETYPE'] ?? ''
                  ),
                  isDeleted: attrs['IS_DELETED'] ?? 0
                };

                let messageDetail = {
                  clouddisk: attrs['CLOUDDISK'] ?? 0,
                  fileName: attrs['FILENAME'] ?? '',
                  fileKey: attrs['FILEKEY'] ?? '',
                  fileSize: attrs['FILESIZE'] ?? 0,
                  fileStatus:
                    TalkConstants.MessageFileStatus
                      .FILE_MSG_FROM_ANOTHER_DEVICE,
                  fileAddress: attrs['IP'] ?? '',
                  recordTime: attrs['RECORDINGTIME'] ?? -1,
                  hasFile: hasFile,
                  filePort: attrs['PORT'] ?? 0
                };
                fileMessage.messageDetail = messageDetail;
                messageList.push(fileMessage);

                //check thumbnail exist
                if (!checkPhotoThumbnailExist(response.roomKey, fileMessage)) {
                  needRequestThumbnailList.push(fileMessage);
                }

                saveThumbnailIfHave(
                  messageElement,
                  response.roomKey,
                  fileMessage.numberKey
                );
              }
            } else if (messageElement.name == 'FILEREV') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let messageReadedKey = attrs['READED'] ?? '';
                if (TextUtils.isNotEmpty(messageReadedKey)) {
                  messageReadedKey = '|' + messageReadedKey + '|';
                }

                let timeStamp = attrs['TIME'] ?? '';
                if (TextUtils.isEmpty(firstTimeStamp)) {
                  firstTimeStamp = timeStamp;
                }

                let hasFile = true;
                let strHasFile = attrs['HASFILE'] ?? '';
                if (TextUtils.isEmpty(strHasFile)) {
                  hasFile = true;
                } else {
                  hasFile = parseInt(strHasFile) > 0;
                }

                let fileMessage = {
                  isFileMsg: true,
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp),
                  isBookmark: attrs['BOOKMARK'] ?? 0,
                  bookmarkKey: attrs['BOOKMARKKEY'] ?? '',
                  userKey: attrs['SENDUSERKEY'] ?? '',
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  numberKey: attrs['NUMBER'] ?? '',
                  messageBody: attrs['FILENAME'] ?? '',
                  messageFailed: 1,
                  messageType: TalkConstants.typeMessageFromFileType(
                    attrs['FILETYPE'] ?? ''
                  ),
                  isDeleted: attrs['IS_DELETED'] ?? 0
                };

                let messageDetail = {
                  clouddisk: attrs['CLOUDDISK'] ?? 0,
                  fileName: attrs['FILENAME'] ?? '',
                  fileKey: attrs['FILEKEY'] ?? '',
                  fileAddress: attrs['IP'] ?? '',
                  filePort: attrs['PORT'] ?? 0,
                  fileSize: attrs['FILESIZE'] ?? 0,
                  fileStatus: TalkConstants.MessageFileStatus.FILE_MSG_NORMAL,
                  recordTime: attrs['RECORDINGTIME'] ?? -1,
                  hasFile: hasFile
                };
                fileMessage.messageDetail = messageDetail;
                messageList.push(fileMessage);

                //check thumbnail exist
                if (!checkPhotoThumbnailExist(response.roomKey, fileMessage)) {
                  needRequestThumbnailList.push(fileMessage);
                }

                saveThumbnailIfHave(
                  messageElement,
                  response.roomKey,
                  fileMessage.numberKey
                );
              }
            } else if (messageElement.name == 'CLOUDREV') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let messageReadedKey = attrs['READED'] ?? '';
                if (TextUtils.isNotEmpty(messageReadedKey)) {
                  messageReadedKey = '|' + messageReadedKey + '|';
                }

                let timeStamp = attrs['TIME'] ?? '';
                if (TextUtils.isEmpty(firstTimeStamp)) {
                  firstTimeStamp = timeStamp;
                }

                let hasFile = true;
                let strHasFile = attrs['HASFILE'] ?? '';
                if (TextUtils.isEmpty(strHasFile)) {
                  hasFile = true;
                } else {
                  hasFile = parseInt(strHasFile) > 0;
                }

                let messageDetail = {
                  fileName: attrs['FILENAME'] ?? '',
                  fileAddress: attrs['URL'] ?? '',
                  fileSize: attrs['FILESIZE'] ?? 0,
                  fileStatus: TalkConstants.MessageFileStatus.FILE_MSG_NORMAL,
                  recordTime: attrs['RECORDINGTIME'] ?? -1,
                  hasFile: hasFile
                };

                let fileType = attrs['FILETYPE'] ?? '';
                if (TextUtils.isNotEmpty(messageDetail.fileName)) {
                  fileType = TalkConstants.getFileExtension(
                    messageDetail.fileName
                  );
                }

                let fileMessage = {
                  isFileMsg: true,
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_CLOUDDISK_ID,
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp),
                  isBookmark: attrs['BOOKMARK'] ?? 0,
                  bookmarkKey: attrs['BOOKMARKKEY'] ?? '',
                  userKey: attrs['SENDUSERKEY'] ?? '',
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  numberKey: attrs['NUMBER'] ?? '',
                  messageBody: attrs['FILENAME'] ?? '',
                  messageFailed: 1,
                  messageType: TalkConstants.typeMessageFromFileType(fileType),
                  isDeleted: attrs['IS_DELETED'] ?? 0
                };

                fileMessage.messageDetail = messageDetail;
                messageList.push(fileMessage);
              }
            } else if (messageElement.name == 'CLOUDSEND') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let messageReadedKey = attrs['READED'] ?? '';
                if (TextUtils.isNotEmpty(messageReadedKey)) {
                  messageReadedKey = '|' + messageReadedKey + '|';
                }

                let timeStamp = attrs['TIME'] ?? '';
                if (TextUtils.isEmpty(firstTimeStamp)) {
                  firstTimeStamp = timeStamp;
                }

                let hasFile = true;
                let strHasFile = attrs['HASFILE'] ?? '';
                if (TextUtils.isEmpty(strHasFile)) {
                  hasFile = true;
                } else {
                  hasFile = parseInt(strHasFile) > 0;
                }

                let messageDetail = {
                  fileName: attrs['FILENAME'] ?? '',
                  fileSize: attrs['FILESIZE'] ?? 0,
                  fileStatus:
                    TalkConstants.MessageFileStatus
                      .FILE_MSG_FROM_ANOTHER_DEVICE,
                  fileAddress: attrs['URL'] ?? '',
                  recordTime: attrs['RECORDINGTIME'] ?? -1,
                  hasFile: hasFile
                };

                let fileType = attrs['FILETYPE'] ?? '';
                if (TextUtils.isNotEmpty(messageDetail.fileName)) {
                  fileType = TalkConstants.getFileExtension(
                    messageDetail.fileName
                  );
                }

                let fileMessage = {
                  isFileMsg: true,
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_CLOUDDISK_ID,
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp),
                  isBookmark: attrs['BOOKMARK'] ?? 0,
                  bookmarkKey: attrs['BOOKMARKKEY'] ?? '',
                  messageBody: attrs['FILENAME'] ?? '',
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  numberKey: attrs['NUMBER'] ?? '',
                  messageFailed: 1,
                  messageType: TalkConstants.typeMessageFromFileType(fileType),
                  isDeleted: attrs['IS_DELETED'] ?? 0
                };

                fileMessage.messageDetail = messageDetail;
                messageList.push(fileMessage);
              }
            } else if (messageElement.name == 'HTTPFILESEND') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let timeStamp = attrs['TIME'] ?? '';
                if (TextUtils.isEmpty(firstTimeStamp)) {
                  firstTimeStamp = timeStamp;
                }

                let hasFile = true;
                let strHasFile = attrs['HASFILE'] ?? '';
                if (TextUtils.isEmpty(strHasFile)) {
                  hasFile = true;
                } else {
                  hasFile = parseInt(strHasFile) > 0;
                }

                let messageDetail = {
                  clouddisk: attrs['CLOUDDISK'] ?? 0,
                  fileKey: attrs['FILEKEY'] ?? '',
                  fileName: attrs['FILENAME'] ?? '',
                  fileSize: attrs['FILESIZE'] ?? 0,
                  httpHostName: attrs['HOSTNAME'] ?? '',
                  httpFileReceiver: attrs['REVUSERKEY'] ?? '',
                  httpServerFileName: attrs['SERVERFILENAME'] ?? '',
                  httpServerFilePath: attrs['SERVERFILEPATH'] ?? '',
                  fileStatus:
                    TalkConstants.MessageFileStatus
                      .FILE_MSG_FROM_ANOTHER_DEVICE,
                  recordTime: attrs['RECORDINGTIME'] ?? -1,
                  hasFile: hasFile
                };

                let fileType = attrs['FILETYPE'] ?? '';
                if (TextUtils.isNotEmpty(messageDetail.fileName)) {
                  fileType = TalkConstants.getFileExtension(
                    messageDetail.fileName
                  );
                }

                let fileMessage = {
                  isFileMsg: true,
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_FAST_HTTP_ID,
                  messageType: TalkConstants.typeMessageFromFileType(fileType),
                  isDeleted: attrs['IS_DELETED'] ?? 0,
                  isBookmark: attrs['BOOKMARK'] ?? 0,
                  bookmarkKey: attrs['BOOKMARKKEY'] ?? '',
                  numberKey: attrs['NUMBER'] ?? '',
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp),
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  messageBody: attrs['FILENAME'] ?? '',
                  messageFailed: 1,
                  clientKey: attrs['CLIENTKEY'] ?? ''
                };
                fileMessage.messageDetail = messageDetail;
                messageList.push(fileMessage);

                //check thumbnail exist
                if (!checkPhotoThumbnailExist(response.roomKey, fileMessage)) {
                  needRequestThumbnailList.push(fileMessage);
                }
                saveThumbnailIfHave(
                  messageElement,
                  response.roomKey,
                  fileMessage.numberKey
                );
              }
            } else if (messageElement.name == 'HTTPFILEREV') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let timeStamp = attrs['TIME'] ?? '';
                if (TextUtils.isEmpty(firstTimeStamp)) {
                  firstTimeStamp = timeStamp;
                }

                let hasFile = true;
                let strHasFile = attrs['HASFILE'] ?? '';
                if (TextUtils.isEmpty(strHasFile)) {
                  hasFile = true;
                } else {
                  hasFile = parseInt(strHasFile) > 0;
                }

                let messageDetail = {
                  clouddisk: attrs['CLOUDDISK'] ?? 0,
                  fileKey: attrs['FILEKEY'] ?? '',
                  fileName: attrs['FILENAME'] ?? '',
                  fileSize: attrs['FILESIZE'] ?? 0,
                  httpHostName: attrs['HOSTNAME'] ?? '',
                  httpServerFileName: attrs['SERVERFILENAME'] ?? '',
                  httpServerFilePath: attrs['SERVERFILEPATH'] ?? '',
                  fileStatus: TalkConstants.MessageFileStatus.FILE_MSG_NORMAL,
                  recordTime: attrs['RECORDINGTIME'] ?? -1,
                  hasFile: hasFile
                };

                let fileType = attrs['FILETYPE'] ?? '';
                if (TextUtils.isNotEmpty(messageDetail.fileName)) {
                  fileType = TalkConstants.getFileExtension(
                    messageDetail.fileName
                  );
                }

                let fileMessage = {
                  isFileMsg: true,
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_FAST_HTTP_ID,
                  messageType: TalkConstants.typeMessageFromFileType(fileType),
                  isDeleted: attrs['IS_DELETED'] ?? 0,
                  isBookmark: attrs['BOOKMARK'] ?? 0,
                  bookmarkKey: attrs['BOOKMARKKEY'] ?? '',
                  numberKey: attrs['NUMBER'] ?? '',
                  userKey: attrs['SENDUSERKEY'] ?? '',
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp),
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  messageBody: attrs['FILENAME'] ?? '',
                  messageFailed: 1
                };
                fileMessage.messageDetail = messageDetail;
                messageList.push(fileMessage);

                //check thumbnail exist
                if (!checkPhotoThumbnailExist(response.roomKey, fileMessage)) {
                  needRequestThumbnailList.push(fileMessage);
                }

                saveThumbnailIfHave(
                  messageElement,
                  response.roomKey,
                  fileMessage.numberKey
                );
              }
            }
          });
        }

        response.firstTimeStamp = firstTimeStamp;
        response.messageList = messageList;
        response.needRequestThumbnailList = needRequestThumbnailList;
      } else if (element.name == 'ROOMCREATE') {
        response.apiID = 'ROOMCREATE';
        const attrs = element.attributes;
        if (attrs) {
          response.result = {
            statusCode: attrs['RESULT'] ?? 0,
            roomKey: attrs['ROOMKEY'] ?? '',
            createDate: Date.now(),
            roomTagName: attrs['TAG'] ?? '',
            roomTypeGU: attrs['TYPE'] ?? '',
            roomAlias: attrs['ALIAS'] ?? ''
          };
        }
      } else if (element.name == 'ROOMINVITE') {
        response.apiID = 'ROOMINVITE';
        const attrs = element.attributes;
        if (attrs) {
          response.result = {
            statusCode: attrs['RESULT'] ?? 0,
            roomKey: attrs['ROOMKEY'] ?? '',
            inviterName: attrs['INVITERNAME'] ?? '',
            createDate: TalkConstants.timeFromTimeStamp(attrs['TIME'] ?? ''),
            whos: attrs['WHOS'] ?? ''
          };
        }
      } else if (element.name == 'ROOMONE') {
        response.apiID = 'ROOMONE';
        const attrs = element.attributes;
        if (attrs) {
          response.result = {
            roomKey: attrs['ROOMKEY'] ?? '',
            guestChat: attrs['GUESTCHAT'] ?? 0,
            liveChat: attrs['LIVECHAT'] ?? 0,
            fireChat: attrs['FIRECHAT'] ?? 0,
            roomAlias: attrs['ALIAS'] ?? '',
            roomTypeGU: attrs['TYPE'] ?? ''
          };

          const whos = [];
          const whosElements = element.elements;
          if (whosElements) {
            whosElements.forEach(who => {
              const attrs = who.attributes;
              if (attrs) {
                let userKey = attrs['WHO'] ?? '';
                if (TextUtils.isNotEmpty(userKey)) {
                  whos.push({
                    userKey: userKey,
                    name: attrs['NAME'] ?? ''
                  });
                }
              }
            });
          }
          response.whos = whos;
        }
      } else if (element.name == 'ALLREAD') {
        response.apiID = 'ALLREAD';
        const attrs = element.attributes;
        if (attrs) {
          response.statusCode = attrs['RESULT'] ?? 0;
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.userKey = attrs['USERKEY'] ?? '';
          response.timeStamp = attrs['TIMESTAMP'] ?? '';
        }
      } else if (element.name == 'MSG') {
        response.apiID = 'MSG';
        const attrs = element.attributes;
        if (attrs) {
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.message = {
            userKey: attrs['USERKEY'] ?? '',
            numberKey: attrs['NUMBER'] ?? '0',
            createDate: TalkConstants.timeFromTimeStamp(attrs['TIME']),
            clientKey: attrs['CLIENTKEY'] ?? '',
            emoticon: attrs['EMOTICON'] ?? ''
          };
          if (element.elements) {
            let contentElement = element.elements[0];
            response.message.msgBody = contentElement
              ? contentElement.text
              : '';
          } else {
            response.message.msgBody = '';
          }
        }
      } else if (element.name == 'TYPING') {
        response.apiID = 'TYPING';
        const attrs = element.attributes;
        if (attrs) {
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.userKey = attrs['USERKEY'] ?? '';
          response.name = attrs['USERNAME'] ?? '';
          response.show = parseInt(attrs['STATUS'] ?? '') > 0;
        }
      } else if (element.name == 'ROOMOUT') {
        response.apiID = 'ROOMOUT';
        const attrs = element.attributes;
        if (attrs) {
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.userKey = attrs['USERKEY'] ?? '';
          response.createDate = TalkConstants.timeFromTimeStamp(
            attrs['TIME'] ?? ''
          );
        }
      } else if (element.name == 'ROOMNAME') {
        response.apiID = 'ROOMNAME';
        const attrs = element.attributes;
        if (attrs) {
          response.statusCode = attrs['RESULT'] ?? 0;
          response.roomKey = attrs['ROOMKEY'] ?? '';

          const hasSendAlias = attrs['HAS_SEND_ALIAS'] ?? 0;
          if (hasSendAlias > 0) {
            response.name = attrs['SENDALIAS'] ?? '';
          } else {
            response.name = attrs['NAME'] ?? '';
          }
          response.private = attrs['PRIVATE'] ?? 1;
        }
      } else if (element.name == 'FAVORITEROOM') {
        response.apiID = 'FAVORITEROOM';
        const attrs = element.attributes;
        if (attrs) {
          response.statusCode = attrs['RESULT'] ?? 0;
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.isAdd = 'ADD' == attrs['MODE'];
        }
      } else if (element.name == 'CANCELMSG') {
        response.apiID = 'CANCELMSG';
        const attrs = element.attributes;
        if (attrs) {
          response.statusCode = attrs['RESULT'] ?? 0;
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.userKey = attrs['USERKEY'] ?? '';
          response.timestamp = TalkConstants.timeFromTimeStamp(
            attrs['TIMESTAMP'] ?? ''
          );
        }
        response.numberKey = attrs['NUMBER'] ?? '';
      } else if (element.name == 'GETFILES') {
        response.apiID = 'GETFILES';

        const attrs = element.attributes;
        if (attrs) {
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.talkCount = attrs['COUNT'] ?? 0;
          response.filterType = attrs['FILTER_TYPE'] ?? 'IMAGE';
          response.requestTime = attrs['LASTTIME'] ?? 0;
        } else {
          response.filterType = '';
          response.roomKey = '';
          response.talkCount = 0;
          response.requestTime = 0;
        }

        let firstTimeStamp = '';
        const messageList = [];

        const messageListElements = element.elements;

        if (messageListElements) {
          messageListElements.forEach(messageElement => {
            if (messageElement.name == 'HTTPFILESEND') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let timeStamp = attrs['TIME'] ?? '';
                if (
                  TextUtils.isEmpty(firstTimeStamp) ||
                  firstTimeStamp > timeStamp
                ) {
                  firstTimeStamp = timeStamp;
                }

                let hasFile = true;
                let strHasFile = attrs['HASFILE'] ?? '';
                if (TextUtils.isEmpty(strHasFile)) {
                  hasFile = true;
                } else {
                  hasFile = parseInt(strHasFile) > 0;
                }

                let messageDetail = {
                  clouddisk: attrs['CLOUDDISK'] ?? 0,
                  fileKey: attrs['FILEKEY'] ?? '',
                  fileName: attrs['FILENAME'] ?? '',
                  fileSize: attrs['FILESIZE'] ?? 0,
                  httpHostName: attrs['HOSTNAME'] ?? '',
                  httpFileReceiver: attrs['REVUSERKEY'] ?? '',
                  httpServerFileName: attrs['SERVERFILENAME'] ?? '',
                  httpServerFilePath: attrs['SERVERFILEPATH'] ?? '',
                  fileStatus:
                    TalkConstants.MessageFileStatus
                      .FILE_MSG_FROM_ANOTHER_DEVICE,
                  recordTime: attrs['RECORDINGTIME'] ?? -1,
                  hasFile: hasFile
                };

                let fileType = attrs['FILETYPE'] ?? '';
                if (TextUtils.isNotEmpty(messageDetail.fileName)) {
                  fileType = TalkConstants.getFileExtension(
                    messageDetail.fileName
                  );
                }

                let fileMessage = {
                  isFileMsg: true,
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_FAST_HTTP_ID,
                  messageType: TalkConstants.typeMessageFromFileType(fileType),
                  isDeleted: attrs['IS_DELETED'] ?? 0,
                  isBookmark: attrs['BOOKMARK'] ?? 0,
                  bookmarkKey: attrs['BOOKMARKKEY'] ?? '',
                  numberKey: attrs['NUMBER'] ?? '',
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp),
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  messageBody: attrs['FILENAME'] ?? '',
                  messageFailed: 1,
                  clientKey: attrs['CLIENTKEY'] ?? ''
                };
                fileMessage.messageDetail = messageDetail;
                messageList.push(fileMessage);

                //check thumbnail exist
                if (!checkPhotoThumbnailExist(response.roomKey, fileMessage)) {
                  needRequestThumbnailList.push(fileMessage);
                }
                saveThumbnailIfHave(
                  messageElement,
                  response.roomKey,
                  fileMessage.numberKey
                );
              }
            } else if (messageElement.name == 'HTTPFILEREV') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let timeStamp = attrs['TIME'] ?? '';
                if (
                  TextUtils.isEmpty(firstTimeStamp) ||
                  firstTimeStamp > timeStamp
                ) {
                  firstTimeStamp = timeStamp;
                }

                let hasFile = true;
                let strHasFile = attrs['HASFILE'] ?? '';
                if (TextUtils.isEmpty(strHasFile)) {
                  hasFile = true;
                } else {
                  hasFile = parseInt(strHasFile) > 0;
                }

                let messageDetail = {
                  clouddisk: attrs['CLOUDDISK'] ?? 0,
                  fileKey: attrs['FILEKEY'] ?? '',
                  fileName: attrs['FILENAME'] ?? '',
                  fileSize: attrs['FILESIZE'] ?? 0,
                  httpHostName: attrs['HOSTNAME'] ?? '',
                  httpServerFileName: attrs['SERVERFILENAME'] ?? '',
                  httpServerFilePath: attrs['SERVERFILEPATH'] ?? '',
                  fileStatus: TalkConstants.MessageFileStatus.FILE_MSG_NORMAL,
                  recordTime: attrs['RECORDINGTIME'] ?? -1,
                  hasFile: hasFile
                };

                let fileType = attrs['FILETYPE'] ?? '';
                if (TextUtils.isNotEmpty(messageDetail.fileName)) {
                  fileType = TalkConstants.getFileExtension(
                    messageDetail.fileName
                  );
                }

                let fileMessage = {
                  isFileMsg: true,
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_FAST_HTTP_ID,
                  messageType: TalkConstants.typeMessageFromFileType(fileType),
                  isDeleted: attrs['IS_DELETED'] ?? 0,
                  isBookmark: attrs['BOOKMARK'] ?? 0,
                  bookmarkKey: attrs['BOOKMARKKEY'] ?? '',
                  numberKey: attrs['NUMBER'] ?? '',
                  userKey: attrs['SENDUSERKEY'] ?? '',
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp),
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  messageBody: attrs['FILENAME'] ?? '',
                  messageFailed: 1
                };
                fileMessage.messageDetail = messageDetail;
                messageList.push(fileMessage);

                //check thumbnail exist
                if (!checkPhotoThumbnailExist(response.roomKey, fileMessage)) {
                  needRequestThumbnailList.push(fileMessage);
                }

                saveThumbnailIfHave(
                  messageElement,
                  response.roomKey,
                  fileMessage.numberKey
                );
              }
            } else if (messageElement.name == 'FILESEND') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let messageReadedKey = attrs['READED'] ?? '';
                if (TextUtils.isNotEmpty(messageReadedKey)) {
                  messageReadedKey = '|' + messageReadedKey + '|';
                }

                let timeStamp = attrs['TIME'] ?? '';
                if (
                  TextUtils.isEmpty(firstTimeStamp) ||
                  firstTimeStamp > timeStamp
                ) {
                  firstTimeStamp = timeStamp;
                }

                let hasFile = true;
                let strHasFile = attrs['HASFILE'] ?? '';
                if (TextUtils.isEmpty(strHasFile)) {
                  hasFile = true;
                } else {
                  hasFile = parseInt(strHasFile) > 0;
                }

                let fileMessage = {
                  isFileMsg: true,
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp),
                  isBookmark: attrs['BOOKMARK'] ?? 0,
                  bookmarkKey: attrs['BOOKMARKKEY'] ?? '',
                  messageBody: attrs['FILENAME'] ?? '',
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  numberKey: attrs['NUMBER'] ?? '',
                  messageFailed: 1,
                  messageType: TalkConstants.typeMessageFromFileType(
                    attrs['FILETYPE'] ?? ''
                  ),
                  isDeleted: attrs['IS_DELETED'] ?? 0
                };

                let messageDetail = {
                  clouddisk: attrs['CLOUDDISK'] ?? 0,
                  fileName: attrs['FILENAME'] ?? '',
                  fileKey: attrs['FILEKEY'] ?? '',
                  fileSize: attrs['FILESIZE'] ?? 0,
                  fileStatus:
                    TalkConstants.MessageFileStatus
                      .FILE_MSG_FROM_ANOTHER_DEVICE,
                  fileAddress: attrs['IP'] ?? '',
                  recordTime: attrs['RECORDINGTIME'] ?? -1,
                  hasFile: hasFile,
                  filePort: attrs['PORT'] ?? 0
                };
                fileMessage.messageDetail = messageDetail;
                messageList.push(fileMessage);

                //check thumbnail exist
                if (!checkPhotoThumbnailExist(response.roomKey, fileMessage)) {
                  needRequestThumbnailList.push(fileMessage);
                }

                saveThumbnailIfHave(
                  messageElement,
                  response.roomKey,
                  fileMessage.numberKey
                );
              }
            } else if (messageElement.name == 'FILEREV') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let messageReadedKey = attrs['READED'] ?? '';
                if (TextUtils.isNotEmpty(messageReadedKey)) {
                  messageReadedKey = '|' + messageReadedKey + '|';
                }

                let timeStamp = attrs['TIME'] ?? '';
                if (
                  TextUtils.isEmpty(firstTimeStamp) ||
                  firstTimeStamp > timeStamp
                ) {
                  firstTimeStamp = timeStamp;
                }

                let hasFile = true;
                let strHasFile = attrs['HASFILE'] ?? '';
                if (TextUtils.isEmpty(strHasFile)) {
                  hasFile = true;
                } else {
                  hasFile = parseInt(strHasFile) > 0;
                }

                let fileMessage = {
                  isFileMsg: true,
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp),
                  isBookmark: attrs['BOOKMARK'] ?? 0,
                  bookmarkKey: attrs['BOOKMARKKEY'] ?? '',
                  userKey: attrs['SENDUSERKEY'] ?? '',
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  numberKey: attrs['NUMBER'] ?? '',
                  messageBody: attrs['FILENAME'] ?? '',
                  messageFailed: 1,
                  messageType: TalkConstants.typeMessageFromFileType(
                    attrs['FILETYPE'] ?? ''
                  ),
                  isDeleted: attrs['IS_DELETED'] ?? 0
                };

                let messageDetail = {
                  clouddisk: attrs['CLOUDDISK'] ?? 0,
                  fileName: attrs['FILENAME'] ?? '',
                  fileKey: attrs['FILEKEY'] ?? '',
                  fileAddress: attrs['IP'] ?? '',
                  filePort: attrs['PORT'] ?? 0,
                  fileSize: attrs['FILESIZE'] ?? 0,
                  fileStatus: TalkConstants.MessageFileStatus.FILE_MSG_NORMAL,
                  recordTime: attrs['RECORDINGTIME'] ?? -1,
                  hasFile: hasFile
                };
                fileMessage.messageDetail = messageDetail;
                messageList.push(fileMessage);

                //check thumbnail exist
                if (!checkPhotoThumbnailExist(response.roomKey, fileMessage)) {
                  needRequestThumbnailList.push(fileMessage);
                }

                saveThumbnailIfHave(
                  messageElement,
                  response.roomKey,
                  fileMessage.numberKey
                );
              }
            } else if (messageElement.name == 'CLOUDREV') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let messageReadedKey = attrs['READED'] ?? '';
                if (TextUtils.isNotEmpty(messageReadedKey)) {
                  messageReadedKey = '|' + messageReadedKey + '|';
                }

                let timeStamp = attrs['TIME'] ?? '';
                if (
                  TextUtils.isEmpty(firstTimeStamp) ||
                  firstTimeStamp > timeStamp
                ) {
                  firstTimeStamp = timeStamp;
                }

                let hasFile = true;
                let strHasFile = attrs['HASFILE'] ?? '';
                if (TextUtils.isEmpty(strHasFile)) {
                  hasFile = true;
                } else {
                  hasFile = parseInt(strHasFile) > 0;
                }

                let messageDetail = {
                  fileName: attrs['FILENAME'] ?? '',
                  fileAddress: attrs['URL'] ?? '',
                  fileSize: attrs['FILESIZE'] ?? 0,
                  fileStatus: TalkConstants.MessageFileStatus.FILE_MSG_NORMAL,
                  recordTime: attrs['RECORDINGTIME'] ?? -1,
                  hasFile: hasFile
                };

                let fileType = attrs['FILETYPE'] ?? '';
                if (TextUtils.isNotEmpty(messageDetail.fileName)) {
                  fileType = TalkConstants.getFileExtension(
                    messageDetail.fileName
                  );
                }

                let fileMessage = {
                  isFileMsg: true,
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_CLOUDDISK_ID,
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp),
                  isBookmark: attrs['BOOKMARK'] ?? 0,
                  bookmarkKey: attrs['BOOKMARKKEY'] ?? '',
                  userKey: attrs['SENDUSERKEY'] ?? '',
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  numberKey: attrs['NUMBER'] ?? '',
                  messageBody: attrs['FILENAME'] ?? '',
                  messageFailed: 1,
                  messageType: TalkConstants.typeMessageFromFileType(fileType),
                  isDeleted: attrs['IS_DELETED'] ?? 0
                };

                fileMessage.messageDetail = messageDetail;
                messageList.push(fileMessage);
              }
            } else if (messageElement.name == 'CLOUDSEND') {
              const attrs = messageElement.attributes;
              if (attrs) {
                let messageReadedKey = attrs['READED'] ?? '';
                if (TextUtils.isNotEmpty(messageReadedKey)) {
                  messageReadedKey = '|' + messageReadedKey + '|';
                }

                let timeStamp = attrs['TIME'] ?? '';
                if (
                  TextUtils.isEmpty(firstTimeStamp) ||
                  firstTimeStamp > timeStamp
                ) {
                  firstTimeStamp = timeStamp;
                }

                let hasFile = true;
                let strHasFile = attrs['HASFILE'] ?? '';
                if (TextUtils.isEmpty(strHasFile)) {
                  hasFile = true;
                } else {
                  hasFile = parseInt(strHasFile) > 0;
                }

                let messageDetail = {
                  fileName: attrs['FILENAME'] ?? '',
                  fileSize: attrs['FILESIZE'] ?? 0,
                  fileStatus:
                    TalkConstants.MessageFileStatus
                      .FILE_MSG_FROM_ANOTHER_DEVICE,
                  fileAddress: attrs['URL'] ?? '',
                  recordTime: attrs['RECORDINGTIME'] ?? -1,
                  hasFile: hasFile
                };

                let fileType = attrs['FILETYPE'] ?? '';
                if (TextUtils.isNotEmpty(messageDetail.fileName)) {
                  fileType = TalkConstants.getFileExtension(
                    messageDetail.fileName
                  );
                }

                let fileMessage = {
                  isFileMsg: true,
                  messageFromThirdPartyID:
                    TalkConstants.MessageThirdParty.MESSAGE_CLOUDDISK_ID,
                  createDate: TalkConstants.timeFromTimeStamp(timeStamp),
                  isBookmark: attrs['BOOKMARK'] ?? 0,
                  bookmarkKey: attrs['BOOKMARKKEY'] ?? '',
                  messageBody: attrs['FILENAME'] ?? '',
                  messageUnreadedKey: attrs['UNREADED'] ?? '',
                  numberKey: attrs['NUMBER'] ?? '',
                  messageFailed: 1,
                  messageType: TalkConstants.typeMessageFromFileType(fileType),
                  isDeleted: attrs['IS_DELETED'] ?? 0
                };

                fileMessage.messageDetail = messageDetail;
                messageList.push(fileMessage);
              }
            }
          });
        }

        response.firstTimeStamp = firstTimeStamp;
        response.messageList = messageList;
      }
    });

    if (response.apiID == 'ROOMLIST') {
      PacketListener.onReceivedRoomListPacket(messengerCenter, response);
    } else if (response.apiID == 'GETTALK') {
      PacketListener.onReceivedGetTalkPacket(messengerCenter, response);
    } else if (response.apiID == 'ROOMCREATE') {
      PacketListener.onReceivedRoomCreatePacket(messengerCenter, response);
    } else if (response.apiID == 'ROOMINVITE') {
      PacketListener.onReceivedRoomInvitePacket(messengerCenter, response);
    } else if (response.apiID == 'ROOMONE') {
      PacketListener.onReceivedRoomOnePacket(messengerCenter, response);
    } else if (response.apiID == 'ALLREAD') {
      PacketListener.onReceivedAllReadPacket(messengerCenter, response);
    } else if (response.apiID == 'MSG') {
      PacketListener.onReceivedMsgPacket(messengerCenter, response);
    } else if (response.apiID == 'TYPING') {
      PacketListener.onReceivedTypingPacket(messengerCenter, response);
    } else if (response.apiID == 'ROOMOUT') {
      PacketListener.onReceivedRoomOutPacket(messengerCenter, response);
    } else if (response.apiID == 'ROOMNAME') {
      PacketListener.onReceivedRoomNamePacket(messengerCenter, response);
    } else if (response.apiID == 'FAVORITEROOM') {
      PacketListener.onReceivedRoomFavoritePacket(messengerCenter, response);
    } else if (response.apiID == 'CANCELMSG') {
      PacketListener.onReceivedCancelMsgPacket(messengerCenter, response);
    } else if (response.apiID == 'GETFILES') {
      PacketListener.onReceivedGetFilesPacket(messengerCenter, response);
    }
  },
  processFile(messengerCenter: MessengerCenter, rootElement) {
    let response = {};
    rootElement.forEach(element => {
      if (element.name == 'GETTHUMBNAIL') {
        response.apiID = 'GETTHUMBNAIL';
        const attrs = element.attributes;
        if (attrs) {
          const number = attrs['NUMBER'] ?? '';
          const roomKey = attrs['ROOMKEY'] ?? '';
          let thumbnailBase64 = '';
          if (element.elements) {
            thumbnailBase64 = element.elements[0].text ?? '';
          }

          let filePath = talkPath.filePathThumbWithName(
            roomKey,
            TalkConstants.generateThumbFileNameMessage(number)
          );
          if (!talkPath.fileExistAtPath(filePath)) {
            talkPath.writeBase64Image(filePath, thumbnailBase64);
          }
        }
      } else if (element.name == 'HTTPFILERES') {
        response.apiID = 'HTTPFILERES';
        const attrs = element.attributes;
        if (attrs) {
          response.statusCode = attrs['RESULT'] ?? 1;
          response.createDate = TalkConstants.timeFromTimeStamp(
            attrs['TIME'] ?? ''
          );
          response.storagePath = attrs['STORAGEPATH'] ?? '';
          response.numberKey = attrs['NUMBER'] ?? '';
          response.size = attrs['SIZE'] ?? 0;
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.path = attrs['PATH'] ?? '';
          response.name = attrs['NAME'] ?? '';
          response.recordTime = attrs['RECORDINGTIME'] ?? 0;
          response.fileKey = attrs['FILEKEY'] ?? '';
          response.receiver = attrs['RECVER'] ?? '';
          response.serverFileName = attrs['SERVERFILENAME'] ?? '';
          response.clientKey = attrs['CLIENTKEY'] ?? '';
          response.userKey = messengerCenter.userKey;
          response.fileType =
            attrs['FILETYPE'] ?? TalkConstants.MessageFileEXT.ETC_EXT;
          response.messageType = TalkConstants.typeMessageFromFileType(
            TalkConstants.getFileExtension(response.name)
          );
          response.messageThirdParty =
            TalkConstants.MessageThirdParty.MESSAGE_FAST_HTTP_ID;
          const thumbnailElements = element.elements;

          if (thumbnailElements) {
            thumbnailElements.forEach(thumbElement => {
              if (thumbElement.name == 'THUMBNAIL') {
                if (thumbElement.elements) {
                  response.thumbnailBase64 = thumbElement.elements[0].text;
                }
              }
            });
          }
        }
      } else if (element.name == 'HTTPFILEFINISH') {
        response.apiID = 'HTTPFILEFINISH';
        const attrs = element.attributes;
        if (attrs) {
          response.statusCode = attrs['RESULT'] ?? 1;
          response.clouddisk = attrs['CLOUDDISK'] ?? 0;
          response.fileKey = attrs['FILEKEY'] ?? '';
          response.httpHostName = attrs['HOSTNAME'] ?? '';
          response.numberKey = attrs['NUMBER'] ?? '';
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.serverFileName = attrs['SERVERFILENAME'] ?? '';
          response.serverFilePath = attrs['SERVERPATH'] ?? '';
          const type = attrs['TYPE'] ?? '';
          response.fileFinishedOfUpload = type == 'RECEIVER';
          response.createDate = TalkConstants.timeFromTimeStamp(
            attrs['TIMESTAMP'] ?? ''
          );
          response.fileSize = attrs['SIZE'] ?? 0;
        }
      } else if (element.name == 'MFILESENDING') {
        response.apiID = 'MFILESENDING';
        const attrs = element.attributes;
        if (attrs) {
          response.fileKey = attrs['FILEKEY'] ?? '';
          response.name = attrs['NAME'] ?? '';
          response.fileType = attrs['FILETYPE'];
          if (TextUtils.isNotEmpty(response.name)) {
            response.fileType = TalkConstants.getFileExtension(response.name);
          }
          response.ip = attrs['IP'] ?? '';
          response.numberKey = attrs['NUMBER'] ?? '';
          response.path = attrs['PATH'] ?? '';
          response.port = attrs['PORT'] ?? 0;
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.fileSent = attrs['SENT'] ?? 0;
          response.size = attrs['SIZE'] ?? 0;
          response.statusFile = attrs['STATUS'] ?? '';
          response.createDate = TalkConstants.timeFromTimeStamp(
            attrs['TIME'] ?? ''
          );
          response.userKey = messengerCenter.userKey;
          response.clouddisk = attrs['CLOUDDISK'] ?? 0;
          response.messageType = TalkConstants.typeMessageFromFileType(
            TalkConstants.getFileExtension(response.name)
          );
          response.fileAddress = attrs['IP'] ?? '';
          response.filePort = attrs['PORT'] ?? 0;
          response.messageThirdParty =
            response.fileKey == ''
              ? TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID
              : TalkConstants.MessageThirdParty
                  .MESSAGE_MESSENGER_UNKNOWN_METHOD;
        }
      } else if (element.name == 'HTTPFILEREQ') {
        response.apiID = 'HTTPFILEREQ';
        const attrs = element.attributes;
        if (attrs) {
          response.clouddisk = attrs['CLOUDDISK'] ?? 1;
          response.fileKey = attrs['FILEKEY'] ?? '';
          response.name = attrs['NAME'] ?? '';
          response.fileType = attrs['FILETYPE'];
          if (TextUtils.isNotEmpty(response.name)) {
            response.fileType = TalkConstants.getFileExtension(response.name);
          }
          response.hostName = attrs['HOSTNAME'] ?? '';
          response.numberKey = attrs['NUMBER'] ?? '';
          response.path = attrs['PATH'] ?? '';
          response.receiver = attrs['RECVER'] ?? '';
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.userKey = attrs['SENDER'] ?? '';
          response.serverFileName = attrs['SERVERFILENAME'] ?? '';
          response.serverFilePath = attrs['SERVERPATH'] ?? '';
          response.size = attrs['SIZE'] ?? 0;
          response.createDate = TalkConstants.timeFromTimeStamp(
            attrs['TIME'] ?? ''
          );
          response.recordTime = attrs['RECORDINGTIME'] ?? 0;
          response.messageType = TalkConstants.typeMessageFromFileType(
            TalkConstants.getFileExtension(response.name)
          );
          response.messageThirdParty =
            TalkConstants.MessageThirdParty.MESSAGE_FAST_HTTP_ID;
        }
      } else if (element.name == 'MFILESAVE') {
        response.apiID = 'MFILESAVE';
        response.statusCode = 0;
        const attrs = element.attributes;
        if (attrs) {
          response.statusCode = attrs['RESULT'] ?? 0;
          response.numberKey = attrs['NUMBER'] ?? '';
          response.roomKey = attrs['ROOMKEY'] ?? '';
        }
      } else if (element.name == 'MFILEREQ') {
        response.apiID = 'MFILEREQ';
        const attrs = element.attributes;
        if (attrs) {
          response.fileKey = attrs['FILEKEY'] ?? '';
          response.roomKey = attrs['ROOMKEY'] ?? '';
          response.size = attrs['SIZE'] ?? 0;
          response.name = attrs['NAME'] ?? '';
          response.userKey = attrs['SENDER'] ?? '';
          response.fileType = attrs['FILETYPE'];
          if (TextUtils.isNotEmpty(response.name)) {
            response.fileType = TalkConstants.getFileExtension(response.name);
          }
          response.numberKey = attrs['NUMBER'] ?? '';
          response.recordTime = attrs['RECORDINGTIME'] ?? 0;
          response.createDate = TalkConstants.timeFromTimeStamp(
            attrs['TIME'] ?? ''
          );

          response.clouddisk = 1;

          response.hostName = '';

          response.path = '';
          response.receiver = '';

          response.serverFileName = '';
          response.serverFilePath = '';

          response.messageType = TalkConstants.typeMessageFromFileType(
            TalkConstants.getFileExtension(response.name)
          );
          response.messageThirdParty =
            TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID;
          response.fileAddress = attrs['IP'] ?? '';
          response.filePort = attrs['PORT'] ?? 0;
        }
      }
    });

    if (response.apiID == 'GETTHUMBNAIL') {
      PacketListener.onReceivedGetThumbnailPacket(messengerCenter, response);
    } else if (response.apiID == 'HTTPFILERES') {
      PacketListener.onReceivedHttpFileResPacket(messengerCenter, response);
    } else if (response.apiID == 'HTTPFILEFINISH') {
      PacketListener.onReceivedHttpFileFinishedPacket(
        messengerCenter,
        response
      );
    } else if (response.apiID == 'MFILESENDING') {
      PacketListener.onReceivedHttpFileSendingPacket(messengerCenter, response);
    } else if (response.apiID == 'HTTPFILEREQ') {
      PacketListener.onReceivedHttpFileReqPacket(messengerCenter, response);
    } else if (response.apiID == 'MFILESAVE') {
      PacketListener.onReceivedFileSaveClouddiskPacket(
        messengerCenter,
        response
      );
    } else if (response.apiID == 'MFILEREQ') {
      PacketListener.onReceivedMFileReqPacket(messengerCenter, response);
    }
  }
};

let delayReloadAllRead = null;
const PacketListener = {
  getCustomApiSupport(messengerCenter, packetResponse) {
    const otpLogin = messengerCenter.serverInfo.otpLogin > 0;
    const clouddiskSupportXml = messengerCenter.serverInfo.clouddiskSupport > 0;
    const whisperSupportXml = messengerCenter.serverInfo.whisperSupport > 0;
    const chatBoardSupportXml = messengerCenter.serverInfo.chatBoardSupport > 0;

    let clouddiskSupportInLogin = true;
    let whisperSupportInLogin = true;
    if (packetResponse.menu) {
      clouddiskSupportInLogin = packetResponse.menu.webdisk > 0;
      whisperSupportInLogin = packetResponse.menu.whisperSupport > 0;
    }

    let clouddiskSupport = true;
    if (otpLogin && messengerCenter.clouddiskSupportInOTP >= 0) {
      clouddiskSupport = messengerCenter.clouddiskSupportInOTP > 0;
    } else {
      clouddiskSupport = clouddiskSupportXml && clouddiskSupportInLogin;
    }

    let whisperSupport = true;
    if (otpLogin && messengerCenter.whisperSupportInOTP >= 0) {
      whisperSupport = messengerCenter.whisperSupportInOTP > 0;
    } else {
      whisperSupport = whisperSupportXml && whisperSupportInLogin;
    }

    let result = [];
    if (clouddiskSupport) {
      result.push(CUSTOM_SUPPORT_CLOUDDISK);
    }

    if (whisperSupport) {
      result.push(CUSTOM_SUPPORT_WHISPER);
    }

    if (chatBoardSupportXml) {
      result.push(CUSTOM_SUPPORT_CHAT_BOARD);
    }

    return '|' + result.join('|') + '|';
  },
  onReceivedLoginPacket(messengerCenter: MessengerCenter, packetResponse) {
    messengerCenter.tcpConnection.isLogin = false;
    messengerCenter.resetNumberRetry();
    messengerCenter.transferCacheAfterLogin();
    delayReloadAllRead = null;

    messengerCenter.useLoginForRelogin = false;

    if (packetResponse.login.result >= 1) {
      appPrefs.setShowMobileLogin(messengerCenter.serverInfo.showMobileLogin);

      appPrefs.setSecretAccountInfo(
        packetResponse.login.authkey,
        packetResponse.login.autologinkey,
        packetResponse.login.userKey
      );

      messengerCenter._sendExtraLoginChanged({
        weblogin: true,
        value: packetResponse.login.autologinkey
      });

      const customSupport = this.getCustomApiSupport(
        messengerCenter,
        packetResponse
      );

      packetResponse.supportAPIList =
        packetResponse.supportAPIList + customSupport;

      messengerCenter.apiSupportList.updateApiSupport(
        packetResponse.supportAPIList
      );

      messengerCenter.userKey = packetResponse.login.userKey;
      talkPath.userKey = messengerCenter.userKey;
      packetResponse.login.authkey = '';
      packetResponse.login.autologinkey = '';
      packetResponse.login.userKey = '';

      let dtFormat;
      let dFormat;
      let dateFormat = packetResponse.login.gwdateformat;
      if (dateFormat) {
        dtFormat = phpToMoment(dateFormat);
        let words = dateFormat.split(' ');
        dFormat = phpToMoment(words[0]);
      }

      packetResponse.datetimeformat = TextUtils.isEmpty(dtFormat)
        ? 'YYYY-MM-DD HH:mm'
        : dtFormat;
      packetResponse.dateformat = TextUtils.isEmpty(dFormat)
        ? 'YYYY-MM-DD'
        : dFormat;

      appPrefs.setExtraLoginInfo(packetResponse);

      talkDB.updateNickNameByUserKey(
        messengerCenter.userKey,
        packetResponse.login.nickname
      );

      messengerCenter._sendAuthenticatedToApp(
        messengerCenter.typeLogin,
        constantsApp.SOCKET_ERROR_CODE.login_success
      );

      const mySipNumber = packetResponse.login.mysip;
      const mySipPass = packetResponse.login.mysippass;
      let result = talkDB.setActiveAccount(
        messengerCenter.domain,
        messengerCenter.userId,
        messengerCenter.userKey,
        mySipNumber,
        mySipPass
      );
      messengerCenter.accountID = result;

      talkDB.updateAllFileUploadToFailed();
      const currentRoomKey = messengerCenter.getCurrentRoomKeyActive();
      if (currentRoomKey) {
        messengerCenter.sendGetTalkPacketNoCache({
          roomKey: currentRoomKey,
          lastTime: 0,
          count: constantsApp.GETTALK_COUNT,
          clientKey: ''
        });
      }

      if (
        messengerCenter.videoConferenceRoomKey &&
        messengerCenter.videoConferenceRoomKey != '' &&
        messengerCenter.videoConferenceRoomKey != currentRoomKey
      ) {
        messengerCenter.sendGetTalkPacketNoCache({
          roomKey: messengerCenter.videoConferenceRoomKey,
          lastTime: 0,
          count: constantsApp.GETTALK_COUNT,
          clientKey: ''
        });
      }

      messengerCenter.sendRoomListPacket();

      messengerCenter.curUserListUpdate =
        packetResponse.login.lasmodifieduserlist;
      messengerCenter.curUserListFavoriteUpdate =
        packetResponse.login.lasmodifiedfavuserlist;
      messengerCenter.curUserModTime = packetResponse.login.lastusermodtime;

      if (messengerCenter.serverInfo.useHTTPOrgAPI) {
        messengerCenter.sendHTTPUserListRequest();
      } else {
        if (
          appPrefs.isNeedRequestUserList(
            messengerCenter.curUserListUpdate,
            messengerCenter.curUserListFavoriteUpdate
          )
        ) {
          messengerCenter.sendUserListPacket();
        }

        if (messengerCenter.apiSupportList.hasSupportUserModInfoApi()) {
          let lastModUserTime = appPrefs.getLastUpdateUserModInfo();
          if (lastModUserTime == 0) {
            lastModUserTime = packetResponse.login.lasmodifieduserlist;
          }
          if (lastModUserTime < packetResponse.login.lastusermodtime) {
            messengerCenter.sendUserModInfoPacket(lastModUserTime);
          }
        }
      }

      messengerCenter.sendTimeCardPacketTimer();
      messengerCenter.sendLivePacketTimer();
    } else {
      messengerCenter.initServerInfo();
      messengerCenter.disconnectSocket();
      messengerCenter._sendAuthenticationFailedToApp(
        messengerCenter.typeLogin,
        constantsApp.SOCKET_ERROR_CODE.login_failed
      );
    }
  },
  onReceivedReLoginPacket(messengerCenter: MessengerCenter, packetResponse) {
    messengerCenter.transferCacheAfterLogin();
    delayReloadAllRead = null;

    if (packetResponse.relogin.result >= 1) {
      messengerCenter.useLoginForRelogin = false;
      messengerCenter.resetNumberRetry();
      appPrefs.setExtraLoginInfoAuthKey(packetResponse.relogin.authkey);

      messengerCenter._sendExtraLoginChanged({
        authKey: true,
        value: packetResponse.relogin.authkey
      });

      talkDB.updateNickNameByUserKey(
        messengerCenter.userKey,
        packetResponse.relogin.nickname
      );

      messengerCenter._sendAuthenticatedToApp(
        messengerCenter.typeLogin,
        constantsApp.SOCKET_ERROR_CODE.login_success
      );

      talkDB.updateAllFileUploadToFailed();
      const currentRoomKey = messengerCenter.getCurrentRoomKeyActive();
      if (currentRoomKey) {
        messengerCenter.sendGetTalkPacketNoCache({
          roomKey: currentRoomKey,
          lastTime: 0,
          count: constantsApp.GETTALK_COUNT,
          clientKey: ''
        });
      }

      if (
        messengerCenter.videoConferenceRoomKey &&
        messengerCenter.videoConferenceRoomKey != '' &&
        messengerCenter.videoConferenceRoomKey != currentRoomKey
      ) {
        messengerCenter.sendGetTalkPacketNoCache({
          roomKey: messengerCenter.videoConferenceRoomKey,
          lastTime: 0,
          count: constantsApp.GETTALK_COUNT,
          clientKey: ''
        });
      }

      messengerCenter.sendRoomListPacket();

      messengerCenter.curUserListUpdate =
        packetResponse.relogin.lasmodifieduserlist;
      messengerCenter.curUserListFavoriteUpdate =
        packetResponse.relogin.lasmodifiedfavuserlist;
      messengerCenter.curUserModTime = packetResponse.relogin.lastusermodtime;

      if (messengerCenter.serverInfo.useHTTPOrgAPI) {
        messengerCenter.sendHTTPUserListRequest();
      } else {
        if (
          appPrefs.isNeedRequestUserList(
            messengerCenter.curUserListUpdate,
            messengerCenter.curUserListFavoriteUpdate
          )
        ) {
          messengerCenter.sendUserListPacket();
        }

        if (messengerCenter.apiSupportList.hasSupportUserModInfoApi()) {
          let lastModUserTime = appPrefs.getLastUpdateUserModInfo();
          if (lastModUserTime == 0) {
            lastModUserTime = packetResponse.relogin.lasmodifieduserlist;
          }
          if (lastModUserTime < packetResponse.relogin.lastusermodtime) {
            messengerCenter.sendUserModInfoPacket(lastModUserTime);
          }
        }
      }

      messengerCenter.sendTimeCardPacketTimer();
      messengerCenter.sendLivePacketTimer();
    } else {
      if (packetResponse.relogin.result == 0) {
        messengerCenter.logoutWithStatus({
          code: 4,
          msg: 'Relogin failed. GO to login'
        });
      } else if (packetResponse.relogin.result == -1) {
        //do login
        messengerCenter.useLoginForRelogin = true;
        messengerCenter.disconnectSocket();
        messengerCenter._sendConnectionCloseOnErrorToApp('relogin = -1');
      } else {
        messengerCenter.useLoginForRelogin = false;
        messengerCenter.logoutWithStatus({
          code: 4,
          msg: 'Relogin failed. GO to login'
        });
      }
    }
  },
  onReceivedLoginRequestAccessControlPacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    messengerCenter.sendLoginResAccessControlPacket();
  },
  onReceivedWebAutoLoginPacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }
    if (
      packetResponse.result == 1 &&
      TextUtils.isNotEmpty(packetResponse.webLoginKey)
    ) {
      appPrefs.updateWebAutoLoginKey(packetResponse.webLoginKey);
      messengerCenter._sendExtraLoginChanged({
        weblogin: true,
        value: packetResponse.webLoginKey
      });
    }
  },
  onReceivedUserListPacket(messengerCenter: MessengerCenter, packetResponse) {
    let result = cj4dxUtils.storeBranchUserOnly(
      packetResponse.manageDept,
      packetResponse.branchDept,
      packetResponse.deptList,
      packetResponse.listUser,
      packetResponse.listGroup
    );
    if (result.hasBranchInMyKey) {
      talkDB.storeContactAndGroup(result.newListGroup, result.newListContact);
      messengerCenter._sendORGChangedNotify();
      messengerCenter.sendLivePacket();
    } else {
      talkDB.storeContactAndGroup(
        packetResponse.listGroup,
        packetResponse.listUser
      );
      messengerCenter._sendORGChangedNotify();
      messengerCenter.sendLivePacket();
    }
    appPrefs.setLastUserListUpdate(
      messengerCenter.curUserListUpdate,
      messengerCenter.curUserListFavoriteUpdate
    );
  },
  onReceivedUserModInfoPacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    talkDB.updateNickNameAndPhotoTimeList(
      packetResponse.nicknames,
      packetResponse.photos
    );
    appPrefs.setLastUpdateUserModInfo(messengerCenter.curUserModTime);
  },
  onReceivedAlertRoomPacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }
    if (packetResponse.result > 0) {
      talkDB.updateRoomAlertByRoomKey(
        accountID,
        packetResponse.roomKey,
        packetResponse.enable
      );
      messengerCenter._sendReloadFullContentChangedNotify({
        roomKey: packetResponse.roomKey,
        queryRoomDetailOnly: true
      });
    }
  },
  onReceivedFolderDelPacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }
    if (packetResponse.result > 0) {
      talkDB.deleteGroup(packetResponse.folderKey);
      messengerCenter._sendORGChangedNotify();
    }
  },
  onReceivedFolderCreatePacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }
    if (packetResponse.result > 0) {
      talkDB.insertAGroupFolder(packetResponse.group);
      messengerCenter._sendORGChangedNotify();
    }
  },
  onReceivedUserDelPacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }
    if (packetResponse.result > 0) {
      talkDB.deleteMultiUserInFolder(
        packetResponse.folderKey,
        packetResponse.userKeyList
      );

      messengerCenter._sendORGChangedNotify();
    }
  },
  onReceivedUserUpdatePacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }
    if (packetResponse.result > 0) {
      talkDB.updateMultiUserInFolder(
        packetResponse.folderKey,
        packetResponse.userKeyList
      );

      messengerCenter._sendORGChangedNotify();
    }
  },
  onReceivedUserAddPacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }
    if (packetResponse.result > 0) {
      talkDB.insertMultiUserInFolder(
        packetResponse.folderKey,
        packetResponse.userKeyList
      );

      messengerCenter._sendORGChangedNotify();
    }
  },
  onReceivedUserStatusPacket(messengerCenter: MessengerCenter, packetResponse) {
    if (TextUtils.isEmpty(packetResponse.userKey)) {
      return;
    }
    talkDB.updateStatusLoginTypeByUserKey(
      packetResponse.userKey,
      packetResponse.loginType,
      packetResponse.status
    );
    messengerCenter._sendORGStatusChangedNotify();
  },
  onReceivedUserNickNamePacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    if (TextUtils.isEmpty(packetResponse.userKey)) {
      return;
    }
    talkDB.updateNickNameByUserKey(
      packetResponse.userKey,
      packetResponse.nickName
    );
    if (packetResponse.userKey == messengerCenter.userKey) {
      appPrefs.updateNickname(packetResponse.nickName);
      messengerCenter._sendExtraLoginChanged({
        nickname: true,
        value: packetResponse.nickName
      });
    }
    messengerCenter._sendORGStatusChangedNotify();
  },
  onReceivedRoomListPacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }
    let result = talkDB.storeRoomList(
      packetResponse.roomList,
      packetResponse.oldRoomTime,
      accountID,
      messengerCenter.userKey
    );

    messengerCenter.roomListPacketResponse = true;
    let lastRoomListUpdateTime = packetResponse.lastRoomListUpdate;
    if (lastRoomListUpdateTime > 0) {
      appPrefs.setLastUpdateRoomList(lastRoomListUpdateTime);
    } else if (appPrefs.getLastUpdateRoomList() == 0) {
      appPrefs.setLastUpdateRoomList(-1);
    }

    messengerCenter._sendRoomChangedNotify({ hasNewMessage: result });
  },
  onReceivedRoomOnePacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }

    if (
      !packetResponse.result ||
      TextUtils.isEmpty(packetResponse.result.roomKey)
    ) {
      return;
    }

    talkDB.storeRoomDetailWithRoomKey(
      packetResponse.result.roomKey,
      accountID,
      packetResponse,
      messengerCenter.userKey
    );

    if (packetResponse.whos && packetResponse.whos.length > 0) {
      talkDB.updateAllMessageReadByRoomKeyVer2(
        packetResponse.result.roomKey,
        accountID
      );
    }

    messengerCenter._sendReloadFullContentChangedNotify({
      roomKey: packetResponse.result.roomKey,
      queryRoomDetailOnly: true
    });
  },
  onReceivedAllReadPacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }

    let needBroadcast = true;
    if (
      packetResponse.userKey != messengerCenter.userKey &&
      TextUtils.isNotEmpty(packetResponse.timeStamp)
    ) {
      const timeStamp = TalkConstants.timeFromTimeStamp(
        packetResponse.timeStamp
      );
      needBroadcast = false;
      talkDB.updateUnreadMessageWithAllReadAndRemoveCount(
        packetResponse.roomKey,
        accountID,
        packetResponse.userKey,
        timeStamp
      );
    } else {
      messengerCenter.markPositionUnreadMessageCached.clearMarkUnreadWithRoomKey(
        packetResponse.roomKey
      );
      talkDB.updateAllMessageAsReadedWithRoomKey(
        packetResponse.roomKey,
        accountID
      );
    }

    if (
      needBroadcast ||
      packetResponse.roomKey == messengerCenter.getCurrentRoomKeyActive()
    ) {
      if (delayReloadAllRead) {
        clearTimeout(delayReloadAllRead);
      }
      delayReloadAllRead = setTimeout(() => {
        messengerCenter._sendDataChangedToApp(
          constantsApp.ACTION_RELOAD_ALL_ROOM_CONTENT,
          {
            roomKey: packetResponse.roomKey
          }
        );
      }, 1000);
    }
  },
  onReceivedMsgPacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }

    messengerCenter.updateMarkPosition(
      packetResponse.roomKey,
      packetResponse.message?.userKey,
      packetResponse.message?.createDate
    );

    const isNewRoom = talkDB.insertMessageTextV2(
      packetResponse.roomKey,
      packetResponse.message,
      messengerCenter.userKey,
      accountID
    );

    messengerCenter.updateTalkInfo(
      packetResponse.roomKey,
      packetResponse.message?.createDate
    );

    if (isNewRoom) {
      messengerCenter.sendRoomOnePacket(packetResponse.roomKey);
    } else {
      talkDB.updateAllMessageReadByRoomKeyVer2(
        packetResponse.roomKey,
        accountID
      );
    }
    messengerCenter.sendMessageResultPacket(
      packetResponse.roomKey,
      packetResponse.message.numberKey
    );
    if (messengerCenter.userKey != packetResponse.message.userKey) {
      messengerCenter._sendDataChangedToApp(
        constantsApp.ACTION_SHOW_NOTIFICATION_WITH_DATA,
        packetResponse
      );
    }

    messengerCenter._sendReloadFullContentChangedNotify({
      roomKey: packetResponse.roomKey
    });
  },
  onReceivedRoomOutPacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }

    if (packetResponse.userKey == messengerCenter.userKey) {
      messengerCenter.removeTalkInfo(packetResponse.roomKey);
      talkDB.deleteRoomAndMessageWithRoomKey(packetResponse.roomKey, accountID);
      messengerCenter._sendDataChangedToApp(
        constantsApp.ACTION_RELOAD_ROOM_AND_REMOVE,
        {
          roomKey: packetResponse.roomKey
        }
      );
    } else {
      talkDB.insertRoomOutMessage(
        accountID,
        packetResponse.roomKey,
        packetResponse.userKey,
        packetResponse.createDate
      );

      messengerCenter.updateTalkInfo(
        packetResponse.roomKey,
        packetResponse.createDate
      );

      messengerCenter.updateMarkPosition(
        packetResponse.roomKey,
        packetResponse.userKey,
        packetResponse.createDate
      );
      messengerCenter.sendRoomOnePacket(packetResponse.roomKey);
      messengerCenter._sendReloadFullContentChangedNotify({
        roomKey: packetResponse.roomKey
      });
    }
  },
  onReceivedRoomNamePacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }
    if (packetResponse.statusCode > 0) {
      const aliasPublic = packetResponse.private <= 0;
      talkDB.updateRoomNameByRoomKey(
        accountID,
        packetResponse.roomKey,
        aliasPublic,
        packetResponse.name
      );
      if (aliasPublic) {
        messengerCenter.sendGetTalkPacketNoCache({
          roomKey: packetResponse.roomKey,
          lastTime: 0,
          count: constantsApp.GETTALK_COUNT,
          clientKey: 'IGNORE_UPDATE'
        });
      }
      messengerCenter._sendReloadFullContentChangedNotify({
        roomKey: packetResponse.roomKey,
        queryRoomDetailOnly: true
      });
    }
  },
  onReceivedRoomFavoritePacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }
    if (packetResponse.statusCode > 0) {
      talkDB.updateRoomFavoriteByRoomKey(
        accountID,
        packetResponse.roomKey,
        packetResponse.isAdd ? 1 : 0
      );
      messengerCenter._sendReloadFullContentChangedNotify({
        roomKey: packetResponse.roomKey,
        queryRoomDetailOnly: true
      });
    }
  },
  onReceivedCancelMsgPacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }
    if (packetResponse.statusCode > 0) {
      const roomKey = packetResponse.roomKey;
      const numberKey = packetResponse.numberKey;
      talkDB.updateMessageDeleteByRoomKey(accountID, roomKey, numberKey);
      messengerCenter._sendReloadFullContentChangedNotify({
        roomKey: roomKey
      });
    }
  },
  onReceivedTypingPacket(messengerCenter: MessengerCenter, packetResponse) {
    if (packetResponse.roomKey == messengerCenter.getCurrentRoomKeyActive()) {
      messengerCenter._sendDataChangedToApp(
        constantsApp.ACTION_RECEIVED_TYPING,
        packetResponse
      );
    }
  },
  onReceivedRoomCreatePacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }

    if (packetResponse.result.statusCode > 0) {
      let roomID = talkDB.storeNewRoom(packetResponse.result, accountID);
      if (messengerCenter.keepRoomCreateQueue != null) {
        const primaryKey = messengerCenter.keepRoomCreateQueue.primaryKey;
        if (primaryKey == packetResponse.result.roomTagName) {
          const data = {
            ...messengerCenter.keepRoomCreateQueue,
            primaryKey: packetResponse.result.roomKey,
            room: {
              rID: roomID,
              rRoomKey: packetResponse.result.roomKey
            }
          };
          messengerCenter._sendDataChangedToApp(
            constantsApp.ACTION_OPEN_ROOM_SELECTED,
            data
          );
          messengerCenter.keepRoomCreateQueue = null;
        }
      }
      const shareMsgObj =
        messengerCenter.keepForwardMessageList[
          packetResponse.result.roomTagName
        ];
      if (shareMsgObj) {
        const shareMsg = shareMsgObj.data;
        const roomKey = packetResponse.result.roomKey;
        // send message only
        if (shareMsgObj.filePath && shareMsgObj.filePath != '') {
          messengerCenter.sendAPIToSever(constantsApp.API_SEND_FILE_HTTP_FAST, {
            roomKey: roomKey,
            file: {
              name: shareMsg.msgdtFileName,
              path: shareMsgObj.filePath,
              size: shareMsg.msgdtFileSize
            }
          });
        } else {
          const clientID = appPrefs.generateRandomUUID();
          let msgId = talkDB.insertSendingMessage(
            messengerCenter.isAuthenticated,
            roomID,
            roomKey,
            clientID,
            messengerCenter.userKey,
            shareMsg.msgEmotion,
            shareMsg.msgBody
          );

          messengerCenter._sendReloadFullContentChangedNotify({
            roomKey: roomKey
          });

          messengerCenter.sendAPIToSever(constantsApp.API_SEND_MESSAGE, {
            roomKey: roomKey,
            clientId: clientID,
            userKey: messengerCenter.userKey,
            emoticon: shareMsg.msgEmotion,
            message: shareMsg.msgBody
          });
        }

        messengerCenter.keepForwardMessageList[
          packetResponse.result.roomTagName
        ] = null;
      }

      //send room one
      messengerCenter.sendRoomOnePacket(packetResponse.result.roomKey);
    }
  },
  onReceivedRoomInvitePacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }

    if (packetResponse.result.statusCode > 0) {
      const roomKey = packetResponse.result.roomKey;
      const inviterName = packetResponse.result.inviterName;
      const createDate = packetResponse.result.createDate;
      const whos = packetResponse.result.whos;

      messengerCenter.updateTalkInfo(roomKey, createDate);

      // talkDB.insertInviteMessage(
      //   accountID,
      //   roomKey,
      //   createDate,
      //   inviterName,
      //   whos
      // );
      // messengerCenter._sendReloadFullContentChangedNotify({
      //   roomKey: roomKey
      // });

      messengerCenter.sendRoomOnePacket(roomKey);
      messengerCenter.sendGetTalkPacketNoCache({
        roomKey: roomKey,
        lastTime: 0,
        count: constantsApp.GETTALK_COUNT,
        clientKey: 'IGNORE_UPDATE'
      });
    }
  },
  onReceivedGetFilesPacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }

    let roomId = -1;
    if (packetResponse.talkCount > 0) {
      roomId = talkDB.storeRoomTalk(
        packetResponse.roomKey,
        packetResponse.talkCount,
        packetResponse.messageList,
        messengerCenter.userKey,
        accountID
      );
    } else {
      roomId = talkDB.findRoomIDByRoomKey(packetResponse.roomKey, accountID);
    }
    packetResponse.roomID = roomId;
    messengerCenter._sendDataChangedToApp(
      constantsApp.ACTION_MSG_GETFILES_CHANGED,
      packetResponse
    );
  },
  onReceivedGetTalkPacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }

    let roomId = -1;
    if (packetResponse.talkCount > 0) {
      roomId = talkDB.storeRoomTalk(
        packetResponse.roomKey,
        packetResponse.talkCount,
        packetResponse.messageList,
        messengerCenter.userKey,
        accountID
      );

      let latestMsg = null;
      if (packetResponse.messageList && packetResponse.messageList.length > 0) {
        latestMsg =
          packetResponse.messageList[packetResponse.messageList.length - 1];
      }
      messengerCenter.addTalkInfo(
        packetResponse.roomKey,
        packetResponse.firstTimeStamp,
        latestMsg.createDate
      );

      //send get thumbnail
      if (packetResponse.needRequestThumbnailList.length > 0) {
        messengerCenter.sendGetThumbnailList(
          packetResponse.roomKey,
          packetResponse.needRequestThumbnailList
        );
      }
    } else {
      roomId = talkDB.findRoomIDByRoomKey(packetResponse.roomKey, accountID);
    }

    if (packetResponse.clientKey == 'IGNORE_UPDATE') {
      messengerCenter._sendReloadFullContentChangedNotify({
        roomKey: packetResponse.roomKey
      });
      return;
    }

    const data = {
      roomKey: packetResponse.roomKey,
      clientKey: packetResponse.clientKey,
      firstTimeStamp: packetResponse.firstTimeStamp,
      talkCount: packetResponse.talkCount,
      roomID: roomId
    };
    messengerCenter._sendGetTalkChangedNotify(data);
  },
  onReceivedGetThumbnailPacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    messengerCenter._sendMsgListStatusChangedNotify();
  },
  insertMessageFileToDB(
    messengerCenter: MessengerCenter,
    packetResponse,
    newFilePath
  ) {
    // insert message file
    talkDB.insertMessageFileV2(
      packetResponse.roomKey,
      packetResponse,
      {
        fileName: packetResponse.name ?? '',
        fileKey: packetResponse.fileKey ?? '',
        fileStatus: TalkConstants.MessageFileStatus.FILE_MSG_UPLOAD_WORKING,
        fileSize: packetResponse.size ?? 0,
        filePath: newFilePath ?? '',
        fileIP: '',
        filePort: 0,
        fileReceiver: packetResponse.receiver,
        fileStoragePath: packetResponse.storagePath,
        fileServerFileName: packetResponse.serverFileName ?? '',
        fileServerFilePath: '',
        fileServerHostName: messengerCenter.domain,
        fileRecordTime: packetResponse.recordTime ?? 0,
        hasFile: 1,
        clouddisk: 1
      },
      messengerCenter.userKey,
      messengerCenter.accountID
    );
    messengerCenter._sendReloadFullContentChangedNotify({
      roomKey: packetResponse.roomKey
    });

    packetResponse.userKey = messengerCenter.userKey;
    packetResponse.hostName = messengerCenter.domain;
    packetResponse.thumbnailBase64 = '';

    // UPLOAD FILE TEST
    // let formData = new FormData();
    // let str = fs.createReadStream(packetResponse.path);
    // formData.append('userfile', str);

    // formData.getLength((error, length) => {
    //   let axiosUploader = Axios.create({});

    //   axiosUploader
    //     .post(
    //       'https://n.hanbiro.com/winapp/hcsong/messenger_file.php',
    //       formData,
    //       {
    //         headers: {
    //           ...formData.getHeaders(),
    //           'Content-Length': length,
    //           HANBIRO_MODE: 'put',
    //           'HANBIRO-MODE': 'put',
    //           HANBIRO_SENDER: packetResponse.userKey,
    //           'HANBIRO-SENDER': packetResponse.userKey,
    //           HANBIRO_STORAGEPATH: packetResponse.storagePath,
    //           'HANBIRO-STORAGEPATH': packetResponse.storagePath,
    //           HANBIRO_FILEKEY: packetResponse.fileKey,
    //           'HANBIRO-FILEKEY': packetResponse.fileKey
    //         },
    //         onUploadProgress: progressEvent => {
    //           console.log(progressEvent);
    //         }
    //       }
    //     )
    //     .then(response => {
    //       console.log(response);
    //     })
    //     .catch(error => {
    //       console.log(error);
    //     });
    // });

    messengerCenter._sendDataChangedToApp(
      constantsApp.ACTION_FILE_UPLOAD_RES,
      packetResponse
    );
  },
  onReceivedHttpFileResPacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }

    if (packetResponse.statusCode <= 0 || packetResponse.roomKey == '') {
      return;
    }

    const typeFile = TalkConstants.getFileExtension(packetResponse.name);
    if (typeFile == TalkConstants.MessageFileEXT.IMAGE_EXT) {
      // write file to cached
      let newFilePath = talkPath.filePathDownloadedWithName(
        packetResponse.roomKey,
        TalkConstants.generateFileNameDownloadedMessage(
          packetResponse.numberKey,
          packetResponse.name
        )
      );
      fs.copyFile(packetResponse.path, newFilePath, error => {
        if (error) {
          return;
        }
        let filePath = talkPath.filePathThumbWithName(
          packetResponse.roomKey,
          TalkConstants.generateThumbFileNameMessage(packetResponse.numberKey)
        );

        resizeImageWithPathAndSave(
          newFilePath,
          constantsApp.THUMBNAIL_SIZE_LOCAL_STORE,
          filePath,
          constantsApp.THUMBNAIL_SIZE_LOCAL_STORE_QUALITY,
          packetResponse.name,
          () => {
            packetResponse.useServerResponsePath = false;
            this.insertMessageFileToDB(messengerCenter, packetResponse, '');
            messengerCenter.updateTalkInfo(
              packetResponse.roomKey,
              packetResponse.createDate
            );
          }
        );
      });
    } else {
      packetResponse.useServerResponsePath = true;
      this.insertMessageFileToDB(
        messengerCenter,
        packetResponse,
        packetResponse.path
      );
      messengerCenter.updateTalkInfo(
        packetResponse.roomKey,
        packetResponse.createDate
      );
    }
  },
  onReceivedMFileReqPacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }

    if (packetResponse.roomKey == '') {
      return;
    }

    // insert message file
    const isNewRoom = talkDB.insertMessageFileV2(
      packetResponse.roomKey,
      {
        createDate: packetResponse.createDate,
        storagePath: packetResponse.serverFilePath,
        numberKey: packetResponse.numberKey,
        size: packetResponse.size,
        roomKey: packetResponse.roomKey,
        path: packetResponse.path,
        name: packetResponse.name,
        recordTime: packetResponse.recordTime,
        fileKey: packetResponse.fileKey,
        receiver: packetResponse.receiver,
        serverFileName: packetResponse.serverFileName,
        clientKey: '',
        userKey: packetResponse.userKey,
        fileType: packetResponse.fileType,
        messageType: packetResponse.messageType,
        messageThirdParty: packetResponse.messageThirdParty
      },
      {
        fileName: packetResponse.name ?? '',
        fileKey: packetResponse.fileKey ?? '',
        fileStatus:
          TalkConstants.MessageFileStatus.FILE_MSG_FROM_ANOTHER_DEVICE,
        fileSize: packetResponse.size ?? 0,
        filePath: '',
        fileIP: packetResponse.fileAddress,
        filePort: packetResponse.filePort,
        fileReceiver: packetResponse.receiver,
        fileStoragePath: packetResponse.serverFilePath,
        fileServerFileName: packetResponse.serverFileName ?? '',
        fileServerFilePath: packetResponse.serverFilePath,
        fileServerHostName: packetResponse.hostName,
        fileRecordTime: packetResponse.recordTime ?? 0,
        hasFile: 1,
        clouddisk: 1
      },
      messengerCenter.userKey,
      messengerCenter.accountID
    );

    if (isNewRoom) {
      messengerCenter.sendRoomOnePacket(packetResponse.roomKey);
    }

    messengerCenter.updateTalkInfo(
      packetResponse.roomKey,
      packetResponse.createDate
    );

    messengerCenter.updateMarkPosition(
      packetResponse.roomKey,
      packetResponse.userKey,
      packetResponse.createDate
    );

    messengerCenter._sendDataChangedToApp(
      constantsApp.ACTION_SHOW_NOTIFICATION_WITH_DATA,
      packetResponse
    );

    messengerCenter._sendReloadFullContentChangedNotify({
      roomKey: packetResponse.roomKey
    });
  },
  onReceivedFileCloudPacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }

    if (packetResponse.roomKey == '') {
      return;
    }

    // insert message file
    const isNewRoom = talkDB.insertMessageFileV2(
      packetResponse.roomKey,
      {
        createDate: packetResponse.createDate,
        storagePath: packetResponse.serverFilePath,
        numberKey: packetResponse.numberKey,
        size: packetResponse.size,
        roomKey: packetResponse.roomKey,
        path: packetResponse.path,
        name: packetResponse.name,
        recordTime: packetResponse.recordTime,
        fileKey: packetResponse.fileKey,
        receiver: packetResponse.receiver,
        serverFileName: packetResponse.serverFileName,
        clientKey: '',
        userKey: packetResponse.userKey,
        fileType: packetResponse.fileType,
        messageType: packetResponse.messageType,
        messageThirdParty: packetResponse.messageThirdParty
      },
      {
        fileName: packetResponse.name ?? '',
        fileKey: packetResponse.fileKey ?? '',
        fileStatus:
          TalkConstants.MessageFileStatus.FILE_MSG_FROM_ANOTHER_DEVICE,
        fileSize: packetResponse.size ?? 0,
        filePath: '',
        fileIP: packetResponse.fileAddress,
        filePort: packetResponse.filePort,
        fileReceiver: packetResponse.receiver,
        fileStoragePath: packetResponse.serverFilePath,
        fileServerFileName: packetResponse.serverFileName ?? '',
        fileServerFilePath: packetResponse.serverFilePath,
        fileServerHostName: packetResponse.hostName,
        fileRecordTime: packetResponse.recordTime ?? 0,
        hasFile: 1,
        clouddisk: 1
      },
      messengerCenter.userKey,
      messengerCenter.accountID
    );

    if (isNewRoom) {
      messengerCenter.sendRoomOnePacket(packetResponse.roomKey);
    }

    messengerCenter.updateTalkInfo(
      packetResponse.roomKey,
      packetResponse.createDate
    );

    messengerCenter.updateMarkPosition(
      packetResponse.roomKey,
      packetResponse.userKey,
      packetResponse.createDate
    );

    if (messengerCenter.userKey != packetResponse.userKey) {
      messengerCenter._sendDataChangedToApp(
        constantsApp.ACTION_SHOW_NOTIFICATION_WITH_DATA,
        packetResponse
      );
    }

    messengerCenter._sendReloadFullContentChangedNotify({
      roomKey: packetResponse.roomKey
    });
  },
  onReceivedHttpFileReqPacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }

    if (packetResponse.roomKey == '') {
      return;
    }

    // insert message file
    const isNewRoom = talkDB.insertMessageFileV2(
      packetResponse.roomKey,
      {
        createDate: packetResponse.createDate,
        storagePath: packetResponse.serverFilePath,
        numberKey: packetResponse.numberKey,
        size: packetResponse.size,
        roomKey: packetResponse.roomKey,
        path: packetResponse.path,
        name: packetResponse.name,
        recordTime: packetResponse.recordTime,
        fileKey: packetResponse.fileKey,
        receiver: packetResponse.receiver,
        serverFileName: packetResponse.serverFileName,
        clientKey: '',
        userKey: packetResponse.userKey,
        fileType: packetResponse.fileType,
        messageType: packetResponse.messageType,
        messageThirdParty: packetResponse.messageThirdParty
      },
      {
        fileName: packetResponse.name ?? '',
        fileKey: packetResponse.fileKey ?? '',
        fileStatus:
          TalkConstants.MessageFileStatus.FILE_MSG_FROM_ANOTHER_DEVICE,
        fileSize: packetResponse.size ?? 0,
        filePath: '',
        fileIP: '',
        filePort: 0,
        fileReceiver: packetResponse.receiver,
        fileStoragePath: packetResponse.serverFilePath,
        fileServerFileName: packetResponse.serverFileName ?? '',
        fileServerFilePath: packetResponse.serverFilePath,
        fileServerHostName: packetResponse.hostName,
        fileRecordTime: packetResponse.recordTime ?? 0,
        hasFile: 1,
        clouddisk: 1
      },
      messengerCenter.userKey,
      messengerCenter.accountID
    );

    if (isNewRoom) {
      messengerCenter.sendRoomOnePacket(packetResponse.roomKey);
    }

    messengerCenter.updateTalkInfo(
      packetResponse.roomKey,
      packetResponse.createDate
    );

    messengerCenter.updateMarkPosition(
      packetResponse.roomKey,
      packetResponse.userKey,
      packetResponse.createDate
    );

    messengerCenter._sendDataChangedToApp(
      constantsApp.ACTION_SHOW_NOTIFICATION_WITH_DATA,
      packetResponse
    );
    messengerCenter._sendReloadFullContentChangedNotify({
      roomKey: packetResponse.roomKey
    });
  },
  onReceivedHttpFileFinishedPacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }

    if (
      packetResponse.statusCode <= 0 ||
      packetResponse.roomKey == '' ||
      packetResponse.fileKey == ''
    ) {
      return;
    }

    talkDB.updateMessageFileWithRoomKeyNumberKey(
      packetResponse.roomKey,
      packetResponse.numberKey,
      {
        createDate: packetResponse.createDate,
        messageThirdParty: TalkConstants.MessageThirdParty.MESSAGE_FAST_HTTP_ID
      },
      {
        fileSize: packetResponse.fileSize,
        fileStatus:
          TalkConstants.MessageFileStatus.FILE_MSG_FROM_ANOTHER_DEVICE,
        serverFilePath: packetResponse.serverFilePath,
        serverFileName: packetResponse.serverFileName,
        serverHostName: packetResponse.httpHostName
      }
    );

    messengerCenter.updateTalkInfo(
      packetResponse.roomKey,
      packetResponse.createDate
    );

    messengerCenter.updateMarkPosition(
      packetResponse.roomKey,
      packetResponse.fileFinishedOfUpload ? messengerCenter.userKey : '-999',
      packetResponse.createDate
    );

    messengerCenter._sendReloadFullContentChangedNotify({
      roomKey: packetResponse.roomKey,
      msgOnly: true
    });
  },
  onReceivedHttpFileSendingPacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    if (packetResponse.statusFile != 'COMPLETED') {
      return;
    }
    if (packetResponse.roomKey == '') {
      return;
    }

    const needReload =
      packetResponse.messageThirdParty ==
      TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID;

    if (packetResponse.fileKey == '' && !needReload) {
      return;
    }

    // insert message file
    const isNewRoom = talkDB.insertMessageFileV2(
      packetResponse.roomKey,
      packetResponse,
      {
        fileName: packetResponse.name ?? '',
        fileKey: packetResponse.fileKey ?? '',
        fileStatus:
          TalkConstants.MessageFileStatus.FILE_MSG_FROM_ANOTHER_DEVICE,
        fileSize: packetResponse.size ?? 0,
        filePath: '',
        fileIP: packetResponse.fileAddress,
        filePort: packetResponse.filePort,
        fileReceiver: packetResponse.receiver,
        fileStoragePath: packetResponse.storagePath,
        fileServerFileName: packetResponse.serverFileName ?? '',
        fileServerFilePath: '',
        fileServerHostName: messengerCenter.domain,
        fileRecordTime: packetResponse.recordTime ?? 0,
        hasFile: 1,
        clouddisk: 0
      },
      messengerCenter.userKey,
      messengerCenter.accountID
    );

    if (isNewRoom) {
      messengerCenter.sendRoomOnePacket(packetResponse.roomKey);
    }

    if (needReload) {
      messengerCenter.updateTalkInfo(
        packetResponse.roomKey,
        packetResponse.createDate
      );

      messengerCenter._sendReloadFullContentChangedNotify({
        roomKey: packetResponse.roomKey
      });
    }
  },
  onReceivedFileSaveClouddiskPacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }

    if (packetResponse.statusCode == 0 || packetResponse.roomKey == '') {
      return;
    }

    let fileName = '';
    const msg = talkDB.findMessageByRoomKeyNumberKey(
      packetResponse.roomKey,
      accountID,
      packetResponse.numberKey
    );
    if (msg) {
      fileName = msg.msgdtFileName;
    }

    let message = '';
    if (packetResponse.statusCode == 1) {
      message = allTranslation
        .text('Save to Clouddisk successful')
        .replace('$1', fileName);
    } else if (packetResponse.statusCode == -1) {
      message = allTranslation
        .text('Save to Clouddisk failed [File is not valid]')
        .replace('$1', fileName);
    } else {
      message = allTranslation
        .text('Save to Clouddisk failed [File is not exist]')
        .replace('$1', fileName);
    }
    messengerCenter._sendDataChangedToApp(
      constantsApp.ACTION_SHOW_MSG_INFO_TO_USER,
      message
    );
  },
  onReceivedCompanyUserPacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    let sendRefresh = false;
    if (packetResponse.mode == 'REF') {
      messengerCenter.sendDelayRequestUserList(false);
    } else if (packetResponse.mode == 'MOD') {
      if (packetResponse.user) {
        talkDB.updateAContact(packetResponse.user);
        sendRefresh = true;
        console.log(packetResponse.user);
      }
    } else if (packetResponse.mode == 'DEL') {
      talkDB.deleteContact(packetResponse.userKey);
      sendRefresh = true;
    } else if (packetResponse.mode == 'ADD') {
      if (constantsApp.IS_CHECK_BRANCH_DEPT_CJ4DX) {
        messengerCenter.sendDelayRequestUserList(false);
      } else {
        talkDB.insertAContact(packetResponse.user);
        sendRefresh = true;
      }
    }

    if (sendRefresh) {
      messengerCenter._sendORGChangedNotify();
    }
  },
  onReceivedCompanyGroupPacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    let sendRefresh = false;
    if (packetResponse.mode == 'REF') {
      messengerCenter.sendDelayRequestUserList(false);
    } else if (packetResponse.mode == 'MOD') {
      if (packetResponse.group) {
        talkDB.updateAGroup(packetResponse.group);
        sendRefresh = true;
      }
    } else if (packetResponse.mode == 'DEL') {
      talkDB.deleteGroup(packetResponse.groupKey);
      sendRefresh = true;
    } else if (packetResponse.mode == 'ADD') {
      if (constantsApp.IS_CHECK_BRANCH_DEPT_CJ4DX) {
        messengerCenter.sendDelayRequestUserList(false);
      } else {
        talkDB.insertAGroup(packetResponse.group);
        sendRefresh = true;
      }
    }

    if (sendRefresh) {
      messengerCenter._sendORGChangedNotify();
    }
  },
  onReceivedFavoritePacket(messengerCenter: MessengerCenter, packetResponse) {
    if (packetResponse.statusCode > 0) {
      const key = packetResponse.key;
      const target = packetResponse.target;
      const mode = packetResponse.mode;
      let msg;
      if (mode == 'ADD') {
        talkDB.addNewKeyToFavorite(key, target == 'U');
        msg = allTranslation.text('Add to Favorites success');
      } else if (mode == 'DEL') {
        talkDB.deleteKeyToFavorite(key, target == 'U');
        msg = allTranslation.text('Remove from Favorite success');
      } else if (mode == 'REF') {
        messengerCenter.sendDelayRequestUserList(false);
      } else if (mode == 'MOD') {
        messengerCenter.sendDelayRequestUserList(false);
      }

      if (msg) {
        messengerCenter._sendORGChangedNotify();
        messengerCenter._sendDataChangedToApp(
          constantsApp.ACTION_SHOW_MSG_INFO_TO_USER,
          msg
        );
      }
    }
  },
  onReceivedOldProtocolPacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    messengerCenter.logoutWithStatus({ code: 1, msg: 'Old protocol' });
  },
  onReceivedSocketClosedPacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    if (packetResponse.status == 1) {
      messengerCenter.logoutWithStatus({ code: 2, msg: 'Dupplicate login' });
    } else {
      messengerCenter.logoutWithStatus({ code: 3, msg: 'Server restart' });
    }
  },
  onReceivedChatBoardRoomPacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }

    messengerCenter.updateMarkPosition(
      packetResponse.roomKey,
      packetResponse.message?.userKey,
      packetResponse.message?.createDate
    );

    const isNewRoom = talkDB.insertMessageBoard(
      packetResponse.roomKey,
      packetResponse.message,
      messengerCenter.userKey,
      accountID
    );

    messengerCenter.updateTalkInfo(
      packetResponse.roomKey,
      packetResponse.message?.createDate
    );

    if (isNewRoom) {
      messengerCenter.sendRoomOnePacket(packetResponse.roomKey);
    } else {
      talkDB.updateAllMessageReadByRoomKeyVer2(
        packetResponse.roomKey,
        accountID
      );
    }

    if (messengerCenter.userKey != packetResponse.message.userKey) {
      messengerCenter._sendDataChangedToApp(
        constantsApp.ACTION_SHOW_NOTIFICATION_WITH_DATA,
        packetResponse
      );
    }
    messengerCenter._sendDataChangedToApp(
      constantsApp.ACTION_SHOW_BOARD_ALARM_NOTIFY,
      packetResponse
    );
    messengerCenter._sendReloadFullContentChangedNotify({
      roomKey: packetResponse.roomKey
    });
  },
  onReceivedSipEndPacket(messengerCenter: MessengerCenter, packetResponse) {
    // hangup call here
    messengerCenter._sendDataChangedToApp(
      constantsApp.ACTION_SIP_END,
      packetResponse
    );

    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }
    messengerCenter.sendBothRoomListAndGettalkAPI(packetResponse.roomKey);
  },
  onReceivedSipResPacket(messengerCenter: MessengerCenter, packetResponse) {
    if (sipController.existCall()) {
      return;
    }
    if (packetResponse.clientKey == sipController.makeCallClientKey) {
      messengerCenter.clearCallStateTimer();
      messengerCenter._sendDataChangedToApp(
        constantsApp.ACTION_SIPRES_RESPONSE,
        packetResponse
      );
      sipController.makeCallClientKey = null;
    }
  },
  onReceivedSipAnswerPacket(messengerCenter: MessengerCenter, packetResponse) {
    // if (sipController.id == packetResponse.sipKey) {
    //   const myCallNumber = buildMyNumber(
    //     messengerCenter.domain,
    //     messengerCenter.userKey
    //   );
    //   if (
    //     TextUtils.isNotEmpty(packetResponse.sipNumber) &&
    //     myCallNumber != packetResponse.sipNumber
    //   ) {
    //     sipController.closeCall();
    //   }
    // }
  },
  onReceivedSipResultPacket(messengerCenter: MessengerCenter, packetResponse) {
    if (sipController.isLoaded()) {
      messengerCenter._sendDataChangedToApp(
        constantsApp.ACTION_SIP_RESULT,
        packetResponse
      );
    } else {
      sipController.cachedRequest = {
        action: constantsApp.ACTION_SIP_RESULT,
        data: packetResponse
      };
    }
  },
  onReceivedIncomingPacket(messengerCenter: MessengerCenter, packetResponse) {
    if (sipController.existCall()) {
      if (
        packetResponse.timeStamp >= sipController.timeStamp &&
        sipController.id != packetResponse.sipKey
      ) {
        //send busy
        const callNumber = buildMyNumber(
          messengerCenter.domain,
          messengerCenter.userKey
        );
        messengerCenter.sendSipResultAPI({
          roomKey: packetResponse.roomKey,
          fromKey: packetResponse.fromKey,
          toKey: packetResponse.toKey,
          sipKey: packetResponse.sipKey,
          video: packetResponse.video,
          sipNumber: callNumber,
          result: SIP_RESULT_STATUS.busy
        });
      }

      return;
    }

    messengerCenter._sendDataChangedToApp(
      constantsApp.ACTION_SIP_INCOMING_CALL,
      packetResponse
    );
  },
  onReceivedWhisperPacket(messengerCenter: MessengerCenter, packetResponse) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }
    messengerCenter._sendDataChangedToApp(
      constantsApp.ACTION_SHOW_NOTIFICATION_WITH_DATA,
      packetResponse
    );
  },
  onReceivedGroupwareCountPacket(
    messengerCenter: MessengerCenter,
    packetResponse
  ) {
    const accountID = messengerCenter.accountID;
    if (!messengerCenter.isAuthenticated || accountID < 0) {
      return;
    }
    messengerCenter._sendDataChangedToApp(
      constantsApp.ACTION_UPDATE_GROUPWARE_COUNT,
      packetResponse
    );
  },
  onReceivedLivePacket(messengerCenter: MessengerCenter, packetResponse) {
    safeRetryLiveTimeout = 0;
    messengerCenter.removeEarlyPingTimeout();
    if (packetResponse.statusList && packetResponse.statusList.length > 0) {
      talkDB.updateStatusLoginTypeContact(packetResponse.statusList);
      messengerCenter._sendORGStatusChangedNotify();
    }
  },
  onReceivedTimeCardPacket(messengerCenter: MessengerCenter, packetResponse) {
    messengerCenter._sendDataChangedToApp(
      constantsApp.ACTION_ORG_TIME_CARD_CHANGED,
      TalkConstants.splitStatusTimeCardVer2(packetResponse.statusStr)
    );
  }
};
