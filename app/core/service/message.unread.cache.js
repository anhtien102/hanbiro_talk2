export default class MarkPositionUnreadMessageCached {
  constructor() {
    this.data = {};
  }

  markUnreadMessagePosition(roomKey, createDate) {
    let info = this.data[roomKey];
    if (info) {
      return;
    } else {
      this.data[roomKey] = createDate;
    }
  }

  getUnreadMessageWithRoomKey(roomKey) {
    return this.data[roomKey];
  }

  clearMarkUnreadWithRoomKey(roomKey) {
    this.data[roomKey] = null;
  }

  clearAllMark() {
    this.data = {};
  }
}
