import talkApi from '../talk.api.render';
import axios from 'axios';
import parser from 'xml-js';
import fs from 'fs';
import { ipcRenderer } from 'electron';
import { resizeImageWithPathAndSave } from '../../utils/image.process';
import {
  REQUEST_COMMON_ACTION_FROM_RENDER,
  SEND_SOCKET_API_EVENT,
  API_SEND_FILE_CANCEL,
  API_SEND_FILE_HTTP_SENDING_COMPLETED_FINISHED,
  API_SEND_FILE_HTTP_FINISHED,
  API_WEB_AUTO_LOGIN,
  MAIN_TO_RENDER_EVENT,
  ACTION_FILE_UPLOAD_RES,
  ACTION_UPDATE_FILE_STATUS,
  THUMBNAIL_SIZE_LOCAL_STORE,
  THUMBNAIL_SIZE_LOCAL_STORE_QUALITY,
  REQUEST_COMMON_UPLOAD_DOWNLOAD_STATUS,
  ACTION_DOWNLOAD_UPLOAD_MAIN_TO_EXTRA,
  ACTION_DOWNLOAD_UPLOAD_EXTRA_TO_MAIN
} from '../../../configs/constant';

import {
  MessageFileStatus,
  keyOfMessage,
  generateThumbFileNameMessage,
  generateFileNameDownloadedMessage
} from '../talk-constants';

import { messsageUtils } from '../../model/MessageUtils';

import { debounceExt } from '../../../utils/debounce.utils';

const ENABLE_SHARE_DOWNLOAD_UPLOAD = true;

const MAX_REQUESTS_COUNT = 5;
const INTERVAL_MS = 100;
let PENDING_DOWNLOAD_REQUESTS = 0;
let PENDING_UPLOAD_REQUESTS = 0;

class DownloadUploadManager {
  initAxios(pendingRequest) {
    const api = axios.create({});
    /**
     * Axios Request Interceptor
     */
    api.interceptors.request.use(function(config) {
      return new Promise((resolve, reject) => {
        let interval = setInterval(() => {
          if (pendingRequest < MAX_REQUESTS_COUNT) {
            pendingRequest++;
            clearInterval(interval);
            resolve(config);
          }
        }, INTERVAL_MS);
      });
    });
    /**
     * Axios Response Interceptor
     */
    api.interceptors.response.use(
      function(response) {
        pendingRequest = Math.max(0, pendingRequest - 1);
        return Promise.resolve(response);
      },
      function(error) {
        pendingRequest = Math.max(0, pendingRequest - 1);
        return Promise.reject(error);
      }
    );
    return api;
  }

  constructor() {
    this.keepCancelToken = {};
    this.mapListener = {};
    this.queueDownloads = {};
    this.queueUploads = {};
    PENDING_DOWNLOAD_REQUESTS = 0;
    PENDING_UPLOAD_REQUESTS = 0;
    this.axiosDownloader = this.initAxios(PENDING_DOWNLOAD_REQUESTS);
    this.axiosUploader = this.initAxios(PENDING_UPLOAD_REQUESTS);

    this.lastAutoLoginExpired = null;

    this.broacastOtherView = debounceExt(
      this.broacastOtherView.bind(this),
      500,
      1000
    );
  }

  initFunc() {
    ipcRenderer.on(MAIN_TO_RENDER_EVENT, (event, action, args) => {
      if (action == ACTION_FILE_UPLOAD_RES) {
        // console.log(args);
        const roomKey = args.roomKey;
        const numberKey = args.numberKey;
        const fileName = args.name;
        const userKey = args.userKey;
        const serverStoragePath = args.storagePath;
        const fileKey = args.fileKey;
        const hostName = args.hostName;
        const fileSize = args.size;
        let newFilePath = args.useServerResponsePath
          ? args.path
          : talkApi.filePathDownloadedWithName(
              roomKey,
              generateFileNameDownloadedMessage(numberKey, fileName)
            );
        const key = keyOfMessage(roomKey, numberKey);
        this.startUploadFileWithPath(
          key,
          roomKey,
          userKey,
          numberKey,
          serverStoragePath,
          args.path,
          newFilePath,
          fileKey,
          fileName,
          hostName,
          fileSize,
          args.fileType,
          args.receiver,
          args.createDate,
          args.serverFileName,
          args.recordTime
        );
      } else if (action == ACTION_DOWNLOAD_UPLOAD_EXTRA_TO_MAIN) {
        if (args.request == 'stopUploadIfCan') {
          this.stopUploadIfCan(args.key);
          return;
        }

        if (args.request == 'stopDownloadIfCan') {
          this.stopDownloadIfCan(args.key);
          return;
        }

        if (args.request == 'startDownloadIfCan') {
          const data = this.startDownloadIfCan(
            args.key,
            args.downloadItem,
            args.filePathDownloaded,
            args.ignoreSaveFilePath
          );
          this.broacastOtherView(args.key, data);
          return;
        }
      }
    });
  }

  destroy() {
    ipcRenderer.removeAllListeners(MAIN_TO_RENDER_EVENT);
    this.stopAllTask();
  }

  stopAllTask() {
    for (let key in this.keepCancelToken) {
      if (this.keepCancelToken[key]) {
        this.keepCancelToken[key].cancel();
      }
    }

    this.keepCancelToken = {};
    this.queueDownloads = {};
    this.queueUploads = {};
  }

  addListener(key, category, listener) {
    let value = this.mapListener[key];
    if (value) {
      value[category] = listener;
    } else {
      value = {};
      value[category] = listener;
    }
    this.mapListener[key] = value;
  }

  removeListener(key, category) {
    let value = this.mapListener[key];
    if (value) {
      value[category] = null;
    }
  }

  broacastOtherView(key, data) {
    ipcRenderer.send(REQUEST_COMMON_UPLOAD_DOWNLOAD_STATUS, {
      action: ACTION_DOWNLOAD_UPLOAD_MAIN_TO_EXTRA,
      obj: {
        key: key,
        data: data
      }
    });
  }

  broadcastListener(key, data) {
    if (ENABLE_SHARE_DOWNLOAD_UPLOAD) {
      this.broacastOtherView(key, data);
    }

    let value = this.mapListener[key];
    if (value) {
      for (let callBackKey in value) {
        let callBack = value[callBackKey];
        if (callBack) {
          callBack(data);
        }
      }
    }
  }

  removeDownloadKey(key) {
    this.keepCancelToken[key] = null;
    this.queueDownloads[key] = null;
  }

  removeUploadKey(key) {
    this.keepCancelToken[key] = null;
    this.queueUploads[key] = null;
  }

  stopDownloadIfCan(key) {
    let token = this.keepCancelToken[key];
    if (token) {
      token.cancel();
    }
    this.removeDownloadKey(key);
  }

  stopUploadIfCan(key) {
    let token = this.keepCancelToken[key];
    if (token) {
      token.cancel();
    }
    this.removeUploadKey(key);
  }

  startDownloadIfCan(
    key,
    downloadItem,
    filePathDownloaded,
    ignoreSaveFilePath
  ) {
    let download = this.queueDownloads[key];
    if (download) {
      return {
        key: key,
        percent: download,
        isDownload: true,
        finished: false,
        error: null
      };
    }
    console.log('startDownloading', key, downloadItem);

    this.queueDownloads[key] = 0.01;

    let fileName = encodeURIComponent(downloadItem.msgdtFileName);
    let serverFilePath = downloadItem.msgdtHttpServerFilePath;
    let serverFileName = downloadItem.msgdtHttpServerFileName;
    let hostName = downloadItem.msgdtHttpServerHostName;
    let fileSize = downloadItem.msgdtFileSize ?? 0;
    let roomKey = downloadItem.msgRoomKey ?? '';
    let numberKey = downloadItem.msgNumberKey ?? '';

    const urlDownload = messsageUtils.isClouddiskFile(downloadItem)
      ? downloadItem.msgdtIP
      : talkApi.downloadUploadFileURL(hostName);

    const CancelToken = axios.CancelToken;
    const curCancelToken = CancelToken.source();
    this.keepCancelToken[key] = curCancelToken;

    const autoLoginKey = talkApi.autoLoginKey;

    this.axiosDownloader
      .post(urlDownload, null, {
        headers: {
          'Content-Type': 'xml',
          HANBIRO_MODE: 'get',
          'HANBIRO-MODE': 'get',
          HANBIRO_FILENAME: fileName,
          'HANBIRO-FILENAME': fileName,
          HANBIRO_SERVERPATH: serverFilePath,
          'HANBIRO-SERVERPATH': serverFilePath,
          HANBIRO_SERVERFILENAME: serverFileName,
          'HANBIRO-SERVERFILENAME': serverFileName,
          HANBIRO_LOGINKEY: autoLoginKey,
          'HANBIRO-LOGINKEY': autoLoginKey,
          HANBIRO_SENDER: downloadItem.msgUserKey,
          'HANBIRO-SENDER': downloadItem.msgUserKey
        },
        cancelToken: curCancelToken.token,
        responseType: 'arraybuffer',
        onDownloadProgress: progressEvent => {
          // console.log(progressEvent);

          let percent = (progressEvent.loaded * 100) / progressEvent.total;
          this.queueDownloads[key] = percent;

          this.broadcastListener(key, {
            key: key,
            percent: percent,
            isDownload: true,
            finished: false,
            error: null
          });
        }
      })
      .then(response => {
        // console.log(response);
        let ok = false;
        if (response.status == 200) {
          const buffer = Buffer.from(response.data);

          if (buffer.length == fileSize) {
            // download ok
            ok = true;
            let newFilePath =
              filePathDownloaded ??
              talkApi.filePathDownloadedWithName(
                roomKey,
                generateFileNameDownloadedMessage(
                  numberKey,
                  downloadItem.msgdtFileName
                )
              );
            fs.writeFile(newFilePath, buffer, () => {
              const tmpThumPath = talkApi.filePathThumbnailWithName(
                roomKey,
                generateThumbFileNameMessage(numberKey)
              );

              resizeImageWithPathAndSave(
                newFilePath,
                THUMBNAIL_SIZE_LOCAL_STORE,
                tmpThumPath,
                THUMBNAIL_SIZE_LOCAL_STORE_QUALITY,
                downloadItem.msgdtFileName,
                () => {
                  this.broadcastListener(key, {
                    key: key,
                    percent: 100,
                    filePathDownloaded: newFilePath,
                    isDownload: true,
                    finished: true,
                    error: null
                  });

                  this.removeDownloadKey(key);

                  if (!ignoreSaveFilePath && filePathDownloaded) {
                    ipcRenderer.send(
                      REQUEST_COMMON_ACTION_FROM_RENDER,
                      ACTION_UPDATE_FILE_STATUS,
                      {
                        roomKey: roomKey,
                        numberKey: numberKey,
                        filePath: filePathDownloaded
                      }
                    );
                  }

                  // WE NEED SEND FILE FINISHED FOR DOWNLOAD OR NOT???
                  // ipcRenderer.send(
                  //   SEND_SOCKET_API_EVENT,
                  //   API_SEND_FILE_HTTP_FINISHED,
                  //   {
                  //     sender: downloadItem.msgUserKey,
                  //     fileKey: downloadItem.msgdtFileKey,
                  //     number: numberKey,
                  //     fileSize: fileSize,
                  //     roomKey: roomKey,
                  //     fileType: 'IMAGE',
                  //     filePath: downloadItem.msgdtFilePath,
                  //     fileName: downloadItem.msgdtFileName,
                  //     receiver: '',
                  //     serverFilePath: serverFilePath,
                  //     time: downloadItem.msgCreateDate,
                  //     serverFileName: serverFileName,
                  //     type: 'DOWNLOAD',
                  //     recordTime: downloadItem.msgdtFileRecordTime
                  //   }
                  // );
                }
              );
            });
          }
        }
        if (!ok) {
          this.broadcastListener(key, {
            key: key,
            percent: -1,
            isDownload: true,
            finished: true,
            error: 'Failed'
          });

          this.removeDownloadKey(key);
        }
      })
      .catch(error => {
        if (error.response && error.response.status == 401) {
          this.sendWebAutoLogin(autoLoginKey);
        }
        this.broadcastListener(key, {
          key: key,
          percent: -1,
          isDownload: true,
          finished: true,
          error: error
        });

        this.removeDownloadKey(key);
      });
    return {
      key: key,
      percent: 0,
      isDownload: true,
      finished: false,
      error: null
    };
  }

  sendWebAutoLogin(autoLoginKey) {
    console.log('token is expired');
    if (this.lastAutoLoginExpired != autoLoginKey) {
      this.lastAutoLoginExpired = autoLoginKey;
      ipcRenderer.send(SEND_SOCKET_API_EVENT, API_WEB_AUTO_LOGIN, null);
    }
  }

  tryGetProgressUpload(key) {
    let upload = this.queueUploads[key];
    if (upload) {
      return {
        key: key,
        percent: upload,
        isDownload: false,
        finished: false,
        error: null
      };
    }
    return null;
  }

  tryGetProgressDownload(key) {
    let download = this.queueDownloads[key];
    if (download) {
      return {
        key: key,
        percent: download,
        isDownload: true,
        finished: false,
        error: null
      };
    }
    return null;
  }

  startUploadFileWithPath(
    key,
    roomKey,
    userKey,
    numberKey,
    serverStoragePath,
    filePathLocal,
    filePath,
    fileKey,
    fileName,
    hostName,
    fileSize,
    fileType,
    receiver,
    time,
    serverFileName,
    recordTime
  ) {
    let upload = this.queueUploads[key];
    if (upload) {
      return;
    }

    console.log('startUploading', key, fileName);
    this.queueUploads[key] = 0.01;

    fs.readFile(filePath, (error, data) => {
      if (error) {
        this.broadcastListener(key, {
          key: key,
          percent: -1,
          isDownload: false,
          finished: true,
          error: error
        });

        this.removeUploadKey(key);
        ipcRenderer.send(
          REQUEST_COMMON_ACTION_FROM_RENDER,
          ACTION_UPDATE_FILE_STATUS,
          {
            roomKey: roomKey,
            numberKey: numberKey,
            actionFailedUpload: true
          }
        );
        ipcRenderer.send(SEND_SOCKET_API_EVENT, API_SEND_FILE_CANCEL, {
          roomKey: roomKey,
          fileKey: fileKey
        });
        return;
      }

      const file = new File([data], fileName);
      let formData = new FormData();
      formData.append('userfile', file);

      const CancelToken = axios.CancelToken;
      const curCancelToken = CancelToken.source();
      this.keepCancelToken[key] = curCancelToken;
      this.axiosUploader
        .post(talkApi.downloadUploadFileURL(hostName), formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
            HANBIRO_MODE: 'put',
            'HANBIRO-MODE': 'put',
            HANBIRO_SENDER: userKey,
            'HANBIRO-SENDER': userKey,
            HANBIRO_STORAGEPATH: serverStoragePath,
            'HANBIRO-STORAGEPATH': serverStoragePath,
            HANBIRO_FILEKEY: fileKey,
            'HANBIRO-FILEKEY': fileKey
          },
          onUploadProgress: progressEvent => {
            let percent = (progressEvent.loaded * 100) / progressEvent.total;
            this.queueUploads[key] = percent;
            this.broadcastListener(key, {
              key: key,
              percent: percent,
              isDownload: false,
              finished: false,
              error: null
            });
          },
          cancelToken: curCancelToken.token
        })
        .then(response => {
          if (response.status == 200) {
            try {
              const jsonData = parser.xml2js(response.data, { compact: false });
              const childXML = jsonData.elements[0].elements[0];
              if (childXML.name == 'FILE') {
                const result = childXML.attributes['RESULT'];
                const serverFilePath = childXML.attributes['FILEPATH'] ?? '';
                if (result > 0) {
                  ipcRenderer.send(
                    SEND_SOCKET_API_EVENT,
                    API_SEND_FILE_HTTP_SENDING_COMPLETED_FINISHED,
                    {
                      sender: userKey,
                      fileKey: fileKey,
                      number: numberKey,
                      fileSize: fileSize,
                      roomKey: roomKey,
                      fileType: fileType,
                      filePath: filePathLocal,
                      fileName: fileName,
                      receiver: receiver,
                      serverFilePath: serverFilePath,
                      time: time,
                      serverFileName: serverFileName,
                      type: 'UPLOAD',
                      recordTime: recordTime
                    }
                  );
                  this.removeUploadKey(key);
                  return;
                }
              }
            } catch (error) {
              console.log(error);
            }
          }

          this.broadcastListener(key, {
            key: key,
            percent: -1,
            isDownload: false,
            finished: true,
            error: 'Failed'
          });

          this.removeUploadKey(key);
          ipcRenderer.send(
            REQUEST_COMMON_ACTION_FROM_RENDER,
            ACTION_UPDATE_FILE_STATUS,
            {
              roomKey: roomKey,
              numberKey: numberKey,
              actionFailedUpload: true
            }
          );
          ipcRenderer.send(SEND_SOCKET_API_EVENT, API_SEND_FILE_CANCEL, {
            roomKey: roomKey,
            fileKey: fileKey
          });
        })
        .catch(error => {
          this.broadcastListener(key, {
            key: key,
            percent: -1,
            isDownload: false,
            finished: true,
            error: error
          });

          this.removeUploadKey(key);
          ipcRenderer.send(
            REQUEST_COMMON_ACTION_FROM_RENDER,
            ACTION_UPDATE_FILE_STATUS,
            {
              roomKey: roomKey,
              numberKey: numberKey,
              actionFailedUpload: true
            }
          );
          ipcRenderer.send(SEND_SOCKET_API_EVENT, API_SEND_FILE_CANCEL, {
            roomKey: roomKey,
            fileKey: fileKey
          });
        });
    });
  }
}

const downloadUploadManager = new DownloadUploadManager();
export default downloadUploadManager;
