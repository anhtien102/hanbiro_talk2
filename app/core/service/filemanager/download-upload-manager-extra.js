import { ipcRenderer } from 'electron';
import {
  REQUEST_COMMON_UPLOAD_DOWNLOAD_STATUS,
  ACTION_DOWNLOAD_UPLOAD_EXTRA_TO_MAIN
} from '../../../configs/constant';

class DownloadUploadManagerExtra {
  constructor() {
    this.mapListener = {};
  }

  initFunc() {
    ipcRenderer.on(
      REQUEST_COMMON_UPLOAD_DOWNLOAD_STATUS,
      (event, action, args) => {
        if (args.data.percent == 100 && args.data.isDownload == false) {
          return;
        }
        this.broadcastListener(args.key, args.data);
      }
    );
  }

  destroy() {
    ipcRenderer.removeAllListeners(REQUEST_COMMON_UPLOAD_DOWNLOAD_STATUS);
    this.stopAllTask();
  }

  stopAllTask() {}

  addListener(key, category, listener) {
    let value = this.mapListener[key];
    if (value) {
      value[category] = listener;
    } else {
      value = {};
      value[category] = listener;
    }
    this.mapListener[key] = value;
  }

  removeListener(key, category) {
    let value = this.mapListener[key];
    if (value) {
      value[category] = null;
    }
  }
  broadcastListener(key, data) {
    let value = this.mapListener[key];
    if (value) {
      for (let callBackKey in value) {
        let callBack = value[callBackKey];
        if (callBack) {
          callBack(data);
        }
      }
    }
  }

  stopDownloadIfCan(key) {
    ipcRenderer.send(REQUEST_COMMON_UPLOAD_DOWNLOAD_STATUS, {
      action: ACTION_DOWNLOAD_UPLOAD_EXTRA_TO_MAIN,
      obj: {
        request: 'stopDownloadIfCan',
        key: key
      }
    });
  }

  stopUploadIfCan(key) {
    ipcRenderer.send(REQUEST_COMMON_UPLOAD_DOWNLOAD_STATUS, {
      action: ACTION_DOWNLOAD_UPLOAD_EXTRA_TO_MAIN,
      obj: {
        request: 'stopUploadIfCan',
        key: key
      }
    });
  }

  startDownloadIfCan(
    key,
    downloadItem,
    filePathDownloaded,
    ignoreSaveFilePath
  ) {
    ipcRenderer.send(REQUEST_COMMON_UPLOAD_DOWNLOAD_STATUS, {
      action: ACTION_DOWNLOAD_UPLOAD_EXTRA_TO_MAIN,
      obj: {
        request: 'startDownloadIfCan',
        key,
        downloadItem,
        filePathDownloaded,
        ignoreSaveFilePath
      }
    });
  }

  tryGetProgressUpload(key) {
    ipcRenderer.send(REQUEST_COMMON_UPLOAD_DOWNLOAD_STATUS, {
      action: ACTION_DOWNLOAD_UPLOAD_EXTRA_TO_MAIN,
      obj: {
        request: 'tryGetProgressUpload',
        key: key
      }
    });
    return null;
  }

  tryGetProgressDownload(key) {
    ipcRenderer.send(REQUEST_COMMON_UPLOAD_DOWNLOAD_STATUS, {
      action: ACTION_DOWNLOAD_UPLOAD_EXTRA_TO_MAIN,
      obj: {
        request: 'tryGetProgressDownload',
        key: key
      }
    });
    return null;
  }
}

const downloadUploadManagerExtra = new DownloadUploadManagerExtra();
export default downloadUploadManagerExtra;
