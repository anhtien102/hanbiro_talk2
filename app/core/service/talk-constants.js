import TextUtils from '../utils/text.util';

export const LOGIN_TYPE_WEB = 1;
export const LOGIN_TYPE_WINDOWS = 2;
export const LOGIN_TYPE_MAC = 3;
export const LOGIN_TYPE_MOBILE = 4;
export const LOGIN_TYPE_DUAL = 5;

export const GROUP_TYPE_ADMIN = 0;
export const GROUP_TYPE_USER = 1;

export const ROOM_STATUS_LIVE = 1;
export const ROOM_STATUS_OUT = 2;

export const ROOM_STATUS_READED = 1;
export const ROOM_STATUS_UNREADED = 0;

export const ROOM_TYPE_LIVE = 1;
export const ROOM_TYPE_GUEST = 2;

export const ROOM_TYPE_NONE_STRING = 'ROOM_TYPE_NONE';
export const ROOM_TYPE_USER_STRING = 'U';
export const ROOM_TYPE_GUEST_STRING = 'G';

export const ROOM_DETAIL_STATUS_ENTER = 1;
export const ROOM_DETAIL_STATUS_LEAVE = 2;

export const LIMIT_MESSAGE_LENGTH = 1000;

export const CREATE_DATE_SENDING = 90000000000000;

// MESSAGE CONSTANT
export const MessageThirdParty = {
  MESSAGE_MESSENGER_UNKNOWN_METHOD: -1,
  MESSAGE_MESSENGER_ID: 0,
  MESSAGE_CLOUDDISK_ID: 1,
  MESSAGE_FAST_HTTP_ID: 2
};

export const MessageType = {
  MESSAGE_TYPE_TEXT: 1,
  MESSAGE_TYPE_PHOTO: 2,
  MESSAGE_TYPE_VIDEO: 3,
  MESSAGE_TYPE_FILE: 4,
  MESSAGE_TYPE_OUT: 5,
  MESSAGE_TYPE_INVITE: 6,
  MESSAGE_TYPE_RECORD: 7,
  MESSAGE_TYPE_CALL_LOG: 8,
  MESSAGE_TYPE_GROUP_CALL_LOG: 9,
  MESSAGE_TYPE_ROOM_ALIAS: 10,
  MESSAGE_TYPE_BOARD_INFO: 11
};

export const MessageSipConstant = {
  SIP_STATUS_CALLING: 0,
  SIP_STATUS_ME_REJECTED: 1,
  SIP_STATUS_OTHER_USER_REJECTED: 2,
  SIP_STATUS_CALLED: 3,

  GROUP_SIP_STATUS_IN: 0,
  GROUP_SIP_STATUS_OUT: 1,

  SIP_TYPE_VIDEO: 1,
  SIP_TYPE_VOICE: 0
};

export const MessageFileEXT = {
  IMAGE_EXT: 'IMAGE',
  VIDEO_EXT: 'VIDEO',
  RECORD_EXT: 'RECORD',
  ETC_EXT: 'ETC'
};

export const MessageFileStatus = {
  FILE_MSG_NORMAL: 0,
  FILE_MSG_UPLOAD_WORKING: 1,
  FILE_MSG_FINISHED: 2,
  FILE_MSG_DOWNLOAD_FAILED: 3,
  FILE_MSG_DOWNLOAD_WORKING: 4,
  FILE_MSG_UPLOAD_FAILED: 5,
  FILE_MSG_DOWNLOAD_FILE_NOT_FOUND: 6,
  FILE_MSG_DOWNLOAD_FILE_KEY_NOT_FOUND: 7,
  FILE_MSG_FROM_ANOTHER_DEVICE: 8,
  FILE_MSG_DOWNLOAD_INTERRUPT: 9, // Kill app or something wrong
  FILE_MSG_UPLOAD_INTERRUPT: 10 // Kill app or something wrong
};

export const MessageStatus = {
  STATUS_READ: 1,
  STATUS_CLOSED: 2,
  STATUS_UNREAD: 3
};

export const MessageServerStatus = {
  MARK_UNREAD: 1,
  MARK_READ: 2,
  MARK_SERVER_READ: 3
};

export const MessageSendStatus = {
  STATUS_SEND_COMPLETED: 0,
  STATUS_SEND_PENDING: 1,
  STATUS_SEND_SENDING: 2,
  STATUS_SEND_MAYBE_PENDING: 3,
  STATUS_SEND_ANY: 100
};

function fileExtension(fileName) {
  if (fileName == null || fileName == '') {
    return '';
  }
  let posDot = fileName.lastIndexOf('.');
  if (posDot < 0) {
    return '';
  }

  return fileName.substring(posDot + 1).toLowerCase();
}

export function keyOfMessage(roomKey, number) {
  return roomKey + number;
}

export function generateFileNameDownloadedMessage(numberKey, fileName) {
  return numberKey + '.' + fileExtension(fileName);
}

export function generateThumbFileNameMessage(numberKey) {
  return numberKey + '_thumb';
}

export function isContainExactUserKeyInList(list, userKey) {
  if (TextUtils.isEmpty(list)) {
    return false;
  }
  let result = list.split('|', -1);
  return result && result.includes(userKey);
}

export function getNumberUsersAMessage(str) {
  if (!TextUtils.isEmpty(str)) {
    const array = str.split('|');
    if (array.length > 0) {
      if (array[0] != '' && array[array.length - 1] != '') {
        return array.length;
      }

      if (array[0] != '' || array[array.length - 1] != '') {
        return array.length - 1;
      }

      return array.length - 2;
    }
  }
  return 0;
}

export function getFileExtension(fileName) {
  if (TextUtils.isEmpty(fileName)) {
    return MessageFileEXT.ETC_EXT;
  }

  let posDot = fileName.lastIndexOf('.');
  if (posDot < 0) {
    return MessageFileEXT.ETC_EXT;
  }

  const ext = fileName.substring(posDot + 1).toLowerCase();
  if (ext == 'jpg' || ext == 'png' || ext == 'gif' || ext == 'jpeg') {
    return MessageFileEXT.IMAGE_EXT;
  }

  if (
    ext == 'mp4' ||
    ext == 'flv' ||
    ext == 'mov' ||
    ext == '3gp' ||
    ext == 'mkv' ||
    ext == 'webm'
  ) {
    return MessageFileEXT.VIDEO_EXT;
  }

  if (ext == 'mp3' || ext == 'm4a') {
    return MessageFileEXT.RECORD_EXT;
  }

  return MessageFileEXT.ETC_EXT;
}

export function typeMessageFromFileType(fileType) {
  if (fileType == MessageFileEXT.IMAGE_EXT) {
    return MessageType.MESSAGE_TYPE_PHOTO;
  }

  if (fileType == MessageFileEXT.VIDEO_EXT) {
    return MessageType.MESSAGE_TYPE_VIDEO;
  }

  if (fileType == MessageFileEXT.RECORD_EXT) {
    return MessageType.MESSAGE_TYPE_RECORD;
  }

  return MessageType.MESSAGE_TYPE_FILE;
}

export function getMessageSipTypeFromString(str) {
  if (TextUtils.isEmpty(str)) {
    return MessageSipConstant.SIP_TYPE_VOICE;
  }

  if (str == '1') {
    return MessageSipConstant.SIP_TYPE_VIDEO;
  }

  return MessageSipConstant.SIP_TYPE_VOICE;
}

export function getMessageSipStatusFromString(str, received, duration) {
  if (TextUtils.isEmpty(str) || str == 'SIP') {
    return MessageSipConstant.SIP_STATUS_CALLING;
  }

  if (duration > 0) {
    return MessageSipConstant.SIP_STATUS_CALLED;
  }

  if (str == 'SIPHANGUP') {
    if (received) {
      return MessageSipConstant.SIP_STATUS_OTHER_USER_REJECTED;
    }
    return MessageSipConstant.SIP_STATUS_ME_REJECTED;
  }

  if (str == 'SIPRESULT') {
    if (received) {
      return MessageSipConstant.SIP_STATUS_ME_REJECTED;
    }
    return MessageSipConstant.SIP_STATUS_OTHER_USER_REJECTED;
  }

  if (str == 'SIPCLOSE') {
    if (received) {
      return MessageSipConstant.SIP_STATUS_CALLED;
    }
    return MessageSipConstant.SIP_STATUS_CALLED;
  }

  if (received) {
    return MessageSipConstant.SIP_STATUS_OTHER_USER_REJECTED;
  }
  return MessageSipConstant.SIP_STATUS_ME_REJECTED;
}

export function getMessageGroupSipStatusFromString(str) {
  if (TextUtils.isEmpty(str)) {
    return MessageSipConstant.GROUP_SIP_STATUS_IN;
  }

  if (str == 'IN') {
    return MessageSipConstant.GROUP_SIP_STATUS_IN;
  }

  if (str == 'OUT') {
    return MessageSipConstant.GROUP_SIP_STATUS_OUT;
  }

  return MessageSipConstant.GROUP_SIP_STATUS_IN;
}

export function loginTypeFromString(loginType: string) {
  if (loginType == null || loginType == '') {
    return LOGIN_TYPE_WINDOWS;
  }

  let dual = loginType.split(',');
  if (dual.length > 1) {
    let empty0 = dual[0] == null || dual[0] == '' ? true : false;
    let empty1 = dual[1] == null || dual[1] == '' ? true : false;
    if (!empty0 && !empty1) {
      return LOGIN_TYPE_DUAL;
    }

    if (!empty0) {
      return parseInt(dual[0]);
    }

    if (!empty0) {
      return parseInt(dual[1]);
    }
    return LOGIN_TYPE_WINDOWS;
  }

  let type = parseInt(loginType);
  if (type) {
    return type;
  }
  return LOGIN_TYPE_WINDOWS;
}

export function splitStatusLoginVer2(statusList: string) {
  let result = [];
  if (statusList == null || statusList == '') {
    return result;
  }

  let array = statusList.split('|');
  if (array && array.length > 0) {
    for (let index = 0; index < array.length; index++) {
      const element = array[index];
      if (element) {
        const arrStatus = element.split(':');
        if (arrStatus && arrStatus.length >= 3) {
          let statusObj = {
            userKey: arrStatus[0],
            status: arrStatus[1],
            loginType: loginTypeFromString(arrStatus[2])
          };
          result.push(statusObj);
        }
      }
    }
  }
  return result;
}

export function splitStatusTimeCardVer2(statusList: string) {
  let result = {};
  if (statusList == null || statusList == '') {
    return result;
  }

  let array = statusList.split('|');
  if (array && array.length > 0) {
    for (let index = 0; index < array.length; index++) {
      const element = array[index];
      if (element) {
        const arrStatus = element.split(':');
        if (arrStatus && arrStatus.length >= 4) {
          let statusObj = {
            userKey: arrStatus[0],
            holiday: parseInt(arrStatus[1]),
            workOfSaturday: parseInt(arrStatus[2]),
            birthDay: parseInt(arrStatus[3])
          };
          result[statusObj.userKey] = statusObj;
        }
      }
    }
  }
  return result;
}

export function splitUserKey(users: string) {
  if (users == null || users == '') {
    return [];
  }

  return users.split(',');
}

export function timeFromTimeStamp(time) {
  if (!time || time == '') {
    return Date.now();
  }
  return time;
}

export function timeFromTimeStampSafe(time) {
  if (!time || time == '') {
    return Date.now();
  }
  if (time.length < 12) {
    return time * 1000;
  }
  return time;
}

export function isLiveUser(userKey) {
  return userKey.endsWith('L');
}

export function isGuestUser(userKey) {
  return userKey.endsWith('G');
}

export const StatusMode = {
  logout: 0,
  available: 1,
  away: 2,
  busy: 3,
  meeting: 4,
  meal: 5,
  phone: 6,
  out: 7,
  offline: 8,
  business_trip: 9
};
