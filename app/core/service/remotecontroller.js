class RemoteControlWindowsObject {
  constructor() {
    this.view = null;
  }

  reset() {
    this.view = null;
  }

  existRemoteControlPage() {
    return this.view != null;
  }

  moveToTop() {
    if (this.view) {
      this.view.show();
      this.view.moveTop();
      this.view.focus();
    }
  }

  closeRemoteControl() {
    if (this.view) {
      this.view.close();
    }
    this.reset();
  }

  sendData(event, actionName, data) {
    if (this.view) {
      this.view.webContents.send(event, actionName, data);
    }
  }
}

const remoteControlObject = new RemoteControlWindowsObject();
export default remoteControlObject;
