export default class GetTalkCached {
  constructor() {
    this.data = {};
    this.prevData = {};
  }

  updateTalkInfo(roomKey, latestTime, talkDB) {
    let info = this.data[roomKey];
    if (info) {
      if (info.latestTime > 0) {
        if (latestTime && info.latestTime < latestTime) {
          info.latestTime = latestTime;
        }
      } else {
        if (latestTime && info.oldTime > latestTime) {
          info.oldTime = latestTime;
        }
      }
      this.data[roomKey] = info;
      if (talkDB) {
        talkDB.insertOrUpdateTalkInfo(roomKey, info.oldTime, info.latestTime);
      }
    } else {
      this.data[roomKey] = {
        oldTime: latestTime,
        latestTime: 0
      };
      if (talkDB) {
        talkDB.insertOrUpdateTalkInfo(roomKey, latestTime, 0);
      }
    }
    // console.log('updateTalkInfo', this.data[roomKey]);
  }

  addTalkInfo(roomKey, oldTime, latestTime, talkDB) {
    let info = this.data[roomKey];
    if (info) {
      if (latestTime && info.latestTime < latestTime) {
        info.latestTime = latestTime;
      }
      if (oldTime && info.oldTime > oldTime) {
        info.oldTime = oldTime;
      }

      const prev = this.getPrevTalkInfo(roomKey, talkDB);
      // console.log(prev, oldTime);
      if (
        prev &&
        prev.rtPrevLatestTime > 0 &&
        prev.rtPrevOldTime > 0 &&
        info.oldTime <= prev.rtPrevLatestTime
      ) {
        if (info.oldTime > prev.rtPrevOldTime) {
          info.oldTime = prev.rtPrevOldTime;
        }
      }

      this.data[roomKey] = info;
      if (talkDB) {
        talkDB.insertOrUpdateTalkInfo(roomKey, info.oldTime, info.latestTime);
      }
    } else {
      let tmpOldTime = oldTime;
      const prev = this.getPrevTalkInfo(roomKey, talkDB);
      if (
        prev &&
        prev.rtPrevLatestTime > 0 &&
        prev.rtPrevOldTime > 0 &&
        tmpOldTime <= prev.rtPrevLatestTime
      ) {
        if (tmpOldTime > prev.rtPrevOldTime) {
          tmpOldTime = prev.rtPrevOldTime;
        }
      }

      this.data[roomKey] = {
        oldTime: tmpOldTime,
        latestTime: latestTime
      };
      if (talkDB) {
        talkDB.insertOrUpdateTalkInfo(roomKey, tmpOldTime, latestTime);
      }
    }
    // console.log('addTalkInfo', this.data[roomKey]);
  }

  hasAvailable(roomKey) {
    return this.data[roomKey] != null;
  }

  getPrevTalkInfo(roomKey, talkDB) {
    let info = this.prevData[roomKey];
    if (!info) {
      info = talkDB.findPrevTalkInfo(roomKey);
      this.prevData[roomKey] = info;
    }
    return info;
  }

  getTalkInfo(roomKey) {
    return this.data[roomKey] ?? { oldTime: 0, latestTime: 0 };
  }

  clearWithRoomKey(roomKey) {
    this.data[roomKey] = null;
  }

  clearAll() {
    this.data = {};
    this.prevData = {};
  }

  infoCachedFromRoomKeyRequestTime(roomKey, requestTime) {
    let info = this.getTalkInfo(roomKey);
    if (info) {
      if (
        (requestTime == 0 && info.latestTime > 0 && info.oldTime > 0) ||
        (info.latestTime > requestTime && requestTime > info.oldTime)
      ) {
        return info;
      }
    }
    return null;
  }
}
