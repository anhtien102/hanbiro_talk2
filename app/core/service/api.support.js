export const CUSTOM_SUPPORT_CLOUDDISK = 'CUSTOM_SUPPORT_CLOUDDISK';
export const CUSTOM_SUPPORT_WHISPER = 'CUSTOM_SUPPORT_WHISPER';
export const CUSTOM_SUPPORT_CHAT_BOARD = 'CUSTOM_SUPPORT_CHAT_BOARD';

export default class ApiSupportList {
  constructor() {
    this.apiSupportList = null;
  }

  updateApiSupport(list) {
    this.apiSupportList = list
      ? list.split('|').filter(function(el) {
          return el.length != 0;
        })
      : [];
  }

  hasSupportApi(apiName) {
    return this.apiSupportList && this.apiSupportList.includes(apiName);
  }

  hasSupportUserModInfoApi() {
    return this.hasSupportApi('USERMODINFO');
  }

  hasSupportMFileCancelApi() {
    return this.hasSupportApi('MFILECANCEL');
  }

  hasSupportRoomPushAlertApi() {
    return this.hasSupportApi('ALERTROOM');
  }

  hasSupportChangeRoomName() {
    return this.hasSupportApi('ROOMNAME');
  }

  hasSupportRoomFavorite() {
    return this.hasSupportApi('FAVORITEROOM');
  }

  hasSupportDeleteMessage() {
    return this.hasSupportApi('CANCELMSG');
  }

  hasSupportClouddisk() {
    return this.hasSupportApi(CUSTOM_SUPPORT_CLOUDDISK);
  }

  hasSupportWhisper() {
    return this.hasSupportApi(CUSTOM_SUPPORT_WHISPER);
  }

  hasSupportChatBoard() {
    return this.hasSupportApi(CUSTOM_SUPPORT_CHAT_BOARD);
  }
}
