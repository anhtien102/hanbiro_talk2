import talkPathRender from '../utils/folder-manage/talk-folder.render';
import ApiSupportList from '../service/api.support';
import { remote } from 'electron';

const DEFAULT_BRANCH = '/ngw';
const HTTP_SCHEME = 'http://';
const HTTPS_SCHEME = 'https://';

class TalkAPI {
  constructor() {
    this.domain = '';
    this.branch = DEFAULT_BRANCH;
    this.autoLoginKey = '';
    this.newAutoLoginKey = '';
    this.shareObj = {};
    this.apiSupportList = new ApiSupportList();

    this.currentFirstRoomTime = 0;
    this.companyTitle = 'HanbiroTalk';
  }

  isCookiesSuccess() {
    return remote.getGlobal('ShareKeepValue').IS_GOT_COOKIES;
  }
  applyCookiesStatus(ok, gwToken = '') {
    remote.getGlobal('ShareKeepValue').IS_GOT_COOKIES = ok;
    remote.getGlobal('ShareKeepValue').GROUPWARE_TOKEN = gwToken;
  }

  transferSettingInfoFromElectronToBrowser() {
    this.shareObj = remote.getGlobal('ShareGlobalObject');
    this.apiSupportList.updateApiSupport(
      this.shareObj.extra_login_info?.supportAPIList
    );

    this.companyTitle = this.shareObj.extra_server_info?.title ?? 'HanbiroTalk';

    const account_info = this.shareObj.account_info;
    if (account_info) {
      this.domain = account_info.domain;
      this.newAutoLoginKey = account_info.auto_login_key;
      this.autoLoginKey = account_info.web_auto_login_key;
      talkPathRender.userKey = account_info.user_key;
    }
  }

  updateAllAccountInfo() {
    this.companyTitle = this.shareObj.extra_server_info?.title ?? 'HanbiroTalk';
    const account_info = this.shareObj.account_info;
    if (account_info) {
      this.domain = account_info.domain;
      this.newAutoLoginKey = account_info.auto_login_key;
      this.autoLoginKey = account_info.web_auto_login_key;
      talkPathRender.userKey = account_info.user_key;
    }
  }

  allowTranslate() {
    const extra_server_info = this.shareObj.extra_server_info;
    const translateObj = extra_server_info?.translate;
    return translateObj?.useTranslate;
  }

  allowVideoConference() {
    const extra_server_info = this.shareObj.extra_server_info;
    const videoConference = extra_server_info?.videoConference;
    return videoConference == '1';
  }

  allowAudioConference() {
    const extra_server_info = this.shareObj.extra_server_info;
    const audioroom = extra_server_info?.videoConferenceInfo?.audioroom;
    return audioroom == '1';
  }

  allowFileTransfer() {
    const extra_server_info = this.shareObj.extra_server_info;
    return extra_server_info?.allowFileTransfer;
  }

  allowCallOneOne() {
    const extra_server_info = this.shareObj.extra_server_info;
    const callOneOne = extra_server_info?.videoConferenceInfo?.callOneOne;
    return callOneOne == '1';
  }

  webrtcInformation() {
    const extra_server_info = this.shareObj.extra_server_info;
    return extra_server_info?.videoConferenceInfo;
  }

  allowRoomCreate() {
    const extra_server_info = this.shareObj.extra_server_info;
    const priviledge = extra_server_info.priviledge;
    let allow = true;
    if (priviledge && priviledge.roomcreate) {
      allow = priviledge.roomcreate > 0;
    }
    return allow;
  }

  allowRoomInvite() {
    const extra_server_info = this.shareObj.extra_server_info;
    const priviledge = extra_server_info.priviledge;
    let allow = true;
    if (priviledge && priviledge.roominvite) {
      allow = priviledge.roominvite > 0;
    }
    return allow;
  }

  allowRoomDelete() {
    const extra_server_info = this.shareObj.extra_server_info;
    const priviledge = extra_server_info.priviledge;
    let allow = true;
    if (priviledge && priviledge.roomout) {
      allow = priviledge.roomout > 0;
    }
    return allow;
  }

  dateTimeFormat() {
    const dateFormat = this.shareObj.extra_login_info?.dateformat;
    return dateFormat ?? 'YYYY-MM-DD';
  }

  dateTimeFormatWithTime() {
    const dateFormat = this.shareObj.extra_login_info?.datetimeformat;
    return dateFormat ?? 'YYYY-MM-DD HH:mm';
  }

  photoUserDir() {
    return talkPathRender.photoFolder();
  }
  appStateDir() {
    return talkPathRender.appStateFolder();
  }

  filePathDownloadedWithName(roomKey, fileName) {
    return talkPathRender.filePathDownloadedWithName(roomKey, fileName);
  }

  filePathThumbnailWithName(roomKey, fileName) {
    return talkPathRender.filePathThumbWithName(roomKey, fileName);
  }

  filePathDownloadedGroupwareWithName(key, fileName) {
    return talkPathRender.filePathDownloadedGroupwareWithName(key, fileName);
  }

  tempPath() {
    return talkPathRender.tempPath();
  }

  portableDirPath() {
    return talkPathRender.portableDirPath();
  }

  imageResourceDirPathWithDir(dir) {
    return talkPathRender.imageResourceDirPathWithDir(dir);
  }

  gifsFolderPath() {
    return talkPathRender.gifsFolder();
  }

  tempPathWithName(name) {
    return talkPathRender.tempPathWithName(name);
  }

  gwUserPhotoURL(
    userkey,
    auth = true,
    width = 100,
    height = 100,
    phototime = 0
  ) {
    return `${HTTP_SCHEME}${
      this.domain
    }/eapproval/photo_mobiletalk.php?userkey=${userkey}&width=${width}&height=${height}&authkey=${
      auth ? this.newAutoLoginKey : ''
    }&phototime=${phototime}`;
  }

  // move = @"mail";
  //           move = @"approval";
  //           move = @"whisper";
  //           move = @"board";
  //           move = @"todo";
  //           move = @"circular";
  //           move = @"task";
  //           move = @"main";
  externalGroupwareURL(action) {
    if (this.autoLoginKey && this.autoLoginKey != '') {
      return `${HTTP_SCHEME}${this.domain}/cgi-bin/NEW/summary_arg.do?foo=login&move=${action}&etc=&key=${this.autoLoginKey}`;
    }
    return '';
  }

  downloadUploadFileURL(hostName) {
    return `${HTTPS_SCHEME}${
      hostName == null || hostName == '' ? this.domain : hostName
    }/winapp/hcsong/messenger_file.php`;
  }

  getCookie() {
    return `${HTTPS_SCHEME}${this.domain}${this.branch}/sign/tunnel_web?connectd=mac_messenger&mode=tunnel&cookie=true`;
  }

  whisperList() {
    return `${HTTPS_SCHEME}${this.domain}${this.branch}/whisper/main/list`;
  }

  whisperView() {
    return `${HTTPS_SCHEME}${this.domain}${this.branch}/whisper/main/getconversation`;
  }

  whisperWrite() {
    return `${HTTPS_SCHEME}${this.domain}${this.branch}/whisper/main/insert`;
  }

  baseBoardList(action) {
    return `${HTTPS_SCHEME}${this.domain}${this.branch}/messenger/messenger_board/${action}`;
  }

  boardList() {
    return this.baseBoardList('board_list');
  }

  boardFileList() {
    return this.baseBoardList('boardfilesbyroom');
  }

  boardInsert() {
    return this.baseBoardList('board_in');
  }

  boardDelete() {
    return this.baseBoardList('boarddelete');
  }

  boardCommentList() {
    return this.baseBoardList('boardcomment_list');
  }

  boardCommentInsert() {
    return this.baseBoardList('boardcomment_in');
  }

  boardCommentDelete() {
    return this.baseBoardList('boardcommentdelete');
  }

  boardThumbnailInsert() {
    return this.baseBoardList('thumbnailregi');
  }

  boardThumbnailGet(thumbId) {
    return this.baseBoardList(`thumbnail_get/${thumbId}`);
  }

  boardFileUpload() {
    return this.baseBoardList(`fileupload`);
  }

  boardFileDownload(id) {
    return this.baseBoardList(`filedownload/${id}`);
  }

  boardNewNofication() {
    return this.baseBoardList(`boardgetnewboard`);
  }

  boardFileDelete() {
    return this.baseBoardList(`boardfiledel`);
  }

  boardReadSave() {
    return this.baseBoardList(`boardreadsave`);
  }

  videoConferenceURL(roomKey) {
    const extra_server_info = this.shareObj.extra_server_info;
    const videoConferenceInfo = extra_server_info?.videoConferenceInfo;
    if (
      videoConferenceInfo.server == null ||
      videoConferenceInfo.server == ''
    ) {
      return `${HTTPS_SCHEME}${this.domain}:3738/?view=1&roomkey=${roomKey}&loginkey=${this.autoLoginKey}&domain=${this.domain}`;
    } else {
      return `${HTTPS_SCHEME}${videoConferenceInfo.server}/?view=1&roomkey=${roomKey}&loginkey=${this.autoLoginKey}&domain=${this.domain}`;
    }
  }

  historyList(startDate, endDate, page, keyword, roomKey, sort) {
    let encodeKeyword = encodeURI(keyword);
    let url = `${HTTPS_SCHEME}${this.domain}${this.branch}/messenger/messenger/new_search?loginkey=${this.autoLoginKey}&format=json&start_date=${startDate}&end_date=${endDate}&sort=${sort}&from=${page}&size=20&keyword=${encodeKeyword}`;
    if (roomKey) {
      url = url + '&roomkey=' + roomKey;
    }
    return url;
  }

  historyDetailList(roomKey, time, downCount, upCount) {
    let hide = 0;
    if (downCount == 0 || upCount == 0) {
      hide = 1;
    }
    return `${HTTPS_SCHEME}${this.domain}${this.branch}/messenger/messenger/new_info?loginkey=${this.autoLoginKey}&format=json&roomkey=${roomKey}&message_time=${time}&down_count=${downCount}&up_count=${upCount}&hide_standard_msg=${hide}`;
  }

  downloadWhisperFile(link) {
    return `${HTTPS_SCHEME}${this.domain}${this.branch}/whisper${link}`;
  }

  fileExtension(fileName) {
    if (fileName == null || fileName == '') {
      return '';
    }
    let posDot = fileName.lastIndexOf('.');
    if (posDot < 0) {
      return '';
    }

    return fileName.substring(posDot + 1).toLowerCase();
  }
}

const talkAPI = new TalkAPI();
export default talkAPI;
