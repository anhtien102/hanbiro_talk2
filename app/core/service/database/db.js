import path from 'path';
import talkPath from '../../utils/folder-manage/talk-folder';
import appPrefs from '../../utils/pref';
import * as TalkConstants from '../../service/talk-constants';
const Database = require('better-sqlite3');
import TextUtils from '../../utils/text.util';
import fs from 'fs';
import basicUtils from '../../utils/basic.utils';

import myUtil from '../../utils/utils';

const TAG = 'SQLITE_LOGGER';

const dbPrefix = basicUtils.prefixFromArguments();

let DB_NAME = 'Talk' + dbPrefix + '.db';
const VERSION_DB = 4;

const CREATE_ACCOUNT_TB = `CREATE TABLE IF NOT EXISTS tbAccount(
  accountID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  domain VARCHAR,
  userName VARCHAR,
  userKey VARCHAR,
  sipNumber VARCHAR,
  sipPassword VARCHAR
)`;

const CREATE_GROUP_TB = `CREATE TABLE IF NOT EXISTS tbGroup(
  groupId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  groupPKey VARCHAR,
  groupKey VARCHAR,
  groupAlias VARCHAR,
  groupShortName VARCHAR,
  groupFullName VARCHAR,
  groupType INTEGER,
  groupHeader VARCHAR,
  groupLanguage VARCHAR
)`;

const CREATE_USER_TB = `CREATE TABLE IF NOT EXISTS tbUser(
  userId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  userKey VARCHAR,
  userEmail VARCHAR,
  userChosunText VARCHAR,
  userLocalPhone VARCHAR,
  userMobilePhone VARCHAR,
  userAlias VARCHAR,
  userFullName VARCHAR,
  userNickName VARCHAR,
  userShortName VARCHAR,
  userPosition VARCHAR,
  userShortPosition VARCHAR,
  userSex INTEGER,
  userStatus INTEGER,
  userOrder INTEGER,
  userPhotoTime INTEGER,
  userLoginType INTEGER,
  userSIPPhone VARCHAR,
  userLanguage VARCHAR,
  userHidden INTEGER,
  userExtraNameCountry VARCHAR
)`;

const CREATE_GROUP_USER_TB = `CREATE TABLE IF NOT EXISTS tbGroupUser(
  userID INTEGER NOT NULL,
  groupId INTEGER NOT NULL,
  groupKey VARCHAR NOT NULL
)`;

const CREATE_GROUP_USER_FAV_TB = `CREATE TABLE IF NOT EXISTS tbGroupUserFav(
  key VARCHAR,
  pKey VARCHAR,
  isGroup INTEGER DEFAULT 1
)`;

const CREATE_MESSAGE_TB = `CREATE TABLE IF NOT EXISTS tbMessage(
  msgID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  msgUserKey VARCHAR,
  msgNumberKey VARCHAR,
  msgRoomKey VARCHAR,
  msgRoomID INTEGER,
  msgStatus INTEGER,
  msgStyle VARCHAR,
  msgEmotion VARCHAR,
  msgBody VARCHAR,
  msgCreateDate INTEGER,
  msgFormat VARCHAR,
  msgFormatSize INTEGER,
  msgFailed INTEGER,
  msgType INTEGER,
  msgClientId VARCHAR,
  msgSendStatus INTEGER,
  msgReadedKey VARCHAR,
  msgUnReadedKey VARCHAR,
  msgTotalUser INTEGER,
  msgMarkAsReaded INTEGER,
  msgPreviewImagePath VARCHAR,
  msgPreviewDesc VARCHAR,
  msgPreviewTitle VARCHAR,
  msgThirdPartyID INTEGER,
  msgPreviewCanonicalURL VARCHAR,
  msgSIPUserKeyOther VARCHAR,
  msgSIPKey VARCHAR,
  msgSIPStatus INTEGER,
  msgSIPDuration INTEGER,
  msgSIPType INTEGER,
  msgIsBookmark INTEGER,
  msgIsDeleted INTEGER,
  msgBookmarkKey VARCHAR,
  msgCountUnreaded INTEGER
)`;

const CREATE_MESSAGE_FILE_LIST_TB = `CREATE TABLE IF NOT EXISTS tbMessageFileList(
  flMsgID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  flMsgRoomKey VARCHAR,
  msgId INTEGER,
  flMsgFileType INTEGER
)`;

const CREATE_MESSAGE_BOOKMARK_LIST_TB = `CREATE TABLE IF NOT EXISTS tbMessageBookmarkList(
  flMsgID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  flMsgBookmarkKey VARCHAR,
  flMsgRoomKey VARCHAR,
  flMsgBookmarkCreateTime VARCHAR,
  msgId INTEGER,
  flMsgFilterType INTEGER
)`;

const CREATE_MESSAGE_DETAIL_TB = `CREATE TABLE IF NOT EXISTS tbMessageDetail(
  msgdtId INTEGER,
  msgdtFileName VARCHAR,
  msgdtFileKey VARCHAR,
  msgdtStatus INTEGER,
  msgdtFileSize INTEGER,
  msgdtFilePath VARCHAR,
  msgdtIP VARCHAR,
  msgdtPort INTEGER,
  msgdtHttpServerFileReceiver VARCHAR,
  msgdtHttpServerStoreagePath VARCHAR,
  msgdtHttpServerFileName VARCHAR,
  msgdtHttpServerFilePath VARCHAR,
  msgdtHttpServerHostName VARCHAR,
  msgdtFileRecordTime INTEGER,
  msgdtHasFile INTEGER,
  msgdtSupportClouddisk INTEGER,
  msgdtImageWidth INTEGER,
  msgdtImageHeight INTEGER
)`;

const CREATE_ROOM_TB = `CREATE TABLE IF NOT EXISTS tbRoom(
  rID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  rAccID INTEGER,
  rRoomKey VARCHAR,
  rRoomName VARCHAR,
  rRoomNameLabelPrevAlias VARCHAR,
  rRoomNameLabelPrivate VARCHAR,
  rRoomNameLabelPublic VARCHAR,
  rRoomCreateDate INTEGER,
  rRoomStatus INTEGER,
  rRoomLastMsgID INTEGER,
  rRoomIRead INTEGER,
  rRoomUnreadMsgServer INTEGER,
  rRoomServerLastMsgTime INTEGER,
  rRoomServerLastMsgEmoticon VARCHAR,
  rRoomServerLastMsgBody VARCHAR,
  rRoomServerLastMsgNumber VARCHAR,
  rRoomServerLastSenderUserKey VARCHAR,
  rRoomServerLastMsgDeleted INTEGER,
  rRoomGroupUserType VARCHAR,
  rRoomPushAlert INTEGER,
  rRoomType INTEGER,
  rRoomFavorite INTEGER DEFAULT 0,
  rRoomGroupCallKey VARCHAR,
  rRoomBoardNotiSetting VARCHAR,
  rRoomTag VARCHAR,
  rRoomFireCode VARCHAR,
  rRoomFireGPS VARCHAR,
  rRoomFireType INTEGER
)`;

const CREATE_ROOM_DETAIL_TB = `CREATE TABLE IF NOT EXISTS tbRoomDetail(
  rdtID INTEGER,
  rdtUKey VARCHAR,
  rdtUName VARCHAR,
  rdtJoinDate INTEGER,
  rdtStatus INTEGER DEFAULT 1,
  PRIMARY KEY(rdtID, rdtUKey)
)`;

const CREATE_RECENT_SEARCH_TB = `CREATE TABLE IF NOT EXISTS tbRecentSearch(
  recentID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  recentAccId INTEGER,
  recentUserKey VARCHAR,
  recentRoomKey VARCHAR,
  recentTime INTEGER
)`;

const CREATE_ROOM_TIME_MSG_TB = `CREATE TABLE IF NOT EXISTS tbRoomTimeMsg(
  rtId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  rtRoomKey VARCHAR,
  rtPrevOldTime INTEGER,
  rtPrevLatestTime INTEGER,
  rtOldTime INTEGER,
  rtLatestTime INTEGER
)`;

// tbGroup
// tbUser
// tbGroupUser
// tbGroupUserFav
// tbMessage
// tbMessageFileList
// tbMessageBookmarkList
// tbMessageDetail
// tbRoom
// tbRoomDetail
// tbRecentSearch
// tbServer
// tbAccount

const dbPath = path.join(talkPath.databaseFolder(), DB_NAME);
if (!fs.existsSync(dbPath)) {
  appPrefs.eraseSetting();
}
const options = null;
// const options = { verbose: console.log };
const dbManagement = new Database(dbPath, options);
// dbManagement.pragma('journal_mode = TRUNCATE');
// console.log('DKM', dbManagement.pragma('journal_mode'));
const lastVersion = appPrefs.getLastDBVersion();
if (lastVersion !== VERSION_DB) {
  appPrefs.eraseSetting();
  dbManagement.prepare('DROP TABLE IF EXISTS tbAccount').run();
  dbManagement.prepare('DROP TABLE IF EXISTS tbGroup').run();
  dbManagement.prepare('DROP TABLE IF EXISTS tbUser').run();
  dbManagement.prepare('DROP TABLE IF EXISTS tbGroupUser').run();
  dbManagement.prepare('DROP TABLE IF EXISTS tbGroupUserFav').run();
  dbManagement.prepare('DROP TABLE IF EXISTS tbMessage').run();
  dbManagement.prepare('DROP TABLE IF EXISTS tbMessageFileList').run();
  dbManagement.prepare('DROP TABLE IF EXISTS tbMessageBookmarkList').run();
  dbManagement.prepare('DROP TABLE IF EXISTS tbMessageDetail').run();
  dbManagement.prepare('DROP TABLE IF EXISTS tbRoom').run();
  dbManagement.prepare('DROP TABLE IF EXISTS tbRoomDetail').run();
  dbManagement.prepare('DROP TABLE IF EXISTS tbRecentSearch').run();
  dbManagement.prepare('DROP TABLE IF EXISTS tbRoomTimeMsg').run();
  createTable();
} else {
  createTable();
}

function createTable() {
  dbManagement.prepare(CREATE_ACCOUNT_TB).run();
  dbManagement.prepare(CREATE_GROUP_TB).run();
  dbManagement.prepare(CREATE_USER_TB).run();
  dbManagement.prepare(CREATE_GROUP_USER_TB).run();
  dbManagement.prepare(CREATE_GROUP_USER_FAV_TB).run();
  dbManagement.prepare(CREATE_MESSAGE_TB).run();
  dbManagement.prepare(CREATE_MESSAGE_FILE_LIST_TB).run();
  dbManagement.prepare(CREATE_MESSAGE_BOOKMARK_LIST_TB).run();
  dbManagement.prepare(CREATE_MESSAGE_DETAIL_TB).run();
  dbManagement.prepare(CREATE_ROOM_TB).run();
  dbManagement.prepare(CREATE_ROOM_DETAIL_TB).run();
  dbManagement.prepare(CREATE_RECENT_SEARCH_TB).run();
  dbManagement.prepare(CREATE_ROOM_TIME_MSG_TB).run();
  appPrefs.setLastDBVersion(VERSION_DB);
}

export function closeDB() {
  dbManagement.close();
}

export function setActiveAccount(
  domain,
  userName,
  userKey,
  sipNumber,
  sipPassword
) {
  const user = dbManagement
    .prepare('SELECT * FROM tbAccount WHERE domain=? AND userName=?')
    .get(domain, userName);
  if (user && user.accountID) {
    dbManagement
      .prepare(
        'UPDATE tbAccount SET userKey=? ,sipNumber=?,sipPassword=? WHERE accountID=?'
      )
      .run(userKey, sipNumber, sipPassword, user.accountID);
    return user.accountID;
  } else {
    const result = dbManagement
      .prepare('INSERT INTO tbAccount VALUES(NULL,?,?,?,?,?)')
      .run(domain, userName, userKey, sipNumber, sipPassword);
    return result.lastInsertRowid;
  }
}

function findRoomInfoByRoomKey() {
  return dbManagement.prepare(
    'SELECT rID, rRoomStatus, rRoomServerLastMsgNumber,rRoomUnreadMsgServer,rRoomServerLastSenderUserKey,rRoomServerLastMsgTime FROM tbRoom WHERE rRoomKey = ? AND rAccID = ?'
  );
}

function findMsgIDByRoomKeyAndNumberKey() {
  return dbManagement.prepare(
    'SELECT * FROM tbMessage WHERE msgNumberKey = ? AND msgRoomID = ?'
  );
}

function findMsgIDByRoomIDAndUserKeyAndClientIDAndWithOutStatus() {
  return dbManagement.prepare(
    'SELECT msgID FROM tbMessage WHERE msgUserKey = ? AND msgRoomID = ? AND msgClientId = ? AND msgSendStatus <> ?'
  );
}

function findMsgIDByRoomIDAndSipKey() {
  return dbManagement.prepare(
    'SELECT * FROM tbMessage WHERE msgSIPKey = ? AND msgRoomID = ?'
  );
}

function findMsgIDByRoomIDAndUserKeyAndCreateDate() {
  return dbManagement.prepare(
    'SELECT msgID FROM tbMessage WHERE msgUserKey = ? AND msgRoomID = ? AND msgCreateDate = ? AND msgType = ? '
  );
}

function insertUserFavoriteWithKey(key) {
  const result = dbManagement
    .prepare('SELECT * FROM tbGroupUserFav WHERE key=? AND pKey=?')
    .get(key, -1);
  if (!result) {
    dbManagement
      .prepare('INSERT INTO tbGroupUserFav  VALUES(? , ? , ?)')
      .run(key, '-1', 0);
  }
}

function deleteUserFavoriteWithKey(key) {
  dbManagement
    .prepare('DELETE FROM tbGroupUserFav WHERE key=? AND pKey=? AND isGroup=?')
    .run(key, '-1', 0);
}

function findGroupUserAndInsertFavourite(
  stmKeyExistInGroupFav,
  stmInsertGroupFav,
  stmFindAllUserWithParentKey,
  stmFindAllGroupWithParentKey,
  groupKey,
  groupParentKey
) {
  const result = stmKeyExistInGroupFav.get(groupKey, groupParentKey);
  if (!result) {
    stmInsertGroupFav.run(groupKey, groupParentKey, 1);
  }

  // find all user have parent key same
  const userList = stmFindAllUserWithParentKey.all(groupKey);
  if (userList) {
    for (let i = 0; i < userList.length; i++) {
      let item = userList[i];
      const userKey = item.userKey;
      const parentKey = groupKey;
      const foundKey = stmKeyExistInGroupFav.get(userKey, parentKey);
      if (!foundKey) {
        stmInsertGroupFav.run(userKey, parentKey, 0);
      }
    }
  }

  const groupList = stmFindAllGroupWithParentKey.all(groupKey);
  if (groupList) {
    for (let i = 0; i < groupList.length; i++) {
      let item = groupList[i];
      const gK = item.groupKey;
      const parentKey = groupKey;
      this.findGroupUserAndInsertFavourite(
        stmKeyExistInGroupFav,
        stmInsertGroupFav,
        stmFindAllUserWithParentKey,
        stmFindAllGroupWithParentKey,
        gK,
        parentKey
      );
    }
  }
}

function findGroupUserAndDeleteFavourite(
  stmDeleteGroupUserFav,
  stmKeyExistInGroupFav,
  stmFindAllUserGroupWithParentKey,
  groupKey,
  groupParentKey,
  isGroup
) {
  stmDeleteGroupUserFav.run(groupKey, groupParentKey, isGroup);
  const found = stmKeyExistInGroupFav.get(groupKey, isGroup);
  if (found) {
    return;
  }

  const allItem = stmFindAllUserGroupWithParentKey.all(groupKey);
  if (allItem) {
    for (let i = 0; i < allItem.length; i++) {
      const item = allItem[i];
      const key = item.key;
      const isG = item.isGroup;
      const pKey = groupKey;
      if (isG == 0) {
        stmDeleteGroupUserFav.run(key, pKey, isG);
      } else {
        findGroupUserAndDeleteFavourite(
          stmDeleteGroupUserFav,
          stmKeyExistInGroupFav,
          stmFindAllUserGroupWithParentKey,
          key,
          pKey,
          isG
        );
      }
    }
  }
}

function insertGroupFavoriteWithKey(key) {
  const stmKeyExistInGroupFav = dbManagement.prepare(
    'SELECT * FROM tbGroupUserFav WHERE key=? AND pKey=?'
  );
  const stmInsertGroupFav = dbManagement.prepare(
    'INSERT INTO tbGroupUserFav  VALUES(? , ? , ?)'
  );
  const stmFindAllUserWithParentKey = dbManagement.prepare(
    ` SELECT USR.userKey
      FROM tbUser USR
      JOIN tbGroupUser GUS
      ON USR.userId = GUS.userID
      JOIN tbGroup GRP
      ON GUS.groupId = GRP.groupId
      WHERE GRP.groupKey=?
      GROUP BY USR.userId
    `
  );
  const stmFindAllGroupWithParentKey = dbManagement.prepare(
    ` SELECT tbGroup.groupKey
      FROM tbGroup
      WHERE tbGroup.groupPKey=?
    `
  );
  dbManagement
    .transaction(key => {
      findGroupUserAndInsertFavourite(
        stmKeyExistInGroupFav,
        stmInsertGroupFav,
        stmFindAllUserWithParentKey,
        stmFindAllGroupWithParentKey,
        key,
        '-1'
      );
    })
    .immediate(key);
}

function deleteGroupFavouriteWithKey(key) {
  const stmDeleteGroupUserFav = dbManagement.prepare(
    'DELETE FROM tbGroupUserFav WHERE key=? AND pKey=? AND isGroup=?'
  );

  const stmKeyExistInGroupFav = dbManagement.prepare(
    'SELECT * FROM tbGroupUserFav WHERE key=? AND isGroup=?'
  );

  const stmFindAllUserGroupWithParentKey = dbManagement.prepare(
    ` SELECT *
      FROM tbGroupUserFav
      WHERE tbGroupUserFav.pKey=?
    `
  );
  dbManagement
    .transaction(key => {
      findGroupUserAndDeleteFavourite(
        stmDeleteGroupUserFav,
        stmKeyExistInGroupFav,
        stmFindAllUserGroupWithParentKey,
        key,
        '-1',
        1
      );
    })
    .immediate(key);
}

export function addNewKeyToFavorite(key, isUser) {
  if (isUser) {
    insertUserFavoriteWithKey(key);
  } else {
    insertGroupFavoriteWithKey(key);
  }
}

export function deleteKeyToFavorite(key, isUser) {
  if (isUser) {
    deleteUserFavoriteWithKey(key);
  } else {
    deleteGroupFavouriteWithKey(key);
  }
}

export function deleteContact(key) {
  dbManagement
    .transaction(key => {
      const result = dbManagement
        .prepare('SELECT userId FROM tbUser WHERE userKey=?')
        .get(key);
      if (result) {
        dbManagement.prepare('DELETE FROM tbUser WHERE userKey=?').run(key);
        dbManagement
          .prepare('DELETE FROM tbGroupUser WHERE userID=?')
          .run(result.userId);
        dbManagement
          .prepare('DELETE FROM tbGroupUserFav WHERE key=? AND isGroup=?')
          .run(key, 0);
      }
    })
    .immediate(key);
}

export function updateAGroup(group) {
  dbManagement
    .transaction(group => {
      const result = dbManagement
        .prepare('SELECT groupId FROM tbGroup WHERE groupKey=?')
        .get(group.groupKey);
      let groupID = -1;
      if (!result) {
      } else {
        groupID = result.groupId;
        const updateGroup = dbManagement
          .prepare(
            `UPDATE tbGroup SET groupAlias=?, groupShortName=?, groupFullName=?,groupType=?,groupHeader=?, groupLanguage=?
              WHERE groupId=?`
          )
          .run(
            group.groupAlias,
            group.groupShortName,
            group.groupFullName,
            group.groupType,
            group.groupHeader,
            group.groupLang,
            groupID
          );
      }
    })
    .immediate(group);
}

export function updateAContact(contact) {
  dbManagement
    .transaction(contact => {
      const result = dbManagement
        .prepare('SELECT userId FROM tbUser WHERE userKey=?')
        .get(contact.userKey);
      let userId = -1;
      if (!result) {
      } else {
        userId = result.userId;
        const updateUser = dbManagement
          .prepare(
            `UPDATE tbUser SET userEmail=?, userChosunText=?,
            userLocalPhone=?, userMobilePhone=?, userAlias=?,
            userFullName=?, userNickName=?, userShortName=?, userPosition=?,
             userShortPosition=?, userSex=?,userStatus=?,userOrder=?,userPhotoTime=?,
             userLoginType=?, userSIPPhone=?, userLanguage=?, userHidden=?, userExtraNameCountry=? WHERE userKey=?`
          )
          .run(
            contact.userEmail,
            contact.chosunText,
            contact.userLocalPhone,
            contact.mobilephone,
            contact.userAlias,
            contact.fullname,
            contact.nickname,
            contact.shortname,
            contact.position,
            contact.position,
            contact.sex,
            contact.status,
            contact.order,
            contact.phototime,
            contact.loginType,
            contact.sipphone,
            contact.userLang,
            contact.hiddenUser ? 1 : 0,
            contact.extraUserNameCountry ?? '',
            contact.userKey
          );
      }
    })
    .immediate(contact);
}

export function insertAContact(contact) {
  dbManagement
    .transaction(contact => {
      const result = dbManagement
        .prepare('SELECT userId FROM tbUser WHERE userKey=?')
        .get(contact.userKey);
      let userId = -1;
      if (!result) {
        const insertUser = dbManagement
          .prepare(
            'INSERT INTO tbUser VALUES(NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
          )
          .run(
            contact.userKey,
            contact.email,
            contact.chosunText,
            contact.localPhone,
            '',
            contact.name,
            '',
            contact.nickName,
            contact.shortName,
            contact.position,
            '',
            contact.sex,
            contact.status,
            contact.order,
            contact.photoTime,
            0,
            contact.sip,
            '',
            0,
            contact.extraUserNameCountry ?? ''
          );
        userId = insertUser.lastInsertRowid;
      } else {
        userId = result.userId;
      }

      if (userId >= 0) {
        const resultGroup = dbManagement
          .prepare('SELECT groupId FROM tbGroup WHERE groupKey=?')
          .get(contact.pKey);
        if (resultGroup) {
          const found = dbManagement
            .prepare(
              'SELECT userID FROM tbGroupUser WHERE userID=? AND groupId=?'
            )
            .get(userId, resultGroup.groupId);
          if (!found) {
            dbManagement
              .prepare('INSERT INTO tbGroupUser VALUES(?,?,?)')
              .run(userId, resultGroup.groupId, contact.pKey);
          }
        }
      }
    })
    .immediate(contact);
}

export function deleteGroup(key) {
  dbManagement
    .transaction(key => {
      const result = dbManagement
        .prepare('SELECT groupId FROM tbGroup WHERE groupKey=?')
        .get(key);
      if (result) {
        dbManagement.prepare('DELETE FROM tbGroup WHERE groupKey=?').run(key);
        dbManagement
          .prepare('DELETE FROM tbGroupUser WHERE groupId=?')
          .run(result.groupId);
        dbManagement
          .prepare('DELETE FROM tbGroupUserFav WHERE key=? AND isGroup=?')
          .run(key, 1);
      }
    })
    .immediate(key);
}

export function insertAGroup(group) {
  dbManagement
    .transaction(group => {
      const result = dbManagement
        .prepare('SELECT groupId FROM tbGroup WHERE groupKey=?')
        .get(group.groupKey);
      if (!result) {
        dbManagement
          .prepare('INSERT INTO tbGroup VALUES(NULL,?,?,?,?,?,?,?,?)')
          .run(
            group.pKey,
            group.groupKey,
            group.name,
            group.shortName,
            '',
            0,
            group.header,
            ''
          );
      }
    })
    .immediate(group);
}

export function insertAGroupFolder(group) {
  dbManagement
    .transaction(group => {
      let pKey = group.pKey != '' ? group.pKey : '-1';
      const result = dbManagement
        .prepare(
          'SELECT * FROM tbGroupUserFav WHERE key=? AND (pKey=? OR pKey=?)'
        )
        .get(group.groupKey, group.pKey, pKey);

      if (!result) {
        dbManagement
          .prepare('INSERT INTO tbGroupUserFav VALUES(?,?,?)')
          .run(group.groupKey, group.pKey, 1);
      }

      const resultG = dbManagement
        .prepare('SELECT * FROM tbGroup WHERE groupKey=?')
        .get(group.groupKey);
      if (resultG) {
        dbManagement
          .prepare(
            'UPDATE tbGroup SET groupAlias=?, groupPKey=? WHERE groupId=?'
          )
          .run(group.name, group.pKey, resultG.groupId);
      } else {
        dbManagement
          .prepare('INSERT INTO tbGroup VALUES(NULL,?,?,?,?,?,?,?,?)')
          .run(
            group.pKey,
            group.groupKey,
            group.name,
            group.shortName,
            '',
            1,
            group.header,
            ''
          );
      }
    })
    .immediate(group);
}

export function deleteMultiUserInFolder(folderKey, userKeyList) {
  const stmDelete = dbManagement.prepare(
    'DELETE tbGroupUserFav WHERE key=? AND pKey=? AND isGroup=?'
  );
  dbManagement
    .transaction((folderKey, userKeyList) => {
      const users = userKeyList.split(',');
      for (let i = 0; i < users.length; i++) {
        const key = users[i];
        stmDelete.run(key, folderKey, 0);
      }
    })
    .immediate(folderKey, userKeyList);
}

export function updateMultiUserInFolder(folderKey, userKeyList) {
  const stmCheckExist = dbManagement.prepare(
    'SELECT key FROM tbGroupUserFav WHERE key = ? AND pKey=?'
  );

  const stmInsert = dbManagement.prepare(
    'INSERT INTO tbGroupUserFav VALUES(?,?,?)'
  );

  dbManagement
    .transaction((folderKey, userKeyList) => {
      const users = userKeyList.split(',');
      dbManagement
        .prepare('DELETE FROM tbGroupUserFav WHERE pKey=?')
        .run(folderKey);
      for (let i = 0; i < users.length; i++) {
        const key = users[i];
        const found = stmCheckExist.get(key, folderKey);
        if (!found) {
          stmInsert.run(key, folderKey, 0);
        }
      }
    })
    .immediate(folderKey, userKeyList);
}

export function insertMultiUserInFolder(folderKey, userKeyList) {
  const stmCheckExist = dbManagement.prepare(
    'SELECT key FROM tbGroupUserFav WHERE key = ? AND pKey=?'
  );

  const stmInsert = dbManagement.prepare(
    'INSERT INTO tbGroupUserFav VALUES(?,?,?)'
  );

  dbManagement
    .transaction((folderKey, userKeyList) => {
      const users = userKeyList.split(',');
      for (let i = 0; i < users.length; i++) {
        const key = users[i];
        const found = stmCheckExist.get(key, folderKey);
        if (!found) {
          stmInsert.run(key, folderKey, 0);
        }
      }
    })
    .immediate(folderKey, userKeyList);
}

export function storeRoomDetailWithRoomKey(
  roomKey,
  accountID,
  roomOneData,
  loggedUserKey
) {
  const stmFindRoomIdByRoomKey = dbManagement.prepare(
    'SELECT rID FROM tbRoom WHERE rRoomKey = ? AND rAccID = ? AND rRoomStatus = ?'
  );
  const stmUpdateRoomStatusById = dbManagement.prepare(
    'UPDATE tbRoom SET rRoomStatus=? WHERE rID=?'
  );

  const stmUpdateRoomDetailStatusById = dbManagement.prepare(
    'UPDATE tbRoomDetail SET rdtStatus=? WHERE rdtID=?'
  );

  const stmUpdateRoomDetailById = dbManagement.prepare(
    'UPDATE tbRoomDetail SET rdtStatus=? , rdtJoinDate=?,rdtUName=? WHERE rdtID=? AND rdtUKey=?'
  );

  const stmUpdateAllMessageStatusByRoomId = dbManagement.prepare(
    'UPDATE tbMessage SET msgStatus=? WHERE msgRoomID=?'
  );

  const stmFindRoomDetailByRoomIdUserKey = dbManagement.prepare(
    'SELECT * FROM tbRoomDetail WHERE rdtID = ? AND rdtUKey = ?'
  );

  const stmInsertRoomDetail = dbManagement.prepare(
    'INSERT INTO tbRoomDetail VALUES(?,?,?,?,?)'
  );

  const storeRoomDetail = dbManagement.transaction(
    (roomKey, accountID, roomOneData) => {
      let roomResult = stmFindRoomIdByRoomKey.get(
        roomKey,
        accountID,
        TalkConstants.ROOM_STATUS_LIVE
      );
      const roomId = roomResult ? roomResult.rID : -1;
      if (roomId >= 0) {
        if (roomOneData.whos && roomOneData.whos.length > 0) {
          let roomType = 0;
          if (roomOneData.result.guestChat == 1) {
            roomType = TalkConstants.ROOM_TYPE_GUEST;
          } else if (roomOneData.result.liveChat == 1) {
            roomType = TalkConstants.ROOM_TYPE_LIVE;
          }
          let roomAliasPublic = roomOneData.result.roomAlias;
          let fireRoom = -999;
          if (roomOneData.result.fireChat == null) {
            fireRoom = roomOneData.result.fireChat;
          }
          let saveAlias = true;

          stmUpdateRoomDetailStatusById.run(
            TalkConstants.ROOM_DETAIL_STATUS_LEAVE,
            roomId
          );

          let found = roomOneData.whos.find(
            element => element.userKey == loggedUserKey
          );

          if (!found) {
            roomOneData.whos.push({
              userKey: loggedUserKey,
              name: ''
            });
          }
          for (let i = 0; i < roomOneData.whos.length; i++) {
            let roomOne = roomOneData.whos[i];
            if (TextUtils.isEmpty(roomOne.userKey)) {
              continue;
            }

            const detailResult = stmFindRoomDetailByRoomIdUserKey.get(
              roomId,
              roomOne.userKey
            );
            if (detailResult) {
              let joinDate;
              let roomDetailStatus = detailResult.rdtStatus;
              if (roomDetailStatus == TalkConstants.ROOM_DETAIL_STATUS_ENTER) {
                joinDate = detailResult.rdtJoinDate;
              } else {
                joinDate = Date.now();
              }

              let name = roomOne.name;
              if (TextUtils.isEmpty(name)) {
                name = detailResult.rdtUName;
              }

              stmUpdateRoomDetailById.run(
                TalkConstants.ROOM_DETAIL_STATUS_ENTER,
                joinDate,
                name,
                detailResult.rdtID,
                roomOne.userKey
              );
            } else {
              stmInsertRoomDetail.run(
                roomId,
                roomOne.userKey,
                roomOne.name,
                Date.now(),
                TalkConstants.ROOM_DETAIL_STATUS_ENTER
              );
            }
          }

          let roomTypeExist = roomType > 0;
          let roomTypeGUExist = TextUtils.isNotEmpty(
            roomOneData.result.roomTypeGU
          );
          let updateFireRoom = fireRoom > -999;
          if (roomTypeExist || roomTypeGUExist || updateFireRoom || saveAlias) {
            let sqlUpdateRoom = 'UPDATE tbRoom SET ';
            let addPhay = false;
            if (roomTypeExist) {
              sqlUpdateRoom = sqlUpdateRoom + `rRoomType = ${roomType}`;
              addPhay = true;
            }

            if (roomTypeGUExist) {
              if (addPhay) {
                sqlUpdateRoom = sqlUpdateRoom + ',';
              }
              sqlUpdateRoom =
                sqlUpdateRoom +
                `rRoomGroupUserType = '${roomOneData.result.roomTypeGU}'`;
              addPhay = true;
            }

            if (updateFireRoom) {
              if (addPhay) {
                sqlUpdateRoom = sqlUpdateRoom + ',';
              }
              sqlUpdateRoom = sqlUpdateRoom + `rRoomFireType = ${fireRoom}`;
              addPhay = true;
            }

            if (saveAlias) {
              if (addPhay) {
                sqlUpdateRoom = sqlUpdateRoom + ',';
              }
              sqlUpdateRoom =
                sqlUpdateRoom +
                `rRoomNameLabelPrevAlias = '${roomAliasPublic}',`;
              sqlUpdateRoom =
                sqlUpdateRoom + `rRoomNameLabelPublic = '${roomAliasPublic}'`;
            }

            sqlUpdateRoom = sqlUpdateRoom + ' WHERE rID = ' + roomId;

            dbManagement.prepare(sqlUpdateRoom).run();
          }
        } else {
          stmUpdateRoomStatusById.run(TalkConstants.ROOM_STATUS_OUT, roomId);
          stmUpdateRoomDetailStatusById.run(
            TalkConstants.ROOM_DETAIL_STATUS_LEAVE,
            roomId
          );
          stmUpdateAllMessageStatusByRoomId.run(
            TalkConstants.MessageStatus.STATUS_CLOSED,
            roomId
          );
        }
      }
    }
  );
  storeRoomDetail.immediate(roomKey, accountID, roomOneData);
}

export function storeNewRoom(room, accountID) {
  const stmFindRoomByRoomKey = findRoomInfoByRoomKey();
  const stmInsertRoom = dbManagement.prepare(
    'INSERT INTO tbRoom VALUES(NULL, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
  );
  const stmUpdateRoom = dbManagement.prepare(
    `UPDATE tbRoom SET
        rRoomName = ?,
        rRoomNameLabelPrivate=?,
        rRoomStatus=?,
        rRoomGroupUserType=?
    WHERE rID=?`
  );

  let roomID = -1;
  const storeNewRoom = dbManagement.transaction((room, accountID) => {
    const roomInfo = stmFindRoomByRoomKey.get(room.roomKey, accountID);
    if (roomInfo == null) {
      const result = stmInsertRoom.run(
        accountID,
        room.roomKey,
        room.roomTagName,
        '',
        room.roomAlias,
        '',
        room.createDate,
        TalkConstants.ROOM_STATUS_LIVE,
        -1,
        0,
        0,
        0,
        '',
        '',
        '',
        '',
        0,
        room.roomTypeGU,
        1,
        0,
        0,
        '',
        '',
        '',
        '',
        '',
        0
      );
      roomID = result.lastInsertRowid;
    } else {
      roomID = roomInfo.rID;
      stmUpdateRoom.run(
        room.roomTagName,
        room.roomAlias,
        TalkConstants.ROOM_STATUS_LIVE,
        room.roomTypeGU,
        roomInfo.rID
      );
    }
  });
  storeNewRoom.immediate(room, accountID);
  return roomID;
}

export function deleteAllUserGroupAndRoom() {
  const deleteAll = dbManagement.transaction(() => {
    dbManagement.prepare('DELETE FROM tbGroup').run();
    dbManagement.prepare('DELETE FROM tbUser').run();
    dbManagement.prepare('DELETE FROM tbGroupUser').run();
    dbManagement.prepare('DELETE FROM tbGroupUserFav').run();

    dbManagement.prepare('DELETE FROM tbRoom').run();
    dbManagement.prepare('DELETE FROM tbRoomDetail').run();
    dbManagement.prepare('DELETE FROM tbMessage').run();
    dbManagement.prepare('DELETE FROM tbMessageDetail').run();
    dbManagement.prepare('DELETE FROM tbRecentSearch').run();
    dbManagement.prepare('DELETE FROM tbRoomTimeMsg').run();
  });
  deleteAll.immediate();
}

export function deleteAllRoomMessage() {
  const deleteAll = dbManagement.transaction(() => {
    dbManagement.prepare('DELETE FROM tbRoom').run();
    dbManagement.prepare('DELETE FROM tbRoomDetail').run();
    dbManagement.prepare('DELETE FROM tbMessage').run();
    dbManagement.prepare('DELETE FROM tbMessageDetail').run();
    dbManagement.prepare('DELETE FROM tbRecentSearch').run();
    dbManagement.prepare('DELETE FROM tbRoomTimeMsg').run();
  });
  deleteAll.immediate();
}

function insertMessageText(
  roomInfo,
  roomKey,
  message,
  accountUserKey,
  stmFindMsgIByRoomIdAndNumberKey,
  stmFindMsgIDByRoomIDAndUserKeyAndClientIDAndWithOutStatus,
  stmFindMsgIDByRoomIDAndSipKey,
  stmFindMsgIDByRoomIDAndUserKeyAndCreateDate,
  stmInsertMessageText,
  stmUpdateMessageText,
  stmUpdateMessageTextIgnore,
  stmUpdateRoomLastMsg,
  lastMessage
) {
  if (message.messageType == TalkConstants.MessageType.MESSAGE_TYPE_CALL_LOG) {
    message.userKey = message.useOtherUserKey
      ? message.sipOtherUserKey
      : accountUserKey;
  }

  if (TextUtils.isEmpty(message.userKey)) {
    message.userKey = accountUserKey;
  }
  message.status = TalkConstants.MessageStatus.STATUS_READ;
  message.countUnread = TalkConstants.getNumberUsersAMessage(
    message.messageUnreadedKey
  );
  let numberReaded = TalkConstants.getNumberUsersAMessage(
    message.messageReadedKey
  );
  let total = numberReaded + message.countUnread;
  message.totalUser = total;

  let ignoreUserKeySipType = false;
  let markAsRead = TalkConstants.MessageServerStatus.MARK_UNREAD;

  let msgId = -1;
  if (
    message.messageType == TalkConstants.MessageType.MESSAGE_TYPE_TEXT ||
    message.messageType == TalkConstants.MessageType.MESSAGE_TYPE_CALL_LOG ||
    message.messageType == TalkConstants.MessageType.MESSAGE_TYPE_GROUP_CALL_LOG
  ) {
    let findMsg = null;
    if (
      message.messageType == TalkConstants.MessageType.MESSAGE_TYPE_TEXT ||
      message.messageType ==
        TalkConstants.MessageType.MESSAGE_TYPE_GROUP_CALL_LOG
    ) {
      findMsg = stmFindMsgIByRoomIdAndNumberKey.get(
        message.numberKey,
        roomInfo.rID
      );
      if (findMsg) {
        msgId = findMsg.msgID;
      } else if (TextUtils.isNotEmpty(message.clientKey)) {
        let result = stmFindMsgIDByRoomIDAndUserKeyAndClientIDAndWithOutStatus.get(
          message.userKey,
          roomInfo.rID,
          message.clientKey,
          TalkConstants.MessageSendStatus.STATUS_SEND_COMPLETED
        );
        msgId = result ? result.msgID : null;
      }
    } else {
      findMsg = stmFindMsgIDByRoomIDAndSipKey.get(message.sipKey, roomInfo.rID);
      if (findMsg) {
        msgId = findMsg.msgID;
        if (
          msgId >= 0 &&
          message.sipStatus !=
            TalkConstants.MessageSipConstant.SIP_STATUS_CALLING
        ) {
          //ignore update user key and sip type
          ignoreUserKeySipType = true;
        }
      }
    }

    if (
      findMsg &&
      findMsg.msgMarkAsReaded ==
        TalkConstants.MessageServerStatus.MARK_SERVER_READ
    ) {
      markAsRead = TalkConstants.MessageServerStatus.MARK_SERVER_READ;
    } else {
      if (
        TextUtils.isNotEmpty(message.messageUnreadedKey) &&
        TalkConstants.isContainExactUserKeyInList(
          message.messageUnreadedKey,
          accountUserKey
        )
      ) {
        markAsRead = TalkConstants.MessageServerStatus.MARK_UNREAD;
      } else {
        markAsRead = TalkConstants.MessageServerStatus.MARK_SERVER_READ;
      }
    }
  } else if (
    message.messageType == TalkConstants.MessageType.MESSAGE_TYPE_INVITE
  ) {
    let result = stmFindMsgIDByRoomIDAndUserKeyAndCreateDate.get(
      message.userKey,
      roomInfo.rID,
      message.createDate,
      TalkConstants.MessageType.MESSAGE_TYPE_INVITE
    );
    msgId = result ? result.msgID : null;
  } else if (
    message.messageType == TalkConstants.MessageType.MESSAGE_TYPE_OUT
  ) {
    let result = stmFindMsgIDByRoomIDAndUserKeyAndCreateDate.get(
      message.userKey,
      roomInfo.rID,
      message.createDate,
      TalkConstants.MessageType.MESSAGE_TYPE_OUT
    );
    msgId = result ? result.msgID : null;
  } else if (
    message.messageType == TalkConstants.MessageType.MESSAGE_TYPE_ROOM_ALIAS
  ) {
    let result = stmFindMsgIDByRoomIDAndUserKeyAndCreateDate.get(
      message.userKey,
      roomInfo.rID,
      message.createDate,
      TalkConstants.MessageType.MESSAGE_TYPE_ROOM_ALIAS
    );
    msgId = result ? result.msgID : null;
  } else if (
    message.messageType == TalkConstants.MessageType.MESSAGE_TYPE_BOARD_INFO
  ) {
    let result = stmFindMsgIDByRoomIDAndUserKeyAndCreateDate.get(
      message.userKey,
      roomInfo.rID,
      message.createDate,
      TalkConstants.MessageType.MESSAGE_TYPE_BOARD_INFO
    );
    msgId = result ? result.msgID : null;
  }

  if (!msgId || msgId < 0) {
    const result = stmInsertMessageText.run(
      message.userKey,
      message.numberKey ?? '',
      roomKey,
      roomInfo.rID,
      message.status,
      message.messageStyle ?? '',
      message.messageEmoticon ?? '',
      message.messageBody ?? '',
      message.createDate,
      message.messageFormat ?? '',
      0, //format size
      message.messageFailed ?? 1, //success alway?
      message.messageType,
      message.clientKey ?? '',
      TalkConstants.MessageSendStatus.STATUS_SEND_COMPLETED,
      message.messageReadedKey,
      message.messageUnreadedKey,
      message.totalUser,
      markAsRead,
      '',
      '',
      '',
      message.messageFromThirdPartyID ??
        TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
      '',
      message.sipOtherUserKey ?? '',
      message.sipKey ?? '',
      message.sipStatus ?? 0,
      message.sipDuration ?? 0,
      message.sipType ?? 0,
      message.isBookmark ?? 0,
      message.isDeleted ?? 0,
      message.bookmarkKey ?? '',
      message.countUnread ?? 0
    );
    msgId = result.lastInsertRowid;
    if (lastMessage) {
      if (
        TextUtils.isNotEmpty(message.numberKey) &&
        roomInfo.rRoomServerLastMsgTime < message.createDate
      ) {
        stmUpdateRoomLastMsg.run(
          TalkConstants.ROOM_STATUS_LIVE,
          msgId,
          message.createDate,
          message.messageEmoticon ?? '',
          message.messageBody ?? '',
          message.userKey,
          message.isDeleted ?? 0,
          message.numberKey ?? '',
          message.createDate,
          roomInfo.rID
        );
      }
    }
  } else {
    if (ignoreUserKeySipType) {
      stmUpdateMessageTextIgnore.run(
        message.numberKey ?? '',
        roomKey,
        roomInfo.rID,
        message.status,
        message.messageStyle ?? '',
        message.messageEmoticon ?? '',
        message.messageBody,
        message.createDate,
        message.messageFormat ?? '',
        0, //format size
        message.messageFailed ?? 1, //success alway?
        message.messageType,
        message.clientKey ?? '',
        TalkConstants.MessageSendStatus.STATUS_SEND_COMPLETED,
        message.messageReadedKey,
        message.messageUnreadedKey,
        message.totalUser,
        markAsRead,
        '',
        '',
        '',
        message.messageFromThirdPartyID ??
          TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
        '',
        message.sipOtherUserKey ?? '',
        message.sipKey ?? '',
        message.sipStatus ?? 0,
        message.sipDuration ?? 0,
        message.isBookmark ?? 0,
        message.isDeleted ?? 0,
        message.bookmarkKey ?? '',
        message.countUnread ?? 0,
        msgId
      );
    } else {
      stmUpdateMessageText.run(
        message.userKey,
        message.numberKey ?? '',
        roomKey,
        roomInfo.rID,
        message.status,
        message.messageStyle ?? '',
        message.messageEmoticon ?? '',
        message.messageBody ?? '',
        message.createDate,
        message.messageFormat ?? '',
        0, //format size
        message.messageFailed ?? 1, //success alway?
        message.messageType,
        message.clientKey ?? '',
        TalkConstants.MessageSendStatus.STATUS_SEND_COMPLETED,
        message.messageReadedKey ?? '',
        message.messageUnreadedKey ?? '',
        message.totalUser,
        markAsRead,
        '',
        '',
        '',
        message.messageFromThirdPartyID ??
          TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
        '',
        message.sipOtherUserKey ?? '',
        message.sipKey ?? '',
        message.sipStatus ?? 0,
        message.sipDuration ?? 0,
        message.sipType ?? 0,
        message.isBookmark ?? 0,
        message.isDeleted ?? 0,
        message.bookmarkKey ?? '',
        message.countUnread ?? 0,
        msgId
      );
    }
  }
}

function insertMessageFile(
  roomInfo,
  roomKey,
  message,
  accountUserKey,
  stmFindMsgIByRoomIdAndNumberKey,
  stmFindMsgIDByRoomIDAndUserKeyAndClientIDAndWithOutStatus,
  stmInsertMessageText,
  stmUpdateMessageText,
  stmUpdateRoomLastMsg,
  stmInsertMessageDetail,
  stmFindMessageDetail,
  stmUpdateMessageDetail,
  lastMessage
) {
  if (TextUtils.isEmpty(message.userKey)) {
    message.userKey = accountUserKey;
  }
  message.status = TalkConstants.MessageStatus.STATUS_READ;
  message.countUnread = TalkConstants.getNumberUsersAMessage(
    message.messageUnreadedKey
  );
  let numberReaded = TalkConstants.getNumberUsersAMessage(
    message.messageReadedKey
  );
  let total = numberReaded + message.countUnread;
  message.totalUser = total;

  let markAsRead = TalkConstants.MessageServerStatus.MARK_UNREAD;
  let msgId = -1;
  let fromLocal = false;

  let findMsg = stmFindMsgIByRoomIdAndNumberKey.get(
    message.numberKey,
    roomInfo.rID
  );
  if (findMsg) {
    msgId = findMsg.msgID;
  } else if (TextUtils.isNotEmpty(message.clientKey)) {
    result = stmFindMsgIDByRoomIDAndUserKeyAndClientIDAndWithOutStatus.get(
      message.userKey,
      roomInfo.rID,
      message.clientKey,
      TalkConstants.MessageSendStatus.STATUS_SEND_COMPLETED
    );
    msgId = result ? result.msgID : null;
    fromLocal = true;
  }

  if (
    findMsg &&
    findMsg.msgMarkAsReaded ==
      TalkConstants.MessageServerStatus.MARK_SERVER_READ
  ) {
    markAsRead = TalkConstants.MessageServerStatus.MARK_SERVER_READ;
  } else {
    if (
      TextUtils.isNotEmpty(message.messageUnreadedKey) &&
      TalkConstants.isContainExactUserKeyInList(
        message.messageUnreadedKey,
        accountUserKey
      )
    ) {
      markAsRead = TalkConstants.MessageServerStatus.MARK_UNREAD;
    } else {
      markAsRead = TalkConstants.MessageServerStatus.MARK_SERVER_READ;
    }
  }

  if (!msgId || msgId < 0) {
    const result = stmInsertMessageText.run(
      message.userKey,
      message.numberKey ?? '',
      roomKey,
      roomInfo.rID,
      message.status,
      message.messageStyle ?? '',
      message.messageEmoticon ?? '',
      message.messageBody ?? '',
      message.createDate,
      message.messageFormat ?? '',
      0, //format size
      message.messageFailed ?? 1, //success alway?
      message.messageType,
      message.clientKey ?? '',
      TalkConstants.MessageSendStatus.STATUS_SEND_COMPLETED,
      message.messageReadedKey,
      message.messageUnreadedKey,
      message.totalUser,
      markAsRead,
      '',
      '',
      '',
      message.messageFromThirdPartyID ??
        TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
      '',
      message.sipOtherUserKey ?? '',
      message.sipKey ?? '',
      message.sipStatus ?? 0,
      message.sipDuration ?? 0,
      message.sipType ?? 0,
      message.isBookmark ?? 0,
      message.isDeleted ?? 0,
      message.bookmarkKey ?? '',
      message.countUnread ?? 0
    );
    msgId = result.lastInsertRowid;

    //insert message detail
    const messageDetail = message.messageDetail;
    if (messageDetail) {
      stmInsertMessageDetail.run(
        msgId,
        messageDetail.fileName ?? '',
        messageDetail.fileKey ?? '',
        messageDetail.fileStatus ??
          TalkConstants.MessageFileStatus.FILE_MSG_NORMAL,
        messageDetail.fileSize ?? 0,
        '',
        messageDetail.fileAddress ?? '',
        messageDetail.filePort ?? 0,
        messageDetail.httpFileReceiver ?? '',
        '',
        messageDetail.httpServerFileName ?? '',
        messageDetail.httpServerFilePath ?? '',
        messageDetail.httpHostName ?? '',
        messageDetail.recordTime ?? 0,
        messageDetail.hasFile ? 1 : 0,
        messageDetail.clouddisk,
        0,
        0
      );
    }

    if (lastMessage) {
      if (
        TextUtils.isNotEmpty(message.numberKey) &&
        roomInfo.rRoomServerLastMsgTime < message.createDate
      ) {
        stmUpdateRoomLastMsg.run(
          TalkConstants.ROOM_STATUS_LIVE,
          msgId,
          message.createDate,
          message.messageEmoticon ?? '',
          message.messageBody ?? '',
          message.userKey,
          message.isDeleted ?? 0,
          message.numberKey ?? '',
          message.createDate,
          roomInfo.rID
        );
      }
    }
  } else {
    const messageDetail = message.messageDetail;
    if (messageDetail) {
      let fileName = messageDetail.fileName ?? '';
      let fileKey = messageDetail.fileKey ?? '';
      let fileStatus =
        messageDetail.fileStatus ??
        TalkConstants.MessageFileStatus.FILE_MSG_NORMAL;
      let fileSize = messageDetail.fileSize ?? 0;
      let filePath = '';
      let fileAddress = messageDetail.fileAddress ?? '';
      let filePort = messageDetail.filePort ?? 0;
      let httpFileReceiver = messageDetail.httpFileReceiver ?? '';
      let httpServerStoragePath = '';
      let httpServerFileName = messageDetail.httpServerFileName ?? '';
      let httpServerFilePath = messageDetail.httpServerFilePath ?? '';
      let httpHostName = messageDetail.httpHostName ?? '';
      let recordTime = messageDetail.recordTime ?? 0;
      let hasFile = messageDetail.hasFile ? 1 : 0;
      let clouddisk = messageDetail.clouddisk;

      // update message detail
      if (!fromLocal) {
        const prevDetail = stmFindMessageDetail.get(msgId);
        if (prevDetail) {
          //if upload interrupt thi thanh success
          if (
            prevDetail.msgdtStatus !=
              TalkConstants.MessageFileStatus.FILE_MSG_UPLOAD_INTERRUPT &&
            prevDetail.msgdtStatus !=
              TalkConstants.MessageFileStatus.FILE_MSG_UPLOAD_WORKING &&
            prevDetail.msgdtStatus !=
              TalkConstants.MessageFileStatus.FILE_MSG_UPLOAD_FAILED
          ) {
            fileStatus = prevDetail.msgdtStatus;
          }

          fileAddress = prevDetail.msgdtIP;
          filePort = prevDetail.msgdtPort;

          if (fileSize <= 0) {
            fileSize = prevDetail.msgdtFileSize;
          }
          filePath = prevDetail.msgdtFilePath;
          fileKey = prevDetail.msgdtFileKey;

          if (TextUtils.isEmpty(httpFileReceiver)) {
            httpFileReceiver = prevDetail.msgdtHttpServerFileReceiver;
          }

          if (TextUtils.isEmpty(httpServerStoragePath)) {
            httpServerStoragePath = prevDetail.msgdtHttpServerStoreagePath;
          }

          if (TextUtils.isEmpty(httpServerFilePath)) {
            httpServerFilePath = prevDetail.msgdtHttpServerFilePath;
          }

          if (TextUtils.isEmpty(httpServerFileName)) {
            httpServerFileName = prevDetail.msgdtHttpServerFileName;
          }

          if (TextUtils.isEmpty(httpHostName)) {
            httpHostName = prevDetail.msgdtHttpServerHostName;
          }
        }
      }

      stmUpdateMessageDetail.run(
        fileName,
        fileKey,
        fileStatus,
        fileSize,
        filePath,
        fileAddress,
        filePort,
        httpFileReceiver,
        httpServerStoragePath,
        httpServerFileName,
        httpServerFilePath,
        httpHostName,
        recordTime,
        hasFile,
        clouddisk,
        msgId
      );

      stmUpdateMessageText.run(
        message.userKey,
        message.numberKey ?? '',
        roomKey,
        roomInfo.rID,
        message.status,
        message.messageStyle ?? '',
        message.messageEmoticon ?? '',
        message.messageBody,
        message.createDate,
        message.messageFormat ?? '',
        0, //format size
        message.messageFailed ?? 1, //success alway?
        message.messageType,
        message.clientKey ?? '',
        TalkConstants.MessageSendStatus.STATUS_SEND_COMPLETED,
        message.messageReadedKey,
        message.messageUnreadedKey,
        message.totalUser,
        markAsRead,
        '',
        '',
        '',
        message.messageFromThirdPartyID ??
          TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
        '',
        message.sipOtherUserKey ?? '',
        message.sipKey ?? '',
        message.sipStatus ?? 0,
        message.sipDuration ?? 0,
        message.sipType ?? 0,
        message.isBookmark ?? 0,
        message.isDeleted ?? 0,
        message.bookmarkKey ?? '',
        message.countUnread ?? 0,
        msgId
      );
    }

    if (lastMessage) {
      if (
        TextUtils.isNotEmpty(message.numberKey) &&
        roomInfo.rRoomServerLastMsgTime < message.createDate
      ) {
        stmUpdateRoomLastMsg.run(
          TalkConstants.ROOM_STATUS_LIVE,
          msgId,
          message.createDate,
          message.messageEmoticon ?? '',
          message.messageBody ?? '',
          message.userKey,
          message.isDeleted ?? 0,
          message.numberKey ?? '',
          message.createDate,
          roomInfo.rID
        );
      }
    }
  }
}

export function storeRoomTalk(
  roomKey: string,
  talkCount: Number,
  messageList: [],
  accountUserKey,
  accountID
) {
  if (talkCount > 0) {
    const stmFindRoomByRoomKey = findRoomInfoByRoomKey();
    const stmFindMsgIByRoomIdAndNumberKey = findMsgIDByRoomKeyAndNumberKey();
    const stmFindMsgIDByRoomIDAndUserKeyAndClientIDAndWithOutStatus = findMsgIDByRoomIDAndUserKeyAndClientIDAndWithOutStatus();
    const stmFindMsgIDByRoomIDAndSipKey = findMsgIDByRoomIDAndSipKey();
    const stmFindMsgIDByRoomIDAndUserKeyAndCreateDate = findMsgIDByRoomIDAndUserKeyAndCreateDate();
    const stmUpdateRoomLastMsg = dbManagement.prepare(
      `UPDATE tbRoom SET
        rRoomStatus=?,
        rRoomLastMsgID=?,
        rRoomCreateDate=?,
        rRoomServerLastMsgEmoticon=?,
        rRoomServerLastMsgBody=?,
        rRoomServerLastSenderUserKey=?,
        rRoomServerLastMsgDeleted=?,
        rRoomServerLastMsgNumber=?,
        rRoomServerLastMsgTime=?
    WHERE rID=?`
    );

    const stmInsertMessageDetail = dbManagement.prepare(
      'INSERT INTO tbMessageDetail VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
    );

    const stmFindMessageDetail = dbManagement.prepare(
      'SELECT * FROM tbMessageDetail WHERE msgdtId=?'
    );

    const stmUpdateMessageDetail = dbManagement.prepare(
      `UPDATE tbMessageDetail SET
      msgdtFileName=?,
      msgdtFileKey=?,
      msgdtStatus=?,
      msgdtFileSize=?,
      msgdtFilePath=?,
      msgdtIP=?,
      msgdtPort=?,
      msgdtHttpServerFileReceiver=?,
      msgdtHttpServerStoreagePath=?,
      msgdtHttpServerFileName=?,
      msgdtHttpServerFilePath=?,
      msgdtHttpServerHostName=?,
      msgdtFileRecordTime=?,
      msgdtHasFile=?,
      msgdtSupportClouddisk=?
    WHERE msgdtId=?`
    );

    const stmInsertMessageText = dbManagement.prepare(
      'INSERT INTO tbMessage VALUES(NULL, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
    );
    const stmUpdateMessageText = dbManagement.prepare(
      `UPDATE tbMessage SET
        msgUserKey=?,
        msgNumberKey=?,
        msgRoomKey=?,
        msgRoomID=?,
        msgStatus=?,
        msgStyle=?,
        msgEmotion=?,
        msgBody=?,
        msgCreateDate=?,
        msgFormat=?,
        msgFormatSize=?,
        msgFailed=?,
        msgType=?,
        msgClientId=?,
        msgSendStatus=?,
        msgReadedKey=?,
        msgUnReadedKey=?,
        msgTotalUser=?,
        msgMarkAsReaded=?,
        msgPreviewImagePath=?,
        msgPreviewDesc=?,
        msgPreviewTitle=?,
        msgThirdPartyID=?,
        msgPreviewCanonicalURL=?,
        msgSIPUserKeyOther=?,
        msgSIPKey=?,
        msgSIPStatus=?,
        msgSIPDuration=?,
        msgSIPType=?,
        msgIsBookmark=?,
        msgIsDeleted=?,
        msgBookmarkKey=?,
        msgCountUnreaded=?
    WHERE msgId=?`
    );

    const stmUpdateMessageTextIgnore = dbManagement.prepare(
      `UPDATE tbMessage SET
        msgNumberKey=?,
        msgRoomKey=?,
        msgRoomID=?,
        msgStatus=?,
        msgStyle=?,
        msgEmotion=?,
        msgBody=?,
        msgCreateDate=?,
        msgFormat=?,
        msgFormatSize=?,
        msgFailed=?,
        msgType=?,
        msgClientId=?,
        msgSendStatus=?,
        msgReadedKey=?,
        msgUnReadedKey=?,
        msgTotalUser=?,
        msgMarkAsReaded=?,
        msgPreviewImagePath=?,
        msgPreviewDesc=?,
        msgPreviewTitle=?,
        msgThirdPartyID=?,
        msgPreviewCanonicalURL=?,
        msgSIPUserKeyOther=?,
        msgSIPKey=?,
        msgSIPStatus=?,
        msgSIPDuration=?,
        msgIsBookmark=?,
        msgIsDeleted=?,
        msgBookmarkKey=?,
        msgCountUnreaded=?
    WHERE msgId=?`
    );

    const stmMaybePendingToPending = dbManagement.prepare(
      'UPDATE tbMessage SET msgSendStatus = ? WHERE msgSendStatus = ?'
    );
    let roomInfo;
    const storeRoomTalk = dbManagement.transaction(
      (roomKey, messageList, accountUserKey, accountID) => {
        roomInfo = stmFindRoomByRoomKey.get(roomKey, accountID);
        if (roomInfo != null) {
          for (let i = 0; i < messageList.length; i++) {
            let message = messageList[i];
            if (message.isFileMsg) {
              insertMessageFile(
                roomInfo,
                roomKey,
                message,
                accountUserKey,
                stmFindMsgIByRoomIdAndNumberKey,
                stmFindMsgIDByRoomIDAndUserKeyAndClientIDAndWithOutStatus,
                stmInsertMessageText,
                stmUpdateMessageText,
                stmUpdateRoomLastMsg,
                stmInsertMessageDetail,
                stmFindMessageDetail,
                stmUpdateMessageDetail,
                i == messageList.length - 1
              );
            } else {
              insertMessageText(
                roomInfo,
                roomKey,
                message,
                accountUserKey,
                stmFindMsgIByRoomIdAndNumberKey,
                stmFindMsgIDByRoomIDAndUserKeyAndClientIDAndWithOutStatus,
                stmFindMsgIDByRoomIDAndSipKey,
                stmFindMsgIDByRoomIDAndUserKeyAndCreateDate,
                stmInsertMessageText,
                stmUpdateMessageText,
                stmUpdateMessageTextIgnore,
                stmUpdateRoomLastMsg,
                i == messageList.length - 1
              );
            }
          }

          stmMaybePendingToPending.run(
            TalkConstants.MessageSendStatus.STATUS_SEND_PENDING,
            TalkConstants.MessageSendStatus.STATUS_SEND_MAYBE_PENDING
          );
        }
      }
    );

    storeRoomTalk.immediate(roomKey, messageList, accountUserKey, accountID);
    return roomInfo ? roomInfo.rID : -1;
  }
  return -1;
}

export function storeRoomList(
  listRoom: [],
  oldRoomTime: number,
  accountID,
  accountUserKey
) {
  const stmDeleteOldRoom = dbManagement.prepare(
    'DELETE FROM tbRoom WHERE rRoomCreateDate <= ? AND rAccID = ?'
  );

  const stmDeleteRoom = dbManagement.prepare(
    'DELETE FROM tbRoom WHERE rRoomKey= ? AND rAccID = ?'
  );

  const stmDeleteRoomRecent = dbManagement.prepare(
    'DELETE FROM tbRecentSearch WHERE recentRoomKey= ? AND recentAccId = ?'
  );

  const stmInsertRoom = dbManagement.prepare(
    'INSERT INTO tbRoom VALUES(NULL, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
  );

  const stmFindRoomDetail = dbManagement.prepare(
    'SELECT * FROM tbRoomDetail WHERE rdtID = ? AND rdtUKey = ?'
  );

  const stmUpdateRoom = dbManagement.prepare(
    `UPDATE tbRoom SET
        rRoomNameLabelPrevAlias=?,
        rRoomNameLabelPrivate=?,
        rRoomNameLabelPublic=?,
        rRoomCreateDate=?,
        rRoomStatus=?,
        rRoomIRead=?,
        rRoomUnreadMsgServer=?,
        rRoomServerLastMsgTime=?,
        rRoomServerLastMsgEmoticon=?,
        rRoomServerLastMsgBody=?,
        rRoomServerLastMsgNumber=?,
        rRoomServerLastSenderUserKey=?,
        rRoomServerLastMsgDeleted=?,
        rRoomGroupUserType=?,
        rRoomPushAlert=?,
        rRoomType=?,
        rRoomFavorite=?,
        rRoomGroupCallKey=?,
        rRoomTag=?,
        rRoomFireCode=?,
        rRoomFireGPS=?,
        rRoomFireType=?
    WHERE rID=?`
  );

  const updateRoomDetailToLeave = dbManagement.prepare(
    'UPDATE tbRoomDetail SET rdtStatus=? WHERE rdtID=?'
  );

  const updateOneRoomDetail = dbManagement.prepare(
    'UPDATE tbRoomDetail SET rdtStatus=?, rdtUName=?,rdtJoinDate=?  WHERE rdtID=? AND rdtUKey=?'
  );

  const insertOneRoomDetail = dbManagement.prepare(
    'INSERT INTO tbRoomDetail VALUES(?,?,?,?,?)'
  );

  const stmFindRoomByRoomKey = findRoomInfoByRoomKey();

  const storeRoomList = dbManagement.transaction(
    (listRoom, oldRoomTime, accountID, accountUserKey) => {
      if (oldRoomTime > 0) {
        stmDeleteOldRoom.run(oldRoomTime, accountID);
      }

      let storeRoomListResult = {};

      for (let i = 0; i < listRoom.length; i++) {
        let roomItem = listRoom[i];
        let roomKey = roomItem.roomKey;
        if (roomItem.leftRoom) {
          //left room
          stmDeleteRoomRecent.run(roomKey, accountID);
          stmDeleteRoom.run(roomKey, accountID);
        } else {
          //active room
          let lastMsgBody = roomItem.lastMsgBody;
          if (
            lastMsgBody != null &&
            lastMsgBody.length > TalkConstants.LIMIT_MESSAGE_LENGTH
          ) {
            lastMsgBody = lastMsgBody.substring(
              0,
              TalkConstants.LIMIT_MESSAGE_LENGTH
            );
          }

          let roomType = 0;
          if (roomItem.guestChat == 1) {
            roomType = TalkConstants.ROOM_TYPE_GUEST;
          } else if (roomItem.liveChat == 1) {
            roomType = TalkConstants.ROOM_TYPE_LIVE;
          }

          let paramName = roomItem.name;

          const roomInfo = stmFindRoomByRoomKey.get(roomKey, accountID);
          let roomID = -1;
          if (roomInfo == null) {
            const result = stmInsertRoom.run(
              accountID,
              roomKey,
              '',
              roomItem.alias,
              roomItem.aliasPrivate,
              roomItem.aliasPublic,
              roomItem.lastTime,
              TalkConstants.ROOM_STATUS_LIVE,
              0,
              roomItem.iRead,
              roomItem.unreadMessageServer,
              roomItem.lastTime,
              '',
              lastMsgBody,
              roomItem.lastMsgNumber,
              roomItem.lastSenderUserKey,
              roomItem.lastMsgDeleted,
              roomItem.roomTypeGU,
              roomItem.pushAlert,
              roomType,
              roomItem.favoriteRoom,
              roomItem.groupCallKey,
              '',
              roomItem.roomTag,
              roomItem.fireRoomCode,
              roomItem.fireRoomGPS,
              roomItem.fireRoom
            );
            roomID = result.lastInsertRowid;
            const hasNewMessage = roomItem.pushAlert >= 1 && roomItem.iRead < 1;
            if (hasNewMessage) {
              storeRoomListResult[roomKey] = hasNewMessage;
            }
          } else {
            roomID = roomInfo.rID;

            const hasNewMessage =
              roomItem.pushAlert >= 1 &&
              roomItem.iRead < 1 &&
              roomItem.lastTime > roomInfo.rRoomServerLastMsgTime;

            if (hasNewMessage) {
              storeRoomListResult[roomKey] = hasNewMessage;
            }

            stmUpdateRoom.run(
              roomItem.alias,
              roomItem.aliasPrivate,
              roomItem.aliasPublic,
              roomItem.lastTime,
              TalkConstants.ROOM_STATUS_LIVE,
              roomItem.iRead,
              roomItem.unreadMessageServer,
              roomItem.lastTime,
              '',
              lastMsgBody,
              roomItem.lastMsgNumber,
              roomItem.lastSenderUserKey,
              roomItem.lastMsgDeleted,
              roomItem.roomTypeGU,
              roomItem.pushAlert,
              roomType,
              roomItem.favoriteRoom,
              roomItem.groupCallKey,
              roomItem.roomTag,
              roomItem.fireRoomCode,
              roomItem.fireRoomGPS,
              roomItem.fireRoom,
              roomID
            );
          }

          updateRoomDetailToLeave.run(
            TalkConstants.ROOM_DETAIL_STATUS_LEAVE,
            roomID
          );

          let users = roomItem.users;
          let emptyName = paramName == null || paramName == '';

          users.forEach(userKey => {
            if (userKey == null || userKey == '') {
              return;
            }

            const roomDetailResult = stmFindRoomDetail.get(roomID, userKey);
            if (roomDetailResult) {
              let joinDate = roomDetailResult.rdtJoinDate;
              let name = '';
              if (
                TalkConstants.isGuestUser(userKey) ||
                TalkConstants.isLiveUser(userKey)
              ) {
                name = emptyName ? roomDetailResult.rdtUName : paramName;
              } else {
                name = roomDetailResult.rdtUName;
              }

              if (
                roomDetailResult.rdtStatus !=
                TalkConstants.ROOM_DETAIL_STATUS_ENTER
              ) {
                joinDate = Date.now();
              }
              updateOneRoomDetail.run(
                TalkConstants.ROOM_DETAIL_STATUS_ENTER,
                name,
                joinDate,
                roomDetailResult.rdtID,
                roomDetailResult.rdtUKey
              );
            } else {
              let name = '';
              if (
                TalkConstants.isGuestUser(userKey) ||
                TalkConstants.isLiveUser(userKey)
              ) {
                name = emptyName ? '' : paramName;
              }
              insertOneRoomDetail.run(
                roomID,
                userKey,
                name,
                Date.now(),
                TalkConstants.ROOM_DETAIL_STATUS_ENTER
              );
            }
          });

          const roomDetailResult = stmFindRoomDetail.get(
            roomID,
            accountUserKey
          );
          if (roomDetailResult) {
            let joinDate = roomDetailResult.rdtJoinDate;
            let name = roomDetailResult.rdtUName;
            if (
              roomDetailResult.rdtStatus !=
              TalkConstants.ROOM_DETAIL_STATUS_ENTER
            ) {
              joinDate = Date.now();
            }
            updateOneRoomDetail.run(
              TalkConstants.ROOM_DETAIL_STATUS_ENTER,
              name,
              joinDate,
              roomDetailResult.rdtID,
              roomDetailResult.rdtUKey
            );
          } else {
            let name = '';
            insertOneRoomDetail.run(
              roomID,
              accountUserKey,
              name,
              Date.now(),
              TalkConstants.ROOM_DETAIL_STATUS_ENTER
            );
          }
        }
      }

      return storeRoomListResult;
    }
  );

  return storeRoomList.immediate(
    listRoom,
    oldRoomTime,
    accountID,
    accountUserKey
  );
}

export function storeContactAndGroup(listGroup: [], listContact: []) {
  let mapGUFav = {};
  let mapGroup = {};
  let mapUser = {};
  let mapGU = {};

  const stmInsertGroupFav = dbManagement.prepare(
    'INSERT INTO tbGroupUserFav VALUES(?,?,?)'
  );

  const stmInsertGroupUser = dbManagement.prepare(
    'INSERT INTO tbGroupUser VALUES(?,?,?)'
  );

  const stmInsertGroup = dbManagement.prepare(
    'INSERT INTO tbGroup VALUES(NULL,?,?,?,?,?,?,?,?)'
  );

  const stmInsertUser = dbManagement.prepare(
    'INSERT INTO tbUser VALUES(NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
  );

  const insertContactGroup = dbManagement.transaction(
    (listGroup, listContact) => {
      dbManagement.prepare('DELETE FROM tbGroup').run();
      dbManagement.prepare('DELETE FROM tbUser').run();
      dbManagement.prepare('DELETE FROM tbGroupUser').run();
      dbManagement.prepare('DELETE FROM tbGroupUserFav').run();

      for (var i = 0; i < listGroup.length; i++) {
        let group = listGroup[i];
        let isFavourite = group.favorite;
        if (isFavourite) {
          const guFavKey = group.groupKey + group.groupParentKey + 'G';
          if (!mapGUFav[guFavKey]) {
            mapGUFav[guFavKey] = true;
            stmInsertGroupFav.run(group.groupKey, group.groupParentKey, 1);
          }

          if (group.groupType == TalkConstants.GROUP_TYPE_USER) {
            const groupID = mapGroup[group.groupKey] ?? -1;
            if (groupID < 0) {
              const result = stmInsertGroup.run(
                group.groupParentKey,
                group.groupKey,
                group.groupAlias,
                group.groupShortName,
                group.groupFullName,
                group.groupType,
                group.groupHeader,
                group.groupLang
              );
              mapGroup[group.groupKey] = result.lastInsertRowid;
            }
          }
        } else {
          const groupID = mapGroup[group.groupKey] ?? -1;
          if (groupID < 0) {
            const result = stmInsertGroup.run(
              group.groupParentKey,
              group.groupKey,
              group.groupAlias,
              group.groupShortName,
              group.groupFullName,
              group.groupType,
              group.groupHeader,
              group.groupLang
            );
            mapGroup[group.groupKey] = result.lastInsertRowid;
          }
        }
      }

      for (var i = 0; i < listContact.length; i++) {
        let contact = listContact[i];
        let isFavourite = contact.favorite;
        if (isFavourite) {
          const guFavKey = contact.userKey + contact.parentKey;
          if (!mapGUFav[guFavKey]) {
            mapGUFav[guFavKey] = true;
            stmInsertGroupFav.run(contact.userKey, contact.parentKey, 0);
          }
        } else {
          let uID = mapUser[contact.userKey] ?? -1;

          if (uID < 0) {
            const result = stmInsertUser.run(
              contact.userKey,
              contact.userEmail,
              contact.chosunText,
              contact.userLocalPhone,
              contact.mobilephone,
              contact.userAlias,
              contact.fullname,
              contact.nickname,
              contact.shortname,
              contact.position,
              contact.position,
              contact.sex,
              contact.status,
              contact.order,
              contact.phototime,
              contact.loginType,
              contact.sipphone,
              contact.userLang,
              contact.hiddenUser ? 1 : 0,
              contact.extraUserNameCountry ?? ''
            );
            mapUser[contact.userKey] = result.lastInsertRowid;
            uID = result.lastInsertRowid;
          }

          const groupID = mapGroup[contact.parentKey] ?? -1;
          const guKey = uID + '_' + groupID;
          if (!mapGU[guKey]) {
            stmInsertGroupUser.run(uID, groupID, contact.parentKey);
            mapGU[guKey] = true;
          }
        }
      }
    }
  );

  insertContactGroup.immediate(listGroup, listContact);
}

export function updateStatusLoginTypeByUserKey(userKey, loginType, status) {
  dbManagement
    .prepare('UPDATE tbUser SET userStatus=? ,userLoginType=? WHERE userKey=?')
    .run(status, loginType, userKey);
}

export function updateMessageDeleteByRoomKey(accountId, roomKey, numberKey) {
  dbManagement
    .transaction((accountId, roomKey, numberKey) => {
      const roomInfo = findRoomInfoByRoomKey().get(roomKey, accountId);
      const roomID = roomInfo.rID ?? -1;
      if (roomID >= 0) {
        dbManagement
          .prepare(
            'UPDATE tbMessage SET msgIsDeleted=?,msgBody=?  WHERE msgRoomID=? AND msgNumberKey=?'
          )
          .run(1, 'This message has been deleted.', roomID, numberKey);

        if (numberKey == roomInfo.rRoomServerLastMsgNumber) {
          dbManagement
            .prepare(
              'UPDATE tbRoom SET rRoomServerLastMsgDeleted=?,rRoomServerLastMsgBody=? WHERE rID=?'
            )
            .run(1, 'This message has been deleted.', roomID);
        }
      }
    })
    .immediate(accountId, roomKey, numberKey);
}

export function updateRoomAlertByRoomKey(accountId, roomKey, value) {
  dbManagement
    .prepare('UPDATE tbRoom SET rRoomPushAlert=? WHERE rRoomKey=? AND rAccID=?')
    .run(value, roomKey, accountId);
}

export function updateRoomFavoriteByRoomKey(accountId, roomKey, value) {
  dbManagement
    .prepare('UPDATE tbRoom SET rRoomFavorite=? WHERE rRoomKey=? AND rAccID=?')
    .run(value, roomKey, accountId);
}

export function updateRoomNameByRoomKey(
  accountId,
  roomKey,
  isPublicAlias,
  roomName
) {
  if (isPublicAlias) {
    dbManagement
      .prepare(
        'UPDATE tbRoom SET rRoomNameLabelPrevAlias=?,rRoomNameLabelPublic=? WHERE rRoomKey=? AND rAccID=?'
      )
      .run(roomName, roomName, roomKey, accountId);
  } else {
    dbManagement
      .prepare(
        'UPDATE tbRoom SET rRoomNameLabelPrevAlias=?,rRoomNameLabelPrivate=? WHERE rRoomKey=? AND rAccID=?'
      )
      .run(roomName, roomName, roomKey, accountId);
  }
}

export function updateNickNameByUserKey(userKey, nickName) {
  dbManagement
    .prepare('UPDATE tbUser SET userNickName=? WHERE userKey=?')
    .run(nickName, userKey);
}

export function updateNickNameAndPhotoTimeListFromORGHTTP(nicknames, photos) {
  const stmNickName = dbManagement.prepare(
    'UPDATE tbUser SET userNickName=? WHERE userKey=?'
  );
  const stmPhotoTime = dbManagement.prepare(
    'UPDATE tbUser SET userPhotoTime=? WHERE userKey=?'
  );

  dbManagement
    .transaction((nicknames, photos) => {
      nicknames.forEach(item => {
        stmNickName.run(item.nickName, item.userKey);
      });
      photos.forEach(item => {
        stmPhotoTime.run(item.timeStamp, item.userKey);
      });
    })
    .immediate(nicknames, photos);
}

export function updateNickNameAndPhotoTimeList(nicknames, photos) {
  const stmNickName = dbManagement.prepare(
    'UPDATE tbUser SET userNickName=? WHERE userKey=?'
  );
  const stmPhotoTime = dbManagement.prepare(
    'UPDATE tbUser SET userPhotoTime=? WHERE userKey=?'
  );

  dbManagement
    .transaction((nicknames, photos) => {
      nicknames.forEach(item => {
        stmNickName.run(item.value, item.userKey);
      });
      photos.forEach(item => {
        stmPhotoTime.run(item.value, item.userKey);
      });
    })
    .immediate(nicknames, photos);
}

export function updateStatusLoginTypeContact(statusList: []) {
  const updateState = dbManagement.prepare(
    'UPDATE tbUser SET userStatus=? ,userLoginType=? WHERE userKey=?'
  );
  const updateLogoutState = dbManagement.prepare(
    'UPDATE tbUser SET userStatus=? ,userLoginType=?'
  );
  const updateMultiple = dbManagement.transaction(statusList => {
    updateLogoutState.run(
      TalkConstants.StatusMode.logout,
      TalkConstants.LOGIN_TYPE_WINDOWS
    );
    for (let i = 0; i < statusList.length; i++) {
      const obj = statusList[i];
      updateState.run(obj.status, obj.loginType, obj.userKey);
    }
  });

  updateMultiple.immediate(statusList);
}

function getORGTree(fullOrg) {
  const groups = dbManagement
    .prepare('SELECT * FROM tbGroup where groupType=?')
    .all(TalkConstants.GROUP_TYPE_ADMIN);
  const users = dbManagement
    .prepare(
      'SELECT USR.*, GUS.groupKey as userParentKey FROM tbUser USR JOIN tbGroupUser GUS ON USR.userId=GUS.userID'
    )
    .all();

  let usersFav = null;
  let groupsFav = null;
  if (fullOrg) {
    usersFav = dbManagement
      .prepare(
        'SELECT USR.*, FAV.pKey as userParentKey FROM tbUser USR JOIN tbGroupUserFav FAV ON USR.userKey=FAV.key AND FAV.isGroup=0'
      )
      .all();
    groupsFav = dbManagement
      .prepare(
        'SELECT GRP.*,FAV.pKey FROM tbGroup GRP JOIN tbGroupUserFav FAV ON GRP.groupKey=FAV.key AND FAV.isGroup = 1 ORDER BY GRP.groupType DESC,FAV.pKey ASC'
      )
      .all();
  }
  return {
    listGroup: groups ?? [],
    listUser: users ?? [],
    listUserFav: usersFav,
    listGroupFav: groupsFav
  };
}

export function getCompanyORGTree() {
  return getORGTree(false);
}

export function getFullORGTree() {
  return getORGTree(true);
}

export function getRoomList(accountID: number) {
  const stmRoomList = dbManagement.prepare(`
    SELECT *
    FROM tbRoom room
    LEFT JOIN tbRoomDetail detail
    ON room.rID = detail.rdtID
    WHERE room.rRoomStatus = ${TalkConstants.ROOM_STATUS_LIVE}
    AND room.rRoomServerLastMsgBody IS NOT NULL
    AND room.rAccID = ?
    ORDER BY room.rRoomFavorite DESC,  room.rRoomServerLastMsgTime DESC, room.rRoomIRead ASC
  `);

  return stmRoomList.all(accountID);
}

export function getFullRoomDetailListByRoomID(roomId) {
  if (!roomId) {
    return {
      room: {},
      details: []
    };
  }

  const roomResult = dbManagement
    .prepare('SELECT * FROM tbRoom WHERE rID = ?')
    .get(roomId);

  const roomDetailList = dbManagement
    .prepare('SELECT * FROM tbRoomDetail WHERE rdtID = ?')
    .all(roomId);

  return { room: roomResult, details: roomDetailList };
}

export function getMessageList(roomInfo, isFocusedWindow, count) {
  if (isFocusedWindow) {
    const updateMultiple = dbManagement.transaction(roomInfo => {
      dbManagement
        .prepare(
          `UPDATE tbMessage SET
        msgStatus=?
        WHERE msgRoomID=? AND msgStatus <> ? AND msgStatus <> ?
      `
        )
        .run(
          TalkConstants.MessageStatus.STATUS_READ,
          roomInfo.roomID,
          TalkConstants.MessageStatus.STATUS_READ,
          TalkConstants.MessageStatus.STATUS_CLOSED
        );

      dbManagement
        .prepare(
          `UPDATE tbRoom SET
        rRoomIRead=?
        WHERE rID=? AND rRoomIRead = ?
      `
        )
        .run(
          TalkConstants.ROOM_STATUS_READED,
          roomInfo.roomID,
          TalkConstants.ROOM_STATUS_UNREADED
        );
    });
    updateMultiple.immediate(roomInfo);
  }

  let stmMessageList;
  if (roomInfo.firstTimeStamp == 0) {
    stmMessageList = dbManagement.prepare(
      `
      SELECT *
      FROM tbMessage msg
      LEFT JOIN tbMessageDetail detail
      ON msg.msgID = detail.msgdtId
      WHERE msg.msgCreateDate >= ?
      AND msg.msgStatus <> ?
      AND msg.msgRoomID = ?
      ORDER BY msg.msgCreateDate DESC, msg.msgID DESC LIMIT 0 , ${count}
  `
    );
  } else {
    stmMessageList = dbManagement.prepare(
      `
      SELECT *
      FROM tbMessage msg
      LEFT JOIN tbMessageDetail detail
      ON msg.msgID = detail.msgdtId
      WHERE msg.msgCreateDate >= ?
      AND msg.msgStatus <> ?
      AND msg.msgRoomID = ?
      ORDER BY msg.msgCreateDate DESC, msg.msgID DESC
  `
    );
  }

  const result = stmMessageList.all(
    roomInfo.firstTimeStamp,
    TalkConstants.MessageStatus.STATUS_CLOSED,
    roomInfo.roomID
  );

  return {
    roomID: roomInfo.roomID,
    listMessage: result,
    roomKey: roomInfo.roomKey,
    firstTimeStamp: roomInfo.firstTimeStamp
  };
}

export function getFileListWithFilterType(
  roomID,
  roomKey,
  filterType,
  firstTimeStamp,
  count
) {
  let sql = `
  SELECT *
  FROM tbMessage msg
  LEFT JOIN tbMessageDetail detail
  ON msg.msgID = detail.msgdtId
  WHERE msg.msgCreateDate >= ?
  AND msg.msgStatus <> ?
  AND msg.msgRoomID = ?
`;
  if (filterType == 0) {
    sql =
      sql + 'AND msg.msgType = ' + TalkConstants.MessageType.MESSAGE_TYPE_PHOTO;
  } else {
    sql =
      sql +
      ` AND (msg.msgType=${TalkConstants.MessageType.MESSAGE_TYPE_VIDEO}
        OR msg.msgType=${TalkConstants.MessageType.MESSAGE_TYPE_FILE}
        OR msg.msgType=${TalkConstants.MessageType.MESSAGE_TYPE_RECORD})`;
  }

  if (firstTimeStamp == 0) {
    sql =
      sql +
      ` ORDER BY msg.msgCreateDate DESC, msg.msgID DESC LIMIT 0 , ${count}`;
  } else {
    sql = sql + ' ORDER BY msg.msgCreateDate DESC, msg.msgID DESC';
  }

  let stmMessageList = dbManagement.prepare(sql);
  const result = stmMessageList.all(
    firstTimeStamp,
    TalkConstants.MessageStatus.STATUS_CLOSED,
    roomID
  );

  return {
    roomID: roomID,
    roomKey: roomKey,
    filterType: filterType,
    firstTimeStamp: firstTimeStamp,
    messageList: result
  };
}

export function updateAllMessageAsReadedWithRoomId(roomId) {
  dbManagement
    .transaction(roomId => {
      dbManagement
        .prepare(
          'UPDATE tbRoom SET rRoomIRead=? , rRoomUnreadMsgServer = 0 WHERE rID=?'
        )
        .run(TalkConstants.ROOM_STATUS_READED, roomId);

      dbManagement
        .prepare(
          'UPDATE tbMessage SET msgStatus=? WHERE msgRoomID=? AND msgStatus <> ? AND msgStatus <> ?'
        )
        .run(
          TalkConstants.MessageStatus.STATUS_READ,
          roomId,
          TalkConstants.MessageStatus.STATUS_READ,
          TalkConstants.MessageStatus.STATUS_CLOSED
        );
    })
    .immediate(roomId);
}

export function updateAllMessageAsReadedWithRoomKey(roomKey, accountID) {
  const stmFindRoom = findRoomInfoByRoomKey();
  dbManagement
    .transaction((roomKey, accountID) => {
      const result = stmFindRoom.get(roomKey, accountID);
      if (result) {
        dbManagement
          .prepare(
            'UPDATE tbRoom SET rRoomIRead=? , rRoomUnreadMsgServer = 0 WHERE rID=?'
          )
          .run(TalkConstants.ROOM_STATUS_READED, result.rID);

        dbManagement
          .prepare(
            'UPDATE tbMessage SET msgStatus=? WHERE msgRoomID=? AND msgStatus <> ? AND msgStatus <> ?'
          )
          .run(
            TalkConstants.MessageStatus.STATUS_READ,
            result.rID,
            TalkConstants.MessageStatus.STATUS_READ,
            TalkConstants.MessageStatus.STATUS_CLOSED
          );
      }
    })
    .immediate(roomKey, accountID);
}

export function updateAllMessageAsReadedAndSendAPIWithRoomKey(
  roomKey,
  accountID
) {
  const stmFindRoom = findRoomInfoByRoomKey();
  dbManagement
    .transaction((roomKey, accountID) => {
      const result = stmFindRoom.get(roomKey, accountID);
      if (result) {
        dbManagement
          .prepare(
            'UPDATE tbRoom SET rRoomIRead=? , rRoomUnreadMsgServer = 0 WHERE rID=?'
          )
          .run(TalkConstants.ROOM_STATUS_READED, result.rID);

        dbManagement
          .prepare(
            'UPDATE tbMessage SET msgMarkAsReaded=? WHERE msgRoomID=? AND msgMarkAsReaded <> ?'
          )
          .run(
            TalkConstants.MessageServerStatus.MARK_READ,
            result.rID,
            TalkConstants.MessageServerStatus.MARK_SERVER_READ
          );
      }
    })
    .immediate(roomKey, accountID);
}

export function updateAllMessageReadByRoomKeyVer2(roomKey, accountID) {
  const stmFindRoom = dbManagement.prepare(
    'SELECT rID from tbRoom WHERE rRoomKey = ? AND rAccID = ? AND rRoomStatus = ?'
  );
  dbManagement
    .transaction((roomKey, accountID) => {
      const roomInfo = stmFindRoom.get(
        roomKey,
        accountID,
        TalkConstants.ROOM_STATUS_LIVE
      );
      if (roomInfo) {
        const result = dbManagement
          .prepare(
            'SELECT COUNT(*) as numberDetailActive FROM tbRoomDetail WHERE rdtID = ? AND rdtStatus = ? GROUP BY rdtID'
          )
          .get(roomInfo.rID, TalkConstants.ROOM_DETAIL_STATUS_ENTER);
        let numberDetailActive = result.numberDetailActive ?? 0;

        let sql = `UPDATE tbMessage
        SET msgCountUnreaded=IFNULL(msgCountUnreaded, 0) + ${numberDetailActive},
        msgTotalUser = ${numberDetailActive}
        WHERE msgRoomID = ? AND (msgTotalUser <= 0 OR msgTotalUser IS NULL ) AND msgStatus <> ? AND msgNumberKey <> ''
         `;
        dbManagement
          .prepare(sql)
          .run(roomInfo.rID, TalkConstants.MessageStatus.STATUS_CLOSED);
      }
    })
    .immediate(roomKey, accountID);
}

export function updateUnreadMessageWithAllReadAndRemoveCount(
  roomKey,
  accountID,
  userKey,
  timeStamp
) {
  const stmFindRoom = findRoomInfoByRoomKey();
  const stmUpdateMessage = dbManagement.prepare(
    'UPDATE tbMessage SET msgReadedKey = ?, msgCountUnreaded = ? WHERE msgID = ?'
  );
  dbManagement
    .transaction((roomKey, accountID, userKey, timeStamp) => {
      const result = stmFindRoom.get(roomKey, accountID);
      if (result) {
        const readCompareValue = '%|' + userKey + '|%';
        const messagesResult = dbManagement
          .prepare(
            `SELECT * from tbMessage WHERE msgRoomID=? AND msgCreateDate <= ? AND msgNumberKey <> '' AND  msgReadedKey NOT LIKE ?`
          )
          .all(result.rID, timeStamp, readCompareValue);

        if (messagesResult && messagesResult.length > 0) {
          for (let i = 0; i < messagesResult.length; i++) {
            let msg = messagesResult[i];
            let msgReadKey = '';
            if (TextUtils.isEmpty(msg.msgReadedKey)) {
              msgReadKey = '|' + userKey + '|';
            } else {
              msgReadKey = msg.msgReadedKey + userKey + '|';
            }
            let totalUser = msg.msgTotalUser ?? 0;
            let countUnread = msg.msgCountUnreaded ?? 0;
            if (totalUser <= 0) {
              countUnread--;
              // console.log('countUnread decrease', countUnread);
            } else {
              let arrayReaded = msgReadKey
                .split('|')
                .filter(el => el.length != 0);
              countUnread = totalUser - arrayReaded.length;
              // console.log('countUnread', countUnread);
            }

            countUnread = countUnread < 0 ? 0 : countUnread;
            stmUpdateMessage.run(msgReadKey, countUnread, msg.msgID);
          }
        }

        dbManagement
          .prepare(
            'UPDATE tbMessage SET msgStatus=? WHERE msgRoomID=? AND msgStatus <> ? AND msgStatus <> ?'
          )
          .run(
            TalkConstants.MessageStatus.STATUS_READ,
            result.rID,
            TalkConstants.MessageStatus.STATUS_READ,
            TalkConstants.MessageStatus.STATUS_CLOSED
          );
      }
    })
    .immediate(roomKey, accountID, userKey, timeStamp);
}

export function findMessageByRoomKeyNumberKey(roomKey, accountID, numberKey) {
  const roomId = findRoomIDByRoomKey(roomKey, accountID);
  if (roomId >= 0) {
    return dbManagement
      .prepare(
        'SELECT msg.*,detail.* FROM tbMessage msg JOIN tbMessageDetail detail ON msg.msgID = detail.msgdtId WHERE msg.msgRoomID = ?  AND msg.msgNumberKey=?'
      )
      .get(roomId, numberKey);
  }
  return null;
}

export function findRoomIDByRoomKey(roomKey, accountID) {
  const stmFindRoomByRoomKey = findRoomInfoByRoomKey();
  const result = stmFindRoomByRoomKey.get(roomKey, accountID);
  return result ? result.rID : -1;
}

export function findRoomActiveByRoomKey(roomKey, accountID) {
  return dbManagement
    .prepare(
      'SELECT * FROM tbRoom WHERE rRoomKey = ? AND rAccID = ? AND rRoomStatus=?'
    )
    .get(roomKey, accountID, TalkConstants.ROOM_STATUS_LIVE);
}

export function findUserByUserKey(userKey) {
  return dbManagement
    .prepare('SELECT * FROM tbUser WHERE userKey = ?')
    .get(userKey);
}

export function findRoomActiveByListUser(users: [], account_info, accountId) {
  if (users.length == 1) {
    const user = users[0];
    if (user.userKey == account_info.user_key) {
      //My Room
      const result = dbManagement
        .prepare(
          `SELECT room.*
            FROM tbRoom room
            JOIN tbRoomDetail detail
            ON room.rID = detail.rdtID
            WHERE room.rRoomStatus = ?
            AND room.rAccID = ?
            GROUP BY room.rID
            HAVING count(detail.rdtID) = 1
            ORDER BY room.rRoomServerLastMsgTime DESC
            `
        )
        .get(TalkConstants.ROOM_STATUS_LIVE, accountId);
      return result;
    }
  }

  let userKeyList = [];
  users.forEach(user => {
    userKeyList.push(`'${user.userKey}'`);
  });

  if (!userKeyList.includes(`'${account_info.user_key}'`)) {
    userKeyList.push(`'${account_info.user_key}'`);
  }

  let strUserKeyList = userKeyList.join(',');
  let sql = `SELECT master.*
      FROM tbRoom master
      JOIN ((SELECT detail.rdtID, detail.rdtUKey, COUNT(detail.rdtUKey) countUserKey
              FROM tbRoomDetail detail
              WHERE detail.rdtUKey IN (${strUserKeyList})
              AND detail.rdtStatus = ${TalkConstants.ROOM_DETAIL_STATUS_ENTER}
              GROUP BY detail.rdtID) A
              JOIN (SELECT dt2.rdtID, COUNT(dt2.rdtUKey) countUserKey
              FROM tbRoomDetail dt2
              GROUP BY dt2.rdtID
              HAVING COUNT(dt2.rdtUKey) = ${userKeyList.length}) B
              ON A.rdtID = B.rdtID
              AND A.countUserKey = B.countUserKey
            ) detail ON master.rID = detail.rdtID
            WHERE master.rRoomStatus = ?
            AND master.rAccID = ?
            ORDER BY master.rRoomServerLastMsgTime DESC
      `;

  const array = dbManagement
    .prepare(sql)
    .all(TalkConstants.ROOM_STATUS_LIVE, accountId);

  let result = null;
  if (array) {
    for (let i = 0; i < array.length; i++) {
      let r = array[i];
      if (
        r.rRoomType != TalkConstants.ROOM_TYPE_GUEST &&
        r.rRoomType != TalkConstants.ROOM_TYPE_LIVE
      ) {
        result = r;
        break;
      }
    }
  }
  return result;
}

export function insertSendingMessage(
  authenticated,
  roomId,
  roomKey,
  clientId,
  userKey,
  emojMessage,
  message
) {
  let statusSendInit = authenticated
    ? TalkConstants.MessageSendStatus.STATUS_SEND_SENDING
    : TalkConstants.MessageSendStatus.STATUS_SEND_PENDING;
  const stmInsertMessageText = dbManagement.prepare(
    'INSERT INTO tbMessage VALUES(NULL, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
  );
  const result = stmInsertMessageText.run(
    userKey,
    '',
    roomKey,
    roomId,
    TalkConstants.MessageStatus.STATUS_UNREAD,
    '',
    emojMessage,
    message,
    TalkConstants.CREATE_DATE_SENDING,
    '',
    0, //format size
    1, //success alway?
    TalkConstants.MessageType.MESSAGE_TYPE_TEXT,
    clientId,
    statusSendInit,
    '',
    '',
    0,
    0,
    '',
    '',
    '',
    TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
    '',
    '',
    '',
    0,
    0,
    0,
    0,
    0,
    '',
    0
  );
  return result ? result.lastInsertRowid : -1;
}

export function insertInviteMessage(
  accID,
  roomKey,
  createDate,
  inviterName,
  whos
) {
  const userKeyList = whos ? whos.split(',') : [];
  if (!userKeyList || userKeyList.length <= 0) {
    return;
  }

  const stmInsertMessageText = dbManagement.prepare(
    'INSERT INTO tbMessage VALUES(NULL, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
  );
  const stmFindRoomByRoomKey = findRoomInfoByRoomKey();

  const multiInvite = dbManagement.transaction(
    (accID, roomKey, createDate, inviterName, userKeyList) => {
      const roomInfo = stmFindRoomByRoomKey.get(roomKey, accID);
      const roomId = roomInfo ? roomInfo.rID : -1;
      if (roomId >= 0) {
        for (let i = 0; i < userKeyList.length; i++) {
          const userKey = userKeyList[i];
          stmInsertMessageText.run(
            userKey,
            '',
            roomKey,
            roomId,
            TalkConstants.MessageStatus.STATUS_UNREAD,
            '',
            '',
            '',
            createDate,
            inviterName,
            0, //format size
            1, //success alway?
            TalkConstants.MessageType.MESSAGE_TYPE_INVITE,
            '',
            TalkConstants.MessageSendStatus.STATUS_SEND_COMPLETED,
            '',
            '',
            0,
            0,
            '',
            '',
            '',
            TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
            '',
            '',
            '',
            0,
            0,
            0,
            0,
            0,
            '',
            0
          );
        }
      }
    }
  );
  multiInvite.immediate(accID, roomKey, createDate, inviterName, userKeyList);
}

export function insertRoomOutMessage(accID, roomKey, userKey, createDate) {
  const roomId = findRoomIDByRoomKey(roomKey, accID);
  if (roomId >= 0) {
    const stmInsertMessageText = dbManagement.prepare(
      'INSERT INTO tbMessage VALUES(NULL, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
    );
    stmInsertMessageText.run(
      userKey,
      '',
      roomKey,
      roomId,
      TalkConstants.MessageStatus.STATUS_UNREAD,
      '',
      '',
      '',
      createDate,
      '',
      0, //format size
      1, //success alway?
      TalkConstants.MessageType.MESSAGE_TYPE_OUT,
      '',
      0,
      '',
      '',
      0,
      0,
      '',
      '',
      '',
      TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
      '',
      '',
      '',
      0,
      0,
      0,
      0,
      0,
      '',
      0
    );
  }
}

function insertMessageTextV2Impl(roomKey, message, accountUserKey, accountID) {
  const stmFindRoom = findRoomInfoByRoomKey();
  const stmInsertRoom = dbManagement.prepare(
    'INSERT INTO tbRoom VALUES(NULL, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
  );

  const stmFindRoomDetailByRoomIdUserKey = dbManagement.prepare(
    'SELECT * FROM tbRoomDetail WHERE rdtID = ? AND rdtUKey = ?'
  );

  const stmInsertRoomDetail = dbManagement.prepare(
    'INSERT INTO tbRoomDetail VALUES(?,?,?,?,?)'
  );

  const stmInsertMessageText = dbManagement.prepare(
    'INSERT INTO tbMessage VALUES(NULL, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
  );
  const stmUpdateMessageText = dbManagement.prepare(
    `UPDATE tbMessage SET
      msgUserKey=?,
      msgNumberKey=?,
      msgRoomKey=?,
      msgRoomID=?,
      msgStatus=?,
      msgStyle=?,
      msgEmotion=?,
      msgBody=?,
      msgCreateDate=?,
      msgFormat=?,
      msgFormatSize=?,
      msgFailed=?,
      msgType=?,
      msgClientId=?,
      msgSendStatus=?,
      msgReadedKey=?,
      msgUnReadedKey=?,
      msgTotalUser=?,
      msgMarkAsReaded=?,
      msgPreviewImagePath=?,
      msgPreviewDesc=?,
      msgPreviewTitle=?,
      msgThirdPartyID=?,
      msgPreviewCanonicalURL=?,
      msgSIPUserKeyOther=?,
      msgSIPKey=?,
      msgSIPStatus=?,
      msgSIPDuration=?,
      msgSIPType=?,
      msgIsBookmark=?,
      msgIsDeleted=?,
      msgBookmarkKey=?,
      msgCountUnreaded=?
  WHERE msgId=?`
  );

  const stmFindMsgIByRoomIdAndNumberKey = findMsgIDByRoomKeyAndNumberKey();
  const stmFindMsgIDByRoomIDAndUserKeyAndClientIDAndWithOutStatus = findMsgIDByRoomIDAndUserKeyAndClientIDAndWithOutStatus();

  let isNewRoom;
  dbManagement
    .transaction((roomKey, message, accountUserKey, accountID) => {
      const roomInfo = stmFindRoom.get(roomKey, accountID);
      let roomId = -1;
      let unreadMessageServer = 0;
      if (roomInfo) {
        isNewRoom = roomInfo.rRoomStatus != TalkConstants.ROOM_STATUS_LIVE;
        roomId = roomInfo.rID;
        unreadMessageServer = roomInfo.rRoomUnreadMsgServer;
      } else {
        const result = stmInsertRoom.run(
          accountID,
          roomKey,
          '',
          '',
          '',
          '',
          message.createDate,
          TalkConstants.ROOM_STATUS_LIVE,
          -1,
          0,
          0,
          0,
          '',
          '',
          '',
          '',
          0,
          '',
          1,
          0,
          0,
          '',
          '',
          '',
          '',
          '',
          0
        );
        roomId = result.lastInsertRowid;
        isNewRoom = true;
      }

      // update room detail if need
      const detailResult = stmFindRoomDetailByRoomIdUserKey.get(
        roomId,
        message.userKey
      );
      if (!detailResult) {
        stmInsertRoomDetail.run(
          roomId,
          message.userKey,
          '',
          Date.now(),
          TalkConstants.ROOM_DETAIL_STATUS_ENTER
        );
      }
      if (message.userKey != accountUserKey) {
        const detailResult1 = stmFindRoomDetailByRoomIdUserKey.get(
          roomId,
          accountUserKey
        );
        if (!detailResult1) {
          stmInsertRoomDetail.run(
            roomId,
            accountUserKey,
            '',
            Date.now(),
            TalkConstants.ROOM_DETAIL_STATUS_ENTER
          );
        }
      }

      // insert message
      let msgId = null;
      let findMsg = stmFindMsgIByRoomIdAndNumberKey.get(
        message.numberKey,
        roomId
      );
      if (findMsg) {
        msgId = findMsg.msgID;
      } else if (TextUtils.isNotEmpty(message.clientKey)) {
        let result = stmFindMsgIDByRoomIDAndUserKeyAndClientIDAndWithOutStatus.get(
          message.userKey,
          roomId,
          message.clientKey,
          TalkConstants.MessageSendStatus.STATUS_SEND_COMPLETED
        );
        msgId = result ? result.msgID : null;
      }
      if (!msgId || msgId < 0) {
        const result = stmInsertMessageText.run(
          message.userKey,
          message.numberKey ?? '',
          roomKey,
          roomId,
          TalkConstants.MessageStatus.STATUS_UNREAD,
          message.msgStyle ?? '',
          message.emoticon ?? '',
          message.msgBody ?? '',
          message.createDate,
          '',
          0, //format size
          1, //success alway?
          message.messageTypeHard ??
            TalkConstants.MessageType.MESSAGE_TYPE_TEXT,
          message.clientKey ?? '',
          TalkConstants.MessageSendStatus.STATUS_SEND_COMPLETED,
          '',
          '',
          0,
          TalkConstants.MessageServerStatus.MARK_UNREAD,
          '',
          '',
          '',
          TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
          '',
          '',
          '',
          0,
          0,
          0,
          0,
          0,
          '',
          0
        );
        msgId = result.lastInsertRowid;
      } else {
        stmUpdateMessageText.run(
          message.userKey,
          message.numberKey ?? '',
          roomKey,
          roomId,
          TalkConstants.MessageStatus.STATUS_UNREAD,
          message.msgStyle ?? '',
          message.emoticon ?? '',
          message.msgBody ?? '',
          message.createDate,
          '',
          0, //format size
          1, //success alway?
          message.messageTypeHard ??
            TalkConstants.MessageType.MESSAGE_TYPE_TEXT,
          message.clientKey ?? '',
          TalkConstants.MessageSendStatus.STATUS_SEND_COMPLETED,
          '',
          '',
          0,
          TalkConstants.MessageServerStatus.MARK_UNREAD,
          '',
          '',
          '',
          TalkConstants.MessageThirdParty.MESSAGE_MESSENGER_ID,
          '',
          '',
          '',
          0,
          0,
          0,
          0,
          0,
          '',
          0,
          msgId
        );
      }
      let sqlUpdate = `UPDATE tbRoom SET
            rRoomLastMsgID=?,
            rRoomStatus=?,
            rRoomServerLastMsgEmoticon=?,
            rRoomServerLastMsgBody=?,
            rRoomServerLastMsgNumber=?,
            rRoomServerLastMsgDeleted=?,
            rRoomServerLastSenderUserKey=?,
            rRoomServerLastMsgTime=?,
            rRoomCreateDate=?`;
      if (message.userKey != accountUserKey) {
        sqlUpdate =
          sqlUpdate +
          ', rRoomIRead = ' +
          TalkConstants.ROOM_STATUS_UNREADED +
          ', rRoomUnreadMsgServer = ' +
          (unreadMessageServer + 1);
      }

      sqlUpdate = sqlUpdate + ' WHERE rID=?';

      dbManagement
        .prepare(sqlUpdate)
        .run(
          msgId,
          TalkConstants.ROOM_STATUS_LIVE,
          message.emoticon,
          message.msgBody,
          message.numberKey,
          0,
          message.userKey,
          message.createDate,
          message.createDate,
          roomId
        );
    })
    .immediate(roomKey, message, accountUserKey, accountID);

  return isNewRoom;
}

export function insertMessageTextV2(
  roomKey,
  message,
  accountUserKey,
  accountID
) {
  message.messageTypeHard = TalkConstants.MessageType.MESSAGE_TYPE_TEXT;
  return insertMessageTextV2Impl(roomKey, message, accountUserKey, accountID);
}

export function insertMessageBoard(
  roomKey,
  message,
  accountUserKey,
  accountID
) {
  message.messageTypeHard = TalkConstants.MessageType.MESSAGE_TYPE_BOARD_INFO;
  return insertMessageTextV2Impl(roomKey, message, accountUserKey, accountID);
}

export function insertMessageFileV2(
  roomKey,
  message,
  messageDetail,
  accountUserKey,
  accountID
) {
  const stmFindRoom = findRoomInfoByRoomKey();
  const stmInsertRoom = dbManagement.prepare(
    'INSERT INTO tbRoom VALUES(NULL, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
  );

  const stmFindRoomDetailByRoomIdUserKey = dbManagement.prepare(
    'SELECT * FROM tbRoomDetail WHERE rdtID = ? AND rdtUKey = ?'
  );

  const stmInsertRoomDetail = dbManagement.prepare(
    'INSERT INTO tbRoomDetail VALUES(?,?,?,?,?)'
  );

  const stmInsertMessageText = dbManagement.prepare(
    'INSERT INTO tbMessage VALUES(NULL, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
  );
  const stmUpdateMessageText = dbManagement.prepare(
    `UPDATE tbMessage SET
      msgUserKey=?,
      msgNumberKey=?,
      msgRoomKey=?,
      msgRoomID=?,
      msgStatus=?,
      msgStyle=?,
      msgEmotion=?,
      msgBody=?,
      msgCreateDate=?,
      msgFormat=?,
      msgFormatSize=?,
      msgFailed=?,
      msgType=?,
      msgClientId=?,
      msgSendStatus=?,
      msgReadedKey=?,
      msgUnReadedKey=?,
      msgTotalUser=?,
      msgMarkAsReaded=?,
      msgPreviewImagePath=?,
      msgPreviewDesc=?,
      msgPreviewTitle=?,
      msgThirdPartyID=?,
      msgPreviewCanonicalURL=?,
      msgSIPUserKeyOther=?,
      msgSIPKey=?,
      msgSIPStatus=?,
      msgSIPDuration=?,
      msgSIPType=?,
      msgIsBookmark=?,
      msgIsDeleted=?,
      msgBookmarkKey=?,
      msgCountUnreaded=?
  WHERE msgId=?`
  );

  const stmInsertMessageDetail = dbManagement.prepare(
    'INSERT INTO tbMessageDetail VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
  );

  const stmFindMessageDetail = dbManagement.prepare(
    'SELECT * FROM tbMessageDetail WHERE msgdtId=?'
  );

  const stmUpdateMessageDetail = dbManagement.prepare(
    `UPDATE tbMessageDetail SET
    msgdtFileName=?,
    msgdtFileKey=?,
    msgdtStatus=?,
    msgdtFileSize=?,
    msgdtFilePath=?,
    msgdtIP=?,
    msgdtPort=?,
    msgdtHttpServerFileReceiver=?,
    msgdtHttpServerStoreagePath=?,
    msgdtHttpServerFileName=?,
    msgdtHttpServerFilePath=?,
    msgdtHttpServerHostName=?,
    msgdtFileRecordTime=?,
    msgdtHasFile=?,
    msgdtSupportClouddisk=?
  WHERE msgdtId=?`
  );

  const stmFindMsgIByRoomIdAndNumberKey = findMsgIDByRoomKeyAndNumberKey();
  const stmFindMsgIDByRoomIDAndUserKeyAndClientIDAndWithOutStatus = findMsgIDByRoomIDAndUserKeyAndClientIDAndWithOutStatus();

  let isNewRoom;
  dbManagement
    .transaction(
      (roomKey, message, messageDetail, accountUserKey, accountID) => {
        const roomInfo = stmFindRoom.get(roomKey, accountID);
        let roomId = -1;
        let unreadMessageServer = 0;
        let rRoomServerLastMsgTime = 0;
        if (roomInfo) {
          isNewRoom = roomInfo.rRoomStatus != TalkConstants.ROOM_STATUS_LIVE;
          unreadMessageServer = roomInfo.rRoomUnreadMsgServer;
          rRoomServerLastMsgTime = roomInfo.rRoomServerLastMsgTime;
          roomId = roomInfo.rID;
        } else {
          const result = stmInsertRoom.run(
            accountID,
            roomKey,
            '',
            '',
            '',
            '',
            message.createDate,
            TalkConstants.ROOM_STATUS_LIVE,
            -1,
            0,
            0,
            0,
            '',
            '',
            '',
            '',
            0,
            '',
            1,
            0,
            0,
            '',
            '',
            '',
            '',
            '',
            0
          );
          roomId = result.lastInsertRowid;
          isNewRoom = true;
        }

        // update room detail if need
        const detailResult = stmFindRoomDetailByRoomIdUserKey.get(
          roomId,
          message.userKey
        );
        if (!detailResult) {
          stmInsertRoomDetail.run(
            roomId,
            message.userKey,
            '',
            Date.now(),
            TalkConstants.ROOM_DETAIL_STATUS_ENTER
          );
        }
        if (message.userKey != accountUserKey) {
          const detailResult1 = stmFindRoomDetailByRoomIdUserKey.get(
            roomId,
            accountUserKey
          );
          if (!detailResult1) {
            stmInsertRoomDetail.run(
              roomId,
              accountUserKey,
              '',
              Date.now(),
              TalkConstants.ROOM_DETAIL_STATUS_ENTER
            );
          }
        }

        let fromLocal = false;
        // insert message file
        let msgId = null;
        let findMsg = stmFindMsgIByRoomIdAndNumberKey.get(
          message.numberKey,
          roomId
        );

        if (findMsg) {
          msgId = findMsg.msgID;
        } else if (TextUtils.isNotEmpty(message.clientKey)) {
          result = stmFindMsgIDByRoomIDAndUserKeyAndClientIDAndWithOutStatus.get(
            message.userKey,
            roomId,
            message.clientKey,
            TalkConstants.MessageSendStatus.STATUS_SEND_COMPLETED
          );
          msgId = result ? result.msgID : null;
          fromLocal = true;
        }

        if (!msgId || msgId < 0) {
          const result = stmInsertMessageText.run(
            message.userKey,
            message.numberKey ?? '',
            roomKey,
            roomId,
            TalkConstants.MessageStatus.STATUS_UNREAD,
            '',
            message.emoticon ?? '',
            messageDetail.fileName ?? '', // body
            message.createDate,
            '',
            0, //format size
            1, //success alway?
            message.messageType,
            message.clientKey ?? '',
            TalkConstants.MessageSendStatus.STATUS_SEND_COMPLETED,
            '',
            '',
            0,
            TalkConstants.MessageServerStatus.MARK_UNREAD,
            '',
            '',
            '',
            message.messageThirdParty ??
              TalkConstants.MessageThirdParty.MESSAGE_FAST_HTTP_ID,
            '',
            '',
            '',
            0,
            0,
            0,
            0,
            0,
            '',
            0
          );
          msgId = result.lastInsertRowid;

          stmInsertMessageDetail.run(
            msgId,
            messageDetail.fileName,
            messageDetail.fileKey,
            messageDetail.fileStatus,
            messageDetail.fileSize,
            messageDetail.filePath,
            messageDetail.fileIP,
            messageDetail.filePort,
            messageDetail.fileReceiver,
            messageDetail.fileStoragePath,
            messageDetail.fileServerFileName,
            messageDetail.fileServerFilePath,
            messageDetail.fileServerHostName,
            messageDetail.fileRecordTime,
            messageDetail.hasFile,
            messageDetail.clouddisk,
            0,
            0
          );
        } else {
          let fileName = messageDetail.fileName;
          let fileKey = messageDetail.fileKey;
          let fileStatus = messageDetail.fileStatus;
          let fileSize = messageDetail.fileSize;
          let filePath = messageDetail.filePath;
          let fileAddress = messageDetail.fileIP;
          let filePort = messageDetail.filePort;
          let httpFileReceiver = messageDetail.fileReceiver;
          let httpServerStoragePath = messageDetail.fileStoragePath;
          let httpServerFileName = messageDetail.fileServerFileName;
          let httpServerFilePath = messageDetail.fileServerFilePath;
          let httpHostName = messageDetail.fileServerHostName;
          let recordTime = messageDetail.fileRecordTime;
          let hasFile = messageDetail.hasFile;
          let clouddisk = messageDetail.clouddisk;

          // update message detail
          if (!fromLocal) {
            const prevDetail = stmFindMessageDetail.get(msgId);
            if (prevDetail) {
              fileStatus = prevDetail.msgdtStatus;
              fileAddress = prevDetail.msgdtIP;
              filePort = prevDetail.msgdtPort;

              if (fileSize <= 0) {
                fileSize = prevDetail.msgdtFileSize;
              }

              filePath = prevDetail.msgdtFilePath;
              fileKey = prevDetail.msgdtFileKey;

              if (TextUtils.isEmpty(httpFileReceiver)) {
                httpFileReceiver = prevDetail.msgdtHttpServerFileReceiver;
              }

              if (TextUtils.isEmpty(httpServerStoragePath)) {
                httpServerStoragePath = prevDetail.msgdtHttpServerStoreagePath;
              }

              if (TextUtils.isEmpty(httpServerFilePath)) {
                httpServerFilePath = prevDetail.msgdtHttpServerFilePath;
              }

              if (TextUtils.isEmpty(httpServerFileName)) {
                httpServerFileName = prevDetail.msgdtHttpServerFileName;
              }

              if (TextUtils.isEmpty(httpHostName)) {
                httpHostName = prevDetail.msgdtHttpServerHostName;
              }
            }
          }

          stmUpdateMessageDetail.run(
            fileName,
            fileKey,
            fileStatus,
            fileSize,
            filePath,
            fileAddress,
            filePort,
            httpFileReceiver,
            httpServerStoragePath,
            httpServerFileName,
            httpServerFilePath,
            httpHostName,
            recordTime,
            hasFile,
            clouddisk,
            msgId
          );

          stmUpdateMessageText.run(
            message.userKey,
            message.numberKey ?? '',
            roomKey,
            roomId,
            TalkConstants.MessageStatus.STATUS_UNREAD,
            '',
            message.emoticon ?? '',
            messageDetail.fileName ?? '', // body
            message.createDate,
            '',
            0, //format size
            1, //success alway?
            message.messageType,
            message.clientKey ?? '',
            TalkConstants.MessageSendStatus.STATUS_SEND_COMPLETED,
            '',
            '',
            0,
            TalkConstants.MessageServerStatus.MARK_UNREAD,
            '',
            '',
            '',
            message.messageThirdParty ??
              TalkConstants.MessageThirdParty.MESSAGE_FAST_HTTP_ID,
            '',
            '',
            '',
            0,
            0,
            0,
            0,
            0,
            '',
            0,
            msgId
          );
        }

        if (
          TextUtils.isNotEmpty(message.numberKey) &&
          rRoomServerLastMsgTime < message.createDate
        ) {
          let sqlUpdate = `UPDATE tbRoom SET
            rRoomLastMsgID=?,
            rRoomStatus=?,
            rRoomServerLastMsgDeleted=?,
            rRoomServerLastMsgNumber=?,
            rRoomServerLastSenderUserKey=?,
            rRoomServerLastMsgBody=?,
            rRoomServerLastMsgEmoticon=?,
            rRoomServerLastMsgTime=?,
            rRoomCreateDate=?`;
          if (message.userKey != accountUserKey) {
            sqlUpdate =
              sqlUpdate +
              ', rRoomIRead = ' +
              TalkConstants.ROOM_STATUS_UNREADED +
              ', rRoomUnreadMsgServer = ' +
              (unreadMessageServer + 1);
          }

          sqlUpdate = sqlUpdate + ' WHERE rID=?';

          dbManagement
            .prepare(sqlUpdate)
            .run(
              msgId,
              TalkConstants.ROOM_STATUS_LIVE,
              0,
              message.numberKey,
              message.userKey,
              messageDetail.fileName,
              message.emoticon,
              message.createDate,
              message.createDate,
              roomId
            );
        }
      }
    )
    .immediate(roomKey, message, messageDetail, accountUserKey, accountID);

  return isNewRoom;
}

export function updateMessageFileWithRoomKeyNumberKey(
  roomKey,
  numberKey,
  msg,
  msgDetail
) {
  const updateTrasaction = dbManagement.transaction(
    (roomKey, numberKey, msg, msgDetail) => {
      const result = dbManagement
        .prepare(
          'SELECT  msgID,msgCreateDate FROM tbMessage WHERE msgRoomKey = ? AND msgNumberKey=?'
        )
        .get(roomKey, numberKey);

      const msgID = result ? result.msgID : -1;
      const msgCreateDate = result ? result.msgCreateDate : 0;
      if (msgID >= 0) {
        if (msgCreateDate < msg.createDate) {
          dbManagement
            .prepare(
              'UPDATE tbMessage SET msgCreateDate=?,msgThirdPartyID=? WHERE msgID = ?'
            )
            .run(msg.createDate, msg.messageThirdParty, msgID);
        } else {
          dbManagement
            .prepare('UPDATE tbMessage SET msgThirdPartyID=? WHERE msgID = ?')
            .run(msg.messageThirdParty, msgID);
        }

        let sql = 'UPDATE tbMessageDetail SET msgdtStatus= ? , msgdtFileSize=?';
        if (TextUtils.isNotEmpty(msgDetail.serverFilePath)) {
          sql += `, msgdtHttpServerFilePath='${msgDetail.serverFilePath}'`;
        }
        if (TextUtils.isNotEmpty(msgDetail.serverFileName)) {
          sql += `, msgdtHttpServerFileName='${msgDetail.serverFileName}'`;
        }
        if (TextUtils.isNotEmpty(msgDetail.serverHostName)) {
          sql += `, msgdtHttpServerHostName='${msgDetail.serverHostName}'`;
        }
        sql += ' WHERE msgdtId = ?';
        dbManagement
          .prepare(sql)
          .run(msgDetail.fileStatus, msgDetail.fileSize, msgID);
        return true;
      }
      return false;
    }
  );

  return updateTrasaction.immediate(roomKey, numberKey, msg, msgDetail);
}

export function updateAllFileUploadToFailed() {
  try {
    dbManagement
      .prepare(
        'UPDATE tbMessageDetail SET msgdtStatus = ? WHERE msgdtStatus = ?'
      )
      .run(
        TalkConstants.MessageFileStatus.FILE_MSG_UPLOAD_INTERRUPT,
        TalkConstants.MessageFileStatus.FILE_MSG_UPLOAD_WORKING
      );
    console.log('Update all file  uploading and message to failed');
    updateAllMessageSendingToMaybePending();
  } catch (error) {}
}

export function updateFileStatusWithData(roomKey, numberKey, data) {
  const result = dbManagement
    .prepare(
      'SELECT msgID from tbMessage WHERE msgNumberKey=? AND msgRoomKey=?'
    )
    .get(numberKey, roomKey);
  const msgID = result ? result.msgID : -1;
  if (msgID >= 0) {
    if (data.actionFailedUpload) {
      dbManagement
        .prepare('UPDATE tbMessageDetail SET msgdtStatus = ? WHERE msgdtId = ?')
        .run(TalkConstants.MessageFileStatus.FILE_MSG_UPLOAD_INTERRUPT, msgID);
    } else if (data.filePath) {
      dbManagement
        .prepare(
          'UPDATE tbMessageDetail SET msgdtFilePath = ? WHERE msgdtId = ?'
        )
        .run(data.filePath, msgID);
    }
  }
}

export function updateAllMessageSendingToMaybePending() {
  dbManagement
    .prepare('UPDATE tbMessage SET msgSendStatus = ? WHERE msgSendStatus = ?')
    .run(
      TalkConstants.MessageSendStatus.STATUS_SEND_MAYBE_PENDING,
      TalkConstants.MessageSendStatus.STATUS_SEND_SENDING
    );
  console.log('Update all sending message to maybe pending');
}

export function updateMessagePendingToSending(msgID) {
  dbManagement
    .prepare('UPDATE tbMessage SET msgSendStatus = ? WHERE msgID = ?')
    .run(TalkConstants.MessageSendStatus.STATUS_SEND_SENDING, msgID);
}

export function deleteMessage(msgID) {
  dbManagement.prepare('DELETE FROM tbMessage WHERE msgID = ?').run(msgID);
}

export function getOnlineUserWithRoomKey(roomKey, accountID) {
  if (!roomKey || accountID < 0) {
    return [];
  }

  const roomResult = dbManagement
    .prepare(
      `SELECT detail.rdtUkey
    FROM tbRoom room
    LEFT JOIN tbRoomDetail detail
    ON room.rID = detail.rdtID
    WHERE room.rRoomKey=? AND room.rAccID = ? AND detail.rdtStatus = ?`
    )
    .all(roomKey, accountID, TalkConstants.ROOM_DETAIL_STATUS_ENTER);

  let userKeyList = [];
  if (roomResult) {
    roomResult.forEach(element => {
      userKeyList.push(element.rdtUKey);
    });
  }
  return userKeyList;
}

export function queryContactAndRoomWithSearchText(
  searchText,
  accountID,
  showOnlineOnly = false
) {
  const chosunSearchText = myUtil.decomposeSylLabelsNotExceptMIDTAIL(
    searchText
  );

  let sql = `
  SELECT USR.*, GUS.groupKey
  FROM tbUser USR
  JOIN tbGroupUser GUS
  ON USR.userId = GUS.userID
  JOIN tbGroup G
  ON G.groupId = GUS.groupId
  `;

  let addChosun = false;
  sql =
    sql +
    ` WHERE
               (USR.userEmail like ? COLLATE NOCASE
                OR USR.userAlias like ? COLLATE NOCASE
                OR USR.userNickName like ? COLLATE NOCASE
                OR USR.userLocalPhone like ? COLLATE NOCASE
                OR USR.userMobilePhone like ? COLLATE NOCASE`;
  if (TextUtils.isNotEmpty(chosunSearchText)) {
    sql = sql + ' OR USR.userChosunText like ? COLLATE NOCASE';
    addChosun = true;
  }

  sql = sql + ' OR USR.userFullName like ? COLLATE NOCASE)';

  if (showOnlineOnly) {
    sql =
      sql +
      ' AND (USR.userStatus > 0 AND USR.userStatus < 8 OR USR.userStatus = 9)';
  }

  sql = sql + ' AND GUS.groupId >= 0  ORDER BY G.groupId ASC';

  const searchParam = '%' + searchText + '%';
  const searchChosunParam = '%' + chosunSearchText + '%';
  const stm = dbManagement.prepare(sql);

  let orgResult;
  if (addChosun) {
    orgResult = stm.all(
      searchParam,
      searchParam,
      searchParam,
      searchParam,
      searchParam,
      searchChosunParam,
      searchParam
    );
  } else {
    orgResult = stm.all(
      searchParam,
      searchParam,
      searchParam,
      searchParam,
      searchParam,
      searchParam
    );
  }

  return {
    org: orgResult,
    searchText: searchText,
    searchChosun: chosunSearchText
  };
}

export function deleteRoomAndMessageWithRoomKey(roomKey, accountId) {
  const roomId = findRoomIDByRoomKey(roomKey, accountId);
  if (roomId >= 0) {
    dbManagement
      .transaction((roomId, roomKey) => {
        dbManagement
          .prepare('DELETE FROM tbMessage WHERE msgRoomID = ?')
          .run(roomId);
        dbManagement.prepare('DELETE FROM tbRoom WHERE rID = ?').run(roomId);
        dbManagement
          .prepare('DELETE FROM tbRoomTimeMsg WHERE rtRoomKey = ?')
          .run(roomKey);
      })
      .immediate(roomId, roomKey);
  }
}

export function getBeginTimeFromMessageList(
  accId,
  roomKey,
  clientKey,
  oldTimeInfo,
  firstTime,
  count
) {
  const stmFindRoom = findRoomInfoByRoomKey(roomKey);
  const roomInfo = stmFindRoom.get(roomKey, accId);
  const roomId = roomInfo?.rID;
  let resultTime = -1;
  let resultCount = 0;
  let result = null;
  if (roomId >= 0) {
    let stmMessageList = null;
    if (firstTime == 0) {
      stmMessageList = dbManagement.prepare(
        `
        SELECT msg.msgCreateDate
        FROM tbMessage msg
        LEFT JOIN tbMessageDetail detail
        ON msg.msgID = detail.msgdtId
        WHERE msg.msgCreateDate >= ?
        AND msg.msgStatus <> ?
        AND msg.msgRoomID = ?
        ORDER BY msg.msgCreateDate DESC, msg.msgID DESC LIMIT 0 , ?
    `
      );
      result = stmMessageList.all(
        oldTimeInfo,
        TalkConstants.MessageStatus.STATUS_CLOSED,
        roomId,
        count
      );

      if (result && result.length >= count) {
        resultCount = result.length;
        resultTime = result[result.length - 1].msgCreateDate;
      }
    } else {
      stmMessageList = dbManagement.prepare(
        `
        SELECT msg.msgCreateDate
        FROM tbMessage msg
        LEFT JOIN tbMessageDetail detail
        ON msg.msgID = detail.msgdtId
        WHERE msg.msgCreateDate < ?
        AND msg.msgCreateDate >= ?
        AND msg.msgStatus <> ?
        AND msg.msgRoomID = ?
        ORDER BY msg.msgCreateDate DESC, msg.msgID DESC LIMIT 0 , ?
    `
      );
      result = stmMessageList.all(
        firstTime,
        oldTimeInfo,
        TalkConstants.MessageStatus.STATUS_CLOSED,
        roomId,
        count
      );

      if (result && result.length > 0) {
        resultCount = result.length;
        resultTime = result[result.length - 1].msgCreateDate;
      }
    }
  }

  return {
    roomKey: roomKey,
    clientKey: clientKey,
    firstTimeStamp: resultTime,
    talkCount: resultCount,
    roomID: roomId
  };
}

export function deleteAllTalkInfo() {
  dbManagement.prepare('DELETE FROM tbRoomTimeMsg').run();
}

export function findPrevTalkInfo(roomKey) {
  const info = dbManagement
    .prepare(
      'SELECT rtPrevOldTime,rtPrevLatestTime  FROM tbRoomTimeMsg WHERE rtRoomKey = ?'
    )
    .get(roomKey);
  return info;
}

export function insertOrUpdateTalkInfo(roomKey, oldTime, latestTime) {
  const info = dbManagement
    .prepare('SELECT * FROM tbRoomTimeMsg WHERE rtRoomKey = ?')
    .get(roomKey);
  if (info) {
    dbManagement
      .prepare(
        'UPDATE tbRoomTimeMsg SET rtOldTime=? , rtLatestTime=? WHERE rtId=?'
      )
      .run(oldTime, latestTime, info.rtId);
  } else {
    dbManagement
      .prepare('INSERT INTO tbRoomTimeMsg VALUES(NULL,?,?,?,?,?)')
      .run(roomKey, 0, 0, oldTime, latestTime);
  }
}

export function transferCurrentToPrevTalkInfo() {
  dbManagement
    .transaction(() => {
      dbManagement
        .prepare(
          'UPDATE tbRoomTimeMsg SET rtPrevOldTime=rtOldTime , rtPrevLatestTime=rtLatestTime WHERE rtOldTime > 0 AND rtLatestTime > 0'
        )
        .run();

      dbManagement
        .prepare('UPDATE tbRoomTimeMsg SET rtOldTime=0 , rtLatestTime=0')
        .run();
    })
    .immediate();
}
