import Packet from '../../socket/packet';
import TextUtils from '../../utils/text.util';
import appPrefs from '../../utils/pref';

function cleanInvalidXMLChars(str) {
  var regex = /((?:[\0-\x08\x0B\f\x0E-\x1F\uFFFD\uFFFE\uFFFF]|[\uD800-\uDBFF](?![\uDC00-\uDFFF])|(?:[^\uD800-\uDBFF]|^)[\uDC00-\uDFFF]))/g;
  return str.replace(regex, '');
}

const USE_ROOM_LIST_SIX_MONTH_LIMIT = false;

export class LoginPacket extends Packet {
  constructor(domain, userId, password, pid, zip, deviceName, loginMode) {
    super();
    this.domain = domain;
    this.userId = userId;
    this.password = password;
    this.pid = pid;
    this.zip = zip;
    this.deviceName = deviceName;
    this.loginMode = loginMode;
  }

  extensionXml(root) {
    if (appPrefs.getMobileLogin()) {
      let user = root.ele('USER');
      let login = user.ele('LOGIN');
      login.att('ID', this.userId);
      login.att('PASS', this.password);
      login.att('DOMAIN', this.domain);
      login.att('DEVICE', 'ANDROID');
      login.att('DEVICEID', this.pid);
      login.att('DEVICEMODEL', 'TalkElectron');
      login.att('PUSHTOKEN', 'JkCjMsudAcNbbkwwvv016VaZMzxuOyaA');
      login.att('PUSHTOKENSIP', 'JkCjMsudAcNbbkwwvv016VaZMzxuOyaA');
      login.att('ZIP', this.zip);
      if (this.loginMode) {
        login.att('MODE', this.loginMode);
      }
    } else {
      let user = root.ele('USER');
      let login = user.ele('LOGIN');
      login.att('ID', this.userId);
      login.att('PASS', this.password);
      login.att('DOMAIN', this.domain);
      login.att('AUTOLOGINKEY', '');
      login.att('MODE', this.loginMode ?? '');
      login.att('PUNCH', '0');
      login.att('DEVICE', this.deviceName);
      login.att('PID', this.pid);
      login.att('ZIP', this.zip);
    }
  }
}

export class LoginResAccessControlPacket extends Packet {
  constructor(macAddressList) {
    super();
    this.macAddressList = macAddressList;
  }

  extensionXml(root) {
    let user = root.ele('USER');
    let loginRes = user.ele('LOGINRES');
    let loginAuth = loginRes.ele('LOGINAUTH');
    loginAuth.att('USB', '');
    loginAuth.att('MAC', this.macAddressList);
    loginAuth.att('CARD', '');
  }
}

export class ReLoginPacket extends Packet {
  constructor(userKey, deviceId, zip) {
    super();
    this.deviceId = deviceId;
    this.userKey = userKey;
    this.zip = zip;
  }

  extensionXml(root) {
    let user = root.ele('USER');
    let relogin = user.ele('RELOGIN');
    relogin.att('USERKEY', this.userKey);
    relogin.att('DEVICEID', this.deviceId);
    relogin.att('ZIP', this.zip);
    relogin.att('PUSHTOKEN', '');
    relogin.att('PUSHTOKENSIP', '');
  }
}

export class WebAutoLoginPackage extends Packet {
  constructor() {
    super();
  }

  extensionXml(root) {
    let user = root.ele('USER');
    let webLogin = user.ele('WEBAUTOLOGIN');
    webLogin.att('KIND', 1);
    webLogin.att('PARAMETER', '');
    webLogin.att('PARAMETER2', '');
  }
}

export class LogoutPacket extends Packet {
  constructor(_device) {
    super();
    this.device = _device;
  }

  extensionXml(root) {
    if (appPrefs.getMobileLogin()) {
      let user = root.ele('USER');
      let logout = user.ele('LOGOUT');
      logout.att('PUNCH', '1');
      logout.att('DEVICE', '4');
    } else {
      let user = root.ele('USER');
      let logout = user.ele('LOGOUT');
      logout.att('PUNCH', '1');
      logout.att('DEVICE', this.device);
    }
  }
}

export class UserList extends Packet {
  constructor(domain, userKey, language) {
    super();
    this.domain = domain;
    this.userKey = userKey;
    this.language = language;
  }

  extensionXml(root) {
    let user = root.ele('USER');
    let userList = user.ele('USERLIST');
    userList.att('USERKEY', this.userKey);
    userList.att('SORT', 'JOBTITLE');
    userList.att('LANGUAGE', this.language);
    userList.att('DOMAIN', this.domain);
  }
}

export class FolderCreatePacket extends Packet {
  constructor(folderName, folderKey, folderParentKey) {
    super();
    this.folderName = folderName;
    this.folderKey = folderKey;
    this.folderParentKey = folderParentKey;
  }

  extensionXml(root) {
    let user = root.ele('USER');
    let folderCreate = user.ele('FOLDERCREATE');
    folderCreate.att('NAME', this.folderName);
    folderCreate.att('PFOLDERKEY', this.folderParentKey);
    folderCreate.att('FOLDERKEY', this.folderKey);
  }
}

export class FolderDelPacket extends Packet {
  constructor(folderKey) {
    super();
    this.folderKey = folderKey;
  }

  extensionXml(root) {
    let user = root.ele('USER');
    let folderDel = user.ele('FOLDERDEL');
    folderDel.att('FOLDERKEY', this.folderKey);
  }
}

export class FolderUpdateChildPacket extends Packet {
  constructor(folderKey, userKeyList) {
    super();
    this.folderKey = folderKey;
    this.userKeyList = userKeyList;
  }

  extensionXml(root) {
    let combineUserKey =
      !this.userKeyList || this.userKeyList.length <= 0
        ? ''
        : this.userKeyList.join(',');
    let user = root.ele('USER');
    let userupdate = user.ele('USERUPDATE');
    userupdate.att('FOLDERKEY', this.folderKey);
    userupdate.att('GROUPKEY', '');
    userupdate.att('USERKEY', combineUserKey);
  }
}

export class UserModInfoPacket extends Packet {
  constructor(time) {
    super();
    this.time = time;
  }

  extensionXml(root) {
    let user = root.ele('USER');
    let userMod = user.ele('USERMODINFO');
    userMod.att('LAST_MODTIME', this.time);
  }
}

export class AlarmBoardNotificationPacket extends Packet {
  constructor(boardInfo) {
    super();
    console.log('AlarmBoardNotificationPacket:', boardInfo);
    this.boardInfo = boardInfo;
  }

  extensionXml(root) {
    let alarm = root.ele('ALARM');
    let fav = alarm.ele('CHATROOMBOARDALARM');
    fav.att('USERKEY', this.boardInfo.userKey);
    fav.att('CONTENT', this.boardInfo.content);
    fav.att('NOTI', this.boardInfo.notification);
    fav.att('ROOMKEY', this.boardInfo.roomKey);
    fav.att('BOARDKEY', this.boardInfo.boardKey);
    fav.att('REGTIME', this.boardInfo.rawTime);
    console.log('fav:', fav);
  }
}

export class OrgFavoritePacket extends Packet {
  constructor(key, isGroup, isAdd) {
    super();
    this.key = key;
    this.isGroup = isGroup;
    this.isAdd = isAdd;
  }

  extensionXml(root) {
    let alarm = root.ele('ALARM');
    let fav = alarm.ele('FAVORITE');
    fav.att('TARGET', this.isGroup ? 'G' : 'U');
    fav.att('KEY', this.key);
    fav.att('MODE', this.isAdd ? 'ADD' : 'DEL');
  }
}

export class ChangeStatus extends Packet {
  constructor(userKey, status) {
    super();
    this.userKey = userKey;
    this.status = status;
  }

  extensionXml(root) {
    let user = root.ele('USER');
    let stateE = user.ele('STATUS');
    stateE.att('USERKEY', this.userKey);
    stateE.att('STATE', this.status);
  }
}

export class ChangeNickName extends Packet {
  constructor(userKey, name) {
    super();
    this.userKey = userKey;
    this.name = name;
  }

  extensionXml(root) {
    let user = root.ele('USER');
    let nickNameE = user.ele('NICKNAME');
    nickNameE.att('USERKEY', this.userKey);
    nickNameE.att('NAME', cleanInvalidXMLChars(this.name));
  }
}

export class LivePacket extends Packet {
  constructor() {
    super();
  }

  extensionXml(root) {
    let alarm = root.ele('ALARM');
    let live = alarm.ele('LIVE');
  }
}

export class TimeCardPacket extends Packet {
  constructor() {
    super();
  }

  extensionXml(root) {
    let alarm = root.ele('ALARM');
    let timeCard = alarm.ele('TIMECARD');
  }
}

export class RoomListPacket extends Packet {
  constructor(statusTime) {
    super();
    this.strStatus = statusTime < 0 ? '' : statusTime;
    if (USE_ROOM_LIST_SIX_MONTH_LIMIT) {
      if (this.strStatus == 0 || this.strStatus == '') {
        this.strStatus = Date.now() - 1000 * 60 * 60 * 24 * 30 * 12;
      }
    }
  }

  extensionXml(root) {
    let chat = root.ele('CHAT');
    let roomList = chat.ele('ROOMLIST');
    roomList.att('ROOMKEY', '');
    roomList.att('STATUSTIME', this.strStatus);
  }
}

export class AlertRoomPacket extends Packet {
  constructor(roomKey, enable) {
    super();
    this.roomKey = roomKey;
    this.enable = enable;
  }
  extensionXml(root) {
    let user = root.ele('USER');
    let alert = user.ele('ALERTROOM');
    alert.att('ROOMKEY', this.roomKey);
    alert.att('ENABLE', this.enable ? 1 : 0);
  }
}

export class RoomCreatePacket extends Packet {
  constructor(roomTag, roomAlias, userList: []) {
    super();
    this.roomTag = roomTag;
    this.roomAlias = roomAlias;
    this.userList = userList;
  }

  extensionXml(root) {
    let chat = root.ele('CHAT');
    let roomCreate = chat.ele('ROOMCREATE');
    roomCreate.att('TAG', this.roomTag);
    if (TextUtils.isNotEmpty(this.roomAlias)) {
      roomCreate.att('ALIAS', this.roomAlias);
    }
    this.userList.forEach(info => {
      let room = roomCreate.ele('ROOM');
      room.att('WHO', info.key);
      room.att('NAME', info.name);
    });
  }
}

export class RoomOutPacket extends Packet {
  constructor(roomKey, userKey) {
    super();
    this.roomKey = roomKey;
    this.userKey = userKey;
  }
  extensionXml(root) {
    let chat = root.ele('CHAT');
    let roomOut = chat.ele('ROOMOUT');
    roomOut.att('ROOMKEY', this.roomKey);
    roomOut.att('USERKEY', this.userKey);
  }
}

export class RoomOnePacket extends Packet {
  constructor(roomKey) {
    super();
    this.roomKey = roomKey;
  }
  extensionXml(root) {
    let chat = root.ele('CHAT');
    let roomOne = chat.ele('ROOMONE');
    roomOne.att('ROOMKEY', this.roomKey);
  }
}

export class RoomInvitePacket extends Packet {
  constructor(roomKey, myUserKey, listUser) {
    super();
    this.roomKey = roomKey;
    this.myUserKey = myUserKey;
    this.listUser = listUser;
  }
  extensionXml(root) {
    let chat = root.ele('CHAT');
    let roomInvite = chat.ele('ROOMINVITE');
    roomInvite.att('ROOMKEY', this.roomKey);
    roomInvite.att('USERKEY', this.myUserKey);
    this.listUser.forEach(info => {
      let room = roomInvite.ele('ROOM');
      room.att('WHO', info.key);
      room.att('NAME', info.name);
    });
  }
}

export class RoomNamePacket extends Packet {
  constructor(roomKey, isPublic, roomName) {
    super();
    this.roomKey = roomKey;
    this.isPublic = isPublic;
    this.roomName = roomName;
  }
  extensionXml(root) {
    let chat = root.ele('CHAT');
    let roomName = chat.ele('ROOMNAME');
    roomName.att('ROOMKEY', this.roomKey);
    roomName.att('NAME', cleanInvalidXMLChars(this.roomName));
    roomName.att('PRIVATE', this.isPublic ? 0 : 1);
  }
}

export class RoomFavoritePacket extends Packet {
  constructor(roomKey, isAdd) {
    super();
    this.roomKey = roomKey;
    this.isAdd = isAdd;
  }
  extensionXml(root) {
    let chat = root.ele('CHAT');
    let favRoom = chat.ele('FAVORITEROOM');
    favRoom.att('ROOMKEY', this.roomKey);
    favRoom.att('MODE', this.isAdd ? 'ADD' : 'DEL');
  }
}

export class AllMessageAsReadPacket extends Packet {
  constructor(roomKey, userKey) {
    super();
    this.roomKey = roomKey;
    this.userKey = userKey;
  }
  extensionXml(root) {
    let chat = root.ele('CHAT');
    let allRead = chat.ele('ALLREAD');
    allRead.att('ROOMKEY', this.roomKey);
    allRead.att('USERKEY', this.userKey);
  }
}

export class CancelMessagePacket extends Packet {
  constructor(roomKey, userKey, numberKey, timeStamp) {
    super();
    this.roomKey = roomKey;
    this.userKey = userKey;
    this.numberKey = numberKey;
    this.timeStamp = timeStamp;
  }
  extensionXml(root) {
    let chat = root.ele('CHAT');
    let cancel = chat.ele('CANCELMSG');
    cancel.att('ROOMKEY', this.roomKey);
    cancel.att('USERKEY', this.userKey);
    cancel.att('NUMBER', this.numberKey);
    cancel.att('TIMESTAMP', this.timeStamp);
  }
}

export class SaveFileClouddiskPacket extends Packet {
  constructor(roomKey, numberKey, timeStamp) {
    super();
    this.roomKey = roomKey;
    this.numberKey = numberKey;
    this.timeStamp = timeStamp;
  }
  extensionXml(root) {
    let file = root.ele('FILE');
    let fileSave = file.ele('MFILESAVE');
    fileSave.att('ROOMKEY', this.roomKey);
    fileSave.att('NUMBER', this.numberKey);
    fileSave.att('TIMESTAMP', this.timeStamp);
  }
}

export class HTTPFileCancelPacket extends Packet {
  constructor(roomKey, fileKey) {
    super();
    this.roomKey = roomKey;
    this.fileKey = fileKey;
  }
  extensionXml(root) {
    let file = root.ele('FILE');
    let httpFileCancel = file.ele('MFILECANCEL');
    httpFileCancel.att('ROOMKEY', this.roomKey);
    httpFileCancel.att('FILEKEY', this.fileKey);
  }
}

export class HTTPFileRequestPacket extends Packet {
  constructor(
    roomKey,
    fileType,
    filePath,
    fileSize,
    fileName,
    clientKey,
    receiverList,
    recordTime,
    thumb64
  ) {
    super();
    this.roomKey = roomKey;
    this.fileSize = fileSize;
    this.fileType = fileType;
    this.filePath = filePath;
    this.fileName = fileName;
    this.clientKey = clientKey;
    this.receiverList = receiverList;
    this.recordTime = recordTime;
    this.thumb64 = thumb64;
  }
  extensionXml(root) {
    let file = root.ele('FILE');
    let httpFileReq = file.ele('HTTPFILEREQ');
    httpFileReq.att('SIZE', this.fileSize);
    httpFileReq.att('ROOMKEY', this.roomKey);
    httpFileReq.att('FILETYPE', this.fileType);
    httpFileReq.att('PATH', this.filePath);
    httpFileReq.att('NAME', this.fileName);
    httpFileReq.att('CLIENTKEY', this.clientKey);
    httpFileReq.att('RECVER', this.receiverList.join('|'));
    if (this.recordTime > 0) {
      httpFileReq.att('RECORDINGTIME', this.recordTime);
    }
    file.ele('THUMBNAIL', null, this.thumb64);
  }
}

export class HTTPFileSendingPacket extends Packet {
  constructor(
    status,
    number,
    fileKey,
    roomKey,
    fileType,
    filePath,
    sent,
    fileSize,
    fileName,
    receiver,
    time
  ) {
    super();
    this.status = status;
    this.number = number;
    this.fileKey = fileKey;
    this.roomKey = roomKey;
    this.fileType = fileType;
    this.filePath = filePath;
    this.sent = sent;
    this.fileSize = fileSize;
    this.fileName = fileName;
    this.receiver = receiver;
    this.time = time;
  }
  extensionXml(root) {
    let file = root.ele('FILE');
    let httpFileSending = file.ele('MFILESENDING');
    httpFileSending.att('STATUS', this.status);
    httpFileSending.att('NUMBER', this.number);
    httpFileSending.att('CLOUDDISK', 1);
    httpFileSending.att('FILEKEY', this.fileKey);
    httpFileSending.att('ROOMKEY', this.roomKey);
    httpFileSending.att('FILETYPE', this.fileType);
    httpFileSending.att('PATH', this.filePath);
    httpFileSending.att('SENT', this.sent);
    httpFileSending.att('SIZE', this.fileSize);
    httpFileSending.att('NAME', this.fileName);
    httpFileSending.att('RECVER', this.receiver);
    httpFileSending.att('TIME', this.time);
    httpFileSending.att('IP', '');
    httpFileSending.att('PORT', 0);
  }
}

export class HTTPFileFinishedPacket extends Packet {
  constructor(
    sender,
    fileKey,
    number,
    fileSize,
    roomKey,
    fileType,
    filePath,
    fileName,
    receiver,
    serverFilePath,
    time,
    serverFileName,
    type,
    recordTime
  ) {
    super();
    this.sender = sender;
    this.fileKey = fileKey;
    this.number = number;
    this.fileSize = fileSize;
    this.roomKey = roomKey;
    this.fileType = fileType;
    this.filePath = filePath;
    this.fileName = fileName;
    this.receiver = receiver;
    this.serverFilePath = serverFilePath;
    this.time = time;
    this.serverFileName = serverFileName;
    this.type = type;
    this.recordTime = recordTime;
  }
  extensionXml(root) {
    let file = root.ele('FILE');
    let httpFileFinished = file.ele('HTTPFILEFINISH');
    httpFileFinished.att('SENDER', this.sender);
    httpFileFinished.att('FILEKEY', this.fileKey);
    httpFileFinished.att('NUMBER', this.number);
    httpFileFinished.att('SIZE', this.fileSize);
    httpFileFinished.att('ROOMKEY', this.roomKey);
    httpFileFinished.att('FILETYPE', this.fileType);
    httpFileFinished.att('PATH', this.filePath);
    httpFileFinished.att('NAME', this.fileName);
    httpFileFinished.att('RECVER', this.receiver);
    httpFileFinished.att('SERVERFILEPATH', this.serverFilePath);
    httpFileFinished.att('TIME', this.time);
    httpFileFinished.att('SERVERFILENAME', this.serverFileName);
    httpFileFinished.att('TYPE', this.type);
    if (this.recordTime > 0) {
      httpFileFinished.att('RECORDINGTIME', this.recordTime);
    }
  }
}

export class MsgTypingPacket extends Packet {
  constructor(roomKey, userKey, name, status) {
    super();
    this.roomKey = roomKey;
    this.userKey = userKey;
    this.name = name;
    this.status = status;
  }
  extensionXml(root) {
    let chat = root.ele('CHAT');
    let typing = chat.ele('TYPING');
    typing.att('ROOMKEY', this.roomKey);
    typing.att('USERKEY', this.userKey);
    typing.att('USERNAME', this.name);
    typing.att('STATUS', this.status);
  }
}

export class MessagePacket extends Packet {
  constructor(roomKey, clientId, userKey, emoticon, msg) {
    super();
    this.roomKey = roomKey;
    this.userKey = userKey;
    this.clientId = clientId;
    this.emoticon = emoticon;
    this.msg = msg;
  }
  extensionXml(root) {
    let chat = root.ele('CHAT');
    let msgTag = chat.ele('MSG', null, cleanInvalidXMLChars(this.msg));
    msgTag.att('ROOMKEY', this.roomKey);
    msgTag.att('CLIENTKEY', this.clientId);
    msgTag.att('USERKEY', this.userKey);
    msgTag.att('EMOTICON', this.emoticon);
  }
}

export class GetFilesPacket extends Packet {
  constructor(roomKey, lastTime, filterType, count) {
    super();
    this.roomKey = roomKey;
    this.lastTime = lastTime;
    this.filterType = filterType;
    this.count = count;
  }
  extensionXml(root) {
    let chat = root.ele('CHAT');
    let getFiles = chat.ele('GETFILES');
    getFiles.att('ROOMKEY', this.roomKey);
    getFiles.att('FILTER_TYPE', this.filterType);
    getFiles.att('LASTTIME', this.lastTime);
    getFiles.att('COUNT', this.count);
  }
}

export class MessageResultPacket extends Packet {
  constructor(roomKey, numberKey) {
    super();
    this.roomKey = roomKey;
    this.numberKey = numberKey;
  }
  extensionXml(root) {
    let chat = root.ele('CHAT');
    let msgResult = chat.ele('MSGRESULT');
    msgResult.att('ROOMKEY', this.roomKey);
    msgResult.att('NUMBER', this.numberKey);
  }
}

export class RoomTalkPacket extends Packet {
  constructor(roomKey, lastTime, count, clientKey) {
    super();
    this.roomKey = roomKey;
    this.lastTime = lastTime;
    this.count = count;
    this.clientKey = clientKey;
  }

  extensionXml(root) {
    let chat = root.ele('CHAT');
    let getTalk = chat.ele('GETTALK');
    getTalk.att('ROOMKEY', this.roomKey);
    getTalk.att('LASTTIME', this.lastTime);
    getTalk.att('COUNT', this.count);
    getTalk.att('CLIENTKEY', this.clientKey);
    getTalk.att('THUMBNAIL', 0);
  }
}

export class GetThumbnailPacket extends Packet {
  constructor(roomKey, fileList: [], item) {
    super();
    this.roomKey = roomKey;
    this.fileList = fileList;
    this.item = item;
  }

  extensionXml(root) {
    let file = root.ele('FILE');
    if (this.fileList) {
      this.fileList.forEach(item => {
        let getThumbnail = file.ele('GETTHUMBNAIL');
        getThumbnail.att('TIMESTAMP', item.createDate);
        getThumbnail.att('ROOMKEY', this.roomKey);
        getThumbnail.att('NUMBER', item.numberKey);
      });
    } else {
      let getThumbnail = file.ele('GETTHUMBNAIL');
      getThumbnail.att('TIMESTAMP', this.item.createDate);
      getThumbnail.att('ROOMKEY', this.roomKey);
      getThumbnail.att('NUMBER', this.item.numberKey);
    }
  }
}

// SIP
export class SIPMakeCallPacket extends Packet {
  constructor({ fromKey, toKey, clientKey, isVideo }) {
    super();
    this.fromKey = fromKey;
    this.toKey = toKey;
    this.clientKey = clientKey;
    this.isVideo = isVideo;
  }

  extensionXml(root) {
    let alarm = root.ele('ALARM');
    let sip = alarm.ele('SIP');
    sip.att('FROM_USERKEY', this.fromKey);
    sip.att('TO_USERKEY', this.toKey);
    sip.att('CLIENT_KEY', this.clientKey ?? '');
    sip.att('VIDEO', this.isVideo ? 1 : 0);
  }
}

export class SIPResultPacket extends Packet {
  constructor({ fromKey, toKey, sipKey, result, roomKey, numberKey, time }) {
    super();
    this.fromKey = fromKey;
    this.toKey = toKey;
    this.sipKey = sipKey;
    this.result = result;
    this.roomKey = roomKey;
  }

  extensionXml(root) {
    let alarm = root.ele('ALARM');
    let sip = alarm.ele('SIPRESULT');
    sip.att('FROM_USERKEY', this.fromKey);
    sip.att('TO_USERKEY', this.toKey);
    sip.att('SIPKEY', this.sipKey);
    sip.att('ROOMKEY', this.roomKey);
    sip.att('RESULT', this.result);
  }
}

export class SIPHangupPacket extends Packet {
  constructor({ fromKey, toKey, sipKey, roomKey, isVideo }) {
    super();
    this.fromKey = fromKey;
    this.toKey = toKey;
    this.sipKey = sipKey;
    this.roomKey = roomKey;
    this.isVideo = isVideo;
  }

  extensionXml(root) {
    let alarm = root.ele('ALARM');
    let sip = alarm.ele('SIPHANGUP');
    sip.att('FROM_USERKEY', this.fromKey);
    sip.att('TO_USERKEY', this.toKey);
    sip.att('SIPKEY', this.sipKey);
    sip.att('ROOMKEY', this.roomKey);
    sip.att('VIDEO', this.isVideo ? 1 : 0);
  }
}

export class SIPClosedPacket extends Packet {
  constructor({ fromKey, toKey, sipKey, roomKey, isVideo, duration }) {
    super();
    this.fromKey = fromKey;
    this.toKey = toKey;
    this.sipKey = sipKey;
    this.roomKey = roomKey;
    this.isVideo = isVideo;
    this.duration = duration;
  }

  extensionXml(root) {
    let alarm = root.ele('ALARM');
    let sip = alarm.ele('SIPCLOSE');
    sip.att('FROM_USERKEY', this.fromKey);
    sip.att('TO_USERKEY', this.toKey);
    sip.att('SIPKEY', this.sipKey);
    sip.att('ROOMKEY', this.roomKey);
    sip.att('VIDEO', this.isVideo ? 1 : 0);
    sip.att('DURATION', this.duration ?? 1);
  }
}
