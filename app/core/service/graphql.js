import { ApolloClient, InMemoryCache, createHttpLink } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';

class GraphQL {
  constructor() {
    this.token;
    this.refreshToken;
    this.authClient = this.authClientSetup();
    this.translateClient = this.translateClientSetup();
  }

  setToken({ token, refresh_token }) {
    this.token = token;
    if (refresh_token) {
      this.refreshToken = refresh_token;
    }
  }

  authClientSetup() {
    const authLink = setContext((_, { headers }) => {
      return {
        headers: {
          ...headers,
          'Hanbiro-Language': 'vi',
          category: 'translate'
        }
      };
    });
    const httpLink = createHttpLink({
      uri: 'http://translate.hanbiro.com/v1/graphql/token'
    });
    return new ApolloClient({
      link: authLink.concat(httpLink),
      cache: new InMemoryCache()
    });
  }

  translateClientSetup() {
    const authLink = setContext((_, { headers }) => {
      return {
        headers: {
          ...headers,
          Authorization: this.token ? `Bearer ${this.token}` : '',
          'Hanbiro-Language': 'vi',
          category: 'translate'
        }
      };
    });
    const httpLink = createHttpLink({
      uri: 'http://translate.hanbiro.com/v1/graphql/translate'
    });
    return new ApolloClient({
      link: authLink.concat(httpLink),
      cache: new InMemoryCache()
    });
  }
}

const _GraphQL = new GraphQL();

export default _GraphQL;
