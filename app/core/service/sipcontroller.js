class OneOneWindowsObject {
  constructor() {
    this.view = null;
    this.id = null;
    this.timeStamp = 0;
    this.userKey = null;
    this.extraURLs = [];
    this.loaded = false;
    this.cachedRequest = null;
    this.makeCallClientKey = null;
  }

  reset() {
    this.timeStamp = 0;
    this.view = null;
    this.id = null;
    this.userKey = null;
    this.extraURLs = [];
    this.loaded = false;
    this.cachedRequest = null;
  }

  existCall() {
    return this.view != null;
  }

  isLoaded() {
    return this.loaded;
  }

  closeCall() {
    if (this.view) {
      this.view.close();
    }

    this.reset();
  }
}

const videoOneOneWindowsObject = new OneOneWindowsObject();
export default videoOneOneWindowsObject;
