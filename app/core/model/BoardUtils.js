import talkApi from '../service/talk.api.render';
import parser from 'xml-js';
import moment from 'moment';
function timeFromTimeStampSafe(time) {
  if (!time || time == '') {
    return Date.now();
  }
  if (time.length < 12) {
    return time * 1000;
  }
  return time;
}
const rawAuthorizedHeader = extras => {
  const tmp = extras ? extras : {};
  return { ...tmp, Authorization: `api_key=${talkApi.autoLoginKey}` };
};

const rawBodyNewBoardNotify = roomKey => {
  return `<XML><ROOMKEY>${roomKey}</ROOMKEY></XML>`;
};

const rawBodyReadBoardNotify = (roomKey, item) => {
  return `<XML><ROOMKEY>${roomKey}</ROOMKEY><ITEMKEY>${item.itemKey}</ITEMKEY><REGTIME>${item.rawTime}</REGTIME></XML>`;
};

const rawBodyGetBoardList = (roomKey, regTime) => {
  return `<XML><ROOMKEY>${roomKey}</ROOMKEY><REGTIME>${regTime}</REGTIME><BOARD_COUNT>20</BOARD_COUNT><ATTACHMENT_COUNT>5</ATTACHMENT_COUNT></XML>`;
};

const rawBodyGetBoardFileList = (roomKey, regTime, isImage) => {
  return `<XML><ROOMKEY>${roomKey}</ROOMKEY><REGTIME>${regTime}</REGTIME><BIMAGE>${
    isImage ? 1 : 0
  }</BIMAGE></XML>`;
};

const rawBodyGetBoardCommentList = (cn, itemKey, regTime, count) => {
  return `<XML><CN>${cn}</CN><ITEMKEY>${itemKey}</ITEMKEY><REGTIME>${regTime}</REGTIME><COUNT>${count}</COUNT></XML>`;
};

const rawBodyDeleteBoard = (userkey, boardkey) => {
  return `<XML><USERKEY>${userkey}</USERKEY><MAIN_NO>${boardkey}</MAIN_NO></XML>`;
};

const rawBodyNewComment = (userkey, content, boardKey) => {
  const xmlMainNo =
    boardKey == null || boardKey == '' ? '' : `<MAIN_NO>${boardKey}</MAIN_NO>`;
  console.log(
    'rawBodyNewComment: ',
    `<XML><USERKEY>${userkey}</USERKEY><CONTENT>${content}</CONTENT>${xmlMainNo}</XML>`
  );
  return `<XML><USERKEY>${userkey}</USERKEY><CONTENT>${content}</CONTENT>${xmlMainNo}</XML>`;
};

const boardCommentListFromXML = xml => {
  const rawJsonData = parser.xml2json(xml, { compact: false });
  const jsonData = JSON.parse(rawJsonData);
  const childXML = jsonData.elements[0].elements;
  let boardListCommentResult = {
    boardData: {},
    boardFileList: [],
    boardCommentList: []
  };
  const boardListResult = [];

  childXML.forEach(element => {
    if (element.name == 'BOARDDATA') {
      const childBoardList = element.attributes;
      if (childBoardList) {
        let boardData = {};
        boardData.userKey = childBoardList['USERKEY'];
        boardData.notification = childBoardList['KIND'] == 1 ? true : false;
        boardData.rawTime = childBoardList['REGTIME'];
        boardData.time = timeFromTimeStampSafe(boardData.rawTime);
        boardData.commentCount = childBoardList['COMMENTCOUNT'] ?? 0;
        boardData.content =
          element.elements && element.elements?.length > 0
            ? element.elements[0].text
            : '';
        boardData.type =
          element.elements && element.elements?.length > 0
            ? element.elements[0].type
            : '';

        boardData.dateTimeString = moment(boardData.time).format(
          talkApi.dateTimeFormat()
        );
        boardListCommentResult.boardData = boardData;
      }
    } else if (element.name == 'BOARDFILELIST') {
      const childBoardFileList = element.elements;

      let boardFileList = [];
      childBoardFileList.forEach(fileItem => {
        const fileItemAttribute = fileItem.attributes;
        let commentFileObj = {};
        commentFileObj.fileKey = fileItemAttribute['FILEKEY'];
        commentFileObj.fileName = fileItemAttribute['FILENAME'] ?? '';
        commentFileObj.fileSize = fileItemAttribute['FILESIZE'] ?? 0;
        commentFileObj.bImage = fileItemAttribute['BIMAGE'] ?? '';

        commentFileObj.content =
          fileItem.elements && fileItem.elements?.length > 0
            ? fileItem.elements[0].text
            : '';
        commentFileObj.type =
          fileItem.elements && fileItem.elements?.length > 0
            ? fileItem.elements[0].type
            : '';

        boardFileList.push(commentFileObj);
      });

      boardListCommentResult.boardFileList = boardFileList;
    } else if (element.name == 'BOARDCOMMENTLIST') {
      const childBoardCommentList = element.elements;

      let boardCommentList = [];

      childBoardCommentList.forEach(commentItem => {
        const commentAttributes = commentItem.attributes;

        let commentObj = {};

        commentObj.userKey = commentAttributes['USERKEY'];
        commentObj.notification = commentAttributes['KIND'] == 1 ? true : false;
        commentObj.rawTime = commentAttributes['REGTIME'];
        commentObj.time = timeFromTimeStampSafe(commentObj.rawTime);
        commentObj.commentKey = commentAttributes['COMMENTKEY'];
        commentObj.content =
          commentItem.elements && commentItem.elements?.length > 0
            ? commentItem.elements[0].text
            : '';
        commentObj.type =
          commentItem.elements && commentItem.elements?.length > 0
            ? commentItem.elements[0].type
            : '';
        commentObj.dateTimeString = moment(commentObj.time).format(
          talkApi.dateTimeFormat()
        );
        boardCommentList.splice(0, 0, commentObj);
      });
      boardListCommentResult.boardCommentList = boardCommentList;
    }
  });
  console.log('boardListCommentResult: ', boardListCommentResult);
  return boardListCommentResult;
};

const boardCommentProcessBoolXML = xml => {
  const rawJsonData = parser.xml2json(xml, { compact: false });
  const jsonData = JSON.parse(rawJsonData);
  const childXML = jsonData.elements[0].elements;

  let result = false;
  if (childXML && childXML.length > 0) {
    const attribute = childXML[0].attributes;
    const res = attribute['RESULT'] ?? 0;
    result = res == 1;
  }
  return result;
};

const boardListFromXML = xml => {
  const rawJsonData = parser.xml2json(xml, { compact: false });
  const jsonData = JSON.parse(rawJsonData);
  const childXML = jsonData.elements[0].elements;
  const boardListResult = [];
  let firstTime = 0;

  childXML.forEach(element => {
    if (element.name == 'BOARDLIST') {
      const childBoardList = element.elements;
      if (childBoardList) {
        childBoardList.forEach(board => {
          if (board.name == 'ITEM') {
            let boardItem = { attachments: [] };
            const attrBoard = board.attributes;
            if (attrBoard) {
              boardItem.itemKey = attrBoard['ITEMKEY'];
              boardItem.userKey = attrBoard['USERKEY'];
              boardItem.notification = attrBoard['KIND'] == 1 ? true : false;
              boardItem.rawTime = attrBoard['REGTIME'];
              boardItem.time = timeFromTimeStampSafe(boardItem.rawTime);
              boardItem.cn = attrBoard['CN'];
              boardItem.commentCount = attrBoard['COMMENTCOUNT'] ?? 0;
              boardItem.dateString = moment(boardItem.time).format(
                talkApi.dateTimeFormat()
              );
              boardItem.dateTimeString = moment(boardItem.time).format(
                talkApi.dateTimeFormatWithTime()
              );
              if (firstTime == 0 || firstTime > boardItem.rawTime) {
                firstTime = boardItem.rawTime;
              }
            }
            const childContent = board.elements;
            if (childContent) {
              childContent.forEach(sub => {
                if (sub.name == 'CONTENT') {
                  boardItem.bodyMessage = sub.elements[0].text ?? '';
                } else if (sub.name == 'ATTACHMENTS') {
                  const childAttachments = sub.elements;
                  if (childAttachments) {
                    childAttachments.forEach(attachment => {
                      if (attachment.name == 'ITEM') {
                        const attrAttachment = attachment.attributes;
                        if (attrAttachment) {
                          const attObj = {
                            fileKey: attrAttachment['FILEKEY'] ?? '',
                            fileName: attrAttachment['FILENAME'] ?? '',
                            fileSize: attrAttachment['FILESIZE'] ?? 0,
                            bigImage:
                              attrAttachment['BIMAGE'] == 1 ? true : false
                          };
                          boardItem.attachments.push(attObj);
                        }
                      }
                    });
                  }
                }
              });
            }
            boardListResult.push(boardItem);
          }
        });
      }
    }
  });
  return { boardListResult, firstTime };
};

const boardFileListFromXML = xml => {
  const rawJsonData = parser.xml2json(xml, { compact: false });
  const jsonData = JSON.parse(rawJsonData);
  const childXML = jsonData.elements[0].elements;
  const boardFileListResult = [];
  let firstTime = 0;

  childXML.forEach(element => {
    if (element.name == 'BOARDFILELIST') {
      const childBoardFileList = element.elements;
      if (childBoardFileList) {
        childBoardFileList.forEach(board => {
          if (board.name == 'ITEM') {
            let boardItem = {};
            const attrBoard = board.attributes;
            if (attrBoard) {
              boardItem.fileKey = attrBoard['FILEKEY'];
              boardItem.fileName = attrBoard['FILENAME'];
              boardItem.fileSize = attrBoard['FILESIZE'] ?? 0;
              boardItem.bigImage = attrBoard['BIMAGE'] == 1 ? true : false;
              if (board.elements) {
                boardItem.thumbnail = board.elements[0].text;
              }
            }
            boardFileListResult.push(boardItem);
          }
        });
      }
    }
  });
  return { boardFileListResult, firstTime };
};

const boardListFromNotificationObject = (list, message) => {
  // TODO if list len > 1 need make priority to show
  if (!message) {
    return list;
  }
  return [
    {
      itemKey: message.clientKey,
      userKey: message.userKey,
      notification: message.msgStyle == 1 ? true : false,
      rawTime: message.rawCreateDate,
      time: message.createDate,
      bodyMessage: message.msgBody
    }
  ];
};

const boardItemFromXml = xml => {
  const rawJsonData = parser.xml2json(xml, { compact: false });
  const jsonData = JSON.parse(rawJsonData);
  const childXML = jsonData.elements[0].elements;
  const items = [];
  childXML.forEach(element => {
    if (element.name == 'NEWBOARD') {
      const attrs = element.attributes;
      let item;
      if (attrs) {
        item = {};
        item.itemKey = attrs['ITEMKEY'];
        if (!item.itemKey) {
          return;
        }
        item.userKey = attrs['USERKEY'];
        item.notification = attrs['KIND'] == 1 ? true : false;
        item.rawTime = attrs['REGTIME'];
        item.time = timeFromTimeStampSafe(item.rawTime);
        items.push(item);
      }
      if (item && element.elements) {
        const t = element.elements[0].text ?? '';
        item.bodyMessage = t.replace(/\r?\n|\r/g, '');
      }
    }
  });

  let rItem = null;
  for (let i = items.length - 1; i >= 0; i--) {
    const item = items[i];
    if (KEEP_READ_BOARD_VALUE[item.itemKey] == true) {
      continue;
    }
    if (!rItem || rItem.time < item.time) {
      rItem = item;
    }
  }

  return rItem ? [rItem] : [];
};

var KEEP_READ_BOARD_VALUE = {};

export default {
  KEEP_READ_BOARD_VALUE,
  rawAuthorizedHeader,
  rawBodyNewBoardNotify,
  rawBodyReadBoardNotify,
  rawBodyGetBoardList,
  rawBodyGetBoardFileList,
  rawBodyGetBoardCommentList,
  rawBodyNewComment,
  boardItemFromXml,
  boardListFromXML,
  boardFileListFromXML,
  boardCommentListFromXML,
  boardListFromNotificationObject,
  boardCommentProcessBoolXML,
  rawBodyDeleteBoard
};
