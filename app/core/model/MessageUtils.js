import {
  MessageServerStatus,
  MessageType,
  MessageThirdParty,
  MessageFileStatus
} from '../service/talk-constants';
const MB_20 = 20 * 1024 * 1024;
export function fileExtension(fileName) {
  if (fileName == null || fileName == '') {
    return '';
  }
  let posDot = fileName.lastIndexOf('.');
  if (posDot < 0) {
    return '';
  }

  return fileName.substring(posDot + 1).toLowerCase();
}
export const messsageUtils = {
  isReaded(message) {
    return (
      message.msgMarkAsReaded == MessageServerStatus.MARK_SERVER_READ ||
      message.msgMarkAsReaded == MessageServerStatus.MARK_READ
    );
  },
  isServerReaded(message) {
    return message.msgMarkAsReaded == MessageServerStatus.MARK_SERVER_READ;
  },
  isPhoto(message) {
    return (
      message.msgType == MessageType.MESSAGE_TYPE_PHOTO &&
      fileExtension(message.msgdtFileName) != 'bmp'
    );
  },
  isPhotoLarger20MB(message) {
    return this.isPhoto(message) && message.msgdtFileSize > MB_20;
  },
  isPhotoSmaller20MB(message) {
    return this.isPhoto(message) && message.msgdtFileSize <= MB_20;
  },
  isGIF(message) {
    return (
      this.isPhoto(message) && fileExtension(message.msgdtFileName) == 'gif'
    );
  },
  isText(message) {
    return message.msgType == MessageType.MESSAGE_TYPE_TEXT;
  },
  isVideo(message) {
    return message.msgType == MessageType.MESSAGE_TYPE_VIDEO;
  },
  isFileOther(message) {
    return (
      message.msgType == MessageType.MESSAGE_TYPE_FILE ||
      fileExtension(message.msgdtFileName) == 'bmp'
    );
  },
  isRecordFile(message) {
    return message.msgType == MessageType.MESSAGE_TYPE_RECORD;
  },
  isBoardText(message) {
    return message.msgType == MessageType.MESSAGE_TYPE_BOARD_INFO;
  },
  isLeftRoom(message) {
    return message.msgType == MessageType.MESSAGE_TYPE_OUT;
  },
  isEnterRoom(message) {
    return message.msgType == MessageType.MESSAGE_TYPE_INVITE;
  },
  isRoomTitleChanged(message) {
    return message.msgType == MessageType.MESSAGE_TYPE_ROOM_ALIAS;
  },
  isVoiceCallMessage(message) {
    return message.msgType == MessageType.MESSAGE_TYPE_CALL_LOG;
  },
  isGroupCallMessage(message) {
    return message.msgType == MessageType.MESSAGE_TYPE_GROUP_CALL_LOG;
  },
  isEmoticonIcon(message) {
    return message.msgEmotion != null && message.msgEmotion != '';
  },
  isMessageDeleted(message) {
    return message.msgIsDeleted && message.msgIsDeleted > 0;
  },
  isMessageBookmarked(message) {
    return message.msgIsBookmark && message.msgIsBookmark > 0;
  },
  isFileAttachment(message) {
    return (
      this.isPhoto(message) ||
      this.isRecordFile(message) ||
      this.isVideo(message) ||
      this.isFileOther(message)
    );
  },
  isClouddiskFile(message) {
    return message.msgThirdPartyID == MessageThirdParty.MESSAGE_CLOUDDISK_ID;
  },
  isTCPFileAttachment(message) {
    // message.msgdtHttpServerFilePath == null ||
    //     message.msgdtHttpServerFilePath == '' ||
    //     message.msgdtHttpServerFileName == null ||
    //     message.msgdtHttpServerFileName == '' ||
    return (
      !this.isClouddiskFile(message) &&
      this.isFileAttachment(message) &&
      (message.msgThirdPartyID == MessageThirdParty.MESSAGE_MESSENGER_ID ||
        message.msgdtFileKey == '' ||
        message.msgdtFileKey == null)
    );
  },
  isSupportUnreadCheck(message) {
    return (
      message.msgType != MessageType.MESSAGE_TYPE_OUT &&
      message.msgType != MessageType.MESSAGE_TYPE_INVITE &&
      message.msgType != MessageType.MESSAGE_TYPE_CALL_LOG &&
      message.msgType != MessageType.MESSAGE_TYPE_GROUP_CALL_LOG &&
      message.msgType != MessageType.MESSAGE_TYPE_ROOM_ALIAS
    );
  }
};
