import TextUtils from '../utils/text.util';
import Utils from '../utils/utils';
import {
  StatusMode,
  LOGIN_TYPE_WINDOWS,
  LOGIN_TYPE_WEB,
  LOGIN_TYPE_MAC,
  LOGIN_TYPE_MOBILE,
  LOGIN_TYPE_DUAL,
  GROUP_TYPE_USER
} from '../service/talk-constants';

export const KEY_MY_ROOT = 'KEY_MY_ROOT_ABCXYZ_NHANNT';
export const TALK_KEY_FAVORITE = 'favorite';
export const TALK_KEY_COMPANY = 'company';

export const contactUtils = {
  //DATA
  //   userId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  //   userKey VARCHAR,
  //   userParentKey VARCHAR,
  //   userEmail VARCHAR,
  //   userChosunText VARCHAR,
  //   userLocalPhone VARCHAR,
  //   userMobilePhone VARCHAR,
  //   userAlias VARCHAR,
  //   userFullName VARCHAR,
  //   userNickName VARCHAR,
  //   userShortName VARCHAR,
  //   userPosition VARCHAR,
  //   userShortPosition VARCHAR,
  //   userSex INTEGER,
  //   userStatus INTEGER,
  //   userOrder INTEGER,
  //   userPhotoTime INTEGER,
  //   userLoginType INTEGER,
  //   userSIPPhone VARCHAR,
  //   userLanguage VARCHAR,
  //   userHidden INTEGER

  isAdmin(contact) {
    return contact.userKey.endsWith('A');
  },

  isHiddenUser(contact) {
    return contact.userHidden > 0;
  },

  isOnlineUser(contact) {
    return (
      contact.userStatus != StatusMode.logout &&
      contact.userStatus != StatusMode.offline
    );
  },
  isContactObj(contact) {
    return contact.userKey ?? false;
  },
  isMan(contact) {
    return contact.userSex != 2;
  },
  getDisplayName(contact, language, loginName, userKey) {
    let displayName = '';
    if (
      TextUtils.isEmpty(contact.userLanguage) ||
      contact.userLanguage.toLowerCase() == language
    ) {
      displayName = TextUtils.isEmpty(contact.userAlias)
        ? contact.userShortName
        : contact.userAlias;
    } else {
      displayName = TextUtils.isEmpty(contact.userShortName)
        ? contact.userAlias
        : contact.userShortName;
    }
    displayName = TextUtils.isEmpty(displayName)
      ? contact.userFullName
      : displayName;

    if (TextUtils.isEmpty(displayName)) {
      if (contact.userKey == userKey) {
        //logged account
        displayName = loginName;
      }
    }
    return displayName ?? '_';
  },
  getDisplayDuty(contact, language) {
    let position = '';
    if (
      TextUtils.isEmpty(contact.userLanguage) ||
      contact.userLanguage.toLowerCase() == language
    ) {
      position = TextUtils.isEmpty(contact.userPosition)
        ? contact.userShortPosition
        : contact.userPosition;
    } else {
      position = TextUtils.isEmpty(contact.userShortPosition)
        ? contact.userPosition
        : contact.userShortPosition;
    }
    return position;
  },
  isMobileLogin(contact) {
    let userLoginType = contact.userLoginType;
    return userLoginType == LOGIN_TYPE_MOBILE && this.isOnlineUser(contact);
  },
  isPCLogin(contact) {
    let userLoginType = contact.userLoginType;
    return (
      (userLoginType == LOGIN_TYPE_WEB ||
        userLoginType == LOGIN_TYPE_WINDOWS ||
        userLoginType == LOGIN_TYPE_MAC) &&
      this.isOnlineUser(contact)
    );
  },
  isDualLogin(contact) {
    return contact.userLoginType == LOGIN_TYPE_DUAL;
  },
  hasContainSearchText(contact, searchText) {
    if (TextUtils.isEmpty(searchText)) {
      return true;
    }
    let conditionName = contact.userAlias
      .toLowerCase()
      .includes(searchText.toLowerCase());
    let conditionEmail = contact.userEmail
      .toLowerCase()
      .includes(searchText.toLowerCase());
    let conditionNickName =
      TextUtils.isNotEmpty(contact.userNickName) &&
      contact.userNickName.toLowerCase().includes(searchText.toLowerCase());
    let conditionPhone =
      TextUtils.isNotEmpty(contact.userLocalPhone) &&
      contact.userLocalPhone.toLowerCase().includes(searchText.toLowerCase());
    let conditionMobilePhone =
      TextUtils.isNotEmpty(contact.userMobilePhone) &&
      contact.userMobilePhone.toLowerCase().includes(searchText.toLowerCase());
    let conditionLongName =
      TextUtils.isNotEmpty(contact.userFullName) &&
      contact.userFullName.toLowerCase().includes(searchText.toLowerCase());
    return (
      conditionName ||
      conditionEmail ||
      conditionNickName ||
      conditionPhone ||
      conditionMobilePhone ||
      conditionLongName
    );
  }
};

export const groupUtils = {
  // groupId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  //   groupPKey VARCHAR,
  //   groupKey VARCHAR,
  //   groupAlias VARCHAR,
  //   groupShortName VARCHAR,
  //   groupFullName VARCHAR,
  //   groupType INTEGER,
  //   groupHeader VARCHAR,
  //   groupLanguage VARCHAR

  isGroupCustomUser(group) {
    return group.groupType == GROUP_TYPE_USER;
  },
  isGroupObj(group) {
    return group.groupKey ?? false;
  },
  isRootFavorite(group) {
    return group.groupKey == TALK_KEY_FAVORITE;
  },
  getDisplayName(group, language) {
    let displayName = '';
    if (
      TextUtils.isEmpty(group.groupLanguage) ||
      group.groupLanguage.toLowerCase() == language
    ) {
      displayName = TextUtils.isEmpty(group.groupAlias)
        ? group.groupShortName
        : group.groupAlias;
    } else {
      displayName = TextUtils.isEmpty(group.groupShortName)
        ? group.groupAlias
        : group.groupShortName;
    }
    displayName = TextUtils.isEmpty(displayName)
      ? group.groupFullName
      : displayName;
    return displayName;
  },
  hasContainSearchText(group, searchText) {
    if (TextUtils.isEmpty(searchText)) {
      return false;
    }

    let conditionAlias =
      TextUtils.isNotEmpty(group.groupAlias) &&
      group.groupAlias.toLowerCase().includes(searchText.toLowerCase());

    let conditionShortName =
      TextUtils.isNotEmpty(group.groupShortName) &&
      group.groupShortName.toLowerCase().includes(searchText.toLowerCase());

    let conditionFullName =
      TextUtils.isNotEmpty(group.groupFullName) &&
      group.groupFullName.toLowerCase().includes(searchText.toLowerCase());

    return conditionAlias || conditionFullName || conditionShortName;
  },
  hasChildContainSearchText(group, searchText) {
    let children = group.childrenList;
    for (let index = 0; index < children.length; index++) {
      const element = children[index];
      if (this.isGroupObj(element)) {
        if (
          element.groupAlias.toLowerCase().includes(searchText.toLowerCase())
        ) {
          return true;
        }
        if (this.hasChildContainSearchText(element, searchText)) {
          return true;
        }
      } else {
        if (contactUtils.isAdmin(element)) {
          continue;
        }
        if (contactUtils.hasContainSearchText(element, searchText)) {
          return true;
        }
      }
    }
    return false;
  },

  getUserInGroup(group) {
    let users = [];
    this.getUserInGroupRecursive(group, users);
    return users;
  },
  getUserInGroupRecursive(group, users) {
    let children = group.childrenList;
    for (let index = 0; index < children.length; index++) {
      const element = children[index];
      if (this.isGroupObj(element)) {
        this.getUserInGroupRecursive(element, users);
      } else {
        if (contactUtils.isAdmin(element)) {
          continue;
        }
        if (contactUtils.isHiddenUser(element)) {
          continue;
        }
        const find = users.find(obj => obj.userKey == element.userKey);
        if (!find) {
          users.push(element);
        }
      }
    }
  }
};

export const roomUtils = {
  isFireRoom(room) {
    return room.rRoomFireType > 0;
  },

  isMyRoom(room) {
    return room.rRoomKey.startsWith('MY');
  },

  stringFromRoomJidList(room, listUser: {}, loggedUser: {}) {},
  getDefaultDisplayName(room, listUser: {} = null, loggedUser: {} = null) {
    if (this.isFireRoom(room)) {
      return room.rRoomNameLabelPublic;
    }

    if (this.isMyRoom(room)) {
      return 'My Room';
    }

    let roomName = room.rRoomNameLabelPrivate;
    if (TextUtils.isEmpty(roomName)) {
      roomName = room.rRoomNameLabelPublic;
      if (TextUtils.isEmpty(roomName)) {
        roomName = room.rRoomNameLabelPrevAlias;
        if (TextUtils.isEmpty(roomName)) {
          if (listUser != null) {
            roomName = this.stringFromRoomJidList(room, listUser, loggedUser);
          }
        }
      }
    }
    return roomName;
  },
  isRoomHasSearchText(room, searchText) {
    return (
      (TextUtils.isNotEmpty(room.rRoomNameLabelPrevAlias) &&
        room.rRoomNameLabelPrevAlias
          .toLowerCase()
          .includes(searchText.toLowerCase())) ||
      (TextUtils.isNotEmpty(room.rRoomNameLabelPrivate) &&
        room.rRoomNameLabelPrivate
          .toLowerCase()
          .includes(searchText.toLowerCase())) ||
      (TextUtils.isNotEmpty(room.rRoomNameLabelPublic) &&
        room.rRoomNameLabelPublic
          .toLowerCase()
          .includes(searchText.toLowerCase())) ||
      (TextUtils.isNotEmpty(room.rRoomTag) &&
        room.rRoomTag.toLowerCase().includes(searchText.toLowerCase()))
    );
  },
  isRoomHasChosunSearchText(room, searchText) {
    if (TextUtils.isEmpty(searchText)) {
      return false;
    }

    const chosungPreviousAlias = Utils.decomposeSylLabels(
      room.rRoomNameLabelPrevAlias
    );
    const chosungPrivate = Utils.decomposeSylLabels(room.rRoomNameLabelPrivate);
    const chosungPublic = Utils.decomposeSylLabels(room.rRoomNameLabelPublic);
    const chosungRoomTag = Utils.decomposeSylLabels(room.rRoomTag);

    return (
      chosungPreviousAlias.toLowerCase().includes(searchText.toLowerCase()) ||
      chosungPrivate.toLowerCase().includes(searchText.toLowerCase()) ||
      chosungPublic.toLowerCase().includes(searchText.toLowerCase()) ||
      chosungRoomTag.toLowerCase().includes(searchText.toLowerCase())
    );
  },
  isRoomDetailHasSearchText(room, searchText, lastUserKeyOnlyResult) {
    let found = false;
    if (TextUtils.isNotEmpty(room.displayName)) {
      found = room.displayName.toLowerCase().includes(searchText.toLowerCase());
    }
    if (found) {
      return true;
    }

    if (lastUserKeyOnlyResult && lastUserKeyOnlyResult.length > 0) {
      if (room.roomDetailds) {
        for (let i = 0; i < room.roomDetailds.length; i++) {
          const detail = room.roomDetailds[i];
          if (lastUserKeyOnlyResult.includes(detail.rdtUKey)) {
            return true;
          }
        }
      }
    }

    return false;
  }
};
