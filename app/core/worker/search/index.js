import PromiseWorker from 'promise-worker';
import Worker from './search.worker.js';
const worker = new Worker();
const promiseWorker = new PromiseWorker(worker);

const getSearchResult = data =>
  promiseWorker.postMessage({
    type: 'getSearchResult',
    data
  });

const getSearchResultV2 = data =>
  promiseWorker.postMessage({
    type: 'getSearchResultV2',
    data
  });

const getSearchResultOrgOnly = data =>
  promiseWorker.postMessage({
    type: 'getSearchResultOrgOnly',
    data
  });

const getExtraUserInfoFromList = data =>
  promiseWorker.postMessage({
    type: 'getExtraUserInfo',
    data
  });

export default {
  getSearchResult,
  getSearchResultV2,
  getSearchResultOrgOnly,
  getExtraUserInfoFromList
};
