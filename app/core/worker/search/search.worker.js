import registerPromiseWorker from 'promise-worker/register';
import * as searchLoader from '../../loader/search-loader';
registerPromiseWorker(message => {
  if (message.type === 'getSearchResult') {
    let data = message.data;

    return searchLoader.buildSearchResult(
      data.org,
      data.rooms,
      data.group_cached,
      data.searchText,
      data.searchChosun,
      data.loggedUser
    );
  }

  if (message.type === 'getSearchResultV2') {
    let data = message.data;
    return searchLoader.buildSearchResultV2(data);
  }

  if (message.type === 'getExtraUserInfo') {
    let data = message.data;
    return searchLoader.buildExtraUserInfo(data);
  }

  if (message.type === 'getSearchResultOrgOnly') {
    let data = message.data;
    return searchLoader.buildSearchResultOrgOnly(
      data.org,
      data.group_cached,
      data.searchText,
      data.loggedUser
    );
  }
});
