import registerPromiseWorker from 'promise-worker/register';
import * as roomListLoader from '../../loader/roomlist-loader';
registerPromiseWorker(message => {
  if (message.type === 'getRoomList') {
    let data = message.data;
    return roomListLoader.buildRoomList(
      data.listRoom,
      data.listUser,
      data.loggedUser,
      data.curRoomKey,
      data.window_visible
    );
  }
});
