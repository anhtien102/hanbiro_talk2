import PromiseWorker from 'promise-worker';
import Worker from './roomlist.worker.js';
const worker = new Worker();
const promiseWorker = new PromiseWorker(worker);

const getRoomList = data =>
  promiseWorker.postMessage({
    type: 'getRoomList',
    data
  });
export default { getRoomList };
