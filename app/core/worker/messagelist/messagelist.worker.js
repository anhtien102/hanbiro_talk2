import registerPromiseWorker from 'promise-worker/register';
import * as messageListLoader from '../../loader/messagelist-loader';
registerPromiseWorker(message => {
  if (message.type === 'getMessageList') {
    let data = message.data;

    return messageListLoader.buildMessageList(
      data.roomID,
      data.roomKey,
      data.firstTimeStamp,
      data.listMessage,
      data.loggedUser,
      data.myInfo
    );
  }

  if (message.type == 'getRoomDetailList') {
    let data = message.data;
    return messageListLoader.buildRoomDetailList(
      data.roomDetails,
      data.userCached,
      data.loggedUser
    );
  }

  if (message.type == 'getFileListWorker') {
    let data = message.data;
    return messageListLoader.buildFileList(data);
  }
});
