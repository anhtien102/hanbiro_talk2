import PromiseWorker from 'promise-worker';
import Worker from './messagelist.worker.js';
const worker = new Worker();
const promiseWorker = new PromiseWorker(worker);

const getMessageList = data =>
  promiseWorker.postMessage({
    type: 'getMessageList',
    data
  });

const getRoomDetailList = data =>
  promiseWorker.postMessage({
    type: 'getRoomDetailList',
    data
  });

const getFileListWorker = data =>
  promiseWorker.postMessage({
    type: 'getFileListWorker',
    data
  });

export { getMessageList, getRoomDetailList, getFileListWorker };
