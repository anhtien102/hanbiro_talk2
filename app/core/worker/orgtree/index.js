import PromiseWorker from 'promise-worker';
import Worker from './org.worker.js';
const worker = new Worker();
const promiseWorker = new PromiseWorker(worker);

const getTreeOrg = data =>
  promiseWorker.postMessage({
    type: 'getTreeOrg',
    data
  });
export default { getTreeOrg };
