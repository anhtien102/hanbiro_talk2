import registerPromiseWorker from 'promise-worker/register';
import * as orgLoader from '../../loader/org-loader';
registerPromiseWorker(message => {
  if (message.type === 'getTreeOrg') {
    let data = message.data;
    return orgLoader.buildAllOrgTree(
      data.listUser,
      data.listGroup,
      data.listUserFav,
      data.listGroupFav,
      data.loggedUser
    );
  }
});
