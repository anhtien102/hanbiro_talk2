const funcs = {
  isEmpty(str) {
    return str == null || str == '';
  },
  isNotEmpty(str) {
    return str != null && str != '';
  }
};

export default funcs;
