import { contactUtils, groupUtils, KEY_MY_ROOT } from '../model/OrgUtils';

import { IS_CHECK_BRANCH_DEPT_CJ4DX } from '../../configs/constant';

const IS_CHECK_BRANCH_DEPT = IS_CHECK_BRANCH_DEPT_CJ4DX;

const funcs = {
  isBranchDeptShowOnly(myDeptList, branchDept) {
    if (!IS_CHECK_BRANCH_DEPT) {
      return false;
    }

    if (branchDept && myDeptList) {
      for (var i = 0; i < myDeptList.length; i++) {
        var myKey = myDeptList[i];
        if (myKey == branchDept) {
          return true;
        }
      }
    }
    return false;
  },
  storeBranchUserOnly(
    manageDept,
    branchDept,
    myDeptList,
    listContact: [],
    listGroup: []
  ) {
    const hasBranchInMyKey = this.isBranchDeptShowOnly(myDeptList, branchDept);
    let newListGroup = [];
    let newListContact = [];
    if (hasBranchInMyKey) {
      let mapCached = {};
      let hashTable = {};
      let listRoot = [];

      let favContactTemp = [];
      //build for branch only
      listContact.forEach(element => {
        if (contactUtils.isContactObj(element)) {
          if (element.favorite) {
            favContactTemp.push(element);
          } else {
            foundContact(listRoot, hashTable, element, mapCached);
          }
        }
      });

      listGroup.forEach(element => {
        if (groupUtils.isGroupObj(element)) {
          if (element.favorite) {
            newListGroup.push(element);
          } else {
            foundGroup(listRoot, hashTable, element, mapCached);
          }
        }
      });

      let managerGroup = findGroupByGroupKey(listRoot, manageDept);
      if (managerGroup != null) {
        managerGroup.groupParentKey = '';
        addAllChildrenToList(managerGroup, newListGroup, newListContact);
      }

      if (myDeptList != null) {
        myDeptList.forEach(myKey => {
          let myGroup = findGroupByGroupKey(listRoot, myKey);
          if (myGroup != null && myGroup.groupKey != branchDept) {
            addAllChildrenToList(myGroup, newListGroup, newListContact);
            myGroup.groupParentKey = '';
          }
        });
      }

      favContactTemp.forEach(element => {
        if (newListContact.includes(element)) {
          newListContact.splice(0, 0, element); //insert at index 0
        }
      });

      listContact.forEach(element => {
        if (contactUtils.isContactObj(element)) {
          element.hiddenUser = !newListContact.includes(element);
        }
      });
    }

    return {
      hasBranchInMyKey: hasBranchInMyKey,
      newListContact: newListContact,
      newListGroup: newListGroup
    };
  }
};

function addAllChildrenToList(groupContact, listGroup, listContact) {
  listGroup.push(groupContact);
  if (groupContact.childrenList != null) {
    groupContact.childrenList.forEach(element => {
      if (groupUtils.isGroupObj(element)) {
        addAllChildrenToList(element, listGroup, listContact);
      } else {
        listContact.push(element);
      }
    });
  }
}

function findGroupByGroupKey(listRoot, groupKey) {
  for (let index = 0; index < listRoot.length; index++) {
    const element = listRoot[index];
    if (groupUtils.isGroupObj(element)) {
      if (element.groupKey == groupKey) {
        return element;
      }

      let result = findGroupByGroupKey(element.childrenList, groupKey);
      if (result != null) {
        return result;
      }
    }
  }
  return null;
}

function hasNotParent(contact) {
  return (
    contact.parentKey == '' ||
    contact.parentKey == 'rosters' ||
    parseInt(contact.parentKey) < 0
  );
}

function checkContactExistInRoot(mapCachedValue, parentKey, contact) {
  const parentMap = mapCachedValue[parentKey];
  if (parentMap != null) {
    return parentMap[contact.userKey] != null;
  }
  return false;
}

function putContactToMapCached(mapCachedValue, parentKey, contact) {
  let parentMap = mapCachedValue[parentKey];
  let key = contact.userKey;
  if (parentMap == null) {
    mapCachedValue[parentKey] = { key: true };
  } else {
    parentMap[key] = true;
  }
}

function foundContact(listRoot, hashTable, contact, mapCached) {
  let isNoParent = hasNotParent(contact);
  if (isNoParent) {
    if (!checkContactExistInRoot(mapCached, KEY_MY_ROOT, contact)) {
      listRoot.push(contact);
      putContactToMapCached(mapCached, KEY_MY_ROOT, contact);
    }
  } else {
    let childList = hashTable[contact.parentKey];
    if (childList) {
      if (!checkContactExistInRoot(mapCached, contact.parentKey, contact)) {
        childList.push(contact);
        putContactToMapCached(mapCached, contact.parentKey, contact);
      }
    } else {
      let key = contact.parentKey;
      hashTable[key] = [contact];
    }
  }
}

function hasNotParentGroup(group) {
  return (
    group.groupParentKey == '' ||
    group.groupParentKey == '0' ||
    group.groupParentKey == '00000000' ||
    group.groupParentKey == '0000' ||
    parseInt(group.groupParentKey) < 0
  );
}

function checkGroupExistInRoot(mapCachedValue, parentKey, groupContact) {
  const parentMap = mapCachedValue[parentKey];
  if (parentMap != null) {
    let key = groupContact.groupKey + 'G';
    return parentMap[key] != null;
  }
  return false;
}

function putGroupToMapCached(mapCachedValue, parentKey, groupContact) {
  let parentMap = mapCachedValue[parentKey];
  let key = groupContact.groupKey + 'G';
  if (parentMap == null) {
    mapCachedValue[parentKey] = { key: true };
  } else {
    parentMap[key] = true;
  }
}

function foundGroup(listRoot, hashTable, groupContact, mapCached) {
  foundGroupEx(listRoot, hashTable, groupContact, false, mapCached);
}

function foundGroupEx(
  listRoot,
  hashTable,
  groupContact,
  needCheckRoot,
  mapCached
) {
  let isNoParent = hasNotParentGroup(groupContact);
  if (isNoParent) {
    if (!checkGroupExistInRoot(mapCached, KEY_MY_ROOT, groupContact)) {
      listRoot.push(groupContact);
      putGroupToMapCached(mapCached, KEY_MY_ROOT, groupContact);
    }
  } else {
    let parentKey = groupContact.groupParentKey;
    let childList = hashTable[parentKey];
    if (childList == null) {
      childList = [];
      if (needCheckRoot) {
        if (!checkGroupExistInRoot(mapCached, KEY_MY_ROOT, groupContact)) {
          childList.push(groupContact);
        }
      } else {
        childList.push(groupContact);
      }
      hashTable[parentKey] = childList;
    } else {
      if (!checkGroupExistInRoot(mapCached, parentKey, groupContact)) {
        if (needCheckRoot) {
          if (!checkGroupExistInRoot(mapCached, KEY_MY_ROOT, groupContact)) {
            childList.push(groupContact);
            putGroupToMapCached(mapCached, parentKey, groupContact);
          }
        } else {
          childList.push(groupContact);
          putGroupToMapCached(mapCached, parentKey, groupContact);
        }
      }
    }
  }

  let groupKey = groupContact.groupKey;
  let childList = hashTable[groupKey];
  if (childList) {
    groupContact.childrenList = childList;
  } else {
    childList = [];
    groupContact.childrenList = childList;
    hashTable[groupKey] = childList;
  }
}

export default funcs;
