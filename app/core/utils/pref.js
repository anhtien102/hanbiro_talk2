import Store from 'electron-store';
import keytar from 'keytar';
import os from 'os';
import crypto from 'crypto';
import basicUtils from './basic.utils';

function normalLang(lang) {
  return lang.split('-')[0];
}

global.ShareKeepValue = {
  CURRENT_ROOM_KEY: null,
  IS_GOT_COOKIES: false,
  GROUPWARE_TOKEN: null
};
const keystoreName = 'HanbiroTalk' + basicUtils.prefixFromArguments();

function cloneObjGlobal() {
  global.ShareGlobalObject = {
    account_info: {
      domain: '',
      user_id: '',
      password: '',
      auto_login: false,
      mobile_login: false,
      user_key: '',
      auto_login_key: '',
      web_auto_login_key: ''
    },
    extra_server_info: null,
    extra_login_info: null,
    langcode: '',
    inLoginPage: true,
    attempDisableAutoLogin: false
  };
}

cloneObjGlobal();

// STORE PATH IN HERE console.log(app.getPath('userData')); config.json

class AppPreference {
  constructor() {
    const prefix = basicUtils.prefixFromArguments();
    if (prefix) {
      const name = 'Talk' + prefix;
      this.store = new Store({ name: name });
    } else {
      this.store = new Store();
    }
  }

  updateWebAutoLoginKey(newKey) {
    global.ShareGlobalObject.account_info.web_auto_login_key = newKey;
  }

  updateNickname(newNickname) {
    const extra = global.ShareGlobalObject.extra_login_info;
    if (extra && extra.login) {
      extra.login.nickname = newNickname;
    }
  }

  generateRandomUUID() {
    return crypto.randomBytes(16).toString('hex');
  }

  getDeviceName() {
    if (process.platform == 'win32') {
      return 'WINDOWS';
    }

    if (process.platform == 'darwin') {
      return 'MAC';
    }

    return 'LINUX';
  }

  getDeviceCode() {
    if (process.platform == 'win32') {
      return 2;
    }

    if (process.platform == 'darwin') {
      return 3;
    }

    return 2;
  }

  getMethodLogin() {
    if (process.platform == 'win32') {
      return 'messenger/window';
    }
    if (process.platform == 'darwin') {
      return 'mac_messenger/mac';
    }
    return 'messenger/linux';
  }

  getPCUUid() {
    var pid = this.store.get('pref_pc_uuid');
    if (pid == null) {
      const id = crypto.randomBytes(16).toString('hex');
      pid = os.hostname + '_' + id;
      this.store.set('pref_pc_uuid', pid);
    }
    return pid;
  }

  async setDomainUserIdPasswordAutoLogin(
    domain,
    userId,
    password,
    autologin,
    mobile_login,
    callback
  ) {
    this.store.set('pref_domain', domain);
    this.store.set('pref_user_id', userId);
    this.store.set('pref_auto_login', autologin);
    this.store.set('pref_mobile_login', mobile_login);
    try {
      if (password) {
        await keytar.setPassword(keystoreName, domain + '_' + userId, password);
      } else {
        await keytar.deletePassword(keystoreName, domain + '_' + userId);
      }
      global.ShareGlobalObject.account_info.domain = domain;
      global.ShareGlobalObject.account_info.user_id = userId;
      global.ShareGlobalObject.account_info.password = password;
      global.ShareGlobalObject.account_info.auto_login = autologin;
      global.ShareGlobalObject.account_info.mobile_login = mobile_login;
    } catch (err) {}
    callback();
  }

  getLastDomain() {
    return this.store.get('pref_domain') ?? '';
  }

  getLastUserId() {
    return this.store.get('pref_user_id') ?? '';
  }

  getLastPassword(domain, userId) {
    return new Promise(resolve => {
      keytar.getPassword(keystoreName, domain + '_' + userId).then(
        v => {
          resolve(v);
        },
        err => {
          resolve('');
        }
      );
    });
  }

  getAutoLogin() {
    return this.store.get('pref_auto_login') ?? false;
  }

  getMobileLogin() {
    return this.store.get('pref_mobile_login') ?? false;
  }

  // db version
  setLastDBVersion(version) {
    this.store.set('pref_last_db_ver', version);
  }

  getLastDBVersion() {
    return this.store.get('pref_last_db_ver') ?? 1;
  }

  // keep current language
  setLanguage(lang, cb) {
    const nLang = normalLang(lang);
    this.store.set('pref_lang', nLang);
    this.updateLanguageCode(nLang, cb);
  }

  getLanguage() {
    const lang = this.store.get('pref_lang') ?? '';
    return normalLang(lang);
  }

  getLanguageDef() {
    const lang = this.store.get('pref_lang') ?? 'ko';
    return normalLang(lang);
  }

  updateLanguageCode(lang, cb) {
    const nLang = normalLang(lang);
    if (global.ShareGlobalObject.langcode != nLang) {
      global.ShareGlobalObject.langcode = nLang;
      console.log('init all language key in callback', nLang);
    }
    if (cb) {
      cb(nLang);
    }
  }

  setExtraServerInfo(info) {
    global.ShareGlobalObject.extra_server_info = info;
  }

  setSecretAccountInfo(auto_login_key, web_auto_login_key, user_key) {
    global.ShareGlobalObject.account_info.auto_login_key = auto_login_key;
    global.ShareGlobalObject.account_info.web_auto_login_key = web_auto_login_key;
    global.ShareGlobalObject.account_info.user_key = user_key;
  }

  setExtraLoginInfo(info) {
    global.ShareGlobalObject.extra_login_info = info;
  }

  setExtraLoginInfoAuthKey(authKey) {
    global.ShareGlobalObject.account_info.auto_login_key = authKey;
  }

  setAttempDisableAutoLogin(enable) {
    global.ShareGlobalObject.attempDisableAutoLogin = enable;
  }

  setShowMobileLogin(show) {
    this.store.set('pref_mobile_login_show', show);
  }

  getShowMobileLogin() {
    return this.store.get('pref_mobile_login_show', false);
  }

  eraseAccountSetting(callback) {
    this.store.set('pref_auto_login', false);
    try {
      const domain = this.getLastDomain();
      const userId = this.getLastUserId();
      keytar
        .deletePassword(keystoreName, domain + '_' + userId)
        .then(ok => {
          if (callback) {
            callback();
          }
        })
        .catch(error => {
          if (callback) {
            callback();
          }
        });
    } catch (e) {
      if (callback) {
        callback();
      }
    }
  }

  eraseSetting() {
    this.clearAllSetting();
    this.setLastUpdateRoomList(0);
    this.setLastUserListUpdate(0, 0);
    this.setLastHTTPUserListUpdate(0);
    this.setLastUpdateUserModInfo(0);
  }

  clearAllSetting() {
    cloneObjGlobal();
    console.log('clear all app setting + clear database, not develop yet');
  }

  async loadAllAppSetting(callback) {
    const account_info = global.ShareGlobalObject.account_info;
    if (
      account_info == null ||
      account_info.domain == null ||
      account_info.domain == ''
    ) {
      const domain = this.getLastDomain();
      if (domain != null || domain != '') {
        this.updateLanguageCode(this.getLanguageDef());
        global.ShareGlobalObject.account_info.domain = domain;
        global.ShareGlobalObject.account_info.user_id = this.getLastUserId();
        global.ShareGlobalObject.account_info.auto_login = this.getAutoLogin();
        global.ShareGlobalObject.account_info.mobile_login = this.getMobileLogin();
        global.ShareGlobalObject.account_info.password = await this.getLastPassword(
          global.ShareGlobalObject.account_info.domain,
          global.ShareGlobalObject.account_info.user_id
        );
        global.ShareGlobalObject.debug = {
          showMobileLogin: this.getShowMobileLogin()
        };
      }
    }
    const acc = global.ShareGlobalObject.account_info;
    const alreadyLogin =
      acc.domain &&
      acc.user_id &&
      acc.password &&
      acc.user_key &&
      acc.domain != '' &&
      acc.user_id != '' &&
      acc.password != '' &&
      acc.user_key != ''
        ? true
        : false;
    callback(global.ShareGlobalObject, alreadyLogin);
  }

  setLastUpdateUserModInfo(statusTime) {
    this.store.set('pref_last_update_user_mod_info', statusTime);
  }

  getLastUpdateUserModInfo() {
    const statusTime = this.store.get('pref_last_update_user_mod_info') ?? 0;
    return statusTime;
  }

  setLastUpdateRoomList(statusTime) {
    this.store.set('pref_last_room_list_update_time', statusTime);
  }

  getLastUpdateRoomList() {
    const statusTime = this.store.get('pref_last_room_list_update_time') ?? 0;
    return statusTime;
  }

  setLastUserListUpdate(time, timeFav) {
    this.store.set('pref_last_user_list_update_time', time);
    this.store.set('pref_last_user_list_fav_update_time', timeFav);
  }

  getLastUserListUpdate() {
    return this.store.get('pref_last_user_list_update_time') ?? 0;
  }

  getLastUserLisFavtUpdate() {
    return this.store.get('pref_last_user_list_fav_update_time') ?? 0;
  }

  getLastHTTPUserListUpdate() {
    return this.store.get('pref_last_user_list_http_update_time') ?? 0;
  }

  setLastHTTPUserListUpdate(time) {
    this.store.set('pref_last_user_list_http_update_time', time ?? 0);
  }

  isNeedRequestUserList(curUserListUpdate, curUserListFavUpdate) {
    let lastUserListUpdate = this.getLastUserListUpdate();
    let lastUserListFavUpdate = this.getLastUserLisFavtUpdate();

    if (!curUserListUpdate || !curUserListFavUpdate) {
      return true;
    }
    const okFav = lastUserListFavUpdate < curUserListFavUpdate;
    const ok = lastUserListUpdate < curUserListUpdate;
    return okFav || ok;
  }
}

const appPrefs = new AppPreference();

export default appPrefs;
