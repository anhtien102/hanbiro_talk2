import fs from 'fs';
import path from 'path';

export default class BaseTalkFolder {
  appPath() {
    return '';
  }

  tempPath() {
    return '';
  }

  portableDirPath() {
    return '';
  }

  rootFolder() {
    let dir = path.join(this.appPath(), 'HanbiroTalk');
    if (this.userKey) {
      dir = path.join(dir, this.userKey);
    }
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    return dir;
  }

  rootFolderCommon() {
    let dir = path.join(this.appPath(), 'HanbiroTalk');
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    return dir;
  }

  databaseFolder() {
    const dir = path.join(this.rootFolderCommon(), '.database');
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    return dir;
  }

  photoFolder() {
    const dir = path.join(this.rootFolderCommon(), '.photodir');
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    return dir;
  }

  fileDownloadFolder() {
    const dir = path.join(this.rootFolder(), 'files');
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    return dir;
  }

  gifsFolder() {
    const dir = path.join(this.rootFolder(), 'gifs');
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    return dir;
  }

  fileDownloadGroupwareFolder() {
    const dir = path.join(this.rootFolder(), 'groupware');
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    return dir;
  }

  fileThumbFolder() {
    const dir = path.join(this.rootFolder(), '.files_thumb');
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    return dir;
  }

  appStateFolder() {
    const dir = path.join(this.rootFolder(), '.appstate');
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    return dir;
  }

  filePathThumbWithName(roomKey, fileName) {
    const dir = path.join(this.fileThumbFolder(), roomKey);
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    return path.join(dir, fileName);
  }

  filePathDownloadedWithName(roomKey, fileName) {
    const dir = path.join(this.fileDownloadFolder(), roomKey);
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    return path.join(dir, fileName);
  }

  tempPathWithName(name) {
    const dir = this.tempPath();
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    return path.join(dir, name);
  }

  imageResourceDirPathWithDir(dir) {
    return path.join(this.portableDirPath(), 'images', dir);
  }

  /**
   * [whisper] save file of whisper in one folder matching unique id whisper
   *
   * @param {*} key
   * @param {*} fileName
   * @returns
   * @memberof BaseTalkFolder
   */
  filePathDownloadedGroupwareWithName(key, fileName) {
    const dir = path.join(this.fileDownloadGroupwareFolder(), key);
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    return path.join(dir, fileName);
  }

  fileExistAtPath(filePath) {
    return fs.existsSync(filePath);
  }

  writeBase64Image(filePath, base64Str) {
    fs.writeFileSync(filePath, base64Str, 'base64');
  }
}
