import { app } from 'electron';
import BaseTalkFolder from './base-talk-folder';

class TalkFolder extends BaseTalkFolder {
  appPath() {
    return app.getPath('appData');
  }

  tempPath() {
    return app.getPath('temp');
  }

  portableDirPath() {
    return app.getAppPath();
  }
}

const talkPath = new TalkFolder();
export default talkPath;
