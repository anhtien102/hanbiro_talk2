import { remote } from 'electron';
import BaseTalkFolder from './base-talk-folder';

class TalkFolderRender extends BaseTalkFolder {
  appPath() {
    return remote.app.getPath('appData');
  }
  tempPath() {
    return remote.app.getPath('temp');
  }
  portableDirPath() {
    return remote.app.getAppPath();
  }
}

const talkPathRender = new TalkFolderRender();
export default talkPathRender;
