export function isDifferentMinute(lastTime, time) {
  return time - lastTime > 60000;
}

export function isBefore1Hour(lastTime, time) {
  let diff = time - lastTime;
  return diff >= 1800000;
}

export function isEqualDate(date1, date2) {
  let y1 = date1.getFullYear();
  let m1 = date1.getMonth();
  let d1 = date1.getDate();

  let y2 = date2.getFullYear();
  let m2 = date2.getMonth();
  let d2 = date2.getDate();

  return y1 == y2 && m1 == m2 && d1 == d2;
}

export function isBefore(lastTime, time) {
  let date = new Date(time);
  let lastDate = new Date(lastTime);

  let y = lastDate.getFullYear();
  let m = lastDate.getMonth();
  let d = lastDate.getDate();

  let y1 = date.getFullYear();
  let m1 = date.getMonth();
  let d1 = date.getDate();

  if (y < y1) {
    return true;
  } else if (y > y1) {
    return false;
  } else if (m < m1) {
    return true;
  } else if (m > m1) {
    return false;
  } else if (d < d1) {
    return true;
  } else if (d > d1) {
    return false;
  }
  return false;
}
