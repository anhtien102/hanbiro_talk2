import Hangul from 'hangul-js';

const funcs = {
  decomposeSylLabels(str) {
    if (str == null || str == '') {
      return '';
    }
    let result = '';
    let arr = Hangul.disassemble(str, true);
    if (arr.length > 0) {
      arr.forEach(element => {
        if (element.length > 0) {
          result += element[0];
        }
      });
    }
    if (Hangul.isChoAll(result)) {
      return result;
    }
    return '';
  },
  decomposeSylLabelsNotExceptMIDTAIL(str) {
    if (str == null || str == '') {
      return '';
    }
    let result = '';
    let arr = Hangul.disassemble(str, true);
    if (arr.length > 0) {
      for (let i = 0; i < arr.length; i++) {
        let element = arr[i];
        if (element.length == 1) {
          if (Hangul.isCho(element[0])) {
            result += element[0];
          }
        } else {
          return '';
        }
      }
    }
    return result;
  }
};

export default funcs;
