import { nativeImage, ipcMain } from 'electron';
import BadgeGenerator from './badge_generator';

const UPDATE_BADGE_EVENT = 'talk2-update-badge';
const UPDATE_BADGE_ACTION_INCREASE = 'increase';
const UPDATE_BADGE_ACTION_SET = 'set';
let currentOverlayIcon = { image: null, badgeDescription: '' };

export default class Badge {
  constructor({ win, opts = {}, callback = null }) {
    this.initListeners.bind(this);
    this.currentBadgeNumber = 0;
    this.win = win;
    this.callback = callback;
    this.opts = opts;
    this.generator = new BadgeGenerator(win, opts);
    this.initListeners();
    this.win.on('closed', () => {
      this.win = null;
    });
    this.win.on('show', () => {
      this.win.setOverlayIcon(
        currentOverlayIcon.image,
        currentOverlayIcon.badgeDescription
      );
    });
  }

  update(badgeNumber) {
    if (badgeNumber) {
      this.generator.generate(badgeNumber).then(base64 => {
        const image = nativeImage.createFromDataURL(base64);
        currentOverlayIcon = {
          image: image,
          badgeDescription: ''
        };
        this.win.setOverlayIcon(
          currentOverlayIcon.image,
          currentOverlayIcon.badgeDescription
        );
      });
    } else {
      currentOverlayIcon = {
        image: null,
        badgeDescription: ''
      };
      this.win.setOverlayIcon(
        currentOverlayIcon.image,
        currentOverlayIcon.badgeDescription
      );
    }
  }

  initListeners() {
    ipcMain.on(UPDATE_BADGE_EVENT, (event, action, badgeNumber) => {
      if (this.win) {
        if (action == UPDATE_BADGE_ACTION_INCREASE) {
          this.currentBadgeNumber++;
        } else if ((action = UPDATE_BADGE_ACTION_SET)) {
          this.currentBadgeNumber = badgeNumber;
        }
        if (this.callback) {
          this.callback(this.currentBadgeNumber);
        }
        this.update(this.currentBadgeNumber);
      }
      event.returnValue = `success_${this.currentBadgeNumber}`;
    });
  }
}
