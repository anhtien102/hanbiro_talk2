import { nativeImage } from 'electron';
// import Jimp from 'jimp';
// import imageSize from 'image-size';
import fs from 'fs';
// export function dimensionImageSync(filePath) {
//   if (filePath) {
//     return imageSize(filePath);
//   }
//   return { widht: 0, height: 0 };
// }

export function dimensionNativeImageSync(filePath) {
  if (filePath) {
    return nativeImage.createFromPath(filePath).getSize();
    // try {
    //   return imageSize(filePath);
    // } catch (error) {}
  }
  return { widht: 0, height: 0 };
}

export function base64FromImagePath(
  srcPath,
  size,
  quality,
  realName,
  callback
) {
  if (!checkNameSupport(realName)) {
    callback('');
    return;
  }
  const thumbImage = nativeImage.createFromPath(srcPath);
  const dimen = thumbImage.getSize();

  const thumbnailBase64 = thumbImage
    .resize({
      width: getThumbnailSize(dimen.width, dimen.height, size)
    })
    .toJPEG(quality)
    .toString('base64');
  if (callback) {
    callback(thumbnailBase64);
  }
}

export function resizeImageWithPathAndSave(
  srcPath,
  size,
  destPath,
  quality,
  realName,
  callback
) {
  resizeNativeImageWithPathAndSave(
    srcPath,
    size,
    destPath,
    quality,
    realName,
    callback
  );
}

function resizeNativeImageWithPathAndSave(
  srcPath,
  size,
  destPath,
  quality,
  realName,
  callback
) {
  if (!checkNameSupport(realName)) {
    callback();
    return;
  }
  const thumbImage = nativeImage.createFromPath(srcPath);
  const dimen = thumbImage.getSize();

  fs.writeFileSync(
    destPath,
    thumbImage
      .resize({
        width: getThumbnailSize(dimen.width, dimen.height, size)
      })
      .toJPEG(quality)
  );
  if (callback) {
    callback();
  }
}

// function resizejimpImageWithPathAndSave(
//   srcPath,
//   size,
//   destPath,
//   quality,
//   realName,
//   callback
// ) {
//   if (!checkNameSupport(realName)) {
//     callback();
//     return;
//   }
//   Jimp.read(srcPath, (err, image) => {
//     if (!err) {
//       image
//         .resize(size, size)
//         .quality(30)
//         .getBase64(Jimp.AUTO, (error, data) => {
//           if (!error) {
//             let encondedImageBuffer = data.replace(
//               /^data:image\/(png|gif|jpeg|bmp);base64,/,
//               ''
//             );
//             fs.writeFileSync(destPath, encondedImageBuffer, {
//               encoding: 'base64'
//             });
//           }
//           if (callback) {
//             callback();
//           }
//         });
//       return;
//     }
//     if (callback) {
//       callback();
//     }
//   });
// }

function checkNameSupport(fileName) {
  if (fileName == null || fileName == '') {
    return false;
  }

  let posDot = fileName.lastIndexOf('.');
  if (posDot < 0) {
    return false;
  }

  const ext = fileName.substring(posDot + 1).toLowerCase();
  return ext == 'jpg' || ext == 'png' || ext == 'gif' || ext == 'jpeg';
}

function getThumbnailSize(orgWidth, orgHeight, requireSize) {
  const ratio = orgWidth / orgHeight;
  let targetWidth = Math.min(requireSize, Math.max(orgWidth, orgHeight));
  let targetHeight = targetWidth;
  if (ratio < 1) {
    targetWidth = targetHeight * ratio;
  } else {
    targetHeight = targetWidth / ratio;
  }

  return targetWidth;
}
