import TextUtils from '../utils/text.util';
import {
  ROOM_STATUS_READED,
  ROOM_DETAIL_STATUS_ENTER,
  ROOM_DETAIL_STATUS_LEAVE,
  isLiveUser,
  isGuestUser
} from '../service/talk-constants';
import { roomUtils } from '../model/OrgUtils';

export function buildRoomList(
  listRoom: [],
  listUser: {},
  loggedUser: {},
  currentRoomKey,
  window_visible
) {
  let loginName =
    loggedUser && loggedUser.account_info
      ? loggedUser.account_info.user_id
      : '';
  let userKey =
    loggedUser && loggedUser.account_info
      ? loggedUser.account_info.user_key
      : '';

  let allUnreadMessageBadgeIcon = 0;
  let allUnreadMessage = 0;
  let allRoomQuery = {};
  for (let i = 0; i < listRoom.length; i++) {
    let obj = listRoom[i];
    let roomKey = obj.rRoomKey;

    if (TextUtils.isEmpty(roomKey)) {
      continue;
    }

    let room = allRoomQuery[roomKey];
    if (room == null) {
      room = { ...obj };
      delete room['rdtID'];
      delete room['rdtJoinDate'];
      delete room['rdtStatus'];
      delete room['rdtUKey'];
      delete room['rdtUName'];

      allRoomQuery[roomKey] = room;
      room.roomDetailds = [];
      if (room.rRoomIRead == ROOM_STATUS_READED) {
        room.unReadMessage = 0;
      } else {
        if (room.rRoomKey == currentRoomKey) {
          if (!window_visible) {
            room.unReadMessage = 1;
            allUnreadMessage += room.rRoomUnreadMsgServer;
            allUnreadMessageBadgeIcon += room.rRoomUnreadMsgServer;
          } else {
            room.unReadMessage = 0;
          }
        } else {
          room.unReadMessage = 1;
          allUnreadMessage += room.rRoomUnreadMsgServer;
          allUnreadMessageBadgeIcon += room.rRoomUnreadMsgServer;
        }
      }

      room.isFireRoom = roomUtils.isFireRoom(room);
      room.displayName = roomUtils.getDefaultDisplayName(room);
      if (TextUtils.isEmpty(room.displayName)) {
        room.userJidArrString = [];
        room.userJidArrStringTmp = [];
      }
    }

    if (obj.rdtID > 0 && TextUtils.isNotEmpty(obj.rdtUKey)) {
      let roomDetail = {
        rdtID: obj.rdtID,
        rdtJoinDate: obj.rdtJoinDate,
        rdtStatus: obj.rdtStatus,
        rdtUName: obj.rdtUName,
        rdtUKey: obj.rdtUKey
      };
      room.roomDetailds.push(roomDetail);
      if (room.userJidArrString && room.userJidArrString.length < 20) {
        let contact = listUser[roomDetail.rdtUKey];
        if (contact != null) {
          if (contact.userKey != userKey) {
            // not logged user
            if (roomDetail.rdtStatus == ROOM_DETAIL_STATUS_ENTER) {
              room.userJidArrString.push(contact.displayName);
            }
            room.userJidArrStringTmp.push(contact.displayName);
          }
        } else {
          if (
            isGuestUser(roomDetail.rdtUKey) ||
            isLiveUser(roomDetail.rdtUKey)
          ) {
            if (roomDetail.rdtStatus == ROOM_DETAIL_STATUS_ENTER) {
              room.userJidArrString.push(roomDetail.rdtUName);
            }
            room.userJidArrStringTmp.push(roomDetail.rdtUName);
          } else if (roomDetail.rdtUKey != userKey) {
            if (roomDetail.rdtStatus == ROOM_DETAIL_STATUS_ENTER) {
              if (TextUtils.isEmpty(roomDetail.rdtUName)) {
                room.userJidArrString.push('_');
              } else {
                room.userJidArrString.push(roomDetail.rdtUName);
              }
            }
          }
        }
      }
    }
  }

  const objArray = Object.values(allRoomQuery);
  const objArrayFilter = [];
  const roomCached = {};
  objArray.forEach(room => {
    if (room.roomDetailds.length <= 2) {
      room.rRoomServerLastSenderUserKey = '';
    }
    roomCached[room.rRoomKey] = room;

    if (TextUtils.isEmpty(room.displayName)) {
      if (room.userJidArrString.length > 0) {
        room.displayName = room.userJidArrString.join(' ,');
      } else if (room.userJidArrStringTmp.length > 0) {
        room.displayName = room.userJidArrStringTmp.join(' ,');
      } else {
        room.displayName = '';
      }

      delete room['userJidArrString'];
      delete room['userJidArrStringTmp'];
    }

    let lastServerMsgNumber = room.rRoomServerLastMsgNumber;
    if (TextUtils.isNotEmpty(lastServerMsgNumber)) {
      objArrayFilter.push(room);
    }
  });

  return {
    allUnreadMessageBadgeIcon: allUnreadMessageBadgeIcon,
    allUnreadMessage: allUnreadMessage,
    rooms: objArrayFilter,
    room_cached: roomCached
  };
}
