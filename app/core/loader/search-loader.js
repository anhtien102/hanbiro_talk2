import TextUtils from '../utils/text.util';
import { contactUtils, roomUtils, groupUtils } from '../model/OrgUtils';
import { ROOM_DETAIL_STATUS_ENTER } from '../service/talk-constants';

var lastUserKeyOnlyResult;
var lastOrgResult;
var lastSearchText;
var lastSearchChosun;
var lastDepartmentResult;
export function buildSearchResult(
  org,
  rooms,
  group_cached,
  searchText,
  searchChosun,
  loggedUser: {}
) {
  if (searchText == '') {
    return {
      searchText: [],
      allSearchList: []
    };
  }

  let orgResults = [];
  let roomResults = [];
  let departmentResults = [];

  let userKeyListOnlyResult = [];

  let language = loggedUser ? loggedUser.langcode : '';
  let loginName =
    loggedUser && loggedUser.account_info
      ? loggedUser.account_info.user_id
      : '';
  let userKey =
    loggedUser && loggedUser.account_info
      ? loggedUser.account_info.user_key
      : '';

  if (org) {
    org.forEach(contact => {
      if (contactUtils.isAdmin(contact) || contactUtils.isHiddenUser(contact)) {
        return;
      }
      contact.primaryKey = contact.userKey + '_' + contact.userId;
      contact.displayName = contactUtils.getDisplayName(
        contact,
        language,
        loginName,
        userKey
      );
      const duty = contactUtils.getDisplayDuty(contact, language);
      contact.displayNameWithDuty = `${contact.displayName} (${duty})`;
      contact.parentGroup = group_cached[contact.groupKey];
      contact.fullLocation = findFullLocation(contact.groupKey, group_cached);
      orgResults.push(contact);
      userKeyListOnlyResult.push(contact.userKey);
    });
  }

  if (rooms) {
    rooms.forEach(room => {
      if (
        roomUtils.isRoomHasSearchText(room, searchText) ||
        roomUtils.isRoomHasChosunSearchText(room, searchChosun) ||
        roomUtils.isRoomDetailHasSearchText(
          room,
          searchText,
          userKeyListOnlyResult
        )
      ) {
        roomResults.push(room);
      }
    });
  }

  for (const [key, value] of Object.entries(group_cached)) {
    if (groupUtils.hasContainSearchText(value, searchText)) {
      departmentResults.push(value);
    }
  }

  lastUserKeyOnlyResult = userKeyListOnlyResult;
  lastSearchText = searchText;
  lastSearchChosun = searchChosun;
  lastOrgResult = orgResults;

  lastDepartmentResult = departmentResults;

  const orgValue = orgResults.length > 0 ? orgResults : ['no_result'];
  const roomValue = roomResults.length > 0 ? roomResults : ['no_result'];
  const departmentValue =
    departmentResults.length > 0 ? departmentResults : ['no_result'];

  const allSearchList = [
    'contacts',
    ...orgValue,
    'rooms',
    ...roomValue,
    'departments',
    ...departmentValue
  ];

  return {
    searchText: searchText,
    allSearchList: allSearchList
  };
}

export function buildSearchResultOrgOnly(
  org,
  group_cached,
  searchText,
  loggedUser: {}
) {
  if (searchText == '') {
    return {
      searchText: [],
      allSearchList: []
    };
  }

  let orgResults = [];

  let userKeyListOnlyResult = [];

  let language = loggedUser ? loggedUser.langcode : '';
  let loginName =
    loggedUser && loggedUser.account_info
      ? loggedUser.account_info.user_id
      : '';
  let userKey =
    loggedUser && loggedUser.account_info
      ? loggedUser.account_info.user_key
      : '';

  if (org) {
    org.forEach(contact => {
      if (contactUtils.isAdmin(contact) || contactUtils.isHiddenUser(contact)) {
        return;
      }
      contact.primaryKey = contact.userKey + '_' + contact.userId;
      contact.displayName = contactUtils.getDisplayName(
        contact,
        language,
        loginName,
        userKey
      );
      const duty = contactUtils.getDisplayDuty(contact, language);
      contact.displayNameWithDuty = `${contact.displayName} (${duty})`;
      contact.fullLocation = findFullLocation(contact.groupKey, group_cached);
      orgResults.push(contact);
      userKeyListOnlyResult.push(contact.userKey);
    });
  }
  const orgValue = orgResults.length > 0 ? orgResults : ['no_result'];

  return {
    searchText: searchText,
    allSearchList: orgValue
  };
}

export function buildExtraUserInfo(data) {
  const result = {};
  const userCached = data.userCached;
  const users = data.users;
  const group_cached = data.group_cached;

  if (users && userCached) {
    users.forEach(element => {
      if (element.rdtStatus == ROOM_DETAIL_STATUS_ENTER) {
        const data = userCached[element?.rdtUKey];
        if (data) {
          data.fullLocation = findFullLocation(
            data.userParentKey,
            group_cached
          );
          result[data.userKey] = data.fullLocation;
        }
      }
    });
  }

  return result;
}

export function buildSearchResultV2(rooms) {
  if (lastSearchText == null || lastSearchText == '' || lastOrgResult == null) {
    return null;
  }
  let roomResults = [];
  rooms.forEach(room => {
    if (
      roomUtils.isRoomHasSearchText(room, lastSearchText) ||
      roomUtils.isRoomHasChosunSearchText(room, lastSearchChosun) ||
      roomUtils.isRoomDetailHasSearchText(
        room,
        lastSearchText,
        lastUserKeyOnlyResult
      )
    ) {
      roomResults.push(room);
    }
  });

  const orgValue = lastOrgResult.length > 0 ? lastOrgResult : ['no_result'];
  const roomValue = roomResults.length > 0 ? roomResults : ['no_result'];
  const departmentValue =
    lastDepartmentResult?.length > 0 ? lastDepartmentResult : ['no_result'];

  const allSearchList = [
    'contacts',
    ...orgValue,
    'rooms',
    ...roomValue,
    'departments',
    ...departmentValue
  ];
  return {
    searchText: lastSearchText,
    allSearchList: allSearchList
  };
}

function findFullLocation(groupKey, group_cached) {
  let result = '';
  let first = null;
  let curGroupKey = groupKey;
  do {
    let curParentGroup = group_cached[curGroupKey];
    if (curParentGroup == null) {
      break;
    }

    if (isRootGroup(curParentGroup)) {
      if (first) {
        result = curParentGroup.displayName + ' > ' + result;
      } else {
        first = curParentGroup.displayName;
      }

      break;
    }
    if (first) {
      result = curParentGroup.displayName + ' > ' + result;
    } else {
      first = curParentGroup.displayName;
    }
    curGroupKey = curParentGroup.groupPKey;
  } while (curGroupKey != null);
  return {
    first: first,
    result: result
  };
}

function isRootGroup(group) {
  return (
    group.groupKey == '' ||
    group.groupKey == '0' ||
    group.groupKey == '00000000' ||
    group.groupKey == '0000' ||
    parseInt(group.groupKey) < 0
  );
}
