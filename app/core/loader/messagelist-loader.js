import TextUtils from '../utils/text.util';
import { messsageUtils } from '../model/MessageUtils';
import { roomUtils } from '../model/OrgUtils';
import {
  CREATE_DATE_SENDING,
  ROOM_STATUS_READED,
  ROOM_DETAIL_STATUS_ENTER,
  MessageFileStatus,
  MessageSendStatus,
  isLiveUser,
  isGuestUser,
} from '../service/talk-constants';

import {
  isBefore,
  isBefore1Hour,
  isDifferentMinute,
  isEqualDate,
} from '../utils/date.utils';

export function buildFileList(param) {
  let messages = param.messageList;

  if (!messages) {
    return { ...param, messages: [] };
  }
  let newMessages = [];
  let curDate = null;
  for (let i = 0; i < messages.length; i++) {
    let msg = messages[i];
    if (
      msg.msgdtStatus == MessageFileStatus.FILE_MSG_UPLOAD_INTERRUPT ||
      msg.msgdtStatus == MessageFileStatus.FILE_MSG_UPLOAD_FAILED ||
      msg.msgdtStatus == MessageFileStatus.FILE_MSG_UPLOAD_WORKING ||
      msg.msgSendStatus == MessageSendStatus.STATUS_SEND_SENDING ||
      msg.msgSendStatus == MessageSendStatus.STATUS_SEND_MAYBE_PENDING ||
      msg.msgSendStatus == MessageSendStatus.STATUS_SEND_PENDING ||
      msg.msgIsDeleted == 1
    ) {
      continue;
    }
    msg.msgdtHasFile = 1;

    msg.primaryKey =
      msg.msgID +
      msg.msgNumberKey +
      '_' +
      msg.msgdtStatus +
      '_' +
      msg.msgThirdPartyID +
      '_' +
      msg.msgdtHasFile;

    let date = new Date(msg.msgCreateDate);
    if (curDate == null || !isEqualDate(date, curDate)) {
      curDate = date;
      let labelDate = { type: 'date', time: date };
      labelDate.primaryKey = msg.msgCreateDate;
      newMessages.push(labelDate);
    }
    newMessages.push(msg);
  }
  return { ...param, messages: newMessages };
}

export function buildMessageList(
  roomID,
  roomKey,
  firstMessageTime,
  listMessage: [],
  loggedUser: {},
  myInfo
) {
  let messageResultList = [];
  let currentUserKey = '';
  let accountName = '';
  if (loggedUser && loggedUser.account_info) {
    currentUserKey = loggedUser.account_info.user_key;
    accountName = loggedUser.account_info.user_id ?? '';
  }

  let shiftMessage = null;
  let shiftNameMessage = null;
  let shiftTimeMessage = null;
  let shiftPrevMessage = null;

  for (let i = listMessage.length - 1; i >= 0; i--) {
    let msg = listMessage[i];

    let countUnread = msg.msgCountUnreaded ?? 0;
    let msgReadedKey = msg.msgReadedKey ?? '';
    let isSenderReaded = msgReadedKey.includes(msg.msgUserKey);
    if (!isSenderReaded && msg.msgUserKey != currentUserKey) {
      countUnread--;
    }

    if (!messsageUtils.isServerReaded(msg)) {
      countUnread--;
    }

    msg.msgBodies = findHighLight(msg.msgBody, myInfo, accountName);
    // for detect remote link
    // const rLinks = findRemoteLink(msg.msgBody, myInfo, accountName);
    // msg.msgBodies = rLinks != null && rLinks.length > 0 ? rLinks : null;

    msg.msgCountUnreaded = countUnread < 0 ? 0 : countUnread;

    if (shiftMessage == null) {
      shiftMessage = msg;
    }

    msg.primaryKey =
      msg.msgID +
      msg.msgNumberKey +
      '_' +
      msg.msgdtStatus +
      '_' +
      msg.msgThirdPartyID +
      '_' +
      msg.msgdtHasFile;

    msg.isSending =
      msg.msgSendStatus == MessageSendStatus.STATUS_SEND_SENDING ||
      msg.msgSendStatus == MessageSendStatus.STATUS_SEND_MAYBE_PENDING;
    msg.isPending = msg.msgSendStatus == MessageSendStatus.STATUS_SEND_PENDING;
    msg.isMe =
      msg.msgUserKey == currentUserKey || msg.isSending || msg.isPending;

    if (
      shiftTimeMessage != null &&
      (shiftTimeMessage.msgUserKey != msg.msgUserKey ||
        (shiftTimeMessage.msgCreateDate < CREATE_DATE_SENDING &&
          isDifferentMinute(shiftTimeMessage.msgCreateDate, msg.msgCreateDate)))
    ) {
      shiftTimeMessage.isShowTime = true;
    }
    shiftTimeMessage = msg;

    if (msg.msgUserKey != currentUserKey) {
      if (shiftNameMessage == null) {
        shiftNameMessage = msg;
        shiftNameMessage.isShowName = true;

        if (
          shiftPrevMessage != null &&
          msg.msgUserKey != shiftPrevMessage.msgUserKey
        ) {
          if (
            isBefore1Hour(shiftPrevMessage.msgCreateDate, msg.msgCreateDate)
          ) {
            let isShow = msg.msgCreateDate < CREATE_DATE_SENDING;
            msg.showTimePerHours = isShow;
          }
        }
      } else if (
        shiftPrevMessage != null &&
        msg.msgUserKey != shiftPrevMessage.msgUserKey
      ) {
        shiftNameMessage = msg;
        shiftNameMessage.isShowName = true;
        if (isBefore1Hour(shiftPrevMessage.msgCreateDate, msg.msgCreateDate)) {
          let isShow = msg.msgCreateDate < CREATE_DATE_SENDING;
          msg.showTimePerHours = isShow;
        }
      }
    } else {
      if (
        shiftPrevMessage != null &&
        msg.msgUserKey != shiftPrevMessage.msgUserKey
      ) {
        msg.isShowPad = true;
      }
      msg.isShowName = false;
      shiftNameMessage = null;
    }

    if (messageResultList.length > 0) {
      if (isBefore(shiftMessage.msgCreateDate, msg.msgCreateDate)) {
        let isShow = shiftMessage.msgCreateDate < CREATE_DATE_SENDING;
        shiftMessage.isShowDate = isShow;
        shiftMessage.isShowName = isShow;
        shiftMessage = msg;
        if (shiftPrevMessage != null) {
          shiftPrevMessage.isShowTime = isShow;
        }
      }
    }

    shiftPrevMessage = msg;

    messageResultList.push(msg);
  }

  if (shiftMessage != null) {
    let isShow = shiftMessage.msgCreateDate < CREATE_DATE_SENDING;
    shiftMessage.isShowDate = isShow;
    shiftMessage.isShowName = isShow;
  }

  if (shiftTimeMessage != null) {
    shiftTimeMessage.isShowTime = true;
  }

  return {
    roomID: roomID,
    roomKey: roomKey,
    firstMessageTime: firstMessageTime,
    messages: messageResultList,
  };
}

function getUserInfo(userCached, msgDetail) {
  if (userCached && userCached[msgDetail.rdtUKey]) {
    return userCached[msgDetail.rdtUKey];
  } else {
    return {
      primaryKey: msgDetail.rdtUKey,
      userId: -1,
      userAlias: msgDetail.rdtUName,
      displayName: msgDetail.rdtUName,
      userKey: msgDetail.rdtUKey,
      userPhotoTime: -1,
    };
  }
}

export function buildRoomDetailList(roomDetail, listUser: {}, loggedUser: {}) {
  let room = roomDetail.room;
  let roomDetailes = roomDetail.details;
  let listAllUserInRoom = [];
  let listUserActiveInRoom = [];

  if (!room) {
    return null;
  }

  let userKey =
    loggedUser && loggedUser.account_info
      ? loggedUser.account_info.user_key
      : '';

  room.roomDetailds = roomDetailes;

  if (room.rRoomIRead == ROOM_STATUS_READED) {
    room.unReadMessage = 0;
  } else {
    room.unReadMessage = 1;
  }

  room.isFireRoom = roomUtils.isFireRoom(room);
  room.displayName = roomUtils.getDefaultDisplayName(room);
  if (TextUtils.isEmpty(room.displayName)) {
    room.userJidArrString = [];
    room.userJidArrStringTmp = [];
  }

  for (let i = 0; i < roomDetailes.length; i++) {
    let roomDetail = roomDetailes[i];
    if (roomDetail.rdtID > 0 && TextUtils.isNotEmpty(roomDetail.rdtUKey)) {
      if (room.userJidArrString && room.userJidArrString.length < 20) {
        let contact = listUser ? listUser[roomDetail.rdtUKey] : null;
        if (contact != null) {
          if (contact.userKey != userKey) {
            // not logged user
            if (roomDetail.rdtStatus == ROOM_DETAIL_STATUS_ENTER) {
              room.userJidArrString.push(contact.displayName);
            }
            room.userJidArrStringTmp.push(contact.displayName);
          }
        } else {
          if (
            isGuestUser(roomDetail.rdtUKey) ||
            isLiveUser(roomDetail.rdtUKey)
          ) {
            if (roomDetail.rdtStatus == ROOM_DETAIL_STATUS_ENTER) {
              room.userJidArrString.push(roomDetail.rdtUName);
            }
            room.userJidArrStringTmp.push(roomDetail.rdtUName);
          } else if (roomDetail.rdtUKey != userKey) {
            if (roomDetail.rdtStatus == ROOM_DETAIL_STATUS_ENTER) {
              if (TextUtils.isEmpty(roomDetail.rdtUName)) {
                room.userJidArrString.push('_');
              } else {
                room.userJidArrString.push(roomDetail.rdtUName);
              }
            }
          }
        }
      }

      listAllUserInRoom.push(getUserInfo(listUser, roomDetail));
      if (roomDetail.rdtStatus == ROOM_DETAIL_STATUS_ENTER) {
        listUserActiveInRoom.push(getUserInfo(listUser, roomDetail));
      }
    }
  }

  if (TextUtils.isEmpty(room.displayName)) {
    if (room.userJidArrString.length > 0) {
      room.displayName = room.userJidArrString.join(' ,');
    } else if (room.userJidArrStringTmp.length > 0) {
      room.displayName = room.userJidArrStringTmp.join(' ,');
    } else {
      room.displayName = '';
    }

    delete room['userJidArrString'];
    delete room['userJidArrStringTmp'];
  }

  return {
    room: room,
    listAllUserInRoom: listAllUserInRoom,
    listUserActiveInRoom: listUserActiveInRoom,
  };
}

const QUOTE_REGEX = new RegExp('@[^@^:]*[:,]', 'gm');
const REMOTE_REGEX = new RegExp('remote_control://\\w*', 'gm');

function findRemoteLink(textLow, myInfo, accountName) {
  if (!textLow.includes('remote_control://')) {
    return findHighLightNotNull(textLow, myInfo, accountName);
  }

  let result = [];
  let matchs = textLow.match(REMOTE_REGEX);

  if (matchs.length == 0) {
    return findHighLightNotNull(textLow, myInfo, accountName);
  }

  let fromIndex = 0;
  for (let index = 0; index < matchs.length; index++) {
    const s = matchs[index];
    const indexInWord = textLow.indexOf(s, fromIndex);
    const start = indexInWord;
    const prevText = textLow.substring(fromIndex, start);
    let prevNotSpaceOrNull = true;
    if (prevText) {
      result = result.concat(
        findHighLightNotNull(prevText, myInfo, accountName)
      );
      prevNotSpaceOrNull =
        prevText.endsWith(' ') ||
        prevText.endsWith('\r') ||
        prevText.endsWith('\r\n') ||
        prevText.endsWith('\n')
          ? true
          : false;
    }

    if (!prevNotSpaceOrNull) {
      result = result.concat(findHighLightNotNull(s, myInfo, accountName));
      fromIndex = indexInWord + s.length;
      continue;
    }

    result.push({ text: s, remote_control: true });
    fromIndex = indexInWord + s.length;
  }
  const prevText = textLow.substring(fromIndex);
  if (prevText) {
    result = result.concat(findHighLightNotNull(prevText, myInfo, accountName));
  }
  return result;
}

function findHighLightNotNull(textLow, myInfo, accountName) {
  if (!textLow.includes('@')) {
    return [{ text: textLow, highlight: false }];
  }

  let result = [];
  let matchs = textLow.match(QUOTE_REGEX);

  let newMatch = [];
  if (matchs) {
    matchs.forEach((s) => {
      if (
        s.split(' ').length > 5 ||
        s.startsWith('@ ') ||
        s.includes('\n') ||
        s.includes('\r') ||
        s.includes('\r\n')
      ) {
        return;
      }

      newMatch.push(s);
    });
  }

  if (newMatch.length == 0) {
    return [{ text: textLow, highlight: false }];
  }

  let fromIndex = 0;
  for (let index = 0; index < newMatch.length; index++) {
    const s = newMatch[index];
    const indexInWord = textLow.indexOf(s, fromIndex);
    const start = indexInWord;
    const prevText = textLow.substring(fromIndex, start);
    let prevNotSpaceOrNull = true;
    if (prevText) {
      result.push({ text: prevText, highlight: false });
      prevNotSpaceOrNull =
        prevText.endsWith(' ') ||
        prevText.endsWith('\r') ||
        prevText.endsWith('\r\n') ||
        prevText.endsWith('\n')
          ? true
          : false;
    }

    if (!prevNotSpaceOrNull) {
      result.push({ text: s, highlight: false });
      fromIndex = indexInWord + s.length;
      continue;
    }

    const tag = s.replace('@', '').replace(':', '');

    // const tagMyName =
    //   myInfo &&
    //   tag &&
    //   (myInfo.displayName.includes(tag) ||
    //     myInfo.userFullName.includes(tag) ||
    //     myInfo.userShortName.includes(tag) ||
    //     accountName.includes(tag) ||
    //     (TextUtils.isNotEmpty(myInfo.displayName) &&
    //       tag.includes(myInfo.displayName)) ||
    //     (TextUtils.isNotEmpty(myInfo.userFullName) &&
    //       tag.includes(myInfo.userFullName)) ||
    //     (TextUtils.isNotEmpty(myInfo.userShortName) &&
    //       tag.includes(myInfo.userShortName)));
    const tagMyName =
      myInfo &&
      tag &&
      (myInfo.displayName.includes(tag) ||
        myInfo.userFullName.includes(tag) ||
        myInfo.userShortName.includes(tag) ||
        accountName.includes(tag));

    result.push({ text: s, highlight: true, tagMyName: tagMyName });
    fromIndex = indexInWord + s.length;
  }

  const prevText = textLow.substring(fromIndex);
  if (prevText) {
    result.push({ text: prevText, highlight: false });
  }

  return result;
}

function findHighLight(textLow, myInfo, accountName) {
  if (!textLow.includes('@')) {
    return null;
  }

  let result = [];
  let matchs = textLow.match(QUOTE_REGEX);

  // console.log(matchs);
  let newMatch = [];
  if (matchs) {
    matchs.forEach((s) => {
      if (
        s.split(' ').length > 5 ||
        s.startsWith('@ ') ||
        s.includes('\n') ||
        s.includes('\r') ||
        s.includes('\r\n')
      ) {
        return;
      }

      newMatch.push(s);
    });
  }

  if (newMatch.length == 0) {
    return null;
  }

  let fromIndex = 0;
  for (let index = 0; index < newMatch.length; index++) {
    const s = newMatch[index];
    const indexInWord = textLow.indexOf(s, fromIndex);
    const start = indexInWord;
    const prevText = textLow.substring(fromIndex, start);
    let prevNotSpaceOrNull = true;
    if (prevText) {
      result.push({ text: prevText, highlight: false });

      prevNotSpaceOrNull =
        prevText.endsWith(' ') ||
        prevText.endsWith('\r') ||
        prevText.endsWith('\r\n') ||
        prevText.endsWith('\n')
          ? true
          : false;
    }

    if (!prevNotSpaceOrNull) {
      result.push({ text: s, highlight: false });
      fromIndex = indexInWord + s.length;
      continue;
    }

    const tag = s.replace('@', '').replace(':', '');

    // const tagMyName =
    //   myInfo &&
    //   tag &&
    //   (myInfo.displayName.includes(tag) ||
    //     myInfo.userFullName.includes(tag) ||
    //     myInfo.userShortName.includes(tag) ||
    //     accountName.includes(tag) ||
    //     (TextUtils.isNotEmpty(myInfo.displayName) &&
    //       tag.includes(myInfo.displayName)) ||
    //     (TextUtils.isNotEmpty(myInfo.userFullName) &&
    //       tag.includes(myInfo.userFullName)) ||
    //     (TextUtils.isNotEmpty(myInfo.userShortName) &&
    //       tag.includes(myInfo.userShortName)));
    const tagMyName =
      myInfo &&
      tag &&
      (myInfo.displayName.includes(tag) ||
        myInfo.userFullName.includes(tag) ||
        myInfo.userShortName.includes(tag) ||
        accountName.includes(tag));

    result.push({ text: s, highlight: true, tagMyName: tagMyName });
    fromIndex = indexInWord + s.length;
  }

  const prevText = textLow.substring(fromIndex);
  if (prevText) {
    result.push({ text: prevText, highlight: false });
  }

  return result;
}
