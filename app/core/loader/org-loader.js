import {
  contactUtils,
  groupUtils,
  TALK_KEY_FAVORITE,
  KEY_MY_ROOT,
  TALK_KEY_COMPANY
} from '../model/OrgUtils';
import TextUtils from '../utils/text.util';
import * as TalkConstant from '../service/talk-constants';

var showUserOnlineOnly = false;

function hasNotParent(contact) {
  return (
    contact.userParentKey == '' ||
    contact.userParentKey == 'rosters' ||
    parseInt(contact.userParentKey) < 0
  );
}

function checkContactExistInRoot(mapCachedValue, parentKey, contact) {
  const parentMap = mapCachedValue[parentKey];
  if (parentMap != null) {
    return parentMap[contact.userKey] != null;
  }
  return false;
}

function putContactToMapCached(mapCachedValue, parentKey, contact) {
  let parentMap = mapCachedValue[parentKey];
  let key = contact.userKey;
  if (parentMap == null) {
    mapCachedValue[parentKey] = { key: true };
  } else {
    parentMap[key] = true;
  }
}

function foundContact(listRoot, hashTable, contact, mapCached) {
  let isNoParent = hasNotParent(contact);
  if (isNoParent) {
    if (!checkContactExistInRoot(mapCached, KEY_MY_ROOT, contact)) {
      listRoot.push(contact);
      putContactToMapCached(mapCached, KEY_MY_ROOT, contact);
    }
  } else {
    let childList = hashTable[contact.userParentKey];
    if (childList) {
      if (!checkContactExistInRoot(mapCached, contact.userParentKey, contact)) {
        childList.push(contact);
        putContactToMapCached(mapCached, contact.userParentKey, contact);
      }
    } else {
      let key = contact.userParentKey;
      hashTable[key] = [contact];
    }
  }
}

function hasNotParentGroup(group) {
  return (
    group.groupPKey == '' ||
    group.groupPKey == '0' ||
    group.groupPKey == '00000000' ||
    group.groupPKey == '0000' ||
    parseInt(group.groupPKey) < 0
  );
}

function checkGroupExistInRoot(mapCachedValue, parentKey, groupContact) {
  const parentMap = mapCachedValue[parentKey];
  if (parentMap != null) {
    let key = groupContact.groupKey + 'G';
    return parentMap[key] != null;
  }
  return false;
}

function putGroupToMapCached(mapCachedValue, parentKey, groupContact) {
  let parentMap = mapCachedValue[parentKey];
  let key = groupContact.groupKey + 'G';
  if (parentMap == null) {
    mapCachedValue[parentKey] = { key: true };
  } else {
    parentMap[key] = true;
  }
}

function foundGroup(listRoot, hashTable, groupContact, mapCached) {
  foundGroupEx(listRoot, hashTable, groupContact, false, mapCached);
}

function foundGroupEx(
  listRoot,
  hashTable,
  groupContact,
  needCheckRoot,
  mapCached
) {
  groupContact.numberAll = 0;
  groupContact.numberOnline = 0;
  let isNoParent = hasNotParentGroup(groupContact);
  if (isNoParent) {
    if (!checkGroupExistInRoot(mapCached, KEY_MY_ROOT, groupContact)) {
      listRoot.push(groupContact);
      putGroupToMapCached(mapCached, KEY_MY_ROOT, groupContact);
    }
  } else {
    let parentKey = groupContact.groupPKey;
    let childList = hashTable[parentKey];
    if (childList == null) {
      childList = [];
      if (needCheckRoot) {
        if (!checkGroupExistInRoot(mapCached, KEY_MY_ROOT, groupContact)) {
          childList.push(groupContact);
        }
      } else {
        childList.push(groupContact);
      }
      hashTable[parentKey] = childList;
    } else {
      if (!checkGroupExistInRoot(mapCached, parentKey, groupContact)) {
        if (needCheckRoot) {
          if (!checkGroupExistInRoot(mapCached, KEY_MY_ROOT, groupContact)) {
            childList.push(groupContact);
            putGroupToMapCached(mapCached, parentKey, groupContact);
          }
        } else {
          childList.push(groupContact);
          putGroupToMapCached(mapCached, parentKey, groupContact);
        }
      }
    }
  }

  let groupKey = groupContact.groupKey;
  let childList = hashTable[groupKey];
  if (childList) {
    groupContact.childrenList = childList;
  } else {
    childList = [];
    groupContact.childrenList = childList;
    hashTable[groupKey] = childList;
  }
}

function calculateOrganizationTableViewOptimize(
  searchText,
  ignoreSearch,
  parent,
  array,
  currentDept,
  output,
  cached,
  groupCached,
  checkTypeGroup,
  leafList
) {
  currentDept++;
  for (var i = 0; i < array.length; i++) {
    let item = array[i];
    if (groupUtils.isGroupObj(item)) {
      //group
      item.level = currentDept;
      item.parentGroup = parent;
      let isNeedIgnore = true;
      if (
        TextUtils.isEmpty(searchText) ||
        (!ignoreSearch &&
          TextUtils.isNotEmpty(searchText) &&
          groupUtils.hasChildContainSearchText(item, searchText))
      ) {
        output.push(item);
        isNeedIgnore = false;
      }
      if (groupCached != null) {
        if (!checkTypeGroup) {
          groupCached[item.groupKey] = item;
        } else if (item.groupType == TalkConstant.GROUP_TYPE_USER) {
          groupCached[item.groupKey] = item;
        }
      }
      calculateOrganizationTableViewOptimize(
        searchText,
        isNeedIgnore,
        item,
        item.childrenList,
        currentDept,
        output,
        cached,
        groupCached,
        checkTypeGroup,
        leafList
      );
    } else {
      //contact
      let isAdmin = contactUtils.isAdmin(item);
      let canDisplayUser =
        !showUserOnlineOnly || contactUtils.isOnlineUser(item);
      let containSearch =
        (TextUtils.isEmpty(searchText) && !isAdmin) ||
        (!ignoreSearch &&
          TextUtils.isNotEmpty(searchText) &&
          !isAdmin &&
          contactUtils.hasContainSearchText(item, searchText));

      if (canDisplayUser && containSearch) {
        output.push(item);
      }

      if (!isAdmin) {
        if (containSearch && canDisplayUser) {
          leafList.push(item);
        }
        item.parentGroup = parent;
      }

      if (cached != null) {
        if (!cached[item.userKey]) {
          cached[item.userKey] = item;
        }
      }
      item.level = currentDept;
    }
  }
  currentDept--;
}

function countTotalUserV2(item, countedUser, user) {
  if (contactUtils.isContactObj(item)) {
    let dept = item.parentGroup;
    if (dept != null) {
      dept.numberAll = (dept.numberAll ?? 0) + 1;
      let deptAddedKey = countedUser[dept.groupKey];
      if (deptAddedKey == null) {
        deptAddedKey = {};
        countedUser[dept.groupKey] = deptAddedKey;
      }
      deptAddedKey[user.userKey] = user.userKey;

      if (contactUtils.isOnlineUser(item)) {
        dept.numberOnline = (dept.numberOnline ?? 0) + 1;
      }
    }
  } else {
    let dept = item.parentGroup;
    if (dept != null) {
      // count from bottom to up (user -> dept(+1) -> dept(+1) -> root(+1)). +1 for dept with each user
      // and child of item is not counted on Dept
      let deptKey = dept.groupKey;
      let deptAddedKey = countedUser[deptKey];
      // Not count item on dept
      if (deptAddedKey == null || deptAddedKey[user.userKey] == null) {
        dept.numberAll = (dept.numberAll ?? 0) + 1;
        if (contactUtils.isOnlineUser(user)) {
          dept.numberOnline = (dept.numberOnline ?? 0) + 1;
        }
        if (deptAddedKey == null) {
          deptAddedKey = {};
          countedUser[deptKey] = deptAddedKey;
        }
        deptAddedKey[user.userKey] = user.userKey;
      }
    }
  }

  if (contactUtils.isContactObj(item)) {
    // if item is user then item must not count for dept
    let dept = item.parentGroup;
    if (dept == null) {
      return;
    }

    let deptKey = item.parentGroup.groupKey;
    let userKey = user.userKey;
    let countDeptValue = countedUser[deptKey];
    if (
      countDeptValue == null ||
      (countDeptValue != null && countDeptValue[userKey] == null)
    ) {
      if (countDeptValue == null) {
        countDeptValue = {};
        countedUser[deptKey] = countDeptValue;
      }
      countDeptValue[userKey] = userKey;
    }
    countTotalUserV2(dept, countedUser, item);
  } else {
    let dept = item.parentGroup;
    if (dept == null) {
      return;
    }
    countTotalUserV2(dept, countedUser, user);
  }
}

function calculateNumberOfOnlineAndAll(leafList) {
  let cachedCount = {};
  for (let index = 0; index < leafList.length; index++) {
    let contact = leafList[index];
    countTotalUserV2(contact, cachedCount, contact);
  }
}

function buildMainOrgTree(
  listContact,
  listGroup,
  organizationCached,
  loggedUser,
  searchText
) {
  let mapCached = {};
  let hashTable = {};
  let listRoot = [];

  let language = loggedUser ? loggedUser.langcode : '';
  let loginName =
    loggedUser && loggedUser.account_info
      ? loggedUser.account_info.user_id
      : '';
  let userKey =
    loggedUser && loggedUser.account_info
      ? loggedUser.account_info.user_key
      : '';

  for (var i = 0; i < listContact.length; i++) {
    const contact = listContact[i];
    if (contactUtils.isHiddenUser(contact)) {
      continue;
    }
    contact.primaryKey = contact.userKey + '_' + contact.userId;
    contact.displayName = contactUtils.getDisplayName(
      contact,
      language,
      loginName,
      userKey
    );
    const duty = contactUtils.getDisplayDuty(contact, language);
    contact.displayNameWithDuty = `${contact.displayName} (${duty})`;
    foundContact(listRoot, hashTable, contact, mapCached);
  }

  for (var i = 0; i < listGroup.length; i++) {
    const group = listGroup[i];
    group.primaryKey =
      group.groupPKey +
      '_' +
      group.groupKey +
      '_' +
      (group.isFavorite ?? false) +
      '_';
    group.displayName = groupUtils.getDisplayName(group, language);
    foundGroup(listRoot, hashTable, group, mapCached);
  }

  //build table list
  let treeList = [];
  let groupCached = {};
  let leafList = [];
  calculateOrganizationTableViewOptimize(
    searchText,
    false,
    null,
    listRoot,
    1,
    treeList,
    organizationCached,
    groupCached,
    false,
    leafList
  );
  //calculate offline onine
  calculateNumberOfOnlineAndAll(leafList);
  return {
    treeList: treeList,
    allGroupMap: groupCached
  };
}

function buildFavoriteOrgTree(
  listContactFav,
  listGroupFav,
  organizationCached,
  loggedUser,
  searchText
) {
  let listRoot = [];
  let groupCached = {};
  let treeListFavorite = [];

  let language = loggedUser ? loggedUser.langcode : '';
  let loginName =
    loggedUser && loggedUser.account_info
      ? loggedUser.account_info.user_id
      : '';
  let userKey =
    loggedUser && loggedUser.account_info
      ? loggedUser.account_info.user_key
      : '';

  let listActualUserKeyFavourite = {};
  if (listContactFav && listGroupFav) {
    let mapCached = {};
    let hashTable = {};
    let mapCachedMyGroup = {};

    for (var i = 0; i < listContactFav.length; i++) {
      const contact = listContactFav[i];
      if (contactUtils.isHiddenUser(contact)) {
        continue;
      }
      contact.primaryKey = contact.userKey + '_' + contact.userId;
      contact.displayName = contactUtils.getDisplayName(
        contact,
        language,
        loginName,
        userKey
      );
      const duty = contactUtils.getDisplayDuty(contact, language);
      contact.displayNameWithDuty = `${contact.displayName} (${duty})`;
      contact.isFavorite = true;
      if (hasNotParent(contact)) {
        let key = '0_' + contact.userKey;
        listActualUserKeyFavourite[key] = true;
      }
      foundContact(listRoot, hashTable, contact, mapCached);
    }

    for (var i = 0; i < listGroupFav.length; i++) {
      const group = listGroupFav[i];
      group.groupPKey = group.pKey;
      group.isFavorite = true;
      group.primaryKey =
        group.groupPKey +
        '_' +
        group.groupKey +
        '_' +
        (group.isFavorite ?? false) +
        '_';
      group.displayName = groupUtils.getDisplayName(group, language);
      if (hasNotParentGroup(group)) {
        let key = '1_' + group.groupKey;
        listActualUserKeyFavourite[key] = true;
      }
      foundGroupEx(listRoot, hashTable, group, true, mapCached);
    }

    let leafList = [];
    calculateOrganizationTableViewOptimize(
      searchText,
      false,
      null,
      listRoot,
      2,
      treeListFavorite,
      organizationCached,
      groupCached,
      true,
      leafList
    );
    calculateNumberOfOnlineAndAll(leafList);
  }

  return {
    rootFav: listRoot,
    treeListFavorite: treeListFavorite,
    allGroupMapFav: groupCached,
    listActualUserKeyFavourite: listActualUserKeyFavourite
  };
}

export function buildAllOrgTree(
  listContact: [],
  listGroup: [],
  listContactFav: [],
  listGroupFav: [],
  loggedUser: {},
  searchText = '',
  userOnlineOnly = false
) {
  showUserOnlineOnly = userOnlineOnly;
  let organizationCached = {};
  let resultMain = buildMainOrgTree(
    listContact,
    listGroup,
    organizationCached,
    loggedUser,
    searchText
  );

  let resultFav = buildFavoriteOrgTree(
    listContactFav,
    listGroupFav,
    organizationCached,
    loggedUser,
    searchText
  );

  const allGroupMap = {
    ...resultMain.allGroupMap,
    ...resultFav.allGroupMapFav
  };

  let numberAllFav = 0;
  let numberAllFavOnline = 0;
  resultFav.rootFav.forEach(element => {
    if (contactUtils.isContactObj(element)) {
      numberAllFav++;
      if (contactUtils.isOnlineUser(element)) {
        numberAllFavOnline++;
      }
    } else {
      numberAllFav += element.numberAll;
      numberAllFavOnline += element.numberOnline;
    }
  });

  let groupRootFav = {
    groupKey: TALK_KEY_FAVORITE,
    groupPKey: '-1',
    level: 2,
    childrenList: resultFav.treeListFavorite,
    numberAll: numberAllFav,
    numberOnline: numberAllFavOnline
  };

  let mainTreeAll = [
    groupRootFav,
    ...resultFav.treeListFavorite,
    ...resultMain.treeList
  ];

  return {
    main_tree: resultMain.treeList,
    main_tree_favorite: resultFav.treeListFavorite,
    main_tree_all: mainTreeAll,
    group_cached: allGroupMap,
    user_cached: organizationCached,
    listActualUserKeyFavourite: resultFav.listActualUserKeyFavourite
  };
}

function foundContactV2(listRoot, hashTable, contact) {
  let isNoParent = hasNotParent(contact);
  if (isNoParent) {
    listRoot.push(contact);
  } else {
    let childList = hashTable[contact.userParentKey];
    if (childList) {
      childList.push(contact);
    } else {
      let key = contact.userParentKey;
      hashTable[key] = [contact];
    }
  }
}

function foundGroupV2(listRoot, hashTable, groupContact) {
  let isNoParent = hasNotParentGroup(groupContact);
  if (isNoParent) {
    listRoot.push(groupContact);
  } else {
    let parentKey = groupContact.groupPKey;
    let childList = hashTable[parentKey];
    if (childList == null) {
      childList = [];
      childList.push(groupContact);
      hashTable[parentKey] = childList;
    } else {
      childList.push(groupContact);
    }
  }

  let groupKey = groupContact.groupKey;
  let childList = hashTable[groupKey];
  if (childList) {
    groupContact.childrenList = childList;
  } else {
    childList = [];
    groupContact.childrenList = childList;
    hashTable[groupKey] = childList;
  }
}

export function buildAllOrgTreeComplex(
  listContact: [],
  listGroup: [],
  listContactFav: [],
  listGroupFav: [],
  language: '',
  loginName: '',
  searchText = '',
  userOnlineOnly = false
) {
  showUserOnlineOnly = userOnlineOnly;
  let organizationCached = {};

  let resultMain = buildMainOrgTree(
    listContact,
    listGroup,
    organizationCached,
    language,
    loginName,
    searchText
  );

  let resultFav = buildFavoriteOrgTree(
    listContactFav,
    listGroupFav,
    organizationCached,
    language,
    loginName,
    searchText
  );

  const allGroupMap = {
    ...resultMain.allGroupMap,
    ...resultFav.allGroupMapFav
  };

  let hashTable = {};
  let listRoot = [];
  resultMain.treeList.forEach(element => {
    if (contactUtils.isContactObj(element)) {
      foundContactV2(listRoot, hashTable, element);
    } else {
      foundGroupV2(listRoot, hashTable, element);
    }
  });

  let hashTableFav = {};
  let listRootFav = [];
  resultFav.treeListFavorite.forEach(element => {
    if (contactUtils.isContactObj(element)) {
      foundContactV2(listRootFav, hashTableFav, element);
    } else {
      foundGroupV2(listRootFav, hashTableFav, element);
    }
  });

  let numberAllFav = 0;
  let numberAllFavOnline = 0;
  listRootFav.forEach(element => {
    if (contactUtils.isContactObj(element)) {
      numberAllFav++;
      if (contactUtils.isOnlineUser(element)) {
        numberAllFavOnline++;
      }
    } else {
      numberAllFav += element.numberAll;
      numberAllFavOnline += element.numberOnline;
    }
  });

  let groupRootFav = {
    groupKey: TALK_KEY_FAVORITE,
    primaryKey: TALK_KEY_FAVORITE,
    groupPKey: '-1',
    level: 2,
    childrenList: listRootFav,
    desc: 'this is root temp of favorite',
    numberAll: numberAllFav,
    numberOnline: numberAllFavOnline
  };

  let mainTreeAll = [groupRootFav, ...listRoot];
  return {
    main_tree: listRoot,
    main_tree_favorite: listRootFav,
    main_tree_all: mainTreeAll,
    group_cached: allGroupMap,
    user_cached: organizationCached,
    listActualUserKeyFavourite: resultFav.listActualUserKeyFavourite
  };
}
