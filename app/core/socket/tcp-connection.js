import net from 'net';
import crypto from 'crypto';
import JSZip from 'jszip';
import log from 'electron-log';
import myLogger from '../logger/debug';
import Packet from './packet';

const CRYPT_KEY = 'This is a key456';
const HEADER_MSG_LENGTH_SIZE = 4;

const hanCrypt = new HanCryptor();

class TCPConnection {
  constructor() {
    this.saveLogger = false;
    this.connecting = false;
    this.abort = false;
    this.socket = null;
    this.userCloseSocket = false;

    this.readTimeoutTimer = null;

    this.packageSizeLength = -1;
    this.packageBuffer = null;
    this.socketCallBack = null;
    this.isLogin = true;
  }

  startReadTimeoutTimer() {
    const readTimeout = this.isLogin ? 180000 : 40000;
    this.stopReadTimeoutTimer();
    this.readTimeoutTimer = setTimeout(() => {
      myLogger.log('read timeout ');
      if (!this.userCloseSocket) {
        this.socketCallBack.socketDisconnectWithError('read timeout');
      }
      this.disconnectSocket(true);
    }, readTimeout);
  }

  stopReadTimeoutTimer() {
    if (this.readTimeoutTimer) {
      clearTimeout(this.readTimeoutTimer);
      this.readTimeoutTimer = null;
    }
  }

  connectWithIpPort(ip, port, sslSupport, sslPort, zipSupport, callback) {
    this.socketCallBack = callback;
    if (this.connecting) {
      return;
    }
    this.connecting = true;
    this.abort = false;
    this.userCloseSocket = false;
    this.sslSupport = sslSupport;
    this.zipSupport = zipSupport;

    this.ip = ip;
    if (this.sslSupport) {
      this.port = sslPort;
    } else {
      this.port = port;
    }

    this.socket = new net.Socket();
    this.socket.setTimeout(180000);
    this.timer = setTimeout(() => {
      myLogger.log('connected timeout ');
      if (!this.userCloseSocket) {
        this.socketCallBack.socketDisconnectWithError('connected timeout ');
      }
      this.disconnectSocket(true);
    }, 30000);

    this.socket.connect(port, ip, () => {
      this._clearTimeoutTimer();
      this.packageSizeLength = -1;
      this.packageBuffer = null;
      this.socketCallBack.socketConnected();
      this.startReadTimeoutTimer();
    });

    this.socket.on('onePackageBuffer', onePackageBuffer => {
      this.startReadTimeoutTimer();
      const type = this._getPayloadType(onePackageBuffer);
      try {
        let payload = onePackageBuffer.slice(
          HEADER_MSG_LENGTH_SIZE,
          onePackageBuffer.length
        );

        if (type == '') {
          myLogger.log('unknow package type, simple ignore it');
        } else if (type == '<ZIP>') {
          //extract zip
          JSZip.loadAsync(payload)
            .then(zip => {
              if (this.abort) {
                return;
              }
              var files = zip.file(/.*/);
              for (var i = 0; i < files.length; i++) {
                files[i]
                  .async('nodebuffer')
                  .then(unzipContent => {
                    if (this.abort) {
                      return;
                    }

                    let packageXML = this._decryptData(unzipContent);
                    const strPacketXML = packageXML.toString();
                    if (this.saveLogger) {
                      log.log('RECEIVED: ', type, strPacketXML);
                    } else {
                      myLogger.log('RECEIVED: ', type, strPacketXML);
                    }

                    this.socketCallBack.onReceivedPacket(strPacketXML);
                  })
                  .catch(eZ => {
                    myLogger.log(eZ);
                  });
              }
            })
            .catch(eZi => {
              myLogger.log(eZi);
            });
        } else if (type == '<MDS>' || type == '<MDQ>' || type == '<MDE>') {
          //room key file, binary
        } else if (type == '<JPG>') {
          //remote jpg file
          myLogger.log('received share image');
        } else if (type == '<GRA>') {
          myLogger.log('received drawing');
        } else {
          //plain package
          let packageXML = this._decryptData(payload);
          const strPacketXML = packageXML.toString();
          if (this.saveLogger) {
            log.log('RECEIVED: ', 'plain', strPacketXML);
          } else {
            myLogger.log('RECEIVED: ', 'plain', strPacketXML);
          }

          this.socketCallBack.onReceivedPacket(strPacketXML);
        }
      } catch (error) {
        myLogger.log(error);
      }
    });

    this.socket.on('data', data => {
      if (this.abort) {
        return;
      }

      if (!data || data.length <= 0) {
        this._clearTimeoutTimer();
        if (!this.userCloseSocket) {
          this.socketCallBack.socketDisconnectWithError(
            'data null or data length zero'
          );
        }
        this.disconnectSocket(true);
        return;
      }

      // append data to buffer
      if (this.packageBuffer == null) {
        this.packageBuffer = data;
      } else {
        this.packageBuffer = Buffer.concat([this.packageBuffer, data]);
      }

      if (this.packageBuffer.length < this.packageSizeLength) {
        return;
      }

      var done = false;
      while (!done && !this.abort) {
        if (this.packageBuffer.length < HEADER_MSG_LENGTH_SIZE) {
          return;
        }

        const payloadSize = this.packageBuffer.readUIntLE(
          0,
          HEADER_MSG_LENGTH_SIZE
        );

        if (payloadSize < 0) {
          this._clearTimeoutTimer();
          if (!this.userCloseSocket) {
            this.socketCallBack.socketDisconnectWithError('payload size < 0');
          }
          this.disconnectSocket(true);
          return;
        }

        this.packageSizeLength = payloadSize + HEADER_MSG_LENGTH_SIZE;

        if (this.packageBuffer.length < this.packageSizeLength) {
          return;
        }

        const onePackageBuffer = this.packageBuffer.slice(
          0,
          this.packageSizeLength
        );

        this.socket.emit('onePackageBuffer', onePackageBuffer);

        this.packageBuffer = this.packageBuffer.slice(this.packageSizeLength);
        this.packageSizeLength = -1;
      }
    });

    this.socket.on('error', error => {
      myLogger.log(
        'disconnected socket with error ',
        error,
        'user closed ',
        this.userCloseSocket
      );
      this._clearTimeoutTimer();
      if (!this.userCloseSocket) {
        this.socketCallBack.socketDisconnectWithError(error);
      }
      this.disconnectSocket(true);
    });

    this.socket.on('timeout', () => {
      myLogger.log('timeout read write socket event');
      this._clearTimeoutTimer();

      if (!this.userCloseSocket) {
        this.socketCallBack.socketDisconnectWithError(
          'timeout read write socket event'
        );
      }
      this.disconnectSocket(true);
    });

    this.socket.on('end', () => {
      myLogger.log('socket end');
      this._clearTimeoutTimer();
      if (!this.userCloseSocket) {
        this.socketCallBack.socketDisconnectWithError('Socket end');
      }
      this.disconnectSocket(true);
    });

    this.socket.on('close', () => {
      myLogger.log(
        'disconnected socket with user closed ',
        this.userCloseSocket
      );
      this._clearTimeoutTimer();

      if (!this.userCloseSocket) {
        this.socketCallBack.socketDisconnectWithError('Unknown');
      }
      this.disconnectSocket(true);
    });
  }

  _clearTimeoutTimer() {
    if (this.timer != null) {
      clearTimeout(this.timer);
      this.timer = null;
    }
  }

  disconnectSocket(userClose) {
    this.connecting = false;
    this.abort = true;
    this._clearTimeoutTimer();
    this.stopReadTimeoutTimer();
    this.userCloseSocket = userClose;
    if (this.socket && !this.socket.destroyed) {
      this.socket.destroy();
    }
  }

  sendPacket(packet: Packet) {
    if (this.abort) {
      return;
    }
    const xmlData = packet.xmlData();
    if (this.saveLogger) {
      log.log('SEND: ', xmlData);
    } else {
      myLogger.log('SEND: ', xmlData);
    }

    let encryptData = this._encryptData(xmlData);
    if (this.zipSupport && packet.zipSupport) {
      var zip = new JSZip();
      zip.file('s.txt', encryptData);
      zip.generateAsync({ type: 'nodebuffer' }).then(contentZip => {
        this.socket.write(hanCrypt.addHeader(contentZip, true));
      });
    } else {
      this.socket.write(hanCrypt.addHeader(encryptData));
    }
  }

  _encryptData(data) {
    return hanCrypt.aesEncryptData(data);
  }

  _decryptData(data) {
    return hanCrypt.aesDecryptData(data);
  }

  _getPayloadType(data) {
    if (data.length < HEADER_MSG_LENGTH_SIZE + 5) {
      return '';
    }

    var typebuf = data.slice(
      HEADER_MSG_LENGTH_SIZE,
      HEADER_MSG_LENGTH_SIZE + 5
    );
    var type = typebuf.toString();
    if (type === '<MDS>' || type === '<MDQ>' || type === '<MDE>') {
      return type;
    } else if (type === '<ZIP>') {
      return type;
    } else if (type === '<JPG>') {
      return type;
    } else if (type === '<GRA>') {
      return type;
    }
    return '<XML>';
  }
}

function HanCryptor() {
  var addHeader = function(encryptData, compress = false) {
    var head = Buffer.alloc(HEADER_MSG_LENGTH_SIZE);
    head.fill(0);
    var len = compress ? encryptData.length + 5 : encryptData.length;

    head.writeUIntLE(len, 0, HEADER_MSG_LENGTH_SIZE);
    if (compress) {
      var buffZipTag = Buffer.from('<ZIP>');
      return Buffer.concat([head, buffZipTag, encryptData]);
    }
    return Buffer.concat([head, encryptData]);
  };

  var aesEncryptData = function(data) {
    const cipher = crypto.createCipheriv(
      'aes-128-ecb',
      Buffer.from(CRYPT_KEY).toString('binary'),
      ''
    );
    cipher.setAutoPadding(false);
    var paddedBuffer = pad0(data, 16);
    var encrypted1 = cipher.update(paddedBuffer);
    var encrypted2 = cipher.final();
    var encrypted = Buffer.concat([encrypted1, encrypted2]).toString('hex');
    return Buffer.from(encrypted);
  };

  var aesDecryptData = function(data) {
    var decipher = crypto.createDecipheriv(
      'aes-128-ecb',
      Buffer.from(CRYPT_KEY).toString('binary'),
      ''
    );
    decipher.setAutoPadding(false); // use 0 padding, not default

    var buffer = data.toString();
    // decrypt and add new header
    var decrypted = decipher.update(buffer, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return Buffer.from(decrypted);
  };

  function pad0(buff, size) {
    var tmpBuff = Buffer.from(buff);
    var mod = tmpBuff.length % size;
    if (mod > 0) {
      var pad = Buffer.alloc(size - mod);
      pad.fill(0);
      return Buffer.concat([tmpBuff, pad]);
    }
    return tmpBuff;
  }

  function str2ab(str) {
    var buf = new ArrayBuffer(str.length);
    var bufView = new Uint8Array(buf);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
      bufView[i] = str.charCodeAt(i);
    }
    return buf;
  }

  return {
    aesEncryptData: aesEncryptData,
    aesDecryptData: aesDecryptData,
    addHeader: addHeader
  };
}

export default TCPConnection;
