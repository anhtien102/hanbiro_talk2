import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from './actions';

import React, { Component } from 'react';
import { Menu } from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import { ipcRenderer, remote, desktopCapturer } from 'electron';
import * as constantsApp from './configs/constant';
import orgWorker from './core/worker/orgtree';
import roomListWorker from './core/worker/roomlist';
import downloadUploadManagerExtra from './core/service/filemanager/download-upload-manager-extra';
import OrgDialog from './components/OrgDialog';
import ProfileDialog from './components/ProfileDialog';
import RoomUserChoiceDialog from './components/RoomUserChoiceDialog';
import HistoryDialog from './components/HistoryDialog';
import ShareScreenDialog from './components/ShareScreenDialog';
import ConfirmCloseDialog from './components/ConfirmDialog';
import { withTranslation } from 'react-i18next';
import {
  remoteAskMediaAccess,
  remoteGetMediaAccessStatus,
  openLinkWithURL
} from './utils/electron.utils';
import talkApi from './core/service/talk.api.render';

const MAX_FRAME_RATE = 10; //25

import ChatWindowContainer from './containers/ChatWindowContainer';

import VideoContainer from './janus_client/src/app';
import withStyles from './video_styles';

const disableVideo = false;

export const MenuOptionContext = React.createContext({
  onOpen: () => {},
  onClose: () => {},
  onOpenProfile: item => {},
  onOpenCreateGroupDialog: item => {},
  onOpenOrgDialog: () => {}
});

export const OrgContext = React.createContext({
  onOpen: () => {},
  onOpenProfile: item => {},
  onOpenRoomUserChoiceDialog: dataItem => {},
  onOpenSearchHistoryDialog: roomKey => {}
});

let closeWindow = false;
let callbackScreenShare = null;
class VideoRoomPage extends Component {
  constructor(props) {
    super(props);

    this.onSelectedUserOrg = null;
    this.screenChoose = null;
    this.delayShowDisconnect = null;

    this.state = {
      isShowChat: true,
      isFullScreen: false,
      sources: [],
      showOrgDialog: false,
      notAllowDisabledSelectedUser: false,
      changeOrgDialogTitleUpdate: false,
      showScreenShare: false,
      historyDialogData: {
        show: false,
        item: null
      },
      profileDialogData: {
        show: false,
        item: null
      },
      roomUserDialogData: {
        show: false,
        data: null
      },
      confirmCloseData: {
        show: false
      },
      openSnackbar: false,
      errorSnackbar: '',
      mouseY: 0,
      mouseX: 0,
      childrenOptionMenu: [],
      listSelectedUser: [],
      maximumOrg: 50,
      roomData: null
    };
  }

  haveWebRTCInformation() {
    const webRTCInfo = talkApi.webrtcInformation();
    return (
      webRTCInfo && webRTCInfo.rtcserver != null && webRTCInfo.rtcserver != ''
    );
  }

  ensureWebRTCInformation() {
    const webRTCInfo = talkApi.webrtcInformation();
    return {
      useWebSocket: webRTCInfo.method == 'https' ? false : true,
      serverRTC: webRTCInfo.rtcserver,
      httpPortRTC: webRTCInfo.httpsport,
      wsPortRTC: webRTCInfo.wssport,
      restPort: webRTCInfo.api,
      restProtocol: 'https',
      platform: 'electron',
      version: webRTCInfo.version
    };
  }

  handleClosed() {
    this.setState({
      openSnackbar: false
    });
  }

  showAlert(msg) {
    this.setState({
      errorSnackbar: msg,
      openSnackbar: true
    });
  }

  isCurrentWindowFocused() {
    return (
      remote.getCurrentWindow().isFocused() &&
      remote.getCurrentWindow().isVisible() &&
      !remote.getCurrentWindow().isMinimized()
    );
  }

  updateVideoConferenceData(data) {
    const r = ipcRenderer.sendSync(
      constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
      constantsApp.ACTION_SYNC_UPDATE_VIDEO_CONFERENCE_DATA,
      data
    );
  }

  componentDidMount() {
    const { actions } = this.props;

    // window.addEventListener('beforeunload', evt => {
    //   if (closeWindow) return;
    //   evt.returnValue = false;
    // For prevent close windows
    // });

    document.title = 'Video Conference';

    const urlParams = new URLSearchParams(global.location.search);
    const roomKey = urlParams.get('roomkey');
    const audio = urlParams.get('audio');

    const r = ipcRenderer.sendSync(
      constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
      constantsApp.ACTION_SYNC_GET_ROOM_BY_ROOM_KEY,
      { roomKey: roomKey }
    );
    this.setState({
      roomData: { room: r, primary_key: roomKey, users: null, audio: audio }
    });

    downloadUploadManagerExtra.initFunc();

    ipcRenderer.send(constantsApp.SEND_EVENT_LOAD_APP_SETTING, '');
    ipcRenderer.on(
      constantsApp.REPLY_EVENT_LOAD_APP_SETTING,
      (event, setting, alreadyLogin, languageMap) => {
        // update domain for API
        talkApi.transferSettingInfoFromElectronToBrowser();
        actions.requestInitAppSetting(setting, {}, alreadyLogin);
      }
    );

    // ORG
    ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ORG_TREE, '');
    ipcRenderer.on(constantsApp.REPLY_FULL_QUERY_ORG_TREE, (event, args) => {
      args.loggedUser = this.props.logged_user;
      orgWorker.getTreeOrg(args).then(result => {
        actions.saveOrgTree(
          result.main_tree,
          result.main_tree_favorite,
          result.group_cached,
          result.user_cached,
          result.listActualUserKeyFavourite
        );
        ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ROOM_LIST, '');
      });
    });

    ipcRenderer.on(
      constantsApp.REPLY_FULL_QUERY_ORG_TREE_FOR_STATE,
      (event, args) => {
        args.loggedUser = this.props.logged_user;
        orgWorker.getTreeOrg(args).then(result => {
          actions.saveOrgTree(
            result.main_tree,
            result.main_tree_favorite,
            result.group_cached,
            result.user_cached,
            result.listActualUserKeyFavourite
          );
        });
      }
    );

    //ROOM LIST
    ipcRenderer.on(constantsApp.REPLY_FULL_QUERY_ROOM_LIST, (event, args) => {
      let roomData = {
        listRoom: args,
        listUser: this.props.user_cached,
        loggedUser: this.props.logged_user,
        curRoomKey: '',
        window_visible: this.isCurrentWindowFocused()
      };
      roomListWorker.getRoomList(roomData).then(result => {
        actions.saveRoomList(result);
      });
    });

    ipcRenderer.on(constantsApp.MAIN_TO_RENDER_EVENT, (event, action, args) => {
      if (action == constantsApp.ACTION_SHOW_DISCONNECT_SOCKET) {
        this.showDisconnectSocket(args.force, args.show);
        return;
      }

      if (action == constantsApp.ACTION_CONFIRM_CLOSE) {
        this.setState({ confirmCloseData: { show: true } });
        return;
      }

      if (action == constantsApp.ACTION_EXTRA_LOGIN_CHANGED) {
        talkApi.updateAllAccountInfo();
        actions.requestUpdateExtraLoginInfo(args);
        return;
      }

      if (action == constantsApp.ACTION_ORG_CHANGED) {
        ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ORG_TREE, '');
        return;
      }

      if (action == constantsApp.ACTION_ORG_STATUS_CHANGED) {
        ipcRenderer.send(
          constantsApp.REQUEST_FULL_QUERY_ORG_TREE_FOR_STATE,
          ''
        );
        return;
      }

      if (action == constantsApp.ACTION_ORG_TIME_CARD_CHANGED) {
        actions.saveOrgTimeCard(args);
        return;
      }

      if (action == constantsApp.ACTION_ROOM_LIST_CHANGED) {
        if (args?.hasNewMessage) {
          if (Object.keys(args?.hasNewMessage).length > 0) {
            if (!this.isCurrentWindowFocused()) {
              this.setFlashBadgeIcon();
            }
          }
        }
        ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ROOM_LIST, '');
        return;
      }

      if (action == constantsApp.ACTION_GOT_FOCUSED_WINDOWS_AGAIN) {
        return;
      }

      if (action == constantsApp.ACTION_RELOAD_ROOM_AND_REMOVE) {
        const roomKey = args.roomKey;
        const { roomData } = this.state;
        if (roomKey == roomData?.room.rRoomKey) {
          ipcRenderer.send(
            constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
            constantsApp.ACTION_CLOSE_VIDEO_CONFERENCE_WINDOWS,
            {
              ignoreSendOutRoom: true
            }
          );
        }
      }

      if (action == constantsApp.ACTION_SHOW_MSG_INFO_TO_USER) {
        const msg = args;
        this.showAlert(msg);
        return;
      }

      if (action == constantsApp.ACTION_STOP_ALL_REQUEST) {
        downloadUploadManagerExtra.stopAllTask();
        return;
      }
    });
  }

  showDisconnectSocket(force, show) {
    const { actions } = this.props;
    clearTimeout(this.delayShowDisconnect);
    if (force) {
      actions.showDisconnectSocket(show);
    } else {
      this.delayShowDisconnect = setTimeout(() => {
        actions.showDisconnectSocket(show);
      }, 5000);
    }
  }

  setFlashBadgeIcon() {
    if (process.platform == 'win32') {
      remote.getCurrentWindow().flashFrame(true);
    }
  }

  componentWillUnmount() {
    clearTimeout(this.delayShowDisconnect);

    ipcRenderer.removeAllListeners(constantsApp.REPLY_EVENT_LOAD_APP_SETTING);
    ipcRenderer.removeAllListeners(constantsApp.REPLY_FULL_QUERY_ROOM_LIST);
    ipcRenderer.removeAllListeners(constantsApp.REPLY_FULL_QUERY_ORG_TREE);

    ipcRenderer.removeAllListeners(
      constantsApp.REPLY_FULL_QUERY_ORG_TREE_FOR_STATE
    );

    ipcRenderer.removeAllListeners(constantsApp.MAIN_TO_RENDER_EVENT);
    downloadUploadManagerExtra.destroy();
  }

  onOpenOrgDialog = (
    callbackSelected = () => {},
    listSelectedUser = [],
    maximumOrg = 50,
    notAllowDisabledSelectedUser,
    updateTitle = false
  ) => {
    this.setState({
      showOrgDialog: true,
      listSelectedUser,
      maximumOrg,
      notAllowDisabledSelectedUser: notAllowDisabledSelectedUser ?? false,
      changeOrgDialogTitleUpdate: updateTitle ?? false
    });
    this.onSelectedUserOrg = callbackSelected;
  };

  onCloseOrgDialog = user => {
    if (user) {
      this.onSelectedUserOrg(user);
    }
    this.setState({
      showOrgDialog: false,
      listSelectedUser: [],
      maximumOrg: 50,
      notAllowDisabledSelectedUser: false
    });
    this.onSelectedUserOrg = null;
  };

  onCloseRoomUserChoiceDialog = (selectedList, data) => {
    if (selectedList?.length > 0 && data) {
      ipcRenderer.send(
        constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
        constantsApp.ACTION_QUERY_ROOM_OR_CREATE_ROOM_IF_NEED_AND_SHARE_MESSAGE,
        { targetData: selectedList, shareMsg: data }
      );
    }
    this.setState({
      roomUserDialogData: {
        show: false,
        data: null
      }
    });
  };

  onOpenMenu = (event, children) => {
    this.setState({
      childrenOptionMenu: children,
      mouseX: event.clientX - 2,
      mouseY: event.clientY - 4
    });
  };

  onCloseMenu = () => {
    this.setState({ mouseX: 0, mouseY: 0 });
  };

  onOpenProfile = item => {
    this.setState({
      profileDialogData: {
        show: true,
        item: item
      }
    });
  };

  onOpenRoomUserChoiceDialog = dataItem => {
    this.setState({
      roomUserDialogData: {
        show: true,
        data: dataItem
      }
    });
  };

  onOpenSearchHistoryDialog = roomKey => {
    this.setState({
      historyDialogData: { show: true, item: roomKey }
    });
  };

  onToggleFullScreen = () => {
    this.setState({
      isFullScreen: !this.state.isFullScreen
    });
  };

  onToggleChat = () => {
    this.setState({
      isShowChat: !this.state.isShowChat
    });
  };

  onScreenShare = async (constraints, callback) => {
    callbackScreenShare = callback;
    this.setState({
      showScreenShare: true
    });
    try {
      const newSource = await desktopCapturer.getSources({
        types: ['screen', 'window']
      });

      if (process.platform == 'darwin') {
        const state = remoteGetMediaAccessStatus('screen');
        if (state != 'granted') {
          if (state == 'not-determined' || state == 'unknown') {
          } else {
            openLinkWithURL(
              `x-apple.systempreferences:com.apple.preference.security?Privacy_ScreenCapture`
            );
          }
          callback?.(null);
          return;
        }
      }

      if (newSource.length <= 0) {
        callback?.(null);
        this.setState({
          showScreenShare: false,
          sources: []
        });
        return;
      }
      this.setState({ sources: newSource });
    } catch (error) {
      console.log('error', error);
      callback?.(null);
      this.setState({
        showScreenShare: false
      });
      if (process.platform == 'darwin') {
        const state = remoteGetMediaAccessStatus('screen');
        if (state != 'granted') {
          if (state == 'not-determined' || state == 'unknown') {
          } else {
            openLinkWithURL(
              `x-apple.systempreferences:com.apple.preference.security?Privacy_ScreenCapture`
            );
          }
        }
      }
    }
  };

  render() {
    const {
      org,
      group_cached,
      user_cached,
      logged_user,
      rooms,
      classes,
      t
    } = this.props;
    const {
      openSnackbar,
      errorSnackbar,
      showOrgDialog,
      notAllowDisabledSelectedUser,
      changeOrgDialogTitleUpdate,
      mouseX,
      mouseY,
      childrenOptionMenu,
      listSelectedUser,
      maximumOrg,
      profileDialogData,
      roomUserDialogData,
      historyDialogData,
      roomData,
      showScreenShare,
      confirmCloseData,
      sources,
      isFullScreen,
      isShowChat
    } = this.state;

    const myUserKey = talkApi.shareObj?.account_info?.user_key;
    const contact = myUserKey ? user_cached[myUserKey] : null;
    var sizeUserCached = Object.keys(user_cached).length;

    const displayName = contact
      ? contact.displayName
      : talkApi.shareObj?.account_info?.user_id;

    const email = contact ? contact.userEmail : null;

    return (
      <MenuOptionContext.Provider
        value={{
          onOpen: this.onOpenMenu,
          onClose: this.onCloseMenu,
          onOpenProfile: this.onOpenProfile,
          onOpenOrgDialog: this.onOpenOrgDialog
        }}
      >
        <OrgContext.Provider
          value={{
            onOpen: this.onOpenOrgDialog,
            onOpenProfile: this.onOpenProfile,
            onOpenRoomUserChoiceDialog: this.onOpenRoomUserChoiceDialog,
            onOpenSearchHistoryDialog: this.onOpenSearchHistoryDialog
          }}
        >
          <div className={classes.main}>
            <div
              className={
                isFullScreen || !isShowChat
                  ? classes.videoLeftFullScreen
                  : classes.videoLeft
              }
            >
              {!disableVideo &&
                this.haveWebRTCInformation() &&
                roomData &&
                sizeUserCached > 0 &&
                talkApi.autoLoginKey &&
                talkApi.domain && (
                  <VideoContainer
                    t={t}
                    onCloseWindows={() => {
                      remote.getCurrentWindow().close();
                    }}
                    audio={roomData.audio}
                    talkApi={talkApi}
                    isShowChat={isShowChat}
                    onToggleChat={this.onToggleChat}
                    onToggleFullScreen={this.onToggleFullScreen}
                    isFullScreen={isFullScreen}
                    roomKey={roomData.room.rRoomKey}
                    autoLoginKey={talkApi.autoLoginKey}
                    useNewLoginKey={constantsApp.USE_NEW_LOGIN_KEY}
                    messengerLoginKey={talkApi.newAutoLoginKey}
                    domain={talkApi.domain}
                    userKey={myUserKey}
                    defaultName={displayName}
                    email={email}
                    onScreenShare={this.onScreenShare}
                    configsInformation={this.ensureWebRTCInformation()}
                    onUpdateVideoConferenceData={this.updateVideoConferenceData}
                  />
                )}
            </div>
            <div
              className={
                isFullScreen || !isShowChat
                  ? classes.videoRightFullScreen
                  : classes.videoRight
              }
            >
              {roomData ? (
                <div className={classes.chatContainerParent}>
                  <ChatWindowContainer
                    className={classes.chatContainer}
                    roomData={roomData}
                    roomKey={roomData.room.rRoomKey}
                  />
                </div>
              ) : null}
            </div>

            <Menu
              keepMounted
              open={mouseX && mouseY ? true : false}
              onClose={this.onCloseMenu}
              anchorReference="anchorPosition"
              anchorPosition={{ top: mouseY ?? 0, left: mouseX ?? 0 }}
            >
              {childrenOptionMenu}
            </Menu>

            {historyDialogData.show && (
              <HistoryDialog
                show={true}
                roomKey={historyDialogData.item}
                onClose={() =>
                  this.setState({
                    historyDialogData: { show: false, item: null }
                  })
                }
              />
            )}

            {profileDialogData.show && (
              <ProfileDialog
                show={true}
                item={profileDialogData.item}
                onClose={() =>
                  this.setState({
                    profileDialogData: {
                      show: false,
                      item: null
                    }
                  })
                }
              />
            )}

            {roomUserDialogData.show && (
              <RoomUserChoiceDialog
                rooms={rooms}
                org={org}
                groupCached={group_cached}
                user_cached={user_cached}
                user={logged_user?.account_info}
                loggedUser={logged_user}
                dataObj={roomUserDialogData.data}
                open={true}
                maximum={1}
                onSelect={this.onCloseRoomUserChoiceDialog}
                onClose={this.onCloseRoomUserChoiceDialog}
              />
            )}
            {showOrgDialog && (
              <OrgDialog
                org={org}
                groupCached={group_cached}
                loggedUser={logged_user}
                listSelectedUser={listSelectedUser}
                open={true}
                changeOrgDialogTitleUpdate={changeOrgDialogTitleUpdate}
                notAllowDisableSelected={notAllowDisabledSelectedUser}
                maximum={maximumOrg}
                onSelect={this.onCloseOrgDialog}
                onClose={this.onCloseOrgDialog}
              />
            )}
            {showScreenShare && (
              <ShareScreenDialog
                show={true}
                sources={sources}
                onClose={() => {
                  this.setState({ showScreenShare: false });
                  callbackScreenShare?.(null);
                }}
                onSelect={async selected => {
                  this.setState({ showScreenShare: false });
                  const stream = await navigator.mediaDevices.getUserMedia({
                    audio: false,
                    video: {
                      mandatory: {
                        chromeMediaSource: 'desktop',
                        chromeMediaSourceId: selected?.id,
                        maxWidth: screen.availWidth,
                        maxHeight: screen.availHeight,
                        maxFrameRate: MAX_FRAME_RATE
                      }
                    }
                  });
                  // https://stackoverflow.com/questions/56204917/javascript-navigator-mediadevices-getusermedia-moves-source-to-front
                  callbackScreenShare?.(stream);
                }}
              />
            )}
            {confirmCloseData.show && (
              <ConfirmCloseDialog
                show={true}
                onClose={() => {
                  this.setState({ confirmCloseData: { show: false } });
                }}
                onExit={() => {
                  ipcRenderer.send(
                    constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
                    constantsApp.ACTION_CLOSE_VIDEO_CONFERENCE_WINDOWS,
                    {
                      ignoreSendOutRoom: false
                    }
                  );
                  this.setState({ confirmCloseData: { show: false } });
                }}
              />
            )}
            <Snackbar
              autoHideDuration={2000}
              anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
              open={openSnackbar}
              onClose={() => this.handleClosed()}
              message={errorSnackbar}
            />
          </div>
        </OrgContext.Provider>
      </MenuOptionContext.Provider>
    );
  }
}

function mapStateToProps(state) {
  return {
    logged_user: state.auth.user,
    group_cached: state.company.group_cached,
    org: state.company.org,
    rooms: state.room_list.rooms,
    room_cached: state.room_list.room_cached,
    user_cached: state.company.user_cached,
    room_info: state.message_list.roomInfo
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation()(withStyles(VideoRoomPage)));
