import React, { useState, useEffect, Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { remote } from 'electron';
import { ipcRenderer } from 'electron';
import * as constantsApp from '../../configs/constant';
import {
  FormControlLabel,
  Grid,
  Snackbar,
  CircularProgress,
  Container
} from '@material-ui/core';
import { useStyles } from './styles';
import { useTheme } from '@material-ui/core/styles';
import {
  CssTextFieldStyleDL,
  CSSCheckBoxStyleDL,
  CSSButtonStyleDL
} from './component.styles';
import * as utils from '../../utils';
import talkAPI from '../../core/service/talk.api.render';
import Api from '../../core/service/api';
import * as Actions from '../../actions';
import { useTranslation } from 'react-i18next';
import { encryptRsa } from '../../utils/rsa.utils.renderer';
import OTPDialog from './OTPDialog';
import GraphQL from '../../core/service/graphql';
import { gql } from '@apollo/client';

const NUMBER_WAIT_LOGIN = 180;
var waitTimerLoginInterval = null;
var waitLoginValue = NUMBER_WAIT_LOGIN;

export default function Login() {
  const [domain, setDomain] = useState('');
  const [userid, setID] = useState('');
  const [password, setPassWord] = useState('');
  const [autologin, setAutoLogin] = useState(false);
  const [mobilelogin, setMobileLogin] = useState(false);
  const [enableMobileLogin, setEnableMobileLogin] = useState(false);
  const [loading, setLoading] = useState(false);
  const [domainValid, setDomainValid] = useState(true);
  const [errorDomain, setErrorDomain] = useState('');
  const [idValid, setIdValid] = useState(true);
  const [errorID, setErrorID] = useState('');
  const [passValid, setPassValid] = useState(true);
  const [errorPassword, setErrorPassword] = useState('');
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [errorSnackbar, SetErrorSnackbar] = useState('');

  const [otpData, setOtpData] = useState({
    show: false,
    msg: null
  });

  const theme = useTheme();
  const domainRef = React.createRef();
  const userIdRef = React.createRef();
  const passwordRef = React.createRef();
  const dispatch = useDispatch();
  const extraInfo = useSelector(state => state.auth.extraInfo);
  const classes = useStyles();
  const { t } = useTranslation();

  const setLoginPageIsShow = show => {
    remote.getGlobal('ShareGlobalObject').inLoginPage = show;
    remote.getGlobal('ShareKeepValue').CURRENT_ROOM_KEY = null;
    remote.getGlobal('ShareKeepValue').IS_GOT_COOKIES = false;
  };

  const isAttempDisableAutoLogin = () => {
    return remote.getGlobal('ShareGlobalObject').attempDisableAutoLogin;
  };

  const handleClosed = () => {
    setOpenSnackbar(false);
  };

  const showAlert = msg => {
    SetErrorSnackbar(msg);
    setOpenSnackbar(true);
  };

  const loadWhisperList = () => {
    let body = new FormData();
    body.append('type', 'conversation');
    body.append('page', 1);
    body.append('limit', 20);

    Api.post(talkAPI.whisperList(), body)
      .then(respone => {
        if (respone.rows) {
          dispatch(Actions.saveWhisperList(respone.rows));
        }
      })
      .catch(error => {});
  };

  const setBadgeNumberString = badgeCount => {
    if (process.platform == 'darwin') {
      const badge = badgeCount > 0 ? `${badgeCount}` : '';
      remote.app.dock.setBadge(badge);
    } else if (process.platform == 'linux') {
      const badge = badgeCount > 0 ? badgeCount : 0;
      remote.app.badgeCount = badge;
      ipcRenderer.sendSync('talk2-update-badge', 'set', badge);
    } else if (process.platform == 'win32') {
      const badge = badgeCount > 0 ? badgeCount : 0;
      ipcRenderer.sendSync('talk2-update-badge', 'set', badge);
    }
  };

  const onChangeDomain = domain => {
    const validDomain = utils.validate.isDomain(domain);
    setDomain(validDomain.value);
    setDomainValid(validDomain.success);
    setErrorDomain('');
  };

  const onChangeID = id => {
    const validId = utils.validate.isID(id);
    setID(validId.value);
    setIdValid(validId.value);
    setErrorID('');
  };

  const onChangePass = value => {
    const validPass = utils.validate.isPass(value, false);
    setPassWord(validPass.value);
    setPassValid(validPass.success);
    setErrorPassword('');
  };

  const startWaitLogin = () => {
    // clearInterval(waitTimerLoginInterval);
    // waitLoginValue = NUMBER_WAIT_LOGIN;
    // waitTimerLoginInterval = setInterval(() => {
    //   waitLoginValue--;
    //   if (waitLoginValue <= 0) {
    //     waitLoginValue = NUMBER_WAIT_LOGIN;
    //     clearInterval(waitTimerLoginInterval);
    //   }
    //   setWaitLogin(waitLoginValue);
    // }, 1000);
  };

  const clearWaitLogin = () => {
    // clearInterval(waitTimerLoginInterval);
    // waitLoginValue = NUMBER_WAIT_LOGIN;
  };

  const doLogin = (
    domain,
    userid,
    password,
    autologin,
    mobilelogin,
    otpCode
  ) => {
    if (loading) {
      ipcRenderer.send(constantsApp.CANCEL_LOGIN_EVENT, {});
    } else {
      startWaitLogin();
      const validDomain = utils.validate.isDomain(domain);
      const validId = utils.validate.isID(userid);
      const validPass = utils.validate.isPass(password, false);

      setDomain(validDomain.value);
      setDomainValid(validDomain.success);
      setErrorDomain(t(validDomain.msg));
      setID(validId.value);
      setIdValid(validId.success);
      setErrorID(t(validId.msg));
      setPassWord(validPass.value);
      setPassValid(validPass.success);
      setErrorPassword(t(validPass.msg));

      const encryptUserId = encryptRsa(userid);
      const encryptPassword = encryptRsa(password);

      if (validDomain.success && validId.success && validPass.success) {
        setLoading(true);
        ipcRenderer.send(constantsApp.SEND_LOGIN_EVENT, {
          domain,
          userid,
          password,
          autologin,
          mobilelogin,
          encryptUserId,
          encryptPassword,
          otpCode
        });
      }
    }
  };

  const mainToRenderListener = (event, action, args) => {
    if (action == constantsApp.ACTION_SOCKET_ERROR) {
      setLoading(false);
      let msg = t('An error occurred during login. Please try again later');
      showAlert(msg);
    }
  };

  /**
   * authenticate get token for GraphQL instance
   */
  const authGraphQL = () => {
    const account_info = remote.getGlobal('ShareGlobalObject').account_info;
    const { IS_GOT_COOKIES, GROUPWARE_TOKEN } = remote.getGlobal(
      'ShareKeepValue'
    );
    if (IS_GOT_COOKIES) {
      const GET_TOKEN = gql`
        mutation create(
          $hanbiro_auth: String!
          $host: String!
          $category: String!
        ) {
          create(
            hanbiro_auth: $hanbiro_auth
            host: $host
            category: $category
          ) {
            access_token
            refresh_token
          }
        }
      `;
      GraphQL.authClient
        .mutate({
          mutation: GET_TOKEN,
          variables: {
            hanbiro_auth: GROUPWARE_TOKEN,
            host: account_info.domain,
            category: 'translate'
          }
        })
        .then(result => {
          GraphQL.setToken({
            token: result.data?.create?.access_token,
            refresh_token: result.data?.create?.refresh_token
          });
        })
        .catch(error => {});
    }
  };

  useEffect(() => {
    waitLoginValue = NUMBER_WAIT_LOGIN;
    setBadgeNumberString(0);
    setLoginPageIsShow(true);
    if (extraInfo) {
      let code = extraInfo.code;
      setTimeout(() => {
        if (code == 1) {
          showAlert(t('Your application sent unknown packet to server'));
        } else if (code == 2) {
          showAlert(t('You have already logged in with another device'));
        } else if (code == 3) {
          showAlert(t('The server restarted or an internal error occurred'));
        } else if (code == 4) {
          showAlert(t('The server restarted or an internal error occurred'));
        }
      }, 500);
    }

    ipcRenderer.on(constantsApp.MAIN_TO_RENDER_EVENT, mainToRenderListener);

    ipcRenderer.send(constantsApp.SEND_EVENT_LOAD_APP_SETTING, '');
    ipcRenderer.on(
      constantsApp.REPLY_EVENT_LOAD_APP_SETTING,
      (event, setting, alreadyLogin, languageMap) => {
        // update domain for API
        talkAPI.transferSettingInfoFromElectronToBrowser();
        dispatch(Actions.requestInitAppSetting(setting, {}, alreadyLogin));

        if (alreadyLogin) {
          dispatch(Actions.requestNavigateHomePage(setting));
          //get cookie for whisper
          Api.get(talkAPI.getCookie(), {
            hkey: setting.account_info?.web_auto_login_key
          })
            .then(response => {
              if (
                response == null ||
                (response.success && response.hanbiro_gw)
              ) {
                talkAPI.applyCookiesStatus(true, response?.hanbiro_gw);
                loadWhisperList();
                authGraphQL();
              } else {
                talkAPI.applyCookiesStatus(false);
              }
            })
            .catch(error => {
              talkAPI.applyCookiesStatus(false);
            });
        } else {
          const domain = setting.account_info.domain ?? '';
          const userid = setting.account_info.user_id ?? '';
          const autologin = setting.account_info.auto_login ?? false;
          const showMobileLogin = setting.debug?.showMobileLogin;
          const mobilelogin = showMobileLogin
            ? setting.account_info.mobile_login ?? false
            : false;
          const password = autologin ? setting.account_info.password ?? '' : '';
          setDomain(domain);
          setID(userid);
          setPassWord(password);
          setAutoLogin(autologin);
          setMobileLogin(mobilelogin);
          setEnableMobileLogin(showMobileLogin == 1);

          if (autologin && !isAttempDisableAutoLogin()) {
            doLogin(domain, userid, password, autologin, mobilelogin, null);
          } else {
            const validDomain = utils.validate.isDomain(domain);
            if (!validDomain.success) {
              domainRef && domainRef.current && domainRef.current.focus();
            } else {
              const validId = utils.validate.isID(userid);
              if (!validId.success) {
                userIdRef && userIdRef.current && userIdRef.current.focus();
              } else {
                passwordRef &&
                  passwordRef.current &&
                  passwordRef.current.focus();
              }
            }
          }
        }
      }
    );

    ipcRenderer.on(
      constantsApp.REPLY_LOGIN_EVENT,
      (event, actionName, arg, setting, extra) => {
        if (actionName == constantsApp.ACTION_LOGIN_RESPONSE) {
          clearWaitLogin();
          if (arg.code == constantsApp.SOCKET_ERROR_CODE.login_success) {
            talkAPI.transferSettingInfoFromElectronToBrowser();
            dispatch(Actions.requestNavigateHomePage(setting));
            //get cookie for whisper
            Api.get(talkAPI.getCookie(), {
              hkey: setting.account_info?.web_auto_login_key
            })
              .then(response => {
                if (
                  response == null ||
                  (response.success && response.hanbiro_gw)
                ) {
                  talkAPI.applyCookiesStatus(true, response?.hanbiro_gw);
                  loadWhisperList();
                  authGraphQL();
                } else {
                  talkAPI.applyCookiesStatus(false);
                }
              })
              .catch(error => {
                talkAPI.applyCookiesStatus(false);
              });
          } else {
            setLoading(false);
            if (arg.code == constantsApp.SOCKET_ERROR_CODE.require_otp) {
              setOtpData({ show: true, msg: arg.errorMessage });
              return;
            }

            let msg = t(
              'An error occurred during login. Please try again later'
            );
            if (arg.code == constantsApp.SOCKET_ERROR_CODE.login_failed) {
              msg = t('Cannot authenticate your account');
            } else if (arg.code == constantsApp.SOCKET_ERROR_CODE.dns_error) {
              msg = t('Cannot get messenger server configuration');
            } else if (
              arg.code == constantsApp.SOCKET_ERROR_CODE.access_control
            ) {
              msg = t('Access control - Access deny');
            } else if (arg.code == constantsApp.SOCKET_ERROR_CODE.customError) {
              msg = arg.errorMessage ?? msg;
            }
            showAlert(msg);
          }
        } else if (actionName == constantsApp.ACTION_LOGIN_USER_CANCELED) {
          clearWaitLogin();
          setLoading(false);
        }
      }
    );

    return function cleanup() {
      clearWaitLogin();
      setLoginPageIsShow(false);
      ipcRenderer.removeListener(
        constantsApp.MAIN_TO_RENDER_EVENT,
        mainToRenderListener
      );
      ipcRenderer.removeAllListeners(constantsApp.REPLY_EVENT_LOAD_APP_SETTING);
      ipcRenderer.removeAllListeners(constantsApp.REPLY_LOGIN_EVENT);
    };
  }, []);

  const showWaitLogin =
    waitLoginValue > 0 && waitLoginValue < NUMBER_WAIT_LOGIN;

  const disableControl = showWaitLogin || loading;

  const signInText =
    showWaitLogin && !loading
      ? `${t('Sign In')} (${waitLoginValue})`
      : t('Sign In');

  return (
    <div className={classes.loginContainer}>
      <Container maxWidth="xs" className={classes.center}>
        <div className={classes.paper}>
          <img
            draggable="false"
            src={
              theme.palette.type == 'light'
                ? './images/talk_icon_v2.svg'
                : './images/talk_icon_dark_v2.svg'
            }
            style={{ width: 200, marginBottom: 50 }}
          />
          <form className={classes.form} noValidate>
            <CssTextFieldStyleDL
              ref={domainRef}
              disabled={disableControl}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="domain"
              label={t('Domain')}
              name="domain"
              error={!domainValid}
              helperText={errorDomain}
              value={domain}
              onKeyPress={ev => {
                if (ev.key === 'Enter') {
                  if (userIdRef) {
                    userIdRef.current.focus();
                  }
                  ev.preventDefault();
                }
              }}
              onChange={event => {
                onChangeDomain(event.target.value);
              }}
            />
            <CssTextFieldStyleDL
              inputRef={userIdRef}
              disabled={disableControl}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="userid"
              label={t('User ID')}
              name="userid"
              error={!idValid}
              helperText={errorID}
              value={userid}
              onKeyPress={ev => {
                if (ev.key === 'Enter') {
                  if (passwordRef) {
                    passwordRef.current.focus();
                  }
                  ev.preventDefault();
                }
              }}
              onChange={event => {
                onChangeID(event.target.value);
              }}
            />
            <CssTextFieldStyleDL
              inputRef={passwordRef}
              disabled={disableControl}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label={t('Password')}
              type="password"
              id="password"
              error={!passValid}
              helperText={errorPassword}
              value={password}
              onKeyPress={ev => {
                if (ev.key === 'Enter') {
                  doLogin(
                    domain,
                    userid,
                    password,
                    autologin,
                    mobilelogin,
                    null
                  );
                  ev.preventDefault();
                }
              }}
              onChange={event => {
                onChangePass(event.target.value);
              }}
            />
            <div
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between'
              }}
            >
              <FormControlLabel
                checked={autologin}
                classes={{
                  label: classes.label
                }}
                control={
                  <CSSCheckBoxStyleDL
                    disabled={disableControl}
                    color="primary"
                    onChange={event => {
                      setAutoLogin(event.target.checked);
                    }}
                  />
                }
                label={t('Auto Login')}
              />
              {enableMobileLogin && (
                <FormControlLabel
                  classes={{
                    label: classes.label
                  }}
                  checked={mobilelogin}
                  control={
                    <CSSCheckBoxStyleDL
                      disabled={disableControl}
                      color="primary"
                      onChange={event => {
                        setMobileLogin(event.target.checked);
                      }}
                    />
                  }
                  label={t('Mobile Login')}
                />
              )}
            </div>
            <CSSButtonStyleDL
              fullWidth
              variant="contained"
              color="primary"
              disabled={showWaitLogin && !loading}
              className={classes.submit}
              onClick={() =>
                doLogin(domain, userid, password, autologin, mobilelogin, null)
              }
            >
              {loading ? (
                <Fragment>
                  {t('Logging')}
                  <Grid container justify="center" style={{ width: 26 }}>
                    <CircularProgress color="inherit" size={16} />
                  </Grid>
                </Fragment>
              ) : (
                signInText
              )}
            </CSSButtonStyleDL>
          </form>

          {otpData.show && (
            <OTPDialog
              open={true}
              msg={otpData.msg}
              onClose={() => {
                setOtpData({ show: false });
                ipcRenderer.send(constantsApp.CANCEL_LOGIN_EVENT, {});
              }}
              onConfirm={otp => {
                setOtpData({ ...otpData, msg: null });
                doLogin(domain, userid, password, autologin, mobilelogin, otp);
              }}
            />
          )}

          <Snackbar
            autoHideDuration={2000}
            anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
            open={openSnackbar}
            onClose={() => handleClosed()}
            message={errorSnackbar}
          />
        </div>
      </Container>
    </div>
  );
}
