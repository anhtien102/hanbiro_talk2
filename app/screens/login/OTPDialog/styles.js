import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    dialog: {
      width: '100%',
      flexDirection: 'column',
      display: 'flex'
    },
    contentTitle: {
      padding: 16,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      display: 'flex',
      color: 'white',
      backgroundColor:
        theme.palette.type == 'light'
          ? theme.palette.hanbiroColor.dialogTitle
          : theme.palette.divider
    },
    mainContent: {
      width: '100%',
      height: '100%',
      flexDirection: 'column',
      display: 'flex',
      borderTopColor: theme.palette.divider,
      borderBottomColor: theme.palette.divider,
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderTopStyle: 'solid',
      borderBottomStyle: 'solid',
      padding: 16
    }
  })
);
