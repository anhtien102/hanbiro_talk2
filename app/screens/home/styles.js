import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  main: {
    height: '100%',
    width: '100%',
    flexDirection: 'row',
    display: 'flex',
    overflow: 'hidden'
  },
  mainLayoutRight: {
    height: '100%',
    display: 'flex',
    flexDirection: 'row'
  },
  browserLayoutRight: {
    borderLeftColor: theme.palette.divider,
    borderLeftStyle: 'solid',
    borderLeftWidth: 1,
    height: '100%',
    width: 'calc(100% - 50px)',
    position: 'absolute',
    left: 50,
    backgroundColor:
      theme.palette.type == 'light'
        ? theme.palette.primary.main
        : theme.palette.background.paper,
    zIndex: 100
  },
  listItemIcon: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  listItem: {
    width: 50,
    height: 50,

    justifyContent: 'center'
  },
  listItemActive: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.26)'
  },
  drawer: {
    backgroundColor:
      theme.palette.type == 'light'
        ? theme.palette.primary.main
        : theme.palette.background.paper,
    display: 'flex',
    float: 'left',
    height: '100%',
    width: 50,
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  drawerItemBottom: {
    height: '100%',
    display: 'flex',
    flexWrap: 'wrap',
    alignContent: 'flex-end'
  },
  contentSplitView: {
    position: 'absolute',
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    height: '100%'
  },
  contentSplitViewAll: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%'
  },
  contentSplitViewHor: {
    position: 'absolute',
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    height: '100%'
  },
  contentTop: {
    borderColor: theme.palette.divider,
    display: 'flex'
  },
  resizerStyle: {
    backgroundColor: 'red'
  },
  logo: {},
  btnTranslate: {
    width: 32,
    height: 32,
    position: 'absolute',
    zIndex: 1500,
    backgroundColor: theme.palette.primary.main,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex'
  }
});

export default withStyles(styles);
