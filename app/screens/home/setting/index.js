import React, { useState, useEffect } from 'react';
import {
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  Typography,
  IconButton,
  InputLabel,
  MenuItem,
  FormControl,
  Select,
  Switch
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { useTheme } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';
import ClearOutlined from '@material-ui/icons/ClearOutlined';
import CheckIcon from '@material-ui/icons/Check';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import * as Utils from '../../../utils';
import * as constantsApp from '../../../configs/constant';
import { ipcRenderer } from 'electron';
import { CSSListItem } from './component.styles';
import {
  FontSupport,
  FontSizeSupport,
  ThemeSupport,
  LanguageSetting
} from '../../../configs';
import talkApi from '../../../core/service/talk.api.render';
import * as Actions from '../../../actions';
import { useTranslation } from 'react-i18next';

const isLinuxOS = process.platform == 'linux';
const isMacOS = process.platform == 'darwin';

let keepOpenAtLogin = false;
export default function Setting(props) {
  const { t, i18n } = useTranslation();
  const classes = useStyles();
  const theme = useTheme();
  const dispatch = useDispatch();
  const { onClose } = props;

  const storagePrimaryColor = useSelector(state => state.setting.primaryTheme);

  const storageCommonSetting = useSelector(state => state.setting.common);

  const [route, setRoute] = useState('theme');
  const [font, setFont] = React.useState(theme.typography.fontFamily);
  const [fontSize, setFontSize] = React.useState(theme.typography.fontSize);
  const [primaryColor, setPrimaryColor] = React.useState(storagePrimaryColor);
  const [darkmode, setDarkMode] = useState(theme.palette.type);
  const [language, setLanguage] = useState(i18n.language);

  const [openAtLogin, setOpenAtLogin] = useState(keepOpenAtLogin);

  const [commonSettings, setCommonSettings] = useState(storageCommonSetting);

  const onSave = () => {
    if (i18n.language != language) {
      //save language
      ipcRenderer.sendSync(
        constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
        constantsApp.ACTION_SYNC_SET_LANGUAGE,
        language
      );
      i18n.changeLanguage(language);
    }
    if (theme.typography.fontFamily != font) {
      dispatch(Actions.onChangeFont(font));
      Utils.saveDataToStorage(constantsApp.STORAGE_FONT, font);
    }
    if (theme.typography.fontSize != fontSize) {
      dispatch(Actions.onChangeFontSize(fontSize));
      Utils.saveDataToStorage(constantsApp.STORAGE_FONT_SIZE, fontSize);
    }
    if (
      !storagePrimaryColor?.themeId ||
      storagePrimaryColor?.themeId != primaryColor?.themeId
    ) {
      dispatch(Actions.changePrimaryTheme(primaryColor));
      Utils.saveDataToStorage(constantsApp.STORAGE_PRIMARY_THEME, primaryColor);
    }
    if (theme.palette.type != darkmode) {
      dispatch(Actions.changeDarkTheme(darkmode));
      Utils.saveDataToStorage(constantsApp.STORAGE_DARK_MODE, darkmode);
    }
    if (!isLinuxOS) {
      if (keepOpenAtLogin != openAtLogin) {
        ipcRenderer.sendSync(
          constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
          constantsApp.ACTION_SYNC_SET_OPEN_AT_LOGIN,
          openAtLogin
        );
        keepOpenAtLogin = openAtLogin;
      }
    }

    if (
      commonSettings.use_new_tab != storageCommonSetting.use_new_tab ||
      commonSettings.disable_translate != storageCommonSetting.disable_translate
    ) {
      Utils.saveDataToStorage(
        constantsApp.STORAGE_COMMON_SETTING,
        commonSettings
      );
      dispatch(Actions.changeCommonSetting(commonSettings));
    }
  };

  useEffect(() => {
    if (!isLinuxOS) {
      keepOpenAtLogin = ipcRenderer.sendSync(
        constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
        constantsApp.ACTION_SYNC_GET_OPEN_AT_LOGIN,
        null
      );
      setOpenAtLogin(keepOpenAtLogin);
    }
  }, []);

  const onChangeRoute = route => {
    setRoute(route);
  };

  const renderContent = route => {
    switch (route) {
      case 'theme':
        return (
          <div className={classes.themeContent}>
            <div className={classes.settingItem}>
              <Typography variant="subtitle2" style={{ marginRight: 16 }}>
                {t('Font Setting')}
              </Typography>
              <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel>{t('Font')}</InputLabel>
                <Select
                  value={font}
                  onChange={event => setFont(event.target.value)}
                  label={t('Font')}
                  classes={{
                    root: classes.paddingInput
                  }}
                >
                  {FontSupport.map(item => {
                    return (
                      <MenuItem key={item} value={item}>
                        <Typography variant="subtitle2">{item}</Typography>
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </div>
            <div className={classes.settingItem}>
              <Typography variant="subtitle2" style={{ marginRight: 16 }}>
                {t('Font Size')}
              </Typography>
              <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel>{t('Size')}</InputLabel>
                <Select
                  value={fontSize}
                  onChange={event => setFontSize(event.target.value)}
                  label={t('Size')}
                  classes={{
                    root: classes.paddingInput
                  }}
                >
                  {FontSizeSupport.map(item => {
                    return (
                      <MenuItem key={item} value={item}>
                        <Typography variant="subtitle2">{item}</Typography>
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </div>
            <div className={classes.settingItem}>
              <Typography variant="subtitle2" style={{ marginRight: 16 }}>
                {t('Theme Color')}
              </Typography>
              <div className={classes.lineTheme}>
                {ThemeSupport.map(item => {
                  return (
                    <IconButton
                      key={item.themeId}
                      size="small"
                      onClick={() => setPrimaryColor(item)}
                    >
                      <div
                        className={classes.iconTheme}
                        style={{
                          backgroundColor: item.primary.main
                        }}
                      >
                        {item.themeId == primaryColor?.themeId && (
                          <CheckIcon
                            fontSize="small"
                            style={{ color: 'white' }}
                          />
                        )}
                      </div>
                    </IconButton>
                  );
                })}
              </div>
            </div>
            <div className={classes.settingItem}>
              <Typography variant="subtitle2" style={{ marginRight: 16 }}>
                {t('Dark Mode')}
              </Typography>
              <Switch
                checked={darkmode == 'dark'}
                onChange={() =>
                  setDarkMode(darkmode == 'dark' ? 'light' : 'dark')
                }
                color="primary"
              />
            </div>
          </div>
        );
      case 'general':
        const allowTranslate = talkApi.allowTranslate();
        return (
          <div className={classes.themeContent}>
            <div className={classes.settingItem}>
              <Typography variant="subtitle2" style={{ marginRight: 16 }}>
                {t('Language Setting')}
              </Typography>
              <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel>{t('Language')}</InputLabel>
                <Select
                  value={language}
                  onChange={event => setLanguage(event.target.value)}
                  label={t('Language')}
                  classes={{
                    root: classes.paddingInput
                  }}
                >
                  {LanguageSetting.languageSupport.map(item => {
                    return (
                      <MenuItem key={item} value={item}>
                        <Typography variant="subtitle2">
                          {Utils.languageFromCode(item)}
                        </Typography>
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </div>
            {isLinuxOS ? null : (
              <div className={classes.settingItem}>
                <Typography variant="subtitle2" style={{ marginRight: 16 }}>
                  {t('Open at Login')}
                </Typography>
                <Switch
                  checked={openAtLogin}
                  onChange={() => setOpenAtLogin(!openAtLogin)}
                  color="primary"
                />
              </div>
            )}
            <div className={classes.settingItem}>
              <Typography variant="subtitle2" style={{ marginRight: 16 }}>
                {t('Open new Tab')}
              </Typography>
              <Switch
                checked={commonSettings?.use_new_tab}
                onChange={() =>
                  setCommonSettings({
                    ...commonSettings,
                    use_new_tab: !commonSettings?.use_new_tab
                  })
                }
                color="primary"
              />
            </div>
            {allowTranslate && (
              <div className={classes.settingItem}>
                <Typography variant="subtitle2" style={{ marginRight: 16 }}>
                  {t('Disable translate')}
                </Typography>
                <Switch
                  checked={commonSettings?.disable_translate}
                  onChange={() =>
                    setCommonSettings({
                      ...commonSettings,
                      disable_translate: !commonSettings?.disable_translate
                    })
                  }
                  color="primary"
                />
              </div>
            )}

            {isMacOS && (
              <div className={classes.settingItemCenter}>
                <Button
                  variant="outlined"
                  style={{ color: 'red' }}
                  onClick={() => {
                    ipcRenderer.sendSync(
                      constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
                      constantsApp.ACTION_SYNC_SET_RESET_CAMERA,
                      null
                    );
                  }}
                >
                  {t('Reset Camera')}
                </Button>
                <Typography
                  variant="subtitle2"
                  style={{
                    marginRight: 16,
                    marginTop: 10,
                    textAlign: 'center'
                  }}
                >
                  {t('Reset Camera Summary')}
                </Typography>
              </div>
            )}
          </div>
        );
      case 'about':
        return (
          <div className={classes.themeContent}>
            <div className={classes.settingItem}>
              <Typography
                variant="subtitle2"
                style={{ marginRight: 16, whiteSpace: 'pre-line' }}
              >
                {`
                HanbiroTalk v2.1.50 build on 25 May 2021
                Release note:

                - Fix some minor bug
            `}
              </Typography>
            </div>
          </div>
        );
      default:
        return (
          <div
            style={{
              width: '100%',
              height: '100%',
              alignItems: 'center',
              justifyContent: 'center',
              display: 'flex'
            }}
          >
            {route}
          </div>
        );
    }
  };

  return (
    <Dialog fullWidth={true} maxWidth="sm" open={true} onClose={onClose}>
      <DialogContent style={{ padding: 0 }}>
        <div className={classes.dialog}>
          <div className={classes.contentTitle}>
            <Typography variant="h6">{t('Setting')}</Typography>
            <IconButton
              style={{ color: 'white' }}
              aria-label="delete"
              size="small"
              onClick={onClose}
            >
              <ClearOutlined fontSize="small" />
            </IconButton>
          </div>
          <div className={classes.mainContent}>
            <div className={classes.leftContent}>
              <List style={{ padding: 0 }}>
                <div style={{ flexDirection: 'row', display: 'flex' }}>
                  <CSSListItem
                    button
                    onClick={() => onChangeRoute('general')}
                    selected={route == 'general'}
                  >
                    <ListItemText primary={t('General')} />
                  </CSSListItem>
                  {route == 'general' && (
                    <div className={classes.selectedLine} />
                  )}
                </div>

                <div style={{ flexDirection: 'row', display: 'flex' }}>
                  <CSSListItem
                    button
                    onClick={() => onChangeRoute('theme')}
                    selected={route == 'theme'}
                  >
                    <ListItemText primary={t('Theme')} />
                  </CSSListItem>
                  {route == 'theme' && <div className={classes.selectedLine} />}
                </div>

                <div style={{ flexDirection: 'row', display: 'flex' }}>
                  <CSSListItem
                    button
                    onClick={() => onChangeRoute('about')}
                    selected={route == 'about'}
                  >
                    <ListItemText primary={t('Information')} />
                  </CSSListItem>
                  {route == 'about' && <div className={classes.selectedLine} />}
                </div>
              </List>
            </div>
            <div className={classes.rightContent}>{renderContent(route)}</div>
          </div>
        </div>
      </DialogContent>
      <DialogActions style={{ padding: 0 }}>
        <Button
          onClick={() => {
            onClose();
          }}
        >
          {t('Close')}
        </Button>
        <Button
          onClick={() => {
            onSave();
          }}
          color="primary"
        >
          {t('Save')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

Setting.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func
};

Setting.defaultProps = {
  open: false,
  onClose: () => {}
};
