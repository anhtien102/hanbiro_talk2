import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    main: {
      display: 'flex',
      width: '100%',
      height: '100%',
      flexDirection: 'column'
    },
    content: {
      width: '100%',
      height: '100%',
      display: 'flex',
      position: 'relative',
      flex: 1
    },
    layout: {
      zIndex: 1,
      position: 'absolute',
      backgroundColor: theme.palette.background.default,
      width: '100%',
      height: '100%',
      display: 'flex'
    },
    layoutHidden: {
      zIndex: 0,
      position: 'absolute',
      backgroundColor: theme.palette.background.default,
      width: '100%',
      height: '100%',
      display: 'flex',
      flexDirection: 'column'
    },
    layoutShow: {
      zIndex: 99,
      position: 'absolute',
      backgroundColor: theme.palette.background.default,
      width: '100%',
      height: '100%',
      display: 'flex',
      flexDirection: 'column'
    },
    center: {
      width: '100%',
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex'
    },
    inputSearch: { fontSize: theme.typography.fontSize, width: '100%' },
    search: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-start',
      flexDirection: 'row',
      borderBottomColor: theme.palette.divider,
      borderBottomWidth: 1,
      borderBottomStyle: 'solid',
      padding: 4
    },
    searchIcon: {
      marginLeft: 4
    },
    searchIconHightLight: {
      marginLeft: 4,
      color: theme.palette.primary.main
    },
    clearIcon: {
      marginRight: 4
    },
    clearIconHightLight: {
      marginRight: 4,
      color: theme.palette.primary.main
    }
  })
);
