import React, {
  useState,
  useContext,
  Component,
  useRef,
  useEffect
} from 'react';
import {
  Typography,
  Popover,
  ListItem,
  ListItemText,
  ListItemIcon
} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { ipcRenderer } from 'electron';

import UserPhotoView from '../../../../../../components/UserPhoto';
import { useStyles } from './styles';
import UserGroupPhoto from '../../../../../../components/UserGroupPhoto';
import TalkIcon from '../../../../../../components/TalkIcon';

import {
  UserStatusMobileIcon,
  UserStatusPCIcon
} from '../../../../../../components/HanSVGIcon';

import {
  contactUtils,
  groupUtils
} from '../../../../../../core/model/OrgUtils';

import {
  StatusMode,
  ROOM_DETAIL_STATUS_ENTER
} from '../../../../../../core/service/talk-constants';

import searchWorker from '../../../../../../core/worker/search';

import 'simplebar';
import { OrgContext } from '../../../../../../video_room';

export default function RoomView(props) {
  const { onOpenProfile } = useContext(OrgContext);

  const classes = useStyles();
  const user = useSelector(state => state.auth.user.account_info);
  const loggedUser = useSelector(state => state.auth.user);
  const userCached = useSelector(state => state.company.user_cached);
  const groupCached = useSelector(state => state.company.group_cached);

  const data = useSelector(state => state.message_list.room);

  const [showUserList, setShowUserList] = useState(null);

  const listAllUserInRoom = useSelector(
    state => state.message_list.listUserActiveInRoom
  );

  const popover = useRef(null);

  /**
   * call when Press more action
   */
  const onPressGroupImage = event => {
    setShowUserList(event.currentTarget);
  };

  const onPressSingleImage = event => {
    let otherUser = null;
    const list = listAllUserInRoom.filter(
      item => item.userKey != user.user_key
    );

    otherUser = list.length > 0 ? list[0] : null;
    if (otherUser) {
      onOpenProfile(otherUser);
    } else {
      const myData = userCached[user.user_key];
      if (myData) {
        onOpenProfile(myData);
      } else {
        const loginData = loggedUser?.extra_login_info?.login;
        if (loginData) {
          loginData.userKey = user.user_key;
          onOpenProfile(loginData);
        }
      }
    }
  };

  useEffect(() => {
    const div = popover.current;
    if (div) {
      const pager = div.getElementsByClassName('MuiPopover-paper')[0];
      if (pager) {
        pager.style.overflow = 'hidden';
      }
    }
  }, [popover.current]);

  /**
   * return UI status icon
   * @param {*} status
   * @returns
   */
  const exportIconStatus = status => {
    switch (status) {
      case StatusMode.logout:
      case StatusMode.offline:
        return (
          <div className={classes.contentStatusSmall}>
            <div className={classes.circleOffline} />
          </div>
        );
      case StatusMode.available:
        return (
          <div className={classes.contentStatusSmall}>
            <div className={classes.circleOnline} />
          </div>
        );
      case StatusMode.away:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="idle" />
          </div>
        );
      case StatusMode.busy:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="busy" />
          </div>
        );
      case StatusMode.meeting:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="meeting" />
          </div>
        );
      case StatusMode.meal:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="meal" />
          </div>
        );
      case StatusMode.phone:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="call" />
          </div>
        );
      case StatusMode.out:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="out" />
          </div>
        );
      case StatusMode.business_trip:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="business_trip" />
          </div>
        );

      default:
        return null;
    }
  };

  if (data) {
    let otherUser;
    let userStatus;
    const showStatus = listAllUserInRoom && listAllUserInRoom.length < 3;
    if (showStatus) {
      const list = listAllUserInRoom.filter(
        item => item.userKey != user.user_key
      );
      otherUser = list.length > 0 ? list[0] : null;
      userStatus = exportIconStatus(otherUser?.userStatus);
    }

    const onPress =
      data.roomDetailds?.length >= 3 ? onPressGroupImage : onPressSingleImage;

    return (
      <div className={classes.main}>
        <div className={classes.contentUser}>
          <div style={{ width: 50, height: 50 }}>
            <UserGroupPhoto
              userList={data.roomDetailds}
              user={user}
              userStatus={userStatus}
              onPress={onPress}
            />
          </div>
          {onPress && (
            <Popover
              ref={popover}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right'
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left'
              }}
              anchorEl={showUserList}
              onClick={() => setShowUserList(null)}
              open={Boolean(showUserList)}
            >
              <ContentPerfectScroll
                classes={classes}
                userCached={userCached}
                data={data}
                groupCached={groupCached}
              />
            </Popover>
          )}
          <div className={classes.nameContent}>
            <Typography variant="subtitle2" className={classes.textUser}>
              {data.displayName}
            </Typography>
            {otherUser && (
              <Typography
                variant="caption"
                color="textSecondary"
                className={classes.textStatus}
              >
                {otherUser.userNickName ?? ''}
              </Typography>
            )}
          </div>
        </div>
        <div style={{ display: 'flex', flexDirection: 'row' }}></div>
      </div>
    );
  }

  return null;
}

class ContentPerfectScroll extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullLocations: null
    };
  }

  componentDidMount() {
    const { data, userCached, groupCached } = this.props;
    const param = {
      users: data.roomDetailds,
      userCached: userCached,
      group_cached: groupCached
    };

    searchWorker.getExtraUserInfoFromList(param).then(result => {
      this.setState({
        fullLocations: result
      });
    });
  }

  render() {
    const { classes, data, userCached } = this.props;
    const { fullLocations } = this.state;

    return (
      <div
        data-simplebar
        style={{
          width: 320,
          position: 'relative',
          maxHeight: 350
        }}
      >
        <div
          style={{
            width: '100%'
          }}
        >
          {data.roomDetailds.map(element => {
            if (element.rdtStatus == ROOM_DETAIL_STATUS_ENTER) {
              if (userCached) {
                const myData = userCached[element?.rdtUKey];
                if (myData) {
                  myData.fullLocation = fullLocations
                    ? fullLocations[myData.userKey]
                    : null;

                  return (
                    <UserSearch
                      key={myData.userKey}
                      classes={classes}
                      data={myData}
                    />
                  );
                }
              }
            }
          })}
        </div>
      </div>
    );
  }
}

class UserSearch extends Component {
  constructor(props) {
    super(props);
  }

  exportIconStatus = status => {
    const { classes } = this.props;
    switch (status) {
      case StatusMode.logout:
      case StatusMode.offline:
        return <div className={classes.circleOffline} />;
      case StatusMode.available:
        return <div className={classes.circleOnline} />;
      case StatusMode.away:
        return <TalkIcon name="idle" />;
      case StatusMode.busy:
        return <TalkIcon name="busy" />;
      case StatusMode.meeting:
        return <TalkIcon name="meeting" />;
      case StatusMode.meal:
        return <TalkIcon name="meal" />;
      case StatusMode.phone:
        return <TalkIcon name="call" />;
      case StatusMode.out:
        return <TalkIcon name="out" />;
      case StatusMode.business_trip:
        return <TalkIcon name="business_trip" />;

      default:
        return null;
    }
  };

  render() {
    const { data, org_time_card, classes } = this.props;
    const isDualLogin = contactUtils.isDualLogin(data);
    const isPCLogin = contactUtils.isPCLogin(data);
    const isMobileLogin = contactUtils.isMobileLogin(data);
    const iconStatus = this.exportIconStatus(data.userStatus);
    let renderDeviceList;
    let renderHoliday;
    let renderBirthDay;

    if (org_time_card && org_time_card[data.userKey]) {
      renderHoliday = org_time_card[data.userKey].holiday == 1;
      renderBirthDay = org_time_card[data.userKey].birthDay == 1;
    }

    if (isDualLogin) {
      renderDeviceList = (
        <div className={classes.contentDevice}>
          <div className={classes.contentBadge}>
            <div className={classes.badgeDevice}>
              <UserStatusPCIcon style={{ fontSize: 12 }} />
            </div>
          </div>
          <div style={{ position: 'absolute', left: 12 }}>
            <div className={classes.contentBadge}>
              <div className={classes.badgeDeviceGreen}>
                <UserStatusMobileIcon style={{ fontSize: 12 }} />
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      if (isPCLogin) {
        renderDeviceList = (
          <div className={classes.contentDevice}>
            <div className={classes.contentBadge}>
              <div className={classes.badgeDevice}>
                <UserStatusPCIcon style={{ fontSize: 12 }} />
              </div>
            </div>
          </div>
        );
      }

      if (isMobileLogin) {
        renderDeviceList = (
          <div className={classes.contentDevice}>
            <div className={classes.contentBadge}>
              <div className={classes.badgeDevice}>
                <UserStatusMobileIcon style={{ fontSize: 12 }} />
              </div>
            </div>
          </div>
        );
      }
    }

    const detail = data.fullLocation?.result;
    const first = data.fullLocation?.first;

    return (
      <ListItem
        style={{
          paddingTop: 10,
          paddingBottom: 10,
          paddingRight: 0
        }}
        button
      >
        <ListItemIcon style={{ height: 50, paddingRight: 15 }}>
          <div style={{ flexDirection: 'row', display: 'flex' }}>
            <div className={classes.contentStatus}>
              {renderHoliday ? (
                <div style={{ marginTop: 4 }}>
                  <TalkIcon name="holiday" />
                </div>
              ) : null}
              {renderBirthDay ? (
                <div style={{ marginTop: 4 }}>
                  <TalkIcon name="birthday" />
                </div>
              ) : null}
              {iconStatus}
            </div>
            <div>
              <UserPhotoView key={data.userKey} data={data} imgSize={50} />
              {renderDeviceList}
            </div>
          </div>
        </ListItemIcon>
        <ListItemText
          primary={
            <Typography variant="subtitle2" className={classes.textName}>
              {data.displayNameWithDuty}
            </Typography>
          }
          secondary={
            <Typography
              variant="caption"
              color="textSecondary"
              className={classes.textInfor}
            >
              {detail}
              <b>{first}</b>
            </Typography>
          }
        />
      </ListItem>
    );
  }
}
