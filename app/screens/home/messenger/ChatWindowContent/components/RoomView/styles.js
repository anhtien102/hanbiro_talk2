import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    main: {
      display: 'flex',
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingLeft: 16,
      paddingRight: 16,
      borderBottom: 1,
      borderBottomColor: theme.palette.divider,
      borderBottomStyle: 'solid'
    },
    contentUser: {
      display: 'flex',
      width: '80%',
      flexDirection: 'row',
      alignItems: 'center'
    },
    nameContent: {
      display: 'table',
      paddingLeft: 8,
      tableLayout: 'fixed',
      width: '90%',
      alignItems: 'center'
    },
    textUser: {
      display: 'block',
      width: '100%',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    textStatus: {
      display: 'block',
      width: '100%',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    search: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-start',
      borderRadius: 30,
      backgroundColor: '#EAEAEA',
      '&:hover': {
        backgroundColor: 'white',
        boxShadow: '0 0 0 1px rgba(0, 0, 0, 0.2);'
      },
      margin: 8,
      flexDirection: 'row',
      width: '100%'
    },
    searchIcon: {
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      color: '#959595',
      padding: 4
    },
    contentTitleDialog: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    sizeDialog: {
      width: '500px',
      height: '600px'
    },
    contentDialog: {
      width: '100%',
      height: '100%',
      flexDirection: 'row',
      display: 'flex'
    },
    contentDialogLeft: {
      width: '50%',
      height: '100%',
      borderWidth: 1,
      borderColor: theme.palette.divider,
      borderStyle: 'solid',
      display: 'flex'
    },
    contentDialogRight: {
      width: '50%',
      height: '100%',
      borderWidth: 1,
      borderColor: theme.palette.divider,
      borderStyle: 'solid',
      display: 'flex',
      overflow: 'scroll',
      flexDirection: 'column'
    },
    contentButtonDialog: {
      flexDirection: 'row',
      justifyContent: 'flex-end',
      display: 'flex',
      width: '100%',
      paddingRight: 16,
      backgroundColor: 'white'
    },
    textName: {
      display: 'block',
      maxWidth: 'calc(100% - 20px)',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    textInfor: {
      display: 'block',
      maxWidth: 'calc(100% - 20px)',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    listItem: {
      flexDirection: 'row',
      display: 'flex',
      alignItems: 'center',
      padding: 4
    },
    circleOffline: {
      backgroundColor: '#cbced1',
      width: 10,
      height: 10,
      borderRadius: '50%'
    },
    circleOnline: {
      backgroundColor: theme.palette.success.light,
      width: 10,
      height: 10,
      borderRadius: '50%'
    },
    iconContent: {
      width: 30,
      height: 30,
      position: 'relative'
    },
    contentStatusSmall: {
      position: 'absolute',
      bottom: 5,
      right: -3,
      width: 14,
      height: 14,
      backgroundColor: 'white',
      borderRadius: '50%',
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex'
    },
    contentStatusLarge: {
      position: 'absolute',
      bottom: 5,
      right: -3,
      width: 18,
      height: 18,
      backgroundColor: 'white',
      borderRadius: '50%',
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex'
    },
    circleOnline: {
      backgroundColor: theme.palette.success.light,
      width: 10,
      height: 10,
      borderRadius: '50%'
      // marginTop: 3
    },
    circleOffline: {
      backgroundColor: '#cbced1',
      width: 10,
      height: 10,
      borderRadius: '50%'
      // marginTop: 3
    },
    contentStatus: {
      width: 16,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      marginRight: 4
    },
    contentDevice: {
      display: 'flex',
      position: 'relative',
      bottom: 20,
      left: 30,
      flexDirection: 'row'
    },
    contentBadge: {
      width: 18,
      height: 18,
      borderRadius: '50%'
    },
    badgeDevice: {
      backgroundColor: '#28a7e8',
      display: 'flex',
      width: 18,
      height: 18,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: '50%',
      color: 'white'
    },
    badgeDeviceGreen: {
      backgroundColor: '#94c400',
      display: 'flex',
      width: 18,
      height: 18,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: '50%',
      color: 'white'
    },
    textName: {
      display: 'block',
      maxWidth: 'calc(100% - 20px)',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    textInfor: {
      display: 'block',
      maxWidth: 'calc(100% - 20px)',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    }
  })
);
