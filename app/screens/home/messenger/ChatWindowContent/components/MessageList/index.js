import React, { Component } from 'react';
import { ipcRenderer } from 'electron';
import {
  Snackbar,
  Menu,
  Tabs,
  Tab,
  Typography,
  Box,
  Popper,
  Fade,
  Paper,
  ClickAwayListener
} from '@material-ui/core';
import SwipeableViews from 'react-swipeable-views';
import ScheduleIcon from '@material-ui/icons/Schedule';
import SentimentSatisfiedOutlinedIcon from '@material-ui/icons/SentimentSatisfiedOutlined';
import ImageOutlinedIcon from '@material-ui/icons/ImageOutlined';
import AttachFileOutlinedIcon from '@material-ui/icons/AttachFileOutlined';
import { withTranslation } from 'react-i18next';

import withStyles from './styles';

import * as constantsApp from '../../../../../../configs/constant';
import TextUtils from '../../../../../../core/utils/text.util';
import talkAPI from '../../../../../../core/service/talk.api.render';
import FileInput from '../../../../../../components/FileInput';
import * as Utils from '../../../../../../utils';
import GifAxios from '../../../../../../utils/gif.axios';
import fs from 'fs';

import { OrgContext } from '../../../../../../video_room';

import ItemMessage from './ItemMessage';

import {
  ButtonBottom,
  TextEditor,
  Typing,
  Updating,
  DropFile,
  ToastMessage
} from '../../../../../../components/MessageList/components';

import { remote } from 'electron';

import SimpleBarHanbiro from '../../../../../../components/SimpleBarHanbiro';

const PATH_IMAGE_NAME = 'images';
const PATH_IMAGE = './' + PATH_IMAGE_NAME;

const THRESH_HOLD_SCROLL_BOTTOM = 0;

function a11yPropsTicker(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`
  };
}

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`
  };
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={0}>{children}</Box>}
    </Typography>
  );
}

const initState = {
  typing: { name: null, userKey: null, show: false },
  anchorEl: null,
  tab: 0,
  tickerIndex: 0,
  emojiIndex: 0,
  beginCapture: false,
  openSnackbar: false,
  errorSnackbar: '',
  msgData: {
    userName: '',
    msgBody: ''
  },
  updating: false,
  showScrollButton: false,
  visibleContainer: false,
  visibleChatInput: false,
  contextMenuData: {
    mouseY: 0,
    mouseX: 0,
    anchorEl: null,
    childrenOptionMenu: []
  }
};

const resetState = {
  typing: { name: null, userKey: null, show: false },
  anchorEl: null,
  tab: 0,
  tickerIndex: 0,
  emojiIndex: 0,
  openSnackbar: false,
  errorSnackbar: '',
  updating: false,
  msgData: {
    userName: '',
    msgBody: ''
  },
  showScrollButton: false,
  visibleContainer: false,
  visibleChatInput: false,
  contextMenuData: {
    mouseY: 0,
    mouseX: 0,
    anchorEl: null,
    childrenOptionMenu: []
  }
};

let timerScroll = null;
let timerTyping = null;
let timerSendTyping = null;
let delayMessageGetTalk = null;

let timerShowButtonScroll = null;

class MessageList extends Component {
  constructor(props) {
    super(props);
    this.state = { ...initState };

    this.gifAxios = new GifAxios();

    this.delayScrollAuto = null;

    this.resetAllVariable = this.resetAllVariable.bind(this);
    this.resetAllVariable();

    this.showInfoToUser = this.showInfoToUser.bind(this);
    this.componentImgLoaded = this.componentImgLoaded.bind(this);
    this.onResendMessage = this.onResendMessage.bind(this);
    this.onDeleteMessage = this.onDeleteMessage.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.onTextAreaChange = this.onTextAreaChange.bind(this);
    this.onSendMessageClicked = this.onSendMessageClicked.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeIndex = this.handleChangeIndex.bind(this);
    this.handleChangeTicker = this.handleChangeTicker.bind(this);
    this.handleChangeIndexTicker = this.handleChangeIndexTicker.bind(this);
    this.handleChangeEmoji = this.handleChangeEmoji.bind(this);
    this.handleChangeIndexEmoji = this.handleChangeIndexEmoji.bind(this);
    this.onPressTicker = this.onPressTicker.bind(this);
    this.onPressEmoji = this.onPressEmoji.bind(this);

    this.viewProfile = this.viewProfile.bind(this);
    this.forwardMessage = this.forwardMessage.bind(this);

    this.listRef = React.createRef();

    // Handle drag chat box
    this.handleDropChat = this.handleDropChat.bind(this);
    this.handleDrop = this.handleDrop.bind(this);

    // Send files
    this.sendFiles = this.sendFiles.bind(this);
    this.chooseFile = this.chooseFile.bind(this);
    this.textBox = null;
    this.onQuote = this.onQuote.bind(this);
    this.renderTyping = this.renderTyping.bind(this);
    this.onClearQuote = this.onClearQuote.bind(this);

    this.onOpenContextMenu = this.onOpenContextMenu.bind(this);
    this.onCloseContextMenu = this.onCloseContextMenu.bind(this);

    this.myName = '';
    this.myRoomKeyTyping = '';

    this.simpleBar = null;

    this.handleWindowResize = this.handleWindowResize.bind(this);

    this.checkGetTalkMore = false;

    this.virtualNode = React.createRef();
    this.handleClickAway = this.handleClickAway.bind(this);

    this.onPhotoClicked = this.onPhotoClicked.bind(this);
  }

  /**
   * CONTROL + SHIFT + COMMAND + 4 => capture to clip board => CTR+V
   */

  onPhotoClicked(userInfo) {
    console.log(userInfo);
    this.textBox.insertText(`@${userInfo.displayName}: `);
  }

  onClearQuote() {
    this.setState({
      msgData: {
        userName: '',
        msgBody: ''
      }
    });
  }

  onQuote(msgData) {
    this.setState({
      msgData: msgData
    });
  }

  resetAllVariable() {
    if (timerScroll) {
      clearTimeout(timerScroll);
      timerScroll = null;
    }

    if (timerTyping) {
      clearTimeout(timerTyping);
      timerTyping = null;
    }

    this.sendHideTypingBeforeClose();
    this.myRoomKeyTyping = '';

    this.lastScrollHeight = 0;

    this.keepScrollBottom = true;
    this.canLoadMore = true;
    this.isGetTalk = false;

    this.checkGetTalkMore = false;
  }

  sendTypingToServer(roomKey, isTyping) {
    const { userCached, user_logged } = this.props;
    let myKey = user_logged.account_info.user_key;

    if (this.myName == null || this.myName == '') {
      const user = userCached[myKey];
      this.myName = user ? user.displayName : null;
      if (this.myName == null || this.myName == '') {
        this.myName = user_logged.extra_login_info.username;
      }
    }

    if (myKey == null || myKey == '' || roomKey == null || roomKey == '') {
      return;
    }

    ipcRenderer.send(
      constantsApp.SEND_SOCKET_API_EVENT,
      constantsApp.API_MSG_TYPING,
      {
        roomKey: roomKey,
        userKey: myKey,
        name: this.myName,
        status: isTyping ? 1 : 0
      }
    );
  }

  applyTyping(data) {
    this.setState({
      typing: {
        name: data.name,
        userKey: data.userKey,
        show: data.show
      }
    });
  }

  viewProfile(userInfo) {
    this.context.onOpenProfile(userInfo);
  }

  forwardMessage(data) {
    this.context.onOpenRoomUserChoiceDialog(data);
  }

  onCloseContextMenu() {
    this.setState({
      contextMenuData: {
        mouseY: 0,
        mouseX: 0,
        anchorEl: null,
        childrenOptionMenu: []
      }
    });
  }

  handleClickAway() {
    this.onCloseContextMenu();
  }

  onOpenContextMenu(event, menuData) {
    const { classes, t } = this.props;

    const div = this.virtualNode.current;

    this.setState({
      contextMenuData: {
        mouseX: event.clientX + 2,
        mouseY: event.clientY + 4,
        anchorEl: div,
        childrenOptionMenu: menuData
      }
    });
  }

  componentDidMount() {
    this.simpleBar = new SimpleBarHanbiro(this.listRef.current);

    this.keepScrollBottom = true;

    this.renderListener = (event, action, args) => {
      if (action == constantsApp.ACTION_MSG_GETTALK_CHANGED) {
        const { room } = this.state;
        if (args.roomKey == room.rRoomKey) {
          this.isGetTalk = false;
          this.canLoadMore = args.talkCount > 0;
          this.setState({
            updating: false
          });
          console.log('GETTALK SUCCESS', this.canLoadMore);
        }
      } else if (action == constantsApp.ACTION_RECEIVED_TYPING) {
        const { room, typing } = this.state;
        if (args.roomKey == room.rRoomKey) {
          if (args.show) {
            if (timerTyping) {
              clearTimeout(timerTyping);
              timerTyping = null;
            }
            timerTyping = setTimeout(() => {
              this.applyTyping({ name: null, userKey: null, show: false });
            }, 5000);
            this.applyTyping(args);
          } else {
            if (args.userKey == typing.userKey) {
              if (timerTyping) {
                clearTimeout(timerTyping);
                timerTyping = null;
              }
              this.applyTyping({ name: null, userKey: null, show: false });
            }
          }
        }
      }
    };

    ipcRenderer.on(constantsApp.MAIN_TO_RENDER_EVENT, this.renderListener);
    window.addEventListener('resize', this.handleWindowResize);
    window.addEventListener('splitPannelChanged', this.handleWindowResize);
  }

  handleWindowResize(e) {
    if (this.keepScrollBottom) {
      this.scrollToBottom();
    }
  }

  componentWillUnmount() {
    if (timerShowButtonScroll) {
      clearTimeout(timerShowButtonScroll);
    }

    ipcRenderer.removeListener(
      constantsApp.MAIN_TO_RENDER_EVENT,
      this.renderListener
    );

    window.removeEventListener('resize', this.handleWindowResize);
    window.removeEventListener('splitPannelChanged', this.handleWindowResize);
  }

  onPressTicker(item) {
    if (item.url) {
      this.setState({
        anchorEl: null
      });

      let filePath = talkAPI.imageResourceDirPathWithDir(item.url);
      const stats = fs.statSync(filePath);
      let file = { name: item.name, path: filePath, size: stats.size };
      this.sendFiles([file]);
      return;
    }

    this.onSendMessageClicked('', item.attributes.name);

    if (!item.attributes.ignore_recent) {
      let tickers = Utils.tickerEmojManager.tickerList;
      let ticker = tickers[0];
      let isFound = false;
      let elements = ticker.elements.filter(
        element => element.attributes.name != item.attributes.name
      );

      if (!isFound) {
        elements.unshift(item);
      }

      let tickerRecently = {
        ...ticker,
        elements: elements
      };

      Utils.saveDataToStorage(constantsApp.TICKER_RECENTLY, tickerRecently);

      tickers[0] = tickerRecently;
      Utils.tickerEmojManager.tickerList = tickers;
    }

    this.setState({
      anchorEl: null
    });
  }

  onPressEmoji(item) {
    this.textBox.insertText(item.char);
  }

  handleChangeTicker(event, newValue) {
    this.setState({ tickerIndex: newValue });
  }

  handleChangeIndexTicker(index) {
    this.setState({
      tickerIndex: index
    });
  }

  handleChangeEmoji(event, newValue) {
    this.setState({ emojiIndex: newValue });
  }

  handleChangeIndexEmoji(index) {
    this.setState({
      emojiIndex: index
    });
  }

  handleClick(event) {
    this.setState({
      anchorEl: event.currentTarget
    });
  }

  handleClose() {
    this.setState({
      anchorEl: null
    });
    this.textBox.makeFocus();
  }

  handleChange(event, newValue) {
    this.setState({
      tab: newValue
    });
  }

  handleChangeIndex(index) {
    this.setState({
      tab: index
    });
  }

  getMyUserKey() {
    const { user_logged } = this.props;
    if (user_logged && user_logged.account_info) {
      return user_logged.account_info.user_key;
    }
    return null;
  }

  static contextType = OrgContext;

  static getDerivedStateFromProps(nextProps, prevState) {
    const nextRoomData = nextProps.roomData;
    const nextRoom = nextRoomData ? nextRoomData.room : null;
    const prevRoom = prevState.room;

    if (!prevRoom || nextRoom?.rRoomKey != prevRoom.rRoomKey) {
      remote.getGlobal('ShareKeepValue').CURRENT_ROOM_KEY = nextRoom?.rRoomKey;
      return {
        ...resetState,
        room: nextRoom,
        msgBody: {
          userName: '',
          msgBody: ''
        }
      };
    }
    return null;
  }

  scrollToBottom = () => {
    this.keepScrollBottom = true;
    if (this.simpleBar) {
      this.simpleBar.scrollToBottom();
    }
  };

  onScroll(e) {
    let element = e.target;
    if (element.scrollTop == 0) {
      if (this.isGetTalk == false && this.canLoadMore) {
        const { messageData } = this.props;
        let time = messageData.roomInfo.firstTimeMessage;
        let roomKey = messageData.roomInfo.roomKey;
        if (roomKey && time > 0) {
          this.setState({ updating: true }, () => {
            this.isGetTalk = true;
            ipcRenderer.send(
              constantsApp.SEND_SOCKET_API_EVENT,
              constantsApp.API_GETTALK,
              {
                roomKey: roomKey,
                lastTime: time,
                count: constantsApp.GETTALK_COUNT,
                clientKey: ''
              }
            );
          });
        }
      }
    } else {
      const diffScrollTop = element.scrollHeight - element.clientHeight;
      const scrollAtBottom =
        diffScrollTop - element.scrollTop <= THRESH_HOLD_SCROLL_BOTTOM;

      this.keepScrollBottom = scrollAtBottom;

      if (timerShowButtonScroll) {
        clearTimeout(timerShowButtonScroll);
      }
      timerShowButtonScroll = setTimeout(() => {
        if (this.state.showScrollButton == this.keepScrollBottom) {
          this.setState({
            showScrollButton: !this.keepScrollBottom
          });
        }
      }, 500);
    }
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    // Are we adding new items to the list?
    // Capture the scroll position so we can adjust scroll later.

    if (
      prevProps.messageData.messages.length <
      this.props.messageData.messages.length
    ) {
      const prevMsg = prevProps.messageData.messages[0];
      const nextMsg = this.props.messageData.messages[0];
      if (
        prevMsg &&
        prevMsg.primaryKey != nextMsg.primaryKey &&
        this.simpleBar
      ) {
        const prev_scroll_top =
          this.simpleBar.getScrollHeight() - this.simpleBar.getScrollTop();

        return {
          prev_scroll_top: prev_scroll_top
        };
      }
    }

    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // If we have a snapshot value, we've just added new items.
    // Adjust scroll so these new items don't push the old ones out of view.
    // (snapshot here is the value returned from getSnapshotBeforeUpdate)

    if (this.keepScrollBottom) {
      this.scrollToBottom();
    } else if (snapshot !== null) {
      this.simpleBar.scrollTo(
        this.simpleBar.getScrollHeight() - snapshot.prev_scroll_top
      );
    }

    this.lastScrollHeight = this.simpleBar.getScrollHeight();

    if (!this.checkGetTalkMore) {
      const { messageData, primary_key } = this.props;
      const roomInfo = messageData?.roomInfo;
      if (
        roomInfo &&
        roomInfo.firstTimeMessage > 0 &&
        primary_key == roomInfo.roomKey
      ) {
        if (this.canLoadMore && this.simpleBar.getScrollTop() == 0) {
          this.isGetTalk = true;
          ipcRenderer.send(
            constantsApp.SEND_SOCKET_API_EVENT,
            constantsApp.API_GETTALK,
            {
              roomKey: roomInfo.roomKey,
              lastTime: roomInfo.firstTimeMessage,
              count: constantsApp.GETTALK_COUNT,
              clientKey: ''
            }
          );
        }
        this.checkGetTalkMore = true;
      }
    }
  }

  componentImgLoaded() {
    this.lastScrollHeight = this.simpleBar.getScrollHeight();
  }

  handleClosed() {
    this.setState({
      openSnackbar: false
    });
  }

  showInfoToUser(message) {
    this.setState({
      errorSnackbar: message,
      openSnackbar: true
    });
  }

  onResendMessage(message) {
    ipcRenderer.send(
      constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
      constantsApp.ACTION_RESEND_MESSAGE,
      message
    );
  }

  onDeleteMessage(message) {
    ipcRenderer.send(
      constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
      constantsApp.ACTION_DELETE_MESSAGE,
      message
    );
  }

  sendHideTyping() {
    const { messageData } = this.props;
    const roomInfo = messageData.roomInfo;
    this.myRoomKeyTyping = roomInfo.roomKey;
    this.sendTypingToServer(this.myRoomKeyTyping, 0);
    if (timerSendTyping) {
      clearTimeout(timerSendTyping);
      timerSendTyping = null;
    }
  }

  sendHideTypingBeforeClose() {
    if (timerSendTyping) {
      clearTimeout(timerSendTyping);
      timerSendTyping = null;
      this.sendTypingToServer(this.myRoomKeyTyping, 0);
    }
  }

  sendShowTyping() {
    if (timerSendTyping == null) {
      const { messageData } = this.props;
      const roomInfo = messageData.roomInfo;
      this.myRoomKeyTyping = roomInfo.roomKey;
      this.sendTypingToServer(this.myRoomKeyTyping, 1);
      timerSendTyping = setTimeout(() => {
        this.sendHideTyping();
      }, 5000);
    } else {
      if (timerSendTyping) {
        clearTimeout(timerSendTyping);
        timerSendTyping = null;
      }
      timerSendTyping = setTimeout(() => {
        this.sendHideTyping();
      }, 5000);
    }
  }

  onTextAreaChange(message) {
    if (message == '' || message == null) {
      this.sendHideTyping();
    } else {
      this.sendShowTyping();
    }
  }

  onSendMessageClicked(msgText, emojMessage = '') {
    // Common send function
    const sendData = (willSendText = '', emojMessage = '') => {
      const { messageData } = this.props;
      const roomInfo = messageData.roomInfo;
      if (
        roomInfo &&
        TextUtils.isNotEmpty(roomInfo.roomKey) &&
        roomInfo.roomID >= 0
      ) {
        if (willSendText && willSendText != '') {
          this.sendHideTyping();
        }
        let data = {
          message: willSendText ?? '',
          roomKey: roomInfo.roomKey,
          roomId: roomInfo.roomID,
          emojMessage: emojMessage ?? ''
        };
        ipcRenderer.send(
          constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
          constantsApp.ACTION_SEND_MSG_TO_SERVER,
          data
        );
        this.scrollToBottom();
      }
    };
    if (msgText) {
      if (this.state.msgData.msgBody) {
        let stringQuote = Utils.renderQuote(
          this.state.msgData.userName,
          this.state.msgData.msgBody
        );

        msgText =
          stringQuote + '\n' + '----------------------' + '\n' + msgText;
      }
      let willSendText = msgText.trim();
      if (emojMessage == null || emojMessage == '') {
        //send text msg
        willSendText = Utils.cleanInvalidXMLChars(willSendText);
        if (willSendText == null || willSendText == '') {
          return;
        }

        let testWillSendText = willSendText.replace(' ', '');
        if (!testWillSendText || testWillSendText == '') {
          return;
        }
      }
      sendData(willSendText, '');
      this.textBox.onClear();
      this.onClearQuote();
      this.setState({
        anchorEl: null
      });
    } else if (emojMessage) {
      sendData('', emojMessage);
    }
  }

  handleDrop(e) {
    const { messageData } = this.props;
    let roomKey = messageData.roomInfo.roomKey;
    if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
      if (roomKey) {
        this.sendFiles(e.dataTransfer.files);
      }
      e.dataTransfer.clearData();
    }
  }

  handleDropChat(e) {
    const { messageData } = this.props;
    let roomKey = messageData.roomInfo.roomKey;
    if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
      if (roomKey) {
        this.scrollToBottom();
        let files = [];
        for (let f of e.dataTransfer.files) {
          const item = { name: f.name, path: f.path, size: f.size };
          files.push(item);
        }
        ipcRenderer.send(
          constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
          constantsApp.ACTION_SEND_UPLOAD_FILES,
          {
            roomKey: roomKey,
            files: files
          }
        );
      }
      e.dataTransfer.clearData();
    }
  }

  chooseFile(fileData) {
    this.sendFiles(fileData);
  }

  sendFiles(fileData) {
    const { messageData } = this.props;
    let roomKey = messageData.roomInfo.roomKey;
    if (roomKey) {
      this.scrollToBottom();
      let files = [];
      for (let f of fileData) {
        let item = { name: f.name, path: f.path, size: f.size };
        files.push(item);
      }
      ipcRenderer.send(
        constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
        constantsApp.ACTION_SEND_UPLOAD_FILES,
        {
          roomKey: roomKey,
          files: files
        }
      );
    }
  }

  renderTyping() {
    const { messageData, t } = this.props;
    const { typing } = this.state;
    let roomKey = messageData.roomInfo.roomKey;
    let visible = typing.show && roomKey && roomKey != '';
    return (
      <div
        style={{
          marginLeft: 20,
          marginRight: 20,
          height: 25,
          display: 'flex',
          justifyContent: 'start',
          alignItems: 'center'
        }}
      >
        <Typing
          style={{ height: 25, marginLeft: 20, marginRight: 20 }}
          visible={visible}
          message={`${typing.name} ${t('is typing')}`}
        />
      </div>
    );
  }

  render() {
    const shareTickers = Utils.tickerEmojManager.tickerList;
    const shareEmojis = Utils.tickerEmojManager.emojList;
    const shareGifs = Utils.tickerEmojManager.gifList;

    const {
      classes,
      show_disconnect,
      userCached,
      messageData,
      primary_key,
      t
    } = this.props;

    const messageList = messageData.messages ? messageData.messages : [];
    const listUserActiveInRoom = messageData.listUserActiveInRoom
      ? messageData.listUserActiveInRoom
      : [];

    const userInRoomInfo = messageData.room
      ? messageData.room.roomDetailds
      : [];

    const {
      anchorEl,
      tab,
      tickerIndex,
      emojiIndex,
      openSnackbar,
      errorSnackbar,
      room,
      msgData,
      showScrollButton,
      updating,
      visibleContainer,
      visibleChatInput,
      contextMenuData
    } = this.state;

    return (
      <div className={classes.article}>
        <div className={classes.container}>
          <Updating visible={updating} message={t('Updating conversation')} />
          <ToastMessage
            visible={show_disconnect}
            message={t('Server is disconnected')}
          />
          <ButtonBottom
            visible={showScrollButton}
            onClick={() => {
              this.setState({
                showScrollButton: false
              });
              this.scrollToBottom();
            }}
          />
          <DropFile
            key={`${primary_key}_drop_chat`}
            contextId={'hanbiro_message_list_container'}
            onChange={visible => this.setState({ visibleContainer: visible })}
            visibleOutside={visibleChatInput}
            onSend={this.handleDrop}
          />
          <div
            className={classes.msgChatBody}
            onScroll={this.onScroll}
            ref={this.listRef}
            id="hanbiro_message_list_container"
          >
            <div>
              <MessageContainer
                key={primary_key}
                room={room}
                scrollToBottom={this.scrollToBottom}
                resetAllVariable={this.resetAllVariable}
                messageList={messageList}
                classes={classes}
                userInRoomInfo={userInRoomInfo}
                userCached={userCached}
                showInfoToUser={this.showInfoToUser}
                componentImgLoaded={this.componentImgLoaded}
                onResendMessage={this.onResendMessage}
                onDeleteMessage={this.onDeleteMessage}
                onQuote={this.onQuote}
                viewProfile={this.viewProfile}
                forwardMessage={this.forwardMessage}
                onOpenContextMenu={this.onOpenContextMenu}
                closeContextMenu={this.onCloseContextMenu}
                onPhotoClicked={this.onPhotoClicked}
              ></MessageContainer>
            </div>
          </div>
        </div>
        <div
          className={classes.msgChatInput}
          id="hanbiro_message_list_chat_input"
        >
          <DropFile
            key={`${primary_key}_drop_input`}
            contextId={'hanbiro_message_list_chat_input'}
            onChange={visible => this.setState({ visibleChatInput: visible })}
            visibleOutside={visibleContainer}
            onSend={this.handleDropChat}
          />
          <div className={classes.chatAction} style={{ display: 'flex' }}>
            <div style={{ flex: 1 }}>
              <button
                type="button"
                className={classes.chatButton}
                onClick={this.handleClick}
              >
                <SentimentSatisfiedOutlinedIcon className={classes.icon} />
              </button>
              <FileInput
                accept="image/*"
                style={{ display: 'inline' }}
                onChange={this.chooseFile}
              >
                <button type="button" className={classes.chatButton}>
                  <ImageOutlinedIcon className={classes.icon} />
                </button>
              </FileInput>
              <FileInput
                style={{ display: 'inline' }}
                onChange={this.chooseFile}
              >
                <button type="button" className={classes.chatButton}>
                  <AttachFileOutlinedIcon className={classes.icon} />
                </button>
              </FileInput>
            </div>
            <button
              type="button"
              className={classes.chatButton}
              onClick={() =>
                this.onPressTicker(Utils.tickerEmojManager.likeTicker)
              }
            >
              <img
                draggable="false"
                style={{ width: 25, height: 25, userSelect: 'none' }}
                src="./images/emoj/other/other_like.png"
              />
            </button>
          </div>
          <TextEditor
            key={messageData.room ? messageData.room.rID : ''}
            primaryKey={primary_key}
            msgData={msgData}
            users={listUserActiveInRoom}
            onChange={this.onTextAreaChange}
            onRef={ref => (this.textBox = ref)}
            onClearQuote={this.onClearQuote}
            onSend={this.onSendMessageClicked}
            onSendFiles={this.sendFiles}
          />
          {this.renderTyping()}
        </div>
        <div
          ref={this.virtualNode}
          style={{
            opacity: 0,
            position: 'fixed',
            top: contextMenuData.mouseY,
            left: contextMenuData.mouseX
          }}
        >
          t
        </div>
        <ClickAwayListener onClickAway={this.handleClickAway}>
          <Popper
            id="c-menu"
            onClose={this.onCloseContextMenu}
            anchorEl={contextMenuData.anchorEl}
            open={Boolean(contextMenuData.anchorEl)}
            placement="bottom-start"
            style={{ zIndex: 5 }}
            transition
          >
            {({ TransitionProps }) => (
              <Fade {...TransitionProps} timeout={350}>
                <Paper>
                  <Typography
                    className={classes.typography}
                    style={{ zIndex: 6 }}
                  >
                    {contextMenuData.childrenOptionMenu}
                  </Typography>
                </Paper>
              </Fade>
            )}
          </Popper>
        </ClickAwayListener>

        <Menu
          id="simple-menu"
          disableAutoFocus={true}
          disableEnforceFocus={true}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
          PaperProps={{
            style: {
              width: 340,
              height: 420,
              overflow: 'hidden',
              transform: 'translateX(5%) translateY(-210px)'
            }
          }}
          MenuListProps={{
            style: {
              padding: 0
            }
          }}
        >
          <Tabs
            value={tab}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            classes={{
              root: classes.tabsHeader
            }}
          >
            <Tab
              classes={{
                root: classes.tabHeader
              }}
              label="STICKER"
              {...a11yProps(0)}
            />
            <Tab
              classes={{
                root: classes.tabHeader
              }}
              label="EMOJI"
              {...a11yProps(1)}
            />
            <Tab
              classes={{
                root: classes.tabHeader
              }}
              label="GIF"
              {...a11yProps(2)}
            />
          </Tabs>

          <SwipeableViews
            animateTransitions={false}
            index={tab}
            onChangeIndex={this.handleChangeIndex}
          >
            <TabPanel value={tab} index={0} dir={classes.direction}>
              <SwipeableViews
                animateTransitions={false}
                index={tickerIndex}
                onChangeIndex={this.handleChangeIndexTicker}
              >
                {shareTickers.map((ticker, index) => (
                  <div key={index} className={classes.contentTicker}>
                    <TabPanel
                      value={tickerIndex}
                      index={index}
                      dir={classes.direction}
                    >
                      {ticker.elements.map((elements, index) => (
                        <img
                          key={index}
                          onClick={() => this.onPressTicker(elements)}
                          className={classes.divImg}
                          src={`${PATH_IMAGE}/${elements.attributes.file_path}`}
                          alt={elements.attributes.name}
                        />
                      ))}
                    </TabPanel>
                  </div>
                ))}
              </SwipeableViews>

              <Tabs
                value={tickerIndex}
                onChange={this.handleChangeTicker}
                variant="scrollable"
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                classes={{
                  root: classes.tabs
                }}
              >
                {shareTickers.map((ticker, index) => (
                  <Tab
                    key={index}
                    classes={{
                      root: classes.tab
                    }}
                    icon={
                      ticker.name == 'recently' ? (
                        <ScheduleIcon />
                      ) : (
                        <img
                          style={{ width: 30 }}
                          src={`${PATH_IMAGE}/${ticker.attributes.file_path}`}
                          alt={ticker.attributes.name}
                        />
                      )
                    }
                    {...a11yPropsTicker(0)}
                  />
                ))}
              </Tabs>
            </TabPanel>
            <TabPanel value={tab} index={1} dir={classes.direction}>
              <SwipeableViews
                animateTransitions={false}
                index={emojiIndex}
                onChangeIndex={this.handleChangeIndexEmoji}
              >
                {shareEmojis.map((item, index) => (
                  <div key={index} className={classes.contentTicker}>
                    <TabPanel
                      value={emojiIndex}
                      index={index}
                      dir={classes.direction}
                    >
                      <div onDragStart={() => false} onDrop={() => false}>
                        {item.elements.map((element, index) => (
                          <span
                            onClick={() => this.onPressEmoji(element)}
                            key={index}
                            className={classes.divEmoji}
                          >
                            {element.char}
                          </span>
                        ))}
                      </div>
                    </TabPanel>
                  </div>
                ))}
              </SwipeableViews>

              <Tabs
                value={emojiIndex}
                onChange={this.handleChangeEmoji}
                variant="scrollable"
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                classes={{
                  root: classes.tabs
                }}
              >
                {shareEmojis.map((item, index) => (
                  <Tab
                    key={index}
                    classes={{
                      root: classes.tab
                    }}
                    icon={
                      <div style={{ width: 30, fontSize: 18 }}>{item.icon}</div>
                    }
                    {...a11yPropsTicker(0)}
                  />
                ))}
              </Tabs>
            </TabPanel>
            <TabPanel value={tab} index={2} dir={classes.direction}>
              {shareGifs.length &&
                shareGifs[0].elements.map((gif, index) => (
                  <img
                    key={index}
                    onClick={() => this.onPressTicker(gif)}
                    src={`${PATH_IMAGE}/${gif.attributes.file_path}`}
                    alt={gif.attributes.name}
                    style={{
                      maxWidth: 105,
                      margin: 3,
                      marginBottom: 0
                    }}
                  />
                ))}
            </TabPanel>
          </SwipeableViews>
        </Menu>
        <Snackbar
          autoHideDuration={2000}
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={openSnackbar}
          onClose={() => this.handleClosed()}
          message={errorSnackbar}
        />
      </div>
    );
  }
}

export default withTranslation()(withStyles(MessageList));

class MessageContainer extends Component {
  constructor(props) {
    super(props);
    this.containerRef = React.createRef();
    this.containerSize = this.containerSize.bind(this);
    this.getUserSelection = this.getUserSelection.bind(this);
    this.handleCopy = this.handleCopy.bind(this);
  }
  componentDidMount() {
    const { room, resetAllVariable } = this.props;
    if (resetAllVariable) {
      resetAllVariable();
    }

    this.containerRef.current?.addEventListener('copy', this.handleCopy);

    const roomKey = room ? room.rRoomKey : '';
    const roomID = room ? room.rID : -1;

    talkAPI.currentFirstRoomTime = 0;

    clearTimeout(delayMessageGetTalk);

    const canUseGetTalkCache = ipcRenderer.sendSync(
      constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
      constantsApp.ACTION_SYNC_CHECK_TALK_CACHE,
      { roomKey: roomKey }
    );

    if (canUseGetTalkCache) {
      ipcRenderer.send(
        constantsApp.SEND_SOCKET_API_EVENT,
        constantsApp.API_GETTALK,
        {
          roomKey: roomKey,
          lastTime: 0,
          count: constantsApp.GETTALK_COUNT,
          clientKey: ''
        }
      );
      ipcRenderer.send(
        constantsApp.SEND_SOCKET_API_EVENT,
        constantsApp.API_ROOM_ONE,
        {
          roomKey: room.rRoomKey
        }
      );
    } else {
      setImmediate(() => {
        ipcRenderer.send(constantsApp.REQUEST_QUERY_MESSAGE_LIST_ROOM_DETAIL, {
          roomKey: roomKey,
          clientKey: '',
          firstTimeStamp: 0,
          talkCount: '0',
          roomID: roomID
        });
      });

      delayMessageGetTalk = setTimeout(() => {
        if (room && TextUtils.isNotEmpty(room.rRoomKey)) {
          ipcRenderer.send(
            constantsApp.SEND_SOCKET_API_EVENT,
            constantsApp.API_GETTALK,
            {
              roomKey: room.rRoomKey,
              lastTime: 0,
              count: constantsApp.GETTALK_COUNT,
              clientKey: ''
            }
          );

          ipcRenderer.send(
            constantsApp.SEND_SOCKET_API_EVENT,
            constantsApp.API_ROOM_ONE,
            {
              roomKey: room.rRoomKey
            }
          );
        }
      }, 1000);
    }
  }

  componentWillUnmount() {
    this.containerRef.current?.removeEventListener('copy', this.handleCopy);
    if (delayMessageGetTalk) {
      clearTimeout(delayMessageGetTalk);
      delayMessageGetTalk = null;
    }
  }

  containerSize() {
    return this.containerRef.current?.clientWidth;
  }

  /**
   * Get element bold
   */
  getUserSelection() {
    var userSelection;
    if (window.getSelection) {
      userSelection = window.getSelection();
    } else if (document.selection) {
      // should come last; Opera!
      userSelection = document.selection.createRange();
    }
    return userSelection;
  }

  handleCopy(e) {
    e.preventDefault();
    const selection = this.getUserSelection();
    event.clipboardData.setData('text/plain', selection.toString());
  }

  render() {
    const {
      classes,
      messageList,
      userInRoomInfo,
      userCached,
      showInfoToUser,
      componentImgLoaded,
      onResendMessage,
      onDeleteMessage,
      onQuote,
      viewProfile,
      forwardMessage,
      onOpenContextMenu,
      closeContextMenu,
      onPhotoClicked
    } = this.props;

    return (
      <div
        ref={this.containerRef}
        className={`${classes.viewItem} hanbiroToFadeInAndOut`}
      >
        {messageList.map((element, index) => {
          const divId = `${element.msgRoomKey}_${element.msgCreateDate}`;
          return (
            <div id={divId} key={element.primaryKey}>
              <ItemMessage
                messageData={element}
                userInRoomInfo={userInRoomInfo}
                userCached={userCached}
                showInfoToUser={showInfoToUser}
                componentImgLoaded={componentImgLoaded}
                onResendMessage={onResendMessage}
                onDeleteMessage={onDeleteMessage}
                onQuote={onQuote}
                itemIndex={index}
                containerSize={this.containerSize}
                viewProfile={viewProfile}
                forwardMessage={forwardMessage}
                onOpenContextMenu={onOpenContextMenu}
                closeContextMenu={closeContextMenu}
                onPhotoClicked={onPhotoClicked}
              />
            </div>
          );
        })}
      </div>
    );
  }
}
