import { withStyles } from '@material-ui/core/styles';
const styles = theme => ({
  msgChatBody: {
    padding: '5px 5px',
    flex: '1',
    overflow: 'auto',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    overflowX: 'hidden'
  },
  chatDate: {
    color: 'white',
    contain: 'content',
    margin: '10px 10px 16px 10px',
    fontSize: '13px',
    justifyContent: 'center',
    display: '-webkit-flex',
    display: 'flex',
    position: 'relative',
    '&::before': {
      content: "' '",
      width: '100%',
      height: '0px',
      borderTop: 1,
      borderTopStyle: 'solid',
      borderTopColor: theme.palette.divider,
      zIndex: 0,
      marginTop: '10px',
      marginLeft: '50px'
    },
    '&::after': {
      content: "' '",
      width: '100%',
      height: '0px',
      borderTop: 1,
      borderTopStyle: 'solid',
      borderTopColor: theme.palette.divider,
      zIndex: 0,
      marginTop: '10px'
    }
  },
  spanDate: {
    background: theme.palette.divider,
    fontWeight: 'bold',
    borderRadius: '10px',
    padding: '3px 10px',
    flex: '0 0 auto'
  },
  inputFocus: {
    display: 'flex',
    borderTop: 1,
    borderTopStyle: 'solid',
    borderTopColor: theme.palette.primary.main,
    backgroundColor: theme.palette.background.paper
  },
  iconPrimary: {
    color: theme.palette.primary.main
  },
  iconPrimaryMe: {
    color: 'rgba(0, 0, 0, 0.5)'
  },
  inputNotFocus: {
    display: 'flex',
    borderTop: 1,
    borderTopStyle: 'solid',
    borderTopColor: theme.palette.divider,
    backgroundColor: theme.palette.background.default
  },
  chatWrap: {
    position: 'relative',
    marginTop: '8px',
    paddingLeft: '50px'
  },
  userPic: {
    position: 'absolute',
    top: 0,
    left: 0,
    display: 'inline-block',
    overflow: 'hidden',
    width: '35px',
    height: '35px',
    borderRadius: '50%',
    textAlign: 'center'
  },
  userImg: {
    height: '35px',
    width: '35px',
    borderStyle: 'none'
  },
  chatContWrap: {
    display: 'block'
  },
  linkText: { wordBreak: 'break-word' },
  textMessageEmojOnly: {
    fontSize: 40,
    color: 'rgba(0, 0, 0, 0.87)'
  },
  textMessage: {
    color: 'rgba(0, 0, 0, 0.87)'
  },
  textMessageMe: {
    color:
      theme.palette.type == 'light'
        ? theme.palette.hanbiroColor.textMsgColorMe
        : '#ffff'
  },
  textFileName: {
    color: 'rgba(0, 0, 0, 0.87)',
    maxWidth: 200
  },
  textFileNameMe: {
    color:
      theme.palette.type == 'light'
        ? theme.palette.hanbiroColor.textMsgColorFileNameMe
        : '#ffffff',
    maxWidth: 200
  },
  descMessage: {
    color: 'rgba(0, 0, 0, 0.54)'
  },
  descMessageMe: {
    color: theme.palette.hanbiroColor.textDescMe
  },
  chatCont: {
    cursor: 'pointer',
    whiteSpace: 'pre-wrap',
    position: 'relative',
    display: 'inline-block',
    maxWidth: 'calc(100% - 100px)',
    padding: '10px',
    backgroundColor: theme.palette.hanbiroColor.backgroundOther,
    borderRadius: '10px',
    verticalAlign: 'bottom',
    textAlign: 'left',
    wordBreak: 'break-word',
    wordWrap: 'break-word'
  },
  chatContMy: {
    cursor: 'pointer',
    whiteSpace: 'pre-wrap',
    position: 'relative',
    display: 'inline-block',
    maxWidth: 'calc(100% - 100px)',
    padding: '10px',
    backgroundColor:
      theme.palette.type == 'light'
        ? theme.palette.hanbiroColor.backgroundMe
        : '#424242',
    borderRadius: '10px',
    verticalAlign: 'bottom',
    textAlign: 'left',
    wordBreak: 'break-word',
    wordWrap: 'break-word'
  },
  colorLinkMe: {
    color:
      theme.palette.type == 'light'
        ? theme.palette.hanbiroColor.textMsgLinkMe
        : '#ffffff',
    textDecorationColor: 'rgba(255,255,255,0.5)',
    userDrag: 'none'
  },
  colorLinkOther: {
    color: theme.palette.hanbiroColor.textMsgLinkOther,
    userDrag: 'none'
  },
  unreadNum: {
    display: 'inline-block',
    marginRight: '5px',
    marginLeft: '5px',
    color: theme.palette.info.light
  },
  chatFileWrap: {
    minHeight: '40px',
    position: 'relative',
    paddingLeft: '40px'
  },
  icoFile: {
    position: 'absolute',
    top: 0,
    left: 0,
    borderColor: 'rgba(255,255,255,0.5)',
    borderRadius: 8,
    borderStyle: 'solid',
    borderWidth: 2,
    width: '30px'
  },
  chatFileName: {
    whiteSpace: 'nowrap',
    maxWidth: '200px',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  chatFileSize: {
    color: theme.palette.background.default
  },
  chatFileAction: {
    marginTop: '10px',
    borderTop: '1px solid #ddd',
    paddingTop: '10px'
  },
  chatFileDlProgress: {
    position: 'relative',
    display: 'block',
    height: '5px',
    overflow: 'hidden',
    borderRadius: '10px',
    backgroundColor: theme.palette.background.paper
  },
  progressBar: {
    position: 'absolute',
    top: 0,
    left: 0,
    display: 'inline-block',
    height: '5px',
    backgroundColor: theme.palette.primary.main
  },
  imgArea: {
    width: '100%',
    display: 'block',
    textAlign: 'center',
    cursor: 'pointer'
  },
  myChat: {
    paddingLeft: 0,
    textAlign: 'right'
  },

  chatFileSizeMy: {
    opacity: 0.6
  },
  progressBarMy: {
    backgroundColor: '#f0ff00'
  },
  chatFileDlProgressMy: {
    backgroundColor: 'rgba(255,255,255,0.3)'
  },
  msgChatInput: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    flex: 'none',
    position: 'relative'
    // overflowX: 'hidden'
  },
  chatAction: {
    padding: '5px 10px',
    borderTop: 1,
    borderTopStyle: 'solid',
    borderTopColor: theme.palette.divider,
    backgroundColor: theme.palette.background.paper,
    '&::after': {
      content: "''",
      clear: 'both',
      display: 'table'
    }
  },
  chatButton: {
    marginLeft: 10,
    marginRight: 10,
    padding: '4px 0px 0px 0px',
    color: '#767676',
    background: 'none',
    border: 'none',
    borderRadius: '10px',
    cursor: 'pointer',
    outline: 'none',
    '&:hover': {
      color: theme.palette.primary.main
    }
  },
  chatButtonHideCursor: {
    marginLeft: 10,
    marginRight: 10,
    padding: '4px 0px 0px 0px',
    color: '#767676',
    background: 'none',
    border: 'none',
    borderRadius: '10px',
    cursor: 'none',
    outline: 'none',
    '&:hover': {
      color: theme.palette.primary.main
    }
  },
  icon: {
    fontSize: 20
  },
  btnRight: {
    float: 'right'
  },
  article: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    textShadow: '0px 0 0px rgba(0, 0, 0, 0.4)'
  },
  chatSend: {
    contain: 'content',
    zIndex: 3,
    display: 'flex !important',
    justifyContent: 'center',
    alignItems: 'center'
  },
  chatSendTxt: {
    fontWeight: 600,
    paddingLeft: '10px',
    paddingRight: '18px',
    fontSize: '16px',
    margintop: '-2px',
    padding: '8px',
    borderRadius: '5px',
    textAlign: 'center',
    textTransform: 'uppercase',
    color: theme.palette.primary.main,
    cursor: 'pointer',
    margin: '0px',
    userSelect: 'none'
  },
  circleAvatar: {
    borderRadius: '50%'
  },
  soundContainer: {
    position: 'relative',
    height: '28px',
    overflow: 'hidden',
    display: 'flex',
    alignItems: 'center'
  },
  soundContainerPlay: {
    width: '26px',
    height: '28px',
    borderRadius: '50%',
    textAlign: 'center',
    color: 'white',
    fontSize: '14px',
    backgroundColor: theme.palette.primary.dark,
    paddingLeft: '2px',
    position: 'relative'
  },
  soundContainerProgress: {
    height: '22px',
    margin: '0px 10px',
    display: 'flex',
    alignItems: 'flex-end'
  },
  boardContainer: {
    position: 'relative',
    height: '35px',
    overflow: 'hidden',
    display: 'flex',
    alignItems: 'center'
  },
  boardContent: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  sipContainer: {
    position: 'relative',
    overflow: 'hidden',
    display: 'flex',
    alignItems: 'center'
  },
  messageDelete: {
    color: '#6d7379'
  },
  pendingView: {
    height: '100px',
    alignItems: 'center',
    width: '116px',
    backgroundColor: theme.palette.divider,
    paddingTop: '10px',
    margin: '5px',
    borderRadius: '5px'
  },
  pedingIcon: {
    fontSize: 20,
    color: '#8c95a3',
    marginRight: 5,
    marginLeft: 5
  },
  drag: {
    margin: 5,
    borderStyle: 'dashed',
    borderWidth: 3,
    borderColor: theme.palette.primary.main,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: theme.palette.background.paper,
    position: 'absolute',
    zIndex: 4
  },
  dragDisable: {
    margin: 5,
    border: 'dashed #bdbdbd 3px',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: theme.palette.background.paper,
    position: 'absolute',
    zIndex: 4
  },
  textDragActive: {
    color: theme.palette.primary.main
  },
  textDragInActive: {
    color: '#bdbdbd'
  },
  dragContent: {
    position: 'absolute',
    top: '50%',
    right: 0,
    left: 0,
    textAlign: 'center',
    color: 'grey',
    fontSize: 30
  },
  direction: theme.direction,
  tab: {
    minWidth: 34,
    minHeight: 'auto',
    padding: '4px 12px'
  },
  tabs: {
    minHeight: 'auto'
  },
  tabsHeader: {
    minHeight: 35
  },
  tabHeader: {
    minWidth: 40,
    minHeight: 'auto',
    padding: '6px 12px',
    paddingTop: 8,
    backgroundColor: theme.palette.background.default
  },
  divImg: {
    cursor: 'pointer',
    borderRadius: 3,
    width: 67,
    padding: 10,
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.1)'
    }
  },
  contentTicker: { height: 340, width: '100%' },
  divEmoji: {
    cursor: 'pointer',
    width: 20,
    borderRadius: 3,
    padding: 5,
    fontSize: 30,
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.1)'
    },
    userSelect: 'none'
  },

  divImgError: {
    width: 200,
    borderRadius: 10,
    backgroundColor: 'rgb(229, 229, 233)',
    textAlign: 'center'
  },
  iconRetry: {
    fontSize: 30,
    color: 'rgb(174, 174, 186)',
    position: 'relative',
    top: 'calc(50% - 15px)'
  },
  divVideo: {
    width: 400,
    borderRadius: 8,
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    height: 200,
    textAlign: 'center'
  },
  iconVideo: {
    fontSize: 50,
    color: 'rgb(174, 174, 186)',
    position: 'relative',
    top: 'calc(50% - 25px)'
  },
  divFileOtherProgress: { display: 'flex', alignItems: 'center' },
  iconCloseProgress: {
    fontSize: 15,
    color: theme.palette.primary.dark,
    marginLeft: 5,
    cursor: 'pointer'
  },
  pendingViewImg: {
    height: 'auto',
    alignItems: 'center',
    backgroundColor: '#e8f1ff',
    paddingTop: '4px',
    margin: '5px',
    borderRadius: '5px',
    cursor: 'pointer',
    display: 'inline-flex'
  },
  pendingViewVideo: {
    height: 'auto',
    alignItems: 'center',
    width: '80px',
    borderRadius: '5px',
    cursor: 'pointer',
    zIndex: 4,
    position: 'absolute',
    bottom: 25,
    right: 10,
    width: 'auto',
    display: 'flex'
  },
  pedingIconVideo: {
    zIndex: 4,
    fontSize: 20,
    color: '#8c95a3',
    marginLeft: 5,
    padding: '2px',
    borderRadius: '5px',
    border: '1px solid #bdbdbd',
    backgroundColor: 'white'
  },
  img: {
    display: 'block',
    backgroundColor: 'white',
    minWidth: 100,
    maxHeight: 200,
    maxWidth: '100%',
    borderRadius: 8,
    border: `1px solid ${theme.palette.grey['400']}`
  },
  imgGif: {
    display: 'block',
    minWidth: 100,
    maxHeight: 200,
    maxWidth: '100%'
  },
  imgError: {
    display: 'block',
    minWidth: 110,
    minHeight: 110,
    alignItems: 'left',
    maxWidth: '100%',
    maxHeight: 200,
    borderRadius: 8,
    border: `1px solid ${theme.palette.grey['400']}`
  },
  viewItem: {
    width: '100%',
    padding: 16
  },
  viewItemAlpha: {
    opacity: 0,
    width: '100%',
    padding: 16
  },
  container: {
    flex: '1',
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column'
  },
  memuItem: {
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex',
    flexDirection: 'row'
  },
  quoteItemMe: {
    fontWeight: 700,
    color:
      theme.palette.type == 'light'
        ? theme.palette.hanbiroColor.quoteTitleColorMe
        : '#b08fff'
  },
  quoteItemGuest: {
    fontWeight: 700,
    color:
      theme.palette.type == 'light'
        ? theme.palette.hanbiroColor.quoteTitleColor
        : '#5256ab'
  },
  quoteItemTagMe: {
    fontWeight: 900,
    color:
      theme.palette.type == 'light'
        ? theme.palette.hanbiroColor.quoteTitleColorMyName
        : 'red'
  },
  quoteItemGuestTagMe: {
    fontWeight: 900,
    color: 'red'
  }
});

export default withStyles(styles);
