import React, { Component, Fragment } from 'react';
import withStyles from './styles';
import Linkify from 'linkifyjs/react';
import fs from 'fs';
import clsx from 'clsx';
import { ipcRenderer, remote } from 'electron';
import Skeleton from '@material-ui/lab/Skeleton';
import { messsageUtils, fileExtension } from 'core/model/MessageUtils';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import StarIcon from '@material-ui/icons/Star';
import ReplyIcon from '@material-ui/icons/Reply';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import ReplayIcon from '@material-ui/icons/Replay';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import GetAppIcon from '@material-ui/icons/GetApp';
import FolderOpenIcon from '@material-ui/icons/FolderOpen';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { Typography, ButtonBase, MenuItem } from '@material-ui/core';
import FormatQuoteIcon from '@material-ui/icons/FormatQuote';
import { withTheme } from '@material-ui/core/styles';
import moment from 'moment';
import { withTranslation } from 'react-i18next';
import VideocamOffIcon from '@material-ui/icons/VideocamOff';

import { getImg } from '../../../../../../components/BaseFile';
import {
  ForwardLeftIcon,
  ForwardRightIcon,
  PhoneCallInIcon,
  PhoneVideoCallInIcon,
  PhoneCallOutIcon,
  PhoneVideoCallOutIcon,
  PhoneCallMissedIcon,
  PhoneVideoCallMissedIcon,
  GroupCallInIcon,
  GroupVideoCallInIcon,
  GroupCallOutIcon,
  GroupVideoCallOutIcon,
  ChatBoardAlarmIcon
} from '../../../../../../components/HanSVGIcon';

import UserPhotoView from '../../../../../../components/UserPhoto';

import * as Utils from '../../../../../../utils';

import {
  remoteOpenItem,
  remoteShowItemInFolder,
  openLink
} from '../../../../../../utils/electron.utils';

import {
  resizeImageWithPathAndSave,
  dimensionNativeImageSync
} from '../../../../../../core/utils/image.process';

import talkApi from '../../../../../../core/service/talk.api.render';

import {
  keyOfMessage,
  generateThumbFileNameMessage,
  generateFileNameDownloadedMessage,
  MessageFileStatus,
  MessageSipConstant
} from '../../../../../../core/service/talk-constants';

import { ensureFilePath } from '../../../../../../configs/file_cheat';

import downloadUploadManagerExtra from '../../../../../../core/service/filemanager/download-upload-manager-extra';

import { LineStatus } from '../../../../../../components/MessageList/components';

import {
  REQUEST_COMMON_ACTION_FROM_RENDER,
  ACTION_SEND_ALL_READ_FROM_MESSAGE_VIEW,
  ACTION_REUPLOAD_FILE_MESSAGE,
  THUMBNAIL_SIZE_LOCAL_STORE,
  THUMBNAIL_SIZE_LOCAL_STORE_QUALITY,
  SEND_SOCKET_API_EVENT,
  API_CANCEL_MESSAGE,
  API_SAVE_FILE_CLOUDDISK
} from '../../../../../../configs/constant';

const ENABLE_SHARE_FILE = false;

const DOWNLOAD_CATEGORY = 'chat';

const FILE_STATUS = Utils.FILE_STATUS;

const MARGIN_MENU = -100;
const MARGIN_PHOTO_ERROR_MENU = -50;
const MARGIN_PHOTO_ERROR_MENU_1 = -70;

const DEFAULT_MAX_HEIGHT = 200;
const DEFAULT_IMAGE_HEIGHT = 250;
const PROGESS_BAR_HEIGHT = 15;
const VIDEO_HEIGHT = 200;
const VIDEO_WIDTH = 200;
const EMOTICON_HEIGHT = 100;
const DEFAULT_GIF_HEIGHT = 150;

const SKELETON_WIDTH = 200;

var image_cached = {};
let sendAllReadMsg = null;

class ItemMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      thumbnailVideo: null,
      file_status: FILE_STATUS.NORMAL,
      file_progress: -1,
      file_path_downloaded: null,
      isMenu: false
    };
    this.class = this.getClass();
    this.openFileExternalApp = this.openFileExternalApp.bind(this);
    this.renderFileInfo = this.renderFileInfo.bind(this);
    this.renderPhoto = this.renderPhoto.bind(this);
    this.renderText = this.renderText.bind(this);
    this.renderRecordFile = this.renderRecordFile.bind(this);
    this.renderBoardText = this.renderBoardText.bind(this);
    this.renderMessageDelete = this.renderMessageDelete.bind(this);
    this.renderVoiceCall = this.renderVoiceCall.bind(this);
    this.renderGroupCall = this.renderGroupCall.bind(this);
    this.renderEmoticonIcon = this.renderEmoticonIcon.bind(this);
    this.renderType = this.renderType.bind(this);
    this.renderOther = this.renderOther.bind(this);
    this.renderTCPFile = this.renderTCPFile.bind(this);
    this.renderUI = this.renderUI.bind(this);
    this.renderFileOther = this.renderFileOther.bind(this);
    this.onClickOtherFile = this.onClickOtherFile.bind(this);
    this.onClickOtherFileWithSaveDialog = this.onClickOtherFileWithSaveDialog.bind(
      this
    );
    this.exportFileToFolder = this.exportFileToFolder.bind(this);

    this.renderVideo = this.renderVideo.bind(this);
    this.getStatusFile = this.getStatusFile.bind(this);
    this.retryDownloadUpload = this.retryDownloadUpload.bind(this);
    this.stopDownloadUpload = this.stopDownloadUpload.bind(this);
    this.openFolderDownload = this.openFolderDownload.bind(this);
    this.captureImage = this.captureImage.bind(this);
    this.canvas = null;
    this.videoStream = null;
    this.isTakePhoto = true;
    this.onQuote = this.onQuote.bind(this);
    this.onForwardMessage = this.onForwardMessage.bind(this);

    this.handleWindowResize = this.handleWindowResize.bind(this);

    this.imgRef = React.createRef();
    this.videoRef = React.createRef();
    this.updateImageSize();
  }

  isTCPFileType(messageData) {
    return messsageUtils.isTCPFileAttachment(messageData);
  }

  isPhotoType(messageData) {
    return messsageUtils.isPhotoSmaller20MB(messageData);
  }

  isFileOtherType(messageData) {
    return (
      messsageUtils.isFileOther(messageData) ||
      messsageUtils.isPhotoLarger20MB(messageData)
    );
  }

  imageAreaWidth() {
    const { containerSize } = this.props;
    // return Math.round((containerSize() * 80) / 100 + 0.5) - 26;
    return Math.round(containerSize() - 100 + 0.5) - 26;
  }

  updateImageSize() {
    const { messageData, containerSize } = this.props;
    const isGif = messsageUtils.isGIF(messageData);
    let defaultImageSize = isGif ? DEFAULT_GIF_HEIGHT : DEFAULT_IMAGE_HEIGHT;
    this.height = defaultImageSize;
    this.ratio = -1;
    this.width = 0;

    if (this.isPhotoType(messageData)) {
      let cached = image_cached[messageData.msgRoomKey];
      if (!cached) {
        cached = {};
        image_cached[messageData.msgRoomKey] = cached;
      }
      let size = cached[messageData.msgNumberKey];
      if (!size) {
        size = dimensionNativeImageSync(this.getCorrectFilePath());
        if (size.height > 0) {
          cached[messageData.msgNumberKey] = size;
        }
      }

      if (size && size.width > 0 && size.height > 0) {
        this.ratio = size.height / size.width;
        this.width = size.width;
        const minHeight = Math.min(size.height, defaultImageSize);
        this.height = minHeight > 0 ? minHeight : defaultImageSize;
        if (containerSize) {
          const areaSize = this.imageAreaWidth();
          const width = Math.min(this.width, areaSize);
          this.height = width * this.ratio;
        }
      } else {
        const areaSize = this.imageAreaWidth();
        const tmpHeight = Math.min(areaSize * 0.8, defaultImageSize);
        this.height = tmpHeight > 0 ? tmpHeight : defaultImageSize;
      }
      this.height = Math.min(this.height, DEFAULT_MAX_HEIGHT);
    } else if (messsageUtils.isVideo(messageData)) {
      this.width = VIDEO_WIDTH;
      if (containerSize) {
        const isMe = messageData.isMe;
        const imageAreaWidth = this.imageAreaWidth();
        const areaSize = isMe ? imageAreaWidth - 28 : imageAreaWidth - 78; //pading video
        this.width = Math.min(this.width, areaSize);
      }
    }

    if (isNaN(this.height)) {
      this.height = defaultImageSize;
    }
  }

  handleWindowResize(e) {
    const { containerSize, messageData } = this.props;
    if (containerSize) {
      if (this.imgRef.current) {
        if (this.ratio > 0) {
          const areaSize = this.imageAreaWidth();
          const element = this.imgRef.current;
          const width = Math.min(this.width, areaSize);
          const newHeight = width * this.ratio;
          element.style.height = newHeight + 'px';
        }
      } else if (this.videoRef.current || this.videoStream) {
        const isMe = messageData.isMe;
        const imageAreaWidth = this.imageAreaWidth();
        const areaSize = isMe ? imageAreaWidth - 28 : imageAreaWidth - 78; //pading video
        const width = Math.min(this.width, areaSize);
        if (this.videoStream) {
          this.videoStream.width = width;
        } else {
          const element = this.videoRef.current;
          element.style.width = width + 'px';
        }
      }
    }
  }

  onQuote(userName, msgBody) {
    if (this.props.onQuote) {
      this.props.onQuote({
        userName: userName,
        msgBody: msgBody
      });
    }
  }

  onForwardMessage(data, isPhoto) {
    const { forwardMessage } = this.props;
    if (forwardMessage) {
      if (messsageUtils.isText(data)) {
        forwardMessage({
          data: data,
          filePath: '',
          thumbPath: '',
          isPhoto: false
        });
      } else {
        const roomKey = data.msgRoomKey;
        const msgNumberKey = data.msgNumberKey;
        const isGif = messsageUtils.isGIF(data);
        const filePath = this.getCorrectFilePath();
        const thumbPath = isGif
          ? filePath
          : talkApi.filePathThumbnailWithName(
              roomKey,
              generateThumbFileNameMessage(msgNumberKey)
            );
        forwardMessage({
          data: data,
          filePath: filePath,
          thumbPath: thumbPath,
          isPhoto: isPhoto
        });
      }
    }
  }

  sendAllReadMsgIfCan(messageData) {
    if (!messsageUtils.isReaded(messageData)) {
      if (sendAllReadMsg) {
        clearTimeout(sendAllReadMsg);
      }
      sendAllReadMsg = setTimeout(() => {
        if (
          remote.getCurrentWindow().isFocused() &&
          remote.getCurrentWindow().isVisible() &&
          !remote.getCurrentWindow().isMinimized()
        ) {
          ipcRenderer.send(
            REQUEST_COMMON_ACTION_FROM_RENDER,
            ACTION_SEND_ALL_READ_FROM_MESSAGE_VIEW,
            { roomKey: messageData.msgRoomKey }
          );
        }
      }, 3000);
    }
  }

  getCorrectFilePath() {
    const { messageData } = this.props;
    const { file_path_downloaded } = this.state;
    return (
      file_path_downloaded ??
      talkApi.filePathDownloadedWithName(
        messageData.msgRoomKey,
        generateFileNameDownloadedMessage(
          messageData.msgNumberKey,
          messageData.msgdtFileName
        )
      )
    );
  }

  async initFileStatus(messageData, callback) {
    let isUploading =
      messageData.msgdtStatus == MessageFileStatus.FILE_MSG_UPLOAD_WORKING ||
      messageData.msgdtStatus == MessageFileStatus.FILE_MSG_UPLOAD_INTERRUPT ||
      messageData.msgdtStatus == MessageFileStatus.FILE_MSG_UPLOAD_FAILED;

    let filePathPrivate = messageData.msgdtFilePath
      ? messageData.msgdtFilePath
      : talkApi.filePathDownloadedWithName(
          messageData.msgRoomKey,
          generateFileNameDownloadedMessage(
            messageData.msgNumberKey,
            messageData.msgdtFileName
          )
        );

    if (isUploading) {
      if (
        messageData.msgdtStatus ==
          MessageFileStatus.FILE_MSG_UPLOAD_INTERRUPT ||
        messageData.msgdtStatus == MessageFileStatus.FILE_MSG_UPLOAD_FAILED
      ) {
        callback({ file: filePathPrivate, state: FILE_STATUS.ERROR_UPLOAD });
        return;
      }
      callback({
        file: filePathPrivate,
        state: FILE_STATUS.IN_PROGRESS_UPLOAD
      });
      return;
    }

    if (fs.existsSync(filePathPrivate)) {
      let stats = fs.statSync(filePathPrivate);
      if (stats.size == messageData.msgdtFileSize) {
        let thumbPath = talkApi.filePathThumbnailWithName(
          messageData.msgRoomKey,
          generateThumbFileNameMessage(messageData.msgNumberKey)
        );
        if (!fs.existsSync(thumbPath)) {
          resizeImageWithPathAndSave(
            filePathPrivate,
            THUMBNAIL_SIZE_LOCAL_STORE,
            thumbPath,
            THUMBNAIL_SIZE_LOCAL_STORE_QUALITY,
            messageData.msgdtFileName,
            () => {
              callback({
                file: filePathPrivate,
                state: FILE_STATUS.DOWNLOADED
              });
            }
          );
        } else {
          callback({ file: filePathPrivate, state: FILE_STATUS.DOWNLOADED });
        }
        return;
      }
    }

    if (messageData.msgdtHasFile <= 0) {
      callback({ file: null, state: FILE_STATUS.ERROR_DOWNLOAD });
      return;
    }

    callback({ file: null, state: FILE_STATUS.NORMAL });
  }

  checkAutoDownloadUploadAndUpdateFileStatus(
    messageData,
    roomKey,
    msgNumberKey,
    finishedCallBack
  ) {
    if (messsageUtils.isFileAttachment(messageData)) {
      const keyListener = keyOfMessage(roomKey, msgNumberKey);

      this.initFileStatus(messageData, fileObj => {
        let fileState = fileObj.state;

        downloadUploadManagerExtra.addListener(
          keyListener,
          DOWNLOAD_CATEGORY,
          result => {
            if (result.key == keyListener) {
              let fileState;
              if (result.finished) {
                if (result.error) {
                  fileState = result.isDownload
                    ? FILE_STATUS.ERROR_DOWNLOAD
                    : FILE_STATUS.ERROR_UPLOAD;
                } else {
                  fileState = result.isDownload
                    ? FILE_STATUS.DOWNLOADED
                    : FILE_STATUS.UPLOADED;
                }
              } else {
                fileState = result.isDownload
                  ? FILE_STATUS.IN_PROGRESS_DOWNLOAD
                  : FILE_STATUS.IN_PROGRESS_UPLOAD;
              }

              this.setState({
                file_status: fileState,
                file_progress: result.percent,
                file_path_downloaded: result.filePathDownloaded
              });
            }
          }
        );

        if (fileState == FILE_STATUS.IN_PROGRESS_UPLOAD) {
          const prevState = downloadUploadManagerExtra.tryGetProgressUpload(
            keyListener
          );
          let percent = 0;
          if (prevState) {
            percent = prevState.percent;
          }
          this.setState({
            file_status: fileState,
            file_progress: percent,
            file_path_downloaded: fileObj.file
          });
        }

        const isNeedAutoDownloadFile =
          !this.isTCPFileType(messageData) &&
          this.isPhotoType(messageData) &&
          messageData.msgdtHasFile > 0 &&
          fileState != FILE_STATUS.DOWNLOADED &&
          fileState != FILE_STATUS.IN_PROGRESS_UPLOAD &&
          fileState != FILE_STATUS.ERROR_UPLOAD;

        if (isNeedAutoDownloadFile) {
          let lastFileState = downloadUploadManagerExtra.startDownloadIfCan(
            keyListener,
            messageData
          );
          if (lastFileState) {
            this.setState({
              file_status: lastFileState.isDownload
                ? FILE_STATUS.IN_PROGRESS_DOWNLOAD
                : FILE_STATUS.IN_PROGRESS_UPLOAD,
              file_progress: lastFileState.percent,
              file_path_downloaded: fileObj.file
            });
          }
        } else {
          let percent = 0;
          if (fileState == FILE_STATUS.IN_PROGRESS_DOWNLOAD) {
            const prevState = downloadUploadManagerExtra.tryGetProgressDownload(
              keyListener
            );
            if (prevState) {
              percent = prevState.percent;
            }
          }
          this.setState({
            file_status: fileState,
            file_progress: percent,
            file_path_downloaded: fileObj.file
          });
        }

        if (finishedCallBack) {
          finishedCallBack();
        }
      });
    } else {
      if (finishedCallBack) {
        finishedCallBack();
      }
    }
  }

  componentDidMount() {
    const { messageData, itemIndex } = this.props;

    const roomKey = messageData.msgRoomKey;
    const msgNumberKey = messageData.msgNumberKey;
    if (msgNumberKey && msgNumberKey != '') {
      this.sendAllReadMsgIfCan(messageData);
      this.checkAutoDownloadUploadAndUpdateFileStatus(
        messageData,
        roomKey,
        msgNumberKey
      );
    }
    window.addEventListener('resize', this.handleWindowResize);
    window.addEventListener('splitPannelChanged', this.handleWindowResize);
  }

  componentWillUnmount() {
    const { messageData } = this.props;
    if (sendAllReadMsg) {
      clearTimeout(sendAllReadMsg);
    }
    if (messsageUtils.isFileAttachment(messageData)) {
      const roomKey = messageData.msgRoomKey;
      const msgNumberKey = messageData.msgNumberKey;
      downloadUploadManagerExtra.removeListener(
        keyOfMessage(roomKey, msgNumberKey),
        DOWNLOAD_CATEGORY
      );
    }
    window.removeEventListener('resize', this.handleWindowResize);
    window.removeEventListener('splitPannelChanged', this.handleWindowResize);
  }

  retryDownloadUpload(e) {
    e.stopPropagation();
    const { file_status } = this.state;
    const { messageData, t } = this.props;
    if (file_status == FILE_STATUS.ERROR_DOWNLOAD) {
      if (messageData.msgdtHasFile <= 0) {
        this.showInfoToUser(t('File is deleted'));
        return;
      }
      //retry download
      const isPhoto = this.isPhotoType(messageData);
      if (isPhoto) {
        // photo store to cache
        const keyListener = keyOfMessage(
          messageData.msgRoomKey,
          messageData.msgNumberKey
        );
        const lastFileState = downloadUploadManagerExtra.startDownloadIfCan(
          keyListener,
          messageData
        );
        if (lastFileState) {
          this.setState({
            file_status: lastFileState.isDownload
              ? FILE_STATUS.IN_PROGRESS_DOWNLOAD
              : FILE_STATUS.IN_PROGRESS_UPLOAD,
            file_progress: lastFileState.percent
          });
        }
      } else {
        this.onClickOtherFileWithSaveDialog();
      }
    } else if (file_status == FILE_STATUS.ERROR_UPLOAD) {
      //retry upload
      let filePathPrivate = messageData.msgdtFilePath
        ? messageData.msgdtFilePath
        : talkApi.filePathDownloadedWithName(
            messageData.msgRoomKey,
            generateFileNameDownloadedMessage(
              messageData.msgNumberKey,
              messageData.msgdtFileName
            )
          );

      if (!fs.existsSync(filePathPrivate)) {
        this.showInfoToUser(t('File is deleted'));
        return;
      }
      const stat = fs.statSync(filePathPrivate);
      if (stat.size <= 0) {
        this.showInfoToUser('File is zero length');
        return;
      }
      const file = {
        name: messageData.msgdtFileName,
        path: filePathPrivate,
        size: stat.size
      };
      ipcRenderer.send(
        REQUEST_COMMON_ACTION_FROM_RENDER,
        ACTION_REUPLOAD_FILE_MESSAGE,
        {
          roomKey: messageData.msgRoomKey,
          msgID: messageData.msgID,
          file: file
        }
      );
    }
  }

  stopDownloadUpload(e) {
    e.stopPropagation();
    const { file_status } = this.state;
    const { messageData } = this.props;
    if (file_status == FILE_STATUS.IN_PROGRESS_DOWNLOAD) {
      const keyListener = keyOfMessage(
        messageData.msgRoomKey,
        messageData.msgNumberKey
      );
      downloadUploadManagerExtra.stopDownloadIfCan(keyListener);
    } else if (file_status == FILE_STATUS.IN_PROGRESS_UPLOAD) {
      const keyListener = keyOfMessage(
        messageData.msgRoomKey,
        messageData.msgNumberKey
      );
      downloadUploadManagerExtra.stopUploadIfCan(keyListener);
    }
  }

  openFolderDownload() {
    let newFilePath = this.getCorrectFilePath();
    remoteShowItemInFolder(newFilePath);
  }

  openFileExternalApp(filePath) {
    remoteOpenItem(filePath);
  }

  exportFileToFolder() {
    const { messageData } = this.props;
    const { dialog } = remote;
    let newFilePath = this.getCorrectFilePath();
    let fileName = messageData.msgdtFileName ?? '';
    fileName = fileName.normalize();
    if (fs.existsSync(newFilePath)) {
      dialog
        .showSaveDialog(remote.getCurrentWindow(), {
          defaultPath: fileName
        })
        .then(result => {
          if (result.canceled) {
            return;
          }

          let destPath = result.filePath;
          let extension = fileExtension(destPath);
          if (extension == null || extension == '') {
            destPath = `${destPath}.${fileExtension(fileName)}`;
          }

          fs.copyFile(newFilePath, destPath, () => {});
        });
    } else {
      if (messageData.msgdtHasFile <= 0) {
        return;
      }

      const ignoreSavePath = this.isPhotoType(messageData);

      dialog
        .showSaveDialog(remote.getCurrentWindow(), {
          defaultPath: fileName
        })
        .then(result => {
          if (result.canceled) {
            return;
          }
          const keyListener = keyOfMessage(
            messageData.msgRoomKey,
            messageData.msgNumberKey
          );

          let destPath = result.filePath;
          let extension = fileExtension(destPath);
          if (extension == null || extension == '') {
            destPath = `${destPath}.${fileExtension(fileName)}`;
          }

          const lastFileState = downloadUploadManagerExtra.startDownloadIfCan(
            keyListener,
            messageData,
            destPath,
            ignoreSavePath
          );
          if (lastFileState) {
            this.setState({
              file_status: lastFileState.isDownload
                ? FILE_STATUS.IN_PROGRESS_DOWNLOAD
                : FILE_STATUS.IN_PROGRESS_UPLOAD,
              file_progress: lastFileState.percent,
              file_path_downloaded: destPath
            });
          }
        });
    }
  }

  onClickOtherFileWithSaveDialog() {
    console.log('onClickOtherFileWithSaveDialog');
    const { messageData } = this.props;
    if (messageData.msgdtHasFile <= 0) {
      return;
    }
    const { dialog } = remote;
    let fileName = messageData.msgdtFileName ?? '';
    fileName = fileName.normalize();
    dialog
      .showSaveDialog(remote.getCurrentWindow(), {
        defaultPath: fileName
      })
      .then(result => {
        if (result.canceled) {
          return;
        }
        const keyListener = keyOfMessage(
          messageData.msgRoomKey,
          messageData.msgNumberKey
        );

        let destPath = result.filePath;
        let extension = fileExtension(destPath);
        if (extension == null || extension == '') {
          destPath = `${destPath}.${fileExtension(fileName)}`;
        }

        const lastFileState = downloadUploadManagerExtra.startDownloadIfCan(
          keyListener,
          messageData,
          destPath
        );
        if (lastFileState) {
          this.setState({
            file_status: lastFileState.isDownload
              ? FILE_STATUS.IN_PROGRESS_DOWNLOAD
              : FILE_STATUS.IN_PROGRESS_UPLOAD,
            file_progress: lastFileState.percent,
            file_path_downloaded: destPath
          });
        }
      });
  }

  showInfoToUser(msg) {
    const { showInfoToUser } = this.props;
    if (showInfoToUser) {
      showInfoToUser(msg);
    }
  }

  onClickOtherFile() {
    console.log('onClickOtherFile');
    const { messageData, t } = this.props;
    const { file_status } = this.state;
    const isDone =
      file_status == FILE_STATUS.DOWNLOADED ||
      file_status == FILE_STATUS.UPLOADED;
    const notDownload = file_status == FILE_STATUS.NORMAL;
    if (notDownload) {
      if (messageData.msgdtHasFile > 0) {
        const keyListener = keyOfMessage(
          messageData.msgRoomKey,
          messageData.msgNumberKey
        );
        const lastFileState = downloadUploadManagerExtra.startDownloadIfCan(
          keyListener,
          messageData
        );
        if (lastFileState) {
          this.setState({
            file_status: lastFileState.isDownload
              ? FILE_STATUS.IN_PROGRESS_DOWNLOAD
              : FILE_STATUS.IN_PROGRESS_UPLOAD,
            file_progress: lastFileState.percent
          });
        }
      } else {
        this.showInfoToUser(t('File is deleted'));
      }
    } else if (isDone) {
      let newFilePath = this.getCorrectFilePath();
      this.openFileExternalApp(newFilePath);
    }
  }

  getClass(isMe) {
    let { classes } = this.props;

    return {
      chatCont: isMe ? classes.chatContMy : classes.chatCont,
      chatWrap: isMe
        ? clsx(classes.chatWrap, classes.myChat)
        : classes.chatWrap,
      chatFileDlProgress: classes.chatFileDlProgress,
      progressBar: classes.progressBar,
      chatFileSize: isMe ? classes.chatFileSizeMy : classes.chatFileSize
    };
  }

  getUserInfo(userCached, userInRoomInfo = [], msgUserKey) {
    if (userCached[msgUserKey]) {
      return userCached[msgUserKey];
    } else {
      if (userInRoomInfo) {
        const found = userInRoomInfo.find(element => {
          return element.rdtUKey == msgUserKey;
        });
        if (found) {
          return {
            primaryKey: found.rdtUKey,
            userAlias: found.rdtUName,
            displayName: found.rdtUName,
            userKey: found.rdtUKey,
            userPhotoTime: -1
          };
        }
      }
      return {
        primaryKey: '',
        userAlias: '-',
        displayName: '-',
        userKey: '',
        userPhotoTime: -1
      };
    }
  }

  getEmoticon(name) {
    return Utils.tickerEmojManager.filePathFromName(name);
  }

  getEmoticonError() {
    return `./images/emoj/emoj_error.png`;
  }

  getStatusFile(statusComponent) {
    let { file_status } = this.state;
    const isDoneFile =
      file_status == FILE_STATUS.DOWNLOADED ||
      file_status == FILE_STATUS.UPLOADED;
    const isErrorFile =
      file_status == FILE_STATUS.ERROR_DOWNLOAD ||
      file_status == FILE_STATUS.ERROR_UPLOAD;
    const isProgressFile =
      file_status == FILE_STATUS.IN_PROGRESS_DOWNLOAD ||
      file_status == FILE_STATUS.IN_PROGRESS_UPLOAD;
    const isErrorDownloadFile = file_status == FILE_STATUS.ERROR_DOWNLOAD;
    const isErrorUploadFile = file_status == FILE_STATUS.ERROR_UPLOAD;
    const {
      isDone,
      isErrorDownload,
      isErrorUpload,
      isError,
      isProgress,
      isOther
    } = statusComponent;

    if (isDoneFile) {
      return isDone;
    } else if (isErrorDownloadFile && isErrorDownload) {
      return isErrorDownload;
    } else if (isErrorUploadFile && isErrorUpload) {
      return isErrorUpload;
    } else if (isErrorFile) {
      return isError;
    } else if (isProgressFile) {
      return isProgress;
    } else {
      return isOther;
    }
  }

  captureImage() {
    if (this.canvas && this.videoStream && !this.state.thumbnailVideo) {
      const context = this.canvas.getContext('2d');
      context.drawImage(this.videoStream, 0, 0, 300, 150);
      const image = this.canvas.toDataURL('image/jpeg', 0.5);
      this.setState({
        thumbnailVideo: image
      });
    }
  }

  renderFileInfo() {
    let { messageData, classes } = this.props;
    let { msgdtFileName, msgdtFileSize, isMe } = messageData;

    return (
      <Fragment>
        <Typography
          variant="subtitle2"
          className={isMe ? classes.textFileNameMe : classes.textFileName}
        >
          {msgdtFileName}
        </Typography>
        <Typography
          variant="caption"
          className={isMe ? classes.descMessageMe : classes.descMessage}
        >
          {Utils.convertFileSize(msgdtFileSize)}
        </Typography>
      </Fragment>
    );
  }

  renderText() {
    let { isMenu } = this.state;
    let {
      classes,
      messageData,
      onResendMessage,
      onDeleteMessage,
      userCached,
      userInRoomInfo
    } = this.props;
    let {
      isMe,
      msgBody,
      msgCountUnreaded,
      isSending,
      isPending,
      msgUserKey,
      msgBodies
    } = messageData;
    let { chatCont } = this.getClass(isMe);
    let userInfo = this.getUserInfo(userCached, userInRoomInfo, msgUserKey);

    const renderLeft = () => {
      if (isMe) {
        if (isSending) {
          return null;
        } else if (isPending) {
          return (
            <span className={classes.pendingView}>
              <ReplyIcon
                className={classes.pedingIcon}
                onClick={() => {
                  if (onResendMessage) {
                    onResendMessage(messageData);
                  }
                }}
              />
              <DeleteForeverIcon
                className={classes.pedingIcon}
                onClick={() => {
                  if (onDeleteMessage) {
                    onDeleteMessage(messageData);
                  }
                }}
              />
            </span>
          );
        } else if (messsageUtils.isMessageBookmarked(messageData)) {
          return (
            <StarIcon
              style={{
                fontSize: 18,
                color: '#f2b01e',
                marginLeft: 5
              }}
            />
          );
        } else {
          return (
            <Typography variant="caption" className={classes.unreadNum}>
              {msgCountUnreaded ? msgCountUnreaded : ''}
            </Typography>
          );
        }
      } else {
        return null;
      }
    };

    let subMenu = null;
    if (isSending || isPending) {
      subMenu = null;
    } else if (isMenu) {
      subMenu = isMe ? (
        <span className={classes.pendingViewImg}>
          {ENABLE_SHARE_FILE && (
            <ForwardLeftIcon
              className={classes.pedingIcon}
              onClick={() => this.onForwardMessage(messageData)}
            />
          )}

          <FormatQuoteIcon
            className={classes.pedingIcon}
            onClick={() => this.onQuote(userInfo.displayName, msgBody)}
          />
        </span>
      ) : (
        <span className={classes.pendingViewImg}>
          <FormatQuoteIcon
            className={classes.pedingIcon}
            onClick={() => this.onQuote(userInfo.displayName, msgBody)}
          />
          {ENABLE_SHARE_FILE && (
            <ForwardRightIcon
              className={classes.pedingIcon}
              onClick={() => this.onForwardMessage(messageData)}
            />
          )}
        </span>
      );
    }

    const isEmojOnly = Utils.isOnlyEmojis(msgBody);

    return (
      <div
        className={classes.chatContWrap}
        onMouseEnter={() => this.setState({ isMenu: true })}
        onMouseLeave={() => this.setState({ isMenu: false })}
      >
        {renderLeft()}
        {isMe ? subMenu : null}
        <div className={chatCont}>
          {msgBodies == null ? (
            <Linkify
              options={{
                attributes: {
                  onClick: openLink
                },
                className: isMe ? classes.colorLinkMe : classes.colorLinkOther
              }}
            >
              <Typography
                variant="body2"
                className={
                  isEmojOnly
                    ? classes.textMessageEmojOnly
                    : isMe
                    ? classes.textMessageMe
                    : classes.textMessage
                }
              >
                {msgBodies
                  ? msgBodies.map((item, index) => {
                      return item.highlight ? (
                        <span
                          key={index}
                          className={
                            item.tagMyName
                              ? isMe
                                ? classes.quoteItemTagMe
                                : classes.quoteItemGuestTagMe
                              : isMe
                              ? classes.quoteItemMe
                              : classes.quoteItemGuest
                          }
                        >
                          {item.text}
                        </span>
                      ) : (
                        item.text
                      );
                    })
                  : msgBody}
              </Typography>
            </Linkify>
          ) : (
            <Typography
              variant="body2"
              className={
                isEmojOnly
                  ? classes.textMessageEmojOnly
                  : isMe
                  ? classes.textMessageMe
                  : classes.textMessage
              }
            >
              {msgBodies
                ? msgBodies.map((item, index) => {
                    return item.highlight ? (
                      <span
                        key={index}
                        className={
                          item.tagMyName
                            ? isMe
                              ? classes.quoteItemTagMe
                              : classes.quoteItemGuestTagMe
                            : isMe
                            ? classes.quoteItemMe
                            : classes.quoteItemGuest
                        }
                      >
                        {item.text}
                      </span>
                    ) : (
                      <Linkify
                        key={index}
                        options={{
                          attributes: {
                            onClick: openLink
                          },
                          className: isMe
                            ? classes.colorLinkMe
                            : classes.colorLinkOther
                        }}
                      >
                        {item.text}
                      </Linkify>
                    );
                  })
                : msgBody}
            </Typography>
          )}
        </div>
        {messsageUtils.isMessageBookmarked(messageData) && !isMe ? (
          <StarIcon
            style={{
              fontSize: 18,
              color: '#f2b01e',
              marginLeft: 5
            }}
          />
        ) : null}
        {isMe ? null : (
          <Typography variant="caption" className={classes.unreadNum}>
            {msgCountUnreaded ? msgCountUnreaded : ''}
          </Typography>
        )}
        {isMe ? null : subMenu}
      </div>
    );
  }

  renderPhoto() {
    let { file_progress, file_status, isMenu } = this.state;
    let {
      classes,
      messageData,
      componentImgLoaded,
      onDeleteMessage
    } = this.props;
    let { isMe } = messageData;
    let { chatCont, chatFileDlProgress, progressBar } = this.getClass(isMe);

    const roomKey = messageData.msgRoomKey;
    const msgNumberKey = messageData.msgNumberKey;
    const isGif = messsageUtils.isGIF(messageData);
    const thumbPath = isGif
      ? this.getCorrectFilePath()
      : talkApi.filePathThumbnailWithName(
          roomKey,
          generateThumbFileNameMessage(msgNumberKey)
        );

    const renderError = isUpload => {
      let margin = isUpload
        ? MARGIN_PHOTO_ERROR_MENU_1
        : MARGIN_PHOTO_ERROR_MENU;
      return (
        <div>
          <img
            key={file_status}
            className={classes.imgError}
            style={{ height: this.height }}
            src={'./images/broken-image.png'}
            alt=""
            onLoad={componentImgLoaded}
          />
          {isMenu || true ? (
            <span
              className={classes.pendingViewImg}
              style={{
                position: 'absolute',
                bottom: 0,
                left: isMe ? margin : null,
                right: isMe ? null : margin
              }}
            >
              <ReplayIcon
                className={classes.pedingIcon}
                onClick={this.retryDownloadUpload}
              />
              {isUpload ? (
                <HighlightOffIcon
                  className={classes.pedingIcon}
                  onClick={() => {
                    if (onDeleteMessage) {
                      onDeleteMessage(messageData);
                    }
                  }}
                />
              ) : null}
            </span>
          ) : null}
        </div>
      );
    };

    const statusComponent = {
      isDone: (
        <div>
          <img
            ref={this.imgRef}
            key={file_status}
            onClick={this.onClickOtherFile}
            className={isGif ? classes.imgGif : classes.img}
            style={{ height: this.height }}
            src={ensureFilePath(thumbPath)}
            alt=""
            onLoad={() => {
              if (this.ratio <= 0) {
                const element = this.imgRef.current;
                this.ratio = element.clientHeight / element.clientWidth;
                this.width = this.height / this.ratio;
              }
              componentImgLoaded();
            }}
          />

          {isMenu ? (
            <span
              className={classes.pendingViewImg}
              style={{
                position: 'absolute',
                bottom: 0,
                left: isMe ? MARGIN_MENU : null,
                right: isMe ? null : MARGIN_MENU
              }}
            >
              {isMe ? (
                <Fragment>
                  {ENABLE_SHARE_FILE && (
                    <ForwardLeftIcon
                      className={classes.pedingIcon}
                      onClick={() => this.onForwardMessage(messageData, true)}
                    />
                  )}

                  <FolderOpenIcon
                    className={classes.pedingIcon}
                    onClick={this.openFolderDownload}
                  />
                  <VisibilityIcon
                    className={classes.pedingIcon}
                    onClick={this.onClickOtherFile}
                  />
                </Fragment>
              ) : (
                <Fragment>
                  <FolderOpenIcon
                    className={classes.pedingIcon}
                    onClick={this.openFolderDownload}
                  />
                  <VisibilityIcon
                    className={classes.pedingIcon}
                    onClick={this.onClickOtherFile}
                  />
                  {ENABLE_SHARE_FILE && (
                    <ForwardRightIcon
                      className={classes.pedingIcon}
                      onClick={() => this.onForwardMessage(messageData, true)}
                    />
                  )}
                </Fragment>
              )}
            </span>
          ) : null}
        </div>
      ),
      isErrorUpload: renderError(true),
      isErrorDownload: renderError(false),
      isProgress: (
        <div>
          <ImgPlaceHolder
            isGif={isGif}
            classes={classes}
            height={this.height - PROGESS_BAR_HEIGHT}
            componentImgLoaded={componentImgLoaded}
            thumbPath={ensureFilePath(thumbPath)}
          ></ImgPlaceHolder>
          <div className={classes.divFileOtherProgress}>
            <span className={chatFileDlProgress} style={{ width: '100%' }}>
              <span
                className={progressBar}
                style={{ width: file_progress + '%' }}
              />
            </span>
            <HighlightOffIcon
              onClick={this.stopDownloadUpload}
              className={classes.iconCloseProgress}
            />
          </div>
        </div>
      ),
      isOther: (
        <Skeleton
          variant="rect"
          width={SKELETON_WIDTH}
          height={this.height}
          style={{ borderRadius: 8 }}
        />
      )
    };

    return (
      <div
        className={classes.chatContWrap}
        onMouseEnter={() => this.setState({ isMenu: true })}
        onMouseLeave={() => this.setState({ isMenu: false })}
      >
        <div
          className={chatCont}
          style={{ backgroundColor: 'transparent', padding: 0 }}
        >
          <Fragment>
            <span className={classes.imgArea}>
              {this.getStatusFile(statusComponent)}
            </span>
          </Fragment>
        </div>
      </div>
    );
  }

  renderVideo() {
    let { file_progress, file_status } = this.state;
    let { classes, messageData, onDeleteMessage } = this.props;
    let { isMe, msgdtFileName } = messageData;
    let { chatCont, chatFileDlProgress, progressBar } = this.getClass(isMe);

    let newFilePath = this.getCorrectFilePath();

    const renderError = isUpload => {
      return (
        <div
          ref={this.videoRef}
          className={classes.divVideo}
          style={{ width: this.width }}
        >
          <div
            key={file_status}
            style={{ borderRadius: 8, width: '100%', height: '100%' }}
          >
            <VideocamOffIcon className={classes.iconVideo} />
          </div>
          <span className={classes.pendingViewVideo}>
            <ReplayIcon
              className={classes.pedingIconVideo}
              onClick={this.retryDownloadUpload}
            />
            {isUpload ? (
              <HighlightOffIcon
                className={classes.pedingIconVideo}
                onClick={() => {
                  if (onDeleteMessage) {
                    onDeleteMessage(messageData);
                  }
                }}
              />
            ) : null}
          </span>
        </div>
      );
    };

    const statusComponent = {
      isDone: (
        <Fragment>
          {newFilePath ? (
            <video
              controls
              src={ensureFilePath(newFilePath)}
              ref={stream => {
                this.videoStream = stream;
              }}
              width={this.width}
              height={VIDEO_HEIGHT}
              onLoadedData={() => {}}
              style={{
                backgroundColor: 'black',
                borderRadius: '8px',
                display: 'block'
              }}
            ></video>
          ) : (
            <div
              ref={this.videoRef}
              className={classes.divVideo}
              style={{ width: this.width }}
              onClick={this.onClickOtherFileWithSaveDialog}
            >
              <PlayCircleFilledIcon className={classes.iconVideo} />
            </div>
          )}

          <span className={classes.pendingViewVideo}>
            {ENABLE_SHARE_FILE && (
              <ForwardRightIcon
                className={classes.pedingIconVideo}
                onClick={() => this.onForwardMessage(messageData)}
              />
            )}

            <FolderOpenIcon
              className={classes.pedingIconVideo}
              onClick={this.openFolderDownload}
            />
            <PlayCircleFilledIcon
              className={classes.pedingIconVideo}
              onClick={this.onClickOtherFile}
            />
          </span>
        </Fragment>
      ),
      isErrorUpload: renderError(true),
      isErrorDownload: renderError(false),
      isProgress: (
        <div>
          <div
            ref={this.videoRef}
            className={classes.divVideo}
            style={{
              height: VIDEO_HEIGHT - PROGESS_BAR_HEIGHT,
              width: this.width
            }}
          >
            <PlayCircleFilledIcon className={classes.iconVideo} />
          </div>
          <div className={classes.divFileOtherProgress}>
            <span className={chatFileDlProgress} style={{ width: '100%' }}>
              <span
                className={progressBar}
                style={{ width: file_progress + '%' }}
              />
            </span>
            <HighlightOffIcon
              onClick={this.stopDownloadUpload}
              className={classes.iconCloseProgress}
            />
          </div>
        </div>
      ),
      isOther: (
        <div>
          <div
            ref={this.videoRef}
            className={classes.divVideo}
            style={{
              width: this.width
            }}
          >
            <PlayCircleFilledIcon className={classes.iconVideo} />
          </div>
          <span className={classes.pendingViewVideo}>
            <GetAppIcon
              onClick={this.onClickOtherFileWithSaveDialog}
              className={classes.pedingIconVideo}
            />
          </span>
        </div>
      )
    };

    return (
      <div
        className={classes.chatContWrap}
        onMouseEnter={() => this.setState({ isMenu: true })}
        onMouseLeave={() => this.setState({ isMenu: false })}
      >
        <div className={chatCont} style={{ cursor: 'pointer' }}>
          {this.getStatusFile(statusComponent)}
          <Fragment>
            <div className={classes.chatFileWrap} style={{ marginTop: 10 }}>
              <img
                src={getImg(msgdtFileName.split('.').pop())}
                alt=""
                className={classes.icoFile}
              />
              {this.renderFileInfo()}
            </div>
          </Fragment>
        </div>
      </div>
    );
  }

  renderTCPFile() {
    let { classes, messageData } = this.props;
    let { isMe, msgdtFileName, msgdtFileSize } = messageData;
    let { chatCont } = this.getClass(isMe);

    return (
      <div className={classes.chatContWrap}>
        <div className={chatCont}>
          <Fragment>
            <div className={classes.chatFileWrap}>
              <img
                src={getImg(msgdtFileName.split('.').pop())}
                alt=""
                className={classes.icoFile}
              />
              <Typography variant="subtitle2" className={classes.textFileName}>
                <b>[TCP] </b>
                {msgdtFileName}
              </Typography>
              <Typography
                variant="caption"
                className={isMe ? classes.descMessageMe : classes.descMessage}
              >
                {Utils.convertFileSize(msgdtFileSize)}
              </Typography>
            </div>
          </Fragment>
        </div>
      </div>
    );
  }

  renderFileOther() {
    let { file_progress } = this.state;
    let { classes, messageData, onDeleteMessage, t } = this.props;
    let { isMe, msgdtFileName } = messageData;
    let { chatCont, progressBar } = this.getClass(isMe);

    const renderError = isUpload => {
      return (
        <div className={classes.divFileOtherProgress}>
          <Typography variant="caption" className={classes.textFileName}>
            {t('An error occurred, please try again')}
          </Typography>

          <ReplayIcon
            onClick={this.retryDownloadUpload}
            className={classes.pedingIconVideo}
          />
          {isUpload ? (
            <HighlightOffIcon
              className={classes.pedingIconVideo}
              onClick={() => {
                if (onDeleteMessage) {
                  onDeleteMessage(messageData);
                }
              }}
            />
          ) : null}
        </div>
      );
    };

    const statusComponent = {
      isDone: (
        <div className={classes.divFileOtherProgress}>
          <span style={{ width: '100%' }}></span>
          {ENABLE_SHARE_FILE && (
            <ForwardRightIcon
              className={classes.pedingIconVideo}
              onClick={() => this.onForwardMessage(messageData)}
            />
          )}
          <FolderOpenIcon
            onClick={this.openFolderDownload}
            className={classes.pedingIconVideo}
          />
          <VisibilityIcon
            onClick={this.onClickOtherFile}
            className={classes.pedingIconVideo}
          />
        </div>
      ),
      isErrorUpload: renderError(true),
      isErrorDownload: renderError(false),

      isProgress: (
        <div className={classes.divFileOtherProgress}>
          <span
            className={classes.chatFileDlProgress}
            style={{ width: '100%' }}
          >
            <span
              className={progressBar}
              style={{ width: file_progress + '%' }}
            />
          </span>
          <HighlightOffIcon
            onClick={this.stopDownloadUpload}
            className={classes.pedingIconVideo}
          />
        </div>
      ),
      isOther: (
        <div className={classes.divFileOtherProgress}>
          <span style={{ width: '100%' }}></span>
          <GetAppIcon
            onClick={this.onClickOtherFileWithSaveDialog}
            className={classes.pedingIconVideo}
          />
        </div>
      )
    };

    return (
      <div className={classes.chatContWrap}>
        <div className={chatCont}>
          <Fragment>
            <div className={classes.chatFileWrap}>
              <img
                src={getImg(msgdtFileName.split('.').pop())}
                alt=""
                className={classes.icoFile}
              />
              {this.renderFileInfo()}
            </div>
          </Fragment>
          {this.getStatusFile(statusComponent)}
        </div>
      </div>
    );
  }

  //Record file is file other -> ignore it
  renderRecordFile() {
    let { classes, messageData } = this.props;
    let { isMe } = messageData;
    let { chatCont } = this.getClass(isMe);
    const classIcon = isMe ? classes.iconPrimaryMe : classes.iconPrimary;

    return (
      <div className={classes.chatContWrap}>
        <div className={chatCont}>
          <div className={classes.soundContainer}>
            <PlayCircleFilledIcon className={classIcon} />
            <div className={classes.soundContainerProgress}>
              <EqualizerIcon className={classIcon} />
            </div>
            <span>02:00</span>
          </div>
        </div>
      </div>
    );
  }

  renderBoardText() {
    let { classes, messageData, t } = this.props;
    let { isMe } = messageData;
    let { chatCont } = this.getClass(isMe);
    const classIcon = isMe ? classes.textMessageMe : classes.iconPrimary;
    return (
      <div className={classes.chatContWrap}>
        <div style={{ width: SKELETON_WIDTH }} className={chatCont}>
          <div className={classes.boardContainer}>
            <ChatBoardAlarmIcon className={classIcon} />
            <div style={{ display: 'grid', padding: '0px 10px' }}>
              <Typography
                variant="subtitle2"
                className={isMe ? classes.textMessageMe : classes.textMessage}
              >
                {t('Notice Board')}
              </Typography>
              <Typography
                variant="subtitle2"
                className={isMe ? classes.descMessageMe : classes.descMessage}
              >
                {messageData.msgBody ?? ''}
              </Typography>
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderMessageDelete() {
    let { classes, messageData, t } = this.props;
    let { isMe } = messageData;
    let { chatCont } = this.getClass(isMe);

    return (
      <div className={classes.chatContWrap}>
        <div className={chatCont}>
          <Typography
            variant="caption"
            className={isMe ? classes.descMessageMe : classes.descMessage}
          >
            {t('This message has been deleted')}
          </Typography>
        </div>
      </div>
    );
  }

  renderVoiceCall() {
    let { classes, messageData, t } = this.props;
    let { isMe, msgSIPDuration, msgSIPType, msgSIPStatus } = messageData;
    let { chatCont } = this.getClass(isMe);

    const isVoice = msgSIPType == MessageSipConstant.SIP_TYPE_VOICE;
    let label = '';
    const classIcon = isMe ? classes.iconPrimaryMe : classes.iconPrimary;
    let icon = <PhoneCallOutIcon className={classIcon} />;
    if (isMe) {
      if (msgSIPStatus == MessageSipConstant.SIP_STATUS_CALLING) {
        label = t('Outgoing call');
        if (isVoice) {
          icon = <PhoneCallOutIcon className={classIcon} />;
        } else {
          icon = <PhoneVideoCallOutIcon className={classIcon} />;
        }
      } else if (msgSIPStatus == MessageSipConstant.SIP_STATUS_ME_REJECTED) {
        label = t('Outgoing call');
        if (isVoice) {
          icon = <PhoneCallOutIcon className={classIcon} />;
        } else {
          icon = <PhoneVideoCallOutIcon className={classIcon} />;
        }
      } else if (
        msgSIPStatus == MessageSipConstant.SIP_STATUS_OTHER_USER_REJECTED
      ) {
        label = t('Line busy');
        if (isVoice) {
          icon = <PhoneCallMissedIcon className={classIcon} />;
        } else {
          icon = <PhoneVideoCallMissedIcon className={classIcon} />;
        }
      } else {
        label = t('Outgoing call');
        if (isVoice) {
          icon = <PhoneCallOutIcon className={classIcon} />;
        } else {
          icon = <PhoneVideoCallOutIcon className={classIcon} />;
        }
      }
    } else {
      if (msgSIPStatus == MessageSipConstant.SIP_STATUS_CALLING) {
        label = t('Incoming call');
        if (isVoice) {
          icon = <PhoneCallInIcon className={classIcon} />;
        } else {
          icon = <PhoneVideoCallInIcon className={classIcon} />;
        }
      } else if (msgSIPStatus == MessageSipConstant.SIP_STATUS_ME_REJECTED) {
        label = t('Incoming call');
        if (isVoice) {
          icon = <PhoneCallInIcon className={classIcon} />;
        } else {
          icon = <PhoneVideoCallInIcon className={classIcon} />;
        }
      } else if (
        msgSIPStatus == MessageSipConstant.SIP_STATUS_OTHER_USER_REJECTED
      ) {
        label = t('Missed call');
        if (isVoice) {
          icon = <PhoneCallMissedIcon className={classIcon} />;
        } else {
          icon = <PhoneVideoCallMissedIcon className={classIcon} />;
        }
      } else {
        label = t('Incoming call');
        if (isVoice) {
          icon = <PhoneCallInIcon className={classIcon} />;
        } else {
          icon = <PhoneVideoCallInIcon className={classIcon} />;
        }
      }
    }

    return (
      <div className={classes.chatContWrap}>
        <div className={chatCont}>
          <div className={classes.sipContainer}>
            {icon}
            <div style={{ display: 'grid', padding: '0px 10px' }}>
              <Typography
                variant="subtitle2"
                className={isMe ? classes.textMessageMe : classes.textMessage}
              >
                {label}
              </Typography>
              <Typography
                variant="caption"
                className={isMe ? classes.descMessageMe : classes.descMessage}
              >
                {Utils.formatCallDuration(msgSIPDuration)}
              </Typography>
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderGroupCall() {
    let { classes, messageData, t } = this.props;
    let { isMe, msgSIPType, msgSIPStatus } = messageData;
    let { chatCont } = this.getClass(isMe);

    const isVoice = msgSIPType == MessageSipConstant.SIP_TYPE_VOICE;
    let label = t('joined group');
    const classIcon = isMe ? classes.iconPrimaryMe : classes.iconPrimary;
    let icon = <GroupCallInIcon className={classIcon} />;
    if (isMe) {
      if (msgSIPStatus == MessageSipConstant.GROUP_SIP_STATUS_IN) {
        label = t('joined group');
        if (isVoice) {
          icon = <GroupCallInIcon className={classIcon} />;
        } else {
          icon = <GroupVideoCallInIcon className={classIcon} />;
        }
      } else if (msgSIPStatus == MessageSipConstant.GROUP_SIP_STATUS_OUT) {
        label = t('left group');
        if (isVoice) {
          icon = <GroupCallOutIcon className={classIcon} />;
        } else {
          icon = <GroupVideoCallOutIcon className={classIcon} />;
        }
      }
    } else {
      if (msgSIPStatus == MessageSipConstant.GROUP_SIP_STATUS_IN) {
        label = t('joined group');
        if (isVoice) {
          icon = <GroupCallInIcon className={classIcon} />;
        } else {
          icon = <GroupVideoCallInIcon className={classIcon} />;
        }
      } else if (msgSIPStatus == MessageSipConstant.GROUP_SIP_STATUS_OUT) {
        label = t('left group');
        if (isVoice) {
          icon = <GroupCallOutIcon className={classIcon} />;
        } else {
          icon = <GroupVideoCallOutIcon className={classIcon} />;
        }
      }
    }

    return (
      <div className={classes.chatContWrap}>
        <div className={chatCont}>
          <div className={classes.sipContainer}>
            {icon}
            <div style={{ display: 'grid', padding: '0px 10px' }}>
              <Typography
                variant="subtitle2"
                className={isMe ? classes.textMessageMe : classes.textMessage}
              >
                {label}
              </Typography>
              <Typography
                variant="caption"
                className={isMe ? classes.descMessageMe : classes.descMessage}
              ></Typography>
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderEmoticonIcon() {
    let { messageData, componentImgLoaded } = this.props;
    let { msgBody, msgEmotion } = messageData;
    let path = this.getEmoticon(msgEmotion);

    return (
      <div>
        <EmotionImage
          name={msgEmotion}
          failedPath={this.getEmoticonError()}
          path={path}
          componentImgLoaded={componentImgLoaded}
        />
        {msgBody ? this.renderText() : null}
      </div>
    );
  }

  renderType() {
    let { messageData } = this.props;
    if (messsageUtils.isMessageDeleted(messageData)) {
      return this.renderMessageDelete();
    }

    if (messsageUtils.isTCPFileAttachment(messageData)) {
      return this.renderTCPFile();
    }

    if (this.isPhotoType(messageData)) {
      return this.renderPhoto();
    }

    if (messsageUtils.isEmoticonIcon(messageData)) {
      return this.renderEmoticonIcon();
    }

    if (messsageUtils.isText(messageData)) {
      return this.renderText();
    }

    if (messsageUtils.isVideo(messageData)) {
      return this.renderVideo();
    }

    if (
      this.isFileOtherType(messageData) ||
      messsageUtils.isRecordFile(messageData)
    ) {
      return this.renderFileOther();
    }

    if (messsageUtils.isBoardText(messageData)) {
      return this.renderBoardText();
    }

    if (messsageUtils.isVoiceCallMessage(messageData)) {
      return this.renderVoiceCall();
    }

    if (messsageUtils.isGroupCallMessage(messageData)) {
      return this.renderGroupCall();
    }

    return null;
  }

  renderOther(userInfo) {
    let {
      classes,
      messageData,
      viewProfile,
      onOpenContextMenu,
      closeContextMenu,
      onPhotoClicked,
      t
    } = this.props;

    let {
      isMe,
      isShowName,
      isShowTime,
      msgUserKey,
      msgCreateDate,
      isPending,
      isSending,
      msgIsDeleted
    } = messageData;
    let { chatWrap } = this.getClass(isMe);
    return (
      <Fragment>
        <div
          className={chatWrap}
          onContextMenu={event => {
            event.preventDefault();
            if (msgIsDeleted > 0 || isSending || isPending) {
              return;
            }

            if (
              !messsageUtils.isText(messageData) &&
              !messsageUtils.isFileAttachment(messageData)
            ) {
              return;
            }

            const deleteMessageSupport =
              isMe &&
              !isSending &&
              !isPending &&
              talkApi.apiSupportList.hasSupportDeleteMessage() &&
              Utils.isBeforeMinutesFromNow(msgCreateDate, 5);

            let menuData = [];
            if (messsageUtils.isText(messageData)) {
              menuData = [
                <MenuItem
                  key="Quote"
                  onClick={() => {
                    closeContextMenu();
                    const { userCached, userInRoomInfo } = this.props;
                    let { msgBody, msgUserKey } = messageData;
                    let userInfo = this.getUserInfo(
                      userCached,
                      userInRoomInfo,
                      msgUserKey
                    );
                    this.onQuote(userInfo.displayName, msgBody);
                  }}
                >
                  <Typography variant="caption">{t('Quote')}</Typography>
                </MenuItem>
              ];
              if (ENABLE_SHARE_FILE) {
                menuData.push(
                  <MenuItem
                    key="Share"
                    onClick={() => {
                      closeContextMenu();
                      this.onForwardMessage(messageData);
                    }}
                  >
                    <Typography variant="caption">{t('Share')}</Typography>
                  </MenuItem>
                );
              }
              if (deleteMessageSupport) {
                menuData.push(
                  <MenuItem
                    key="Delete message"
                    onClick={() => {
                      closeContextMenu();
                      const roomKey = messageData.msgRoomKey;
                      if (roomKey && roomKey != '') {
                        ipcRenderer.send(
                          SEND_SOCKET_API_EVENT,
                          API_CANCEL_MESSAGE,
                          {
                            roomKey: roomKey,
                            numberKey: messageData.msgNumberKey,
                            timestamp: messageData.msgCreateDate
                          }
                        );
                      }
                    }}
                  >
                    <Typography variant="caption">
                      {t('Delete message')}
                    </Typography>
                  </MenuItem>
                );
              }
            } else {
              const { file_status } = this.state;
              const isProgressFile =
                file_status == FILE_STATUS.IN_PROGRESS_DOWNLOAD ||
                file_status == FILE_STATUS.IN_PROGRESS_UPLOAD;
              const isErrorUploadFile = file_status == FILE_STATUS.ERROR_UPLOAD;
              if (isProgressFile || isErrorUploadFile) {
                return;
              }
              const isDoneFile =
                file_status == FILE_STATUS.DOWNLOADED ||
                file_status == FILE_STATUS.UPLOADED;

              if (isDoneFile && ENABLE_SHARE_FILE) {
                menuData.push(
                  <MenuItem
                    key="Share"
                    onClick={() => {
                      closeContextMenu();
                      this.onForwardMessage(
                        messageData,
                        this.isPhotoType(messageData)
                      );
                    }}
                  >
                    <Typography variant="caption">{t('Share')}</Typography>
                  </MenuItem>
                );
                menuData.push(
                  <MenuItem
                    key="Show in Folder"
                    onClick={() => {
                      closeContextMenu();
                      this.openFolderDownload();
                    }}
                  >
                    <Typography variant="caption">
                      {t('Show in Folder')}
                    </Typography>
                  </MenuItem>
                );
              }

              menuData.push(
                <MenuItem
                  key="Save to Folder"
                  onClick={() => {
                    closeContextMenu();
                    this.exportFileToFolder();
                  }}
                >
                  <Typography variant="caption">
                    {t('Save to Folder')}
                  </Typography>
                </MenuItem>
              );

              if (talkApi.apiSupportList.hasSupportClouddisk()) {
                if (!messsageUtils.isClouddiskFile(messageData)) {
                  menuData.push(
                    <MenuItem
                      key="Save to Clouddisk"
                      onClick={() => {
                        closeContextMenu();
                        const roomKey = messageData.msgRoomKey;
                        if (roomKey && roomKey != '') {
                          ipcRenderer.send(
                            SEND_SOCKET_API_EVENT,
                            API_SAVE_FILE_CLOUDDISK,
                            {
                              roomKey: roomKey,
                              numberKey: messageData.msgNumberKey,
                              timestamp: messageData.msgCreateDate
                            }
                          );
                        }
                      }}
                    >
                      <Typography variant="caption">
                        {t('Save to Clouddisk')}
                      </Typography>
                    </MenuItem>
                  );
                }
              }

              if (deleteMessageSupport) {
                menuData.push(
                  <MenuItem
                    key="Delete message"
                    onClick={() => {
                      closeContextMenu();
                      const roomKey = messageData.msgRoomKey;
                      if (roomKey && roomKey != '') {
                        ipcRenderer.send(
                          SEND_SOCKET_API_EVENT,
                          API_CANCEL_MESSAGE,
                          {
                            roomKey: roomKey,
                            numberKey: messageData.msgNumberKey,
                            timestamp: messageData.msgCreateDate
                          }
                        );
                      }
                    }}
                  >
                    <Typography variant="caption">
                      {t('Delete message')}
                    </Typography>
                  </MenuItem>
                );
              }
            }

            onOpenContextMenu(event, menuData);
          }}
        >
          {isMe ? null : (
            <Fragment>
              {isShowName ? (
                <span className={classes.userPic} key={msgUserKey}>
                  <ButtonBase
                    onContextMenu={e => {
                      e.preventDefault();
                      e.stopPropagation();
                      viewProfile(userInfo);
                    }}
                    onClick={e => {
                      if (onPhotoClicked) {
                        onPhotoClicked(userInfo);
                      }
                    }}
                  >
                    <UserPhotoView
                      style={{ width: 35, height: 35 }}
                      key={msgUserKey}
                      userKeyData={msgUserKey}
                      data={userInfo}
                      imgSize={50}
                    />
                  </ButtonBase>
                </span>
              ) : null}

              {isShowName ? (
                <Typography variant="subtitle2" color="textSecondary">
                  {userInfo.displayName}
                </Typography>
              ) : null}
            </Fragment>
          )}
          {this.renderType()}
          {isSending ? (
            <Typography variant="caption" color="textSecondary">
              {t('Sending')}
            </Typography>
          ) : isShowTime ? (
            <Typography variant="caption" color="textSecondary">
              {moment(msgCreateDate).format('HH:mm')}
            </Typography>
          ) : null}
        </div>
      </Fragment>
    );
  }

  renderUI() {
    const { messageData, userCached, userInRoomInfo, t } = this.props;
    let { msgBody, msgUserKey, msgFormat } = messageData;
    let userInfo = this.getUserInfo(userCached, userInRoomInfo, msgUserKey);

    if (messsageUtils.isLeftRoom(messageData)) {
      return (
        <LineStatus
          message={`${userInfo.displayName} ${t('has left the chat room')}`}
        />
      );
    } else if (messsageUtils.isEnterRoom(messageData)) {
      let inviteUserInfo = msgFormat
        ? this.getUserInfo(userCached, userInRoomInfo, msgFormat)
        : null;
      if (inviteUserInfo && inviteUserInfo.displayName) {
        let patternF = t('invited $1 to room');
        patternF = patternF.replace('$1', userInfo.displayName);
        return (
          <LineStatus message={`${inviteUserInfo.displayName} ${patternF}`} />
        );
      } else {
        return (
          <LineStatus
            message={`${userInfo.displayName} ${t(
              'has entered the chat room'
            )}`}
          />
        );
      }
    } else if (messsageUtils.isRoomTitleChanged(messageData)) {
      let textFormat = t('changed title to');
      textFormat = textFormat.replace('$1', msgBody);
      return <LineStatus message={`${userInfo.displayName} ${textFormat}`} />;
    } else {
      return this.renderOther(userInfo);
    }
  }

  render() {
    const { messageData } = this.props;
    let { isShowDate, showTimePerHours, msgCreateDate } = messageData;
    return (
      <Fragment>
        <LineStatus
          visible={Boolean(isShowDate || showTimePerHours)}
          message={moment(msgCreateDate).format(talkApi.dateTimeFormat())}
        />
        {this.renderUI()}
      </Fragment>
    );
  }
}

export default withTranslation()(withStyles(withTheme(ItemMessage)));

class EmotionImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      failed: false
    };
    this.onError = this.onError.bind(this);
  }
  onError() {
    this.setState({
      failed: true
    });
  }
  render() {
    const { path, failedPath, componentImgLoaded, name } = this.props;
    const { failed } = this.state;

    const w =
      name == Utils.tickerEmojManager.likeTicker?.attributes?.name
        ? 50
        : EMOTICON_HEIGHT;
    const h =
      name == Utils.tickerEmojManager.likeTicker?.attributes?.name
        ? 50
        : EMOTICON_HEIGHT;
    return (
      <img
        style={{ width: w, height: h }}
        src={failed ? failedPath : path}
        onError={this.onError}
        onLoad={componentImgLoaded}
      />
    );
  }
}

class ImgPlaceHolder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageExist: false
    };
  }

  componentDidMount() {
    const { thumbPath } = this.props;
    const exist = fs.existsSync(thumbPath);
    this.setState({
      imageExist: exist
    });
  }

  render() {
    const {
      classes,
      componentImgLoaded,
      thumbPath,
      height,
      isGif
    } = this.props;
    const { imageExist } = this.state;
    return (
      <Fragment>
        {imageExist ? (
          <img
            className={isGif ? classes.imgGif : classes.img}
            style={{ height: height }}
            src={ensureFilePath(thumbPath)}
            alt=""
            onLoad={componentImgLoaded}
          />
        ) : (
          <Skeleton
            variant="rect"
            width={SKELETON_WIDTH}
            height={height}
            style={{ borderRadius: 8 }}
          />
        )}
      </Fragment>
    );
  }
}
