import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import withStyles from './styles';
import { ipcRenderer, remote } from 'electron';

import RoomView from '../../../home/messenger/ChatWindowContent/components/RoomView';
import MessageList from '../../../home/messenger/ChatWindowContent/components/MessageList';
import { Box } from '@material-ui/core';
import {
  getRoomDetailList,
  getMessageList
} from '../../../../core/worker/messagelist';
import * as constantsApp from '../../../../configs/constant';
import talkAPI from '../../../../core/service/talk.api.render';

class ChatWindowContent extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.delayGetTalkList = null;
    this.delayShowDisconnect = null;
  }

  componentDidMount() {
    const { actions } = this.props;
    talkAPI.currentFirstRoomTime = 0;

    //REPLY_QUERY_MESSAGE_LIST_ROOM_DETAIL
    ipcRenderer.on(
      constantsApp.REPLY_QUERY_MESSAGE_LIST_ROOM_DETAIL,
      (event, messageList, roomDetail, info) => {
        let curRoomKey = this.props.roomKey;
        let nextRoomKey = roomDetail.room ? roomDetail.room.rRoomKey : '';
        if (curRoomKey != nextRoomKey) {
          return;
        }

        let myInfo = null;
        if (
          this.props.user_cached &&
          this.props.logged_user?.account_info?.user_key
        ) {
          myInfo = this.props.user_cached[
            this.props.logged_user.account_info.user_key
          ];
        }

        const useBoth = !info.queryRoomDetailOnly;
        let roomDetailData = {
          roomDetails: roomDetail,
          userCached: this.props.user_cached,
          loggedUser: this.props.logged_user
        };

        if (useBoth) {
          let data = {
            ...messageList,
            loggedUser: this.props.logged_user,
            myInfo: myInfo
          };
          getRoomDetailList(roomDetailData).then(result => {
            getMessageList(data).then(messageResult => {
              actions.saveBothRoomInfoMessageList(
                result.room,
                result.listAllUserInRoom,
                result.listUserActiveInRoom,
                messageResult.messages,
                messageResult.roomKey,
                messageResult.roomID,
                messageResult.firstMessageTime
              );
            });
          });
        } else {
          getRoomDetailList(roomDetailData).then(result => {
            actions.saveRoomInfo(
              result.room,
              result.listAllUserInRoom,
              result.listUserActiveInRoom
            );
          });

          if (!info.queryRoomDetailOnly) {
            let data = {
              ...messageList,
              loggedUser: this.props.logged_user,
              myInfo: myInfo
            };
            getMessageList(data).then(result => {
              actions.saveMessageList(
                result.messages,
                result.roomKey,
                result.roomID,
                result.firstMessageTime
              );
            });
          }
        }
      }
    );

    this.mainToRenderListener = (event, action, args) => {
      if (action == constantsApp.ACTION_MSG_GETTALK_CHANGED) {
        let curRoomKey = this.props.roomKey;
        let nextRoomKey = args.roomKey;
        if (curRoomKey == nextRoomKey && args.firstTimeStamp > 0) {
          talkAPI.currentFirstRoomTime = args.firstTimeStamp;
          ipcRenderer.send(
            constantsApp.REQUEST_QUERY_MESSAGE_LIST_ROOM_DETAIL,
            args
          );
        }
        return;
      }

      if (action == constantsApp.ACTION_RELOAD_ALL_ROOM_CONTENT) {
        let roomKey = args.roomKey;
        let queryRoomDetailOnly = args.queryRoomDetailOnly;
        let roomInfo = this.props.room_info;
        let lastRoomKey = null;
        let lastFirstTime = 0;
        let roomId = -1;
        if (roomInfo) {
          lastRoomKey = roomInfo.roomKey;
          lastFirstTime = roomInfo.firstTimeMessage ?? 0;
          roomId = roomInfo.roomID;
        }

        if (roomKey && roomKey == lastRoomKey && roomId >= 0) {
          if (this.delayGetTalkList) {
            clearTimeout(this.delayGetTalkList);
          }
          this.delayGetTalkList = setTimeout(() => {
            let info = {
              clientKey: '',
              firstTimeStamp: talkAPI.currentFirstRoomTime,
              roomID: roomId,
              roomKey: roomKey,
              talkCount: 0,
              queryRoomDetailOnly: queryRoomDetailOnly
            };

            ipcRenderer.send(
              constantsApp.REQUEST_QUERY_MESSAGE_LIST_ROOM_DETAIL,
              info
            );
          }, 100);
        }
        return;
      }

      if (action == constantsApp.ACTION_STOP_ALL_REQUEST) {
        let roomKey = this.props.roomKey;
        let roomInfo = this.props.room_info;
        let lastRoomKey = null;
        let lastFirstTime = 0;
        let roomId = -1;
        if (roomInfo) {
          lastRoomKey = roomInfo.roomKey;
          lastFirstTime = roomInfo.firstTimeMessage ?? 0;
          roomId = roomInfo.roomID;
        }

        if (roomKey && roomKey == lastRoomKey && roomId >= 0) {
          let info = {
            clientKey: '',
            firstTimeStamp: talkAPI.currentFirstRoomTime,
            roomID: roomId,
            roomKey: roomKey,
            talkCount: 0,
            queryRoomDetailOnly: false
          };
          ipcRenderer.send(
            constantsApp.REQUEST_QUERY_MESSAGE_LIST_ROOM_DETAIL,
            info
          );
        }
        return;
      }
    };

    ipcRenderer.on(
      constantsApp.MAIN_TO_RENDER_EVENT,
      this.mainToRenderListener
    );
  }

  componentWillUnmount() {
    talkAPI.currentFirstRoomTime = 0;
    clearTimeout(this.delayShowDisconnect);
    clearTimeout(this.delayGetTalkList);
    ipcRenderer.removeAllListeners(
      constantsApp.REPLY_QUERY_MESSAGE_LIST_ROOM_DETAIL
    );
    ipcRenderer.removeListener(
      constantsApp.MAIN_TO_RENDER_EVENT,
      this.mainToRenderListener
    );
  }

  isCurrentWindowFocused() {
    return (
      remote.getCurrentWindow().isFocused() &&
      remote.getCurrentWindow().isVisible() &&
      !remote.getCurrentWindow().isMinimized()
    );
  }

  setFlashBadgeIcon() {
    if (process.platform == 'win32') {
      remote.getCurrentWindow().flashFrame(true);
    }
  }

  showDisconnectSocket(force, show) {
    const { actions } = this.props;
    clearTimeout(this.delayShowDisconnect);
    if (force) {
      actions.showDisconnectSocket(show);
    } else {
      this.delayShowDisconnect = setTimeout(() => {
        actions.showDisconnectSocket(show);
      }, 5000);
    }
  }

  render() {
    const {
      user_cached,
      logged_user,
      message_list,
      appUI,
      roomData,
      roomKey,
      classes
    } = this.props;

    return (
      <div className={classes.main}>
        <Box
          minHeight={70}
          width="100%"
          borderTop={1}
          borderBottom={1}
          className={classes.contentTop}
        >
          <RoomView />
        </Box>

        <Box width="100%" className={classes.contentBottom}>
          <MessageList
            primary_key={roomKey}
            show_disconnect={appUI.show_disconnect}
            user_logged={logged_user}
            roomData={roomData}
            messageData={message_list}
            userCached={user_cached}
          ></MessageList>
        </Box>
      </div>
    );
  }
}

export default withRouter(withStyles(ChatWindowContent));
