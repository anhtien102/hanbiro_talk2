import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  main: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    height: '100%'
  },
  contentTop: {
    borderColor: theme.palette.divider,
    display: 'flex'
  },
  contentBottom: {
    height: 'calc(100% - 70px)'
  }
});

export default withStyles(styles);
