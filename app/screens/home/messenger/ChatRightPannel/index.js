import React, { useContext } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { useStyles } from './styles';
import { CSSListItem } from './component.styles';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import { useTranslation } from 'react-i18next';
import FileList from './FileList';
import { FILE_LIST_PANNEL_WIDTH } from '../../../../configs/constant';

import { Snackbar } from '@material-ui/core';
import { OrgContext } from '../../../home';

export default function ChatRightPannel(props) {
  const classes = useStyles();
  const { onOpenRoomUserChoiceDialog } = useContext(OrgContext);
  const { t } = useTranslation();
  const data = useSelector(state => state.message_list.room);
  const userCached = useSelector(state => state.company.user_cached);
  const [tabIndex, setTabIndex] = React.useState(0);
  const [openSnackbar, setOpenSnackbar] = React.useState({
    show: false,
    message: ''
  });

  const onChangeTabIndex = index => {
    setTabIndex(index);
  };

  const handleClosed = () => {
    setOpenSnackbar({ show: false, message: '' });
  };

  const showInfoToUser = message => {
    setOpenSnackbar({ show: true, message: message });
  };

  const forwardMessage = data => {
    onOpenRoomUserChoiceDialog(data);
  };

  return (
    <div
      className={classes.boxContainer}
      style={{ width: FILE_LIST_PANNEL_WIDTH }}
      key={data?.rRoomKey ?? 'nothing'}
    >
      <div className={classes.contentTop}>
        <div className={classes.tabContainer}>
          <CSSListItem
            button
            onClick={() => onChangeTabIndex(0)}
            selected={tabIndex == 0}
          >
            <ListItemText
              disableTypography
              primary={
                <Typography type="body2" className={classes.tabText}>
                  {t('Photo')}
                </Typography>
              }
            />
          </CSSListItem>
          {tabIndex == 0 && <div className={classes.selectedLine} />}
        </div>

        <div className={classes.tabContainer}>
          <CSSListItem
            button
            onClick={() => onChangeTabIndex(1)}
            selected={tabIndex == 1}
          >
            <ListItemText
              disableTypography
              primary={
                <Typography type="body2" className={classes.tabText}>
                  {t('Other')}
                </Typography>
              }
            />
          </CSSListItem>
          {tabIndex == 1 && <div className={classes.selectedLine} />}
        </div>
      </div>
      <TabPanel
        showInfoToUser={showInfoToUser}
        forwardMessage={forwardMessage}
        key={tabIndex}
        value={tabIndex}
        room={data}
        userCached={userCached}
      />
      <Snackbar
        autoHideDuration={2000}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={openSnackbar.show}
        onClose={() => handleClosed()}
        message={openSnackbar.message}
      />
    </div>
  );
}

function TabPanel(props) {
  const classes = useStyles();
  const { value, room, showInfoToUser, forwardMessage, userCached } = props;
  return (
    <div className={classes.tabPanel}>
      <FileList
        userCached={userCached}
        type={value}
        room={room}
        showInfoToUser={showInfoToUser}
        forwardMessage={forwardMessage}
      />
    </div>
  );
}

TabPanel.propTypes = {
  value: PropTypes.any.isRequired,
  room: PropTypes.object.isRequired,
  showInfoToUser: PropTypes.func,
  forwardMessage: PropTypes.func
};
