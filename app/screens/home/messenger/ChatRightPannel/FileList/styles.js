import { withStyles } from '@material-ui/core/styles';
const styles = theme => ({
  allContainer: {
    width: '100%',
    height: '100%',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column'
  },
  nodata: {
    transform: 'translate(-50%, -50%)',
    top: '50%',
    left: '50%',
    position: 'absolute'
  },
  scrollContainer: {
    width: '100%',
    height: '100%',
    position: 'relative',
    display: 'flex',
    padding: 16,
    // paddingTop: 16,
    // paddingBottom: 16,
    overflow: 'auto',
    flexDirection: 'column'
  },
  childContainer: {
    width: '100%',
    display: 'flex',

    flexDirection: 'column'
  },
  cellLabel: {
    backgroundColor: '#21050566',
    paddingBottom: 5,
    paddingTop: 5,
    textAlign: 'center',
    fontWeight: 900
  },
  cellImage: {
    marginTop: 10,
    marginBottom: 10,
    width: '100%'
  },
  cellOtherFile: {
    marginTop: 10,
    marginBottom: 10,
    width: '100%',
    padding: 8,
    borderRadius: 10,
    backgroundColor:
      theme.palette.type == 'light'
        ? theme.palette.hanbiroColor.backgroundMe
        : '#424242'
  },
  imgArea: {
    width: '100%',
    textAlign: 'center',
    cursor: 'pointer'
  },
  img: {
    // display: 'block',
    backgroundColor: 'white',
    minWidth: 100,
    maxHeight: 300,
    maxWidth: '100%',
    borderRadius: 8,
    border: `1px solid ${theme.palette.grey['400']}`
  },
  imgGif: {
    // display: 'block',
    minWidth: 100,
    maxHeight: 300,
    maxWidth: '100%'
  },
  imgError: {
    display: 'block',
    minWidth: 110,
    minHeight: 110,
    alignItems: 'left',
    maxWidth: '100%',
    maxHeight: 300,
    borderRadius: 8,
    border: `1px solid ${theme.palette.grey['400']}`
  },
  pendingViewImg: {
    height: 'auto',
    alignItems: 'center',
    backgroundColor: '#e8f1ff',
    paddingTop: '4px',
    margin: '5px',
    borderRadius: '5px',
    cursor: 'pointer',
    display: 'inline-flex'
  },
  pedingIcon: {
    fontSize: 20,
    color: '#8c95a3',
    marginRight: 5,
    marginLeft: 5
  },
  divFileOtherProgress: { display: 'flex', alignItems: 'center' },
  pedingIconVideo: {
    zIndex: 4,
    fontSize: 20,
    color: '#8c95a3',
    marginLeft: 5,
    padding: '2px',
    borderRadius: '5px',
    border: '1px solid #bdbdbd',
    backgroundColor: 'white'
  },
  chatFileDlProgress: {
    position: 'relative',
    display: 'block',
    height: '5px',
    overflow: 'hidden',
    borderRadius: '10px',
    backgroundColor: theme.palette.background.paper
  },
  progressBar: {
    position: 'absolute',
    top: 0,
    left: 0,
    display: 'inline-block',
    height: '5px',
    backgroundColor: theme.palette.primary.main
  },
  chatFileWrap: {
    minHeight: '40px',
    position: 'relative',
    paddingLeft: '40px'
  },
  textFileName: {
    color: 'rgba(0, 0, 0, 0.87)',
    maxWidth: 200
  },
  viewImage: {
    position: 'relative',
    textAlign: 'center'
  },
  menuImage: {
    height: 'auto',
    alignItems: 'center',
    backgroundColor: '#e8f1ff',
    borderRadius: '5px',
    cursor: 'pointer',
    display: 'inline-flex',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  },
  viewFileOther: {
    minHeight: '40px',
    position: 'relative',
    display: 'flex',
    alignItems: 'center'
  },
  contentFileOther: {
    flex: 1,
    padding: 8
  },
  titleFile: {
    wordBreak: 'break-all'
  },
  menuPhotoError: {
    height: 'auto',
    alignItems: 'center',
    backgroundColor: '#e8f1ff',
    borderRadius: '5px',
    cursor: 'pointer',
    display: 'inline-flex',
    position: 'absolute',
    top: '80%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  },
  icoFile: {
    borderColor: 'rgba(255,255,255,0.5)',
    borderRadius: 16,
    borderStyle: 'solid',
    borderWidth: 2,
    height: 60
  },
  textFileNameMe: {
    wordBreak: 'break-all',
    color: theme.palette.type == 'light' ? '#333333' : '#ffffff',
    maxWidth: 200
  },
  descMessageMe: {
    color: theme.palette.hanbiroColor.textDescMe
  },
  titleFile: {
    width: '100%',
    padding: 3,
    display: 'flex',
    backgroundColor: theme.palette.divider
  }
});

export default withStyles(styles);
