import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { ipcRenderer, remote } from 'electron';
import moment from 'moment';
import {
  MAIN_TO_RENDER_EVENT,
  ACTION_MSG_GETFILES_CHANGED,
  SEND_SOCKET_API_EVENT,
  API_GET_FILES,
  API_SAVE_FILE_CLOUDDISK,
  REQUEST_COMMON_ACTION_FROM_RENDER,
  ACTION_REQUEST_QUERY_FILE_LIST_WITH_FILTER,
  ACTION_REPLY_QUERY_FILE_LIST_WITH_FILTER,
  THUMBNAIL_SIZE_LOCAL_STORE,
  THUMBNAIL_SIZE_LOCAL_STORE_QUALITY
} from '../../../../../configs/constant';

import { ensureFilePath } from '../../../../../configs/file_cheat';

import { Typography, MenuItem } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import ReplayIcon from '@material-ui/icons/Replay';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { ForwardRightIcon } from '../../../../../components/HanSVGIcon';
import FolderOpenIcon from '@material-ui/icons/FolderOpen';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import GetAppIcon from '@material-ui/icons/GetApp';
import { resizeImageWithPathAndSave } from '../../../../../core/utils/image.process';
import { getImg } from '../../../../../components/BaseFile';
import {
  keyOfMessage,
  generateThumbFileNameMessage,
  generateFileNameDownloadedMessage
} from '../../../../../core/service/talk-constants';
import talkApi from '../../../../../core/service/talk.api.render';
import {
  messsageUtils,
  fileExtension
} from '../../../../../core/model/MessageUtils';
import { FILE_STATUS, convertFileSize } from '../../../../../utils';

import downloadUploadManager from '../../../../../core/service/filemanager/download-upload-manager';
import { getFileListWorker } from '../../../../../core/worker/messagelist';
import SimpleBarHanbiro from '../../../../../components/SimpleBarHanbiro';
import fs from 'fs';
import withStyles from './styles';
import { withTranslation } from 'react-i18next';

import { MenuOptionContext } from '../../../../home';

import {
  remoteOpenItem,
  remoteShowItemInFolder
} from '../../../../../utils/electron.utils';

const DOWNLOAD_CATEGORY_FILE_LIST = 'getFiles';

class FileList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      showNoData: false
    };
    this.isGettingFile = false;
    this.currentFirstTimeStamp = 0;
    this.listRef = React.createRef();
    this.timerGetFilesDelay = null;
    this.timerLoadDBDelay = null;
    this.getFilesListener = this.getFilesListener.bind(this);
    this.onScroll = this.onScroll.bind(this);

    this.onOpenContextMenu = this.onOpenContextMenu.bind(this);
    this.onCloseContextMenu = this.onCloseContextMenu.bind(this);
  }
  static contextType = MenuOptionContext;

  getFilesListener(event, action, args) {
    if (action == ACTION_MSG_GETFILES_CHANGED) {
      const { room, type } = this.props;
      const typeString = type == 0 ? 'IMAGE' : 'OTHERS';
      if (args.roomKey != room.rRoomKey || typeString != args.filterType) {
        return;
      }
      this.isGettingFile = false;
      if (!args.firstTimeStamp || args.firstTimeStamp == '') {
        if (args.requestTime == 0) {
          this.currentFirstTimeStamp = 0;
          const { messages } = this.state;
          if (messages.length <= 0) {
            this.setState({ messages: [], showNoData: true });
          }
        }
        return;
      }

      if (!(args.requestTime == 0 && args.messageList?.length < 10)) {
        this.currentFirstTimeStamp = args.firstTimeStamp;
      }

      ipcRenderer.send(
        REQUEST_COMMON_ACTION_FROM_RENDER,
        ACTION_REQUEST_QUERY_FILE_LIST_WITH_FILTER,
        {
          roomID: args.roomID,
          roomKey: args.roomKey,
          filterType: type,
          firstTimeStamp: this.currentFirstTimeStamp
        }
      );
    } else if (action == ACTION_REPLY_QUERY_FILE_LIST_WITH_FILTER) {
      const { room, type } = this.props;
      if (args.roomKey != room.rRoomKey || type != args.filterType) {
        return;
      }

      getFileListWorker(args).then(result => {
        if (result.roomKey != room.rRoomKey || type != result.filterType) {
          return;
        }
        this.setState({ messages: result.messages });
      });
    }
  }

  sendFileListRequest(lastTime) {
    this.isGettingFile = true;
    const { room, type } = this.props;
    ipcRenderer.send(SEND_SOCKET_API_EVENT, API_GET_FILES, {
      roomKey: room.rRoomKey,
      lastTime: lastTime,
      filterType: type
    });
  }

  onScroll(e) {
    let element = e.target;
    const diffScrollTop = element.scrollHeight - element.clientHeight;
    const scrollAtBottom = diffScrollTop - element.scrollTop <= 0;
    if (scrollAtBottom && !this.isGettingFile) {
      this.sendFileListRequest(this.currentFirstTimeStamp);
    }
  }

  componentDidMount() {
    const { room, type } = this.props;
    this.simpleBar = new SimpleBarHanbiro(this.listRef.current);

    ipcRenderer.on(MAIN_TO_RENDER_EVENT, this.getFilesListener);

    if (room) {
      ipcRenderer.send(
        REQUEST_COMMON_ACTION_FROM_RENDER,
        ACTION_REQUEST_QUERY_FILE_LIST_WITH_FILTER,
        {
          roomID: room.rID,
          roomKey: room.rRoomKey,
          filterType: type,
          firstTimeStamp: 0
        }
      );
    }

    clearTimeout(this.timerGetFilesDelay);
    this.timerGetFilesDelay = setTimeout(() => {
      this.currentFirstTimeStamp = 0;
      this.sendFileListRequest(this.currentFirstTimeStamp);
    }, 1000);
  }

  componentWillUnmount() {
    clearTimeout(this.timerGetFilesDelay);
    ipcRenderer.removeListener(MAIN_TO_RENDER_EVENT, this.getFilesListener);
  }

  onOpenContextMenu(event, menuData) {
    this.context.onOpen(event, menuData);
  }

  onCloseContextMenu() {
    this.context.onClose();
  }

  render() {
    const {
      classes,
      type,
      t,
      showInfoToUser,
      forwardMessage,
      userCached
    } = this.props;

    const { messages, showNoData } = this.state;
    return (
      <div className={classes.allContainer}>
        <div
          className={classes.scrollContainer}
          ref={this.listRef}
          onScroll={this.onScroll}
        >
          <div className={classes.childContainer}>
            {messages.map((element, index) => {
              if (element.type == 'date') {
                return (
                  <DateMessage
                    key={element.primaryKey}
                    classes={classes}
                    value={element.time}
                  />
                );
              }
              return (
                <FileMessage
                  t={t}
                  userCached={userCached}
                  onOpenContextMenu={this.onOpenContextMenu}
                  closeContextMenu={this.onCloseContextMenu}
                  forwardMessage={forwardMessage}
                  showInfoToUser={showInfoToUser}
                  classes={classes}
                  type={type}
                  key={element.primaryKey}
                  data={element}
                />
              );
            })}
          </div>
        </div>
        {showNoData && (
          <Typography className={classes.nodata} variant="body2">
            {t('no_result')}
          </Typography>
        )}
      </div>
    );
  }
}

FileList.defaultProps = {
  room: {},
  type: 0,
  showInfoToUser: () => {},
  forwardMessage: () => {}
};

FileList.propTypes = {
  room: PropTypes.object.isRequired,
  type: PropTypes.any.isRequired,
  showInfoToUser: PropTypes.func,
  forwardMessage: PropTypes.func
};

export default withTranslation()(withStyles(FileList));

class FileMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file_status: FILE_STATUS.NORMAL,
      file_progress: -1,
      file_path_downloaded: null,
      isMenu: false
    };
    this.height = 200;
    this.retryDownloadUpload = this.retryDownloadUpload.bind(this);
    this.getCorrectFilePath = this.getCorrectFilePath.bind(this);
    this.onClickOtherFileWithSaveDialog = this.onClickOtherFileWithSaveDialog.bind(
      this
    );
    this.onClickOtherFile = this.onClickOtherFile.bind(this);
    this.openFolderDownload = this.openFolderDownload.bind(this);
    this.openFileExternalApp = this.openFileExternalApp.bind(this);
    this.stopDownloadUpload = this.stopDownloadUpload.bind(this);
    this.onForwardMessage = this.onForwardMessage.bind(this);
  }

  componentDidMount() {
    const { data } = this.props;
    const roomKey = data.msgRoomKey;
    const msgNumberKey = data.msgNumberKey;
    if (msgNumberKey && msgNumberKey != '') {
      this.checkAutoDownloadUploadAndUpdateFileStatus(
        data,
        roomKey,
        msgNumberKey
      );
    }
  }

  componentWillUnmount() {
    const { data } = this.props;
    if (messsageUtils.isFileAttachment(data)) {
      const roomKey = data.msgRoomKey;
      const msgNumberKey = data.msgNumberKey;
      downloadUploadManager.removeListener(
        keyOfMessage(roomKey, msgNumberKey),
        DOWNLOAD_CATEGORY_FILE_LIST
      );
    }
  }

  onForwardMessage(data, isPhoto) {
    const { forwardMessage } = this.props;

    if (forwardMessage) {
      if (messsageUtils.isText(data)) {
        forwardMessage({
          data: data,
          filePath: '',
          thumbPath: '',
          isPhoto: false
        });
      } else {
        const roomKey = data.msgRoomKey;
        const msgNumberKey = data.msgNumberKey;
        const isGif = messsageUtils.isGIF(data);
        const filePath = this.getCorrectFilePath();
        const thumbPath = isGif
          ? filePath
          : talkApi.filePathThumbnailWithName(
              roomKey,
              generateThumbFileNameMessage(msgNumberKey)
            );
        forwardMessage({
          data: data,
          filePath: filePath,
          thumbPath: thumbPath,
          isPhoto: isPhoto
        });
      }
    }
  }

  onClickOtherFileWithSaveDialog() {
    console.log('onClickOtherFileWithSaveDialog');
    const { data } = this.props;
    if (data.msgdtHasFile <= 0) {
      return;
    }
    const { dialog } = remote;
    let fileName = data.msgdtFileName ?? '';
    fileName = fileName.normalize();
    dialog
      .showSaveDialog(remote.getCurrentWindow(), {
        defaultPath: fileName
      })
      .then(result => {
        if (result.canceled) {
          return;
        }
        const keyListener = keyOfMessage(data.msgRoomKey, data.msgNumberKey);

        let destPath = result.filePath;
        let extension = fileExtension(destPath);
        if (extension == null || extension == '') {
          destPath = `${destPath}.${fileExtension(fileName)}`;
        }

        const lastFileState = downloadUploadManager.startDownloadIfCan(
          keyListener,
          data,
          destPath
        );
        this.setState({
          file_status: lastFileState.isDownload
            ? FILE_STATUS.IN_PROGRESS_DOWNLOAD
            : FILE_STATUS.IN_PROGRESS_UPLOAD,
          file_progress: lastFileState.percent,
          file_path_downloaded: destPath
        });
      });
  }

  onClickOtherFile() {
    console.log('onClickOtherFile');
    const { data, t } = this.props;
    const { file_status } = this.state;
    const isDone =
      file_status == FILE_STATUS.DOWNLOADED ||
      file_status == FILE_STATUS.UPLOADED;
    const notDownload = file_status == FILE_STATUS.NORMAL;
    if (notDownload) {
      if (data.msgdtHasFile > 0) {
        const keyListener = keyOfMessage(data.msgRoomKey, data.msgNumberKey);
        const lastFileState = downloadUploadManager.startDownloadIfCan(
          keyListener,
          data
        );
        this.setState({
          file_status: lastFileState.isDownload
            ? FILE_STATUS.IN_PROGRESS_DOWNLOAD
            : FILE_STATUS.IN_PROGRESS_UPLOAD,
          file_progress: lastFileState.percent
        });
      } else {
        this.showInfoToUser(t('File is deleted'));
      }
    } else if (isDone) {
      let newFilePath = this.getCorrectFilePath();
      this.openFileExternalApp(newFilePath);
    }
  }

  openFolderDownload() {
    let newFilePath = this.getCorrectFilePath();
    remoteShowItemInFolder(newFilePath);
  }

  openFileExternalApp(filePath) {
    remoteOpenItem(filePath);
  }

  stopDownloadUpload(e) {
    e.stopPropagation();
    const { file_status } = this.state;
    const { data } = this.props;
    if (file_status == FILE_STATUS.IN_PROGRESS_DOWNLOAD) {
      const keyListener = keyOfMessage(data.msgRoomKey, data.msgNumberKey);
      downloadUploadManager.stopDownloadIfCan(keyListener);
    } else if (file_status == FILE_STATUS.IN_PROGRESS_UPLOAD) {
      const keyListener = keyOfMessage(data.msgRoomKey, data.msgNumberKey);
      downloadUploadManager.stopUploadIfCan(keyListener);
    }
  }

  retryDownloadUpload(e) {
    e.stopPropagation();
    const { file_status } = this.state;
    const { data, t, showInfoToUser } = this.props;
    if (file_status == FILE_STATUS.ERROR_DOWNLOAD) {
      if (data.msgdtHasFile <= 0) {
        if (showInfoToUser) {
          showInfoToUser(t('File is deleted'));
        }

        return;
      }
      //retry download
      const isPhoto = this.isPhotoType(data);
      if (isPhoto) {
        // photo store to cache
        const keyListener = keyOfMessage(data.msgRoomKey, data.msgNumberKey);
        const lastFileState = downloadUploadManager.startDownloadIfCan(
          keyListener,
          data
        );
        this.setState({
          file_status: lastFileState.isDownload
            ? FILE_STATUS.IN_PROGRESS_DOWNLOAD
            : FILE_STATUS.IN_PROGRESS_UPLOAD,
          file_progress: lastFileState.percent
        });
      } else {
        this.onClickOtherFileWithSaveDialog();
      }
    }
  }

  isTCPFileType(messageData) {
    return messsageUtils.isTCPFileAttachment(messageData);
  }

  isPhotoType(messageData) {
    return messsageUtils.isPhotoSmaller20MB(messageData);
  }

  isFileOtherType(messageData) {
    return (
      messsageUtils.isFileOther(messageData) ||
      messsageUtils.isPhotoLarger20MB(messageData)
    );
  }

  async initFileStatus(messageData, callback) {
    let filePathPrivate = messageData.msgdtFilePath
      ? messageData.msgdtFilePath
      : talkApi.filePathDownloadedWithName(
          messageData.msgRoomKey,
          generateFileNameDownloadedMessage(
            messageData.msgNumberKey,
            messageData.msgdtFileName
          )
        );

    if (fs.existsSync(filePathPrivate)) {
      let stats = fs.statSync(filePathPrivate);
      if (stats.size == messageData.msgdtFileSize) {
        let thumbPath = talkApi.filePathThumbnailWithName(
          messageData.msgRoomKey,
          generateThumbFileNameMessage(messageData.msgNumberKey)
        );
        if (!fs.existsSync(thumbPath)) {
          resizeImageWithPathAndSave(
            filePathPrivate,
            THUMBNAIL_SIZE_LOCAL_STORE,
            thumbPath,
            THUMBNAIL_SIZE_LOCAL_STORE_QUALITY,
            messageData.msgdtFileName,
            () => {
              callback({
                file: filePathPrivate,
                state: FILE_STATUS.DOWNLOADED
              });
            }
          );
        } else {
          callback({ file: filePathPrivate, state: FILE_STATUS.DOWNLOADED });
        }
        return;
      }
    }

    if (messageData.msgdtHasFile <= 0) {
      callback({ file: null, state: FILE_STATUS.ERROR_DOWNLOAD });
      return;
    }

    callback({ file: null, state: FILE_STATUS.NORMAL });
  }

  checkAutoDownloadUploadAndUpdateFileStatus(
    messageData,
    roomKey,
    msgNumberKey,
    finishedCallBack
  ) {
    if (messsageUtils.isFileAttachment(messageData)) {
      const keyListener = keyOfMessage(roomKey, msgNumberKey);

      this.initFileStatus(messageData, fileObj => {
        let fileState = fileObj.state;

        downloadUploadManager.addListener(
          keyListener,
          DOWNLOAD_CATEGORY_FILE_LIST,
          result => {
            if (result.key == keyListener) {
              let fileState;
              if (result.finished) {
                if (result.error) {
                  fileState = result.isDownload
                    ? FILE_STATUS.ERROR_DOWNLOAD
                    : FILE_STATUS.ERROR_UPLOAD;
                } else {
                  fileState = result.isDownload
                    ? FILE_STATUS.DOWNLOADED
                    : FILE_STATUS.UPLOADED;
                }
              } else {
                fileState = result.isDownload
                  ? FILE_STATUS.IN_PROGRESS_DOWNLOAD
                  : FILE_STATUS.IN_PROGRESS_UPLOAD;
              }

              this.setState({
                file_status: fileState,
                file_progress: result.percent,
                file_path_downloaded: result.filePathDownloaded
              });
            }
          }
        );

        const isNeedAutoDownloadFile =
          !this.isTCPFileType(messageData) &&
          this.isPhotoType(messageData) &&
          messageData.msgdtHasFile > 0 &&
          fileState != FILE_STATUS.DOWNLOADED &&
          fileState != FILE_STATUS.IN_PROGRESS_UPLOAD &&
          fileState != FILE_STATUS.ERROR_UPLOAD;

        if (isNeedAutoDownloadFile) {
          let lastFileState = downloadUploadManager.startDownloadIfCan(
            keyListener,
            messageData
          );
          this.setState({
            file_status: lastFileState.isDownload
              ? FILE_STATUS.IN_PROGRESS_DOWNLOAD
              : FILE_STATUS.IN_PROGRESS_UPLOAD,
            file_progress: lastFileState.percent,
            file_path_downloaded: fileObj.file
          });
        } else {
          let percent = 0;
          if (fileState == FILE_STATUS.IN_PROGRESS_DOWNLOAD) {
            const prevState = downloadUploadManager.tryGetProgressDownload(
              keyListener
            );
            if (prevState) {
              percent = prevState.percent;
            }
          }
          this.setState({
            file_status: fileState,
            file_progress: percent,
            file_path_downloaded: fileObj.file
          });
        }

        if (finishedCallBack) {
          finishedCallBack();
        }
      });
    } else {
      if (finishedCallBack) {
        finishedCallBack();
      }
    }
  }

  exportFileToFolder() {
    const { data } = this.props;
    const { dialog } = remote;
    let newFilePath = this.getCorrectFilePath();
    let fileName = data.msgdtFileName ?? '';
    fileName = fileName.normalize();
    if (fs.existsSync(newFilePath)) {
      dialog
        .showSaveDialog(remote.getCurrentWindow(), {
          defaultPath: fileName
        })
        .then(result => {
          if (result.canceled) {
            return;
          }

          let destPath = result.filePath;
          let extension = fileExtension(destPath);
          if (extension == null || extension == '') {
            destPath = `${destPath}.${fileExtension(fileName)}`;
          }

          fs.copyFile(newFilePath, destPath, () => {});
        });
    } else {
      if (data.msgdtHasFile <= 0) {
        return;
      }

      const ignoreSavePath = this.isPhotoType(data);

      dialog
        .showSaveDialog(remote.getCurrentWindow(), {
          defaultPath: fileName
        })
        .then(result => {
          if (result.canceled) {
            return;
          }
          const keyListener = keyOfMessage(data.msgRoomKey, data.msgNumberKey);

          let destPath = result.filePath;
          let extension = fileExtension(destPath);
          if (extension == null || extension == '') {
            destPath = `${destPath}.${fileExtension(fileName)}`;
          }

          const lastFileState = downloadUploadManager.startDownloadIfCan(
            keyListener,
            data,
            destPath,
            ignoreSavePath
          );
          this.setState({
            file_status: lastFileState.isDownload
              ? FILE_STATUS.IN_PROGRESS_DOWNLOAD
              : FILE_STATUS.IN_PROGRESS_UPLOAD,
            file_progress: lastFileState.percent,
            file_path_downloaded: destPath
          });
        });
    }
  }

  getCorrectFilePath() {
    const { data } = this.props;
    const { file_path_downloaded } = this.state;
    return (
      file_path_downloaded ??
      talkApi.filePathDownloadedWithName(
        data.msgRoomKey,
        generateFileNameDownloadedMessage(data.msgNumberKey, data.msgdtFileName)
      )
    );
  }

  getStatusFile(statusComponent) {
    let { file_status } = this.state;
    const isDoneFile =
      file_status == FILE_STATUS.DOWNLOADED ||
      file_status == FILE_STATUS.UPLOADED;
    const isErrorFile =
      file_status == FILE_STATUS.ERROR_DOWNLOAD ||
      file_status == FILE_STATUS.ERROR_UPLOAD;
    const isProgressFile =
      file_status == FILE_STATUS.IN_PROGRESS_DOWNLOAD ||
      file_status == FILE_STATUS.IN_PROGRESS_UPLOAD;
    const isErrorDownloadFile = file_status == FILE_STATUS.ERROR_DOWNLOAD;
    const isErrorUploadFile = file_status == FILE_STATUS.ERROR_UPLOAD;
    const {
      isDone,
      isErrorDownload,
      isErrorUpload,
      isError,
      isProgress,
      isOther
    } = statusComponent;

    if (isDoneFile) {
      return isDone;
    } else if (isErrorDownloadFile && isErrorDownload) {
      return isErrorDownload;
    } else if (isErrorUploadFile && isErrorUpload) {
      return isErrorUpload;
    } else if (isErrorFile) {
      return isError;
    } else if (isProgressFile) {
      return isProgress;
    } else {
      return isOther;
    }
  }

  fileContextMenuEvent = event => {
    const { closeContextMenu, onOpenContextMenu } = this.props;

    event.preventDefault();
    const { file_status } = this.state;
    const isProgressFile =
      file_status == FILE_STATUS.IN_PROGRESS_DOWNLOAD ||
      file_status == FILE_STATUS.IN_PROGRESS_UPLOAD;
    const isErrorUploadFile = file_status == FILE_STATUS.ERROR_UPLOAD;
    if (isProgressFile || isErrorUploadFile) {
      return;
    }

    const isDoneFile =
      file_status == FILE_STATUS.DOWNLOADED ||
      file_status == FILE_STATUS.UPLOADED;

    let { data, t } = this.props;
    let menuData = [];
    if (isDoneFile) {
      menuData.push(
        <MenuItem
          key="Share"
          onClick={() => {
            closeContextMenu();
            this.onForwardMessage(data, this.isPhotoType(data));
          }}
        >
          <Typography variant="caption">{t('Share')}</Typography>
        </MenuItem>
      );
      menuData.push(
        <MenuItem
          key="Show in Folder"
          onClick={() => {
            closeContextMenu();
            this.openFolderDownload();
          }}
        >
          <Typography variant="caption">{t('Show in Folder')}</Typography>
        </MenuItem>
      );
    }

    menuData.push(
      <MenuItem
        key="Save to Folder"
        onClick={() => {
          closeContextMenu();
          this.exportFileToFolder();
        }}
      >
        <Typography variant="caption">{t('Save to Folder')}</Typography>
      </MenuItem>
    );

    if (talkApi.apiSupportList.hasSupportClouddisk()) {
      if (!messsageUtils.isClouddiskFile(data)) {
        menuData.push(
          <MenuItem
            key="Save to Clouddisk"
            onClick={() => {
              closeContextMenu();
              const roomKey = data.msgRoomKey;
              if (roomKey && roomKey != '') {
                ipcRenderer.send(
                  SEND_SOCKET_API_EVENT,
                  API_SAVE_FILE_CLOUDDISK,
                  {
                    roomKey: roomKey,
                    numberKey: data.msgNumberKey,
                    timestamp: data.msgCreateDate
                  }
                );
              }
            }}
          >
            <Typography variant="caption">{t('Save to Clouddisk')}</Typography>
          </MenuItem>
        );
      }
    }
    onOpenContextMenu(event, menuData);
  };

  render() {
    const { type } = this.props;
    if (type == 0) {
      return this.renderPhoto();
    }
    return this.renderFileOther();
  }

  renderFileInfo() {
    let { data, classes } = this.props;
    let { msgdtFileName, msgdtFileSize } = data;
    return (
      <Fragment>
        <Typography
          variant="subtitle2"
          className={classes.textFileNameMe}
          classes={{ root: classes.titleFile }}
        >
          {msgdtFileName}
        </Typography>
        <Typography variant="caption" className={classes.descMessageMe}>
          {convertFileSize(msgdtFileSize)}
        </Typography>
      </Fragment>
    );
  }

  renderFileOther() {
    let { file_progress } = this.state;
    let { classes, data, t, userCached } = this.props;
    let { msgdtFileName } = data;

    const user = userCached?.[data.msgUserKey];

    const renderError = () => {
      return (
        <div className={classes.divFileOtherProgress}>
          <div style={{ flex: 1 }}>
            <Typography variant="caption" className={classes.textFileName}>
              {t('An error occurred, please try again')}
            </Typography>
          </div>
          <ReplayIcon
            onClick={this.retryDownloadUpload}
            className={classes.pedingIconVideo}
          />
        </div>
      );
    };

    const statusComponent = {
      isDone: (
        <div className={classes.divFileOtherProgress}>
          <span style={{ width: '100%' }}></span>
          <ForwardRightIcon
            className={classes.pedingIconVideo}
            onClick={() => this.onForwardMessage(data)}
          />
          <FolderOpenIcon
            onClick={this.openFolderDownload}
            className={classes.pedingIconVideo}
          />
          <VisibilityIcon
            onClick={this.onClickOtherFile}
            className={classes.pedingIconVideo}
          />
        </div>
      ),
      isErrorUpload: renderError(),
      isErrorDownload: renderError(),

      isProgress: (
        <div className={classes.divFileOtherProgress}>
          <span
            className={classes.chatFileDlProgress}
            style={{ width: '100%' }}
          >
            <span
              className={classes.progressBar}
              style={{ width: file_progress + '%' }}
            />
          </span>
          <HighlightOffIcon
            onClick={this.stopDownloadUpload}
            className={classes.pedingIconVideo}
          />
        </div>
      ),
      isOther: (
        <div className={classes.divFileOtherProgress}>
          <span style={{ width: '100%' }}></span>
          <GetAppIcon
            onClick={this.onClickOtherFileWithSaveDialog}
            className={classes.pedingIconVideo}
          />
        </div>
      )
    };

    return (
      <div
        className={classes.cellOtherFile}
        onContextMenu={this.fileContextMenuEvent}
      >
        <div className={classes.titleFile}>
          <Typography
            style={{ flex: 1 }}
            variant="subtitle2"
            color="textSecondary"
          >
            {user?.displayName ?? ''}
          </Typography>
          <Typography variant="caption" color="textSecondary">
            {moment(data.msgCreateDate).format('HH:mm')}
          </Typography>
        </div>
        <div className={classes.viewFileOther}>
          <img
            src={getImg(msgdtFileName.split('.').pop())}
            alt=""
            className={classes.icoFile}
          />
          <div className={classes.contentFileOther}>
            {this.renderFileInfo()}
          </div>
        </div>
        {this.getStatusFile(statusComponent)}
      </div>
    );
  }

  renderPhoto() {
    const { data, classes, userCached } = this.props;
    let { file_progress, file_status, isMenu } = this.state;
    const roomKey = data.msgRoomKey;
    const msgNumberKey = data.msgNumberKey;
    const isGif = messsageUtils.isGIF(data);
    const thumbPath = isGif
      ? this.getCorrectFilePath()
      : talkApi.filePathThumbnailWithName(
          roomKey,
          generateThumbFileNameMessage(msgNumberKey)
        );

    const renderError = () => {
      return (
        <div className={classes.viewImage}>
          <img
            key={file_status}
            className={classes.imgError}
            src={'./images/broken-image.png'}
            alt=""
          />
          <span className={classes.menuPhotoError}>
            <ReplayIcon
              className={classes.pedingIcon}
              onClick={this.retryDownloadUpload}
            />
          </span>
        </div>
      );
    };

    const statusComponent = {
      isDone: (
        <div className={classes.viewImage}>
          <img
            key={file_status}
            onClick={this.onClickOtherFile}
            className={isGif ? classes.imgGif : classes.img}
            src={ensureFilePath(thumbPath)}
            alt=""
          />

          {isMenu ? (
            <span className={classes.menuImage}>
              <Fragment>
                <ForwardRightIcon
                  className={classes.pedingIcon}
                  onClick={() => this.onForwardMessage(data, true)}
                />
                <FolderOpenIcon
                  className={classes.pedingIcon}
                  onClick={this.openFolderDownload}
                />
                <VisibilityIcon
                  className={classes.pedingIcon}
                  onClick={this.onClickOtherFile}
                />
              </Fragment>
            </span>
          ) : null}
        </div>
      ),
      isErrorUpload: renderError(),
      isErrorDownload: renderError(),
      isProgress: (
        <div>
          <ImgPlaceHolder
            isGif={isGif}
            classes={classes}
            height={this.height}
            thumbPath={ensureFilePath(thumbPath)}
          ></ImgPlaceHolder>
          <div className={classes.divFileOtherProgress}>
            <span
              className={classes.chatFileDlProgress}
              style={{ width: '100%' }}
            >
              <span
                className={classes.progressBar}
                style={{ width: file_progress + '%' }}
              />
            </span>
            <HighlightOffIcon
              onClick={this.stopDownloadUpload}
              className={classes.iconCloseProgress}
            />
          </div>
        </div>
      ),
      isOther: (
        <Skeleton
          variant="rect"
          height={this.height}
          style={{ borderRadius: 8 }}
        />
      )
    };

    const user = userCached?.[data.msgUserKey];

    return (
      <div
        className={classes.cellImage}
        onMouseEnter={() => this.setState({ isMenu: true })}
        onMouseLeave={() => this.setState({ isMenu: false })}
        onContextMenu={this.fileContextMenuEvent}
      >
        <div
          style={{ backgroundColor: 'transparent', padding: 0, width: '100%' }}
        >
          <div className={classes.titleFile}>
            <Typography
              style={{ flex: 1 }}
              variant="subtitle2"
              color="textSecondary"
            >
              {user?.displayName ?? ''}
            </Typography>
            <Typography variant="caption" color="textSecondary">
              {moment(data.msgCreateDate).format('HH:mm')}
            </Typography>
          </div>

          <span className={classes.imgArea}>
            {this.getStatusFile(statusComponent)}
          </span>
        </div>
      </div>
    );
  }
}

class DateMessage extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { value, classes } = this.props;
    return (
      <div className={classes.cellLabel}>
        {moment(value).format(talkApi.dateTimeFormat())}
      </div>
    );
  }
}

class ImgPlaceHolder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageExist: false
    };
  }

  componentDidMount() {
    const { thumbPath } = this.props;
    const exist = fs.existsSync(thumbPath);
    this.setState({
      imageExist: exist
    });
  }

  render() {
    const { classes, thumbPath, height, isGif } = this.props;
    const { imageExist } = this.state;
    return (
      <Fragment>
        {imageExist ? (
          <img
            className={isGif ? classes.imgGif : classes.img}
            style={{ height: height }}
            src={thumbPath}
            alt=""
          />
        ) : (
          <Skeleton
            variant="rect"
            height={height}
            style={{ borderRadius: 8 }}
          />
        )}
      </Fragment>
    );
  }
}
