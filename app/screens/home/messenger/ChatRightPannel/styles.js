import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    boxContainer: {
      width: 300,
      height: '100%',
      borderLeftWidth: 1,
      borderLeftColor: theme.palette.divider,
      borderLeftStyle: 'solid'
    },
    contentTop: {
      display: 'flex',
      flexDirection: 'row',
      borderBottomWidth: 1,
      borderBottomColor: theme.palette.divider,
      borderBottomStyle: 'solid',
      minHeight: 70,
      height: 70,
      width: '100%'
    },
    tabContainer: {
      flexDirection: 'column',
      display: 'flex',
      flex: 1,
      height: 70
    },
    tabText: {
      textAlign: 'center',
      color: theme.palette.type == 'light' ? 'rgba(0, 0, 0, 0.87)' : '#fff'
    },
    selectedLine: {
      height: 4,
      backgroundColor: theme.palette.primary.main,
      display: 'flex'
    },
    tabPanel: {
      width: '100%',
      height: 'calc(100% - 70px)'
    }
  })
);
