import React, { useState, useEffect, useContext } from 'react';
import OrgTree from '../../../../components/OrgTree';
import SearchResult from '../../../../components/SearchResult';
import { TAB_CATEGORY } from '../../../../configs/constant';
import SearchIcon from '@material-ui/icons/Search';
import { InputBase, IconButton, MenuItem, Typography } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';
import { useStyles } from './styles';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../../../actions';
import { ipcRenderer } from 'electron';
import {
  REQUEST_COMMON_ACTION_FROM_RENDER,
  ACTION_QUERY_ORG_ROOM_WITH_TEXT
} from '../../../../configs/constant';

import { MenuOptionContext } from '../../index';

export default function FavoriteOrgList() {
  const { onOpen, onClose, onOpenCreateGroupDialog } = useContext(
    MenuOptionContext
  );
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const classes = useStyles();
  const listActualyFav = useSelector(state => state.company.listActualyFav);
  const org_fav = useSelector(state => state.company.org_fav);
  const appUI = useSelector(state => state.appUI);
  const org_time_card = useSelector(state => state.company.org_time_card);
  const user_cached = useSelector(state => state.company.user_cached);
  const user = useSelector(state => state.auth.user.account_info);
  const search_text = useSelector(state => state.search_result.search_text);
  const allSeachResult = useSelector(
    state => state.search_result.allSeachResult
  );

  const commonSettings = useSelector(state => state.setting.common);

  const [searchFocused, setSearchFocused] = useState(false);

  const [search, setSearch] = useState('');

  const openOneRoom = (category, primaryKey, room, users, newTab, settings) => {
    dispatch(
      Actions.openOneRoom(category, primaryKey, room, users, newTab, settings)
    );
  };

  const saveScrollOffset = (category, curScrollTop) => {
    dispatch(Actions.saveScrollOffset(category, curScrollTop));
  };

  const updateTab = room => {
    dispatch(Actions.updateTab(room));
  };

  const onChange = value => {
    setSearch(value);
    if (value == '') {
      dispatch(Actions.showSearchResult(commonSettings, false));
    } else {
      if (!appUI.show_search_result) {
        dispatch(Actions.showSearchResult(commonSettings, true, value));
      }
    }
  };

  useEffect(() => {
    let timer1 = setTimeout(() => {
      ipcRenderer.send(
        REQUEST_COMMON_ACTION_FROM_RENDER,
        ACTION_QUERY_ORG_ROOM_WITH_TEXT,
        search
      );
    }, 500);
    return () => {
      clearTimeout(timer1);
    };
  }, [search]);

  return (
    <div className={classes.main}>
      <div className={classes.search}>
        <IconButton
          size="small"
          className={
            searchFocused ? classes.searchIconHightLight : classes.searchIcon
          }
        >
          <SearchIcon fontSize="small" />
        </IconButton>
        <InputBase
          className={classes.inputSearch}
          style={{ height: 25 }}
          placeholder={searchFocused ? null : t('Search')}
          onFocus={() => {
            setSearchFocused(true);
          }}
          onBlur={() => {
            setSearchFocused(false);
          }}
          value={appUI.show_search_result ? search : ''}
          onChange={event => onChange(event.target.value)}
        />
        <IconButton
          size="small"
          className={
            searchFocused ? classes.clearIconHightLight : classes.clearIcon
          }
          onClick={() => onChange('')}
        >
          <ClearIcon fontSize="small" />
        </IconButton>
      </div>
      <div className={classes.content}>
        <div
          className={
            appUI.show_search_result ? classes.layoutShow : classes.layoutHidden
          }
        >
          {appUI.show_search_result ? (
            allSeachResult.length > 0 ? (
              <SearchResult
                listActualyFav={listActualyFav}
                search_text={search_text}
                allSeachResult={allSeachResult}
                org_time_card={org_time_card}
                user_cached={user_cached}
                user={user}
                appUI={appUI}
                openOneRoom={openOneRoom}
                updateTab={updateTab}
                commonSettings={commonSettings}
              />
            ) : (
              <div className={classes.center}>
                <div className="han-loading mini" />
              </div>
            )
          ) : null}
        </div>
        <div
          className={classes.layout}
          onContextMenu={event => {
            event.preventDefault();
            event.stopPropagation();
            const menuData = [
              <MenuItem
                key="creategroup"
                onClick={() => {
                  onClose();
                  onOpenCreateGroupDialog(null);
                }}
              >
                <div className={classes.memuItem}>
                  <Typography variant="caption">{t('Create group')}</Typography>
                </div>
              </MenuItem>
            ];
            onOpen(event, menuData);
          }}
        >
          <OrgTree
            commonSettings={commonSettings}
            listActualyFav={listActualyFav}
            org={org_fav}
            appUI={appUI}
            category={TAB_CATEGORY.tab_favorite}
            saveScrollOffset={saveScrollOffset}
          />
        </div>
      </div>
    </div>
  );
}
