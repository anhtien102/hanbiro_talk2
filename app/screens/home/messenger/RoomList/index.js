import React, { Component, useState, useEffect, Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { AutoSizer, List } from 'react-virtualized';
import withStyles from './styles';
import RoomInfor from '../../../../components/RoomInfor';
import UpdateRoomTitleDialog from '../../../../components/UpdateRoomTitleDialog';
import SearchResult from '../../../../components/SearchResult';
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
import { MenuItem, InputBase, IconButton, Typography } from '@material-ui/core';
import {
  TAB_CATEGORY,
  GETTALK_COUNT,
  COMMON_SYNC_ACTION_FROM_RENDER,
  ACTION_SYNC_GET_UNREAD_MESSAGE_POSITION,
  REQUEST_COMMON_ACTION_FROM_RENDER,
  ACTION_UPDATE_ALL_MESSAGE_AS_READ_AND_SEND_API,
  ACTION_QUERY_ORG_ROOM_WITH_TEXT,
  SEND_SOCKET_API_EVENT,
  API_CHANGE_ALERT_ROOM,
  API_ROOM_OUT,
  API_CHANGE_ROOM_NAME,
  API_CHANGE_ROOM_FAVORITE,
  ACTION_OPEN_CHAT_WINDOWS
} from '../../../../configs/constant';
import { ROOM_DETAIL_STATUS_ENTER } from '../../../../core/service/talk-constants';
import { ipcRenderer, remote } from 'electron';
import { MenuOptionContext } from '../../../home';
import { withTranslation } from 'react-i18next';

import talkAPI from '../../../../core/service/talk.api.render';

import SimpleBarHanbiro from '../../../../components/SimpleBarHanbiro';

const showNewChatWindows = false;

const ROOM_HEIGHT = 70;
class RoomList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      changeRoomTitleDialog: false,
      roomInfo: {}
    };

    this.category = TAB_CATEGORY.tab_room_list;
    this.state = { selectedRoom: null };
    this.listRef = React.createRef();
    this.parentRef = React.createRef();
    this.containerRef = React.createRef();
    this.onScroll = this.onScroll.bind(this);
    this.onRoomClick = this.onRoomClick.bind(this);
    this.getUserInfo = this.getUserInfo.bind(this);
    this.curScrollTop = 0;

    this.simpleBar = null;
  }

  static contextType = MenuOptionContext;

  findIndexOfCurrentRoom() {
    const { selectedRoom } = this.state;
    const { rooms } = this.props;
    let sIndex = -1;
    if (selectedRoom) {
      sIndex = rooms.findIndex(item => {
        return item.rRoomKey == selectedRoom.rRoomKey;
      });
    }
    return sIndex;
  }

  componentDidMount() {
    const { appUI } = this.props;

    const sIndex = this.findIndexOfCurrentRoom();
    this.simpleBar = new SimpleBarHanbiro(this.parentRef.current);

    if (appUI) {
      let scrollData = appUI.scroll_offset;
      let scrollOffset = scrollData ? scrollData[this.category] : 0;
      this.curScrollTop = scrollOffset ?? 0;
      let realScrollTop = this.curScrollTop;
      if (sIndex < 0) {
        realScrollTop = this.curScrollTop;
      } else {
        const visibleTop = sIndex * ROOM_HEIGHT;
        const visibleHeight = this.containerRef.current.clientHeight;

        if (this.curScrollTop > visibleTop) {
          realScrollTop = visibleTop;
        } else if (
          visibleTop - this.curScrollTop + ROOM_HEIGHT >
          visibleHeight
        ) {
          realScrollTop = visibleTop - visibleHeight + ROOM_HEIGHT;
        } else {
          realScrollTop = this.curScrollTop;
        }
      }

      setImmediate(() => {
        this.simpleBar.scrollTo(realScrollTop);
      });
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let room = null;
    if (nextProps.appUI && nextProps.appUI.selected_room) {
      let selectedRoom =
        nextProps.appUI.selected_room[TAB_CATEGORY.tab_room_list];
      room = selectedRoom ? selectedRoom.room : null;
    }

    if (
      !prevState.selectedRoom ||
      !room ||
      prevState.selectedRoom.rRoomKey != room.rRoomKey
    ) {
      return {
        selectedRoom: room
      };
    }
    return null;
  }

  componentWillUnmount() {
    const { saveScrollOffset } = this.props.actions;
    if (saveScrollOffset) {
      saveScrollOffset(this.category, this.curScrollTop);
    }
  }

  onRoomClick(item, newTab) {
    const { actions, commonSettings } = this.props;
    const { selectedRoom } = this.state;
    const lastRoomKey = selectedRoom ? selectedRoom.rRoomKey : null;

    if (item.rRoomKey == lastRoomKey) {
      if (newTab) {
        actions.updateTab(item);
      }
      return;
    }

    const notRead =
      item.rRoomIRead <= 0 &&
      item.unReadMessage > 0 &&
      item.rRoomUnreadMsgServer < GETTALK_COUNT;

    let messagePosition = null;
    let unreadCount = 0;
    if (notRead) {
      unreadCount = item.rRoomUnreadMsgServer;
      // messagePosition = ipcRenderer.sendSync(
      //   COMMON_SYNC_ACTION_FROM_RENDER,
      //   ACTION_SYNC_GET_UNREAD_MESSAGE_POSITION,
      //   {
      //     roomKey: item.rRoomKey
      //   }
      // );
    }

    remote.getGlobal('ShareKeepValue').CURRENT_ROOM_KEY = item.rRoomKey;

    ipcRenderer.send(
      REQUEST_COMMON_ACTION_FROM_RENDER,
      ACTION_UPDATE_ALL_MESSAGE_AS_READ_AND_SEND_API,
      {
        roomId: item.rID,
        roomKey: item.rRoomKey
      }
    );
    actions.openOneRoom(
      this.category,
      item.rRoomKey,
      item,
      null,
      newTab,
      commonSettings,
      { messagePosition: messagePosition, unReadCount: unreadCount }
    );
  }

  onScroll(e) {
    const { scrollTop, scrollLeft } = e.target;

    const { Grid: grid } = this.listRef.current;

    grid.handleScrollEvent({ scrollTop, scrollLeft });

    this.curScrollTop = scrollTop;
  }

  getUserInfo(userCached, msgDetail) {
    if (userCached[msgDetail.rdtUKey]) {
      return userCached[msgDetail.rdtUKey];
    } else {
      return {
        primaryKey: msgDetail.rdtUKey,
        userId: -1,
        userAlias: msgDetail.rdtUName,
        displayName: msgDetail.rdtUName,
        userKey: msgDetail.rdtUKey,
        userPhotoTime: -1
      };
    }
  }

  render() {
    const { search, changeRoomTitleDialog, roomInfo } = this.state;
    const {
      actions,
      classes,
      rooms,
      user,
      user_cached,
      appUI,
      org_time_card,
      search_text,
      allSeachResult,
      listActualyFav,
      commonSettings,
      t
    } = this.props;

    const Row = ({ index, key, style }) => {
      const item = rooms[index];

      return (
        <RoomInfor
          style={style}
          key={key}
          data={item}
          user={user}
          user_cached={user_cached}
          selectedRoom={this.state.selectedRoom}
          onClick={() => this.onRoomClick(item, null)}
          onOpenMenu={event => {
            const offNotification = item.rRoomPushAlert == 0;
            const supportChangeRoomName = talkAPI.apiSupportList.hasSupportChangeRoomName();
            const allowRoomDelete = talkAPI.allowRoomDelete();
            const supportRoomFavorite = talkAPI.apiSupportList.hasSupportRoomFavorite();
            const offFavorite = item.rRoomFavorite <= 0;
            const menuData = [
              showNewChatWindows && (
                <MenuItem
                  key="Open new room"
                  onClick={() => {
                    this.context.onClose();
                    ipcRenderer.send(
                      REQUEST_COMMON_ACTION_FROM_RENDER,
                      ACTION_OPEN_CHAT_WINDOWS,
                      { roomKey: item.rRoomKey }
                    );
                  }}
                >
                  <div className={classes.memuItem}>
                    <Typography variant="caption">
                      {t('Open new Window')}
                    </Typography>
                  </div>
                </MenuItem>
              ),
              commonSettings.use_new_tab && (
                <MenuItem
                  key="Open new Tab"
                  onClick={() => {
                    this.context.onClose();
                    this.onRoomClick(item, true);
                  }}
                >
                  <div className={classes.memuItem}>
                    <Typography variant="caption">
                      {t('Open new Tab')}
                    </Typography>
                  </div>
                </MenuItem>
              ),
              supportChangeRoomName ? (
                <MenuItem
                  key="Change room title"
                  onClick={() => {
                    this.context.onClose();
                    this.setState({
                      roomInfo: item,
                      changeRoomTitleDialog: true
                    });
                  }}
                >
                  <div className={classes.memuItem}>
                    <Typography variant="caption">
                      {t('Change room title')}
                    </Typography>
                  </div>
                </MenuItem>
              ) : null,
              allowRoomDelete ? (
                <MenuItem
                  key="Leave room"
                  onClick={() => {
                    this.context.onClose();
                    ipcRenderer.send(SEND_SOCKET_API_EVENT, API_ROOM_OUT, {
                      roomKey: item.rRoomKey
                    });
                  }}
                >
                  <div className={classes.memuItem}>
                    <Typography variant="caption">{t('Leave room')}</Typography>
                  </div>
                </MenuItem>
              ) : null,
              <MenuItem
                key="Mark all as read"
                onClick={() => {
                  this.context.onClose();
                  ipcRenderer.send(
                    REQUEST_COMMON_ACTION_FROM_RENDER,
                    ACTION_UPDATE_ALL_MESSAGE_AS_READ_AND_SEND_API,
                    {
                      roomId: item.rID,
                      roomKey: item.rRoomKey
                    }
                  );
                }}
              >
                <div className={classes.memuItem}>
                  <Typography variant="caption">
                    {t('Mark all as read')}
                  </Typography>
                </div>
              </MenuItem>,
              <MenuItem
                key="Turn off new message alert"
                onClick={() => {
                  this.context.onClose();
                  ipcRenderer.send(
                    SEND_SOCKET_API_EVENT,
                    API_CHANGE_ALERT_ROOM,
                    {
                      roomKey: item.rRoomKey,
                      enable: !(item.rRoomPushAlert > 0)
                    }
                  );
                }}
              >
                <div className={classes.memuItem}>
                  <Typography variant="caption">
                    {offNotification
                      ? t('Turn on new message alert')
                      : t('Turn off new message alert')}
                  </Typography>
                </div>
              </MenuItem>,
              supportRoomFavorite ? (
                <MenuItem
                  key="On Off Room favorite"
                  onClick={() => {
                    this.context.onClose();
                    ipcRenderer.send(
                      SEND_SOCKET_API_EVENT,
                      API_CHANGE_ROOM_FAVORITE,
                      {
                        roomKey: item.rRoomKey,
                        isAdd: offFavorite
                      }
                    );
                  }}
                >
                  <div className={classes.memuItem}>
                    <Typography variant="caption">
                      {offFavorite
                        ? t('Add to Favorites')
                        : t('Remove from Favorite')}
                    </Typography>
                  </div>
                </MenuItem>
              ) : null,
              <MenuItem
                key="Send Whisper"
                onClick={() => {
                  this.context.onClose();
                  const { user_cached, user } = this.props;
                  const userKey = user.user_key;
                  let listActiveUser = [];

                  for (let detail of item.roomDetailds) {
                    if (
                      detail.rdtUKey != userKey &&
                      detail.rdtStatus == ROOM_DETAIL_STATUS_ENTER
                    ) {
                      listActiveUser.push(
                        this.getUserInfo(user_cached, detail)
                      );
                    }
                  }
                  this.context.onOpenWhisperDialog(listActiveUser);
                }}
              >
                <div className={classes.memuItem}>
                  <Typography variant="caption">{t('Send Whisper')}</Typography>
                </div>
              </MenuItem>
            ];
            this.context.onOpen(event, menuData);
          }}
        />
      );
    };

    return (
      <div className={classes.main}>
        <div className={classes.search}>
          <SearchInput classes={classes} Actions={actions} />
        </div>
        <div className={classes.content}>
          <div
            className={
              appUI.show_search_result
                ? classes.layoutShow
                : classes.layoutHidden
            }
          >
            {appUI.show_search_result ? (
              allSeachResult.length > 0 ? (
                <SearchResult
                  listActualyFav={listActualyFav}
                  search_text={search_text}
                  allSeachResult={allSeachResult}
                  org_time_card={org_time_card}
                  user_cached={user_cached}
                  user={user}
                  appUI={appUI}
                  openOneRoom={actions.openOneRoom}
                  updateTab={actions.updateTab}
                  commonSettings={commonSettings}
                />
              ) : (
                <div className={classes.center}>
                  <div className="han-loading mini" />
                </div>
              )
            ) : null}
          </div>
          <div className={classes.layout}>
            <div className={classes.mainList} ref={this.containerRef}>
              <AutoSizer>
                {({ height, width }) => {
                  return (
                    <div
                      ref={this.parentRef}
                      onScroll={this.onScroll}
                      style={{
                        position: 'relative',
                        width: width,
                        height: height
                      }}
                    >
                      <List
                        style={{
                          overflowX: false,
                          overflowY: false,
                          outline: 'none'
                        }}
                        ref={this.listRef}
                        height={height}
                        rowCount={rooms.length}
                        rowHeight={ROOM_HEIGHT}
                        width={width}
                        rowRenderer={Row}
                      ></List>
                    </div>
                  );
                }}
              </AutoSizer>
            </div>
          </div>
        </div>
        {changeRoomTitleDialog && (
          <UpdateRoomTitleDialog
            show={true}
            roomInfo={roomInfo}
            onUpdate={(obj, applyAll, title) => {
              ipcRenderer.send(SEND_SOCKET_API_EVENT, API_CHANGE_ROOM_NAME, {
                roomKey: obj.rRoomKey,
                isPublic: applyAll,
                name: title
              });
              this.setState({ changeRoomTitleDialog: false, roomInfo: {} });
            }}
            onClose={() => {
              this.setState({ changeRoomTitleDialog: false, roomInfo: {} });
            }}
          />
        )}
      </div>
    );
  }
}

export default withTranslation()(withStyles(RoomList));

function SearchInput(props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { Actions, classes } = props;
  const appUI = useSelector(state => state.appUI);
  const commonSettings = useSelector(state => state.setting.common);

  const [search, setSearch] = useState('');
  const [searchFocused, setSearchFocused] = useState(false);

  const onChange = value => {
    setSearch(value);
    if (value == '') {
      dispatch(Actions.showSearchResult(commonSettings, false));
    } else {
      if (!appUI.show_search_result) {
        dispatch(Actions.showSearchResult(commonSettings, true, value));
      }
    }
  };

  useEffect(() => {
    let timer1 = setTimeout(() => {
      ipcRenderer.send(
        REQUEST_COMMON_ACTION_FROM_RENDER,
        ACTION_QUERY_ORG_ROOM_WITH_TEXT,
        search
      );
    }, 500);
    return () => {
      clearTimeout(timer1);
    };
  }, [search]);

  return (
    <Fragment>
      <IconButton
        size="small"
        className={
          searchFocused ? classes.searchIconHightLight : classes.searchIcon
        }
      >
        <SearchIcon fontSize="small" />
      </IconButton>
      <InputBase
        className={classes.inputSearch}
        style={{ height: 25 }}
        onFocus={() => {
          setSearchFocused(true);
        }}
        onBlur={() => {
          setSearchFocused(false);
        }}
        placeholder={searchFocused ? null : t('Search')}
        value={appUI.show_search_result ? search : ''}
        onChange={event => onChange(event.target.value)}
      />
      <IconButton
        size="small"
        className={
          searchFocused ? classes.clearIconHightLight : classes.clearIcon
        }
        onClick={() => onChange('')}
      >
        <ClearIcon fontSize="small" />
      </IconButton>
    </Fragment>
  );
}
