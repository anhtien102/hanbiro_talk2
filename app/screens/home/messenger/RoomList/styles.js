import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  main: {
    display: 'flex',
    width: '100%',
    height: '100%',
    flexDirection: 'column'
  },
  mainList: {
    width: '100%',
    height: '100%',
    position: 'relative',
    display: 'flex'
  },
  fill_parent: {
    flex: 1
  },
  circleAvatar: {
    borderRadius: '50%'
  },
  cell_container: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'red'
  },
  cell_photo_container: {
    width: '50px',
    height: '50px',
    backgroundColor: 'blue'
  },
  cell_room_info_container: {
    width: ' calc(100% - 50px)',
    flex: 1,
    flexDirection: 'column',
    display: 'flex',
    height: '50px',
    backgroundColor: 'yellow'
  },
  cell_room_info_title: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    height: '25px',
    backgroundColor: '#00ff00'
  },
  cell_room_info_last_msg: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    height: '25px',
    backgroundColor: '#00ff00'
  },
  content: {
    width: '100%',
    height: '100%',
    display: 'flex',
    position: 'relative',
    flex: 1
  },
  layout: {
    zIndex: 1,
    position: 'absolute',
    backgroundColor: theme.palette.background.default,
    width: '100%',
    height: '100%',
    display: 'flex'
  },
  layoutHidden: {
    zIndex: 0,
    position: 'absolute',
    backgroundColor: theme.palette.background.default,
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  layoutShow: {
    zIndex: 99,
    position: 'absolute',
    backgroundColor: theme.palette.background.default,
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  center: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex'
  },
  memuItem: {
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex',
    flexDirection: 'row'
  },
  inputSearch: { fontSize: theme.typography.fontSize, width: '100%' },
  search: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderBottomColor: theme.palette.divider,
    borderBottomWidth: 1,
    borderBottomStyle: 'solid',
    padding: 4
  },
  searchIcon: {
    marginLeft: 4
  },
  searchIconHightLight: {
    marginLeft: 4,
    color: theme.palette.primary.main
  },
  clearIcon: {
    marginRight: 4
  },
  clearIconHightLight: {
    marginRight: 4,
    color: theme.palette.primary.main
  }
});

export default withStyles(styles);
