import { useStyles } from './styles';
import React, {
  Component,
  createRef,
  useEffect,
  useRef,
  useState
} from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Typography, ListItem, Button, List, Fab } from '@material-ui/core';
import UserPhotoView from '../../../../../components/UserPhoto';
import Api from '../../../../../core/service/api';
import talkAPI from '../../../../../core/service/talk.api.render';
import BoardUtils from '../../../../../core/model/BoardUtils';
import SimplebarHanbiro from '../../../../../components/SimpleBarHanbiro';

import {
  BoardRefreshIcon,
  BoardWriteIcon
} from '../../../../../components/HanSVGIcon';
export default function BoardFileListComponent({
  roomKey,
  onOpenDetail,
  onOpenWrite
}) {
  const classes = useStyles();
  const { t } = useTranslation();
  const userCached = useSelector(state => state.company.user_cached);
  const cachedBoardFileList = useRef([]);
  const [boardFileList, setBoardFileList] = useState([]);
  const [loading, setLoading] = useState(false);
  const isRequesting = useRef(false);
  const firstTimeRef = useRef();
  const canLoadMore = useRef(false);
  const tagRequest = useRef();

  const startGetBoardFileList = async (roomKey, regTime, append) => {
    try {
      isRequesting.current = true;
      tagRequest.current = `startGetBoardFileList_${Date.now()}`;
      const response = await Api.postRawBody(
        talkAPI.boardFileList(),
        BoardUtils.rawBodyGetBoardFileList(roomKey, regTime, true),
        BoardUtils.rawAuthorizedHeader(),
        tagRequest.current
      );
      isRequesting.current = false;
      const resultObj = BoardUtils.boardFileListFromXML(response);
      setLoading(false);
      if (append) {
        const list = cachedBoardFileList.current;
        let nList = resultObj.boardFileListResult;
        // Lat item of old list isEqual with first item of new list
        if (list.length > 0 && nList.length > 0) {
          const item = list[list.length - 1];
          const fItem = nList[0];
          if (item.fileKey == fItem.fileKey) {
            nList = nList.filter((e, index) => index > 0);
          }
        }

        canLoadMore.current = nList.length > 0;

        cachedBoardFileList.current = [...list, ...nList];
      } else {
        cachedBoardFileList.current = resultObj.boardFileListResult;
        canLoadMore.current = true;
      }
      setBoardFileList(cachedBoardFileList.current);

      firstTimeRef.current = resultObj.firstTime;
    } catch (error) {
      if (Api.isCancel(error)) {
        return;
      }
      isRequesting.current = false;
      setLoading(false);
      console.log('startGetBoardFileList', error);
    }
  };

  useEffect(() => {
    const latestTime = parseInt((Date.now() + 86400) / 1000);
    setLoading(true);
    cachedBoardFileList.current = [];
    startGetBoardFileList(roomKey, latestTime, false);

    return () => {
      Api.cancelRequest(tagRequest.current);
    };
  }, []);

  const refresh = () => {
    Api.cancelRequest(tagRequest.current);
    const latestTime = parseInt((Date.now() + 86400) / 1000);
    setLoading(true);
    setBoardFileList([]);
    cachedBoardFileList.current = [];
    startGetBoardFileList(roomKey, latestTime, false);
  };

  const loadMore = () => {
    // TODO dont have load more API
    // if (isRequesting.current || !canLoadMore.current) {
    //   return;
    // }
    // startGetBoardFileList(roomKey, firstTimeRef.current, true);
  };

  const showNodata =
    !loading && !isRequesting.current && boardFileList.length == 0;

  return (
    <>
      <WrapperList
        classes={classes}
        loadMore={loadMore}
        boardFileList={boardFileList}
        userCached={userCached}
        onOpenDetail={onOpenDetail}
      />
      {showNodata ? (
        <Typography
          style={{
            position: 'absolute',
            width: '100%',
            top: '50%',
            textAlign: 'center'
          }}
          variant="body2"
        >
          {t('no_result')}
        </Typography>
      ) : null}
      {loading ? <div className="han-loading mini" /> : null}
      <div style={{ position: 'absolute', bottom: 10, right: 10 }}>
        <Fab
          color="primary"
          aria-label="refresh"
          size="small"
          onClick={() => {
            refresh();
          }}
        >
          <BoardRefreshIcon fontSize="small" />
        </Fab>
        <Fab
          color="secondary"
          style={{ marginLeft: 5 }}
          aria-label="write"
          size="small"
          onClick={() => {
            onOpenWrite();
          }}
        >
          <BoardWriteIcon fontSize="small" />
        </Fab>
      </div>
    </>
  );
}

class WrapperList extends Component {
  constructor(props) {
    super(props);
    this.simplebar = null;
    this.listRef = createRef();
    this.onScroll = this.onScroll.bind(this);
  }

  componentDidMount() {
    this.simplebar = new SimplebarHanbiro(this.listRef.current);
  }

  onScroll(e) {
    let element = e.target;
    const diffScrollTop = element.scrollHeight - element.clientHeight;
    const scrollAtBottom = diffScrollTop - element.scrollTop <= 0;
    if (scrollAtBottom) {
      this.props.loadMore();
    }
  }

  render() {
    const { classes, boardFileList, userCached, onOpenDetail } = this.props;

    return (
      <List
        ref={this.listRef}
        className={classes.list}
        onScroll={this.onScroll}
      >
        <div>
          {boardFileList.map(item => {
            return <BoardNotificationItem key={item.fileKey} item={item} />;
          })}
        </div>
      </List>
    );
  }
}

function BoardNotificationItem({ item }) {
  const classes = useStyles();

  return (
    <ListItem button className={classes.rootComponent}>
      {item.thumbnail ? (
        <img
          style={{
            maxWidth: '100%',
            minWidth: '100%',
            maxHeight: 300,
            borderRadius: 10
          }}
          src={`data:image/png;base64,${item.thumbnail}`}
        />
      ) : (
        <img
          style={{
            maxWidth: '100%',
            minWidth: '100%',
            maxHeight: 300,
            borderRadius: 10
          }}
          src={'./images/broken-image.png'}
        />
      )}
    </ListItem>
  );
}
