import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    list: {
      overflowY: 'auto',
      margin: 0,
      padding: 0,
      listStyle: 'none',
      height: '100%',
      width: '100%'
    },
    rootComponent: {
      width: '96%',
      margin: '10px 10px',
      padding: '0px',
      borderRadius: 10,
      backgroundColor: 'grey'
    }
  })
);
