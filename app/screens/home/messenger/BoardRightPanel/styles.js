import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    boxContainer: {
      width: 300,
      height: '100%',
      position: 'relative',
      borderLeftWidth: 1,
      borderLeftColor: theme.palette.divider,
      borderLeftStyle: 'solid'
    },
    contentTop: {
      display: 'flex',
      flexDirection: 'row',
      borderBottomWidth: 1,
      borderBottomColor: theme.palette.divider,
      borderBottomStyle: 'solid',
      minHeight: 70,
      height: 70,
      width: '100%'
    },
    tabContainer: {
      display: 'flex',
      flexDirection: 'column',
      flex: 1,
      height: 70
    },
    tabContainerSub: {
      padding: '0px',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      minHeight: 50,
      height: 50,
      flex: 1
    },
    tabText: {
      textAlign: 'center',
      marginTop: '6px',
      fontWeight: 'normal',
      color: theme.palette.type == 'light' ? 'rgba(0, 0, 0, 0.87)' : '#fff'
    },
    selectedLine: {
      height: 4,
      minHeight: 4,
      backgroundColor: theme.palette.primary.main,
      display: 'flex'
    },
    tabPanel: {
      width: '100%',
      height: 'calc(100% - 70px)'
    },

    comment: {
      display: 'flex',
      alignItems: 'center',
      backgroundColor: 'lightblue',
      flexDirection: 'row',
      alignContent: 'center',
      justifyContent: 'center'
    },
    iconSelected: {
      height: '26px',
      width: '26px',
      color: theme.palette.primary.main
    },
    icon: {
      height: '26px',
      width: '26px'
    },
    list: {
      overflowY: 'auto',
      margin: 0,
      padding: 0,
      listStyle: 'none',
      height: '100%',
      '&::-webkit-scrollbar': {
        width: '0.4em'
      },
      '&::-webkit-scrollbar-track': {
        boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
        webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)'
      },
      '&::-webkit-scrollbar-thumb': {
        backgroundColor: 'rgba(0,0,0,.1)',
        outline: '1px solid slategrey'
      }
    },
    boardDetailContainer: {
      position: 'absolute',
      width: '100%',
      height: '100%',
      top: 0,
      left: 0,
      zIndex: 1,
      backgroundColor: theme.palette.background.default
    }
  })
);
