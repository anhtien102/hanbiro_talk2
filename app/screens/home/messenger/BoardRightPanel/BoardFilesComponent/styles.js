import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      width: '96%',
      display: 'flex',
      flexDirection: 'column',
      borderRadius: '10px',

      margin: '6px 8px 10px 8px',
      backgroundColor:
        theme.palette.type == 'light'
          ? theme.palette.divider
          : theme.palette.divider
    },

    containerTop: {
      display: 'flex',
      width: '100%',
      flexDirection: 'row',
      alignItems: 'center',
      padding: '6px 8px 6px 0px'
    },

    backgroundAvatar: {
      borderRadius: '50%',
      backgroundColor: theme.palette.primary.main,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: '80px',
      height: '80px'
    },

    title: {
      paddingLeft: '10px'
    },

    inline: {
      display: 'inline'
    },
    containerBottom: {
      display: 'flex',
      flexDirection: 'row',
      alignItem: 'center',
      padding: '4px 0px',
      height: '36px',
      width: '100%',
      justifyContent: 'flex-end'
    },
    containerNameFile: {
      display: 'flex',
      width: '80px',
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center'
    },
    secondary: {
      color: theme.palette.primary
    },
    list: {
      overflowY: 'auto',
      margin: 0,
      padding: 0,
      listStyle: 'none',
      height: '100%'
    },
    header: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      textAlign: 'end',
      justifyContent: 'flex-end',
      padding: '0px 10px',
      opacity: 1,
      backgroundColor: theme.palette.background.default
    },
    fileSize: {
      fontStyle: 'italic',
      color: theme.palette.text.secondary
    }
  })
);
