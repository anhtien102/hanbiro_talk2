import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      height: '100%'
    },

    containerTop: {
      display: 'flex',
      width: '100%',
      flexDirection: 'row',
      alignItems: 'flex-start',
      padding: '6px 8px 0px 8px'
    },
    avatar: {
      width: '100%',
      height: '100%',
      padding: '10px 10px'
    },
    backgroundAvatar: {
      borderRadius: '40px',
      backgroundColor: 'white',
      width: '80px',
      height: '80px'
    },

    title: {
      paddingLeft: '10px',
      fontWeight: 'bold',
      fontSize: '16px'
    },
    inline: {
      display: 'inline'
    },
    containerBottom: {
      display: 'flex',
      flexDirection: 'row',
      alignItem: 'center',
      padding: '0px 0px 8px 10px',
      height: '40px',
      width: '100%',
      justifyContent: 'space-between'
    },
    containerNameFile: {
      display: 'flex',
      width: '80px',
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center'
    },
    secondary: {
      color: theme.palette.primary
    },

    rootDetailCommentatorItem: {
      display: 'flex',
      flexDirection: 'row',
      width: '100%',
      padding: '5px 10px 5px 20px'
    },

    avatar: {
      height: 36,
      borderRadius: '50%',
      backgroundColor: 'grey'
    },

    ContainerListComment: {
      height: 'calc(100% - 70px)',
      width: '100%'
    },
    ContainerList: {
      height: 'calc(100% - 194px)',
      width: '100%'
    },
    list: {
      overflowY: 'auto',
      margin: 0,
      padding: 0,
      height: '100%',
      '&::-webkit-scrollbar': {
        width: '0.5em'
      },
      // '&::-webkit-scrollbar-track': {
      //   boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
      //   webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)'
      // },
      '&::-webkit-scrollbar-thumb': {
        backgroundColor: theme.palette.divider,
        outline: '1px solid slategrey'
      }
    },
    containerComment: {
      padding: 10,
      width: '100%',
      borderRadius: 6,
      backgroundColor: theme.palette.divider,
      marginLeft: '10px'
    },
    commentatorTop: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between'
    },
    commentContainer: {
      margin: 20,
      backgroundColor: 'grey',
      width: 'calc(100% - 40px)',
      height: 'calc(100% - 40px)'
    },
    containerInput: {
      display: 'flex',
      height: 194,
      backgroundColor: theme.palette.divider,
      padding: 20,
      alignItems: 'center'
    },
    rootAppbar: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      height: 70,
      width: '100%',
      borderBottom: `1px  solid ${theme.palette.divider}`
    },
    appBarTitle: {
      fontWeight: 700,
      flex: 1,
      textAlign: 'center'
    },
    fileItemRoot: {
      display: 'flex',
      justifyContent: 'center',
      paddingRight: 0
    },
    fileItemImage: {
      width: '96%',
      objectFit: 'cover',
      borderRadius: 6,
      maxHeight: 200,
      marginBottom: 10
    }
  })
);
