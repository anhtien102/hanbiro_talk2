import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    rootDetailItem: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      paddingBottom: 10
    },
    topDetailItem: {
      display: 'flex',
      flexDirection: 'row',
      width: '100%',
      padding: '10px',
      paddingBottom: '0px'
    },
    containerIcon: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end',
      paddingRight: 10,
      flex: 1
    }
  })
);
