import { MoreDotIcon } from '../../../../../../components/HanSVGIcon';
import UserPhotoView from '../../../../../../components/UserPhoto';

import { useStyles } from './styles';

import React, { useEffect, useRef, useState } from 'react';
import {
  IconButton,
  useTheme,
  Typography,
  Grow,
  Popper,
  Paper,
  MenuItem,
  ClickAwayListener,
  MenuList
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';

export default function BoardDetailItem(props) {
  const { boardData, userCached, handleChooseItem } = props;
  const classes = useStyles();
  const theme = useTheme();
  const user = userCached[boardData.userKey];
  const anchorRef = useRef(null);
  const [open, setOpen] = useState(false);
  const { t } = useTranslation();
  const handleToggle = () => {
    setOpen(!open);
  };

  const handleClose = event => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };

  const prevOpen = useRef(open);
  useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);

  function handleListKeyDown(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    }
  }

  function handleChoose(type) {
    handleChooseItem(type);
    setOpen(false);
  }

  return (
    <div
      key={`${boardData.userKey} ${boardData.rawTime}`}
      className={classes.rootDetailItem}
    >
      <div className={classes.topDetailItem}>
        <UserPhotoView
          key={boardData.userKey}
          style={{
            width: 35,
            height: 35,
            borderRadius: '50%'
          }}
          userKeyData={boardData.userKey}
          imgSize={50}
        />
        <div style={{ paddingLeft: 10 }}>
          <Typography variant="body2" style={{ fontWeight: 700 }}>
            {user ? user.displayName : ''}
          </Typography>
          <Typography
            variant={'caption'}
            style={{ color: theme.palette.text.secondary }}
          >
            {boardData.dateTimeString ?? '20-11-2000'}
          </Typography>
        </div>
        <div className={classes.containerIcon} onClick={handleToggle}>
          <IconButton ref={anchorRef}>
            <MoreDotIcon />
          </IconButton>
        </div>
        <Popper
          style={{ marginRight: 20 }}
          open={open}
          anchorEl={anchorRef.current}
          role={undefined}
          transition
          disablePortal
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{
                transformOrigin: 'bottom-end'
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList
                    autoFocusItem={open}
                    id="menu-list-grow"
                    onKeyDown={handleListKeyDown}
                  >
                    <MenuItem onClick={() => handleChoose(1)}>
                      {`${t('edit')}`}
                    </MenuItem>
                    <MenuItem onClick={() => handleChoose(2)}>{`${t(
                      'delete'
                    )}`}</MenuItem>
                    <MenuItem onClick={() => handleChoose(3)}>
                      {`${t('pin_this_board')}`}
                    </MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </div>
      <Typography
        variant="body2"
        style={{
          padding: '0px 20px 0px 10px',
          whiteSpace: 'pre-wrap',
          wordBreak: 'break-word'
        }}
      >
        {boardData.content}
      </Typography>
    </div>
  );
}
