import { ArrowBackIos } from '@material-ui/icons/';
import { useSelector } from 'react-redux';
import { BoardRefreshIcon } from '../../../../../components/HanSVGIcon';
import UserPhotoView from '../../../../../components/UserPhoto';
import { useHistory } from 'react-router-dom';
import SimplebarHanbiro from '../../../../../components/SimpleBarHanbiro';
import { useStyles } from './styles';
import Api from '../../../../../core/service/api';
import talkAPI from '../../../../../core/service/talk.api.render';
import BoardUtils from '../../../../../core/model/BoardUtils';
import { ipcRenderer } from 'electron';
import * as constantsApp from '../../../../../configs/constant';

import React, {
  Component,
  useEffect,
  useRef,
  useState,
  createRef
} from 'react';
import {
  IconButton,
  Button,
  useTheme,
  Typography,
  TextField,
  List,
  ListItem
} from '@material-ui/core';
import BoardDetailItem from './BoardDetailItem';
import { useTranslation } from 'react-i18next';

export default function BoardDetail({
  boardCN,
  boardKey,
  roomKey,
  onReloadList
}) {
  const { t } = useTranslation();
  const history = useHistory();

  const classes = useStyles();
  const LENGTH_ITEM_REQUEST = 5;

  const theme = useTheme();
  const [message, setMessage] = useState('');
  const keycode = useRef(-1);

  const userCached = useSelector(state => state.company.user_cached);
  const authInfo = useSelector(state => state.auth.user);

  const isRequesting = useRef(false);
  const canLoadMore = useRef(false);
  const tagRequest = useRef();
  const tagRequestCommentNew = useRef();
  const count = useRef();

  const [showLoadingMore, setShowLoadingMore] = useState(false);
  const [listComment, setListComment] = useState([]);
  const [fileList, setFileList] = useState([]);
  const [boardData, setBoardData] = useState(null);
  const [loading, setLoading] = useState(false);

  const onChange = e => {
    if (keycode.current == 13) {
      setMessage('');
    } else {
      setMessage(`${e.target.value}`);
    }
  };
  const handleChooseItem = type => {
    if (type == 1) {
      // chinh sua
    } else if (type == 2) {
      // xoa
      const userKey = authInfo.account_info.user_key ?? '';
      if (userKey != null) {
        deleteBoardRequest(userKey, boardKey);
      }
    } else if (type == 3) {
      // thong bao den room
      onSendAlarmNotification();
    }
  };

  const refresh = () => {
    isRequesting.current = false;
    canLoadMore.current = false;
    count.current = LENGTH_ITEM_REQUEST;
    if (loading) {
      setLoading(false);
    }
    setListComment([]);
    setBoardData(null);
    setMessage('');

    Api.cancelRequest(tagRequest.current);
    firstTimeRequest();
  };

  const loadMore = () => {
    if (isRequesting.current || !canLoadMore.current) {
      return;
    }
    setShowLoadingMore(true);
    const regTime = parseInt((Date.now() + 86400) / 1000) + 1000;
    count.current = count.current + LENGTH_ITEM_REQUEST;
    startGetBoardListComment(boardKey, boardCN, regTime, count.current);
  };

  const onSendAlarmNotification = () => {
    const boardInfo = { ...boardData, boardKey: boardKey, roomKey: roomKey };
    ipcRenderer.send(
      constantsApp.SEND_SOCKET_API_EVENT,
      constantsApp.API_BOARD_ALARM_NOTIFY,
      boardInfo
    );
  };

  const loadMoreAddComment = () => {
    setShowLoadingMore(true);
    const regTime = parseInt((Date.now() + 86400) / 1000) + 1000;
    count.current = count.current + 1;
    startGetBoardListComment(boardKey, boardCN, regTime, count.current);
  };

  const startGetBoardListComment = async (
    boardKey,
    boardCN,
    regTime,
    countItem
  ) => {
    try {
      isRequesting.current = true;
      tagRequest.current = `startGetBoardListComment_${Date.now()}`;
      const response = await Api.postRawBody(
        talkAPI.boardCommentList(),
        BoardUtils.rawBodyGetBoardCommentList(
          boardCN,
          boardKey,
          regTime,
          countItem
        ),
        BoardUtils.rawAuthorizedHeader({ 'List-Type': 'ALL' }),
        tagRequest.current
      );

      const resultObj = BoardUtils.boardCommentListFromXML(response);

      const boardCommentListTemp = resultObj.boardCommentList ?? [];
      setListComment(boardCommentListTemp);
      setBoardData(resultObj.boardData);
      setFileList(resultObj.boardFileList ?? []);
      canLoadMore.current = count.current < resultObj.boardData.commentCount;

      isRequesting.current = false;
      setLoading(false);
      setShowLoadingMore(false);
    } catch (error) {
      if (Api.isCancel(error)) {
        return;
      }
      isRequesting.current = false;
      setLoading(false);
      setShowLoadingMore(false);
      canLoadMore.current = false;
      console.log('startGetBoardList', error);
    }
  };

  const deleteBoardRequest = async (userKey, boardKey) => {
    try {
      isRequesting.current = true;
      tagRequest.current = `deleteBoardRequest${Date.now()}`;
      const response = await Api.postRawBody(
        talkAPI.boardDelete(),
        BoardUtils.rawBodyDeleteBoard(userKey, boardKey),
        BoardUtils.rawAuthorizedHeader({ 'List-Type': 'ALL' }),
        tagRequest.current
      );
      isRequesting.current = false;
      const result = BoardUtils.boardCommentProcessBoolXML(response);

      if (result) {
        onReloadList?.();
        history.goBack();
      }
    } catch (error) {
      if (Api.isCancel(error)) {
        return;
      }
      isRequesting.current = false;
      canLoadMore.current = false;
      console.log('deleteBoardRequest', error);
    }
  };

  const sendCommentRequest = async commentContent => {
    Api.cancelRequest(tagRequestCommentNew.current);
    try {
      isRequesting.current = true;
      tagRequestCommentNew.current = `sendCommentRequest${Date.now()}`;
      const bodyCommentNew = BoardUtils.rawBodyNewComment(
        authInfo.account_info.user_key ?? '',
        commentContent,
        boardKey
      );
      const response = await Api.postRawBody(
        talkAPI.boardCommentInsert(),
        bodyCommentNew,
        BoardUtils.rawAuthorizedHeader({ 'List-Type': 'ALL' }),
        tagRequest.current
      );
      isRequesting.current = false;

      setLoading(false);
      const result = BoardUtils.boardCommentProcessBoolXML(response);
      if (result) {
        loadMoreAddComment();
      }
    } catch (error) {
      if (Api.isCancel(error)) {
        return;
      }
      isRequesting.current = false;
      setLoading(false);
      setShowLoadingMore(false);
      canLoadMore.current = false;
      console.log('startGetBoardList', error);
    }
  };

  useEffect(() => {
    setLoading(true);
    firstTimeRequest();
    return () => {
      Api.cancelRequest(tagRequest.current);
    };
  }, []);

  const firstTimeRequest = () => {
    setLoading(true);
    const regTime = parseInt((Date.now() + 86400) / 1000);
    count.current = LENGTH_ITEM_REQUEST;
    startGetBoardListComment(boardKey, boardCN, regTime, count.current);
  };

  const keyPress = e => {
    keycode.current = e.keyCode;
    if (e.keyCode === 13 && e.shiftKey) {
      console.log('keyPress:', e);
      setMessage(`${message}\n`);
      e.preventDefault();
      return;
    }
    if (e.keyCode == 13) {
      sendCommentRequest(message);
    }
  };

  const showNodata = boardData == null;
  return (
    <>
      <div className={classes.root}>
        <AppBarTop
          boardData={boardData}
          userCached={userCached}
          refreshAction={refresh}
        />
        {!showNodata ? (
          <div className={classes.ContainerListComment}>
            <WrapperList
              classes={classes}
              userCached={userCached}
              boardData={boardData}
              showLoadingMore={showLoadingMore}
              listComment={listComment}
              loadMore={loadMore}
              fileList={fileList}
              handleChooseItem={handleChooseItem}
              t={t}
            />
            <div className={classes.containerInput}>
              <TextField
                id="outlined-multiline-static"
                label=""
                multiline
                rows={4}
                value={message}
                variant="outlined"
                placeholder={`${t('Comment')}`}
                fullWidth={true}
                style={{
                  display: 'inline-block',
                  backgroundColor: theme.palette.background.default
                }}
                onKeyDown={e => keyPress(e)}
                onChange={e => onChange(e)}
              />
            </div>
          </div>
        ) : null}
      </div>

      {loading ? <div className="han-loading mini" /> : null}
    </>
  );
}

function AppBarTop({ userCached, boardData, refreshAction }) {
  const history = useHistory();
  const classes = useStyles();
  const userItem =
    boardData != null ? userCached[boardData.userKey ?? ''] : null;
  return (
    <div className={classes.rootAppbar}>
      <IconButton
        aria-label="Go back"
        onClick={() => {
          history.goBack();
        }}
      >
        <ArrowBackIos />
      </IconButton>

      <Typography className={classes.appBarTitle} varient="h5">
        {userItem ? userItem.displayName : ''}
      </Typography>
      <IconButton aria-label="Refresh" onClick={refreshAction}>
        <BoardRefreshIcon fontSize="small" />
      </IconButton>
    </div>
  );
}

class WrapperList extends Component {
  constructor(props) {
    super(props);
    this.simplebar = null;
    this.listRef = createRef();
  }

  componentDidMount() {}

  render() {
    const {
      classes,
      userCached,
      showLoadingMore,
      boardData,
      listComment,
      fileList,
      loadMore,
      handleChooseItem,
      t
    } = this.props;

    return (
      <div className={classes.ContainerList}>
        <List ref={this.listRef} className={classes.list}>
          <BoardDetailItem
            key={'board_detail_key'}
            userCached={userCached}
            boardData={boardData}
            handleChooseItem={handleChooseItem}
          />
          <Header
            key={'header_files'}
            headerName={`${t('File')} (${fileList.length})`}
          />
          {fileList.length > 0 &&
            fileList.map(fileItem => {
              return <FileItem key={fileItem.fileKey} fileItem={fileItem} />;
            })}
          <Header
            key={'header_comment'}
            headerName={`${t('Comment')} (${boardData.commentCount})`}
          />
          <div
            key={'load_more_key'}
            style={{
              padding: 10
            }}
          >
            <Button
              key={'bottom_load_more'}
              style={{ width: '100%' }}
              variant="contained"
              onClick={loadMore}
            >
              {`${t('view_more')}`}
            </Button>
          </div>
          {listComment.map(itemComment => {
            return (
              <CommentatorItem
                key={itemComment.userKey + itemComment.rawTime}
                userCached={userCached}
                itemComment={itemComment}
              />
            );
          })}
          {showLoadingMore ? (
            <ListItem
              key={'loading_more_bottom'}
              className={classes.rootItem}
              ContainerComponent="div"
            >
              <div className="han-loading mini" />
            </ListItem>
          ) : null}
        </List>
      </div>
    );
  }
}

function Header({ headerName }) {
  return (
    <Typography
      key={'header_file'}
      varian="h6"
      style={{
        fontWeight: 600,
        paddingLeft: 10
      }}
    >
      {headerName}
    </Typography>
  );
}

function CommentatorItem(props) {
  const { itemComment, userCached } = props;
  const classes = useStyles();
  const theme = useTheme();
  const user = userCached[itemComment.userKey] ?? {};
  return (
    <div className={classes.rootDetailCommentatorItem}>
      <UserPhotoView
        key={itemComment.userKey}
        style={{
          width: 30,
          height: 30,
          borderRadius: '50%'
        }}
        userKeyData={itemComment.userKey}
        imgSize={30}
      />
      <div className={classes.containerComment}>
        <div className={classes.commentatorTop}>
          <Typography variant="body2" style={{ fontWeight: 700 }}>
            {user ? user.displayName : ''}
          </Typography>
          <Typography
            variant={'caption'}
            style={{
              color: theme.palette.text.secondary
            }}
          >
            {itemComment.dateTimeString ?? '20-11-2000'}
          </Typography>
        </div>
        <Typography
          variant="body2"
          style={{
            whiteSpace: 'pre-wrap',
            wordBreak: 'break-word',
            paddingTop: 10
          }}
        >
          {itemComment.content}
        </Typography>
      </div>
    </div>
  );
}

function FileItem(props) {
  const classes = useStyles();
  const { fileItem } = props;
  return (
    <div className={classes.fileItemRoot}>
      <img
        className={classes.fileItemImage}
        src={`data:image/png;base64,${fileItem.content}`}
      />
    </div>
  );
}
