import React, { useContext } from 'react';
import { useSelector } from 'react-redux';
import { useStyles } from './styles';
import { CSSListItem } from './component.styles';
import { useTranslation } from 'react-i18next';
import { BOARD_PANNEL_WIDTH } from '../../../../configs/constant';

import {
  BoardPhotoIcon,
  BoardNotificationIcon,
  BoardFileIcon,
  BoardAllIcon
} from '../../../../components/HanSVGIcon/';

import { Route, useHistory, useRouteMatch } from 'react-router-dom';
import BoardDetail from './BoardDetail';

import { Snackbar } from '@material-ui/core';
import BoardFileComponent from './BoardFilesComponent';

import BoardAllComponent from './BoardAllComponent';
import BoardPhotosComponent from './BoardPhotosComponent';
import BoardWrite from './BoardWrite';

export default function BoardRightPannel() {
  const classes = useStyles();
  const data = useSelector(state => state.message_list.room);
  return (
    <div
      className={classes.boxContainer}
      style={{ width: BOARD_PANNEL_WIDTH }}
      key={data?.rRoomKey ?? 'nothing'}
    >
      <BoardContainer roomKey={data?.rRoomKey} />
    </div>
  );
}

function BoardContainer({ roomKey }) {
  const classes = useStyles();

  const { url } = useRouteMatch();
  const history = useHistory();

  const { t } = useTranslation();
  const initTab = () => {
    const str = localStorage.getItem('board_selected_tab');
    return str != null ? parseInt(str) : 0;
  };
  const [tabIndex, setTabIndex] = React.useState(initTab);
  const [openSnackbar, setOpenSnackbar] = React.useState({
    show: false,
    message: ''
  });

  const dataSourceTabItem = [
    {
      index: 0,
      title: 'All',
      item: <BoardAllIcon className={classes.icon} />,
      itemSelected: <BoardAllIcon className={classes.iconSelected} />
    },
    {
      index: 1,
      title: 'Notifications',
      item: <BoardNotificationIcon className={classes.icon} />,
      itemSelected: <BoardNotificationIcon className={classes.iconSelected} />
    },
    {
      index: 2,
      title: 'Photo',
      item: <BoardPhotoIcon className={classes.icon} />,
      itemSelected: <BoardPhotoIcon className={classes.iconSelected} />
    },

    {
      index: 3,
      title: 'Files',
      item: <BoardFileIcon className={classes.icon} />,
      itemSelected: <BoardFileIcon className={classes.iconSelected} />
    }
  ];

  const onChangeTabIndex = index => {
    setTabIndex(index);
    localStorage.setItem('board_selected_tab', `${index}`);
  };

  const handleClosed = () => {
    setOpenSnackbar({ show: false, message: '' });
  };

  const showInfoToUser = message => {
    setOpenSnackbar({ show: true, message: message });
  };

  const onOpenDetail = item => {
    console.log('onOpenDetail', item);
    history.push(`${url}?boardkey=${item.itemKey}&boardcn=${item.cn}`);
  };

  const onOpenWrite = () => {
    history.push(`${url}?boardwrite=true`);
  };

  const onReloadList = () => {
    const event = new Event('require_reload_board_list', {});
    window.dispatchEvent(event);
  };

  return (
    <>
      <div className={classes.contentTop}>
        {dataSourceTabItem.map(itemTab => {
          return (
            <div key={itemTab.index} className={classes.tabContainer}>
              <CSSListItem
                button
                onClick={() => onChangeTabIndex(itemTab.index)}
                selected={tabIndex == itemTab.index}
              >
                <div className={classes.tabContainerSub}>
                  {tabIndex == itemTab.index
                    ? itemTab.itemSelected
                    : itemTab.item}
                  <h4 className={classes.tabText}>{itemTab.title}</h4>
                </div>
              </CSSListItem>
              {tabIndex == itemTab.index && (
                <div className={classes.selectedLine} />
              )}
            </div>
          );
        })}
      </div>

      <TabPanel
        showInfoToUser={showInfoToUser}
        roomKey={roomKey}
        key={tabIndex}
        value={tabIndex}
        onOpenDetail={onOpenDetail}
        onOpenWrite={onOpenWrite}
      />

      <Route
        path={url}
        children={({ location }) => {
          console.log('location', location);
          const urlParams = new URLSearchParams(location.search);
          const boardWrite = urlParams.get('boardwrite');
          const boardKey = urlParams.get('boardkey');
          const boardCN = urlParams.get('boardcn');

          if (boardWrite) {
            return (
              <div className={classes.boardDetailContainer}>
                <BoardWrite roomKey={roomKey} onReloadList={onReloadList} />
              </div>
            );
          }

          if (boardKey) {
            return (
              <div className={classes.boardDetailContainer}>
                <BoardDetail
                  boardKey={boardKey}
                  boardCN={boardCN}
                  roomKey={roomKey}
                  onReloadList={onReloadList}
                />
              </div>
            );
          }
          return null;
        }}
      ></Route>

      <Snackbar
        autoHideDuration={2000}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={openSnackbar.show}
        onClose={() => handleClosed()}
        message={openSnackbar.message}
      />
    </>
  );
}

function TabPanel({ value, roomKey, onOpenDetail, onOpenWrite }) {
  const classes = useStyles();

  const selectComponent = index => {
    if (index == 0)
      return (
        <BoardAllComponent
          roomKey={roomKey}
          listType={'ALL'}
          onOpenDetail={onOpenDetail}
          onOpenWrite={onOpenWrite}
        />
      );
    if (index == 1)
      return (
        <BoardAllComponent
          roomKey={roomKey}
          listType={'NOTI'}
          onOpenDetail={onOpenDetail}
          onOpenWrite={onOpenWrite}
        />
      );
    if (index == 2)
      return (
        <BoardPhotosComponent
          roomKey={roomKey}
          onOpenDetail={onOpenDetail}
          onOpenWrite={onOpenWrite}
        />
      );
    if (index == 3)
      return (
        <BoardFileComponent
          roomKey={roomKey}
          onOpenDetail={onOpenDetail}
          onOpenWrite={onOpenWrite}
        />
      );
    return null;
  };
  return <div className={classes.tabPanel}>{selectComponent(value)}</div>;
}
