import { useStyles } from './styles';
import React, {
  Component,
  createRef,
  useEffect,
  useRef,
  useState
} from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import {
  Typography,
  ListItem,
  Button,
  List,
  Fab,
  ListItemAvatar
} from '@material-ui/core';
import UserPhotoView from '../../../../../components/UserPhoto';
import Api from '../../../../../core/service/api';
import talkAPI from '../../../../../core/service/talk.api.render';
import BoardUtils from '../../../../../core/model/BoardUtils';
import SimplebarHanbiro from '../../../../../components/SimpleBarHanbiro';
import { fileExtension } from '../../../../../core/model/MessageUtils';
import { convertFileSize } from '../../../../../utils';

import {
  BoardCalendarIcon,
  BoardRefreshIcon,
  BoardWriteIcon
} from '../../../../../components/HanSVGIcon';
export default function BoardAllComponent({
  roomKey,
  listType,
  onOpenDetail,
  onOpenWrite
}) {
  const classes = useStyles();
  const { t } = useTranslation();
  const userCached = useSelector(state => state.company.user_cached);
  const cachedBoardList = useRef([]);
  const [boardList, setBoardList] = useState([]);
  const [loading, setLoading] = useState(false);
  const isRequesting = useRef(false);
  const firstTimeRef = useRef();
  const canLoadMore = useRef(false);
  const [showLoadingMore, setShowLoadingMore] = useState(false);
  const tagRequest = useRef();

  const startGetBoardList = async (roomKey, regTime, append) => {
    try {
      isRequesting.current = true;
      tagRequest.current = `startGetBoardList_${Date.now()}`;
      const response = await Api.postRawBody(
        talkAPI.boardList(),
        BoardUtils.rawBodyGetBoardList(roomKey, regTime),
        BoardUtils.rawAuthorizedHeader({ 'List-Type': listType }),
        tagRequest.current
      );
      isRequesting.current = false;
      const resultObj = BoardUtils.boardListFromXML(response);
      setLoading(false);
      setShowLoadingMore(false);
      if (append) {
        const list = cachedBoardList.current;
        let nList = resultObj.boardListResult;
        // Lat item of old list isEqual with first item of new list
        if (list.length > 0 && nList.length > 0) {
          const item = list[list.length - 1];
          const fItem = nList[0];
          if (item.itemKey == fItem.itemKey) {
            nList = nList.filter((e, index) => index > 0);
          }
        }

        canLoadMore.current = nList.length > 0;

        cachedBoardList.current = [...list, ...nList];
      } else {
        cachedBoardList.current = resultObj.boardListResult;
        canLoadMore.current = true;
      }
      setBoardList(cachedBoardList.current);

      firstTimeRef.current = resultObj.firstTime;
    } catch (error) {
      if (Api.isCancel(error)) {
        return;
      }
      isRequesting.current = false;
      setLoading(false);
      setShowLoadingMore(false);
      console.log('startGetBoardList', error);
    }
  };

  useEffect(() => {
    window.addEventListener('require_reload_board_list', reloadEvent);
    const latestTime = parseInt((Date.now() + 86400) / 1000);
    setLoading(true);
    cachedBoardList.current = [];
    startGetBoardList(roomKey, latestTime, false);

    return () => {
      Api.cancelRequest(tagRequest.current);
      window.removeEventListener('require_reload_board_list', reloadEvent);
    };
  }, []);

  const reloadEvent = () => {
    refresh(false);
  };

  const refresh = loading => {
    Api.cancelRequest(tagRequest.current);
    const latestTime = parseInt((Date.now() + 86400) / 1000);
    if (loading) {
      setLoading(true);
      setBoardList([]);
      cachedBoardList.current = [];
    }

    startGetBoardList(roomKey, latestTime, false);
  };

  const loadMore = () => {
    if (isRequesting.current || !canLoadMore.current) {
      return;
    }
    setShowLoadingMore(true);
    startGetBoardList(roomKey, firstTimeRef.current, true);
  };

  const showNodata = !loading && !isRequesting.current && boardList.length == 0;
  return (
    <>
      <WrapperList
        classes={classes}
        loadMore={loadMore}
        boardList={boardList}
        userCached={userCached}
        onOpenDetail={onOpenDetail}
        showLoadingMore={showLoadingMore}
      />
      {showNodata ? (
        <Typography
          style={{
            position: 'absolute',
            width: '100%',
            top: '50%',
            textAlign: 'center'
          }}
          variant="body2"
        >
          {t('no_result')}
        </Typography>
      ) : null}
      {loading ? <div className="han-loading mini" /> : null}
      <div style={{ position: 'absolute', bottom: 10, right: 10 }}>
        <Fab
          color="primary"
          aria-label="refresh"
          size="small"
          onClick={() => {
            refresh(true);
          }}
        >
          <BoardRefreshIcon fontSize="small" />
        </Fab>
        <Fab
          color="secondary"
          style={{ marginLeft: 5 }}
          aria-label="write"
          size="small"
          onClick={() => {
            onOpenWrite();
          }}
        >
          <BoardWriteIcon fontSize="small" />
        </Fab>
      </div>
    </>
  );
}

class WrapperList extends Component {
  constructor(props) {
    super(props);
    this.simplebar = null;
    this.listRef = createRef();
    this.onScroll = this.onScroll.bind(this);
  }

  componentDidMount() {
    this.simplebar = new SimplebarHanbiro(this.listRef.current);
  }

  onScroll(e) {
    let element = e.target;
    const diffScrollTop = element.scrollHeight - element.clientHeight;
    const scrollAtBottom = diffScrollTop - element.scrollTop <= 0;
    if (scrollAtBottom) {
      this.props.loadMore();
    }
  }

  render() {
    const {
      classes,
      boardList,
      userCached,
      onOpenDetail,
      showLoadingMore
    } = this.props;

    return (
      <List
        ref={this.listRef}
        className={classes.list}
        subheader={<li />}
        onScroll={this.onScroll}
      >
        <div>
          {boardList.map(item => {
            return (
              <BoardAllItem
                key={item.itemKey}
                item={item}
                userCached={userCached}
                onOpenDetail={onOpenDetail}
              />
            );
          })}
          {showLoadingMore ? (
            <ListItem
              key={'loading_more_bottom'}
              className={classes.rootItem}
              ContainerComponent="div"
            >
              <div className="han-loading mini" />
            </ListItem>
          ) : null}
        </div>
      </List>
    );
  }
}

function BoardAllItem({ item, userCached, onOpenDetail }) {
  const classes = useStyles();
  const { t } = useTranslation();
  const user = userCached[item.userKey];

  const c = t('Comment');
  let commentStr = c;
  if (item.commentCount > 0) {
    commentStr = `${c} (${item.commentCount})`;
  }

  return (
    <ListItem className={classes.rootItem} ContainerComponent="div">
      <div className={classes.container}>
        <Typography className={classes.msgBody} variant="body2">
          {item.bodyMessage}
        </Typography>

        <div className={classes.containerInfomation}>
          <div className={classes.rootHorizontal} style={{ flex: 1 }}>
            <UserPhotoView
              key={item.userKey}
              style={{
                width: 25,
                height: 25,
                borderRadius: '50%'
              }}
              data={user}
              userKeyData={item.userKey}
              imgSize={50}
            />
            <Typography variant={'caption'} style={{ paddingLeft: 8 }}>
              {user ? user.displayName : ''}
            </Typography>
          </div>
          <div className={classes.rootHorizontal}>
            <div className={classes.backgroundIcon}>
              <BoardCalendarIcon className={classes.icon} />
            </div>
            <Typography variant={'caption'} style={{ paddingLeft: 8 }}>
              {item.dateTimeString}
            </Typography>
          </div>
        </div>
        <BoardAttachFileItem item={item} />
        <Button
          variant="contained"
          style={{ textTransform: 'capitalize' }}
          onClick={() => {
            onOpenDetail?.(item);
          }}
        >
          {commentStr}
        </Button>
      </div>
    </ListItem>
  );
}

function BoardAttachFileItem({ item }) {
  const classes = useStyles();

  return (
    <div>
      {item?.attachments.map(att => {
        return (
          <ListItem button key={att.fileKey} className={classes.attachRoot}>
            <div className={classes.backgroundAvatar}>
              <Typography
                variant="caption"
                style={{ color: 'white', fontWeight: 700 }}
              >
                {fileExtension(att.fileName)}
              </Typography>
            </div>
            <Typography variant={'caption'} className={classes.fileNameAttach}>
              {att.fileName}
            </Typography>
            <Typography variant={'caption'} className={classes.fileSizeAttach}>
              {convertFileSize(att.fileSize)}
            </Typography>
          </ListItem>
        );
      })}
    </div>
  );
}
