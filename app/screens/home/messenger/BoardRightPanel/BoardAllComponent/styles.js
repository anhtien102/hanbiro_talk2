import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    rootItem: {
      width: '96%',
      margin: '6px 10px 10px 10px',
      borderRadius: '6px',
      backgroundColor:
        theme.palette.type == 'light'
          ? theme.palette.divider
          : theme.palette.divider
    },
    rootVertical: {
      display: 'flex',
      flexDirection: 'column'
    },
    rootHorizontal: {
      display: 'flex',
      alignItems: 'center'
    },
    container: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      padding: '4px 0px'
    },
    containerInfomation: {
      display: 'flex',
      flexDirection: 'row',
      marginBottom: 8
    },
    msgBody: {
      whiteSpace: 'pre-wrap',
      wordBreak: 'break-word',
      paddingBottom: 10
    },
    icon: {
      width: 15,
      height: 15,
      margin: '5px 5px',
      color: theme.palette.primary.main
    },
    backgroundAvatar: {
      width: 30,
      height: 30,
      margin: '8px 8px 8px 0px'
    },
    backgroundIcon: {
      backgroundColor: 'white',
      borderRadius: '50%',
      width: 25,
      height: 25
    },
    list: {
      overflowY: 'auto',
      margin: 0,
      padding: 0,
      height: '100%'
    },
    header: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      textAlign: 'end',
      justifyContent: 'flex-end',
      padding: '0px 10px',
      opacity: 1,
      backgroundColor: theme.palette.background.default
    },
    backgroundAvatar: {
      borderRadius: '50%',
      backgroundColor: theme.palette.primary.main,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      margin: 5,
      width: 30,
      height: 30
    },

    attachRoot: {
      display: 'flex',
      flexDirection: 'row',
      borderRadius: 4,
      height: 40,
      backgroundColor:
        theme.palette.type == 'dark'
          ? theme.palette.grey[600]
          : theme.palette.grey[100],
      margin: '6px 0px',
      alignItems: 'center'
    },
    fileNameAttach: {
      // paddingLeft: 10,
      fontStyle: 'italic'
    },
    fileSizeAttach: {
      flex: 1,
      textAlign: 'right',
      paddingRight: 10,
      fontStyle: 'italic'
    },
    iconAttach: {
      height: 34,
      width: 34,
      padding: '4px ',
      objectFit: 'cover'
    }
  })
);
