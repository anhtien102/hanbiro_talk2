import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      position: 'relative',
      width: '100%',
      height: '100%'
    },
    contentHeader: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      height: 70,
      width: '100%',
      padding: '10px 20px',
      borderBottom: `1px  solid ${theme.palette.divider}`
    },
    contentBody: {
      flex: 1,
      flexDirection: 'column',
      position: 'relative',
      width: '100%',
      height: 'calc(100% - 70px)'
    },
    headerTitle: {
      fontWeight: 700,
      flex: 1,
      textAlign: 'center'
    }
  })
);
