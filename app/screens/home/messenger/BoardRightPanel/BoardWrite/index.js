import React, { useRef } from 'react';
import { useHistory } from 'react-router-dom';
import { IconButton, Typography, Fab, useTheme } from '@material-ui/core';
import ImageOutlined from '@material-ui/icons/ImageOutlined';
import AttachFileOutlinedIcon from '@material-ui/icons/AttachFileOutlined';
import { useTranslation } from 'react-i18next';
import { ArrowBackIos, SendOutlined } from '@material-ui/icons/';
import talkAPI from '../../../../../core/service/talk.api.render';
import Editor from './editor';
import WhisperDropFile from '../../../../../components/WhisperDropFile';
import { useStyles } from './styles';
import FileInput from '../../../../../components/FileInput';

export default function BoardWrite({ onReloadList }) {
  const classes = useStyles();
  const { t } = useTranslation();
  const editorRef = useRef();
  const history = useHistory();
  const theme = useTheme();
  const allowFileTransfer = talkAPI.allowFileTransfer();
  const handleDrop = e => {
    console.log('handleDrop:', e);
    e.preventDefault();
    e.stopPropagation();
    if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
      if (editorRef.current) {
        let files = [];
        for (let f of e.dataTransfer.files) {
          const item = { name: f.name, path: f.path, size: f.size };
          files.push(item);
        }
        editorRef.current.addAttachment(files);
      }
      e.dataTransfer.clearData();
    }
  };
  const onSendBoard = () => {};

  const onChooseFile = files => {
    let fileList = [];
    for (let f of files) {
      const item = { name: f.name, path: f.path, size: f.size };
      fileList.push(item);
    }
    editorRef.current.addAttachment(fileList);
  };

  return (
    <div className={classes.root}>
      <div className={classes.contentHeader}>
        <IconButton
          size="small"
          onClick={() => {
            onReloadList?.();
            history.goBack();
          }}
        >
          <ArrowBackIos />
        </IconButton>
        <Typography variant="subtitle1" className={classes.headerTitle}>
          {t('Board write')}
        </Typography>

        <IconButton aria-label="Refresh" onClick={onSendBoard}>
          <SendOutlined
            fontSize="small"
            style={{ color: theme.palette.primary.main }}
          />
        </IconButton>
      </div>
      <div className={classes.contentBody}>
        {allowFileTransfer && (
          <WhisperDropFile contextId={'whisper_editor'} onSend={handleDrop} />
        )}

        <div id="whisper_editor" style={{ height: '100%' }}>
          <Editor
            onRef={ref => {
              editorRef.current = ref;
            }}
            showSendButton={false}
            onSend={onSendBoard}
            allowFileTransfer={allowFileTransfer}
          />
        </div>

        <div style={{ position: 'absolute', bottom: 10, right: 10 }}>
          <Fab>
            <FileInput
              accept="image/*"
              style={{ display: 'inline' }}
              onChange={onChooseFile}
            >
              <IconButton>
                <ImageOutlined />
              </IconButton>
            </FileInput>
          </Fab>

          <Fab style={{ marginLeft: 10 }}>
            <FileInput style={{ display: 'inline' }} onChange={onChooseFile}>
              <IconButton>
                <AttachFileOutlinedIcon />
              </IconButton>
            </FileInput>
          </Fab>
        </div>
      </div>
    </div>
  );
}
