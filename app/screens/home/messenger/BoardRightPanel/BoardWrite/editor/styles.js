import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  container: {
    width: '100%'
  },
  richInputContainer: {
    ...theme.typography,
    textShadow: '0px 0 0px rgba(0, 0, 0, 0.4)',
    boxSizing: 'border-box',
    minHeight: '200px',
    maxHeight: '200px',
    display: 'flex',
    flexDirection: 'column',
    overflow: 'auto',
    flex: '1 1 auto',
    overflowX: 'hidden'
  },

  richInput: {
    ...theme.typography,
    userSelect: 'text',
    justifyContent: 'center',
    flexDirection: 'column',
    display: 'inline-block',
    fontSize: theme.typography.body2,
    padding: '20px 10px 20px 12px',
    overflow: 'hidden',
    width: '100%',
    flex: '1 1 auto',
    wordBreak: 'break-word',
    wordErap: 'break-word',
    '&:focus': {
      outline: 'none !important'
    },
    '&:empty&::before': {
      content: 'attr(placeholder)',
      color: theme.palette.grey[400],
      maxWidth: 'calc(100% - 10px)',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      display: 'block'
    }
  },
  userSuggest: {
    backgroundColor: theme.palette.background.default,
    position: 'absolute',
    bottom: 70,
    maxHeight: 400,
    width: 300,
    paddingTop: 16,
    paddingBottom: 16,
    overflowY: 'auto',
    display: 'none',
    borderRadius: '5px',
    boxShadow: '0px 0px 4px 0px rgba(0, 0, 0, 0.2)',
    zIndex: 5
  },
  userSuggestShow: {
    backgroundColor: theme.palette.background.default,
    position: 'absolute',
    bottom: 70,
    maxHeight: 400,
    width: 300,
    paddingTop: 16,
    paddingBottom: 16,
    overflowY: 'auto',
    display: 'block',
    borderRadius: '5px',
    boxShadow: '0px 0px 4px 0px rgba(0, 0, 0, 0.2)',
    zIndex: 5
  },
  userActive: {
    backgroundColor: theme.palette.divider + '!important'
  },
  userNormal: {
    backgroundColor: theme.palette.background.default + '!important'
  },
  quote: {
    margin: 10,
    backgroundColor: theme.palette.divider,
    paddingLeft: 13,
    paddingRight: 9,
    position: 'relative',
    webkitLineClamp: 3,
    webkitBoxOrient: 'vertical',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    borderRadius: 6,
    borderLeft: 3,
    borderRight: 3,
    borderLeftStyle: 'solid',
    borderRightStyle: 'solid',
    borderRightColor: theme.palette.primary.dark,
    borderLeftColor: theme.palette.primary.dark,
    paddingTop: 10,
    paddingBottom: 10
  },
  quoteTitle: {
    ...theme.typography,
    flex: '1 1 0%',
    fontEeight: 400,
    marginBottom: 2,
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    wordWrap: 'break-word',
    fontWeight: 700
  },
  quoteBody: {
    ...theme.typography,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  quoteContent: {
    fontSize: 14,
    paddingRight: 7,
    display: 'grid'
  },
  viewCopyImg: {
    width: '100%',
    // height: 124,
    borderTop: 1,
    borderTopStyle: 'solid',
    borderTopColor: theme.palette.divider,
    padding: 10
  },
  contentCopyImg: {
    position: 'relative',
    width: 124,
    height: 124,
    textAlign: 'center',
    boxShadow:
      '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
    backgroundColor: theme.palette.grey[200]
  },
  iconCopyImg: {
    position: 'absolute',
    right: 0,
    color: 'rgba(0, 0, 0, 0.7)',
    cursor: 'pointer'
  },
  imgCopy: {
    height: '100%',
    maxWidth: '100%'
  },
  nameImgCopy: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 20,
    textAlign: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    color: 'white'
  },
  userSuggestContent: {
    position: 'absolute',
    background: theme.palette.background.paper,
    bottom: 100,
    maxHeight: 400,
    width: 300,
    overflowY: 'auto',
    borderRadius: 8,
    borderColor: theme.palette.background.paper,
    boxShadow: '0px 0px 4px 0px rgba(0, 0, 0, 0.2)',
    zIndex: 4
  },
  chatSend: {
    contain: 'content',
    zIndex: 3,
    display: 'flex !important',
    justifyContent: 'center',
    alignItems: 'center'
  },
  chatSendTxt: {
    fontWeight: 600,
    padding: 8,
    paddingRight: 16,
    textAlign: 'center',
    textTransform: 'uppercase',
    color: theme.palette.primary.main,
    cursor: 'pointer'
  },
  inputFocus: {
    display: 'flex',
    flexDirection: 'column',
    border: 1,
    borderStyle: 'solid',
    borderColor: theme.palette.divider,
    backgroundColor: theme.palette.background.paper,
    height: '100%'
  },
  inputNotFocus: {
    display: 'flex',
    flexDirection: 'column',
    border: 1,
    borderStyle: 'solid',
    borderColor: theme.palette.divider,
    backgroundColor: theme.palette.background.default,
    height: '100%'
  },
  content: {
    display: 'flex',
    borderTop: 1,
    flex: 1
  },
  fileIconContent: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  sendingLoading: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 16
  }
});

export default withStyles(styles);
