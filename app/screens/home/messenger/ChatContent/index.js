import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import withStyles from './styles';
import MessageList from '../../../../components/MessageList';
import { TAB_CATEGORY } from '../../../../configs/constant';
class ChatContent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { location } = nextProps.history;
    let keySelectedRoom = TAB_CATEGORY.tab_company;
    const showSearchResult = nextProps.appUI.show_search_result;
    if (showSearchResult) {
      keySelectedRoom = TAB_CATEGORY.tab_search;
    } else {
      if (location.pathname == '/favorite') {
        keySelectedRoom = TAB_CATEGORY.tab_favorite;
      } else if (location.pathname == '/chat') {
        keySelectedRoom = TAB_CATEGORY.tab_room_list;
      } else if (location.pathname == '/') {
        keySelectedRoom = TAB_CATEGORY.tab_company;
      } else {
        keySelectedRoom = '';
      }
    }

    return {
      selected_room: nextProps.appUI.selected_room,
      keySelectedRoom: keySelectedRoom
    };
  }

  render() {
    const {
      userCached,
      user_logged,
      message_list,
      appUI,
      commonSettings,
      actions
    } = this.props;
    const { keySelectedRoom, selected_room } = this.state;
    const room = selected_room ? selected_room[keySelectedRoom] : null;
    let key = room ? room.primary_key : '';

    let realMessageList =
      message_list?.room?.rRoomKey == key
        ? message_list
        : { ...message_list, messages: [] };

    let extraPositionData =
      message_list?.room?.rRoomKey == key ? appUI.extraPositionData : null;

    return (
      <MessageList
        primary_key={key}
        show_disconnect={appUI.show_disconnect}
        user_logged={user_logged}
        actions={actions}
        roomData={room}
        showRightBoardPannel={appUI.showRightBoardPannel}
        messageData={realMessageList}
        userCached={userCached}
        commonSettings={commonSettings}
        extraPositionData={extraPositionData}
      ></MessageList>
    );
  }
}

export default withRouter(withStyles(ChatContent));
