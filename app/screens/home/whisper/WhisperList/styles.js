import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    main: {
      display: 'flex',
      width: '100%',
      height: '100%',
      flexDirection: 'column'
    },
    item: {
      paddingTop: 0,
      paddingBottom: 0,
      paddingRight: 0
    },
    contentSearch: {
      flexDirection: 'row',
      alignItems: 'center',
      display: 'flex',
      width: '100%',
      paddingLeft: 16,
      paddingRight: 8,
      paddingTop: 8,
      paddingBottom: 8
    },
    inputSearch: { fontSize: theme.typography.fontSize, width: '100%' },
    search: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-start',
      flexDirection: 'row',
      borderBottomColor: theme.palette.divider,
      borderBottomWidth: 1,
      borderBottomStyle: 'solid',
      padding: 4
    },
    searchIcon: {
      marginLeft: 4
    },
    searchIconHightLight: {
      marginLeft: 4,
      color: theme.palette.primary.main
    },
    clearIcon: {
      marginRight: 4
    },
    clearIconHightLight: {
      marginRight: 4,
      color: theme.palette.primary.main
    },
    actionIcon: {
      marginRight: 4
    },
    content: {
      width: '100%',
      height: '100%',
      display: 'flex',
      position: 'relative',
      flex: 1
    },
    layout: {
      zIndex: 1,
      position: 'absolute',
      backgroundColor: theme.palette.background.default,
      width: '100%',
      height: '100%',
      display: 'flex'
    },
    layoutHidden: {
      zIndex: 0,
      position: 'absolute',
      backgroundColor: theme.palette.background.default,
      width: '100%',
      height: '100%',
      display: 'flex',
      flexDirection: 'column'
    },
    layoutShow: {
      zIndex: 99,
      position: 'absolute',
      backgroundColor: theme.palette.background.default,
      width: '100%',
      height: '100%',
      display: 'flex',
      flexDirection: 'column'
    },
    mainList: {
      width: '100%',
      height: '100%',
      position: 'relative',
      display: 'flex'
    },
    center: {
      width: '100%',
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex'
    },
    textUser: {
      display: 'block',
      maxWidth: 'calc(100% - 20px)',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    textMessage: {
      display: 'block',
      maxWidth: 'calc(100% - 20px)',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    textPrimary: {
      color: theme.palette.primary.main,
      display: 'block',
      maxWidth: 'calc(100% - 20px)',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    textTime: {
      display: 'block',
      whiteSpace: 'nowrap',
      fontSize: 11,
      paddingBottom: 4
    },
    contentLoadingMore: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    badge: {
      right: 6,
      top: -10
    },
    contentRight: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'flex-end',
      alignItems: 'flex-end',
      height: '100%'
    },
    contentSelect: {
      width: 8,
      height: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end'
    },
    selectedItem: {
      width: 4,
      height: '100%',
      backgroundColor: theme.palette.primary.main
    },
    popupSearchOptions: {
      display: 'flex',
      outline: 'none',
      flexDirection: 'column'
    },
    popupSearchOptionsDialogAction: {
      alignItems: 'right',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end'
    },
    popupSearchContainer: {
      padding: 20
    },
    nodata: {
      transform: 'translate(-50%, -50%)',
      top: '50%',
      left: '50%',
      position: 'absolute'
    }
  })
);
