import React, { Component, useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
  Badge,
  InputBase,
  IconButton,
  Menu,
  Button,
  RadioGroup,
  Radio,
  FormControlLabel,
  FormLabel
} from '@material-ui/core';
import ReplayIcon from '@material-ui/icons/Replay';
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
import UserPhotoView from '../../../../components/UserPhoto';
import Api from '../../../../core/service/api';
import talkAPI from '../../../../core/service/talk.api.render';
import { useStyles } from './styles';
import { convertUserData } from '../utils';
import * as Actions from '../../../../actions';
import { AutoSizer, List } from 'react-virtualized';
import { useTranslation } from 'react-i18next';
import TimeAgo from 'javascript-time-ago';
import { SortAscIcon, SortDescIcon } from '../../../../components/HanSVGIcon';
import en from 'javascript-time-ago/locale/en';
import vi from 'javascript-time-ago/locale/vi';
import ko from 'javascript-time-ago/locale/ko';
import { timeAgoStringWithTimeAgoLib } from '../../../../utils';

import { timeFromTimeStampSafe } from '../../../../core/service/talk-constants';

TimeAgo.addLocale(en);
TimeAgo.addLocale(vi);
TimeAgo.addLocale(ko);

import SimpleBarHanbiro from '../../../../components/SimpleBarHanbiro';

const KEEP_WHISPER_LIST = false;

let readWhisper = {};
let timerSearch;

let uniqueKeyWhisperList;

export default function WhisperList(props) {
  const { t, i18n } = useTranslation();
  const [keyword, setKeyword] = useState('');
  const selected_whisper = useSelector(state => state.appUI.selected_whisper);
  const whisper_list = useSelector(state => state.whisper_list);
  const [page, setPage] = useState(1);
  const [maxPage, setMaxPage] = useState(1);
  const [loadingMore, setLoadingMore] = useState(false);
  const [requesting, setRequesting] = useState(false);
  const classes = useStyles();
  const dispatch = useDispatch();

  const firstUpdate = useRef(true);

  const [searchFocused, setSearchFocused] = useState(false);

  const [showSearchOptions, setShowSearchOptions] = useState(null);

  const [sortType, setSortType] = useState('DESC');
  const [searchType, setSearchType] = useState('all');
  const [searchTypeControl, setSearchTypeControl] = useState('all');

  const timeAgo = new TimeAgo(i18n.language);

  const requireReloadList = () => {
    Api.cancelRequest(uniqueKeyWhisperList);
    setPage(1);
    loadList(1, keyword);
  };

  useEffect(() => {
    readWhisper = {};
    // loadList(page, keyword);
    window.addEventListener('requireReloadList', requireReloadList);
    return () => {
      Api.cancelRequest(uniqueKeyWhisperList);
      window.removeEventListener('requireReloadList', requireReloadList);
    };
  }, []);

  useEffect(() => {
    // if (firstUpdate.current) {
    //   firstUpdate.current = false;
    //   return;
    // }
    onReload(1);
  }, [sortType, searchType]);

  /**
   * Load List Whisper
   * @param {*} page
   */
  const loadList = (page, search) => {
    setRequesting(true);
    if (talkAPI.isCookiesSuccess()) {
      loadListImpl(page, search);
    } else {
      uniqueKeyWhisperList = `cookies_${Date.now()}`;
      Api.get(
        talkAPI.getCookie(),
        {
          hkey: talkAPI.autoLoginKey
        },
        null,
        uniqueKeyWhisperList
      )
        .then(response => {
          if (response == null || (response.success && response.hanbiro_gw)) {
            talkAPI.applyCookiesStatus(true, response?.hanbiro_gw);
            loadListImpl(page, search);
          } else {
            setRequesting(false);
            talkAPI.applyCookiesStatus(false);
          }
        })
        .catch(error => {
          talkAPI.applyCookiesStatus(false);
          if (Api.isCancel(error)) {
            return;
          }
          setRequesting(false);
        });
    }
  };

  /**
   * Load List Whisper
   * @param {*} page
   */
  const loadListImpl = (page, search) => {
    uniqueKeyWhisperList = Date.now();
    let body = new FormData();
    body.append('type', 'conversation');
    body.append('page', page);
    body.append('limit', 20);

    if (search) {
      body.append('searchtext', search);
      body.append('searchtype', searchType);
    }

    body.append('sortby', 'date');
    body.append('sorttype', sortType);

    // console.log(search, searchType, sortType, page);

    Api.post(talkAPI.whisperList(), body, null, uniqueKeyWhisperList)
      .then(response => {
        if (KEEP_WHISPER_LIST && page == 1) {
          dispatch(Actions.saveWhisperList([]));
        }

        if (response.rows) {
          dispatch(Actions.saveWhisperList(response.rows, page != 1));
          setMaxPage(response?.attr?.maxPage ?? 1);
          setLoadingMore(false);
        }
        setRequesting(false);
      })
      .catch(error => {
        if (Api.isCancel(error)) {
          return;
        }
        if (KEEP_WHISPER_LIST && page == 1) {
          dispatch(Actions.saveWhisperList([]));
        }

        setRequesting(false);
        if (error.response && error.response.status == 401) {
          talkAPI.applyCookiesStatus(false);
        }
      });
  };

  /**
   * On Change search keyword
   * @param {*} value
   */
  const onChange = value => {
    setKeyword(value);
    if (timerSearch) {
      clearTimeout(timerSearch);
    }
    timerSearch = setTimeout(() => {
      Api.cancelRequest(uniqueKeyWhisperList);
      dispatch(Actions.saveWhisperList([]));
      loadList(1, value);
    }, 500);
  };

  /**
   * Action Reload Whisper page
   * @param {*} page
   */
  const onReload = page => {
    Api.cancelRequest(uniqueKeyWhisperList);
    setPage(page);
    if (!KEEP_WHISPER_LIST) {
      dispatch(Actions.saveWhisperList([]));
    }

    loadList(page, keyword);
  };

  const onSortChange = () => {
    setSortType(sortType == 'ASC' ? 'DESC' : 'ASC');
  };

  const onSearchTypeChange = event => {
    setSearchTypeControl(event.target.value);
  };

  const onClosePopup = () => {
    handleSearchOptionsClose();
  };

  const onApplyPopup = () => {
    handleSearchOptionsClose();
    setSearchType(searchTypeControl);
  };

  const handleSearchOptionsShow = e => {
    setSearchTypeControl(searchType);
    setShowSearchOptions(e.currentTarget);
  };

  const handleSearchOptionsClose = () => {
    setShowSearchOptions(null);
  };

  /**
   * onScroll Listener
   * @param {*} e
   */
  const onScroll = e => {
    const { scrollTop, scrollHeight, clientHeight } = e.target;
    if (scrollHeight && scrollHeight - scrollTop == clientHeight) {
      if (page < maxPage) {
        const newPage = page + 1;
        setLoadingMore(true);
        setPage(newPage);
        loadList(newPage, keyword);
      }
    }
  };

  /**
   * Render Row Items
   * @param {*} { index, key, style }
   * @returns
   */
  const Row = ({ index, key, style }) => {
    const loadMore = index == whisper_list.whisper.length;

    if (loadMore) {
      return (
        <div style={style} key={key} className={classes.contentLoadingMore}>
          <div className="han-loading mini" />
        </div>
      );
    }

    const item = whisper_list.whisper[index];
    const selected =
      item.unique_id == selected_whisper?.unique_id ||
      item.unique_id == selected_whisper?.unique_id_reverse;
    if (selected) {
      readWhisper[item.unique_id] = true;
    }

    const unread = item.unread && !readWhisper[item.unique_id] && !selected;
    const unreadCount = item.unread_count ?? 0;
    const userKey = convertUserData(item.ucn, item.userno);
    const name = item.name1 ?? item.name;

    const timeDisplay = timeAgoStringWithTimeAgoLib(
      timeFromTimeStampSafe(item.date_timestamp),
      timeAgo
    );

    return (
      <ListItem
        className={classes.item}
        style={style}
        key={key}
        button
        selected={selected}
        onClick={() => {
          readWhisper[item.unique_id] = true;
          dispatch(Actions.openWhisper(item));
        }}
      >
        <ListItemIcon>
          <UserPhotoView
            key={userKey}
            imgSize={50}
            userKeyData={userKey}
            placeHolder={'./images/user-default.jpg'}
          />
        </ListItemIcon>
        <ListItemText
          primary={
            <Typography variant="subtitle2" className={classes.textUser}>
              {name}
            </Typography>
          }
          secondary={
            <Typography
              variant="caption"
              color="textSecondary"
              className={unread ? classes.textPrimary : classes.textMessage}
            >
              {item.memo}
            </Typography>
          }
        />
        <div style={{ display: 'flex', flexDirection: 'row', height: '100%' }}>
          <div className={classes.contentRight}>
            {unread && unreadCount > 0 ? (
              <Badge
                className={classes.badge}
                badgeContent={unreadCount}
                max={99}
                color="error"
              ></Badge>
            ) : null}
            <Typography
              variant="caption"
              color="textSecondary"
              className={classes.textTime}
            >
              {timeDisplay}
            </Typography>
          </div>
          <div className={classes.contentSelect}>
            {selected && <div className={classes.selectedItem}></div>}
          </div>
        </div>
      </ListItem>
    );
  };

  /**
   * Item Size
   * @param {*} { index }
   * @returns
   */
  const itemSize = ({ index }) => {
    return 70;
  };

  const loading = whisper_list.whisper.length == 0 && requesting;
  const showNoData = whisper_list.whisper.length == 0 && !requesting;

  return (
    <div className={classes.main}>
      <div className={classes.search}>
        <IconButton
          onClick={handleSearchOptionsShow}
          size="small"
          className={
            searchFocused ? classes.searchIconHightLight : classes.searchIcon
          }
        >
          <SearchIcon fontSize="small" />
        </IconButton>
        <InputBase
          className={classes.inputSearch}
          style={{ height: 25 }}
          onFocus={() => {
            setSearchFocused(true);
          }}
          onBlur={() => {
            setSearchFocused(false);
          }}
          placeholder={searchFocused ? null : t('Search')}
          value={keyword}
          onChange={event => onChange(event.target.value)}
        />
        <IconButton
          size="small"
          className={
            searchFocused ? classes.clearIconHightLight : classes.clearIcon
          }
          onClick={() => onChange('')}
        >
          <ClearIcon fontSize="small" />
        </IconButton>

        <IconButton
          onClick={() => onReload(1)}
          size="small"
          className={classes.actionIcon}
        >
          <ReplayIcon fontSize="small" />
        </IconButton>
        <IconButton
          onClick={() => onSortChange()}
          size="small"
          className={classes.actionIcon}
        >
          {sortType == 'ASC' ? (
            <SortAscIcon fontSize="small" />
          ) : (
            <SortDescIcon fontSize="small" />
          )}
        </IconButton>
      </div>

      {loading ? (
        <div className={classes.center}>
          <div className="han-loading mini" />
        </div>
      ) : (
        <div className={classes.mainList}>
          <ListWrapper
            Row={Row}
            onScroll={onScroll}
            itemSize={itemSize}
            whisper_list={whisper_list}
            loadingMore={loadingMore}
          ></ListWrapper>
        </div>
      )}

      {showNoData && (
        <Typography className={classes.nodata} variant="body2">
          {t('no_result')}
        </Typography>
      )}
      <Menu
        id="simple-menu"
        anchorEl={showSearchOptions}
        open={Boolean(showSearchOptions)}
        onClose={handleSearchOptionsClose}
        PaperProps={{
          style: {
            overflow: 'hidden',
            transform: 'translateY(50%)'
          }
        }}
      >
        <div className={classes.popupSearchOptions}>
          <div className={classes.popupSearchContainer}>
            <FormLabel component="legend">{t('Search type')}</FormLabel>
            <RadioGroup
              value={searchTypeControl}
              onChange={onSearchTypeChange}
              row
            >
              <FormControlLabel
                value="all"
                control={<Radio />}
                label={t('All')}
              />
              <FormControlLabel
                value="to"
                control={<Radio />}
                label={t('To')}
              />
              <FormControlLabel
                value="sender"
                control={<Radio />}
                label={t('Sender')}
              />
              <FormControlLabel
                value="whisper"
                control={<Radio />}
                label={t('Whisper')}
              />
            </RadioGroup>
          </div>
          <div className={classes.popupSearchOptionsDialogAction}>
            <Button onClick={onApplyPopup}>{t('Apply')}</Button>
            <Button onClick={onClosePopup}>{t('Close')}</Button>
          </div>
        </div>
      </Menu>
    </div>
  );
}

class ListWrapper extends Component {
  constructor(props) {
    super(props);

    this.simpleBar = null;
    this.parentRef = React.createRef();
    this.listRef = React.createRef();
    this.onScroll = this.onScroll.bind(this);
  }

  componentDidMount() {
    this.simpleBar = new SimpleBarHanbiro(this.parentRef.current);
  }

  onScroll(e) {
    const { onScroll } = this.props;
    const { scrollTop, scrollLeft } = e.target;
    const { Grid: grid } = this.listRef.current;
    grid.handleScrollEvent({ scrollTop, scrollLeft });
    if (onScroll) {
      onScroll(e);
    }
  }

  render() {
    const { Row, itemSize, whisper_list, loadingMore } = this.props;
    return (
      <AutoSizer>
        {({ height, width }) => {
          return (
            <div
              ref={this.parentRef}
              onScroll={this.onScroll}
              style={{
                position: 'relative',
                width: width,
                height: height
              }}
            >
              <List
                style={{ overflowX: false, overflowY: false, outline: 'none' }}
                ref={this.listRef}
                height={height}
                rowCount={whisper_list.whisper.length + (loadingMore ? 1 : 0)}
                rowHeight={itemSize}
                width={width}
                rowRenderer={Row}
              ></List>
            </div>
          );
        }}
      </AutoSizer>
    );
  }
}
