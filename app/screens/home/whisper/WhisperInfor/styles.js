import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    main: {
      display: 'flex',
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingLeft: 16,
      paddingRight: 16
    },
    content: {
      display: 'flex',
      width: '80%',
      flexDirection: 'row',
      alignItems: 'center'
    },
    nameContent: {
      display: 'table',
      paddingLeft: 8,
      tableLayout: 'fixed',
      width: '90%',
      alignItems: 'center'
    },
    textUser: {
      display: 'block',
      width: '100%',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    circleOnline: {
      backgroundColor: theme.palette.success.light,
      width: 10,
      height: 10,
      borderRadius: '50%'
      // marginTop: 3
    },
    circleOffline: {
      backgroundColor: '#cbced1',
      width: 10,
      height: 10,
      borderRadius: '50%'
      // marginTop: 3
    },
    contentStatus: {
      width: 16,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      marginRight: 4
    },
    contentStatusSmall: {
      position: 'absolute',
      bottom: 5,
      right: -3,
      width: 14,
      height: 14,
      backgroundColor: 'white',
      borderRadius: '50%',
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex'
    },
    contentStatusLarge: {
      position: 'absolute',
      bottom: 5,
      right: -3,
      width: 18,
      height: 18,
      backgroundColor: 'white',
      borderRadius: '50%',
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex'
    },
    main_photo: {
      flex: 'flex-wrap',
      width: 50,
      height: 50,
      alignItems: 'center',
      justifyContent: 'center'
    }
  })
);
