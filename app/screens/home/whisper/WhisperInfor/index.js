import React, { useContext } from 'react';
import { useSelector } from 'react-redux';
import { Typography, ButtonBase } from '@material-ui/core';

import PropTypes from 'prop-types';
import { useStyles } from './styles';
import UserPhotoView from '../../../../components/UserPhoto';
import { convertUserData } from '../utils';

import { OrgContext } from '../../../home';

import { StatusMode } from '../../../../core/service/talk-constants';
import TalkIcon from '../../../../components/TalkIcon';

export default function WhisperInfor(props) {
  const { onOpenProfile } = useContext(OrgContext);
  const data = useSelector(state => state.appUI.selected_whisper);
  const user_cached = useSelector(state => state.company.user_cached);
  const classes = useStyles();
  const userKey = data.userKey ?? convertUserData(data.ucn, data.userno);
  const contact = user_cached[userKey];
  const displayName = contact ? contact.displayName : data.name1;
  const name = displayName ?? data.name;

  const onPress = () => {
    onOpenProfile(contact);
  };

  const exportIconStatus = status => {
    switch (status) {
      case StatusMode.logout:
      case StatusMode.offline:
        return (
          <div className={classes.contentStatusSmall}>
            <div className={classes.circleOffline} />
          </div>
        );
      case StatusMode.available:
        return (
          <div className={classes.contentStatusSmall}>
            <div className={classes.circleOnline} />
          </div>
        );
      case StatusMode.away:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="idle" />
          </div>
        );
      case StatusMode.busy:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="busy" />
          </div>
        );
      case StatusMode.meeting:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="meeting" />
          </div>
        );
      case StatusMode.meal:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="meal" />
          </div>
        );
      case StatusMode.phone:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="call" />
          </div>
        );
      case StatusMode.out:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="out" />
          </div>
        );
      case StatusMode.business_trip:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="business_trip" />
          </div>
        );

      default:
        return null;
    }
  };

  const userStatus = exportIconStatus(contact?.userStatus);
  return (
    <div className={classes.main}>
      <div className={classes.content}>
        <div style={{ width: 50 }}>
          <ButtonBase onClick={onPress} disabled={!contact}>
            <div style={{ position: 'relative' }}>
              <UserPhotoView
                key={userKey}
                data={contact}
                className={classes.main_photo}
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: '50%'
                }}
                userKeyData={userKey}
                imgSize={50}
              />
              {userStatus}
            </div>
          </ButtonBase>
        </div>
        <div className={classes.nameContent}>
          <Typography variant="subtitle2" className={classes.textUser}>
            {name}
          </Typography>
        </div>
      </div>
    </div>
  );
}

WhisperInfor.propTypes = {
  data: PropTypes.object,
  onPressMore: PropTypes.func,
  onReload: PropTypes.func
};

WhisperInfor.defaultProps = {
  data: {},
  onPressMore: () => {},
  onReload: () => {}
};
