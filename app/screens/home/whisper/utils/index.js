export const convertUserData = (cn, no) => {
  let ucn = parseInt(cn);
  let uno = parseInt(no);

  let ucnStr = ucn.toString();
  ucnStr = ucnStr.padStart(3, '0');

  return ucnStr + uno.toString();
};

export const userKeyToWhisperKey = userKey => {
  const cn = parseInt(userKey.substring(0, 3));
  const no = parseInt(userKey.substring(3));
  return cn + '_' + no;
};
