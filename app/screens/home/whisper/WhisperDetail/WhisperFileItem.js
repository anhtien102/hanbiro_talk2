import React, { useState } from 'react';
import { useStyles } from './styles';
import { remote } from 'electron';
import GetAppIcon from '@material-ui/icons/GetApp';
import FolderOpen from '@material-ui/icons/FolderOpen';
import ImageIcon from '@material-ui/icons/Image';
import VisibilityIcon from '@material-ui/icons/Visibility';
import ErrorIcon from '@material-ui/icons/Error';
import MusicVideoIcon from '@material-ui/icons/MusicVideo';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import { fileExtension } from '../../../../core/model/MessageUtils';
import {
  ListItem,
  Avatar,
  IconButton,
  CircularProgress,
  Typography
} from '@material-ui/core';
import OndemandVideoIcon from '@material-ui/icons/OndemandVideo';
import Api from '../../../../core/service/api';
import talkAPI from '../../../../core/service/talk.api.render';
import * as TalkConstants from '../../../../core/service/talk-constants';

import {
  remoteShowItemInFolder,
  remoteOpenItem
} from '../../../../utils/electron.utils';

import PropTypes from 'prop-types';

export default function WhisperFileItem(props) {
  const { item, style } = props;
  const [path, setPath] = useState('');
  const [percent, setPercent] = useState(0);
  const [error, setError] = useState(false);
  const classes = useStyles();

  const openFolderDownload = path => {
    remoteShowItemInFolder(path);
  };

  const openFileDownload = path => {
    remoteOpenItem(path);
  };

  const downLoadFile = () => {
    setError(false);

    const { dialog } = remote;
    let fileName = item.filename ?? '';
    fileName = fileName.normalize();
    dialog
      .showSaveDialog(remote.getCurrentWindow(), {
        defaultPath: fileName
      })
      .then(result => {
        if (result.canceled) {
          return;
        }

        let destPath = result.filePath;
        let extension = fileExtension(destPath);
        if (extension == null || extension == '') {
          destPath = `${destPath}.${fileExtension(fileName)}`;
        }

        setPercent(1);
        Api.download(
          talkAPI.downloadWhisperFile(item.link),
          { key: item.id, fileName: item.filename },
          {},
          item.filename,
          destPath,
          response => {
            if (response.percent) {
              setPercent(response.percent);
            }
            if (response.path) {
              setPath(response.path);
            }
          }
        ).catch(error => {
          setError(true);
        });
      });
  };

  const getFileType = () => {
    const fileExt = TalkConstants.getFileExtension(item.filename);
    switch (fileExt) {
      case TalkConstants.MessageFileEXT.IMAGE_EXT:
        return <ImageIcon />;
      case TalkConstants.MessageFileEXT.VIDEO_EXT:
        return <OndemandVideoIcon />;
      case TalkConstants.MessageFileEXT.RECORD_EXT:
        return <MusicVideoIcon />;
      default:
        return <InsertDriveFileIcon />;
    }
  };

  return (
    <ListItem className={classes.fileContent} style={style}>
      <div className={classes.fileContentLeft}>
        <Avatar>{getFileType()}</Avatar>
        <div className={classes.fileContentTitle}>
          <Typography variant="caption" className={classes.nameFile}>
            {item.filename}
          </Typography>
          <Typography
            variant="caption"
            color="textSecondary"
            className={classes.fileSize}
          >
            {item.filesize}
          </Typography>
        </div>
      </div>
      {path ? (
        <div
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            display: 'flex'
          }}
        >
          <IconButton
            onClick={() => {
              openFileDownload(path);
            }}
          >
            <VisibilityIcon />
          </IconButton>
          <IconButton onClick={() => openFolderDownload(path)}>
            <FolderOpen />
          </IconButton>
        </div>
      ) : error ? (
        <IconButton onClick={downLoadFile}>
          <ErrorIcon />
        </IconButton>
      ) : (
        <IconButton onClick={downLoadFile}>
          {percent ? (
            <div style={{ position: 'relative', width: 24, height: 24 }}>
              <CircularProgress
                style={{ position: 'absolute', top: 0, left: 0, color: '#eee' }}
                variant="static"
                value={100}
                size={24}
              />
              <CircularProgress
                style={{ position: 'absolute', top: 0, left: 0 }}
                variant="static"
                value={percent}
                size={24}
              />
            </div>
          ) : (
            <GetAppIcon />
          )}
        </IconButton>
      )}
    </ListItem>
  );
}

WhisperFileItem.propTypes = {
  item: PropTypes.object,
  style: PropTypes.object
};

WhisperFileItem.defaultProps = {
  item: {},
  style: {}
};
