import React, { useEffect, Component, useState, Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../../../actions';
import Snackbar from '@material-ui/core/Snackbar';
import Api from '../../../../core/service/api';
import talkAPI from '../../../../core/service/talk.api.render';
import WhisperMessageItem from './WhisperMessageItem';
import { useStyles } from './styles';
import FileInput from '../../../../components/FileInput';
import WhisperEditor from '../../../../components/WhisperEditor';
import ImageOutlinedIcon from '@material-ui/icons/ImageOutlined';
import AttachFileOutlinedIcon from '@material-ui/icons/AttachFileOutlined';
import WhisperDropFile from '../../../../components/WhisperDropFile';
import fs from 'fs';
import { convertUserData, userKeyToWhisperKey } from '../utils';
import SimpleBarHanbiro from '../../../../components/SimpleBarHanbiro';

const useHTML = true;
let uniqueKey;
let lastU2u;
export default function WhisperDetail(props) {
  const selected_whisper = useSelector(state => state.appUI.selected_whisper);
  const userKey =
    selected_whisper.userKey ??
    convertUserData(selected_whisper.ucn, selected_whisper.userno);
  const whisper_detail = useSelector(state => state.appUI.whisper_detail);
  const whisper_u2u = useSelector(state => state.appUI.whisper_u2u);
  const whisper_page = useSelector(state => state.appUI.whisper_page);
  const [loadApi, setLoadApi] = useState(false);
  const [canLoadMore, setCanLoadMore] = useState(true);
  const [loadingMore, setLoadingMore] = useState(false);

  const classes = useStyles();
  const dispatch = useDispatch();

  const requireReloadList = event => {
    console.log(event.detail);
    loadDetail(event.detail.u2u);
  };

  useEffect(() => {
    window.addEventListener('requireReloadList', requireReloadList);
    return () => {
      window.removeEventListener('requireReloadList', requireReloadList);
      Api.cancelRequest(uniqueKey);
    };
  }, []);

  useEffect(() => {
    if (lastU2u != whisper_u2u) {
      dispatch(Actions.saveWhisperDetail([], whisper_u2u, 0));
    }
  }, [whisper_u2u]);

  useEffect(() => {
    loadDetail(selected_whisper.u2u);
  }, [selected_whisper.u2u]);

  const loadDetail = u2u => {
    setLoadApi(true);
    lastU2u = u2u;
    Api.cancelRequest(uniqueKey);
    uniqueKey = Date.now();
    let body = new FormData();
    body.append('u2u', u2u);
    body.append('page', 1);
    if (useHTML) {
      body.append('format', 'html');
    }

    body.append('limit', 20);
    body.append('bigdata_enabled', 'true');
    Api.post(talkAPI.whisperView(), body, null, uniqueKey)
      .then(respone => {
        if (lastU2u == u2u) {
          if (respone.rows) {
            dispatch(Actions.saveWhisperDetail(respone.rows, u2u, 1));
          }
          setCanLoadMore(respone.rows?.length > 0);
          setLoadApi(false);
        }
      })
      .catch(error => {
        if (lastU2u == u2u) {
          setLoadApi(false);
        }
      });
  };

  const loadDetailMore = u2u => {
    const page = whisper_page + 1;
    setLoadApi(true);
    Api.cancelRequest(uniqueKey);
    uniqueKey = Date.now();
    let body = new FormData();
    body.append('u2u', u2u);
    body.append('page', page);
    if (useHTML) {
      body.append('format', 'html');
    }
    body.append('limit', 20);
    body.append('bigdata_enabled', 'true');
    Api.post(talkAPI.whisperView(), body, null, uniqueKey)
      .then(respone => {
        if (lastU2u == u2u) {
          if (respone.rows) {
            dispatch(Actions.saveWhisperDetail(respone.rows, u2u, page));
          }
          setCanLoadMore(respone.rows?.length > 0);
          setLoadApi(false);
        }
        setLoadingMore(false);
      })
      .catch(error => {
        if (lastU2u == u2u) {
          setLoadApi(false);
        }
        setLoadingMore(false);
      });
  };

  const loading = whisper_detail.length == 0;
  if (loading) {
    return (
      <div className={classes.centerView}>
        <div className="han-loading mini" />
      </div>
    );
  }

  const onLoadMore = () => {
    if (loadApi || !canLoadMore) {
      return;
    }
    setLoadingMore(true);
    loadDetailMore(selected_whisper.u2u);
  };

  return (
    <PerfectScrollWrapper
      classes={classes}
      userKey={userKey}
      whisper_detail={whisper_detail}
      onLoadMore={onLoadMore}
      selected_whisper={selected_whisper}
      loadingMore={loadingMore}
    ></PerfectScrollWrapper>
  );
}

class PerfectScrollWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sending: false,
      editorHeight: 65,
      openSnackbar: { show: false, msg: '' }
    };
    this.writeUniqueKeyDetail = null;
    this.simpleBar = null;
    this.keepScrollBottom = true;
    this.onScroll = this.onScroll.bind(this);

    this.handleWindowResize = this.handleWindowResize.bind(this);
    this.whisperEditor = null;

    this.listRef = React.createRef();

    this.handleCopy = this.handleCopy.bind(this);
    this.getUserSelection = this.getUserSelection.bind(this);
    this.viewItemContainerRef = React.createRef();
  }

  getUserSelection() {
    var userSelection;
    if (window.getSelection) {
      userSelection = window.getSelection();
    } else if (document.selection) {
      // should come last; Opera!
      userSelection = document.selection.createRange();
    }
    return userSelection;
  }

  handleCopy(e) {
    e.preventDefault();
    const selection = this.getUserSelection();
    event.clipboardData.setData('text/plain', selection.toString());
  }

  handleWindowResize(e) {
    if (this.keepScrollBottom) {
      this.simpleBar?.scrollToBottom();
    }
  }

  onScroll(e) {
    let element = e.target;

    if (element.scrollTop == 0) {
      const { onLoadMore } = this.props;
      if (onLoadMore) {
        onLoadMore();
      }
    } else {
      const diffScrollTop = element.scrollHeight - element.clientHeight;
      const scrollAtBottom = diffScrollTop - element.scrollTop <= 0;
      this.keepScrollBottom = scrollAtBottom;
    }
  }

  componentDidMount() {
    this.viewItemContainerRef.current?.addEventListener(
      'copy',
      this.handleCopy
    );
    this.simpleBar = new SimpleBarHanbiro(this.listRef.current);
    this.simpleBar?.scrollToBottom();
    window.addEventListener('resize', this.handleWindowResize);
    window.addEventListener('splitPannelChanged', this.handleWindowResize);
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    // Are we adding new items to the list?
    // Capture the scroll position so we can adjust scroll later.

    if (
      prevProps.whisper_detail.length < this.props.whisper_detail.length &&
      this.simpleBar
    ) {
      const prev_scroll_top =
        this.simpleBar.getScrollHeight() - this.simpleBar.getScrollTop();
      return {
        prev_scroll_top: prev_scroll_top
      };
    }

    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const scrollView = this.listRef.current;
    if (this.keepScrollBottom) {
      this.simpleBar.scrollToBottom();
    } else if (snapshot !== null) {
      this.simpleBar.scrollTo(
        this.simpleBar.getScrollHeight() - snapshot.prev_scroll_top
      );
    }
  }

  componentWillUnmount() {
    this.viewItemContainerRef.current?.removeEventListener(
      'copy',
      this.handleCopy
    );
    Api.cancelRequest(this.writeUniqueKeyDetail);
    window.removeEventListener('resize', this.handleWindowResize);
    window.removeEventListener('splitPannelChanged', this.handleWindowResize);
  }

  handleDrop = e => {
    e.preventDefault();
    e.stopPropagation();
    if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
      if (this.whisperEditor) {
        let files = [];
        for (let f of e.dataTransfer.files) {
          const item = { name: f.name, path: f.path, size: f.size };
          files.push(item);
        }
        this.whisperEditor.addAttachment(files);
      }
      e.dataTransfer.clearData();
    }
  };

  addFile = fileData => {
    if (this.whisperEditor) {
      let files = [];
      for (let f of fileData) {
        const item = { name: f.name, path: f.path, size: f.size };
        files.push(item);
      }
      this.whisperEditor.addAttachment(files);
    }
  };

  readFile = file => {
    return new Promise((resolve, reject) => {
      fs.readFile(file.path, (err, data) => {
        if (err) {
          resolve(null);
          return;
        }
        resolve(data);
      });
    });
  };

  sendWriteWhisperAPI = async (to, enterText, attachments, callback) => {
    let body = new FormData();
    body.append('to', to);
    body.append('memo', enterText ?? '');
    body.append('bigdata_enabled', 'true');
    let i = 0;
    for (let att of attachments) {
      const data = await this.readFile(att);
      if (data) {
        i++;
        const file = new File([data], att.name);
        body.append('file' + i, file);
      }
    }
    this.writeUniqueKeyDetail = Date.now();
    Api.postWithPercent(
      talkAPI.whisperWrite(),
      body,
      null,
      this.writeUniqueKeyDetail,
      percent => {
        console.log(percent);
      }
    )
      .then(respone => {
        callback(respone);
        this.setState({ sending: false });
      })
      .catch(error => {
        callback({ success: false, msg: error?.message ?? 'unknown' });
        this.setState({ sending: false });
      });
  };

  onSendWhisper = (enterText, attachments) => {
    const { userKey, selected_whisper } = this.props;

    if (enterText || attachments.length > 0) {
      this.setState({ sending: true });
      this.sendWriteWhisperAPI(
        userKeyToWhisperKey(userKey),
        enterText,
        attachments,
        respone => {
          if (respone.success) {
            this.whisperEditor.onClear();
            this.whisperEditor.clearAttachment();
            const event = new CustomEvent('requireReloadList', {
              detail: { u2u: selected_whisper.u2u }
            });
            window.dispatchEvent(event);
          } else {
            this.setState({
              openSnackbar: { show: true, msg: respone.msg }
            });
          }
        }
      );
    }
  };

  render() {
    const { whisper_detail, classes, loadingMore } = this.props;
    const { openSnackbar, sending, editorHeight } = this.state;
    const allowFileTransfer = talkAPI.allowFileTransfer();
    // const height = editorHeight + 44;
    return (
      <div className={classes.article}>
        <div className={classes.container}>
          <div
            className={classes.contentList}
            ref={this.listRef}
            onScroll={this.onScroll}
          >
            <div
              className={classes.viewItemContainer}
              ref={this.viewItemContainerRef}
            >
              {loadingMore && (
                <div className={classes.loadMoreContent}>
                  <div className="han-loading mini" />
                </div>
              )}
              {whisper_detail.map(item => {
                return <WhisperMessageItem key={item.unique_id} data={item} />;
              })}
            </div>
          </div>
        </div>
        <div className={classes.whisperChatInput}>
          <div className={classes.chatAction}>
            {allowFileTransfer && (
              <>
                <FileInput
                  accept="image/*"
                  style={{ display: 'inline' }}
                  onChange={this.addFile}
                >
                  <button type="button" className={classes.chatButton}>
                    <ImageOutlinedIcon />
                  </button>
                </FileInput>
                <FileInput
                  style={{ display: 'inline' }}
                  onChange={this.addFile}
                >
                  <button type="button" className={classes.chatButton}>
                    <AttachFileOutlinedIcon />
                  </button>
                </FileInput>
              </>
            )}
          </div>
          <div className={classes.bodyContainer}>
            {allowFileTransfer && (
              <WhisperDropFile
                contextId={'whisper_editor'}
                onSend={this.handleDrop}
              />
            )}

            <div id="whisper_editor">
              <WhisperEditor
                onRef={ref => {
                  this.whisperEditor = ref;
                }}
                minHeight={125}
                onSend={this.onSendWhisper}
                sending={sending}
                allowFileTransfer={allowFileTransfer}
              />
            </div>
          </div>
        </div>
        <Snackbar
          autoHideDuration={2000}
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={openSnackbar.show}
          onClose={() =>
            this.setState({
              openSnackbar: { show: false, msg: '' }
            })
          }
          message={openSnackbar.msg}
        />
      </div>
    );
  }
}
