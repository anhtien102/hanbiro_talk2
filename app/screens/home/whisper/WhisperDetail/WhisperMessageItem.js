import React, { useContext } from 'react';
import { useStyles } from './styles';
import UserPhotoView from '../../../../components/UserPhoto';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Typography, ButtonBase } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { convertUserData } from '../utils';
import { convertHtmlToPlainText } from '../../../../utils';

import WhisperFileItem from './WhisperFileItem';
import Linkify from 'linkifyjs/react';
import { openLink } from '../../../../utils/electron.utils';

import { OrgContext } from '../../../home';

export default function WhisperMessageItem(props) {
  const { t } = useTranslation();
  const { onOpenProfile } = useContext(OrgContext);
  const user_cached = useSelector(state => state.company.user_cached);
  const classes = useStyles();
  const { data } = props;
  const userKey = convertUserData(data.cn, data.from);

  const contact = user_cached[userKey];
  const displayName = contact ? contact.displayName : data.name;

  const isMe = data.me == 'me';

  const messageWhisper = convertHtmlToPlainText(data.memo);

  return (
    <div className={classes.contentItem}>
      <ButtonBase
        onClick={() => {
          onOpenProfile(contact);
        }}
        disabled={!contact}
        style={{ width: 35, height: 35 }}
      >
        <UserPhotoView
          style={{ width: 35, height: 35 }}
          key={userKey}
          userKeyData={userKey}
          imgSize={50}
        />
      </ButtonBase>

      <div className={classes.contentRight}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center'
          }}
        >
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className={classes.chatUserName}
          >
            {displayName}
          </Typography>
          <Typography
            variant="caption"
            color="textSecondary"
            className={classes.date}
          >
            {data.time}
          </Typography>
        </div>
        {messageWhisper && (
          <div
            className={isMe ? classes.messageContentMe : classes.messageContent}
          >
            <Linkify
              options={{
                attributes: {
                  onClick: openLink
                }
              }}
            >
              <Typography
                component={'div'}
                variant="body2"
                className={classes.message}
              >
                {messageWhisper}
              </Typography>
            </Linkify>
          </div>
        )}
        {data.files.map(item => {
          return (
            <WhisperFileItem
              item={item}
              key={item.id}
              style={{ marginTop: 8 }}
            />
          );
        })}
      </div>
    </div>
  );
}

WhisperMessageItem.propTypes = {
  data: PropTypes.object
};

WhisperMessageItem.defaultProps = {
  data: {}
};
