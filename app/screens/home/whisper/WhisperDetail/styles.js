import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    article: {
      width: '100%',
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
      textShadow: '0px 0 0px rgba(0, 0, 0, 0.4)'
    },
    container: {
      flex: '1',
      overflow: 'hidden',
      position: 'relative'
    },
    whisperChatInput: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
      flex: 'none',
      position: 'relative'
    },
    contentItem: {
      display: 'flex',
      flexDirection: 'row',
      paddingBottom: 16,
      width: '100%'
    },
    centerView: {
      display: 'flex',
      width: '100%',
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center'
    },
    contentList: {
      position: 'relative',
      width: '100%',
      height: '100%'
    },
    viewItemContainer: {
      position: 'relative',
      padding: '16px 16px'
    },
    contentRight: {
      display: 'table',
      tableLayout: 'fixed',
      flexDirection: 'column',
      paddingLeft: 8,
      width: '100%'
    },
    chatUserName: {
      display: 'block',
      width: '100%',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    message: {
      width: '100%',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      color: 'rgba(0, 0, 0, 0.87)'
    },
    messageContent: {
      cursor: 'pointer',
      whiteSpace: 'pre-wrap',
      position: 'relative',
      display: 'inline-block',
      padding: '10px',
      backgroundColor: theme.palette.primary.light,
      borderRadius: '10px',
      verticalAlign: 'bottom',
      textAlign: 'left',
      wordBreak: 'break-word',
      wordWrap: 'break-word',
      width: '100%',
      marginTop: 8
    },
    messageContentMe: {
      cursor: 'pointer',
      whiteSpace: 'pre-wrap',
      position: 'relative',
      display: 'inline-block',
      padding: '10px',
      borderRadius: '10px',
      verticalAlign: 'bottom',
      textAlign: 'left',
      wordBreak: 'break-word',
      wordWrap: 'break-word',
      width: '100%',
      backgroundColor: theme.palette.grey[300],
      marginTop: 8
    },
    fileContent: {
      padding: 0,
      margin: 0,
      justifyContent: 'space-between'
    },
    fileContentLeft: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center'
    },
    fileContentTitle: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      width: '90%',
      paddingLeft: 8
    },
    date: {
      whiteSpace: 'nowrap'
    },
    nameFile: {
      width: '100%',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    fileSize: {
      width: '100%',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    attachContent: {
      boxShadow: 'none',
      border: 'none',
      position: 'inherit',
      backgroundColor: theme.palette.background.default
    },
    loadMoreContent: {
      width: '100%',
      paddingBottom: 8,
      paddingTop: 8,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      display: 'flex'
    },
    chatAction: {
      padding: '5px 10px',
      borderTop: 1,
      borderTopStyle: 'solid',
      borderTopColor: theme.palette.divider,
      backgroundColor: theme.palette.background.paper,
      '&::after': {
        content: "''",
        clear: 'both',
        display: 'table'
      }
    },
    chatButton: {
      marginLeft: 15,
      marginRight: 15,
      padding: '4px 0px 0px 0px',
      color: '#767676',
      background: 'none',
      border: 'none',
      borderRadius: '10px',
      cursor: 'pointer',
      outline: 'none',
      '&:hover': {
        color: theme.palette.primary.main
      }
    },
    bodyContainer: {
      overflow: 'hidden',
      position: 'relative',
      display: 'flex',
      flexDirection: 'column'
    }
  })
);
