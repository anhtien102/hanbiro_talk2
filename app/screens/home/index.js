import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import {
  List,
  ListItem,
  ListItemIcon,
  Badge,
  Box,
  Menu,
  MenuItem,
  Typography,
  Tooltip,
  Popover
} from '@material-ui/core';
import { withTheme } from '@material-ui/core/styles';
import SplitPane from 'react-split-pane';
import Snackbar from '@material-ui/core/Snackbar';
import withStyles from './styles';
import { routes } from '../../configs';
import * as constantsApp from '../../configs/constant';
import { ipcRenderer, remote } from 'electron';
import orgWorker from '../../core/worker/orgtree';
import roomListWorker from '../../core/worker/roomlist';
import searchWorker from '../../core/worker/search';
import downloadUploadManager from '../../core/service/filemanager/download-upload-manager';
import Setting from './setting';
import UpdateDialog from '../../components/UpdateDialog';
import Translate from '../../components/Translate';
import OrgDialog from '../../components/OrgDialog';
import ProfileDialog from '../../components/ProfileDialog';
import WhisperDialog from '../../components/WhisperDialog';
import RoomUserChoiceDialog from '../../components/RoomUserChoiceDialog';
import HistoryDialog from '../../components/HistoryDialog';
import CreateGroupDialog from '../../components/CreateGroupDialog';

import {
  openLinkWithURL,
  remoteAskMediaAccess
} from '../../utils/electron.utils';
import TranslateIcon from '@material-ui/icons/Translate';
import {
  GroupwareIcon,
  WhisperIcon,
  RoomListIcon,
  ORGCompanyIcon,
  ORGFavoriteIcon,
  HistoryTabIcon,
  MailTabIcon,
  BoardTabIcon,
  CircularTabIcon,
  ApprovalTabIcon,
  TaskTabIcon,
  TodoTabIcon,
  GroupwareSettingIcon
} from '../../components/HanSVGIcon';

function isSelectionBackwards() {
  var backwards = false;
  if (window.getSelection) {
    var sel = window.getSelection();
    if (!sel.isCollapsed) {
      var range = document.createRange();
      range.setStart(sel.anchorNode, sel.anchorOffset);
      range.setEnd(sel.focusNode, sel.focusOffset);
      backwards = range.collapsed;
    }
  }
  return backwards;
}

import { convertHtmlToPlainText } from '../../utils';
import { userKeyToWhisperKey } from './whisper/utils';

import {
  getRoomDetailList,
  getMessageList
} from '../../core/worker/messagelist';
import { withTranslation } from 'react-i18next';
import talkApi from '../../core/service/talk.api.render';

const OPEN_NEW_BROWSER_GROUPWARE = true;

export const MenuOptionContext = React.createContext({
  onOpen: () => {},
  onClose: () => {},
  onOpenProfile: item => {},
  onOpenWhisperDialog: users => {},
  onOpenCreateGroupDialog: item => {},
  onOpenOrgDialog: () => {},
  onOpenTranslate: () => {}
});

export const OrgContext = React.createContext({
  onOpen: () => {},
  onOpenProfile: item => {},
  onOpenRoomUserChoiceDialog: dataItem => {},
  onOpenWhisperDialog: users => {},
  onOpenSearchHistoryDialog: roomKey => {}
});

class Home extends Component {
  constructor(props) {
    super(props);

    this.chatContainerRef = React.createRef();

    this.delayGetRoomList = null;
    this.delayGetTalkList = null;
    this.delayShowNotification = null;
    this.onSelectedUserOrg = null;

    this.delayShowDisconnect = null;

    this.requestQuitAndInstallApp = this.requestQuitAndInstallApp.bind(this);
    this.onSelectionChanged = this.onSelectionChanged.bind(this);

    this.webViewRef = React.createRef();
    this.popover = React.createRef();
    this.stopWebViewLoading = this.stopWebViewLoading.bind(this);
    this.openGroupwareBrowser = this.openGroupwareBrowser.bind(this);
    this.blockOpenGroupware = false;

    this.state = {
      useCacl: false,
      chatContainerWidth: 0,
      showSetting: false,
      showUpdateDialog: false,
      showOrgDialog: false,
      notAllowDisabledSelectedUser: false,
      changeOrgDialogTitleUpdate: false,
      translateData: {
        showTranslate: false,
        showTranslateResult: null,
        translateText: '',
        x: 0,
        y: 0
      },
      groupCreateDialogData: {
        show: false,
        item: null
      },
      historyDialogData: {
        show: false,
        item: null
      },
      profileDialogData: {
        show: false,
        item: null
      },
      roomUserDialogData: {
        show: false,
        data: null
      },
      whisperDialogData: {
        show: false,
        users: null
      },
      browserTabData: {
        show: false,
        url: null,
        loading: false
      },
      updateContent: null,
      activeMenu: '/',
      openSnackbar: false,
      errorSnackbar: '',
      mouseY: 0,
      mouseX: 0,
      childrenOptionMenu: [],
      listSelectedUser: [],
      maximumOrg: 50
    };
  }

  sendNetworkStatus() {
    ipcRenderer.send(
      'online-status-changed',
      navigator.onLine ? 'online' : 'offline'
    );
  }

  updateOnlineStatus = () => {
    this.sendNetworkStatus();
  };

  handleClosed() {
    this.setState({
      openSnackbar: false
    });
  }

  showAlert(msg) {
    this.setState({
      errorSnackbar: msg,
      openSnackbar: true
    });
  }

  isMessengerTabFocused(activeMenu) {
    return (
      activeMenu == '/favorite' || activeMenu == '/chat' || activeMenu == '/'
    );
  }

  findCategoryFromActiveTab(activeTab) {
    const TAB_CATEGORY = constantsApp.TAB_CATEGORY;
    switch (activeTab) {
      case '/favorite':
        return TAB_CATEGORY.tab_favorite;

      case '/chat':
        return TAB_CATEGORY.tab_room_list;

      case '/':
        return TAB_CATEGORY.tab_company;
    }
    return TAB_CATEGORY.tab_room_list;
  }

  isCurrentWindowFocused() {
    return (
      remote.getCurrentWindow().isFocused() &&
      remote.getCurrentWindow().isVisible() &&
      !remote.getCurrentWindow().isMinimized()
    );
  }

  findCurrentRoomKey() {
    const TAB_CATEGORY = constantsApp.TAB_CATEGORY;
    const { location } = this.props.history;

    let keySelectedRoom = TAB_CATEGORY.tab_company;

    const showSearchResult = this.props.show_search_result;
    if (showSearchResult) {
      keySelectedRoom = TAB_CATEGORY.tab_search;
    } else {
      switch (location.pathname) {
        case '/favorite':
          keySelectedRoom = TAB_CATEGORY.tab_favorite;
          break;
        case '/chat':
          keySelectedRoom = TAB_CATEGORY.tab_room_list;
          break;
        case '/':
          keySelectedRoom = TAB_CATEGORY.tab_company;
          break;
        default:
          keySelectedRoom = '';
          break;
      }
    }

    // return unique whisper key
    if (location.pathname == '/whisper') {
      return this.props.selected_whisper?.unique_id;
    }

    let roomInfo = this.props.selected_room
      ? this.props.selected_room[keySelectedRoom]
      : null;

    let roomKey = null;
    if (roomInfo && roomInfo.room) {
      roomKey = roomInfo.room.rRoomKey;
    }

    return roomKey;
  }

  onSelectionChanged() {
    if (this.props.commonSettings?.disable_translate) {
      return;
    }

    if (this.state.translateData.showTranslateResult) return;
    const selection = document.getSelection();
    const text = selection?.toString();
    if (text?.length > 0) {
      const rect = selection?.getRangeAt(0).getClientRects();
      let x, y;
      if (rect) {
        if (isSelectionBackwards()) {
          let position = rect[0];
          x = position?.x;
          y = position?.y;
        } else {
          let position = rect[rect.length - 1];
          x = position?.x + position?.width;
          y = position?.y;
        }
      }

      if (!x || !y) {
        return;
      }

      this.setState({
        translateData: {
          ...this.state.translateData,
          showTranslate: true,
          translateText: text,
          x: x,
          y: y - 40
        }
      });
    } else {
      this.setState({
        translateData: {
          ...this.state.translateData,
          showTranslate: false
        }
      });
    }
  }

  componentDidMount() {
    const { actions, history } = this.props;
    const webview = this.webViewRef.current;
    this.webviewFinishLoad = e => {
      this.setState({
        browserTabData: {
          ...this.state.browserTabData,
          loading: false
        }
      });
    };
    if (webview) {
      webview.addEventListener('did-finish-load', this.webviewFinishLoad);
      webview.addEventListener('did-fail-load', this.webviewFinishLoad);
    }
    if (talkApi.allowTranslate()) {
      document.addEventListener('mouseup', this.onSelectionChanged);
    }

    this.setState({
      useCacl: true,
      chatContainerWidth: this.chatContainerRef.current.clientWidth
    });

    const location = history.location;
    talkApi.currentFirstRoomTime = 0;

    if (
      process.env.NODE_ENV === 'development' ||
      process.env.DEBUG_PROD === 'true'
    ) {
      console.log('DEVELOPMENT MODE');
      if (location) {
        this.setState({
          activeMenu: location.pathname
        });
      }
    } else {
      console.log('PRODUCT MODE');
      if (location && location.pathname != '/') {
        history.push('/');
      }
    }

    document.title = talkApi.companyTitle;

    downloadUploadManager.initFunc();

    this.updateOnlineStatus();
    window.addEventListener('online', this.updateOnlineStatus);
    window.addEventListener('offline', this.updateOnlineStatus);

    // ORG
    ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ORG_TREE, '');
    ipcRenderer.on(constantsApp.REPLY_FULL_QUERY_ORG_TREE, (event, args) => {
      args.loggedUser = this.props.logged_user;
      orgWorker.getTreeOrg(args).then(result => {
        actions.saveOrgTree(
          result.main_tree,
          result.main_tree_favorite,
          result.group_cached,
          result.user_cached,
          result.listActualUserKeyFavourite
        );
        ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ROOM_LIST, '');
      });
    });

    ipcRenderer.on(
      constantsApp.REPLY_FULL_QUERY_ORG_TREE_FOR_STATE,
      (event, args) => {
        args.loggedUser = this.props.logged_user;
        orgWorker.getTreeOrg(args).then(result => {
          actions.saveOrgTree(
            result.main_tree,
            result.main_tree_favorite,
            result.group_cached,
            result.user_cached,
            result.listActualUserKeyFavourite
          );
        });
      }
    );

    //ROOM LIST
    ipcRenderer.on(constantsApp.REPLY_FULL_QUERY_ROOM_LIST, (event, args) => {
      let curRoomKey = this.findCurrentRoomKey();
      let roomData = {
        listRoom: args,
        listUser: this.props.user_cached,
        loggedUser: this.props.logged_user,
        curRoomKey: curRoomKey,
        window_visible: this.isCurrentWindowFocused()
      };

      roomListWorker.getRoomList(roomData).then(result => {
        actions.saveRoomList(result);
        if (this.props.show_search_result) {
          searchWorker.getSearchResultV2(result.rooms).then(searchResult => {
            if (searchResult) {
              actions.saveSearchResult(
                searchResult.searchText,
                searchResult.allSearchList
              );
            }
          });
        }
      });
    });

    //REPLY_QUERY_MESSAGE_LIST_ROOM_DETAIL
    ipcRenderer.on(
      constantsApp.REPLY_QUERY_MESSAGE_LIST_ROOM_DETAIL,
      (event, messageList, roomDetail, info) => {
        let curRoomKey = this.findCurrentRoomKey();
        let nextRoomKey = roomDetail.room ? roomDetail.room.rRoomKey : '';
        if (curRoomKey != nextRoomKey) {
          return;
        }

        let myInfo = null;
        if (
          this.props.user_cached &&
          this.props.logged_user?.account_info?.user_key
        ) {
          myInfo = this.props.user_cached[
            this.props.logged_user.account_info.user_key
          ];
        }

        const useBoth = !info.queryRoomDetailOnly;
        let roomDetailData = {
          roomDetails: roomDetail,
          userCached: this.props.user_cached,
          loggedUser: this.props.logged_user
        };

        if (useBoth) {
          let data = {
            ...messageList,
            loggedUser: this.props.logged_user,
            myInfo: myInfo
          };
          getRoomDetailList(roomDetailData).then(result => {
            getMessageList(data).then(messageResult => {
              actions.saveBothRoomInfoMessageList(
                result.room,
                result.listAllUserInRoom,
                result.listUserActiveInRoom,
                messageResult.messages,
                messageResult.roomKey,
                messageResult.roomID,
                messageResult.firstMessageTime
              );
            });
          });
        } else {
          getRoomDetailList(roomDetailData).then(result => {
            actions.saveRoomInfo(
              result.room,
              result.listAllUserInRoom,
              result.listUserActiveInRoom
            );
          });

          if (!info.queryRoomDetailOnly) {
            let data = {
              ...messageList,
              loggedUser: this.props.logged_user,
              myInfo: myInfo
            };
            getMessageList(data).then(result => {
              actions.saveMessageList(
                result.messages,
                result.roomKey,
                result.roomID,
                result.firstMessageTime
              );
            });
          }
        }
      }
    );

    ipcRenderer.on(constantsApp.MAIN_TO_RENDER_EVENT, (event, action, args) => {
      if (action == constantsApp.ACTION_SIPRES_RESPONSE) {
        this.openCall(action, args);
        return;
      }
      if (action == constantsApp.ACTION_SIP_INCOMING_CALL) {
        this.openCall(action, args);
        return;
      }
      if (action == constantsApp.ACTION_SHOW_DISCONNECT_SOCKET) {
        this.showDisconnectSocket(args.force, args.show);
        return;
      }

      if (action == constantsApp.ACTION_EXTRA_LOGIN_CHANGED) {
        talkApi.updateAllAccountInfo();
        actions.requestUpdateExtraLoginInfo(args);
        return;
      }

      if (action == constantsApp.ACTION_ORG_CHANGED) {
        ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ORG_TREE, '');
        return;
      }

      if (action == constantsApp.ACTION_ORG_STATUS_CHANGED) {
        ipcRenderer.send(
          constantsApp.REQUEST_FULL_QUERY_ORG_TREE_FOR_STATE,
          ''
        );
        return;
      }

      if (action == constantsApp.ACTION_ORG_TIME_CARD_CHANGED) {
        actions.saveOrgTimeCard(args);
        return;
      }

      if (action == constantsApp.ACTION_ROOM_LIST_CHANGED) {
        if (args?.hasNewMessage) {
          if (Object.keys(args?.hasNewMessage).length > 0) {
            if (!this.isCurrentWindowFocused()) {
              this.setFlashBadgeIcon();
            }
          }
        }
        ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ROOM_LIST, '');
        return;
      }

      if (action == constantsApp.ACTION_GOT_FOCUSED_WINDOWS_AGAIN) {
        ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ROOM_LIST, '');
        this.setBadgeNumberString(this.props.allUnreadMessageBadgeIcon);

        return;
      }

      if (action == constantsApp.ACTION_REPLY_ORG_ROOM_WITH_TEXT) {
        const data = {
          org: args.org,
          rooms: this.props.rooms,
          group_cached: this.props.group_cached,
          searchText: args.searchText,
          searchChosun: args.searchChosun,
          loggedUser: this.props.logged_user
        };
        searchWorker.getSearchResult(data).then(result => {
          actions.saveSearchResult(result.searchText, result.allSearchList);
        });
        return;
      }

      if (action == constantsApp.ACTION_MSG_GETTALK_CHANGED) {
        ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ROOM_LIST, '');
        let curRoomKey = this.findCurrentRoomKey();
        let nextRoomKey = args.roomKey;
        if (curRoomKey == nextRoomKey && args.firstTimeStamp > 0) {
          talkApi.currentFirstRoomTime = args.firstTimeStamp;
          ipcRenderer.send(
            constantsApp.REQUEST_QUERY_MESSAGE_LIST_ROOM_DETAIL,
            args
          );
        }
        return;
      }

      if (action == constantsApp.ACTION_RELOAD_ROOM_AND_REMOVE) {
        const roomKey = args.roomKey;
        ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ROOM_LIST, '');
        actions.checkAndClearRoomKey(roomKey);
      }

      if (action == constantsApp.ACTION_RELOAD_ALL_ROOM_CONTENT) {
        let roomKey = args.roomKey;
        let msgOnly = args.msgOnly;
        let queryRoomDetailOnly = args.queryRoomDetailOnly;
        let roomInfo = this.props.room_info;

        let lastRoomKey = null;
        let lastFirstTime = 0;
        let roomId = -1;
        if (roomInfo) {
          lastRoomKey = roomInfo.roomKey;
          lastFirstTime = roomInfo.firstTimeMessage ?? 0;
          roomId = roomInfo.roomID;
        }

        if (!msgOnly) {
          if (this.delayGetRoomList) {
            clearTimeout(this.delayGetRoomList);
          }
          this.delayGetRoomList = setTimeout(() => {
            ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ROOM_LIST, '');
          }, 300);
        }

        if (roomKey && roomKey == lastRoomKey && roomId >= 0) {
          if (this.delayGetTalkList) {
            clearTimeout(this.delayGetTalkList);
          }
          this.delayGetTalkList = setTimeout(() => {
            let info = {
              clientKey: '',
              firstTimeStamp: talkApi.currentFirstRoomTime,
              roomID: roomId,
              roomKey: roomKey,
              talkCount: 0,
              queryRoomDetailOnly: queryRoomDetailOnly
            };

            ipcRenderer.send(
              constantsApp.REQUEST_QUERY_MESSAGE_LIST_ROOM_DETAIL,
              info
            );
          }, 100);
        }
        return;
      }

      if (action == constantsApp.ACTION_OPEN_ROOM_SELECTED) {
        const category = args.category;
        const primaryKey = args.primaryKey;
        const room = args.room;
        const users = args.users;
        const forceChangeChatTab = args.forceChangeChatTab;
        const roomKey = room ? room.rRoomKey : null;

        if (forceChangeChatTab) {
          const defActiveTab = '/chat';
          this.onActiveTab(defActiveTab);
        }

        remote.getGlobal('ShareKeepValue').CURRENT_ROOM_KEY = roomKey;
        if (roomKey != this.findCurrentRoomKey()) {
          actions.openOneRoom(
            category,
            primaryKey,
            room,
            users,
            args.newTab,
            this.props.commonSettings
          );
        } else {
          if (args.newTab) {
            actions.updateTab(room);
          }
        }
        return;
      }

      if (action == constantsApp.ACTION_SHOW_MSG_INFO_TO_USER) {
        const msg = args;
        this.showAlert(msg);
        return;
      }

      if (action == constantsApp.ACTION_STOP_ALL_REQUEST) {
        downloadUploadManager.stopAllTask();

        let roomKey = this.findCurrentRoomKey();
        let roomInfo = this.props.room_info;
        let lastRoomKey = null;
        let lastFirstTime = 0;
        let roomId = -1;
        if (roomInfo) {
          lastRoomKey = roomInfo.roomKey;
          lastFirstTime = roomInfo.firstTimeMessage ?? 0;
          roomId = roomInfo.roomID;
        }

        if (roomKey && roomKey == lastRoomKey && roomId >= 0) {
          let info = {
            clientKey: '',
            firstTimeStamp: talkApi.currentFirstRoomTime,
            roomID: roomId,
            roomKey: roomKey,
            talkCount: 0,
            queryRoomDetailOnly: false
          };
          ipcRenderer.send(
            constantsApp.REQUEST_QUERY_MESSAGE_LIST_ROOM_DETAIL,
            info
          );
        }
        return;
      }

      if (action == constantsApp.ACTION_SHOW_NOTIFICATION_WITH_DATA) {
        if (args.apiID == 'MEMOREVDATA') {
          this.showWhisperNotification(args);
          let visible = this.isCurrentWindowFocused();
          if (!visible) {
            this.setFlashBadgeIcon();
          }
          return;
        }

        const roomKey = args.roomKey;
        const { room_cached } = this.props;
        const incomeRoom = room_cached ? room_cached[roomKey] : null;

        const allowPush = incomeRoom ? incomeRoom.rRoomPushAlert > 0 : true;
        const curRoomKey = this.findCurrentRoomKey();
        let visible = this.isCurrentWindowFocused();

        const result = ipcRenderer.sendSync(
          constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
          constantsApp.ACTION_SYNC_CHECK_ROOM_AND_WINDOWS_FOCUS_WITH_ROOM_KEY,
          { roomKey: roomKey }
        );

        if (!result && (!visible || (roomKey && roomKey != curRoomKey))) {
          if (allowPush) {
            this.showNotification(roomKey, args);
          }
          if (!visible) {
            this.increaseBadgeNumberString();
            if (allowPush) {
              this.setFlashBadgeIcon();
            }
          }
        }
        return;
      }

      if (action == constantsApp.ACTION_REPLY_OPEN_SELECTED_ROOM_KEY_IF_CAN) {
        const room = args.room;

        if (room) {
          const TAB_CATEGORY = constantsApp.TAB_CATEGORY;
          let category = TAB_CATEGORY.tab_room_list;

          const defActiveTab = '/chat';
          this.onActiveTab(defActiveTab);
          category = this.findCategoryFromActiveTab(defActiveTab);

          remote.getGlobal('ShareKeepValue').CURRENT_ROOM_KEY = room
            ? room.rRoomKey
            : null;
          actions.openOneRoom(
            category,
            room.rRoomKey,
            room,
            null,
            null,
            this.props.commonSettings
          );
          const curWindows = remote.getCurrentWindow();
          curWindows.moveTop();
        }

        return;
      }

      if (action == constantsApp.ACTION_UPDATE_GROUPWARE_COUNT) {
        actions.updateUnreadGroupwareMenu(args);
        return;
      }

      if (action == constantsApp.ACTION_NEW_UPDATE_VERSION_DOWNLOADED) {
        const version = args.version;
        const releaseNotes = args.releaseNotes;
        this.setState({
          showUpdateDialog: true,
          updateContent: {
            version: version,
            releaseNotes: releaseNotes
          }
        });
        return;
      }

      if (action == constantsApp.ACTION_MOVE_TO_LOGIN_EVENT) {
        remote.getGlobal('ShareGlobalObject').attempDisableAutoLogin = true;
        actions.requestNavigateLoginPage(args);
        const curWindows = remote.getCurrentWindow();
        curWindows.show();
        curWindows.moveTop();
        curWindows.focus();

        if (remote.app.dock) {
          remote.app.dock.bounce('critical');
        }
        return;
      }
    });
  }

  showWhisperNotification(data) {
    const { user_cached, actions, logged_user } = this.props;
    const contact = user_cached[data.userKey];
    const displayName = contact?.displayName ?? '';
    const title = '[Whisper] ' + displayName;
    const notification = new Notification(title, {
      body: convertHtmlToPlainText(data.contentMemo) ?? ''
    });

    const { account_info } = logged_user;
    const myUserKey = account_info.user_key;
    const myWhisperKey = userKeyToWhisperKey(myUserKey);
    const otherWhisperKey = userKeyToWhisperKey(data.userKey);
    const strDefault = [otherWhisperKey, myWhisperKey].join(',');
    const strReverse = [myWhisperKey, otherWhisperKey].join(',');
    notification.onclick = () => {
      const event = new CustomEvent('requireReloadList', {
        detail: { u2u: strDefault }
      });

      window.dispatchEvent(event);

      this.onActiveTab('/whisper');
      actions.openWhisper({
        unique_id: strDefault,
        unique_id_reverse: strReverse,
        u2u: strDefault,
        u2u_reverse: strReverse,
        userKey: data.userKey,
        name1: displayName,
        name: displayName
      });
    };
    setTimeout(() => {
      notification.close();
    }, 5000);
  }

  showNotification(roomKey, data) {
    const { t, user_cached, room_cached } = this.props;
    if (this.delayShowNotification) {
      clearTimeout(this.delayShowNotification);
    }
    this.delayShowNotification = setTimeout(() => {
      let title = '-';
      let body;
      let show = false;
      if (data.apiID == 'MSG' || data.apiID == 'CHATROOMBOARDALARM') {
        const message = data.message;
        if (message) {
          const user = user_cached[message.userKey];
          body =
            message.msgBody == null || message.msgBody == ''
              ? t('EMOTICON')
              : message.msgBody;
          if (user) {
            title = user.displayName;
          } else {
            const room = room_cached[roomKey];
            const roomDetails = room ? room.roomDetailds : null;
            if (roomDetails) {
              const foundUser = roomDetails.find(element => {
                return element.rdtUKey == message.userKey;
              });
              title = foundUser ? foundUser.rdtUName : '-';
            }
          }

          if (data.apiID == 'CHATROOMBOARDALARM') {
            title = `${title} [${t('Notice Board')}]`;
          }

          show = true;
        }
      } else if (
        data.apiID == 'HTTPFILEREQ' ||
        data.apiID == 'MFILECLOUD' ||
        data.apiID == 'MFILEREQ'
      ) {
        const user = user_cached[data.userKey];
        body = data.name;
        if (user) {
          title = user.displayName;
        } else {
          const room = room_cached[roomKey];
          const roomDetails = room ? room.roomDetailds : null;
          if (roomDetails) {
            const foundUser = roomDetails.find(element => {
              return element.rdtUKey == data.userKey;
            });
            title = foundUser ? foundUser.rdtUName : '-';
          }
        }
        show = true;
      }

      console.log('show', show);
      if (!show) {
        return;
      }

      const notification = new Notification(title, {
        body: body ?? ''
      });
      notification.onclick = () => {
        const curRoomKey = this.findCurrentRoomKey();
        if (curRoomKey != roomKey) {
          ipcRenderer.send(
            constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
            constantsApp.ACTION_OPEN_SELECTED_ROOM_KEY_IF_CAN,
            {
              roomKey: roomKey
            }
          );
        } else {
          const curWindows = remote.getCurrentWindow();
          curWindows.show();
          curWindows.moveTop();
          curWindows.focus();
        }
      };
      setTimeout(() => {
        notification.close();
      }, 5000);
    }, 1000);
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    if (
      prevProps.showRightPannel != this.props.showRightPannel ||
      prevProps.showRightBoardPannel != this.props.showRightBoardPannel
    ) {
      this.setState({
        useCacl: false,
        chatContainerWidth: this.chatContainerRef.current.clientWidth
      });
      setTimeout(() => {
        this.setState({
          useCacl: true
        });
      }, 300);
    }
    if (
      prevProps.allUnreadMessageBadgeIcon !=
      this.props.allUnreadMessageBadgeIcon
    ) {
      return {
        update_badge: true,
        badge_count: this.props.allUnreadMessageBadgeIcon
      };
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (snapshot) {
      if (snapshot.update_badge) {
        this.setBadgeNumberString(snapshot.badge_count);
      }
    }
  }

  showDisconnectSocket(force, show) {
    const { actions } = this.props;
    clearTimeout(this.delayShowDisconnect);
    if (force) {
      actions.showDisconnectSocket(show);
    } else {
      this.delayShowDisconnect = setTimeout(() => {
        actions.showDisconnectSocket(show);
      }, 5000);
    }
  }

  requestQuitAndInstallApp() {
    ipcRenderer.send(
      constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
      constantsApp.ACTION_REQUEST_QUIT_AND_INSTALL_NEW_APP,
      null
    );
  }

  increaseBadgeNumberString() {
    if (process.platform == 'darwin') {
      let badge = remote.app.dock.getBadge();
      let iBadge = 0;
      if (isNaN(badge) || badge == '' || badge == null) {
      } else {
        iBadge = parseInt(badge);
      }

      iBadge++;
      const strBadge = `${iBadge}`;
      remote.app.dock.setBadge(strBadge);
    } else if (process.platform == 'linux') {
      let badge = remote.app.badgeCount;
      badge++;
      remote.app.badgeCount = badge;
      ipcRenderer.sendSync('talk2-update-badge', 'increase', -1);
    } else if (process.platform == 'win32') {
      ipcRenderer.sendSync('talk2-update-badge', 'increase', -1);
    }
  }

  setFlashBadgeIcon() {
    if (process.platform == 'win32') {
      const curWindow = remote.getCurrentWindow();
      if (curWindow.isVisible() == false && !curWindow.isMinimized()) {
        const bound = curWindow.getBounds();
        // curWindow.setBounds({ x: -9999999, y: -9999999, width: 1, height: 1 });
        curWindow.minimize();
        // curWindow.showInactive();
        // curWindow.minimize();
        // curWindow.setBounds(bound);
      }
      curWindow.flashFrame(true);
    }
  }

  setBadgeNumberString(badgeCount) {
    if (process.platform == 'darwin') {
      const badge = badgeCount > 0 ? `${badgeCount}` : '';
      remote.app.dock.setBadge(badge);
    } else if (process.platform == 'linux') {
      const badge = badgeCount > 0 ? badgeCount : 0;
      remote.app.badgeCount = badge;
      ipcRenderer.sendSync('talk2-update-badge', 'set', badge);
    } else if (process.platform == 'win32') {
      const badge = badgeCount > 0 ? badgeCount : 0;
      ipcRenderer.sendSync('talk2-update-badge', 'set', badge);
    }
  }

  componentWillUnmount() {
    const webview = this.webViewRef.current;
    if (webview) {
      webview.removeEventListener('did-finish-load', this.webviewFinishLoad);
      webview.removeEventListener('did-fail-load', this.webviewFinishLoad);
    }
    if (talkApi.allowTranslate()) {
      document.removeEventListener('mouseup', this.onSelectionChanged);
    }

    talkApi.currentFirstRoomTime = 0;
    if (this.delayShowNotification) {
      clearTimeout(this.delayShowNotification);
    }
    if (this.delayGetRoomList) {
      clearTimeout(this.delayGetRoomList);
    }
    if (this.delayGetTalkList) {
      clearTimeout(this.delayGetTalkList);
    }

    clearTimeout(this.delayShowDisconnect);

    window.removeEventListener('online', this.updateOnlineStatus);
    window.removeEventListener('offline', this.updateOnlineStatus);
    ipcRenderer.removeAllListeners(constantsApp.REPLY_FULL_QUERY_ROOM_LIST);
    ipcRenderer.removeAllListeners(constantsApp.REPLY_FULL_QUERY_ORG_TREE);
    ipcRenderer.removeAllListeners(
      constantsApp.REPLY_QUERY_MESSAGE_LIST_ROOM_DETAIL
    );
    ipcRenderer.removeAllListeners(
      constantsApp.REPLY_FULL_QUERY_ORG_TREE_FOR_STATE
    );

    ipcRenderer.removeAllListeners(constantsApp.MAIN_TO_RENDER_EVENT);
    downloadUploadManager.destroy();
  }

  onActiveTab(tab) {
    const { activeMenu, browserTabData } = this.state;
    const { history, actions, commonSettings } = this.props;
    if (activeMenu != tab) {
      history.push(tab);
      this.stopWebViewLoading();
      this.setState({
        activeMenu: tab,
        browserTabData: {
          show: false,
          loading: false
        }
      });
    } else {
      this.stopWebViewLoading();
      if (browserTabData.show) {
        this.setState({
          browserTabData: {
            show: false,
            loading: false
          }
        });
      }
    }
    actions.showSearchResult(commonSettings, false);
  }

  onOpenCreateGroupDialog = item => {
    this.setState({ groupCreateDialogData: { show: true, item: item } });
  };

  onOpenOrgDialog = (
    callbackSelected = () => {},
    listSelectedUser = [],
    maximumOrg = 50,
    notAllowDisabledSelectedUser,
    updateTitle = false
  ) => {
    this.setState({
      showOrgDialog: true,
      listSelectedUser,
      maximumOrg,
      notAllowDisabledSelectedUser: notAllowDisabledSelectedUser ?? false,
      changeOrgDialogTitleUpdate: updateTitle ?? false
    });
    this.onSelectedUserOrg = callbackSelected;
  };

  onCloseOrgDialog = user => {
    if (user) {
      this.onSelectedUserOrg(user);
    }
    this.setState({
      showOrgDialog: false,
      listSelectedUser: [],
      maximumOrg: 50,
      notAllowDisabledSelectedUser: false
    });
    this.onSelectedUserOrg = null;
  };

  onCloseRoomUserChoiceDialog = (selectedList, data) => {
    if (selectedList?.length > 0 && data) {
      ipcRenderer.send(
        constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
        constantsApp.ACTION_QUERY_ROOM_OR_CREATE_ROOM_IF_NEED_AND_SHARE_MESSAGE,
        { targetData: selectedList, shareMsg: data }
      );
    }
    this.setState({
      roomUserDialogData: {
        show: false,
        data: null
      }
    });
  };

  onOpenMenu = (event, children) => {
    this.setState({
      childrenOptionMenu: children,
      mouseX: event.clientX - 2,
      mouseY: event.clientY - 4
    });
  };

  onCloseMenu = () => {
    this.setState({ mouseX: 0, mouseY: 0 });
  };

  onOpenTranslate = (event, message) => {
    this.setState({
      translateData: {
        ...this.state.translateData,
        showTranslateResult: event.currentTarget,
        showTranslate: false,
        translateText: message,
        x: event.clientX,
        y: event.clientY
      }
    });
  };

  onOpenProfile = item => {
    this.setState({
      profileDialogData: {
        show: true,
        item: item
      }
    });
  };

  onOpenRoomUserChoiceDialog = dataItem => {
    this.setState({
      roomUserDialogData: {
        show: true,
        data: dataItem
      }
    });
  };

  onOpenWhisperDialog = users => {
    this.setState({
      whisperDialogData: {
        show: true,
        users: users
      }
    });
  };

  onOpenSearchHistoryDialog = roomKey => {
    this.setState({
      historyDialogData: { show: true, item: roomKey }
    });
  };

  openGroupwareBrowser(action) {
    if (OPEN_NEW_BROWSER_GROUPWARE) {
      if (!this.blockOpenGroupware) {
        const url = talkApi.externalGroupwareURL(action);
        if (url && url != '') {
          this.blockOpenGroupware = true;
          openLinkWithURL(url);
          setTimeout(() => {
            this.blockOpenGroupware = false;
          }, 2000);
        }
      }
    } else {
      if (!this.state.browserTabData.show) {
        this.setState({
          browserTabData: {
            show: true,
            loading: true
          }
        });
      }
      const webview = this.webViewRef.current;
      if (webview) {
        webview
          .executeJavaScript(
            `window.location.href='${talkApi.externalGroupwareURL(action)}'`
          )
          .then(value => {
            console.log(value);
          })
          .catch(error => {
            console.log(error);
          });
      }
    }
  }

  stopWebViewLoading() {
    const webview = this.webViewRef.current;
    if (webview) {
      webview.stop();
    }
  }

  ensureNullIfNotValue(value) {
    return value > 0 ? value : null;
  }

  openCall(action, data) {
    this.openCallOneOneImpl(action, data);
  }

  openCallOneOneImpl(action, data) {
    const myUserKey = this.props.logged_user?.account_info?.user_key;
    if (data.roomKey && data.toKey && data.fromKey && myUserKey) {
      const direction =
        action == constantsApp.ACTION_SIPRES_RESPONSE
          ? constantsApp.CALL_DIRECTION.OUTBOUND
          : constantsApp.CALL_DIRECTION.INBOUND;

      const useVideo = data.video == 1;
      let otherUserKey = data.toKey == myUserKey ? data.fromKey : data.toKey;
      const otherUser = this.props.user_cached[otherUserKey];
      const displayName = otherUser != null ? otherUser.displayName : '';
      ipcRenderer.send(
        constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
        constantsApp.ACTION_OPEN_CALL_ONE_ONE_WINDOWS,
        {
          roomKey: data.roomKey,
          userKey: otherUserKey,
          displayName: displayName,
          audio: useVideo ? 0 : 1,
          direction: direction,
          sipKey: data.sipKey
        }
      );
    }
  }

  render() {
    const {
      classes,
      allUnreadMessage,
      theme,
      org,
      group_cached,
      user_cached,
      logged_user,
      rooms,
      groupwareUnreadCount,
      t,
      showRightPannel,
      showRightBoardPannel
    } = this.props;
    const {
      activeMenu,
      openSnackbar,
      errorSnackbar,
      showSetting,
      showUpdateDialog,
      showOrgDialog,
      notAllowDisabledSelectedUser,
      changeOrgDialogTitleUpdate,
      updateContent,
      mouseX,
      mouseY,
      childrenOptionMenu,
      listSelectedUser,
      maximumOrg,
      profileDialogData,
      roomUserDialogData,
      whisperDialogData,
      historyDialogData,
      groupCreateDialogData,
      chatContainerWidth,
      useCacl,
      browserTabData,
      translateData
    } = this.state;

    const activeRight = this.findCurrentRoomKey();
    const defSizePanel = localStorage.getItem('splitPos') ?? 450;
    const unreadMessage = allUnreadMessage ? allUnreadMessage : null;

    const showRightMenu =
      this.isMessengerTabFocused(activeMenu) &&
      (showRightBoardPannel || showRightPannel) &&
      activeRight;

    const panelWidth = showRightPannel
      ? constantsApp.FILE_LIST_PANNEL_WIDTH
      : constantsApp.BOARD_PANNEL_WIDTH;

    const isBrowserOpen = !OPEN_NEW_BROWSER_GROUPWARE && browserTabData.show;

    return (
      <MenuOptionContext.Provider
        value={{
          onOpen: this.onOpenMenu,
          onClose: this.onCloseMenu,
          onOpenProfile: this.onOpenProfile,
          onOpenWhisperDialog: this.onOpenWhisperDialog,
          onOpenCreateGroupDialog: this.onOpenCreateGroupDialog,
          onOpenOrgDialog: this.onOpenOrgDialog,
          onOpenTranslate: this.onOpenTranslate
        }}
      >
        <OrgContext.Provider
          value={{
            onOpen: this.onOpenOrgDialog,
            onOpenProfile: this.onOpenProfile,
            onOpenRoomUserChoiceDialog: this.onOpenRoomUserChoiceDialog,
            onOpenWhisperDialog: this.onOpenWhisperDialog,
            onOpenSearchHistoryDialog: this.onOpenSearchHistoryDialog
          }}
        >
          <div className={classes.main}>
            <div className={classes.drawer}>
              <List style={{ padding: 0 }}>
                <ListItem
                  button
                  key="/"
                  className={
                    activeMenu == '/' && !isBrowserOpen
                      ? classes.listItemActive
                      : classes.listItem
                  }
                  onClick={() => {
                    this.onActiveTab('/');
                  }}
                >
                  <Tooltip
                    title={t('tooltip_org_tab')}
                    aria-label={t('tooltip_org_tab')}
                  >
                    <ListItemIcon className={classes.listItemIcon}>
                      <ORGCompanyIcon style={{ color: 'white' }} />
                    </ListItemIcon>
                  </Tooltip>
                </ListItem>
                <ListItem
                  onContextMenu={event => {
                    event.preventDefault();
                    event.stopPropagation();
                    const menuData = [
                      <MenuItem
                        key="creategroup"
                        onClick={() => {
                          this.onCloseMenu();
                          this.onOpenCreateGroupDialog(null);
                        }}
                      >
                        <div className={classes.memuItem}>
                          <Typography variant="caption">
                            {t('Create group')}
                          </Typography>
                        </div>
                      </MenuItem>
                    ];
                    this.onOpenMenu(event, menuData);
                  }}
                  button
                  key="/favorite"
                  className={
                    activeMenu == '/favorite' && !isBrowserOpen
                      ? classes.listItemActive
                      : classes.listItem
                  }
                  onClick={() => {
                    this.onActiveTab('/favorite');
                  }}
                >
                  <Tooltip
                    title={t('tooltip_fav_tab')}
                    aria-label={t('tooltip_fav_tab')}
                  >
                    <ListItemIcon className={classes.listItemIcon}>
                      <ORGFavoriteIcon style={{ color: 'white' }} />
                    </ListItemIcon>
                  </Tooltip>
                </ListItem>
                <ListItem
                  button
                  key="/chat"
                  className={
                    activeMenu == '/chat' && !isBrowserOpen
                      ? classes.listItemActive
                      : classes.listItem
                  }
                  onClick={() => {
                    this.onActiveTab('/chat');
                  }}
                >
                  <Tooltip
                    title={t('tooltip_room_tab')}
                    aria-label={t('tooltip_room_tab')}
                  >
                    <ListItemIcon className={classes.listItemIcon}>
                      <Badge badgeContent={unreadMessage} color="error">
                        <RoomListIcon style={{ color: 'white' }} />
                      </Badge>
                    </ListItemIcon>
                  </Tooltip>
                </ListItem>
                <ListItem
                  button
                  key="/whisper"
                  className={
                    activeMenu == '/whisper' && !isBrowserOpen
                      ? classes.listItemActive
                      : classes.listItem
                  }
                  onClick={() => {
                    this.onActiveTab('/whisper');
                  }}
                >
                  <Tooltip
                    title={t('tooltip_whisper_tab')}
                    aria-label={t('tooltip_whisper_tab')}
                  >
                    <ListItemIcon className={classes.listItemIcon}>
                      <Badge
                        badgeContent={this.ensureNullIfNotValue(
                          groupwareUnreadCount.whisper
                        )}
                        color="error"
                      >
                        <WhisperIcon style={{ color: 'white' }} />
                      </Badge>
                    </ListItemIcon>
                  </Tooltip>
                </ListItem>

                <ListItem
                  button
                  style={{ marginTop: 20 }}
                  className={classes.listItem}
                  onClick={() => {
                    this.openGroupwareBrowser('mail');
                  }}
                >
                  <Tooltip
                    title={t('tooltip_mail_tab')}
                    aria-label={t('tooltip_mail_tab')}
                  >
                    <ListItemIcon className={classes.listItemIcon}>
                      <Badge
                        badgeContent={this.ensureNullIfNotValue(
                          groupwareUnreadCount.mail
                        )}
                        color="error"
                      >
                        <MailTabIcon style={{ color: 'white' }} />
                      </Badge>
                    </ListItemIcon>
                  </Tooltip>
                </ListItem>

                <ListItem
                  button
                  className={classes.listItem}
                  onClick={() => {
                    this.openGroupwareBrowser('board');
                  }}
                >
                  <Tooltip
                    title={t('tooltip_board_tab')}
                    aria-label={t('tooltip_board_tab')}
                  >
                    <ListItemIcon className={classes.listItemIcon}>
                      <Badge
                        badgeContent={this.ensureNullIfNotValue(
                          groupwareUnreadCount.board
                        )}
                        color="error"
                      >
                        <BoardTabIcon style={{ color: 'white' }} />
                      </Badge>
                    </ListItemIcon>
                  </Tooltip>
                </ListItem>

                <ListItem
                  button
                  className={classes.listItem}
                  onClick={() => {
                    this.openGroupwareBrowser('approval');
                  }}
                >
                  <Tooltip
                    title={t('tooltip_approval_tab')}
                    aria-label={t('tooltip_approval_tab')}
                  >
                    <ListItemIcon className={classes.listItemIcon}>
                      <Badge
                        badgeContent={this.ensureNullIfNotValue(
                          groupwareUnreadCount.approval
                        )}
                        color="error"
                      >
                        <ApprovalTabIcon style={{ color: 'white' }} />
                      </Badge>
                    </ListItemIcon>
                  </Tooltip>
                </ListItem>

                <ListItem
                  button
                  className={classes.listItem}
                  onClick={() => {
                    this.openGroupwareBrowser('circular');
                  }}
                >
                  <Tooltip
                    title={t('tooltip_circular_tab')}
                    aria-label={t('tooltip_circular_tab')}
                  >
                    <ListItemIcon className={classes.listItemIcon}>
                      <Badge
                        badgeContent={this.ensureNullIfNotValue(
                          groupwareUnreadCount.circular
                        )}
                        color="error"
                      >
                        <CircularTabIcon style={{ color: 'white' }} />
                      </Badge>
                    </ListItemIcon>
                  </Tooltip>
                </ListItem>

                <ListItem
                  button
                  className={classes.listItem}
                  onClick={() => {
                    this.openGroupwareBrowser('todo');
                  }}
                >
                  <Tooltip
                    title={t('tooltip_todo_tab')}
                    aria-label={t('tooltip_todo_tab')}
                  >
                    <ListItemIcon className={classes.listItemIcon}>
                      <Badge
                        badgeContent={this.ensureNullIfNotValue(
                          groupwareUnreadCount.todo
                        )}
                        color="error"
                      >
                        <TodoTabIcon style={{ color: 'white' }} />
                      </Badge>
                    </ListItemIcon>
                  </Tooltip>
                </ListItem>

                <ListItem
                  button
                  className={classes.listItem}
                  onClick={() => {
                    this.openGroupwareBrowser('task');
                  }}
                >
                  <Tooltip
                    title={t('tooltip_task_tab')}
                    aria-label={t('tooltip_task_tab')}
                  >
                    <ListItemIcon className={classes.listItemIcon}>
                      <Badge
                        badgeContent={this.ensureNullIfNotValue(
                          groupwareUnreadCount.task
                        )}
                        color="error"
                      >
                        <TaskTabIcon style={{ color: 'white' }} />
                      </Badge>
                    </ListItemIcon>
                  </Tooltip>
                </ListItem>
              </List>

              <div>
                <ListItem
                  button
                  className={classes.listItem}
                  onClick={() => this.onOpenSearchHistoryDialog(null)}
                >
                  <Tooltip
                    title={t('tooltip_search_history_tab')}
                    aria-label={t('tooltip_search_history_tab')}
                  >
                    <ListItemIcon className={classes.listItemIcon}>
                      <HistoryTabIcon style={{ color: 'white' }} />
                    </ListItemIcon>
                  </Tooltip>
                </ListItem>

                <ListItem
                  button
                  className={classes.listItem}
                  onClick={() => {
                    this.openGroupwareBrowser('main');
                  }}
                >
                  <Tooltip
                    title={t('tooltip_groupware_tab')}
                    aria-label={t('tooltip_groupware_tab')}
                  >
                    <ListItemIcon className={classes.listItemIcon}>
                      <GroupwareIcon style={{ color: 'white' }} />
                    </ListItemIcon>
                  </Tooltip>
                </ListItem>
                <ListItem
                  button
                  className={classes.listItem}
                  onClick={() => this.setState({ showSetting: !showSetting })}
                >
                  <ListItemIcon className={classes.listItemIcon}>
                    <GroupwareSettingIcon style={{ color: 'white' }} />
                  </ListItemIcon>
                </ListItem>
              </div>
            </div>
            <div
              ref={this.chatContainerRef}
              className={classes.mainLayoutRight}
              style={{
                width: showRightMenu
                  ? useCacl
                    ? `calc(100% - ${panelWidth + 50}px)`
                    : chatContainerWidth
                  : useCacl
                  ? 'calc(100% - 50px)'
                  : chatContainerWidth,
                minWidth: showRightMenu
                  ? useCacl
                    ? `calc(100% - ${panelWidth + 50}px)`
                    : chatContainerWidth
                  : useCacl
                  ? 'calc(100% - 50px)'
                  : chatContainerWidth
              }}
            >
              <SplitPane
                minSize={250}
                maxSize={450}
                defaultSize={parseInt(defSizePanel)}
                onChange={size => {
                  localStorage.setItem('splitPos', size);
                  const event = new Event('splitPannelChanged', { size: size });
                  window.dispatchEvent(event);
                }}
                style={{ position: 'relative' }}
                resizerStyle={{
                  backgroundColor: theme.palette.divider,
                  opacity: 1
                }}
              >
                <div className={classes.contentSplitView}>
                  <Box
                    minHeight={70}
                    width="100%"
                    borderBottom={1}
                    borderTop={1}
                    className={classes.contentTop}
                  >
                    <Switch>
                      {routes.map((route, index) => (
                        <Route
                          key={index}
                          path={route.path}
                          exact={route.exact}
                          children={<route.topLeft />}
                        />
                      ))}
                    </Switch>
                  </Box>
                  <Box height="100%" width="100%">
                    <Switch>
                      {routes.map((route, index) => (
                        <Route
                          key={index}
                          path={route.path}
                          exact={route.exact}
                          children={<route.bottomLeft />}
                        />
                      ))}
                    </Switch>
                  </Box>
                </div>
                {activeRight ? (
                  <div className={classes.contentSplitViewHor}>
                    <div
                      className={classes.contentSplitViewAll}
                      style={{
                        width: '100%'
                      }}
                    >
                      <Box
                        minHeight={70}
                        width="100%"
                        borderTop={1}
                        borderBottom={0}
                        className={classes.contentTop}
                      >
                        <Switch>
                          {routes.map((route, index) => (
                            <Route
                              key={route.commonKey}
                              path={route.path}
                              exact={route.exact}
                              children={<route.topRight />}
                            />
                          ))}
                        </Switch>
                      </Box>
                      <Box height="calc(100% - 70px)" width="100%">
                        <Switch>
                          {routes.map((route, index) => (
                            <Route
                              key={route.commonKey}
                              path={route.path}
                              exact={route.exact}
                              children={<route.bottomRight />}
                            />
                          ))}
                        </Switch>
                      </Box>
                    </div>
                  </div>
                ) : (
                  <Box
                    borderTop={1}
                    borderBottom={1}
                    height="100%"
                    width="100%"
                    className={classes.contentTop}
                  >
                    <div
                      style={{
                        userSelect: 'none',
                        flexDirection: 'column',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: '100%',
                        height: '100%'
                      }}
                    >
                      <img
                        draggable="false"
                        className={classes.logo}
                        src={
                          theme.palette.type == 'light'
                            ? './images/talk_icon_v2.svg'
                            : './images/talk_icon_dark_v2.svg'
                        }
                        style={{ width: 200, marginBottom: 50 }}
                      />

                      <Typography
                        variant="subtitle2"
                        style={{ color: '#b4b4b4' }}
                      >
                        {t(
                          '(Double-)Click a contact to start the conversation.'
                        )}
                      </Typography>
                    </div>
                  </Box>
                )}
              </SplitPane>
            </div>
            {showRightMenu && showRightPannel && (
              <div style={{ width: constantsApp.FILE_LIST_PANNEL_WIDTH }}>
                <Switch>
                  {routes.map((route, index) => (
                    <Route
                      key={index}
                      path={route.path}
                      exact={route.exact}
                      children={<route.rightPannel />}
                    />
                  ))}
                </Switch>
              </div>
            )}
            {showRightMenu && showRightBoardPannel && (
              <div style={{ width: constantsApp.BOARD_PANNEL_WIDTH }}>
                <Switch>
                  {routes.map((route, index) => (
                    <Route
                      key={index}
                      path={route.path}
                      exact={route.exact}
                      children={<route.boardRightPannel />}
                    />
                  ))}
                </Switch>
              </div>
            )}

            {!OPEN_NEW_BROWSER_GROUPWARE && (
              <div
                className={classes.browserLayoutRight}
                style={{ display: browserTabData.show ? 'block' : 'none' }}
              >
                <webview
                  id="web_view_hanbiro"
                  ref={this.webViewRef}
                  style={{ width: '100%', height: '100%' }}
                  src="about:blank"
                ></webview>
                {browserTabData.loading && (
                  <div
                    className="han-loading mini"
                    style={{ position: 'absolute', top: '50%', left: '50%' }}
                  />
                )}
              </div>
            )}

            <Menu
              keepMounted
              open={mouseX && mouseY ? true : false}
              onClose={this.onCloseMenu}
              anchorReference="anchorPosition"
              anchorPosition={{ top: mouseY ?? 0, left: mouseX ?? 0 }}
            >
              {childrenOptionMenu}
            </Menu>

            {groupCreateDialogData.show && (
              <CreateGroupDialog
                show={true}
                item={groupCreateDialogData.item}
                onUpdate={(item, title, extraInfo) => {
                  this.setState({
                    groupCreateDialogData: { show: false, item: null }
                  });
                  let folderKey = item ? item.groupKey : '';
                  ipcRenderer.send(
                    constantsApp.SEND_SOCKET_API_EVENT,
                    constantsApp.API_FOLDER_CREATE,
                    { folderName: title, folderKey: folderKey }
                  );
                }}
                onClose={() =>
                  this.setState({
                    groupCreateDialogData: { show: false, item: null }
                  })
                }
              />
            )}

            {historyDialogData.show && (
              <HistoryDialog
                show={true}
                roomKey={historyDialogData.item}
                onClose={() =>
                  this.setState({
                    historyDialogData: { show: false, item: null }
                  })
                }
              />
            )}

            {showSetting && (
              <Setting
                show={true}
                onClose={() => this.setState({ showSetting: false })}
              />
            )}

            {whisperDialogData.show && (
              <WhisperDialog
                show={true}
                contacts={whisperDialogData.users}
                onSelectUser={this.onOpenOrgDialog}
                onClose={() =>
                  this.setState({
                    whisperDialogData: {
                      show: false,
                      users: null
                    }
                  })
                }
              />
            )}

            {profileDialogData.show && (
              <ProfileDialog
                show={true}
                item={profileDialogData.item}
                onClose={() =>
                  this.setState({
                    profileDialogData: {
                      show: false,
                      item: null
                    }
                  })
                }
              />
            )}
            {showUpdateDialog && (
              <UpdateDialog
                show={true}
                releaseInfo={updateContent}
                onUpdate={this.requestQuitAndInstallApp}
                onClose={() => this.setState({ showUpdateDialog: false })}
              />
            )}
            {roomUserDialogData.show && (
              <RoomUserChoiceDialog
                rooms={rooms}
                org={org}
                groupCached={group_cached}
                user_cached={user_cached}
                user={logged_user?.account_info}
                loggedUser={logged_user}
                dataObj={roomUserDialogData.data}
                open={true}
                maximum={1}
                onSelect={this.onCloseRoomUserChoiceDialog}
                onClose={this.onCloseRoomUserChoiceDialog}
              />
            )}
            {showOrgDialog && (
              <OrgDialog
                org={org}
                groupCached={group_cached}
                loggedUser={logged_user}
                listSelectedUser={listSelectedUser}
                open={true}
                changeOrgDialogTitleUpdate={changeOrgDialogTitleUpdate}
                notAllowDisableSelected={notAllowDisabledSelectedUser}
                maximum={maximumOrg}
                onSelect={this.onCloseOrgDialog}
                onClose={this.onCloseOrgDialog}
              />
            )}
            {translateData.showTranslate && (
              <div
                className={classes.btnTranslate}
                style={{
                  left: translateData.x,
                  top: translateData.y
                }}
                onClick={event => {
                  this.setState(
                    {
                      translateData: {
                        ...translateData,
                        showTranslateResult: event.currentTarget,
                        showTranslate: false
                      }
                    },
                    () => {
                      window.getSelection()?.empty();
                    }
                  );
                }}
              >
                <TranslateIcon />
              </div>
            )}

            {translateData.showTranslateResult && (
              <Popover
                action={this.popover}
                anchorPosition={{
                  left: translateData.x,
                  top: translateData.y
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'left'
                }}
                anchorReference="anchorPosition"
                anchorEl={translateData.showTranslateResult}
                onClose={() => {
                  this.setState({
                    translateData: {
                      ...translateData,
                      showTranslateResult: null
                    }
                  });
                }}
                open={Boolean(translateData.showTranslateResult)}
              >
                <Translate
                  text={translateData.translateText}
                  updatePosition={() =>
                    this.popover.current?.updatePosition?.()
                  }
                />
              </Popover>
            )}

            <Snackbar
              autoHideDuration={2000}
              anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
              open={openSnackbar}
              onClose={() => this.handleClosed()}
              message={errorSnackbar}
            />
          </div>
        </OrgContext.Provider>
      </MenuOptionContext.Provider>
    );
  }
}

export default withTranslation()(withRouter(withTheme(withStyles(Home))));
