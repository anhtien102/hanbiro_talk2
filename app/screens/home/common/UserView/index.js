import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { ipcRenderer } from 'electron';
import { Box } from '@material-ui/core';
import * as constantsApp from '../../../../configs/constant';
import { useStyles } from './styles';
import UserInfor from '../../../../components/UserInfor';
import AlertLogout from '../../../../components/AlertLogout';

import {
  SEND_SOCKET_API_EVENT,
  API_USER_CHANGE_NICKNAME,
  API_USER_CHANGE_STATUS
} from '../../../../configs/constant';
import * as Utils from '../../../../utils';

export default function UserView() {
  const company = useSelector(state => state.company);
  const auth = useSelector(state => state.auth);
  const classes = useStyles();
  const [showAlertLogout, setShowAlertLogout] = useState(false);

  const doLogout = clear => {
    if (clear) {
      Utils.saveDataToStorage(constantsApp.TICKER_RECENTLY, null);
      Utils.tickerEmojManager.resetTickerList();
    }
    ipcRenderer.send(constantsApp.SEND_LOGOUT_EVENT, { clearAndLogout: clear });
  };

  const { user_cached } = company;
  const { user_key } = auth.user.account_info;
  const { login } = auth.user.extra_login_info;
  const data = user_cached[user_key];
  if (data) {
    return (
      <Box width="100%" height="100%" className={classes.main}>
        <UserInfor
          key={data.primaryKey}
          loggedKey={user_key}
          data={data}
          extraLogin={login}
          onPressLogout={() => setShowAlertLogout(true)}
          onChangeStatus={status => {
            ipcRenderer.send(SEND_SOCKET_API_EVENT, API_USER_CHANGE_STATUS, {
              status: status
            });
          }}
          onChangeUserBubble={text => {
            ipcRenderer.send(SEND_SOCKET_API_EVENT, API_USER_CHANGE_NICKNAME, {
              name: text ?? ''
            });
          }}
        />
        {showAlertLogout && (
          <AlertLogout
            show={true}
            onClose={() => setShowAlertLogout(false)}
            onLogout={doLogout}
          />
        )}
      </Box>
    );
  }

  return (
    <Box width="100%" height="100%" className={classes.main}>
      <UserInfor
        className={classes.inputSearch}
        loggedKey={user_key}
        extraLogin={login}
        onPressLogout={() => {
          setShowAlertLogout(true);
        }}
      />
      {showAlertLogout && (
        <AlertLogout
          show={true}
          onClose={() => setShowAlertLogout(false)}
          onLogout={doLogout}
        />
      )}
    </Box>
  );
}
