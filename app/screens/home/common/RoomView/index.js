import React, {
  useState,
  useContext,
  Component,
  useRef,
  useEffect
} from 'react';
import {
  IconButton,
  Typography,
  Popover,
  ListItem,
  ListItemText,
  ListItemIcon,
  Snackbar
} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';

import { FILE_LIST_PANNEL_WIDTH } from '../../../../configs/constant';
import * as Actions from '../../../../actions';
import UserPhotoView from '../../../../components/UserPhoto';
import { useStyles } from './styles';
import UserGroupPhoto from '../../../../components/UserGroupPhoto';
import { ipcRenderer } from 'electron';
import { contactUtils, groupUtils } from '../../../../core/model/OrgUtils';
import {
  StatusMode,
  ROOM_DETAIL_STATUS_ENTER
} from '../../../../core/service/talk-constants';
import TalkIcon from '../../../../components/TalkIcon';
import {
  UserStatusMobileIcon,
  UserStatusPCIcon,
  HistoryIcon,
  AddUserInviteIcon,
  FileListIcon,
  NotificationsOnIcon,
  NotificationsOffIcon,
  AudioOneOneIcon,
  VideoOneOneIcon
} from '../../../../components/HanSVGIcon';

import searchWorker from '../../../../core/worker/search';

import {
  TAB_CATEGORY,
  REQUEST_COMMON_ACTION_FROM_RENDER,
  ACTION_CREATE_OR_INVITE_USER_TO_ROOM,
  SEND_SOCKET_API_EVENT,
  COMMON_SYNC_ACTION_FROM_RENDER,
  ACTION_SYNC_RESIZE_WINDOWS_IF_NEED,
  API_CHANGE_ALERT_ROOM,
  API_SIP_MAKE_CALL
} from '../../../../configs/constant';
import talkAPI from '../../../../core/service/talk.api.render';

import 'simplebar';
import { OrgContext } from '../../../home';
import { useTranslation } from 'react-i18next';

import {
  openLinkWithURL,
  remoteAskMediaAccess
} from '../../../../utils/electron.utils';
import { getMediaCaptureAvailable } from '../../../../janus_client/src/app/janus.utils';

export default function RoomView(props) {
  const { onOpen, onOpenProfile, onOpenSearchHistoryDialog } = useContext(
    OrgContext
  );
  const classes = useStyles();
  const showRightPannel = useSelector(state => state.appUI.showRightPannel);
  const data = useSelector(state => state.message_list.room);
  const user = useSelector(state => state.auth.user.account_info);
  const loggedUser = useSelector(state => state.auth.user);
  const userCached = useSelector(state => state.company.user_cached);
  const groupCached = useSelector(state => state.company.group_cached);
  const org_time_card = useSelector(state => state.company.org_time_card);

  const listAllUserInRoom = useSelector(
    state => state.message_list.listUserActiveInRoom
  );

  const dispatch = useDispatch();

  const popover = useRef(null);

  const { t } = useTranslation();

  const [showUserList, setShowUserList] = useState(null);

  const [openSnackbar, setOpenSnackbar] = useState({
    open: false,
    message: ''
  });
  /**
   * callback users selected invite
   * @param {*} users
   * @returns
   */
  const onPressInvite = () => {
    const maximumContacts = parseInt(
      loggedUser?.extra_server_info?.maxContactInRoom ?? 20
    );
    onOpen(
      users => {
        let newUsers = [];
        for (let item of users) {
          const find = listAllUserInRoom.find(obj => {
            return obj.userKey == item.userKey;
          });
          if (!find) {
            newUsers.push(item);
          }
        }

        ipcRenderer.send(
          REQUEST_COMMON_ACTION_FROM_RENDER,
          ACTION_CREATE_OR_INVITE_USER_TO_ROOM,
          {
            category: TAB_CATEGORY.tab_room_list,
            users: newUsers,
            listAllUserInRoom: listAllUserInRoom,
            roomKey: data.rRoomKey
          }
        );
      },
      listAllUserInRoom,
      maximumContacts
    );
  };

  /**
   * call when Press notification action
   */
  const onPressNotification = () => {
    ipcRenderer.send(SEND_SOCKET_API_EVENT, API_CHANGE_ALERT_ROOM, {
      roomKey: data.rRoomKey,
      enable: !(data.rRoomPushAlert > 0)
    });
  };

  /**
   * call when Press call action
   */
  const onPressCall = isVideo => {
    if (process.platform == 'darwin') {
      remoteAskMediaAccess('microphone')
        .then(successMic => {
          if (successMic) {
            if (isVideo) {
              remoteAskMediaAccess('camera')
                .then(successCam => {
                  if (successCam) {
                    openCall({ useVideo: isVideo });
                  } else {
                    openLinkWithURL(
                      `x-apple.systempreferences:com.apple.preference.security?Privacy_Camera`
                    );
                  }
                })
                .catch(err => {
                  showInfoToUser(t('Unnable to access camera'));
                });
            } else {
              openCall({ useVideo: isVideo });
            }
          } else {
            openLinkWithURL(
              `x-apple.systempreferences:com.apple.preference.security?Privacy_Microphone`
            );
          }
        })
        .catch(error => {
          console.log(error);
          showInfoToUser(t('Unnable to access microphone'));
        });
    } else {
      openCall({ useVideo: isVideo });
    }
  };

  const showInfoToUser = message => {
    setOpenSnackbar({ open: true, message: message });
  };

  const openCall = ({ useVideo = true }) => {
    getMediaCaptureAvailable(result => {
      if (
        (useVideo && result.audioInput && result.videoInput) ||
        (!useVideo && result.audioInput)
      ) {
        let otherUser = null;
        const list = listAllUserInRoom.filter(
          item => item.userKey != user.user_key
        );
        otherUser = list.length > 0 ? list[0] : null;
        if (data.rRoomKey && otherUser) {
          ipcRenderer.send(SEND_SOCKET_API_EVENT, API_SIP_MAKE_CALL, {
            fromKey: user.user_key,
            toKey: otherUser.userKey,
            clientKey: Date.now(),
            isVideo: useVideo
          });
        }
      } else {
        showInfoToUser(t('No capture device available'));
      }
    });
  };

  /**
   * call when Press notification action
   */
  const onPressSearchMessage = () => {
    onOpenSearchHistoryDialog(data.rRoomKey);
  };

  /**
   * call when Press more action
   */
  const onPressMore = () => {
    const nextOpen = !showRightPannel;
    ipcRenderer.sendSync(
      COMMON_SYNC_ACTION_FROM_RENDER,
      ACTION_SYNC_RESIZE_WINDOWS_IF_NEED,
      nextOpen ? FILE_LIST_PANNEL_WIDTH : -FILE_LIST_PANNEL_WIDTH
    );
    dispatch(Actions.showRightPannelPage(nextOpen));
  };

  /**
   * call when Press more action
   */
  const onPressGroupImage = event => {
    setShowUserList(event.currentTarget);
  };

  const onPressSingleImage = event => {
    let otherUser = null;
    const list = listAllUserInRoom.filter(
      item => item.userKey != user.user_key
    );

    otherUser = list.length > 0 ? list[0] : null;
    if (otherUser) {
      onOpenProfile(otherUser);
    } else {
      const myData = userCached[user.user_key];
      if (myData) {
        onOpenProfile(myData);
      } else {
        const loginData = loggedUser?.extra_login_info?.login;
        if (loginData) {
          loginData.userKey = user.user_key;
          onOpenProfile(loginData);
        }
      }
    }
  };

  useEffect(() => {
    const div = popover.current;
    if (div) {
      const pager = div.getElementsByClassName('MuiPopover-paper')[0];
      if (pager) {
        pager.style.overflow = 'hidden';
      }
    }
  }, [popover.current]);

  /**
   * return UI status icon
   * @param {*} status
   * @returns
   */
  const exportIconStatus = status => {
    switch (status) {
      case StatusMode.logout:
      case StatusMode.offline:
        return (
          <div className={classes.contentStatusSmall}>
            <div className={classes.circleOffline} />
          </div>
        );
      case StatusMode.available:
        return (
          <div className={classes.contentStatusSmall}>
            <div className={classes.circleOnline} />
          </div>
        );
      case StatusMode.away:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="idle" />
          </div>
        );
      case StatusMode.busy:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="busy" />
          </div>
        );
      case StatusMode.meeting:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="meeting" />
          </div>
        );
      case StatusMode.meal:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="meal" />
          </div>
        );
      case StatusMode.phone:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="call" />
          </div>
        );
      case StatusMode.out:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="out" />
          </div>
        );
      case StatusMode.business_trip:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="business_trip" />
          </div>
        );

      default:
        return null;
    }
  };

  if (data) {
    let otherUser;
    let userStatus;
    const showStatus = listAllUserInRoom && listAllUserInRoom.length < 3;
    if (showStatus) {
      const list = listAllUserInRoom.filter(
        item => item.userKey != user.user_key
      );
      otherUser = list.length > 0 ? list[0] : null;
      userStatus = exportIconStatus(otherUser?.userStatus);
    }
    const allowCallOneOne = talkAPI.allowCallOneOne();
    const allowRoomInvite = talkAPI.allowRoomInvite();
    const supportRoomPushAlertApi = talkAPI.apiSupportList.hasSupportRoomPushAlertApi();
    const alertActive = data.rRoomPushAlert > 0;

    const onPress =
      data.roomDetailds?.length >= 3 ? onPressGroupImage : onPressSingleImage;

    return (
      <div className={classes.main}>
        <div className={classes.contentUser}>
          <div style={{ width: 50, height: 50 }}>
            <UserGroupPhoto
              userList={data.roomDetailds}
              user={user}
              userStatus={userStatus}
              onPress={onPress}
            />
          </div>
          {onPress && (
            <Popover
              ref={popover}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right'
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left'
              }}
              anchorEl={showUserList}
              onClick={() => setShowUserList(null)}
              open={Boolean(showUserList)}
            >
              <ContentPerfectScroll
                classes={classes}
                userCached={userCached}
                data={data}
                groupCached={groupCached}
                org_time_card={org_time_card}
              />
            </Popover>
          )}
          <div className={classes.nameContent}>
            <Typography variant="subtitle2" className={classes.textUser}>
              {data.displayName}
            </Typography>
            {otherUser && (
              <Typography
                variant="caption"
                color="textSecondary"
                className={classes.textStatus}
              >
                {otherUser.userNickName ?? ''}
              </Typography>
            )}
          </div>
        </div>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          {allowCallOneOne && listAllUserInRoom?.length == 2 && (
            <>
              <IconButton
                className={classes.audioCall}
                onClick={() => {
                  onPressCall(false);
                }}
              >
                <AudioOneOneIcon />
              </IconButton>
              <IconButton
                className={classes.videoCall}
                onClick={() => {
                  onPressCall(true);
                }}
              >
                <VideoOneOneIcon />
              </IconButton>
            </>
          )}

          {allowRoomInvite ? (
            <IconButton onClick={onPressInvite}>
              <AddUserInviteIcon />
            </IconButton>
          ) : null}
          {supportRoomPushAlertApi ? (
            <IconButton onClick={onPressNotification}>
              {alertActive ? <NotificationsOnIcon /> : <NotificationsOffIcon />}
            </IconButton>
          ) : null}
          <IconButton onClick={onPressSearchMessage}>
            <HistoryIcon />
          </IconButton>
          <IconButton onClick={onPressMore}>
            <FileListIcon />
          </IconButton>
        </div>
        <Snackbar
          autoHideDuration={2000}
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={openSnackbar.open}
          onClose={() => setOpenSnackbar({ open: false, message: '' })}
          message={openSnackbar.message}
        />
      </div>
    );
  }

  return null;
}

class ContentPerfectScroll extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullLocations: null
    };
  }

  componentDidMount() {
    const { data, userCached, groupCached } = this.props;
    const param = {
      users: data.roomDetailds,
      userCached: userCached,
      group_cached: groupCached
    };

    searchWorker.getExtraUserInfoFromList(param).then(result => {
      this.setState({
        fullLocations: result
      });
    });
  }

  render() {
    const { classes, data, userCached, org_time_card } = this.props;
    const { fullLocations } = this.state;

    return (
      <div
        data-simplebar
        style={{
          width: 320,
          position: 'relative',
          maxHeight: 350
        }}
      >
        <div
          style={{
            width: '100%'
          }}
        >
          {data.roomDetailds.map(element => {
            if (element.rdtStatus == ROOM_DETAIL_STATUS_ENTER) {
              if (userCached) {
                const myData = userCached[element?.rdtUKey];
                if (myData) {
                  myData.fullLocation = fullLocations
                    ? fullLocations[myData.userKey]
                    : null;

                  return (
                    <UserSearch
                      key={myData.userKey}
                      classes={classes}
                      data={myData}
                      org_time_card={org_time_card}
                    />
                  );
                }
              }
            }
          })}
        </div>
      </div>
    );
  }
}

class UserSearch extends Component {
  constructor(props) {
    super(props);
  }

  exportIconStatus = status => {
    const { classes } = this.props;
    switch (status) {
      case StatusMode.logout:
      case StatusMode.offline:
        return <div className={classes.circleOffline} />;
      case StatusMode.available:
        return <div className={classes.circleOnline} />;
      case StatusMode.away:
        return <TalkIcon name="idle" />;
      case StatusMode.busy:
        return <TalkIcon name="busy" />;
      case StatusMode.meeting:
        return <TalkIcon name="meeting" />;
      case StatusMode.meal:
        return <TalkIcon name="meal" />;
      case StatusMode.phone:
        return <TalkIcon name="call" />;
      case StatusMode.out:
        return <TalkIcon name="out" />;
      case StatusMode.business_trip:
        return <TalkIcon name="business_trip" />;

      default:
        return null;
    }
  };

  render() {
    const { data, org_time_card, classes } = this.props;
    const isDualLogin = contactUtils.isDualLogin(data);
    const isPCLogin = contactUtils.isPCLogin(data);
    const isMobileLogin = contactUtils.isMobileLogin(data);
    const iconStatus = this.exportIconStatus(data.userStatus);
    let renderDeviceList;
    let renderHoliday;
    let renderBirthDay;

    if (org_time_card && org_time_card[data.userKey]) {
      renderHoliday = org_time_card[data.userKey].holiday == 1;
      renderBirthDay = org_time_card[data.userKey].birthDay == 1;
    }

    if (isDualLogin) {
      renderDeviceList = (
        <div className={classes.contentDevice}>
          <div className={classes.contentBadge}>
            <div className={classes.badgeDevice}>
              <UserStatusPCIcon style={{ fontSize: 12 }} />
            </div>
          </div>
          <div style={{ position: 'absolute', left: 12 }}>
            <div className={classes.contentBadge}>
              <div className={classes.badgeDeviceGreen}>
                <UserStatusMobileIcon style={{ fontSize: 12 }} />
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      if (isPCLogin) {
        renderDeviceList = (
          <div className={classes.contentDevice}>
            <div className={classes.contentBadge}>
              <div className={classes.badgeDevice}>
                <UserStatusPCIcon style={{ fontSize: 12 }} />
              </div>
            </div>
          </div>
        );
      }

      if (isMobileLogin) {
        renderDeviceList = (
          <div className={classes.contentDevice}>
            <div className={classes.contentBadge}>
              <div className={classes.badgeDevice}>
                <UserStatusMobileIcon style={{ fontSize: 12 }} />
              </div>
            </div>
          </div>
        );
      }
    }

    const detail = data.fullLocation?.result;
    const first = data.fullLocation?.first;

    return (
      <ListItem
        style={{
          paddingTop: 10,
          paddingBottom: 10,
          paddingRight: 0
        }}
        button
      >
        <ListItemIcon style={{ height: 50, paddingRight: 15 }}>
          <div style={{ flexDirection: 'row', display: 'flex' }}>
            <div className={classes.contentStatus}>
              {renderHoliday ? (
                <div style={{ marginTop: 4 }}>
                  <TalkIcon name="holiday" />
                </div>
              ) : null}
              {renderBirthDay ? (
                <div style={{ marginTop: 4 }}>
                  <TalkIcon name="birthday" />
                </div>
              ) : null}
              {iconStatus}
            </div>
            <div>
              <UserPhotoView key={data.userKey} data={data} imgSize={50} />
              {renderDeviceList}
            </div>
          </div>
        </ListItemIcon>
        <ListItemText
          primary={
            <Typography variant="subtitle2" className={classes.textName}>
              {data.displayNameWithDuty}
            </Typography>
          }
          secondary={
            <Typography
              variant="caption"
              color="textSecondary"
              className={classes.textInfor}
            >
              {detail}
              <b>{first}</b>
            </Typography>
          }
        />
      </ListItem>
    );
  }
}
