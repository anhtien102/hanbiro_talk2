import React from 'react';
import { useSelector } from 'react-redux';
import RoomView from './RoomView';
import TabRoom from './TabRoom';

export default function CommonHeader(props) {
  const storageCommonSetting = useSelector(state => state.setting.common);
  return storageCommonSetting.use_new_tab ? <TabRoom /> : <RoomView />;
}
