import { remote, ipcRenderer, app } from 'electron';
import React, { Component } from 'react';

type Props = {};
export default class WorkerPage extends Component<Props> {
  props: Props;

  constructor(props: Props) {
    super(props);
    console.log('Start worker thread');
  }

  componentDidMount() {
    function sendNetworkStatus() {
      ipcRenderer.send(
        'online-status-changed',
        navigator.onLine ? 'online' : 'offline'
      );
    }
    const updateOnlineStatus = () => {
      sendNetworkStatus();
    };

    window.addEventListener('load', () => {
      sendNetworkStatus();
    });
    window.addEventListener('online', updateOnlineStatus);
    window.addEventListener('offline', updateOnlineStatus);
  }

  componentWillUnmount() {
    window.removeEventListener('load');
    window.removeEventListener('online');
    window.removeEventListener('offline');
  }

  render() {
    return <div></div>;
  }
}
