import * as actionTypes from '../actions/actionTypes';

const initialState = {
  scroll_offset: {},
  selected_room: {},
  selected_whisper: {},
  whisper_detail: [],
  whisper_page: 0,
  whisper_u2u: '',
  show_search_result: false,
  show_disconnect: false,
  showRightPannel: false,
  showRightBoardPannel: false,
  groupwareUnreadCount: {
    mail: 0,
    board: 0,
    approval: 0,
    circular: 0,
    todo: 0,
    task: 0,
    whisper: 0
  },
  room_tabs: [],
  extraPositionData: null
};

const cheatCategories = [
  'tab_company',
  'tab_favorite',
  'tab_room_list',
  'tab_search'
];

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case actionTypes.NAVIGATE_LOGIN_PAGE:
      return {
        scroll_offset: {},
        selected_room: {},
        selected_whisper: {},
        whisper_detail: [],
        show_search_result: false,
        show_disconnect: false,
        groupwareUnreadCount: {
          mail: 0,
          board: 0,
          approval: 0,
          circular: 0,
          todo: 0,
          task: 0,
          whisper: 0
        },
        room_tabs: [],
        extraPositionData: null
      };

    case actionTypes.SHOW_DISCONNECT:
      return {
        ...state,
        show_disconnect: action.show
      };

    case actionTypes.SHOW_RIGHT_PANNEL: {
      return {
        ...state,
        showRightBoardPannel: action.show
          ? !action.show
          : state.showRightBoardPannel,
        showRightPannel: action.show
      };
    }

    case actionTypes.SHOW_RIGHT_BOARD_PANNEL: {
      return {
        ...state,
        showRightPannel: action.show ? !action.show : state.showRightPannel,
        showRightBoardPannel: action.show
      };
    }

    case actionTypes.SAVE_SCROLL_OFFSET:
      let newScrollOffset = { ...state.scroll_offset };
      let category = action.category;
      newScrollOffset[category] = action.scroll_offset;
      return {
        ...state,
        scroll_offset: newScrollOffset
      };

    case actionTypes.OPEN_ROOM: {
      let lastSelectedRoom = null;
      let selected_room = state.selected_room;
      if (action.setting_use_new_tab) {
        lastSelectedRoom = selected_room[cheatCategories[0]];
        cheatCategories.forEach(category => {
          selected_room[category] = {
            primary_key: action.primaryKey,
            users: action.users,
            room: action.room
          };
        });
      } else {
        const category = action.category;
        selected_room[category] = {
          primary_key: action.primaryKey,
          users: action.users,
          room: action.room
        };
      }

      let room_tabs = state.room_tabs;
      let index = room_tabs.findIndex(
        tab => tab.roomKey == action.room.rRoomKey
      );

      if (index >= 0) {
        if (action.newTab) {
          room_tabs[index].mark = true;
        }

        return {
          ...state,
          selected_room: selected_room,
          room_tabs: room_tabs,
          extraPositionData: action.extraPositionData
        };
      }

      let findLastTabIndex = lastSelectedRoom
        ? room_tabs.findIndex(
            tab => tab.roomKey == lastSelectedRoom.room?.rRoomKey
          )
        : room_tabs.length - 1;

      if (action.newTab) {
        findLastTabIndex =
          findLastTabIndex < 0 ? room_tabs.length - 1 : findLastTabIndex;
        if (findLastTabIndex >= 0) {
          room_tabs.splice(findLastTabIndex + 1, 0, {
            mark: action.newTab,
            roomKey: action.room.rRoomKey
          });
        } else {
          room_tabs = [
            ...room_tabs,
            {
              mark: action.newTab,
              roomKey: action.room.rRoomKey
            }
          ];
        }
      } else {
        const lastTab =
          findLastTabIndex >= 0 ? state.room_tabs[findLastTabIndex] : null;
        if (lastTab && !lastTab.mark) {
          room_tabs = state.room_tabs.filter(tab => tab.mark == true);
          room_tabs.splice(findLastTabIndex, 0, {
            mark: action.newTab,
            roomKey: action.room.rRoomKey
          });
        } else {
          room_tabs = state.room_tabs.filter(tab => tab.mark == true);

          findLastTabIndex = lastSelectedRoom
            ? room_tabs.findIndex(
                tab => tab.roomKey == lastSelectedRoom.room?.rRoomKey
              )
            : room_tabs.length - 1;
          findLastTabIndex =
            findLastTabIndex < 0 ? room_tabs.length - 1 : findLastTabIndex;

          if (findLastTabIndex >= 0) {
            room_tabs.splice(findLastTabIndex + 1, 0, {
              mark: action.newTab,
              roomKey: action.room.rRoomKey
            });
          } else {
            room_tabs = [
              ...room_tabs,
              {
                mark: action.newTab,
                roomKey: action.room.rRoomKey
              }
            ];
          }
        }
      }

      return {
        ...state,
        selected_room: selected_room,
        room_tabs: room_tabs,
        extraPositionData: action.extraPositionData
      };
    }

    case actionTypes.REMOVE_TAB_AND_OPEN: {
      let selected_room = state.selected_room;
      if (action.setting_use_new_tab) {
        if (action.primaryKey) {
          cheatCategories.forEach(category => {
            selected_room[category] = {
              primary_key: action.primaryKey,
              users: action.users,
              room: action.room
            };
          });
        }
      } else {
        if (action.primaryKey) {
          const category = action.category;
          selected_room[category] = {
            primary_key: action.primaryKey,
            users: action.users,
            room: action.room
          };
        }
      }

      let room_tabs = state.room_tabs.filter(
        tab => tab.roomKey != action.removeRoom.rRoomKey
      );

      if (action.room) {
        let find = room_tabs.find(tab => tab.roomKey == action.room.rRoomKey);
        if (!find) {
          room_tabs = [
            ...room_tabs,
            {
              mark: action.newTab,
              roomKey: action.room.rRoomKey
            }
          ];
        }
      }

      return {
        ...state,
        selected_room: selected_room,
        room_tabs: room_tabs
      };
    }

    case actionTypes.UPDATE_TAB: {
      let room_tabs = state.room_tabs;

      let index = room_tabs.findIndex(
        tab => tab.roomKey == action.room.rRoomKey
      );

      if (index < 0) {
        room_tabs = [
          ...room_tabs,
          { mark: true, roomKey: action.room.rRoomKey }
        ];
      } else {
        room_tabs[index].mark = true;
      }

      return {
        ...state,
        room_tabs: room_tabs
      };
    }

    case actionTypes.CHECK_AND_CLEAR_ROOM_KEY: {
      const roomKey = action.roomKey;
      let selected_room = state.selected_room;

      for (let key in selected_room) {
        const tab = selected_room[key];
        if (roomKey == tab?.primary_key) {
          selected_room[key] = null;
        }
      }

      let room_tabs = state.room_tabs.filter(tab => tab.roomKey != roomKey);

      return {
        ...state,
        selected_room: selected_room,
        room_tabs: room_tabs
      };
    }

    case actionTypes.UPDATE_UNREAD_GROUPWARE_MENU:
      let data = action.data ?? {};

      let mail = data.mail.newCount;
      let board = data.board.newCount;
      let approval = data.approval.newCount;
      let circular = data.circular.newCount;
      let todo = data.todo.newCount;
      let task = data.task.newCount;
      let whisper = data.whisper.newCount;

      return {
        ...state,
        groupwareUnreadCount: {
          mail: mail,
          board: board,
          approval: approval,
          circular: circular,
          todo: todo,
          task: task,
          whisper: whisper
        }
      };

    case actionTypes.OPEN_WHISPER:
      return {
        ...state,
        selected_whisper: action.whisper,
        whisper_u2u: action.whisper.u2u
      };

    case actionTypes.SAVE_WHISPER_DETAIL:
      let list = action.rows;
      list.sort((a, b) => (a.timestamp > b.timestamp ? 1 : -1));
      if (action.page > 1) {
        return {
          ...state,
          whisper_detail: list.concat(state.whisper_detail),
          whisper_u2u: action.u2u,
          whisper_page: action.page
        };
      }
      return {
        ...state,
        whisper_detail: list,
        whisper_u2u: action.u2u,
        whisper_page: action.page
      };

    case actionTypes.OPEN_SEARCH_RESULT:
      if (action.setting_use_new_tab) {
        return {
          ...state,
          show_search_result: action.show,
          selected_room: {
            ...state.selected_room
          }
        };
      } else {
        return {
          ...state,
          show_search_result: action.show,
          selected_room: {
            ...state.selected_room,
            tab_search: {}
          }
        };
      }

    default:
      return state;
  }
};
