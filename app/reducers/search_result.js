import * as actionTypes from '../actions/actionTypes';
const initialState = {
  search_text: null,
  allSeachResult: []
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case actionTypes.OPEN_SEARCH_RESULT:
      if (action.searchText == null || action.searchText == '') {
        return { ...initialState };
      }
      return {
        ...state,
        search_text: action.searchText
      };
    case actionTypes.SAVE_SEARCH_RESULT:
      return {
        search_text: action.searchText,
        allSeachResult: action.allSearchResult
      };
    default:
      return state;
  }
};
