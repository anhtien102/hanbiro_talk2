import * as actionTypes from '../actions/actionTypes';
const initialState = {
  rooms: [],
  allUnreadMessage: 0,
  allUnreadMessageBadgeIcon: 0,
  room_cached: {}
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case actionTypes.NAVIGATE_LOGIN_PAGE:
      return {
        rooms: [],
        allUnreadMessage: 0,
        allUnreadMessageBadgeIcon: 0,
        room_cached: {}
      };
    case actionTypes.SAVE_ROOM_LIST:
      return {
        ...state,
        rooms: action.roomListObj.rooms,
        allUnreadMessage: action.roomListObj.allUnreadMessage,
        allUnreadMessageBadgeIcon: action.roomListObj.allUnreadMessageBadgeIcon,
        room_cached: action.roomListObj.room_cached
      };
    default:
      return state;
  }
};
