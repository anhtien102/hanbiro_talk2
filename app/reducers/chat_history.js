import * as actionTypes from '../actions/actionTypes';

import {
  isBefore,
  isBefore1Hour,
  isDifferentMinute
} from '../core/utils/date.utils';

import { MessageType } from '../core/service/talk-constants';

const initialState = {
  keyword: null,
  historyList: [],
  selected_history: {},
  history_detail: [],
  history_detail_UI: []
};

export const STATE_REFRESH = 1;
export const STATE_BOTTOM = 2;
export const STATE_UP = 3;

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case actionTypes.NAVIGATE_LOGIN_PAGE:
      return {
        keyword: null,
        historyList: [],
        selected_history: {},
        history_detail: [],
        history_detail_UI: []
      };

    case actionTypes.CLEAR_HISTORY_LIST:
      return {
        keyword: null,
        historyList: [],
        selected_history: {},
        history_detail: [],
        history_detail_UI: []
      };

    case actionTypes.SAVE_HISTORY_LIST:
      if (action.loadMore) {
        return {
          ...state,
          historyList: state.historyList.concat(action.rows)
        };
      }
      return {
        ...state,
        keyword: action.keyword,
        historyList: action.rows
      };

    case actionTypes.OPEN_HISTORY:
      return {
        ...state,
        selected_history: action.history
      };

    case actionTypes.SAVE_HISTORY_DETAIL:
      const loggedUserKey = action.loggedUserKey;

      let list = action.rows;
      let historyDetail = list;

      if (action.state == STATE_UP) {
        historyDetail = list.concat(state.history_detail);
      }
      if (action.state == STATE_BOTTOM) {
        historyDetail = state.history_detail.concat(list);
      }

      let historyDetailUI = [];

      let shiftMessage = null;
      let shiftNameMessage = null;
      let shiftTimeMessage = null;
      let shiftPrevMessage = null;

      historyDetail.forEach(msg => {
        msg.time = parseInt(msg.time);
        msg.isMe = msg.user == loggedUserKey;
        msg.msgIsDeleted = msg.is_deleted;
        msg.primaryKey = msg.time;
        msg.msgEmotion = msg.emoticon;
        msg.isCenterMessage =
          msg.time == state.selected_history.SEARCH_MESSAGE_TIME;

        if (msg.type == 'event') {
          let eventName = msg.event;
          if (eventName == 'ROOMALIAS') {
            msg.msgType = MessageType.MESSAGE_TYPE_ROOM_ALIAS;
          } else if (eventName == 'ROOMOUT') {
            msg.msgType = MessageType.MESSAGE_TYPE_OUT;
          } else if (eventName == 'ROOMIN') {
            msg.msgType = MessageType.MESSAGE_TYPE_INVITE;
          }
        } else if (msg.type == 'file') {
          msg.msgType = MessageType.MESSAGE_TYPE_FILE;
        } else {
          msg.msgType = MessageType.MESSAGE_TYPE_TEXT;
        }

        if (shiftMessage == null) {
          shiftMessage = msg;
        }

        if (
          shiftTimeMessage != null &&
          (shiftTimeMessage.user != msg.user ||
            isDifferentMinute(shiftTimeMessage.time, msg.time))
        ) {
          shiftTimeMessage.isShowTime = true;
        }
        shiftTimeMessage = msg;

        if (msg.user != loggedUserKey) {
          if (shiftNameMessage == null) {
            shiftNameMessage = msg;
            shiftNameMessage.isShowName = true;

            if (shiftPrevMessage != null && msg.user != shiftPrevMessage.user) {
              if (isBefore1Hour(shiftPrevMessage.time, msg.time)) {
                msg.showTimePerHours = true;
              }
            }
          } else if (
            shiftPrevMessage != null &&
            msg.user != shiftPrevMessage.user
          ) {
            shiftNameMessage = msg;
            shiftNameMessage.isShowName = true;
            if (isBefore1Hour(shiftPrevMessage.time, msg.time)) {
              msg.showTimePerHours = true;
            }
          }
        } else {
          if (shiftPrevMessage != null && msg.user != shiftPrevMessage.user) {
            msg.isShowPad = true;
          }
          msg.isShowName = false;
          shiftNameMessage = null;
        }

        if (historyDetailUI.length > 0) {
          if (isBefore(shiftMessage.time, msg.time)) {
            let isShow = true;
            shiftMessage.isShowDate = isShow;
            shiftMessage.isShowName = isShow;
            shiftMessage = msg;
            if (shiftPrevMessage != null) {
              shiftPrevMessage.isShowTime = isShow;
            }
          }
        }

        shiftPrevMessage = msg;

        historyDetailUI.push(msg);
      });

      if (shiftMessage != null) {
        let isShow = true;
        shiftMessage.isShowDate = isShow;
        shiftMessage.isShowName = isShow;
      }

      if (shiftTimeMessage != null) {
        shiftTimeMessage.isShowTime = true;
      }

      return {
        ...state,
        history_detail: historyDetail,
        history_detail_UI: historyDetailUI
      };

    default:
      return state;
  }
};
