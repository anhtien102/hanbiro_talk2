import * as actionTypes from '../actions/actionTypes';
const initialState = {
  whisper: []
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case actionTypes.SAVE_WHISPER_LIST:
      if (action.loadMore) {
        return {
          ...state,
          whisper: state.whisper.concat(action.rows)
        };
      }
      return {
        ...state,
        whisper: action.rows
      };

    default:
      return state;
  }
};
