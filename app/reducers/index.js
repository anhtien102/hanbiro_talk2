// @flow
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import auth from './auth';
import company from './company';
import room_list from './room_list';
import message_list from './message_list';
import appUI from './app.ui.state';
import search_result from './search_result';
import whisper_list from './whisper_list';
import chat_history from './chat_history';
import setting from './setting';

export default function createRootReducer(history) {
  return combineReducers({
    router: connectRouter(history),
    auth,
    company,
    room_list,
    message_list,
    appUI,
    whisper_list,
    search_result,
    setting,
    chat_history
  });
}
