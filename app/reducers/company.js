import * as actionTypes from '../actions/actionTypes';
const initialState = {
  org: [],
  org_fav: [],
  group_cached: {},
  user_cached: {},
  org_time_card: {},
  listActualyFav: {}
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case actionTypes.NAVIGATE_LOGIN_PAGE:
      return {
        org: [],
        org_fav: [],
        group_cached: {},
        user_cached: {},
        org_time_card: {},
        listActualyFav: {}
      };
    case actionTypes.SAVE_ORG_TREE:
      return {
        ...state,
        org: action.org,
        org_fav: action.org_fav,
        group_cached: action.group_cached,
        user_cached: action.user_cached,
        listActualyFav: action.listActualyFav
      };
    case actionTypes.SAVE_ORG_TIME_CARD:
      return {
        ...state,
        org_time_card: action.org_time_card
      };
    default:
      return state;
  }
};
