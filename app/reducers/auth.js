import * as actionTypes from '../actions/actionTypes';
const initialState = {
  user: {},
  extraInfo: {},
  authentication: false
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case actionTypes.INIT_APP_SETTING_UPDATE:
      return {
        user: action.user,
        extraInfo: action.extraInfo,
        authentication: action.authentication
      };

    case actionTypes.UPDATE_EXTRA_LOGIN_INFO:
      if (action.data?.nickname) {
        let stateNew = { ...state };
        stateNew.user.extra_login_info.nickName = action.data?.value;
        stateNew.user.extra_login_info.login.nickname = action.data?.value;
        return stateNew;
      } else if (action.data?.weblogin) {
        let stateNew = { ...state };
        if (stateNew?.user?.account_info) {
          stateNew.user.account_info.web_auto_login_key = action.data?.value;
        }

        return stateNew;
      } else if (action.data?.authKey) {
        let stateNew = { ...state };
        if (stateNew?.user?.account_info) {
          stateNew.user.account_info.auto_login_key = action.data?.value;
        }

        return stateNew;
      }
      return state;

    case actionTypes.NAVIGATE_HOME_PAGE:
      return {
        user: action.user,
        extraInfo: action.extraInfo,
        authentication: true
      };
    case actionTypes.NAVIGATE_LOGIN_PAGE:
      return {
        user: {},
        extraInfo: action.extraInfo,
        authentication: false
      };
    default:
      return state;
  }
};
