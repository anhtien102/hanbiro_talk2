import * as actionTypes from '../actions/actionTypes';
const initialState = {
  messages: [],
  roomInfo: {
    roomKey: '',
    roomID: -1,
    firstTimeMessage: 0
  },
  room: null,
  listAllUserInRoom: [],
  listUserActiveInRoom: []
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case actionTypes.NAVIGATE_LOGIN_PAGE:
      return {
        messages: [],
        roomInfo: {
          roomKey: '',
          roomID: -1,
          firstTimeMessage: 0
        },
        room: null,
        listAllUserInRoom: [],
        listUserActiveInRoom: []
      };

    case actionTypes.OPEN_ROOM:
      let roomInfo = null;
      if (action.room) {
        roomInfo = {
          roomKey: action.room.rRoomKey,
          roomID: action.room.rID,
          firstTimeMessage: 0
        };
      }

      return {
        ...state,
        messages: [],
        roomInfo: roomInfo,
        room: action.room,
        listAllUserInRoom: [],
        listUserActiveInRoom: []
      };

    case actionTypes.CHECK_AND_CLEAR_ROOM_KEY: {
      if (state.roomInfo?.roomKey == action.roomKey) {
        return {
          messages: [],
          roomInfo: {
            roomKey: '',
            roomID: -1,
            firstTimeMessage: 0
          },
          room: null,
          listAllUserInRoom: [],
          listUserActiveInRoom: []
        };
      }
      return state;
    }

    case actionTypes.SAVE_ROOM_INFO:
      return {
        ...state,
        room: action.roomInfo,
        listAllUserInRoom: action.listAllUserInRoom,
        listUserActiveInRoom: action.listUserActiveInRoom
      };

    case actionTypes.SAVE_MSG_LIST:
      return {
        ...state,
        messages: action.messages,
        roomInfo: {
          roomKey: action.roomKey,
          roomID: action.roomID,
          firstTimeMessage: action.firstTimeMessage
        }
      };

    case actionTypes.SAVE_BOTH_ROOM_INFO_MSG_LIST:
      return {
        ...state,
        room: action.roomInfo,
        listAllUserInRoom: action.listAllUserInRoom,
        listUserActiveInRoom: action.listUserActiveInRoom,
        messages: action.messages,
        roomInfo: {
          roomKey: action.roomKey,
          roomID: action.roomID,
          firstTimeMessage: action.firstTimeMessage
        }
      };
    default:
      return state;
  }
};
