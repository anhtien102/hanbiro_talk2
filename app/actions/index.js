import * as actionTypes from './actionTypes';

export function requestNavigateHomePage(
  user = {},
  extraInfo = {},
  authentication = true
) {
  return {
    type: actionTypes.NAVIGATE_HOME_PAGE,
    user,
    extraInfo,
    authentication
  };
}

export function requestInitAppSetting(
  user = {},
  extraInfo = {},
  authentication = false
) {
  return {
    type: actionTypes.INIT_APP_SETTING_UPDATE,
    user: user,
    extraInfo: extraInfo,
    authentication: authentication
  };
}

export function requestUpdateExtraLoginInfo(data) {
  return {
    type: actionTypes.UPDATE_EXTRA_LOGIN_INFO,
    data: data
  };
}

export function requestNavigateLoginPage(extraInfo) {
  return {
    type: actionTypes.NAVIGATE_LOGIN_PAGE,
    user: {},
    extraInfo: extraInfo,
    authentication: false
  };
}

export function saveOrgTree(
  org,
  org_fav,
  group_cached,
  user_cached,
  listActualyFav
) {
  return {
    type: actionTypes.SAVE_ORG_TREE,
    org,
    org_fav,
    group_cached,
    user_cached,
    listActualyFav
  };
}

export function saveOrgTimeCard(timeCard) {
  return {
    type: actionTypes.SAVE_ORG_TIME_CARD,
    org_time_card: timeCard
  };
}

export function saveRoomList(roomListObj) {
  return {
    type: actionTypes.SAVE_ROOM_LIST,
    roomListObj: roomListObj
  };
}

export function saveBothRoomInfoMessageList(
  roomInfo,
  listAllUserInRoom,
  listUserActiveInRoom,
  messages,
  roomKey,
  roomID,
  firstTimeMessage
) {
  return {
    type: actionTypes.SAVE_BOTH_ROOM_INFO_MSG_LIST,
    roomInfo: roomInfo,
    listAllUserInRoom: listAllUserInRoom,
    listUserActiveInRoom: listUserActiveInRoom,
    messages: messages,
    roomID: roomID,
    roomKey: roomKey,
    firstTimeMessage: firstTimeMessage
  };
}

export function saveMessageList(messages, roomKey, roomID, firstTimeMessage) {
  return {
    type: actionTypes.SAVE_MSG_LIST,
    messages: messages,
    roomID: roomID,
    roomKey: roomKey,
    firstTimeMessage: firstTimeMessage
  };
}

export function saveRoomInfo(
  roomInfo,
  listAllUserInRoom,
  listUserActiveInRoom
) {
  return {
    type: actionTypes.SAVE_ROOM_INFO,
    roomInfo: roomInfo,
    listAllUserInRoom: listAllUserInRoom,
    listUserActiveInRoom: listUserActiveInRoom
  };
}

export function checkAndClearRoomKey(roomKey) {
  return {
    type: actionTypes.CHECK_AND_CLEAR_ROOM_KEY,
    roomKey: roomKey
  };
}

export function saveScrollOffset(category, scroll_offset) {
  return {
    type: actionTypes.SAVE_SCROLL_OFFSET,
    category: category,
    scroll_offset: scroll_offset
  };
}

export function openOneRoom(
  category,
  primaryKey,
  room,
  users,
  newTab,
  commonSettings,
  extraPositionData
) {
  return {
    type: actionTypes.OPEN_ROOM,
    category: category,
    users: users,
    primaryKey: primaryKey,
    room: room,
    newTab: newTab,
    setting_use_new_tab: commonSettings?.use_new_tab,
    extraPositionData: extraPositionData
  };
}

export function updateTab(room) {
  return {
    type: actionTypes.UPDATE_TAB,
    room: room
  };
}

export function removeTabAndOpen(
  category,
  primaryKey,
  room,
  users,
  newTab,
  removeRoom,
  commonSettings
) {
  return {
    type: actionTypes.REMOVE_TAB_AND_OPEN,
    category: category,
    users: users,
    primaryKey: primaryKey,
    room: room,
    newTab: newTab,
    removeRoom: removeRoom,
    setting_use_new_tab: commonSettings?.use_new_tab
  };
}

export function showSearchResult(
  commonSettings,
  show = false,
  searchText = ''
) {
  return {
    type: actionTypes.OPEN_SEARCH_RESULT,
    show,
    searchText: searchText,
    setting_use_new_tab: commonSettings?.use_new_tab
  };
}

export function saveSearchResult(searchText, allSearch) {
  return {
    type: actionTypes.SAVE_SEARCH_RESULT,
    searchText: searchText,
    allSearchResult: allSearch
  };
}

export function updateUnreadGroupwareMenu(data) {
  return {
    type: actionTypes.UPDATE_UNREAD_GROUPWARE_MENU,
    data
  };
}

export function saveWhisperList(rows = [], loadMore) {
  return {
    type: actionTypes.SAVE_WHISPER_LIST,
    rows,
    loadMore
  };
}

export function openWhisper(whisper = {}) {
  return {
    type: actionTypes.OPEN_WHISPER,
    whisper: whisper
  };
}

export function saveWhisperDetail(rows = [], u2u = '', page) {
  return {
    type: actionTypes.SAVE_WHISPER_DETAIL,
    rows,
    u2u,
    page
  };
}

export function saveHistoryList(rows = [], keyword = null, loadMore) {
  return {
    type: actionTypes.SAVE_HISTORY_LIST,
    rows,
    keyword,
    loadMore
  };
}

export function clearHistoryList() {
  return {
    type: actionTypes.CLEAR_HISTORY_LIST
  };
}

export function saveHistoryDetail(rows = [], state, loggedUserKey) {
  return {
    type: actionTypes.SAVE_HISTORY_DETAIL,
    rows,
    state,
    loggedUserKey
  };
}

export function openHistory(history = {}) {
  return {
    type: actionTypes.OPEN_HISTORY,
    history: history
  };
}

// SETTING ACCTION

export function onChangeFont(font) {
  return {
    type: actionTypes.CHANGE_FONT_STYLE,
    font
  };
}

export function onChangeFontSize(fontSize) {
  return {
    type: actionTypes.CHANGE_FONT_SIZE,
    fontSize
  };
}

export function changePrimaryTheme(primaryTheme) {
  return {
    type: actionTypes.CHANGE_PRIMARY_THEME,
    primaryTheme
  };
}

export function changeSecondaryTheme(secondaryTheme) {
  return {
    type: actionTypes.CHANGE_SECONDARY_THEME,
    secondaryTheme
  };
}

export function changeDarkTheme(darkmode) {
  return {
    type: actionTypes.CHANGE_DARK_THEME,
    darkmode
  };
}

export function changeCommonSetting(common) {
  return {
    type: actionTypes.CHANGE_COMMON_SETTING,
    common
  };
}

export function showDisconnectSocket(show) {
  return {
    type: actionTypes.SHOW_DISCONNECT,
    show: show
  };
}

export function showRightPannelPage(show) {
  return {
    type: actionTypes.SHOW_RIGHT_PANNEL,
    show: show
  };
}

export function showRightBoardPannelPage(show) {
  return {
    type: actionTypes.SHOW_RIGHT_BOARD_PANNEL,
    show: show
  };
}
