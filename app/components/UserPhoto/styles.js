import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    icon: {
      width: '100%',
      height: '100%'
    },
    circle: {
      borderRadius: '50%'
    },
    border: {
      borderWidth: 1,
      borderColor: theme.palette.divider
    }
  })
);
