import React, { useState, useEffect, useRef } from 'react';
import talkApi from '../../core/service/talk.api.render';
import fs from 'fs';
import path from 'path';
import crypto from 'crypto';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { ensureFilePath } from '../../configs/file_cheat';

export default function UserPhotoView(props) {
  const {
    data,
    imgSize,
    userKeyData,
    placeHolder,
    border,
    circle,
    style
  } = props;
  const elementRef = useRef();
  const [fileURL, setFileURL] = useState(null);
  const classes = useStyles();

  useEffect(() => {
    let userKey = data?.userKey ?? null;
    let photoTime = data?.userPhotoTime ?? null;

    if (!userKey && !photoTime) {
      userKey = userKeyData;
      photoTime = -1;
    }

    if (photoTime == 0) {
      //dont have photo 0 is no photo -1 is force load photo
      return;
    }

    if (userKey.includes('L')) {
      return;
    }

    const url = talkApi.gwUserPhotoURL(
      userKey,
      false,
      imgSize,
      imgSize,
      photoTime
    );

    const fileName = crypto
      .createHash('sha1')
      .update(url)
      .digest('hex');
    const filePath = path.join(talkApi.photoUserDir(), fileName);

    if (fs.existsSync(filePath)) {
      if (elementRef.current) {
        setFileURL(filePath);
      }

      return;
    }

    let image = new Image();
    image.onload = event => {
      if (!fs.existsSync(filePath)) {
        const canvas = document.createElement('canvas');
        canvas.width = image.width;
        canvas.height = image.height;
        const ctx = canvas.getContext('2d');
        ctx.drawImage(image, 0, 0);
        const dataURL = canvas.toDataURL('image/png');
        const src = dataURL.replace(/^data:image\/(png|jpg);base64,/, '');
        fs.writeFileSync(filePath, src, 'base64');
        if (elementRef.current) {
          setFileURL(filePath);
        }
      } else {
        if (elementRef.current) {
          setFileURL(filePath);
        }
      }
    };
    image.onerror = event => {
      // nothing to do
      event.preventDefault();
    };

    const realURL = talkApi.gwUserPhotoURL(
      userKey,
      true,
      imgSize,
      imgSize,
      photoTime
    );
    image.src = realURL;
  }, []);

  // console.log('after', fileURL);
  return (
    <img
      draggable="false"
      style={style}
      ref={elementRef}
      className={clsx(
        classes.icon,
        border ? classes.border : null,
        circle ? classes.circle : null
      )}
      src={ensureFilePath(fileURL) ?? placeHolder}
      border="5"
    ></img>
  );
}

UserPhotoView.propTypes = {
  data: PropTypes.object,
  imgSize: PropTypes.number,
  userKeyData: PropTypes.any,
  placeHolder: PropTypes.string,
  circle: PropTypes.bool,
  border: PropTypes.bool,
  style: PropTypes.object
};

UserPhotoView.defaultProps = {
  data: {},
  imgSize: 50,
  userKeyData: '',
  placeHolder: './images/user-default.jpg',
  circle: true,
  border: true,
  style: {
    width: 50,
    height: 50
  }
};
