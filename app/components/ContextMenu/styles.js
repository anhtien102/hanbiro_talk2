import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    popover: {
      pointerEvents: 'none'
    },
    paper: {
      padding: theme.spacing(1)
    },
    item: {
      minWidth: 100,
      fontSize: '1rem',
      paddingTop: 3,
      paddingBottom: 3
    }
  })
);
