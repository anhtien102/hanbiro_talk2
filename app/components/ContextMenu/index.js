import React, { useEffect, useState } from 'react';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import { Menu, MenuItem } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

function ContextMenu(props) {
  const { t } = useTranslation();
  const { contextId, onCut, onCopy, onPaste } = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const [position, setPosition] = useState({ x: 0, y: 0 });
  const [disabledItem, setDisabledItem] = useState(false);
  const open = Boolean(anchorEl);
  const classes = useStyles();

  const handleOpen = event => {
    const textHighlighted = window.getSelection().toString();
    setDisabledItem(!Boolean(textHighlighted));
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleCut = () => {
    handleClose();
    onCut();
  };

  const handleCopy = () => {
    onCopy();
    handleClose();
  };

  const handlePaste = () => {
    onPaste();
    handleClose();
  };

  useEffect(() => {
    const context = document.getElementById(contextId);
    context.addEventListener('contextmenu', event => {
      event.preventDefault();
      event.stopPropagation();
      setPosition({ x: event.pageX, y: event.pageY });
      handleOpen(event);
    });
  }, []);

  return (
    <div>
      <Menu
        autoFocus={true}
        id="simple-menu"
        // anchorEl={anchorEl}
        anchorReference="anchorPosition"
        // keepMounted
        open={open}
        onClose={handleClose}
        anchorPosition={{ top: position.y, left: position.x }}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left'
        }}
      >
        <MenuItem
          classes={{ root: classes.item }}
          disabled={disabledItem}
          onClick={handleCut}
        >
          {t('Cut')}
        </MenuItem>
        <MenuItem
          classes={{ root: classes.item }}
          disabled={disabledItem}
          onClick={handleCopy}
        >
          {t('Copy')}
        </MenuItem>
        <MenuItem classes={{ root: classes.item }} onClick={handlePaste}>
          {t('Paste')}
        </MenuItem>
      </Menu>
    </div>
  );
}

ContextMenu.propTypes = {
  contextId: PropTypes.string,
  onCut: PropTypes.func,
  onCopy: PropTypes.func,
  onPaste: PropTypes.func
};

ContextMenu.defaultProps = {
  contextId: '',
  onCut: () => {},
  onCopy: () => {},
  onPaste: () => {}
};

export default ContextMenu;
