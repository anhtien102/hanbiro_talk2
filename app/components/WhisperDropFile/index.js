import React, { useEffect, useState, useRef } from 'react';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

export default function WhisperDropFile(props) {
  const { t } = useTranslation();
  const flagChecker = useRef(false);
  const classes = useStyles();
  const { contextId, onChange, onSend } = props;
  const [visible, setVisible] = useState(false);
  const contextDrop = 'WhisperDropFile_' + contextId;

  useEffect(() => {
    const context = document.getElementById(contextId);

    context.addEventListener('dragenter', dragenterParent);
    context.addEventListener('dragleave', dragleaveParent);
    context.addEventListener('drop', dropParent);

    const contextDropFile = document.getElementById(contextDrop);
    contextDropFile.addEventListener('dragenter', dragenter);
    contextDropFile.addEventListener('dragleave', dragleave);
    contextDropFile.addEventListener('dragover', dragover);
    contextDropFile.addEventListener('drop', drop);

    return () => {
      context.removeEventListener('drop', dropParent);
      context.removeEventListener('dragenter', dragenterParent);
      context.removeEventListener('dragleave', dragleaveParent);
      contextDropFile.removeEventListener('dragenter', dragenter);
      contextDropFile.removeEventListener('dragleave', dragleave);
      contextDropFile.removeEventListener('dragover', dragover);
      contextDropFile.removeEventListener('drop', drop);
    };
  }, []);

  const dropParent = e => {
    e.preventDefault();
    e.stopPropagation();
    setVisible(false);
    onChange(false);
    if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
      onSend(e);
    }
  };

  const dragenterParent = e => {
    e.preventDefault();
    e.stopPropagation();
    flagChecker.current = false;
    setVisible(true);
    onChange(true);
  };

  const dragleaveParent = e => {
    e.preventDefault();
    if (!flagChecker.current) {
      setVisible(false);
      onChange(false);
    }
  };

  const dragenter = e => {
    flagChecker.current = true;
    e.preventDefault();
    e.stopPropagation();
    setVisible(true);
    onChange(true);
  };

  const dragleave = e => {
    e.preventDefault();
    setVisible(false);
    onChange(false);
  };

  const dragover = e => {
    e.preventDefault();
    e.stopPropagation();
  };

  const drop = e => {
    e.preventDefault();
    e.stopPropagation();
    setVisible(false);
    onChange(false);
    if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
      onSend(e);
    }
  };

  return (
    <div
      className={visible ? classes.drag : classes.dragDisable}
      style={{
        flex: 1,
        height: '100%',
        opacity: 0.8,
        display: visible ? 'block' : 'none'
      }}
      id={contextDrop}
    >
      <div
        className={classes.dragContent}
        style={{ marginLeft: 50, marginRight: 50, pointerEvents: 'none' }}
      >
        <Typography
          style={{ pointerEvents: 'none' }}
          variant="h6"
          className={
            visible ? classes.textDragActive : classes.textDragInActive
          }
        >
          {t('Drop & send here')}
        </Typography>
      </div>
    </div>
  );
}

WhisperDropFile.propTypes = {
  onChange: PropTypes.func,
  onSend: PropTypes.func,
  contextId: PropTypes.string
};

WhisperDropFile.defaultProps = {
  contextId: 'whisper_drop',
  onChange: () => {},
  onSend: () => {}
};
