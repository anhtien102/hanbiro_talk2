import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    main: {
      display: 'flex',
      flex: 1,
      alignItems: 'center',
      paddingLeft: 16,
      paddingRight: 16,
      width: '100%',
      flexDirection: 'row'
    },
    circleAvatar: {
      borderRadius: '50%'
    },
    contentInfor: {
      display: 'flex',
      width: '100%',
      display: 'table',
      tableLayout: 'fixed',
      flexDirection: 'column',
      paddingLeft: 8
    },
    textName: {
      display: 'block',
      maxWidth: 'calc(100% - 20px)',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    badgeDevice: {
      position: 'absolute',
      bottom: 0,
      right: -3,
      width: 18,
      height: 18,
      backgroundColor: 'white',
      borderRadius: '50%',
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex'
    },
    circleOnline: {
      backgroundColor: theme.palette.success.light,
      width: 10,
      height: 10,
      borderRadius: '50%'
    },
    circleOffline: {
      backgroundColor: '#cbced1',
      width: 10,
      height: 10,
      borderRadius: '50%'
    },
    contentStatusSmall: {
      position: 'absolute',
      bottom: 5,
      right: -3,
      width: 14,
      height: 14,
      backgroundColor: 'white',
      borderRadius: '50%',
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex'
    },
    contentStatusLarge: {
      position: 'absolute',
      bottom: 5,
      right: -3,
      width: 18,
      height: 18,
      backgroundColor: 'white',
      borderRadius: '50%',
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex'
    },
    menuIcon: {
      display: 'flex',
      marginRight: 8,
      alignItems: 'center',
      justifyContent: 'center'
    },
    inputInfor: { fontSize: theme.typography.fontSize, width: '100%' },
    memuItem: {
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex',
      flexDirection: 'row'
    },
    search: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-start',
      paddingLeft: 8,
      flexDirection: 'row',
      height: 24,
      borderColor: theme.palette.divider,
      borderWidth: 1,
      borderStyle: 'solid'
    }
  })
);
