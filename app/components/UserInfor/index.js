import React, { useState } from 'react';
import {
  ButtonBase,
  IconButton,
  ListItemIcon,
  ListItemText,
  Tooltip,
  MenuItem,
  Popover,
  Typography,
  InputBase
} from '@material-ui/core';
import ExitToApp from '@material-ui/icons/ExitToApp';
import PropTypes from 'prop-types';
import UserPhotoView from '../UserPhoto';
import { useStyles } from './styles';
import TalkIcon from '../../components/TalkIcon';
import { StatusMode } from '../../core/service/talk-constants';
import { useTranslation } from 'react-i18next';
import ClearIcon from '@material-ui/icons/Clear';
import CheckIcon from '@material-ui/icons/Check';

export default function UserInfor(props) {
  const classes = useStyles();
  const { t } = useTranslation();
  const {
    data,
    onPressLogout,
    onChangeStatus,
    onChangeUserBubble,
    extraLogin,
    loggedKey
  } = props;

  const [search, setSearch] = useState('');
  const [showInput, setShowInput] = useState(false);
  const [openMenuStatus, setOpenMenuStatus] = useState(null);

  const [searchFocused, setSearchFocused] = useState(false);

  const exportIconStatus = status => {
    switch (status) {
      case StatusMode.logout:
      case StatusMode.offline:
        return (
          <div className={classes.contentStatusSmall}>
            <div className={classes.circleOffline} />
          </div>
        );
      case StatusMode.available:
        return (
          <div className={classes.contentStatusSmall}>
            <div className={classes.circleOnline} />
          </div>
        );
      case StatusMode.away:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="idle" />
          </div>
        );
      case StatusMode.busy:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="busy" />
          </div>
        );
      case StatusMode.meeting:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="meeting" />
          </div>
        );
      case StatusMode.meal:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="meal" />
          </div>
        );
      case StatusMode.phone:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="call" />
          </div>
        );
      case StatusMode.out:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="out" />
          </div>
        );
      case StatusMode.business_trip:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="business_trip" />
          </div>
        );

      default:
        return null;
    }
  };

  /**
   * handle open Dialog Menu
   * @param {*} event
   */
  const handleOpen = event => {
    setOpenMenuStatus(event.currentTarget);
  };

  /**
   * call when select status
   * @param {*} status
   */
  const handleChangeStatus = status => {
    onChangeStatus(status);
    setOpenMenuStatus(null);
  };

  /**
   * call when save status text
   */
  const onSaveStatus = () => {
    onChangeUserBubble(search);
    setShowInput(false);
  };

  let displayName = data.displayName;
  let userNickName = data.userNickName;
  if (extraLogin) {
    if (displayName == '') {
      displayName = extraLogin.username;
    }
    if (userNickName == '') {
      userNickName = extraLogin.nickname;
    }
  }

  const iconStatus = exportIconStatus(data.userStatus);
  return (
    <div className={classes.main}>
      <div style={{ width: 50, height: 50 }}>
        <ButtonBase onClick={handleOpen}>
          <div style={{ position: 'relative' }}>
            <UserPhotoView
              key={loggedKey}
              userKeyData={loggedKey}
              data={data}
              imgSize={50}
            />
            {iconStatus}
          </div>
        </ButtonBase>
        <Popover
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'left'
          }}
          anchorEl={openMenuStatus}
          keepMounted
          onClick={() => setOpenMenuStatus(null)}
          open={Boolean(openMenuStatus)}
        >
          <MenuItem onClick={() => handleChangeStatus(StatusMode.available)}>
            <div className={classes.memuItem}>
              <div className={classes.circleOnline} style={{ marginLeft: 2 }} />
              <Typography
                variant="caption"
                style={{
                  paddingLeft: 8
                }}
              >
                {t('Online')}
              </Typography>
            </div>
          </MenuItem>
          <MenuItem onClick={() => handleChangeStatus(StatusMode.away)}>
            <div className={classes.memuItem}>
              <TalkIcon name="idle" />
              <Typography
                variant="caption"
                style={{
                  paddingLeft: 8
                }}
              >
                {t('Idle')}
              </Typography>
            </div>
          </MenuItem>
          <MenuItem onClick={() => handleChangeStatus(StatusMode.busy)}>
            <div className={classes.memuItem}>
              <TalkIcon name="busy" />
              <Typography
                variant="caption"
                style={{
                  paddingLeft: 8
                }}
              >
                {t('Busy')}
              </Typography>
            </div>
          </MenuItem>
          <MenuItem onClick={() => handleChangeStatus(StatusMode.meeting)}>
            <div className={classes.memuItem}>
              <TalkIcon name="meeting" />
              <Typography
                variant="caption"
                style={{
                  paddingLeft: 8
                }}
              >
                {t('In a Meetting')}
              </Typography>
            </div>
          </MenuItem>
          <MenuItem onClick={() => handleChangeStatus(StatusMode.meal)}>
            <div className={classes.memuItem}>
              <TalkIcon name="meal" />
              <Typography
                variant="caption"
                style={{
                  paddingLeft: 8
                }}
              >
                {t('Out to Lunch')}
              </Typography>
            </div>
          </MenuItem>
          <MenuItem onClick={() => handleChangeStatus(StatusMode.phone)}>
            <div className={classes.memuItem}>
              <TalkIcon name="call" />
              <Typography
                variant="caption"
                style={{
                  paddingLeft: 8
                }}
              >
                {t('On the Phone')}
              </Typography>
            </div>
          </MenuItem>
          <MenuItem onClick={() => handleChangeStatus(StatusMode.out)}>
            <div className={classes.memuItem}>
              <TalkIcon name="out" />
              <Typography
                variant="caption"
                style={{
                  paddingLeft: 8
                }}
              >
                {t('Out of Office')}
              </Typography>
            </div>
          </MenuItem>
          <MenuItem
            onClick={() => handleChangeStatus(StatusMode.business_trip)}
          >
            <div className={classes.memuItem}>
              <TalkIcon name="business_trip" />
              <Typography
                variant="caption"
                style={{
                  paddingLeft: 8
                }}
              >
                {t('On Business trip')}
              </Typography>
            </div>
          </MenuItem>
        </Popover>
      </div>
      <div className={classes.contentInfor}>
        <Typography variant="subtitle2" className={classes.textName}>
          {displayName}
        </Typography>
        {showInput ? (
          <div className={classes.search}>
            <InputBase
              multiline={false}
              className={classes.inputInfor}
              placeholder={searchFocused ? null : t('Type some thing')}
              value={search}
              onFocus={() => {
                setSearchFocused(true);
              }}
              onBlur={() => {
                setSearchFocused(false);
              }}
              onChange={event => setSearch(event.target.value)}
              onKeyDown={e => {
                if (e.keyCode == 13) {
                  onSaveStatus();
                }
              }}
            />
            <IconButton
              size="small"
              onClick={() => {
                setSearch('');
                setShowInput(false);
              }}
            >
              <ClearIcon fontSize="small" />
            </IconButton>
            <IconButton
              size="small"
              style={{ marginRight: 4 }}
              onClick={() => onSaveStatus()}
            >
              <CheckIcon fontSize="small" />
            </IconButton>
          </div>
        ) : (
          <Typography
            variant="caption"
            color="textSecondary"
            className={classes.textName}
            onClick={() => {
              setSearch(userNickName);
              setShowInput(true);
            }}
          >
            {userNickName ? userNickName : t('Type some thing')}
          </Typography>
        )}
      </div>

      <Tooltip title={t('tooltip_logout')} aria-label={t('tooltip_logout')}>
        <IconButton
          aria-label="delete"
          className={classes.margin}
          onClick={onPressLogout}
        >
          <ExitToApp />
        </IconButton>
      </Tooltip>
    </div>
  );
}

UserInfor.propTypes = {
  data: PropTypes.object,
  loggedKey: PropTypes.string,
  extraLogin: PropTypes.object,
  onPressLogout: PropTypes.func,
  onChangeStatus: PropTypes.func,
  onChangeUserBubble: PropTypes.func
};

UserInfor.defaultProps = {
  data: {
    displayName: '',
    userNickName: ''
  },
  loggedKey: '',
  extraLogin: {},
  onPressLogout: () => {},
  onChangeStatus: () => {},
  onChangeUserBubble: () => {}
};
