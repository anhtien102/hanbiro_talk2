export function saveTreeState(openState, name) {
  const json = JSON.stringify(openState);
  localStorage.setItem(name, json);
}

export function readTreeState(name) {
  const openState = localStorage.getItem(name);
  if (openState == null || openState == '') {
    return {};
  }
  return JSON.parse(openState);
}
