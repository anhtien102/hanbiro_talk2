import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  main: {
    width: '100%',
    height: '100%',
    position: 'relative',
    display: 'flex',
    flex: 1
  },
  nested: {
    paddingLeft: theme.spacing(4)
  },
  listItem: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  circleAvatar: {
    borderRadius: '50%'
  },
  circleOnline: {
    backgroundColor: theme.palette.success.light,
    width: 10,
    height: 10,
    borderRadius: '50%'
  },
  circleOffline: {
    backgroundColor: '#cbced1',
    width: 10,
    height: 10,
    borderRadius: '50%'
  },
  contentStatus: {
    width: 16,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    marginRight: 4
  },
  contentDevice: {
    display: 'flex',
    position: 'relative',
    bottom: 20,
    left: 30,
    flexDirection: 'row'
  },
  contentBadge: {
    width: 18,
    height: 18,
    borderRadius: '50%'
  },
  badgeDeviceGreen: {
    backgroundColor: '#94c400',
    display: 'flex',
    width: 18,
    height: 18,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '50%',
    color: 'white'
  },
  badgeDevice: {
    backgroundColor: '#28a7e8',
    display: 'flex',
    width: 18,
    height: 18,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '50%',
    color: 'white'
  },
  textName: {
    display: 'block',
    maxWidth: 'calc(100% - 20px)',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  textInfor: {
    display: 'block',
    maxWidth: 'calc(100% - 20px)',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  contentCouter: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  contentSelect: {
    width: 8,
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  selectedItem: {
    width: 4,
    height: '100%',
    backgroundColor: theme.palette.primary.main
  },
  memuItem: {
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex',
    flexDirection: 'row'
  }
});

export default withStyles(styles);
