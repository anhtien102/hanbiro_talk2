import React, { Component } from 'react';
import withStyles from './styles';
import { withRouter } from 'react-router-dom';
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
  MenuItem
} from '@material-ui/core';
import Folder from '@material-ui/icons/Folder';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import PropTypes from 'prop-types';
import { groupUtils, contactUtils } from '../../core/model/OrgUtils';
import { AutoSizer, List } from 'react-virtualized';
import SimpleBarHanbiro from '../SimpleBarHanbiro';
import UserPhotoView from '../UserPhoto';
import TalkIcon from '../TalkIcon';
import { saveTreeState, readTreeState } from './tree.state';
import { ipcRenderer } from 'electron';
import * as constantsApp from '../../configs/constant';
import { StatusMode } from '../../core/service/talk-constants';
import { MenuOptionContext } from '../../screens/home';
import { withTranslation } from 'react-i18next';

import { UserStatusMobileIcon, UserStatusPCIcon } from '../HanSVGIcon';

const GROUP_HEIGHT = 45;
const CONTACT_HEIGHT = 70;

const defautExpand = false;

function builVisibileTree(allTree, treeStateManager) {
  let listVisibleNode = [];
  if (allTree != null) {
    let currentLevel = -1;
    let isParentExpand = true;
    for (let i = 0; i < allTree.length; i++) {
      let node = allTree[i];
      if (contactUtils.isContactObj(node)) {
        if (node.level <= currentLevel) {
          isParentExpand = true;
          currentLevel = node.level;
          listVisibleNode.push(node);
          continue;
        }

        if (isParentExpand) {
          listVisibleNode.push(node);
          continue;
        }
      } else if (groupUtils.isGroupObj(node)) {
        if (isParentExpand) {
          listVisibleNode.push(node);
          isParentExpand = treeStateManager[node.primaryKey] ?? defautExpand;
          currentLevel = node.level;
          continue;
        }

        if (node.level <= currentLevel) {
          isParentExpand = treeStateManager[node.primaryKey] ?? defautExpand;
          currentLevel = node.level;
          listVisibleNode.push(node);
          continue;
        }
      }
    }
  }

  return listVisibleNode;
}

class OrgTree extends Component {
  constructor(props) {
    super(props);

    this.category = this.props.category;
    const prevTreeState = readTreeState(this.category);
    this.state = {
      groupOpen: prevTreeState
    };
    this.onGroupClick = this.onGroupClick.bind(this);
    this.onUserClick = this.onUserClick.bind(this);

    this.onGroupTalk = this.onGroupTalk.bind(this);
    this.onGroupWhisperCreate = this.onGroupWhisperCreate.bind(this);
    this.onContactWhisperCreate = this.onContactWhisperCreate.bind(this);
    this.changeOrgFavorite = this.changeOrgFavorite.bind(this);

    this.listRef = React.createRef();
    this.parentRef = React.createRef();

    this.simpleBar = null;

    this.curScrollTop = 0;
    this.onScroll = this.onScroll.bind(this);
  }

  static contextType = MenuOptionContext;

  static getDerivedStateFromProps(nextProps, prevState) {
    const { org } = nextProps;
    const { groupOpen } = prevState;
    const tree = builVisibileTree(org, groupOpen);
    let userKey = null;
    if (nextProps.appUI && nextProps.appUI.selected_room) {
      let selectedRoom = nextProps.appUI.selected_room[nextProps.category];
      let users = selectedRoom ? selectedRoom.users : null;
      if (users && users.length == 1) {
        userKey = users[0];
      }
    }

    if (
      !prevState.selectedUser ||
      !userKey ||
      prevState.selectedUser != userKey
    ) {
      return {
        tree: tree,
        selectedUser: userKey
      };
    }
    return {
      tree: tree
    };
  }

  componentDidMount() {
    const { appUI } = this.props;
    this.simpleBar = new SimpleBarHanbiro(this.parentRef.current);

    if (appUI) {
      let scrollData = appUI.scroll_offset;
      let scrollOffset = scrollData ? scrollData[this.category] : 0;
      this.curScrollTop = scrollOffset ?? 0;
      setImmediate(() => {
        this.listRef.current.scrollToPosition(this.curScrollTop);
        this.simpleBar.scrollTo(this.curScrollTop);
      });
    }
  }

  componentWillUnmount() {
    const { saveScrollOffset } = this.props;
    if (saveScrollOffset) {
      saveScrollOffset(this.category, this.curScrollTop);
    }
  }

  onScroll(e) {
    const { scrollTop, scrollLeft } = e.target;

    const { Grid: grid } = this.listRef.current;

    grid.handleScrollEvent({ scrollTop, scrollLeft });

    this.curScrollTop = scrollTop;
  }

  isFavorite(item) {
    const { listActualyFav } = this.props;
    const isGroup = groupUtils.isGroupObj(item);
    const key = isGroup ? '1_' + item.groupKey : '0_' + item.userKey;
    if (listActualyFav[key]) {
      return true;
    }
    return false;
  }

  onGroupClick(item, position) {
    const { groupOpen, tree } = this.state;

    let curState = groupOpen[item.primaryKey] ?? false;
    groupOpen[item.primaryKey] = !curState;
    const endOfView = position >= tree.length - 1;

    saveTreeState(groupOpen, this.category);
    this.setState(
      {
        groupOpen
      },
      () => {
        if (endOfView) {
          this.simpleBar.smoothScrollTo(this.curScrollTop + CONTACT_HEIGHT);
        }
      }
    );
  }

  onGroupTalk(item) {
    const users = groupUtils.getUserInGroup(item);
    ipcRenderer.send(
      constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
      constantsApp.ACTION_QUERY_ROOM_WITH_USER_LIST_AND_CREATE_ROOM_IF_NEED,
      {
        category: constantsApp.TAB_CATEGORY.tab_room_list,
        users: users,
        changeTab: true,
        roomName: item.displayName
      }
    );
  }

  onGroupWhisperCreate(item) {
    const users = groupUtils.getUserInGroup(item);

    this.context.onOpenWhisperDialog(users);
  }

  onContactWhisperCreate(item) {
    const users = [item];
    this.context.onOpenWhisperDialog(users);
  }

  onUserClick(item, newTab) {
    ipcRenderer.send(
      constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
      constantsApp.ACTION_QUERY_ROOM_WITH_USER_LIST_AND_CREATE_ROOM_IF_NEED,
      {
        category: this.category,
        users: [item],
        newTab: newTab
      }
    );
  }

  changeOrgFavorite(item) {
    const isGroup = groupUtils.isGroupObj(item);
    const isFav = this.isFavorite(item);
    const key = isGroup ? item.groupKey : item.userKey;
    ipcRenderer.send(
      constantsApp.SEND_SOCKET_API_EVENT,
      constantsApp.API_CHANGE_ORG_FAVORITE,
      {
        key: key,
        isGroup: isGroup,
        isAdd: !isFav
      }
    );
  }

  render() {
    const { classes, org_time_card, t, commonSettings } = this.props;
    const { groupOpen, selectedUser, tree } = this.state;

    const Row = ({ index, isScrolling, key, style }) => {
      const item = tree[index];
      let paddingTree = 8;

      if (item.level) {
        const level = item.level - 1;
        if (level == 1) {
          paddingTree = 16;
        } else {
          paddingTree = 16 + level * 8;
        }
      }

      if (groupUtils.isGroupObj(item)) {
        return (
          <Group
            style={style}
            paddingTree={paddingTree}
            key={key}
            data={item}
            groupOpen={groupOpen}
            position={index}
            classes={classes}
            onClick={this.onGroupClick}
            onOpenMenu={event => {
              const isFav = this.isFavorite(item);
              const isCustomGroup = groupUtils.isGroupCustomUser(item);
              const menuData = [
                <MenuItem
                  key="Talk"
                  onClick={() => {
                    this.context.onClose();
                    this.onGroupTalk(item);
                  }}
                >
                  <div className={classes.memuItem}>
                    <Typography variant="caption">{t('Talk')}</Typography>
                  </div>
                </MenuItem>,
                isCustomGroup ? null : (
                  <MenuItem
                    key="Add to Favorites"
                    onClick={() => {
                      this.context.onClose();
                      this.changeOrgFavorite(item);
                    }}
                  >
                    <div className={classes.memuItem}>
                      <Typography variant="caption">
                        {isFav
                          ? t('Remove from Favorite')
                          : t('Add to Favorites')}
                      </Typography>
                    </div>
                  </MenuItem>
                ),
                isCustomGroup ? (
                  <MenuItem
                    key="Rename"
                    onClick={() => {
                      this.context.onClose();
                      this.context.onOpenCreateGroupDialog(item);
                    }}
                  >
                    <div className={classes.memuItem}>
                      <Typography variant="caption">{t('Rename')}</Typography>
                    </div>
                  </MenuItem>
                ) : null,
                isCustomGroup ? (
                  <MenuItem
                    key="DeleteGroup"
                    onClick={() => {
                      this.context.onClose();
                      ipcRenderer.send(
                        constantsApp.SEND_SOCKET_API_EVENT,
                        constantsApp.API_FOLDER_DELETE,
                        { folderKey: item.groupKey }
                      );
                    }}
                  >
                    <div className={classes.memuItem}>
                      <Typography variant="caption">
                        {t('Delete group')}
                      </Typography>
                    </div>
                  </MenuItem>
                ) : null,
                isCustomGroup ? (
                  <MenuItem
                    key="EditGroup"
                    onClick={() => {
                      this.context.onClose();
                      this.context.onOpenOrgDialog(
                        newSelected => {
                          let userKeyList = [];
                          newSelected.forEach(element => {
                            userKeyList.push(element.userKey);
                          });
                          ipcRenderer.send(
                            constantsApp.SEND_SOCKET_API_EVENT,
                            constantsApp.API_FOLDER_UPDATE,
                            {
                              folderKey: item.groupKey,
                              userKeyList: userKeyList
                            }
                          );
                        },
                        item.childrenList,
                        1000,
                        true,
                        true
                      );
                    }}
                  >
                    <div className={classes.memuItem}>
                      <Typography variant="caption">
                        {t('Edit group')}
                      </Typography>
                    </div>
                  </MenuItem>
                ) : null,
                <MenuItem
                  key="Send Whisper"
                  onClick={() => {
                    this.context.onClose();
                    this.onGroupWhisperCreate(item);
                  }}
                >
                  <div className={classes.memuItem}>
                    <Typography variant="caption">
                      {t('Send Whisper')}
                    </Typography>
                  </div>
                </MenuItem>
              ];
              this.context.onOpen(event, menuData);
            }}
          />
        );
      }
      return (
        <User
          style={style}
          key={key}
          paddingTree={paddingTree}
          selectedUser={selectedUser}
          data={item}
          org_time_card={org_time_card}
          classes={classes}
          onClick={this.onUserClick}
          onOpenMenu={event => {
            const isFav = this.isFavorite(item);
            const menuData = [
              commonSettings.use_new_tab && (
                <MenuItem
                  key="Open new Tab"
                  onClick={() => {
                    this.context.onClose();
                    this.onUserClick(item, true);
                  }}
                >
                  <div className={classes.memuItem}>
                    <Typography variant="caption">
                      {t('Open new Tab')}
                    </Typography>
                  </div>
                </MenuItem>
              ),
              <MenuItem
                key="Talk"
                onClick={() => {
                  this.context.onClose();
                  this.onUserClick(item, false);
                }}
              >
                <div className={classes.memuItem}>
                  <Typography variant="caption">{t('Talk')}</Typography>
                </div>
              </MenuItem>,
              <MenuItem
                key="View Profile"
                onClick={() => {
                  this.context.onClose();
                  this.context.onOpenProfile(item);
                }}
              >
                <div className={classes.memuItem}>
                  <Typography variant="caption">{t('View Profile')}</Typography>
                </div>
              </MenuItem>,
              <MenuItem
                key="Add to Favorites"
                onClick={() => {
                  this.context.onClose();
                  this.changeOrgFavorite(item);
                }}
              >
                <div className={classes.memuItem}>
                  <Typography variant="caption">
                    {isFav ? t('Remove from Favorite') : t('Add to Favorites')}
                  </Typography>
                </div>
              </MenuItem>,
              <MenuItem
                key="Send Whisper"
                onClick={() => {
                  this.context.onClose();
                  this.onContactWhisperCreate(item);
                }}
              >
                <div className={classes.memuItem}>
                  <Typography variant="caption">{t('Send Whisper')}</Typography>
                </div>
              </MenuItem>
            ];
            this.context.onOpen(event, menuData);
          }}
        />
      );
    };

    const itemSize = ({ index }) => {
      const item = tree[index];

      if (groupUtils.isGroupObj(item)) {
        return GROUP_HEIGHT;
      }
      return CONTACT_HEIGHT;
    };

    return (
      <div className={classes.main}>
        <AutoSizer>
          {({ height, width }) => {
            return (
              <div
                ref={this.parentRef}
                onScroll={this.onScroll}
                style={{
                  position: 'relative',
                  width: width,
                  height: height
                }}
              >
                <List
                  style={{
                    overflowX: false,
                    overflowY: false,
                    outline: 'none'
                  }}
                  ref={this.listRef}
                  className="List"
                  scrollToAlignment={'start'}
                  height={height}
                  rowCount={tree.length}
                  rowHeight={itemSize}
                  width={width}
                  rowRenderer={Row}
                ></List>
              </div>
            );
          }}
        </AutoSizer>
      </div>
    );
  }
}

class Group extends Component {
  constructor(props) {
    super(props);
    this.onGroupClick = this.onGroupClick.bind(this);
  }

  onGroupClick() {
    const { data, onClick, position } = this.props;
    if (onClick) {
      onClick(data, position);
    }
  }

  render() {
    const {
      data,
      groupOpen,
      onOpenMenu,
      style,
      paddingTree,
      classes
    } = this.props;

    return (
      <ListItem
        style={{ ...style, paddingLeft: paddingTree }}
        button
        onClick={this.onGroupClick}
        // divider={true}
        onContextMenu={event => {
          event.preventDefault();
          event.stopPropagation();
          onOpenMenu(event);
        }}
      >
        <ListItemIcon style={{ minWidth: 36 }}>
          <Folder />
        </ListItemIcon>
        <ListItemText
          primary={
            <Typography variant="subtitle1" className={classes.textName}>
              {data.displayName}
            </Typography>
          }
        />
        <div className={classes.contentCouter}>
          <Typography variant="caption" color="textSecondary">
            {data.numberOnline}/{data.numberAll}
          </Typography>
          {groupOpen[data.primaryKey] ? (
            <ExpandLess color="action" />
          ) : (
            <ExpandMore color="action" />
          )}
        </div>
      </ListItem>
    );
  }
}

class User extends Component {
  constructor(props) {
    super(props);
    this.onUserClick = this.onUserClick.bind(this);
  }

  onUserClick() {
    const { data, onClick } = this.props;
    if (onClick) {
      onClick(data, false);
    }
  }

  exportIconStatus = status => {
    const { classes } = this.props;
    switch (status) {
      case StatusMode.logout:
      case StatusMode.offline:
        return <div className={classes.circleOffline} />;
      case StatusMode.available:
        return <div className={classes.circleOnline} />;
      case StatusMode.away:
        return <TalkIcon name="idle" />;
      case StatusMode.busy:
        return <TalkIcon name="busy" />;
      case StatusMode.meeting:
        return <TalkIcon name="meeting" />;
      case StatusMode.meal:
        return <TalkIcon name="meal" />;
      case StatusMode.phone:
        return <TalkIcon name="call" />;
      case StatusMode.out:
        return <TalkIcon name="out" />;
      case StatusMode.business_trip:
        return <TalkIcon name="business_trip" />;

      default:
        return null;
    }
  };

  render() {
    const {
      data,
      org_time_card,
      style,
      paddingTree,
      classes,
      selectedUser,
      onOpenMenu
    } = this.props;
    const isDualLogin = contactUtils.isDualLogin(data);
    const isPCLogin = contactUtils.isPCLogin(data);
    const isMobileLogin = contactUtils.isMobileLogin(data);
    const iconStatus = this.exportIconStatus(data.userStatus);
    const isSelected = data.userKey == selectedUser;
    let renderDeviceList;
    let renderHoliday;
    let renderBirthDay;

    if (org_time_card && org_time_card[data.userKey]) {
      renderHoliday = org_time_card[data.userKey].holiday == 1;
      renderBirthDay = org_time_card[data.userKey].birthDay == 1;
    }

    if (isDualLogin) {
      renderDeviceList = (
        <div className={classes.contentDevice}>
          <div className={classes.contentBadge}>
            <div className={classes.badgeDevice}>
              <UserStatusPCIcon style={{ fontSize: 12 }} />
            </div>
          </div>
          <div style={{ position: 'absolute', left: 12 }}>
            <div className={classes.contentBadge}>
              <div className={classes.badgeDeviceGreen}>
                <UserStatusMobileIcon style={{ fontSize: 12 }} />
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      if (isPCLogin) {
        renderDeviceList = (
          <div className={classes.contentDevice}>
            <div className={classes.contentBadge}>
              <div className={classes.badgeDevice}>
                <UserStatusPCIcon style={{ fontSize: 12 }} />
              </div>
            </div>
          </div>
        );
      }

      if (isMobileLogin) {
        renderDeviceList = (
          <div className={classes.contentDevice}>
            <div className={classes.contentBadge}>
              <div className={classes.badgeDevice}>
                <UserStatusMobileIcon style={{ fontSize: 12 }} />
              </div>
            </div>
          </div>
        );
      }
    }

    return (
      <ListItem
        style={{
          ...style,
          paddingLeft: paddingTree,
          paddingTop: 0,
          paddingBottom: 0,
          paddingRight: 0
        }}
        button
        // divider={true}
        onClick={this.onUserClick}
        onContextMenu={event => {
          event.preventDefault();
          event.stopPropagation();
          onOpenMenu(event);
        }}
        selected={isSelected}
      >
        <ListItemIcon style={{ height: 50, paddingRight: 15 }}>
          <div style={{ flexDirection: 'row', display: 'flex' }}>
            <div className={classes.contentStatus}>
              {renderHoliday ? (
                <div style={{ marginTop: 4 }}>
                  <TalkIcon name="holiday" />
                </div>
              ) : null}
              {renderBirthDay ? (
                <div style={{ marginTop: 4 }}>
                  <TalkIcon name="birthday" />
                </div>
              ) : null}
              {iconStatus}
            </div>
            <div>
              <UserPhotoView key={data.userKey} data={data} imgSize={50} />
              {renderDeviceList}
            </div>
          </div>
        </ListItemIcon>
        <ListItemText
          primary={
            <Typography variant="subtitle2" className={classes.textName}>
              {data.displayNameWithDuty}
            </Typography>
          }
          secondary={
            <Typography
              variant="caption"
              color="textSecondary"
              className={classes.textInfor}
            >
              {data.userNickName}
            </Typography>
          }
        />
        <div className={classes.contentSelect}>
          {isSelected && <div className={classes.selectedItem}></div>}
        </div>
      </ListItem>
    );
  }
}

Group.propTypes = {
  data: PropTypes.object,
  groupOpen: PropTypes.object,
  style: PropTypes.object,
  paddingTree: PropTypes.number,
  classes: PropTypes.any,
  onOpenMenu: PropTypes.func
};

Group.defaultProps = {
  data: {},
  groupOpen: {},
  style: {},
  paddingTree: 0,
  classes: null,
  onOpenMenu: () => {}
};

User.propTypes = {
  data: PropTypes.object,
  org_time_card: PropTypes.object,
  style: PropTypes.object,
  paddingTree: PropTypes.number,
  classes: PropTypes.any,
  selectedUser: PropTypes.string,
  onOpenMenu: PropTypes.func
};

User.defaultProps = {
  data: {},
  org_time_card: {},
  style: {},
  paddingTree: 0,
  classes: null,
  selectedUser: null,
  onOpenMenu: () => {}
};

OrgTree.propTypes = {
  org: PropTypes.array,
  org_time_card: PropTypes.object,
  appUI: PropTypes.object,
  category: PropTypes.string,
  saveScrollOffset: PropTypes.any
};

OrgTree.defaultProps = {
  org: [],
  org_time_card: {},
  appUI: {},
  category: '',
  saveScrollOffset: 0
};

export default withTranslation()(withRouter(withStyles(OrgTree)));
