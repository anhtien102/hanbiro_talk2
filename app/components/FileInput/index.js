import React from 'react';
import PropTypes from 'prop-types';

class FileInput extends React.Component {
  constructor(props) {
    super(props);

    this.handleUpload = this.handleUpload.bind(this);
  }

  handleUpload(evt) {
    const files = evt.target.files;
    this.props.onChange(files);
    // free up the fileInput again
    this.fileInput.value = null;
  }

  render() {
    let { style, accept } = this.props;
    return (
      <div style={style}>
        <input
          accept={accept}
          type="file"
          style={{ display: 'none' }}
          onChange={this.handleUpload}
          ref={ele => (this.fileInput = ele)}
          multiple
        />
        {React.cloneElement(this.props.children, {
          onClick: () => this.fileInput.click()
        })}
      </div>
    );
  }
}

FileInput.propTypes = {
  style: PropTypes.object,
  children: PropTypes.node.isRequired,
  onChange: PropTypes.func.isRequired,
  accept: PropTypes.string
};

export default FileInput;
