import React, { Component } from 'react';
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  Dialog,
  DialogContent,
  DialogActions,
  InputBase,
  Button,
  IconButton,
  Checkbox,
  Typography
} from '@material-ui/core';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';
import Folder from '@material-ui/icons/Folder';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import UserPhotoView from '../../components/UserPhoto';
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
import { groupUtils, contactUtils } from '../../core/model/OrgUtils';
import { AutoSizer, List } from 'react-virtualized';
import { useStyles } from './styles';
import withStyles from './style.component';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { ipcRenderer } from 'electron';
import {
  MAIN_TO_RENDER_EVENT,
  REQUEST_COMMON_ACTION_FROM_RENDER,
  ACTION_QUERY_ORG_ONLY_WITH_TEXT,
  ACTION_REPLY_QUERY_ORG_ONLY_WITH_TEXT
} from '../../configs/constant';

import SimpleBarHanbiro from '../SimpleBarHanbiro';

import searchWorker from '../../core/worker/search';

const GROUP_HEIGHT = 45;
const CONTACT_HEIGHT = 60;
const defautExpand = false;

var pSelectedUser;
function builVisibileTree(allTree, treeStateManager) {
  let listVisibleNode = [];
  if (allTree != null) {
    let currentLevel = -1;
    let isParentExpand = true;
    for (let i = 0; i < allTree.length; i++) {
      let node = allTree[i];
      if (contactUtils.isContactObj(node)) {
        if (node.level <= currentLevel) {
          isParentExpand = true;
          currentLevel = node.level;
          listVisibleNode.push(node);
          continue;
        }

        if (isParentExpand) {
          listVisibleNode.push(node);
          continue;
        }
      } else if (groupUtils.isGroupObj(node)) {
        if (isParentExpand) {
          listVisibleNode.push(node);
          isParentExpand = treeStateManager[node.primaryKey] ?? defautExpand;
          currentLevel = node.level;
          continue;
        }

        if (node.level <= currentLevel) {
          isParentExpand = treeStateManager[node.primaryKey] ?? defautExpand;
          currentLevel = node.level;
          listVisibleNode.push(node);
          continue;
        }
      }
    }
  }

  return listVisibleNode;
}

class OrgDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchFocused: false,
      search: '',
      groupOpen: {},
      selectedUser: [],
      searchResult: []
    };
    pSelectedUser = [];
    this.renderOrg = this.renderOrg.bind(this);
    this.orgRef = React.createRef();
    this.selectedScrollRef = React.createRef();

    this.onGroupClick = this.onGroupClick.bind(this);
    this.onUserClick = this.onUserClick.bind(this);
    this.onUserSearchClick = this.onUserSearchClick.bind(this);
    this.isInRoomAlready = this.isInRoomAlready.bind(this);
    this.delaySearch = null;

    this.onScroll = this.onScroll.bind(this);

    this.curScrollTop = 0;
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { org } = nextProps;
    const { groupOpen } = prevState;
    const tree = builVisibileTree(org, groupOpen);
    return {
      tree: tree
    };
  }

  onScroll(e) {
    const { scrollTop } = e.target;
    this.curScrollTop = scrollTop;
  }

  componentDidMount() {
    this.curScrollTop = 0;
    this.setState({
      selectedUser: [...this.props.listSelectedUser]
    });

    this.renderListener = (event, action, args) => {
      if (action == ACTION_REPLY_QUERY_ORG_ONLY_WITH_TEXT) {
        const data = {
          org: args.org,
          group_cached: this.props.groupCached,
          searchText: args.searchText,
          searchChosun: args.searchChosun,
          loggedUser: this.props.loggedUser
        };

        searchWorker.getSearchResultOrgOnly(data).then(result => {
          this.setState({
            searchResult: result.allSearchList
          });
        });
      }
    };

    ipcRenderer.on(MAIN_TO_RENDER_EVENT, this.renderListener);
  }

  componentWillUnmount() {
    ipcRenderer.removeListener(MAIN_TO_RENDER_EVENT, this.renderListener);
  }

  onChange(value) {
    if (value && value != '') {
      if (this.delaySearch) {
        clearTimeout(this.delaySearch);
        this.delaySearch = null;
      }
      this.delaySearch = setTimeout(() => {
        ipcRenderer.send(
          REQUEST_COMMON_ACTION_FROM_RENDER,
          ACTION_QUERY_ORG_ONLY_WITH_TEXT,
          value
        );
        this.delaySearch = null;
      }, 500);
    }

    this.setState({
      search: value
    });
  }

  onGroupClick(item, position) {
    const { groupOpen, tree } = this.state;
    let curState = groupOpen[item.primaryKey] ?? false;
    groupOpen[item.primaryKey] = !curState;

    const endOfView = position >= tree.length - 1;

    this.setState(
      {
        groupOpen
      },
      () => {
        let list = this.orgRef.current;
        if (endOfView) {
          list.scrollTop(this.curScrollTop + CONTACT_HEIGHT);
        }
      }
    );
  }

  removeElement(array, elem) {
    const index = array.indexOf(elem);
    if (index > -1) {
      array.splice(index, 1);
    }
  }

  removeAllChildGroup(group) {
    const childGroup = group.childrenList;
    if (childGroup && childGroup.length > 0) {
      for (let index = 0; index < childGroup.length; index++) {
        const element = childGroup[index];
        if (groupUtils.isGroupObj(element)) {
          this.removeAllChildGroup(element);
        } else {
          if (
            this.isInRoomAlready(element) ||
            contactUtils.isAdmin(element) ||
            contactUtils.isHiddenUser(element)
          ) {
            continue;
          }
          pSelectedUser = this.removeSelectedUser(pSelectedUser, element);
        }
      }
    }
  }

  addAllChildGroup(group) {
    const childGroup = group.childrenList;

    if (childGroup && childGroup.length > 0) {
      for (let index = 0; index < childGroup.length; index++) {
        const element = childGroup[index];
        if (groupUtils.isGroupObj(element)) {
          this.addAllChildGroup(element);
        } else {
          if (
            this.isInRoomAlready(element) ||
            contactUtils.isAdmin(element) ||
            contactUtils.isHiddenUser(element)
          ) {
            continue;
          }
          if (!this.isSelectedAlready(pSelectedUser, element)) {
            pSelectedUser.push(element);
          }
        }
      }
    }
  }

  onSelectGroup(group) {
    pSelectedUser = [...this.state.selectedUser];

    if (this.isGroupSelected(group)) {
      //remove all child of group
      this.removeAllChildGroup(group);
    } else {
      // add all child of group
      this.addAllChildGroup(group);
    }

    this.setState(
      {
        selectedUser: pSelectedUser
      },
      () => {
        this.scrollToBottom();
      }
    );
  }

  isGroupSelected(group) {
    const { selectedUser } = this.state;
    const member = group.childrenList;
    if (member && member.length > 0) {
      for (let index = 0; index < member.length; index++) {
        const element = member[index];
        if (groupUtils.isGroupObj(element)) {
          if (!this.isGroupSelected(element)) {
            return false;
          }
        } else {
          if (
            this.isInRoomAlready(element) ||
            contactUtils.isAdmin(element) ||
            contactUtils.isHiddenUser(element)
          ) {
            continue;
          }

          if (!this.isSelectedAlready(selectedUser, element)) {
            return false;
          }
        }
      }
    }
    return true;
  }

  removeSelectedUser(selectedUser, element) {
    return selectedUser.filter(item => item.primaryKey != element.primaryKey);
  }

  isSelectedAlready(selectedUser, member) {
    return selectedUser.some(check => check.userKey == member.userKey);
  }

  isInRoomAlready(member) {
    if (this.props.notAllowDisableSelected) {
      return;
    }
    const { listSelectedUser } = this.props;
    return listSelectedUser.some(item => item.userKey == member.userKey);
  }

  onUserClick(user) {
    const { selectedUser } = this.state;
    const selected = this.isSelectedAlready(selectedUser, user);
    if (selected) {
      this.setState({
        selectedUser: this.removeSelectedUser(selectedUser, user)
      });
    } else {
      selectedUser.push(user);
      this.setState(
        {
          selectedUser: selectedUser
        },
        () => {
          this.scrollToBottom();
        }
      );
    }
  }

  onUserSearchClick(user) {
    const { selectedUser } = this.state;
    const selected = this.isSelectedAlready(selectedUser, user);
    if (selected) {
      this.setState({
        selectedUser: this.removeSelectedUser(selectedUser, user)
      });
    } else {
      selectedUser.push(user);
      this.setState(
        {
          selectedUser: selectedUser
        },
        () => {
          this.scrollToBottom();
        }
      );
    }
  }

  scrollToBottom = () => {
    if (this.selectedScrollRef?.current) {
      this.selectedScrollRef.current.scrollToBottom();
    }
  };

  renderOrg() {
    const { groupOpen, selectedUser, tree } = this.state;

    const itemSize = ({ index }) => {
      const item = tree[index];
      if (groupUtils.isGroupObj(item)) {
        return GROUP_HEIGHT;
      }
      return CONTACT_HEIGHT;
    };

    const Row = ({ index, isScrolling, key, style }) => {
      const item = tree[index];
      let paddingLeft = 0;
      if (item.level) {
        paddingLeft = (item.level - 2) * 8;
      }
      if (groupUtils.isGroupObj(item)) {
        return (
          <Group
            style={style}
            paddingLeft={paddingLeft}
            key={key}
            data={item}
            groupOpen={groupOpen}
            selected={this.isGroupSelected(item)}
            onSelect={ev => {
              ev.stopPropagation();
              this.onSelectGroup(item);
            }}
            onClick={() => this.onGroupClick(item, index)}
          />
        );
      }
      const selected = selectedUser.some(i => i.primaryKey == item.primaryKey);
      return (
        <User
          style={style}
          paddingLeft={paddingLeft}
          key={key}
          selected={selected}
          disabled={this.isInRoomAlready(item)}
          data={item}
          onClick={() => this.onUserClick(item)}
        />
      );
    };

    return (
      <PerfectListWrapper
        ref={this.orgRef}
        onScroll={this.onScroll}
        tree={tree}
        itemSize={itemSize}
        Row={Row}
      ></PerfectListWrapper>
    );
  }

  renderSearchResult() {
    const { t, classes } = this.props;
    const { searchResult, selectedUser } = this.state;

    const Row = ({ index, isScrolling, key, style }) => {
      const item = searchResult[index];
      if (typeof item === 'string') {
        return (
          <div className={classes.labelNodata} key={key} style={style}>
            <span>{t(item)}</span>
          </div>
        );
      }
      const selected = selectedUser.some(i => i.primaryKey == item.primaryKey);
      return (
        <User
          style={style}
          key={key}
          selected={selected}
          showStatus={false}
          disabled={this.isInRoomAlready(item)}
          data={item}
          onClick={() => this.onUserSearchClick(item)}
        />
      );
    };

    return (
      <PerfectListSearchWrapper
        Row={Row}
        searchResult={searchResult}
      ></PerfectListSearchWrapper>
    );
  }

  render() {
    const { search, selectedUser, searchFocused } = this.state;
    const {
      classes,
      onClose,
      onSelect,
      t,
      maximum,
      listSelectedUser,
      notAllowDisableSelected,
      changeOrgDialogTitleUpdate
    } = this.props;
    return (
      <Dialog
        fullWidth={true}
        maxWidth="md"
        open={true}
        onClose={() => onClose()}
      >
        <DialogContent>
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between'
            }}
          >
            <div className={classes.search}>
              <IconButton
                size="small"
                className={
                  searchFocused
                    ? classes.searchIconHightLight
                    : classes.searchIcon
                }
              >
                <SearchIcon fontSize="small" />
              </IconButton>
              <InputBase
                onFocus={() => {
                  this.setState({ searchFocused: true });
                }}
                onBlur={() => {
                  this.setState({ searchFocused: false });
                }}
                className={classes.inputSearch}
                placeholder={searchFocused ? null : t('Search')}
                value={search}
                onChange={event => this.onChange(event.target.value)}
              />
              <IconButton
                size="small"
                className={
                  searchFocused
                    ? classes.clearIconHightLight
                    : classes.clearIcon
                }
                onClick={() => this.onChange('')}
              >
                <ClearIcon fontSize="small" />
              </IconButton>
            </div>
            <Typography variant="subtitle2" color="textSecondary">
              {selectedUser.length}/{maximum}
            </Typography>
          </div>
          <div className={classes.sizeDialog}>
            <div className={classes.contentDialog}>
              <div className={classes.contentDialogLeft}>
                <div
                  className={
                    search != '' ? classes.layoutShow : classes.layoutHidden
                  }
                >
                  {this.renderSearchResult()}
                </div>
                <div className={classes.layout}>{this.renderOrg()}</div>
              </div>

              <PerfectListSelectedWrapper
                ref={this.selectedScrollRef}
                classes={classes}
                selectedUser={selectedUser}
                isInRoomAlready={this.isInRoomAlready}
                onUserClick={this.onUserClick}
              ></PerfectListSelectedWrapper>
            </div>
          </div>
        </DialogContent>

        <DialogActions>
          <Button
            onClick={() => {
              onClose();
            }}
          >
            {t('Close')}
          </Button>
          {(notAllowDisableSelected ||
            (selectedUser.length > listSelectedUser.length &&
              selectedUser.length <= maximum)) && (
            <Button
              onClick={() => {
                onSelect(selectedUser);
              }}
              color="primary"
            >
              {changeOrgDialogTitleUpdate ? t('Apply') : t('Add')}
            </Button>
          )}
        </DialogActions>
      </Dialog>
    );
  }
}

export default withTranslation()(withStyles(OrgDialog));

class PerfectListSelectedWrapper extends Component {
  constructor(props) {
    super(props);
    this.simpleBar = null;
    this.scrollRef = React.createRef();
  }

  scrollToBottom() {
    this.simpleBar?.smoothScrollToBottom();
  }

  componentDidMount() {
    this.simpleBar = new SimpleBarHanbiro(this.scrollRef.current);
  }

  render() {
    const { classes, selectedUser, onUserClick, isInRoomAlready } = this.props;
    return (
      <div className={classes.contentDialogRightDisableScroll}>
        <div className={classes.contentSelected} ref={this.scrollRef}>
          <div className={classes.contentSelectedWrapChild}>
            {selectedUser.map(item => {
              return (
                <User
                  style={{ height: 60 }}
                  key={item?.primaryKey}
                  disabled={isInRoomAlready(item)}
                  data={item}
                  onClick={() => onUserClick(item)}
                />
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

class PerfectListWrapper extends Component {
  constructor(props) {
    super(props);
    this.onScroll = this.onScroll.bind(this);
    this.simpleBar = null;
    this.scrollRef = React.createRef();
    this.listRef = React.createRef();
  }

  onScroll(e) {
    const { scrollTop, scrollLeft } = e.target;

    const { Grid: grid } = this.listRef.current;

    grid.handleScrollEvent({ scrollTop, scrollLeft });

    const { onScroll } = this.props;
    if (onScroll) {
      onScroll(e);
    }
  }

  scrollTop(top) {
    this.simpleBar?.smoothScrollTo(top);
  }

  componentDidMount() {
    this.simpleBar = new SimpleBarHanbiro(this.scrollRef.current);
  }

  render() {
    const { tree, itemSize, Row } = this.props;
    return (
      <AutoSizer>
        {({ height, width }) => {
          return (
            <div
              ref={this.scrollRef}
              onScroll={this.onScroll}
              style={{
                position: 'relative',
                width: width,
                height: height
              }}
            >
              <List
                style={{ overflowX: false, overflowY: false, outline: 'none' }}
                ref={this.listRef}
                scrollToAlignment={'start'}
                height={height}
                rowCount={tree.length}
                rowHeight={itemSize}
                width={width}
                rowRenderer={Row}
              ></List>
            </div>
          );
        }}
      </AutoSizer>
    );
  }
}

class PerfectListSearchWrapper extends Component {
  constructor(props) {
    super(props);
    this.onScroll = this.onScroll.bind(this);
    this.simpleBar = null;
    this.scrollRef = React.createRef();
    this.listRef = React.createRef();
  }

  onScroll(e) {
    const { scrollTop, scrollLeft } = e.target;

    const { Grid: grid } = this.listRef.current;

    grid.handleScrollEvent({ scrollTop, scrollLeft });
  }

  componentDidMount() {
    this.simpleBar = new SimpleBarHanbiro(this.scrollRef.current);
  }

  render() {
    const { searchResult, Row } = this.props;
    return (
      <AutoSizer>
        {({ height, width }) => {
          return (
            <div
              ref={this.scrollRef}
              onScroll={this.onScroll}
              style={{
                position: 'relative',
                width: width,
                height: height
              }}
            >
              <List
                style={{ overflowX: false, overflowY: false, outline: 'none' }}
                ref={this.listRef}
                scrollToAlignment={'start'}
                height={height}
                rowCount={searchResult.length}
                rowHeight={60}
                width={width}
                rowRenderer={Row}
              ></List>
            </div>
          );
        }}
      </AutoSizer>
    );
  }
}

OrgDialog.propTypes = {
  org: PropTypes.array,
  open: PropTypes.bool,
  onClose: PropTypes.func,
  onSelect: PropTypes.func,
  listSelectedUser: PropTypes.array,
  maximum: PropTypes.number,
  notAllowDisableSelected: PropTypes.bool,
  changeOrgDialogTitleUpdate: PropTypes.bool
};

OrgDialog.defaultProps = {
  org: [],
  open: false,
  onClose: () => {},
  onSelect: () => {},
  listSelectedUser: [],
  maximum: 50,
  notAllowDisableSelected: false,
  changeOrgDialogTitleUpdate: false
};

function Group(props) {
  const classes = useStyles();
  const {
    data,
    selected,
    onClick,
    groupOpen,
    style,
    paddingLeft,
    onSelect
  } = props;

  return (
    <ListItem
      style={style}
      button
      // divider={true}
      onClick={onClick}
    >
      <ListItemIcon style={{ paddingLeft }}>
        <div className={classes.checkBoxContent}>
          <Checkbox
            disabled={!data.childrenList || data.childrenList.length == 0}
            onClick={onSelect}
            checked={selected}
            color="primary"
            icon={<RadioButtonUncheckedIcon />}
            checkedIcon={<RadioButtonCheckedIcon />}
          />
          <Folder />
        </div>
      </ListItemIcon>
      <ListItemText
        primary={
          <Typography variant="subtitle2" className={classes.text}>
            {data.displayName}
          </Typography>
        }
      />
      <div className={classes.counter}>
        <Typography variant="subtitle2" className={classes.text}>
          {data.numberOnline}/{data.numberAll}
        </Typography>
        {groupOpen[data.primaryKey] ? <ExpandLess /> : <ExpandMore />}
      </div>
    </ListItem>
  );
}

function User(props) {
  const classes = useStyles();

  const { data, style, selected, paddingLeft, onClick, disabled } = props;

  let detail;
  let first;
  if (!props.showStatus) {
    detail = data.fullLocation.result;
    first = data.fullLocation.first;
  }

  const displayName = data.displayNameWithDuty
    ? data.displayNameWithDuty
    : data.displayName;

  return (
    <ListItem
      style={style}
      button
      // divider={true}
      onClick={onClick}
      selected={selected}
      disabled={disabled}
    >
      <ListItemIcon style={{ paddingLeft: paddingLeft, paddingRight: 8 }}>
        <div className={classes.iconContent}>
          {selected || disabled ? (
            <div className={classes.avatarCheck}>
              <CheckIcon fontSize="large" style={{ color: 'white' }} />
            </div>
          ) : null}
          <UserPhotoView key={data?.userKey} data={data} imgSize={50} />
        </div>
      </ListItemIcon>
      <ListItemText
        primary={
          <Typography variant="subtitle2" className={classes.textUser}>
            {displayName}
          </Typography>
        }
        secondary={
          props.showStatus ? (
            <Typography variant="caption" color="textSecondary">
              {data.userNickName}
            </Typography>
          ) : (
            <Typography
              variant="caption"
              color="textSecondary"
              className={classes.textInfor}
            >
              {detail}
              <b>{first}</b>
            </Typography>
          )
        }
      />
    </ListItem>
  );
}

User.propTypes = {
  showStatus: PropTypes.bool
};

User.defaultProps = {
  showStatus: true
};
