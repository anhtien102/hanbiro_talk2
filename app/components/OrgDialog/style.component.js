import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  textName: {
    display: 'block',
    maxWidth: 'calc(100% - 20px)',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  textInfor: {
    display: 'block',
    maxWidth: 'calc(100% - 20px)',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  sizeDialog: {
    marginTop: 24,
    width: '100%',
    height: '450px'
  },
  contentDialog: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    display: 'flex'
  },
  contentDialogLeft: {
    width: '50%',
    height: '100%',
    borderWidth: 1,
    borderColor: theme.palette.divider,
    borderStyle: 'solid',
    display: 'flex',
    position: 'relative',
    flex: 1
  },
  layout: {
    zIndex: 1,
    position: 'absolute',
    backgroundColor: theme.palette.background.paper,
    width: '100%',
    height: '100%',
    display: 'flex'
  },
  layoutHidden: {
    zIndex: 0,
    position: 'absolute',
    backgroundColor: theme.palette.background.paper,
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  layoutShow: {
    zIndex: 99,
    position: 'absolute',
    backgroundColor: theme.palette.background.paper,
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  contentDialogRight: {
    width: '50%',
    height: '100%',
    display: 'flex',
    position: 'relative',
    borderWidth: 1,
    borderColor: theme.palette.divider,
    borderStyle: 'solid',
    overflowY: 'scroll',
    flexDirection: 'column'
  },
  contentDialogRightDisableScroll: {
    width: '50%',
    height: '100%',
    display: 'flex',
    position: 'relative',
    borderRightColor: theme.palette.divider,
    borderRightWidth: 1,
    borderRightStyle: 'solid',
    borderTopColor: theme.palette.divider,
    borderTopWidth: 1,
    borderTopStyle: 'solid',
    borderBottomColor: theme.palette.divider,
    borderBottomWidth: 1,
    borderBottomStyle: 'solid',
    flexDirection: 'column',
    overflow: 'hidden'
  },
  contentSelected: {
    flex: '1',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    overflow: 'auto'
  },
  contentSelectedWrapChild: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column'
  },
  labelNodata: {
    padding: 16,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  search: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: theme.palette.divider,
    borderWidth: 1,
    borderStyle: 'solid',
    padding: 4,
    width: '50%'
  },
  inputSearch: {
    fontSize: theme.typography.fontSize,
    width: '100%'
  },
  searchIcon: {
    marginLeft: 4
  },
  searchIconHightLight: {
    marginLeft: 4,
    color: theme.palette.primary.main
  },
  clearIcon: {
    marginRight: 4
  },
  clearIconHightLight: {
    marginRight: 4,
    color: theme.palette.primary.main
  }
});

export default withStyles(styles);
