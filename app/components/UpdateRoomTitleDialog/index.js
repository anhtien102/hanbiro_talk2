import React, { useState } from 'react';
import {
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  Typography,
  IconButton,
  TextField,
  FormControlLabel,
  Checkbox
} from '@material-ui/core';
import ClearOutlined from '@material-ui/icons/ClearOutlined';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

export default function UpdateRoomTitleDialog(props) {
  const { t } = useTranslation();
  const classes = useStyles();
  const { onClose, roomInfo, onUpdate } = props;

  const enablePrivateCheckBox = true;
  // rRoomNameLabelPrivate
  // rRoomNameLabelPublic

  let initTitle;
  let initChecked;
  if (enablePrivateCheckBox) {
    initTitle = roomInfo?.rRoomNameLabelPrivate;
    initChecked = false;
    if (initTitle == '') {
      initTitle = roomInfo?.rRoomNameLabelPublic;
      initChecked = true;
    }
  } else {
    initTitle = roomInfo?.rRoomNameLabelPublic;
    initChecked = true;
  }

  const [title, setTitle] = useState(initTitle);
  const [errorTitle, setErrorTitle] = useState('');
  const [checked, setChecked] = useState(initChecked);

  return (
    <Dialog fullWidth={true} maxWidth="sm" open={true} onClose={onClose}>
      <DialogContent style={{ padding: 0 }}>
        <div className={classes.dialog}>
          <div className={classes.contentTitle}>
            <Typography variant="subtitle1">
              {t('Update Room Title')}
            </Typography>
            <IconButton
              style={{ color: 'white' }}
              aria-label="delete"
              size="small"
              onClick={onClose}
            >
              <ClearOutlined fontSize="small" />
            </IconButton>
          </div>
          <div className={classes.mainContent}>
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              label={t('Title')}
              error={false}
              helperText={errorTitle}
              value={title}
              onKeyPress={ev => {
                if (ev.key === 'Enter') {
                  onUpdate(roomInfo, checked, title);
                }
              }}
              onChange={event => {
                setTitle(event.target.value);
              }}
            />
            {enablePrivateCheckBox ? (
              <FormControlLabel
                control={
                  <Checkbox
                    checked={checked}
                    onChange={event => {
                      setChecked(event.target.checked);
                    }}
                  />
                }
                label={t('Apply all member in room')}
              />
            ) : null}
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>{t('Close')}</Button>
        <Button
          onClick={() => {
            onUpdate(roomInfo, checked, title);
          }}
          color="primary"
        >
          {t('Update')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

UpdateRoomTitleDialog.propTypes = {
  open: PropTypes.bool,
  roomInfo: PropTypes.object,
  onClose: PropTypes.func,
  onUpdate: PropTypes.func
};

UpdateRoomTitleDialog.defaultProps = {
  open: false,
  roomInfo: {},
  onClose: () => {},
  onUpdate: (item, applyAll, newTitle) => {}
};
