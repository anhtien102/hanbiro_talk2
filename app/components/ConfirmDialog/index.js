import React, { useState } from 'react';
import {
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  Typography,
  IconButton,
  Switch
} from '@material-ui/core';
import ClearOutlined from '@material-ui/icons/ClearOutlined';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

export default function ConfirmDialog(props) {
  const { t } = useTranslation();
  const classes = useStyles();
  const { onClose, onExit } = props;

  return (
    <Dialog fullWidth={true} maxWidth="xs" open={true} onClose={onClose}>
      <DialogContent style={{ padding: 0 }}>
        <div className={classes.dialog}>
          <div className={classes.contentTitle}>
            <Typography variant="h6">{t('Do you want logout?')}</Typography>
            <IconButton
              style={{ color: 'white' }}
              aria-label="delete"
              size="small"
              onClick={onClose}
            >
              <ClearOutlined fontSize="small" />
            </IconButton>
          </div>
          <div className={classes.mainContent}>
            <div className={classes.settingItem}>
              <Typography variant="subtitle2">
                {t('Exit video conference room')}
              </Typography>
            </div>
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => onExit()} color="primary">
          {t('Exit')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

ConfirmDialog.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  onExit: PropTypes.func
};

ConfirmDialog.defaultProps = {
  open: false,
  onClose: () => {},
  onExit: () => {}
};
