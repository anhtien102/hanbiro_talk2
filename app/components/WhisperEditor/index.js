import React, { Component } from 'react';
import withStyles from './styles';
import CancelIcon from '@material-ui/icons/Cancel';
import { withTranslation } from 'react-i18next';
import { getImg } from '../BaseFile';
import fs from 'fs';
import { Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import ContextMenu from '../ContextMenu';
import talkAPI from '../../core/service/talk.api.render';
import 'simplebar';
import escapeLodash from 'lodash/escape';
import { ensureFilePath } from '../../configs/file_cheat';

import {
  remoteClearClipboard,
  remoteReadClipboard
} from '../../utils/electron.utils';

function convertLodash(raw) {
  const result = escapeLodash(raw);
  // console.log('convertLodash', result);
  return result;
}

function placeCaretAtEnd(el: HTMLDivElement, nonSelection = false) {
  el.focus();
  if (
    typeof window.getSelection !== 'undefined' &&
    typeof document.createRange !== 'undefined'
  ) {
    const range = document.createRange();
    range.selectNodeContents(el);
    if (nonSelection) {
      range.collapse(false);
    }
    const sel = window.getSelection();
    if (sel) {
      sel.removeAllRanges();
      sel.addRange(range);
    }
    // @ts-ignore
  } else if (typeof document.body.createTextRange !== 'undefined') {
    // @ts-ignore
    const textRange = document.body.createTextRange();
    textRange.moveToElementText(el);
    textRange.collapse(false);
    textRange.select();
  }
}
class WhisperEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShow: false,
      caretPosition: 0,
      x: 0,
      indexTab: 0,
      attachments: [],
      enterText: '',
      isFocus: false
    };
    this.lastHeight = 0;
    this.textBox = React.createRef();
    this.attachmentRef = React.createRef();
    this.onInput = this.onInput.bind(this);
    this.onRef = this.onRef.bind(this);
    this.onClear = this.onClear.bind(this);
    this.insertText = this.insertText.bind(this);
    this.onPaste = this.onPaste.bind(this);
    this.onDeleteImg = this.onDeleteImg.bind(this);
    this.clearAttachment = this.clearAttachment.bind(this);
    this.onSend = this.onSend.bind(this);
  }

  componentDidMount() {
    this.onRef();
    this.lastHeight = 0;
  }

  onRef() {
    const { onRef } = this.props;
    if (onRef) {
      onRef(this);
    }
  }

  onClear() {
    this.textBox.innerText = '';
    this.setState({
      enterText: ''
    });
  }

  clearAttachment() {
    remoteClearClipboard('clipboard');
    this.setState(
      {
        attachments: []
      },
      () => {
        this.updateHeight(true);
      }
    );
  }

  getAttachments() {
    return this.state.attachments;
  }

  getSendText() {
    return convertLodash(this.state.enterText);
  }

  insertText(text) {
    this.textBox.focus();
    document.execCommand('insertText', false, text);
  }

  makeFocus() {
    setImmediate(() => {
      this.textBox.focus();
      placeCaretAtEnd(this.textBox, true);
    });
  }

  onInput() {
    this.updateHeight();
    this.setState(
      {
        enterText: this.textBox.innerText
      },
      () => {
        const { onChange } = this.props;
        onChange(this.textBox.innerText);
      }
    );
  }

  updateHeight(force) {
    // const { onHeightChange } = this.props;
    // const { attachments } = this.state;
    // if (force || this.lastHeight != this.textBox.clientHeight) {
    //   this.lastHeight = this.textBox.clientHeight;
    //   onHeightChange(
    //     this.lastHeight +
    //       (attachments.length > 0
    //         ? this.attachmentRef.current?.clientHeight
    //         : 0)
    //   );
    // }
  }

  onPaste(e) {
    // cancel paste
    e.preventDefault();

    //get image in clip board
    let clipboardImage = remoteReadClipboard('clipboard');
    let imageBuffer = null;
    if (clipboardImage) {
      imageBuffer = clipboardImage.toPNG();
    }

    if (imageBuffer && imageBuffer.length > 0 && this.props.allowFileTransfer) {
      let now = Date.now();
      let filePath = talkAPI.tempPathWithName(`${now}.png`);
      fs.writeFile(filePath, imageBuffer, () => {
        const { attachments } = this.state;
        this.setState({
          attachments: [
            ...attachments,
            {
              id: now,
              name: 'image.png',
              path: filePath,
              size: imageBuffer.length
            }
          ]
        });
      });
    } else {
      // get text representation of clipboard
      var text = (e.originalEvent || e).clipboardData.getData('text/plain');
      // insert text manually
      document.execCommand('insertText', false, text);
    }
  }

  addAttachment(files) {
    let now = Date.now();
    files.forEach(element => {
      element.id = now++;
    });
    const { attachments } = this.state;
    this.setState(
      {
        attachments: [...attachments, ...files]
      },
      () => {
        this.updateHeight(true);
      }
    );
  }

  onDeleteImg(itemDelete) {
    this.setState(
      {
        attachments: this.state.attachments.filter(
          item => item.id != itemDelete.id
        )
      },
      () => {
        this.updateHeight(true);
      }
    );
  }

  onSend(enterText) {
    let { attachments } = this.state;
    if (
      this.props.onSend &&
      (enterText || (attachments && attachments.length > 0))
    ) {
      this.props.onSend(convertLodash(enterText), attachments);
    }
  }

  isPhoto(fileName) {
    const photos = ['jpg', 'png', 'gif', 'jpeg'];
    const name = fileName.slice(((fileName.lastIndexOf('.') - 1) >>> 0) + 2);
    return photos.includes(name);
  }

  render() {
    let { attachments, enterText, isFocus } = this.state;
    const { classes, t, minHeight, sending, showSendButton } = this.props;
    const margin = showSendButton ? 20 : 0;
    return (
      <div
        className={isFocus ? classes.inputFocus : classes.inputNotFocus}
        style={{
          marginLeft: margin,
          marginRight: margin,
          marginBottom: margin
        }}
      >
        <ContextMenu
          contextId="textBox"
          onCut={() => {
            document.execCommand('cut', false);
          }}
          onCopy={() => {
            document.execCommand('copy', false);
          }}
          onPaste={() => {
            navigator.clipboard.readText().then(clipText => {
              if (clipText) {
                document.execCommand('insertText', false, clipText);
              }
            });
          }}
        />

        <div className={classes.content}>
          <div
            className={classes.richInputContainer}
            onClick={() => {
              this.textBox?.focus();
            }}
            data-simplebar
            style={{ flex: 1, minHeight: minHeight }}
          >
            <div
              className={classes.richInput}
              onPaste={this.onPaste}
              onCut={this.handleTag}
              ref={ref => (this.textBox = ref)}
              id="textBox"
              contentEditable={true}
              suppressContentEditableWarning={true}
              spellCheck={false}
              onInput={this.onInput}
              onFocus={() => this.setState({ isFocus: true })}
              onBlur={() => this.setState({ isFocus: false })}
              placeholder={t('Enter a message')}
            />
          </div>

          {sending ? (
            <div className={classes.sendingLoading}>
              <div className="han-loading mini" />
            </div>
          ) : showSendButton && (enterText || attachments.length > 0) ? (
            <div className={classes.chatSend}>
              <a
                className={classes.chatSendTxt}
                onClick={() => this.onSend(enterText)}
              >
                <Typography variant="overline">{t('SEND')}</Typography>
              </a>
            </div>
          ) : null}
        </div>

        {attachments.length > 0 ? (
          <div ref={this.attachmentRef} className={classes.viewCopyImg}>
            <div style={{ display: 'flex' }}>
              {attachments.map((item, index) => (
                <div key={index.toString()} className={classes.contentCopyImg}>
                  <span
                    className={classes.iconCopyImg}
                    onClick={() => this.onDeleteImg(item)}
                  >
                    <CancelIcon />
                  </span>
                  {this.isPhoto(item.name) ? (
                    <img
                      className={classes.imgCopy}
                      src={ensureFilePath(item.path)}
                    />
                  ) : (
                    <div className={classes.fileIconContent}>
                      <img
                        src={getImg(item.name.split('.').pop())}
                        style={{
                          width: 60,
                          height: 60
                        }}
                      />
                    </div>
                  )}
                  <div className={classes.nameImgCopy}>{item.name}</div>
                </div>
              ))}
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

WhisperEditor.propTypes = {
  onChange: PropTypes.func,
  onRef: PropTypes.func,
  onSend: PropTypes.func,
  onHeightChange: PropTypes.func,
  minHeight: PropTypes.number,
  sending: PropTypes.bool,
  showSendButton: PropTypes.bool,
  allowFileTransfer: PropTypes.bool
};

WhisperEditor.defaultProps = {
  onHeightChange: height => {},
  onChange: () => {},
  onRef: () => {},
  onSend: (text, attachments) => {},
  minHeight: 200,
  sending: false,
  showSendButton: true,
  allowFileTransfer: true
};

export default withTranslation()(withStyles(WhisperEditor));
