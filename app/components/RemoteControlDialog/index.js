import React, { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import {
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  Typography,
  IconButton
} from '@material-ui/core';
import { desktopCapturer } from 'electron';
import ClearOutlined from '@material-ui/icons/ClearOutlined';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import useRemoteControl, {
  generateRemoteRoomID,
  RemoteStatus
} from '../../janus_client/src/app/remote-control';
import 'simplebar';

import talkApi from '../../core/service/talk.api.render';
import * as api from '../../janus_client/src/app/api/index';
import http from '../../janus_client/src/app/api/http';

const haveWebRTCInformation = () => {
  const webRTCInfo = talkApi.webrtcInformation();
  return (
    webRTCInfo && webRTCInfo.rtcserver != null && webRTCInfo.rtcserver != ''
  );
};

const ensureWebRTCInformation = () => {
  const webRTCInfo = talkApi.webrtcInformation();
  return {
    useWebSocket: webRTCInfo.method == 'https' ? false : true,
    serverRTC: webRTCInfo.rtcserver,
    httpPortRTC: webRTCInfo.httpsport,
    wsPortRTC: webRTCInfo.wssport,
    restPort: webRTCInfo.api,
    restProtocol: 'https',
    platform: 'electron',
    version: webRTCInfo.version
  };
};

export default function RemoteControlDialog(props) {
  const { t } = useTranslation();
  const classes = useStyles();
  const { onClose } = props;

  const user_cached = useSelector(state => state.company.user_cached);

  const remoteRoomIdRef = useRef();
  const keepUrlRef = useRef([]);
  const startingRef = useRef(false);
  const [isStart, setIsStart] = useState(false);

  const {
    success,
    loading,
    message,
    init,
    roomSetup,
    closeSession,
    setLoading,
    setMessage
  } = useRemoteControl({ isOwner: true });

  const webrtcConfig = ensureWebRTCInformation();

  const myUserKey = talkApi.shareObj?.account_info?.user_key;
  const contact = myUserKey ? user_cached[myUserKey] : null;
  const displayName = contact
    ? contact.displayName
    : talkApi.shareObj?.account_info?.user_id;

  useEffect(() => {
    return () => {
      closeSession();
      api.cleanUp({ urls: keepUrlRef.current });
    };
  }, []);

  useEffect(() => {
    if (
      message &&
      message != '' &&
      message != RemoteStatus.starting &&
      message != RemoteStatus.success_waiting_connection
    ) {
      startingRef.current = false;
      setIsStart(false);
    }
  }, [message]);

  const onScreenShare = async (constraints, callback) => {
    try {
      const newSource = await desktopCapturer.getSources({
        types: ['screen']
      });

      if (newSource.length <= 0) {
        callback?.(null);
        setLoading(false);
        setMessage(RemoteStatus.connect_remote_failed);
        closeSession();
      }

      const selected = newSource[0];

      const stream = await navigator.mediaDevices.getUserMedia({
        audio: false,
        video: {
          mandatory: {
            chromeMediaSource: 'desktop',
            chromeMediaSourceId: selected?.id,
            maxWidth: screen.availWidth,
            maxHeight: screen.availHeight,
            maxFrameRate: 10
          }
        }
      });
      callback?.(stream);
    } catch (error) {
      console.log('error', error);
      callback?.(null);
      setLoading(false);
      setMessage(RemoteStatus.connect_remote_failed);
      closeSession();
    }
  };

  /**
   * @description authenticate gw and init janus
   */
  const getInfo = async () => {
    let server = `wss://${webrtcConfig.serverRTC}:${webrtcConfig.wsPortRTC}/ws`;
    if (!webrtcConfig.wsPortRTC) {
      server = `wss://${webrtcConfig.serverRTC}/ws`;
    }
    if (!webrtcConfig.useWebSocket) {
      server = `https://${webrtcConfig.serverRTC}:${webrtcConfig.httpPortRTC}/janus`;
    }
    try {
      await http.setupConfig({
        protocol: webrtcConfig.restProtocol,
        domain: webrtcConfig.serverRTC,
        port: webrtcConfig.restPort
      });
      const reponseToken = await api.getJanusToken({
        params: {
          authkey: talkApi.newAutoLoginKey,
          domain: talkApi.domain
        }
      });

      const remoteRoomID = generateRemoteRoomID();
      remoteRoomIdRef.current = remoteRoomID;

      init({
        server: server,
        token: reponseToken.token,
        room: remoteRoomID,
        name: displayName,
        onScreenShare: onScreenShare,
        userKey: myUserKey
      });
    } catch (error) {
      console.log(error);
      setLoading(false);
      setMessage('authenticate_fail');
      closeSession();
    }
  };

  return (
    <Dialog fullWidth={true} maxWidth="sm" open={true} onClose={onClose}>
      <DialogContent style={{ padding: 0, overflow: 'hidden' }}>
        <div className={classes.dialog}>
          <div className={classes.contentTitle}>
            <Typography variant="h6">{t('Share remote')}</Typography>
            <IconButton
              style={{ color: 'white' }}
              aria-label="delete"
              size="small"
              onClick={onClose}
            >
              <ClearOutlined fontSize="small" />
            </IconButton>
          </div>
          <div>
            <Typography
              variant="subtitle2"
              style={{
                marginRight: 16,
                marginTop: 10,
                textAlign: 'center'
              }}
            >
              {t(message)}
            </Typography>
            <Button
              variant="outlined"
              onClick={() => {
                if (startingRef.current) {
                  api.cleanUp({ urls: keepUrlRef.current });
                  setIsStart(true);
                  return;
                }
                startingRef.current = true;
                api.cleanUp({ urls: keepUrlRef.current });
                setMessage(RemoteStatus.starting);
                setIsStart(true);
                getInfo();
              }}
            >
              {isStart ? t('Stop') : t('Start')}
            </Button>
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>{t('Close')}</Button>
      </DialogActions>
    </Dialog>
  );
}

RemoteControlDialog.propTypes = {
  onClose: PropTypes.func
};

RemoteControlDialog.defaultProps = {
  onClose: () => {}
};
