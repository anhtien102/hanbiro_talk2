import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    dialog: {
      width: '100%',
      flexDirection: 'column',
      display: 'flex'
    },
    contentTitle: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      display: 'flex',
      padding: 4,
      paddingLeft: 8,
      color: 'white',
      backgroundColor:
        theme.palette.type == 'light'
          ? theme.palette.hanbiroColor.dialogTitle
          : theme.palette.divider
    },
    mainContent: {
      width: '100%',
      height: '100%',
      flexDirection: 'column',
      display: 'flex',
      borderTopColor: theme.palette.divider,
      borderBottomColor: theme.palette.divider,
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderTopStyle: 'solid',
      borderBottomStyle: 'solid',
      padding: 16
    },
    photoContainer: {
      width: '100%',
      textAlign: 'center',
      marginBottom: 10
    },
    descItemBold: {
      fontSize: 16,
      fontWeight: 800
    },
    descItem: {
      padding: 5,
      display: 'inline-block'
    },
    descItemLabel: {
      width: '25%',
      fontWeight: 800,
      paddingRight: 10,
      display: 'inline-block'
    },
    descItemValue: {
      display: 'inline-block'
    },
    descItemLinkValue: {
      color:
        theme.palette.type == 'light'
          ? theme.palette.hanbiroColor.textMsgLinkOther
          : '#ffffff',

      userDrag: 'none'
    }
  })
);
