import React, { useState } from 'react';
import {
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  Typography,
  IconButton
} from '@material-ui/core';
import ClearOutlined from '@material-ui/icons/ClearOutlined';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

import UserPhotoView from '../UserPhoto';

export default function ProfileDialog(props) {
  const { t } = useTranslation();
  const classes = useStyles();
  const { onClose, item } = props;

  const mapUser = user => {
    if (user.loginid) {
      return {
        userKey: user.userKey,
        displayNameWithDuty: user.username,
        userNickName: user.nickname,
        userMobilePhone: user.mobilephone,
        userLocalPhone: user.localphone
      };
    }
    return user;
  };

  const ensureNull = input => {
    return input && input != '' ? input : null;
  };

  const realUser = mapUser(item);

  const status = ensureNull(realUser.userNickName);
  const email = ensureNull(realUser.userEmail);
  const mobiphone = ensureNull(realUser.userMobilePhone);
  const telephone = ensureNull(realUser.userLocalPhone);

  const groupName = realUser.parentGroup?.displayName;

  return (
    <Dialog fullWidth={true} maxWidth="xs" open={true} onClose={onClose}>
      <DialogContent style={{ padding: 0 }}>
        <div className={classes.dialog}>
          <div className={classes.contentTitle}>
            <Typography variant="subtitle1"></Typography>
            <IconButton
              style={{ color: 'white' }}
              aria-label="delete"
              size="small"
              onClick={onClose}
            >
              <ClearOutlined fontSize="small" />
            </IconButton>
          </div>
          <div className={classes.mainContent}>
            <div className={classes.photoContainer}>
              <UserPhotoView
                style={{ width: 100, height: 100 }}
                key={realUser.userKey}
                userKeyData={realUser.userKey}
                data={realUser}
                imgSize={100}
              />
              <div className={classes.descItemBold}>
                {realUser.displayNameWithDuty}
              </div>
            </div>

            {groupName != null && groupName != '' ? (
              <div className={classes.descItem}>
                <div className={classes.descItemLabel}>{t('Department')}</div>
                <div className={classes.descItemValue}>{groupName}</div>
              </div>
            ) : null}

            <div className={classes.descItem}>
              <div className={classes.descItemLabel}>{t('Status')}</div>
              <div className={classes.descItemValue}>{status ?? 'N/A'}</div>
            </div>
            <div className={classes.descItem}>
              <div className={classes.descItemLabel}>{t('Email')}</div>
              <div className={classes.descItemValue}>
                {email ? (
                  <a
                    className={classes.descItemLinkValue}
                    href={`mailto:${email}`}
                  >
                    {email}
                  </a>
                ) : (
                  'N/A'
                )}
              </div>
            </div>
            <div className={classes.descItem}>
              <div className={classes.descItemLabel}>{t('Mobiphone')}</div>
              <div className={classes.descItemValue}>
                {mobiphone ? (
                  <a
                    className={classes.descItemLinkValue}
                    href={`tel:${mobiphone}`}
                  >
                    {mobiphone}
                  </a>
                ) : (
                  'N/A'
                )}
              </div>
            </div>

            <div className={classes.descItem}>
              <div className={classes.descItemLabel}>{t('Telephone')}</div>
              <div className={classes.descItemValue}>
                {telephone ? (
                  <a
                    className={classes.descItemLinkValue}
                    href={`tel:${telephone}`}
                  >
                    {telephone}
                  </a>
                ) : (
                  'N/A'
                )}
              </div>
            </div>
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>{t('Close')}</Button>
      </DialogActions>
    </Dialog>
  );
}

ProfileDialog.propTypes = {
  item: PropTypes.object,
  onClose: PropTypes.func
};

ProfileDialog.defaultProps = {
  item: {},
  onClose: () => {}
};
