import React from 'react';
import { useStyles } from './styles';
import { Typography } from '@material-ui/core';
import PropTypes from 'prop-types';

function LineStatus(props) {
  const { message, visible, enableBackground } = props;
  const classes = useStyles();
  return visible ? (
    <div className={classes.chatDate}>
      <Typography variant="caption" className={classes.spanDate}>
        {message}
      </Typography>
    </div>
  ) : null;
}

LineStatus.propTypes = {
  message: PropTypes.string,
  visible: PropTypes.bool,
  enableBackground: PropTypes.bool
};

LineStatus.defaultProps = {
  message: '',
  visible: true,
  enableBackground: true
};

export default LineStatus;
