import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    chatDate: {
      color: 'white',
      contain: 'content',
      margin: '10px 10px 16px 10px',
      fontSize: '13px',
      justifyContent: 'center',
      display: '-webkit-flex',
      display: 'flex',
      position: 'relative',
      '&::before': {
        content: "' '",
        width: '100%',
        height: '0px',
        borderTop: 1,
        borderTopStyle: 'solid',
        borderTopColor: theme.palette.divider,
        zIndex: 0,
        marginTop: '10px',
        marginLeft: '50px'
      },
      '&::after': {
        content: "' '",
        width: '100%',
        height: '0px',
        borderTop: 1,
        borderTopStyle: 'solid',
        borderTopColor: theme.palette.divider,
        zIndex: 0,
        marginTop: '10px'
      }
    },
    spanDate: {
      color: theme.palette.type == 'light' ? '#999999' : 'white',
      padding: '3px 10px',
      flex: '0 0 auto'
    }
  })
);
