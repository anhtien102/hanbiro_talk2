import React from 'react';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';

export default function Typing(props) {
  const { message, visible } = props;
  const classes = useStyles();
  return visible ? (
    <div className={classes.viewTyping}>
      <div className={classes.contentTyping}>
        <Typography
          variant="caption"
          color="textSecondary"
          className={classes.textMessage}
        >
          {message}
        </Typography>
      </div>
    </div>
  ) : null;
}
Typing.propTypes = {
  visible: PropTypes.bool,
  message: PropTypes.string
};

Typing.defaultProps = {
  visible: false,
  message: ''
};
