import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    viewTyping: {
      // position: 'absolute',
      // bottom: 0,
      zIndex: 4
    },
    contentTyping: {
      // backgroundColor: theme.palette.grey[300],
      // padding: '4px 16px',
      // borderTopLeftRadius: '8px',
      // borderTopRightRadius: '8px',
      // display: 'inline'
    },
    textMessage: {
      color: theme.palette.type == 'light' ? theme.palette.grey[400] : '#fff'
    }
  })
);
