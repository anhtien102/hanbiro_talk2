class InputController {
  constructor() {
    this.inputs = {};
  }

  saveInput(key, msg) {
    this.inputs[key] = msg;
  }

  getInput(key) {
    return this.inputs[key] ?? '';
  }
}

const inputController = new InputController();
export default inputController;
