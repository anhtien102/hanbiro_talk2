import React, { Component } from 'react';
import withStyles from './styles';
import UserPhotoView from '../../../UserPhoto';
import CloseIcon from '@material-ui/icons/Close';
import * as Utils from '../../../../utils';
import CancelIcon from '@material-ui/icons/Cancel';
import { withTranslation } from 'react-i18next';
import {
  remoteClearClipboard,
  remoteReadClipboard
} from '../../../../utils/electron.utils';
import talkAPI from '../../../../core/service/talk.api.render';
import { ensureFilePath } from '../../../../configs/file_cheat';
import fs from 'fs';
import {
  MenuItem,
  ListItemText,
  ListItemAvatar,
  Typography
} from '@material-ui/core';
import PropTypes from 'prop-types';
import 'simplebar';
import ContextMenu from '../../../ContextMenu';

import inputController from './input.controller';

function placeCaretAtEnd(el: HTMLDivElement, nonSelection = false) {
  el.focus();
  if (
    typeof window.getSelection !== 'undefined' &&
    typeof document.createRange !== 'undefined'
  ) {
    const range = document.createRange();
    range.selectNodeContents(el);
    if (nonSelection) {
      range.collapse(false);
    }
    const sel = window.getSelection();
    if (sel) {
      sel.removeAllRanges();
      sel.addRange(range);
    }
    // @ts-ignore
  } else if (typeof document.body.createTextRange !== 'undefined') {
    // @ts-ignore
    const textRange = document.body.createTextRange();
    textRange.moveToElementText(el);
    textRange.collapse(false);
    textRange.select();
  }
}
class TextEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShow: false,
      users: props.users,
      caretPosition: 0,
      x: 0,
      indexTab: 0,
      imgs: [],
      enterText: '',
      isFocus: false
    };
    this.listUser = React.createRef();
    this.textBox = React.createRef();
    this.handleTag = this.handleTag.bind(this);
    this.onSelected = this.onSelected.bind(this);
    this.onInput = this.onInput.bind(this);
    this.onRef = this.onRef.bind(this);
    this.onClear = this.onClear.bind(this);
    this.insertText = this.insertText.bind(this);
    this.handleKey = this.handleKey.bind(this);
    this.checkKey = this.checkKey.bind(this);
    this.handleTagDown = this.handleTagDown.bind(this);
    this.onPaste = this.onPaste.bind(this);
    this.onClearQuote = this.onClearQuote.bind(this);
    this.onDeleteImg = this.onDeleteImg.bind(this);
    this.clearAttachment = this.clearAttachment.bind(this);
    this.onKeyPressWithDataInput = this.onKeyPressWithDataInput.bind(this);
    this.onSend = this.onSend.bind(this);
  }

  componentDidMount() {
    this.onRef();
    const { primaryKey } = this.props;

    if (primaryKey != null && primaryKey != '') {
      const msg = inputController.getInput(primaryKey);
      this.setState({ enterText: msg }, () => {
        this.textBox.innerText = msg;
      });
    }
  }

  componentDidUpdate(prevProps) {
    const { primaryKey } = this.props;
    if (prevProps.primaryKey != primaryKey) {
      if (prevProps.primaryKey != null && prevProps.primaryKey != '') {
        inputController.saveInput(prevProps.primaryKey, this.state.enterText);
      }
    }
  }

  onRef() {
    const { onRef } = this.props;
    if (onRef) {
      onRef(this);
    }
  }

  onClear() {
    this.textBox.innerText = '';
    this.setState({
      enterText: ''
    });
  }

  clearAttachment() {
    remoteClearClipboard('clipboard');
    this.setState({
      imgs: []
    });
  }

  insertText(text) {
    this.textBox.focus();
    document.execCommand('insertText', false, text);
  }

  makeFocus() {
    setImmediate(() => {
      this.textBox.focus();
      placeCaretAtEnd(this.textBox, true);
    });
  }

  /**
   * Handle action when user press Tab button
   * @param {*} option
   */
  onSelected(option) {
    let { word } = this.state;
    let sel = this.getUserSelection();
    let rangeObject = this.getRangeObject(sel);
    var range = document.createRange();
    let wordString = rangeObject.commonAncestorContainer.textContent;
    let wordSub = wordString.substring(0, rangeObject.endOffset + 1);
    let startIndex = wordSub.lastIndexOf('@');
    if (startIndex > -1) {
      range.setStart(
        rangeObject.commonAncestorContainer,
        rangeObject.endOffset - word.length // start from @ last
      );
      range.setEnd(rangeObject.commonAncestorContainer, rangeObject.endOffset);
      sel.removeAllRanges();
      sel.addRange(range);
      document.execCommand('delete', false);
      let tag = (word.length == 0 ? '@' + option : option) + ': ';

      document.execCommand('insertText', false, tag);
      this.setState({ isShow: false });
    }
  }

  /**
   * Get range of element bold
   * @param {*} selectionObject
   */
  getRangeObject(selectionObject) {
    if (selectionObject.getRangeAt) {
      return selectionObject.getRangeAt(0);
    } else {
      // Safari!
      var range = document.createRange();
      range.setStart(selectionObject.anchorNode, selectionObject.anchorOffset);
      range.setEnd(selectionObject.focusNode, selectionObject.focusOffset);
      return range;
    }
  }

  /**
   * Get element bold
   */
  getUserSelection() {
    var userSelection;
    if (window.getSelection) {
      userSelection = window.getSelection();
    } else if (document.selection) {
      // should come last; Opera!
      userSelection = document.selection.createRange();
    }
    return userSelection;
  }

  /**
   * Filter list user when user is start tag user
   * @param {*} e
   */
  handleKey(e) {
    let { users } = this.props;

    let sel = this.getUserSelection();
    if (sel.rangeCount == 0) {
      e.preventDefault();
      return false;
    }
    let rangeObject = this.getRangeObject(sel);
    let wordString = rangeObject.commonAncestorContainer.textContent;
    let wordSub = wordString.substring(0, rangeObject.endOffset);
    var index =
      wordSub.lastIndexOf('@') != -1
        ? wordSub.lastIndexOf('@')
        : wordSub.length;

    var prIndex = index - 1;
    var ntIndex = index + 1;
    const prCharCode =
      prIndex >= 0 && prIndex < wordSub.length
        ? wordSub.charCodeAt(prIndex)
        : -1;
    const ntCharCode =
      ntIndex < wordString.length ? wordString.charCodeAt(ntIndex) : -1;

    var notIgnore =
      (prCharCode == -1 || prCharCode == 160 || prCharCode == 32) &&
      ntCharCode != 64;
    // console.log(prCharCode, ntCharCode, notIgnore);

    let keyword = wordSub.substring(index, wordSub.length);
    let maxY = this.textBox.offsetWidth - 290;
    let xCurrent = (rangeObject.endOffset * 6) % this.textBox.offsetWidth;
    let x = xCurrent < maxY ? xCurrent : maxY;

    if (keyword.indexOf('@') == 0 && notIgnore) {
      var keywordNew = keyword.replace('@', '');
      let isShow = false;
      let userDataNew = [];
      if (!keywordNew.startsWith(' ')) {
        userDataNew = users.filter(user => {
          if (user.userId < 0) {
            return false;
          }
          let fullName = user.displayName ? user.displayName.toLowerCase() : '';
          let name = keywordNew.toLowerCase();
          return fullName.includes(name);
        });

        isShow = userDataNew.length > 0;
      }

      this.setState({
        isShow: isShow,
        users: userDataNew,
        word: keywordNew,
        x: x
      });
    } else {
      this.setState({
        isShow: false,
        word: keywordNew,
        x: x
      });
    }
  }

  /**
   * Handle key up for Tab, move up, move down
   * @param {*} e
   */
  checkKey(e) {
    switch (e.keyCode) {
      case 9: // Press Tab
      case 13: // Press Enter chose user
        e.preventDefault();
        break;
      case 27: // Press Esc
        e.preventDefault();
        break;
      case 38: // Press move up
        e.preventDefault();
        break;
      case 40: // Press move down
        e.preventDefault();
        break;
      default:
        this.setState({
          indexTab: 0
        });
        this.handleKey(e);
        break;
    }
  }

  handleTag(e) {
    this.checkKey(e);
  }

  /**
   * Handle key down for Tab, move up, move down
   * @param {*} e
   */
  handleTagDown(e) {
    let { indexTab, isShow, enterText } = this.state;

    const onMove = step => {
      let newIndexTab = this.state.indexTab + step;
      let indexTab = 0;
      if (newIndexTab < 0) {
        indexTab = this.state.users.length - 1;
      } else if (newIndexTab > this.state.users.length - 1) {
        indexTab = 0;
      } else {
        indexTab = newIndexTab;
      }
      this.refs[`user_${indexTab}`].focus();
      this.textBox.focus();
      // this.listUser.scroll(50, 50);
      this.setState({
        indexTab: indexTab
      });
    };

    switch (e.keyCode) {
      case 9: // Press Tab
        e.preventDefault();
        isShow && this.onSelected(this.refs[`user_${indexTab}`].innerText);
        break;
      case 13: // Press Enter chose user
        if (!e.shiftKey && !e.altKey) {
          e.preventDefault();
          if (isShow) {
            this.onSelected(this.refs[`user_${indexTab}`].innerText);
          } else {
            if (!this.handleKeyPressEnter(e)) {
              this.onSend(enterText);
            }
          }
        }

        break;
      case 27: // Press Esc
        e.preventDefault();
        this.setState({ isShow: false });
        break;
      case 38: // Press move up
        if (isShow) {
          e.preventDefault();
          onMove(-1);
        }

        break;
      case 40: // Press move down
        if (isShow) {
          e.preventDefault();
          onMove(1);
        }

        break;
      default:
        // e.preventDefault();
        // this.checkKey(e);
        break;
    }
  }

  onInput() {
    this.setState(
      {
        enterText: this.textBox.innerText
      },
      () => {
        const { onChange } = this.props;
        onChange(this.textBox.innerText);
      }
    );
  }

  onPaste(e) {
    // cancel paste
    e.preventDefault();

    //get image in clip board
    let clipboardImage = remoteReadClipboard('clipboard');
    let imageBuffer = null;
    if (clipboardImage) {
      imageBuffer = clipboardImage.toPNG();
    }

    if (imageBuffer && imageBuffer.length > 0 && this.props.allowFileTransfer) {
      let now = Date.now();
      let filePath = talkAPI.tempPathWithName(`${now}.png`);
      fs.writeFile(filePath, imageBuffer, () => {
        this.setState({
          imgs: [
            {
              id: now,
              name: 'image.png',
              path: filePath,
              size: imageBuffer.length
            }
          ]
        });
      });
    } else {
      // get text representation of clipboard
      var text = (e.originalEvent || e).clipboardData.getData('text/plain');
      // insert text manually
      document.execCommand('insertText', false, text);
    }
  }

  onClearQuote() {
    if (this.props.onClearQuote) {
      this.props.onClearQuote({ userName: '', msgBody: '' });
    }
  }

  onDeleteImg(itemDelete) {
    fs.unlink(itemDelete.path, err => {});
    this.setState({
      imgs: this.state.imgs.filter(item => item.id != itemDelete.id)
    });
  }

  handleKeyPressEnter(event) {
    if (event.key === 'Enter' && (event.altKey || event.shiftKey)) {
      // event.preventDefault();
      // this.insertText('\r\n');
      // this.textBox.scroll(0, this.textBox.scrollHeight);
      return true;
    }
    return false;
  }

  onKeyPressWithDataInput(event) {
    this.handleKeyPressEnter(event);
  }

  onSend(enterText) {
    let { imgs } = this.state;
    if (this.props.onSend && enterText) {
      this.props.onSend(enterText);
      this.onClear();
    }
    // Send image clipboard
    if (imgs && imgs.length > 0) {
      if (this.props.onSendFiles) {
        this.props.onSendFiles(imgs);
      }
      this.clearAttachment();
    }
  }

  render() {
    let { isShow, x, users, indexTab, imgs, enterText, isFocus } = this.state;
    const { classes, msgData, t } = this.props;
    let { userName, msgBody } = msgData;

    return (
      <div className={isFocus ? classes.inputFocus : classes.inputNotFocus}>
        <ContextMenu
          contextId="textBox"
          onCut={() => {
            document.execCommand('cut', false);
          }}
          onCopy={() => {
            document.execCommand('copy', false);
          }}
          onPaste={() => {
            navigator.clipboard.readText().then(clipText => {
              if (clipText) {
                document.execCommand('insertText', false, clipText);
              }
            });
          }}
        />
        {msgBody ? (
          <div className={classes.quote}>
            {/* <div className={classes.quoteTitle}>{userName}</div> */}
            <div className={classes.quoteContent}>
              <div className={classes.quoteBody}>
                {Utils.renderQuote(userName, msgBody)}
              </div>
            </div>
            <div
              style={{
                display: 'inline',
                position: 'absolute',
                top: 10,
                right: 5
              }}
            >
              <CloseIcon
                style={{ fontSize: '14px', cursor: 'pointer' }}
                onClick={this.onClearQuote}
              />
            </div>
          </div>
        ) : null}
        <div className={classes.content}>
          <div
            onClick={() => {
              this.textBox?.focus();
            }}
            className={classes.richInputContainer}
            data-simplebar
            style={{ flex: 1 }}
          >
            <div
              className={classes.richInput}
              onKeyUp={this.handleTag}
              onKeyDown={this.handleTagDown}
              onPaste={this.onPaste}
              onCut={this.handleTag}
              onMouseUp={this.handleTag}
              style={{ width: '100%' }}
              ref={ref => (this.textBox = ref)}
              id="textBox"
              contentEditable={true}
              suppressContentEditableWarning={true}
              spellCheck={false}
              onInput={this.onInput}
              onKeyPress={this.onKeyPressWithDataInput}
              onFocus={() => this.setState({ isFocus: true })}
              onBlur={() => this.setState({ isFocus: false })}
              placeholder={t(
                'Input @, press ↑ or ↓ key to move, press Tab key to choose'
              )}
            />
          </div>
          <div className={classes.chatSend}>
            {enterText || imgs.length > 0 ? (
              <a
                className={classes.chatSendTxt}
                onClick={() => this.onSend(enterText)}
              >
                <Typography variant="overline">{t('SEND')}</Typography>
              </a>
            ) : null}
          </div>
        </div>
        <div
          ref={ref => (this.listUser = ref)}
          className={classes.userSuggestContent}
          style={{ display: isShow ? 'block' : 'none', left: x + 'px' }}
        >
          {users.map((user, index) => (
            <MenuItem
              id={`user_${index}`}
              ref={`user_${index}`}
              key={user.userKey}
              onClick={() => this.onSelected(user.displayName)}
              tabIndex={index}
              classes={{
                root:
                  indexTab == index ? classes.userActive : classes.userNormal
              }}
            >
              <ListItemAvatar>
                <UserPhotoView
                  style={{ width: 35, height: 35 }}
                  key={user.userKey}
                  userKeyData={user.userKey}
                  data={user}
                  imgSize={50}
                />
              </ListItemAvatar>
              <ListItemText primary={user.displayName} />
            </MenuItem>
          ))}
        </div>
        {imgs.length > 0 ? (
          <div className={classes.viewCopyImg}>
            <div style={{ display: 'flex' }}>
              {imgs.map((item, index) => (
                <div key={index.toString()} className={classes.contentCopyImg}>
                  <span
                    className={classes.iconCopyImg}
                    onClick={() => this.onDeleteImg(item)}
                  >
                    <CancelIcon />
                  </span>
                  <img
                    className={classes.imgCopy}
                    src={ensureFilePath(item.path)}
                  />
                  <div className={classes.nameImgCopy}>{item.name}</div>
                </div>
              ))}
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

TextEditor.propTypes = {
  msgData: PropTypes.object,
  users: PropTypes.array,
  onChange: PropTypes.func,
  onRef: PropTypes.func,
  onClearQuote: PropTypes.func,
  onSend: PropTypes.func,
  onSendFiles: PropTypes.func,
  primaryKey: PropTypes.string,
  allowFileTransfer: PropTypes.bool
};

TextEditor.defaultProps = {
  msgData: {
    userName: '',
    msgBody: ''
  },
  users: [],
  onChange: () => {},
  onRef: () => {},
  onClearQuote: () => {},
  onSend: () => {},
  onSendFiles: () => {},
  primaryKey: '',
  allowFileTransfer: true
};

export default withTranslation()(withStyles(TextEditor));
