import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    btn: {
      boxShadow:
        '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
      backgroundColor: theme.palette.common.white,
      width: 32,
      height: 32,
      padding: 0,
      position: 'absolute',
      right: 15,
      bottom: 15,
      zIndex: 9,
      '&:hover': {
        backgroundColor: theme.palette.common.white
      }
    },
    icon: {
      fontSize: 25,
      color: 'gray',
      '&:hover': {
        color: theme.palette.primary.main
      }
    }
  })
);
