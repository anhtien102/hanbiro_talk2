import React from 'react';
import { useStyles } from './styles';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import PropTypes from 'prop-types';

export default function ButtonBottom(props) {
  const { onClick, visible } = props;
  const classes = useStyles();
  return visible ? (
    <div>
      <IconButton classes={{ root: classes.btn }} onClick={onClick}>
        <KeyboardArrowDownIcon className={classes.icon} />
      </IconButton>
    </div>
  ) : null;
}
ButtonBottom.propTypes = {
  visible: PropTypes.bool,
  onClick: PropTypes.func
};

ButtonBottom.defaultProps = {
  visible: false,
  onClick: () => {}
};
