import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    viewUpdating: {
      position: 'absolute',
      top: 1,
      left: 10,
      right: 10,
      backgroundColor: theme.palette.grey[400],
      borderRadius: 10,
      zIndex: 5
    },
    contentUpdating: {
      justifyContent: 'center',
      alignItems: 'cemter',
      display: 'flex',
      padding: 4
    },
    toastMessage: {
      color: theme.palette.grey[800]
    }
  })
);
