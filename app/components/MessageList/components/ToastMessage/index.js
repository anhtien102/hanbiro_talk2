import React from 'react';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';

export default function ToastMessage(props) {
  const { message, visible, onPress, action } = props;
  const classes = useStyles();
  return visible ? (
    <div className={classes.viewUpdating} onClick={onPress}>
      <div className={classes.contentUpdating}>
        <Typography variant="subtitle2" className={classes.toastMessage}>
          {message}
        </Typography>
        {action}
      </div>
    </div>
  ) : null;
}
ToastMessage.propTypes = {
  visible: PropTypes.bool,
  message: PropTypes.string,
  onPress: PropTypes.func,
  action: PropTypes.node
};

ToastMessage.defaultProps = {
  visible: false,
  message: '',
  action: null,
  onPress: () => {
    console.log('AAAA');
  }
};
