import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    drag: {
      borderStyle: 'dashed',
      borderWidth: 3,
      borderColor: theme.palette.primary.main,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: '#f0f8ff',
      position: 'absolute',
      zIndex: 4
    },
    dragOutside: {
      borderStyle: 'dashed',
      borderWidth: 3,
      borderColor: '#bdbdbd',
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: '#f0f8ff',
      position: 'absolute',
      zIndex: 4
    },
    dragDisable: {
      border: 'dashed #bdbdbd 3px',
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: theme.palette.background.paper,
      position: 'absolute',
      zIndex: 4
    },
    dragContent: {
      position: 'absolute',
      top: '50%',
      right: 0,
      left: 0,
      textAlign: 'center',
      color: 'grey',
      fontSize: 30
    },
    textDragActive: {
      color: theme.palette.primary.main
    },
    textDragInActive: {
      color: '#bdbdbd'
    }
  })
);
