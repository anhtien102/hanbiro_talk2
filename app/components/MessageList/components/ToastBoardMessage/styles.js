import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    viewContainer: {
      position: 'absolute',
      top: 1,
      left: 10,
      right: 10,
      zIndex: 5
    },
    viewUpdating: {
      backgroundColor: theme.palette.grey[300],
      borderRadius: 10
    },
    viewFloatContainer: {
      position: 'absolute',
      top: 0,
      right: 10,
      zIndex: 5
    },
    contentUpdating: {
      cursor: 'pointer',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      padding: 4
    },
    alarmIcon: {
      color: theme.palette.primary.main
    },
    startIcon: {
      ...theme.typography,
      fontSize: theme.typography.body2
    },
    toastContainer: {
      flex: 1,
      display: 'flex',
      flexDirection: 'column',
      paddingLeft: 10
    },
    toastTitle: {
      fontWeight: 700,
      color: theme.palette.grey[800]
    },
    toastStatus: {
      paddingLeft: 5,
      fontStyle: 'italic',
      color: theme.palette.grey[800]
    },
    toastMessage: {
      color: theme.palette.grey[800],
      maxHeight: 80,
      overflow: 'hidden',
      wordBreak: 'break-all'
    }
  })
);
