import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Typography, IconButton, Button } from '@material-ui/core';
import ClearOutlined from '@material-ui/icons/ClearOutlined';
import {
  ChatBoardAlarmIcon,
  MinimizeIcon,
  MaximizeIcon
} from '../../../HanSVGIcon';
import talkApi from '../../../../core/service/talk.api.render';

export default function ToastBoardMessage(props) {
  const { items, visible, onClose, onOpenDetail } = props;
  const userCached = useSelector(state => state.company.user_cached);
  const [isFloating, setIsFloating] = useState(false);
  const classes = useStyles();
  const { t } = useTranslation();

  const renderDefault = () => {
    return (
      <div className={classes.viewContainer}>
        <div className={classes.viewUpdating}>
          {items.map(item => {
            const user = userCached[item.userKey] ?? {};
            return (
              <div
                key={item.itemKey}
                className={classes.contentUpdating}
                onClick={() => {
                  onOpenDetail?.(item);
                }}
              >
                <ChatBoardAlarmIcon className={classes.alarmIcon} />
                <div className={classes.toastContainer}>
                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center'
                    }}
                  >
                    <Typography
                      variant="subtitle2"
                      className={classes.toastTitle}
                    >
                      {user.displayName}
                    </Typography>
                    <Typography
                      variant="caption"
                      className={classes.toastStatus}
                    >
                      {moment(item.time).format(
                        talkApi.dateTimeFormatWithTime()
                      )}
                    </Typography>
                  </div>

                  <Typography
                    variant="caption"
                    className={classes.toastMessage}
                  >
                    {item.bodyMessage}
                  </Typography>
                </div>

                <IconButton
                  className={classes.alarmIcon}
                  style={{ marginLeft: 10 }}
                  onClick={() => {
                    onClose?.(item);
                  }}
                  aria-label="delete"
                  size="small"
                >
                  <ClearOutlined fontSize="small" />
                </IconButton>
              </div>
            );
          })}
        </div>
        <div style={{ paddingTop: 3 }}>
          <Button
            startIcon={<MinimizeIcon style={{ fontSize: 'inherit' }} />}
            variant="outlined"
            style={{
              position: 'absolute',
              right: 5,
              borderRadius: 40,
              textTransform: 'capitalize',
              paddingTop: 0,
              paddingBottom: 0
            }}
            onClick={() => {
              setIsFloating(true);
            }}
            color="primary"
          >
            {t('Hide')}
          </Button>
        </div>
      </div>
    );
  };

  const renderFloating = () => {
    return (
      <div className={classes.viewFloatContainer} style={{ paddingTop: 3 }}>
        <Button
          startIcon={<MaximizeIcon style={{ fontSize: 'inherit' }} />}
          variant="outlined"
          onClick={() => {
            setIsFloating(false);
          }}
          style={{
            borderRadius: 40,
            textTransform: 'capitalize',
            paddingTop: 0,
            paddingBottom: 0
          }}
          color="primary"
        >
          {t('Show')}
        </Button>
      </div>
    );
  };

  return visible ? (isFloating ? renderFloating() : renderDefault()) : null;
}
ToastBoardMessage.propTypes = {
  visible: PropTypes.bool,
  items: PropTypes.array,
  onClose: PropTypes.func,
  onOpenDetail: PropTypes.func
};

ToastBoardMessage.defaultProps = {
  visible: false,
  items: [],
  onClose: item => {},
  onOpenDetail: item => {}
};
