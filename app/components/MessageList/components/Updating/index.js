import React from 'react';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';

export default function Typing(props) {
  const { message, visible, atBottom } = props;
  const classes = useStyles();
  return visible ? (
    <div
      className={atBottom ? classes.viewUpdatingBottom : classes.viewUpdating}
    >
      <div className={classes.contentUpdating}>{message}</div>
    </div>
  ) : null;
}
Typing.propTypes = {
  visible: PropTypes.bool,
  message: PropTypes.string,
  atBottom: PropTypes.bool
};

Typing.defaultProps = {
  visible: false,
  message: '',
  atBottom: false
};
