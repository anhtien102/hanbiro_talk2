import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    viewUpdating: {
      left: '50%',
      position: 'absolute',
      top: 0,
      height: 35,
      width: 'auto',
      width: 180,
      zIndex: 4
    },
    viewUpdatingBottom: {
      left: '50%',
      position: 'absolute',
      height: 35,
      bottom: 0,
      width: 'auto',
      width: 180,
      zIndex: 4
    },
    contentUpdating: {
      ...theme.typography,
      backgroundColor: theme.palette.primary.main,
      color: 'white',
      textAlign: 'center',
      position: 'absolute',
      top: 2,
      left: '-50%',
      padding: '4px 20px',
      borderRadius: '16px'
    }
  })
);
