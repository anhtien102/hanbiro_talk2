import React, { useState } from 'react';
import {
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  Typography,
  IconButton,
  Tabs,
  Tab,
  AppBar
} from '@material-ui/core';
import CheckCircle from '@material-ui/icons/CheckCircle';
import ClearOutlined from '@material-ui/icons/ClearOutlined';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import 'simplebar';

export default function ShareScreenDialog(props) {
  const { t } = useTranslation();
  const classes = useStyles();
  const { onClose, onSelect, sources } = props;
  const [selected, setSelected] = useState(null);
  const [indexTab, setTab] = React.useState(0);

  const handleChange = (event, newValue) => {
    setTab(newValue);
  };

  const renderContent = () => {
    if (indexTab == 0) {
      return (
        <div className={classes.mainContent}>
          {sources.map((item, index) => {
            if (item.id?.includes('screen')) {
              return (
                <div key={`thump${index}`} className={classes.thump}>
                  <img
                    style={{ width: '100%', height: '100%' }}
                    src={item.thumbnail.toDataURL()}
                    onClick={() => setSelected(item)}
                  />
                  <div className={classes.title}>{item.name ?? ''}</div>
                  {selected == item && (
                    <div className={classes.icon}>
                      <CheckCircle fontSize="small" color="primary" />
                    </div>
                  )}
                </div>
              );
            }
          })}
        </div>
      );
    } else {
      return (
        <div className={classes.mainContent}>
          {sources.map((item, index) => {
            if (item.id?.includes('window')) {
              return (
                <div key={`thump${index}`} className={classes.thump}>
                  <img
                    style={{ width: '100%', height: '100%' }}
                    src={item.thumbnail.toDataURL()}
                    onClick={() => setSelected(item)}
                  />
                  <div className={classes.title}>{item.name ?? ''}</div>
                  {selected == item && (
                    <div className={classes.icon}>
                      <CheckCircle fontSize="small" color="primary" />
                    </div>
                  )}
                </div>
              );
            }
          })}
        </div>
      );
    }
  };
  return (
    <Dialog fullWidth={true} maxWidth="sm" open={true} onClose={onClose}>
      <DialogContent style={{ padding: 0, overflow: 'hidden' }}>
        <div className={classes.dialog}>
          <div className={classes.contentTitle}>
            <Typography variant="h6">{t('Choose Screen Share')}</Typography>
            <IconButton
              style={{ color: 'white' }}
              aria-label="delete"
              size="small"
              onClick={onClose}
            >
              <ClearOutlined fontSize="small" />
            </IconButton>
          </div>
          <AppBar position="static" color="default">
            <Tabs
              value={indexTab}
              onChange={handleChange}
              indicatorColor="primary"
              textColor="primary"
              variant="fullWidth"
              centered
            >
              <Tab label={t('Screen')} />
              <Tab label={t('Windows')} />
            </Tabs>
          </AppBar>

          {renderContent()}
          {sources?.length == 0 && (
            <div
              style={{
                display: 'flex',
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <div className="han-loading mini" />
            </div>
          )}
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>{t('Close')}</Button>
        {selected && (
          <Button onClick={() => onSelect(selected)} color="primary">
            {t('Choose')}
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
}

ShareScreenDialog.propTypes = {
  sources: PropTypes.array,
  onClose: PropTypes.func,
  onSelect: PropTypes.func
};

ShareScreenDialog.defaultProps = {
  sources: [],
  onClose: () => {},
  onSelect: () => {}
};
