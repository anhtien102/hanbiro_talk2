import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    dialog: {
      height: 400,
      width: '100%',
      flexDirection: 'column',
      display: 'flex'
    },
    contentTitle: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      display: 'flex',
      padding: 4,
      paddingLeft: 8,
      color: 'white',
      backgroundColor:
        theme.palette.type == 'light'
          ? theme.palette.hanbiroColor.dialogTitle
          : theme.palette.divider
    },
    mainContent: {
      overflow: 'auto',
      flex: 1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      display: 'flex'
    },
    thump: {
      display: 'flex',
      borderWidth: 1,
      borderRadius: 4,
      borderColor: theme.palette.divider,
      margin: 8,
      width: 180,
      height: 120,
      overflow: 'hidden',
      position: 'relative'
    },
    title: {
      position: 'absolute',
      padding: 2,
      bottom: 0,
      display: 'flex',
      backgroundColor: 'black',
      fontSize: 10,
      zIndex: 99,
      textOverflow: 'ellipsis',
      maxLines: 1,
      color: 'white',
      overflow: 'hidden',
      width: '100%',
      display: 'inline-block',
      whiteSpace: 'nowrap',
      textAlign: 'center'
    },
    icon: {
      position: 'absolute',
      top: 0,
      right: 0,
      zIndex: 99
    }
  })
);
