import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { openLinkWithURL } from '../../utils/electron.utils';

export default class HtmlViewer extends Component {
  constructor(props) {
    super(props);
    this.removeChildBackgrounds = this.removeChildBackgrounds.bind(this);
    this.elRef = React.createRef();
  }

  removeChildBackgrounds(element) {
    var children = element.childNodes;
    for (var i = 0; i < children.length; i++) {
      var node = children[i];
      if (node.nodeName !== '#text') {
        if (node.style) {
          node.style.backgroundColor = 'transparent';
        }

        if (node.hasChildNodes()) {
          this.removeChildBackgrounds(node);
        }
      }
    }
  }

  componentDidMount() {
    const el = this.elRef.current;
    this.removeChildBackgrounds(el);
    el.addEventListener('click', e => {
      const href = e.target.href;
      console.log(href, e.target.text);
      e.preventDefault();
      if (href?.startsWith('http://') || href?.startsWith('https://')) {
        openLinkWithURL(e.target.href);
      }
    });
  }

  render() {
    const { htmlString } = this.props;
    return (
      <div ref={this.elRef}>
        {
          <div
            dangerouslySetInnerHTML={{
              __html: htmlString ?? ''
            }}
          />
        }
      </div>
    );
  }
}

HtmlViewer.defaultProps = {
  htmlString: null
};

HtmlViewer.propTypes = {
  htmlString: PropTypes.string
};
