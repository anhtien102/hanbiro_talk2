import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    textName: {
      display: 'block',
      maxWidth: 'calc(100% - 20px)',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    textInfor: {
      display: 'block',
      maxWidth: 'calc(100% - 20px)',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    search: {
      display: 'flex',
      alignItems: 'center',
      borderRadius: 30,
      backgroundColor: '#EAEAEA',
      '&:hover': {
        backgroundColor: 'white',
        boxShadow: '0 0 0 1px rgba(0, 0, 0, 0.2);'
      },
      flexDirection: 'row',
      width: '50%'
    },
    searchIcon: {
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      color: '#959595',
      padding: 4
    },
    sizeDialog: {
      marginTop: 24,
      width: '100%',
      height: '450px'
    },
    contentDialog: {
      width: '100%',
      height: '100%',
      flexDirection: 'row',
      display: 'flex'
    },
    contentDialogLeft: {
      width: '50%',
      height: '100%',
      borderWidth: 1,
      borderColor: theme.palette.divider,
      borderStyle: 'solid',
      display: 'flex'
    },
    contentDialogRight: {
      width: '50%',
      height: '100%',
      borderWidth: 1,
      borderColor: theme.palette.divider,
      borderStyle: 'solid',
      display: 'flex',
      overflow: 'scroll',
      flexDirection: 'column'
    },
    checkBoxContent: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      paddingRight: 16
    },
    text: {
      display: 'block',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    iconContent: {
      width: 50,
      height: 50,
      position: 'relative'
    },
    counter: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center'
    },
    avatarCheck: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      backgroundColor: theme.palette.primary.main + '7F',
      position: 'absolute',
      zIndex: 10
    }
  })
);
