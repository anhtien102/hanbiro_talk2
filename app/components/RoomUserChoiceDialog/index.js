import React, { Component } from 'react';
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  Dialog,
  DialogContent,
  DialogActions,
  InputBase,
  Button,
  IconButton,
  Typography,
  FormControlLabel
} from '@material-ui/core';
import { CSSCheckBoxStyleDL } from '../../screens/login/component.styles';
import ClearOutlined from '@material-ui/icons/ClearOutlined';
import Linkify from 'linkifyjs/react';

import Folder from '@material-ui/icons/Folder';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import UserPhotoView from '../../components/UserPhoto';
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
import { groupUtils, contactUtils } from '../../core/model/OrgUtils';
import { AutoSizer, List } from 'react-virtualized';
import { useStyles } from './styles';
import withStyles from './style.component';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { ipcRenderer } from 'electron';
import {
  MAIN_TO_RENDER_EVENT,
  REQUEST_COMMON_ACTION_FROM_RENDER,
  ACTION_QUERY_ORG_ONLY_WITH_TEXT,
  ACTION_REPLY_QUERY_ORG_ONLY_WITH_TEXT
} from '../../configs/constant';
import SimpleBarHanbiro from '../SimpleBarHanbiro';
import * as Utils from '../../utils';
import RoomInfor from '../RoomInfor';
import searchWorker from '../../core/worker/search';
import { getImg } from '../BaseFile';
import { openLink } from '../../utils/electron.utils';

import { ensureFilePath } from '../../configs/file_cheat';

const GROUP_HEIGHT = 45;
const CONTACT_HEIGHT = 60;
const defautExpand = false;

function builVisibileTree(allTree, treeStateManager) {
  let listVisibleNode = [];
  if (allTree != null) {
    let currentLevel = -1;
    let isParentExpand = true;
    for (let i = 0; i < allTree.length; i++) {
      let node = allTree[i];
      if (contactUtils.isContactObj(node)) {
        if (node.level <= currentLevel) {
          isParentExpand = true;
          currentLevel = node.level;
          listVisibleNode.push(node);
          continue;
        }

        if (isParentExpand) {
          listVisibleNode.push(node);
          continue;
        }
      } else if (groupUtils.isGroupObj(node)) {
        if (isParentExpand) {
          listVisibleNode.push(node);
          isParentExpand = treeStateManager[node.primaryKey] ?? defautExpand;
          currentLevel = node.level;
          continue;
        }

        if (node.level <= currentLevel) {
          isParentExpand = treeStateManager[node.primaryKey] ?? defautExpand;
          currentLevel = node.level;
          listVisibleNode.push(node);
          continue;
        }
      }
    }
  }

  return listVisibleNode;
}

class RoomUserChoiceDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchFocused: false,
      search: '',
      groupOpen: {},
      selectedUser: [],
      searchResult: [],
      includeSenderName: false
    };

    this.renderOrg = this.renderOrg.bind(this);
    this.orgRef = React.createRef();

    this.onGroupClick = this.onGroupClick.bind(this);
    this.onUserClick = this.onUserClick.bind(this);
    this.onUserSearchClick = this.onUserSearchClick.bind(this);
    this.onRoomClick = this.onRoomClick.bind(this);
    this.delaySearch = null;

    this.onScroll = this.onScroll.bind(this);

    this.curScrollTop = 0;
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { org } = nextProps;
    const { groupOpen } = prevState;
    const tree = builVisibileTree(org, groupOpen);
    return {
      tree: tree
    };
  }

  onScroll(e) {
    const { scrollTop } = e.target;
    this.curScrollTop = scrollTop;
  }

  componentDidMount() {
    this.curScrollTop = 0;
    this.setState({
      includeSenderName:
        localStorage.getItem('include_display_name') == 'true' ? true : false
    });

    this.renderListener = (event, action, args) => {
      if (action == ACTION_REPLY_QUERY_ORG_ONLY_WITH_TEXT) {
        const data = {
          org: args.org,
          group_cached: this.props.groupCached,
          searchText: args.searchText,
          searchChosun: args.searchChosun,
          loggedUser: this.props.loggedUser
        };

        searchWorker.getSearchResultOrgOnly(data).then(result => {
          this.setState({
            searchResult: result.allSearchList
          });
        });
      }
    };

    ipcRenderer.on(MAIN_TO_RENDER_EVENT, this.renderListener);
  }

  componentWillUnmount() {
    ipcRenderer.removeListener(MAIN_TO_RENDER_EVENT, this.renderListener);
  }

  onChange(value) {
    if (value && value != '') {
      if (this.delaySearch) {
        clearTimeout(this.delaySearch);
        this.delaySearch = null;
      }
      this.delaySearch = setTimeout(() => {
        ipcRenderer.send(
          REQUEST_COMMON_ACTION_FROM_RENDER,
          ACTION_QUERY_ORG_ONLY_WITH_TEXT,
          value
        );
        this.delaySearch = null;
      }, 500);
    }

    this.setState({
      search: value
    });
  }

  onGroupClick(item, position) {
    const { groupOpen, tree } = this.state;
    let curState = groupOpen[item.primaryKey] ?? false;
    groupOpen[item.primaryKey] = !curState;

    const endOfView = position >= tree.length - 1;

    this.setState(
      {
        groupOpen
      },
      () => {
        let list = this.orgRef.current;
        if (endOfView) {
          list.scrollTop(this.curScrollTop + CONTACT_HEIGHT);
        }
      }
    );
  }

  removeSelectedUser(selectedUser, element) {
    return selectedUser.filter(item => item.primaryKey != element.primaryKey);
  }

  isSelectedAlready(selectedUser, member) {
    return selectedUser.some(check => check.userKey == member.userKey);
  }

  isRoomSelectedAlready(selectedUser, member) {
    return selectedUser.some(check => check.primaryKey == member.primaryKey);
  }

  onUserClick(user) {
    const { selectedUser } = this.state;
    const selected = this.isSelectedAlready(selectedUser, user);
    if (selected) {
      this.setState({
        selectedUser: this.removeSelectedUser(selectedUser, user)
      });
    } else {
      this.setState({
        selectedUser: [user]
      });
    }
  }

  onUserSearchClick(user) {
    const { selectedUser } = this.state;
    const selected = this.isSelectedAlready(selectedUser, user);
    if (selected) {
      this.setState({
        selectedUser: this.removeSelectedUser(selectedUser, user)
      });
    } else {
      this.setState({
        selectedUser: [user]
      });
    }
  }

  renderOrg() {
    const { groupOpen, selectedUser, tree } = this.state;

    const itemSize = ({ index }) => {
      const item = tree[index];
      if (groupUtils.isGroupObj(item)) {
        return GROUP_HEIGHT;
      }
      return CONTACT_HEIGHT;
    };

    const Row = ({ index, isScrolling, key, style }) => {
      const item = tree[index];
      let paddingLeft = 0;
      if (item.level) {
        paddingLeft = (item.level - 2) * 8;
      }
      if (groupUtils.isGroupObj(item)) {
        return (
          <Group
            style={style}
            paddingLeft={paddingLeft}
            key={key}
            data={item}
            groupOpen={groupOpen}
            onClick={() => this.onGroupClick(item, index)}
          />
        );
      }
      const selected = selectedUser.some(i => i.primaryKey == item.primaryKey);
      return (
        <User
          style={style}
          paddingLeft={paddingLeft}
          key={key}
          selected={selected}
          data={item}
          onClick={() => this.onUserClick(item)}
        />
      );
    };

    return (
      <PerfectListWrapper
        ref={this.orgRef}
        onScroll={this.onScroll}
        tree={tree}
        itemSize={itemSize}
        Row={Row}
      ></PerfectListWrapper>
    );
  }

  onRoomClick(room) {
    room.primaryKey = room.primaryKey ? room.primaryKey : room.rRoomKey;

    const { selectedUser } = this.state;
    const selected = this.isRoomSelectedAlready(selectedUser, room);

    if (selected) {
      this.setState({
        selectedUser: this.removeSelectedUser(selectedUser, room)
      });
    } else {
      this.setState({
        selectedUser: [room]
      });
    }
  }

  renderRoomList() {
    const { rooms, user, user_cached } = this.props;
    const { selectedUser } = this.state;

    const itemSize = ({ index }) => {
      return 70;
    };

    const Row = ({ index, isScrolling, key, style }) => {
      const item = rooms[index];
      const selected = selectedUser.find(i => i.primaryKey == item.primaryKey);
      // console.log(selected);
      return (
        <RoomInfor
          selectedRoom={selected}
          style={style}
          key={key}
          data={item}
          user={user}
          user_cached={user_cached}
          onClick={() => this.onRoomClick(item)}
        />
      );
    };

    return (
      <PerfectListWrapper
        tree={rooms}
        itemSize={itemSize}
        Row={Row}
      ></PerfectListWrapper>
    );
  }

  renderSearchResult() {
    const { t, classes } = this.props;
    const { searchResult, selectedUser } = this.state;

    const Row = ({ index, isScrolling, key, style }) => {
      const item = searchResult[index];
      if (typeof item === 'string') {
        return (
          <div className={classes.labelNodata} key={key} style={style}>
            <span>{t(item)}</span>
          </div>
        );
      }
      const selected = selectedUser.some(i => i.primaryKey == item.primaryKey);
      return (
        <User
          style={style}
          key={key}
          selected={selected}
          showStatus={false}
          data={item}
          onClick={() => this.onUserSearchClick(item)}
        />
      );
    };

    return (
      <PerfectListSearchWrapper
        Row={Row}
        searchResult={searchResult}
      ></PerfectListSearchWrapper>
    );
  }

  renderMsgInfo(dataObj, userDisplayName) {
    const { classes } = this.props;
    const { data, filePath, isPhoto, thumbPath } = dataObj;

    if (data && data.msgdtFileName && data.msgdtFileSize) {
      let fileThumbPath = null;
      if (isPhoto) {
        fileThumbPath = thumbPath;
      }

      return (
        <div>
          <img
            style={{ maxHeight: 100, maxWidth: 100 }}
            src={
              fileThumbPath
                ? ensureFilePath(fileThumbPath)
                : getImg(data.msgdtFileName.split('.').pop())
            }
            alt=""
            className={classes.icoFile}
          />
          <div
            style={{
              display: 'inline-block',
              position: 'absolute',
              margin: 10
            }}
          >
            <Typography variant="subtitle2">{data.msgdtFileName}</Typography>
            <Typography variant="caption" color="textSecondary">
              {Utils.convertFileSize(data.msgdtFileSize)}
            </Typography>
          </div>
        </div>
      );
    }
    return (
      <Linkify
        options={{
          attributes: {
            onClick: openLink
          },
          className: classes.colorLink
        }}
      >
        <Typography variant="body2">
          {this.state.includeSenderName
            ? `@${userDisplayName}: ${data.msgBody}`
            : data.msgBody}
        </Typography>
      </Linkify>
    );
  }

  render() {
    const { search, selectedUser, searchFocused } = this.state;
    const {
      classes,
      onClose,
      onSelect,
      t,
      maximum,
      dataObj,
      user_cached
    } = this.props;

    let userDisplayName = null;
    let userKey = [dataObj?.data?.msgUserKey];
    if (userKey) {
      userDisplayName = user_cached?.[userKey]?.displayName;
    }
    return (
      <Dialog
        fullWidth={true}
        maxWidth="md"
        open={true}
        onClose={() => onClose()}
      >
        <DialogContent style={{ padding: 0, overflow: 'hidden' }}>
          <div className={classes.dialog}>
            <div className={classes.contentTitle}>
              <Typography variant="subtitle1">{t('Share')}</Typography>
              <IconButton
                style={{ color: 'white' }}
                aria-label="delete"
                size="small"
                onClick={onClose}
              >
                <ClearOutlined fontSize="small" />
              </IconButton>
            </div>
            <div className={classes.mainContent}>
              <div data-simplebar className={classes.msgInfoContainer}>
                {this.renderMsgInfo(dataObj, userDisplayName)}
              </div>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between'
                }}
              >
                <div className={classes.search}>
                  <IconButton
                    size="small"
                    className={
                      searchFocused
                        ? classes.searchIconHightLight
                        : classes.searchIcon
                    }
                  >
                    <SearchIcon fontSize="small" />
                  </IconButton>
                  <InputBase
                    className={classes.inputSearch}
                    onFocus={() => {
                      this.setState({ searchFocused: true });
                    }}
                    onBlur={() => {
                      this.setState({ searchFocused: false });
                    }}
                    placeholder={searchFocused ? null : t('Search')}
                    value={search}
                    onChange={event => this.onChange(event.target.value)}
                  />
                  <IconButton
                    size="small"
                    className={
                      searchFocused
                        ? classes.clearIconHightLight
                        : classes.clearIcon
                    }
                    onClick={() => this.onChange('')}
                  >
                    <ClearIcon fontSize="small" />
                  </IconButton>
                </div>
                {userDisplayName && userDisplayName?.length > 0 ? (
                  <FormControlLabel
                    checked={this.state.includeSenderName}
                    classes={{
                      label: classes.label
                    }}
                    control={
                      <CSSCheckBoxStyleDL
                        color="primary"
                        onChange={event => {
                          localStorage.setItem(
                            'include_display_name',
                            event.target.checked
                          );
                          this.setState({
                            includeSenderName: event.target.checked
                          });
                        }}
                      />
                    }
                    label={t('Include sender name')}
                  />
                ) : null}
                <Typography variant="subtitle2" color="textSecondary">
                  {selectedUser.length}/{maximum}
                </Typography>
              </div>
              <div className={classes.sizeDialog}>
                <div className={classes.contentDialog}>
                  <div className={classes.contentDialogLeft}>
                    <div
                      className={
                        search != '' ? classes.layoutShow : classes.layoutHidden
                      }
                    >
                      {this.renderSearchResult()}
                    </div>
                    <div className={classes.layout}>{this.renderOrg()}</div>
                  </div>
                  <div className={classes.contentDialogRightDisableScroll}>
                    {this.renderRoomList()}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </DialogContent>

        <DialogActions>
          <Button
            onClick={() => {
              onClose();
            }}
          >
            {t('Close')}
          </Button>
          {selectedUser.length > 0 && selectedUser.length <= maximum && (
            <Button
              onClick={() => {
                let dataObjNew = { ...dataObj };
                if (this.state.includeSenderName) {
                  dataObjNew.data.msgBody = `@${userDisplayName}: ${dataObj.data.msgBody}`;
                }
                onSelect(selectedUser, dataObjNew);
              }}
              color="primary"
            >
              {t('SEND')}
            </Button>
          )}
        </DialogActions>
      </Dialog>
    );
  }
}

export default withTranslation()(withStyles(RoomUserChoiceDialog));

class PerfectListWrapper extends Component {
  constructor(props) {
    super(props);
    this.onScroll = this.onScroll.bind(this);
    this.simpleBar = null;
    this.scrollRef = React.createRef();
    this.listRef = React.createRef();
  }

  onScroll(e) {
    const { scrollTop, scrollLeft } = e.target;

    const { Grid: grid } = this.listRef.current;

    grid.handleScrollEvent({ scrollTop, scrollLeft });

    const { onScroll } = this.props;
    if (onScroll) {
      onScroll(e);
    }
  }

  scrollTop(top) {
    if (this.simpleBar) {
      this.simpleBar.smoothScrollTo(top);
    }
  }

  componentDidMount() {
    this.simpleBar = new SimpleBarHanbiro(this.scrollRef.current);
  }

  render() {
    const { tree, itemSize, Row } = this.props;
    return (
      <AutoSizer>
        {({ height, width }) => {
          return (
            <div
              ref={this.scrollRef}
              onScroll={this.onScroll}
              style={{
                position: 'relative',
                width: width,
                height: height
              }}
            >
              <List
                style={{ overflowX: false, overflowY: false, outline: 'none' }}
                ref={this.listRef}
                scrollToAlignment={'start'}
                height={height}
                rowCount={tree.length}
                rowHeight={itemSize}
                width={width}
                rowRenderer={Row}
              ></List>
            </div>
          );
        }}
      </AutoSizer>
    );
  }
}

class PerfectListSearchWrapper extends Component {
  constructor(props) {
    super(props);
    this.onScroll = this.onScroll.bind(this);
    this.simpleBar = null;
    this.scrollRef = React.createRef();
    this.listRef = React.createRef();
  }

  onScroll(e) {
    const { scrollTop, scrollLeft } = e.target;

    const { Grid: grid } = this.listRef.current;

    grid.handleScrollEvent({ scrollTop, scrollLeft });
  }

  componentDidMount() {
    this.simpleBar = new SimpleBarHanbiro(this.scrollRef.current);
  }

  render() {
    const { searchResult, Row } = this.props;
    return (
      <AutoSizer>
        {({ height, width }) => {
          return (
            <div
              ref={this.scrollRef}
              onScroll={this.onScroll}
              style={{
                position: 'relative',
                width: width,
                height: height
              }}
            >
              <List
                style={{ overflowX: false, overflowY: false, outline: 'none' }}
                ref={this.listRef}
                scrollToAlignment={'start'}
                height={height}
                rowCount={searchResult.length}
                rowHeight={60}
                width={width}
                rowRenderer={Row}
              ></List>
            </div>
          );
        }}
      </AutoSizer>
    );
  }
}

RoomUserChoiceDialog.propTypes = {
  rooms: PropTypes.array,
  org: PropTypes.array,
  open: PropTypes.bool,
  groupCached: PropTypes.object,
  user: PropTypes.object,
  user_cached: PropTypes.object,
  onClose: PropTypes.func,
  onSelect: PropTypes.func,
  maximum: PropTypes.number,
  dataObj: PropTypes.object
};

RoomUserChoiceDialog.defaultProps = {
  rooms: [],
  org: [],
  groupCached: {},
  user: {},
  user_cached: {},
  open: false,
  onClose: () => {},
  onSelect: (selectedList, itemData) => {},
  dataObj: {},
  maximum: 1
};

function Group(props) {
  const classes = useStyles();
  const { data, onClick, groupOpen, style, paddingLeft } = props;

  return (
    <ListItem
      style={style}
      button
      // divider={true}
      onClick={onClick}
    >
      <ListItemIcon style={{ paddingLeft }}>
        <div className={classes.checkBoxContent}>
          <Folder />
        </div>
      </ListItemIcon>
      <ListItemText
        primary={
          <Typography variant="subtitle2" className={classes.text}>
            {data.displayName}
          </Typography>
        }
      />
      <div className={classes.counter}>
        <Typography variant="subtitle2" className={classes.text}>
          {data.numberOnline}/{data.numberAll}
        </Typography>
        {groupOpen[data.primaryKey] ? <ExpandLess /> : <ExpandMore />}
      </div>
    </ListItem>
  );
}

function User(props) {
  const classes = useStyles();

  const { data, style, selected, paddingLeft, onClick, disabled } = props;

  let detail;
  let first;
  if (!props.showStatus) {
    detail = data.fullLocation.result;
    first = data.fullLocation.first;
  }

  const displayName = data.displayNameWithDuty
    ? data.displayNameWithDuty
    : data.displayName;

  return (
    <ListItem
      style={style}
      button
      onClick={onClick}
      selected={selected}
      disabled={disabled}
    >
      <ListItemIcon style={{ paddingLeft: paddingLeft, paddingRight: 8 }}>
        <div className={classes.iconContent}>
          {selected || disabled ? (
            <div className={classes.avatarCheck}>
              <CheckIcon fontSize="large" style={{ color: 'white' }} />
            </div>
          ) : null}
          <UserPhotoView key={data?.userKey} data={data} imgSize={50} />
        </div>
      </ListItemIcon>
      <ListItemText
        primary={
          <Typography variant="subtitle2" className={classes.textUser}>
            {displayName}
          </Typography>
        }
        secondary={
          props.showStatus ? (
            <Typography variant="caption" color="textSecondary">
              {data.userNickName}
            </Typography>
          ) : (
            <Typography
              variant="caption"
              color="textSecondary"
              className={classes.textInfor}
            >
              {detail}
              <b>{first}</b>
            </Typography>
          )
        }
      />
    </ListItem>
  );
}

User.propTypes = {
  showStatus: PropTypes.bool
};

User.defaultProps = {
  showStatus: true
};
