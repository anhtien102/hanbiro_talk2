import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    main: {
      flex: 'flex-wrap',
      width: 50,
      height: 50,
      alignItems: 'center',
      justifyContent: 'center'
    },
    summaryTotal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: 22,
      height: 22,
      borderRadius: '50%',
      backgroundColor: theme.palette.primary.main
    }
  })
);
