import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  main: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flex: 1
  },
  contentLable: {
    alignItems: 'center',
    paddingLeft: 16,
    display: 'flex',
    width: '100%',
    backgroundColor: theme.palette.divider
  },
  lableNoData: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 16,
    display: 'flex',
    width: '100%'
  },
  center: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex'
  },
  circleOnline: {
    backgroundColor: theme.palette.success.light,
    width: 10,
    height: 10,
    borderRadius: '50%',
    marginTop: 3
  },
  circleOffline: {
    backgroundColor: '#cbced1',
    width: 10,
    height: 10,
    borderRadius: '50%',
    marginTop: 3
  },
  contentStatus: {
    width: 16,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    marginRight: 4
  },
  contentDevice: {
    display: 'flex',
    position: 'relative',
    bottom: 20,
    left: 30,
    flexDirection: 'row'
  },
  contentBadge: {
    width: 18,
    height: 18,
    borderRadius: '50%'
  },
  badgeDeviceGreen: {
    backgroundColor: '#94c400',
    display: 'flex',
    width: 18,
    height: 18,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '50%',
    color: 'white'
  },
  badgeDevice: {
    backgroundColor: '#28a7e8',
    display: 'flex',
    width: 18,
    height: 18,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '50%',
    color: 'white'
  },
  textName: {
    display: 'block',
    maxWidth: 'calc(100% - 20px)',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  textInfor: {
    display: 'block',
    maxWidth: 'calc(100% - 20px)',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  contentSelect: {
    width: 8,
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  selectedItem: {
    width: 4,
    height: '100%',
    backgroundColor: theme.palette.primary.main
  }
});

export default withStyles(styles);
