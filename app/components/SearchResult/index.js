import React, { Component } from 'react';
import withStyles from './styles';
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
  MenuItem,
  Tooltip
} from '@material-ui/core';
import Folder from '@material-ui/icons/Folder';
import { contactUtils, groupUtils } from '../../core/model/OrgUtils';
import RoomInfor from '../../components/RoomInfor';
import UserPhotoView from '../UserPhoto';
import TalkIcon from '../TalkIcon';
import * as constantsApp from '../../configs/constant';
import { StatusMode } from '../../core/service/talk-constants';
import { ipcRenderer, remote } from 'electron';
import { AutoSizer, List } from 'react-virtualized';
import { withTranslation } from 'react-i18next';
import { MenuOptionContext } from '../../screens/home';
import PropTypes from 'prop-types';

import SimpleBarHanbiro from '../SimpleBarHanbiro';
import talkAPI from '../../core/service/talk.api.render';

import { UserStatusMobileIcon, UserStatusPCIcon } from '../HanSVGIcon';

import UpdateRoomTitleDialog from '../UpdateRoomTitleDialog';

class SearchResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      changeRoomTitleDialog: false,
      roomInfo: {}
    };
    this.onGroupTalk = this.onGroupTalk.bind(this);
    this.onGroupWhisperCreate = this.onGroupWhisperCreate.bind(this);
    this.onUserClick = this.onUserClick.bind(this);
    this.onRoomClick = this.onRoomClick.bind(this);
    this.onContactWhisperCreate = this.onContactWhisperCreate.bind(this);
    this.changeOrgFavorite = this.changeOrgFavorite.bind(this);
    this.listRef = React.createRef();
    this.parentRef = React.createRef();

    this.onScroll = this.onScroll.bind(this);

    this.simpleBar = null;
  }

  static contextType = MenuOptionContext;

  onScroll(e) {
    const { scrollTop, scrollLeft } = e.target;

    const { Grid: grid } = this.listRef.current;

    grid.handleScrollEvent({ scrollTop, scrollLeft });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let room = null;
    let userKey = null;
    if (nextProps.appUI && nextProps.appUI.selected_room) {
      let selectedRoom =
        nextProps.appUI.selected_room[constantsApp.TAB_CATEGORY.tab_search];
      let users = selectedRoom ? selectedRoom.users : null;
      room = selectedRoom ? selectedRoom.room : null;
      if (users && users.length == 1) {
        userKey = users[0];
      }
    }

    let allNull = true;
    if (room) {
      allNull = false;
      if (
        !prevState.selectedRoom ||
        prevState.selectedRoom.rRoomKey != room.rRoomKey
      ) {
        return {
          selectedRoom: room,
          selectedUser: null
        };
      }
    } else if (userKey) {
      allNull = false;
      if (!prevState.selectedUser || prevState.selectedUser != userKey) {
        return {
          selectedUser: userKey,
          selectedRoom: null
        };
      }
    }
    if (allNull) {
      return {
        selectedUser: null,
        selectedRoom: null
      };
    }

    return null;
  }

  componentDidMount() {
    this.simpleBar = new SimpleBarHanbiro(this.parentRef.current);
  }

  componentDidUpdate() {
    // this.listRef.current?.forceUpdateGrid();
  }

  componentWillUnmount() {}

  onUserClick(item, newTab) {
    ipcRenderer.send(
      constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
      constantsApp.ACTION_QUERY_ROOM_WITH_USER_LIST_AND_CREATE_ROOM_IF_NEED,
      {
        category: constantsApp.TAB_CATEGORY.tab_search,
        users: [item],
        newTab: newTab
      }
    );
  }

  isFavorite(item) {
    const { listActualyFav } = this.props;

    const isGroup = groupUtils.isGroupObj(item);
    const key = isGroup ? '1_' + item.groupKey : '0_' + item.userKey;
    if (listActualyFav[key]) {
      return true;
    }
    return false;
  }

  changeOrgFavorite(item) {
    const isGroup = groupUtils.isGroupObj(item);
    const isFav = this.isFavorite(item);
    const key = isGroup ? item.groupKey : item.userKey;
    ipcRenderer.send(
      constantsApp.SEND_SOCKET_API_EVENT,
      constantsApp.API_CHANGE_ORG_FAVORITE,
      {
        key: key,
        isGroup: isGroup,
        isAdd: !isFav
      }
    );
  }

  onContactWhisperCreate(item) {
    const users = [item];
    this.context.onOpenWhisperDialog(users);
  }

  onRoomClick(item, newTab) {
    const { openOneRoom, updateTab, commonSettings } = this.props;
    const { selectedRoom } = this.state;
    const lastRoomKey = selectedRoom ? selectedRoom.rRoomKey : null;
    remote.getGlobal('ShareKeepValue').CURRENT_ROOM_KEY = item.rRoomKey;

    if (item.rRoomKey == lastRoomKey) {
      if (newTab) {
        updateTab(item);
      }
      return;
    }
    ipcRenderer.send(
      constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
      constantsApp.ACTION_UPDATE_ALL_MESSAGE_AS_READ_AND_SEND_API,
      {
        roomId: item.rID,
        roomKey: item.rRoomKey
      }
    );

    openOneRoom(
      constantsApp.TAB_CATEGORY.tab_search,
      item.rRoomKey,
      item,
      null,
      newTab,
      commonSettings
    );
  }

  onGroupTalk(item) {
    const users = groupUtils.getUserInGroup(item);
    ipcRenderer.send(
      constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
      constantsApp.ACTION_QUERY_ROOM_WITH_USER_LIST_AND_CREATE_ROOM_IF_NEED,
      {
        category: constantsApp.TAB_CATEGORY.tab_room_list,
        users: users,
        changeTab: true,
        roomName: item.displayName
      }
    );
  }

  onGroupWhisperCreate(item) {
    const users = groupUtils.getUserInGroup(item);
    this.context.onOpenWhisperDialog(users);
  }

  render() {
    const {
      classes,
      allSeachResult,
      org_time_card,
      user_cached,
      user,
      appUI,
      t,
      commonSettings
    } = this.props;

    const { changeRoomTitleDialog, roomInfo } = this.state;

    const Row = ({ index, isScrolling, key, style }) => {
      const item = allSeachResult[index];
      if (typeof item == 'string') {
        return (
          <span
            key={key}
            className={
              item == 'no_result' ? classes.lableNoData : classes.contentLable
            }
            style={style}
          >
            {t(item)}
          </span>
        );
      }
      if (item.rRoomKey) {
        return (
          <RoomInfor
            style={style}
            key={key}
            selectedRoom={appUI.selected_room.tab_search?.room}
            data={item}
            user={user}
            user_cached={user_cached}
            onClick={() => this.onRoomClick(item, false)}
            onOpenMenu={event => {
              const offNotification = item.rRoomPushAlert == 0;
              const supportChangeRoomName = talkAPI.apiSupportList.hasSupportChangeRoomName();
              const allowRoomDelete = talkAPI.allowRoomDelete();
              const supportRoomFavorite = talkAPI.apiSupportList.hasSupportRoomFavorite();
              const offFavorite = item.rRoomFavorite <= 0;
              const menuData = [
                commonSettings.use_new_tab && (
                  <MenuItem
                    key="Open new Tab"
                    onClick={() => {
                      this.context.onClose();
                      this.onRoomClick(item, true);
                    }}
                  >
                    <div className={classes.memuItem}>
                      <Typography variant="caption">
                        {t('Open new Tab')}
                      </Typography>
                    </div>
                  </MenuItem>
                ),
                supportChangeRoomName ? (
                  <MenuItem
                    key="Change room title"
                    onClick={() => {
                      this.context.onClose();
                      this.setState({
                        roomInfo: item,
                        changeRoomTitleDialog: true
                      });
                    }}
                  >
                    <div className={classes.memuItem}>
                      <Typography variant="caption">
                        {t('Change room title')}
                      </Typography>
                    </div>
                  </MenuItem>
                ) : null,
                allowRoomDelete ? (
                  <MenuItem
                    key="Leave room"
                    onClick={() => {
                      this.context.onClose();
                      ipcRenderer.send(
                        constantsApp.SEND_SOCKET_API_EVENT,
                        constantsApp.API_ROOM_OUT,
                        {
                          roomKey: item.rRoomKey
                        }
                      );
                    }}
                  >
                    <div className={classes.memuItem}>
                      <Typography variant="caption">
                        {t('Leave room')}
                      </Typography>
                    </div>
                  </MenuItem>
                ) : null,
                <MenuItem
                  key="Mark all as read"
                  onClick={() => {
                    this.context.onClose();
                    ipcRenderer.send(
                      constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
                      constantsApp.ACTION_UPDATE_ALL_MESSAGE_AS_READ_AND_SEND_API,
                      {
                        roomId: item.rID,
                        roomKey: item.rRoomKey
                      }
                    );
                  }}
                >
                  <div className={classes.memuItem}>
                    <Typography variant="caption">
                      {t('Mark all as read')}
                    </Typography>
                  </div>
                </MenuItem>,
                <MenuItem
                  key="Turn off new message alert"
                  onClick={() => {
                    this.context.onClose();
                    ipcRenderer.send(
                      constantsApp.SEND_SOCKET_API_EVENT,
                      constantsApp.API_CHANGE_ALERT_ROOM,
                      {
                        roomKey: item.rRoomKey,
                        enable: !(item.rRoomPushAlert > 0)
                      }
                    );
                  }}
                >
                  <div className={classes.memuItem}>
                    <Typography variant="caption">
                      {offNotification
                        ? t('Turn on new message alert')
                        : t('Turn off new message alert')}
                    </Typography>
                  </div>
                </MenuItem>,
                supportRoomFavorite ? (
                  <MenuItem
                    key="On Off Room favorite"
                    onClick={() => {
                      this.context.onClose();
                      ipcRenderer.send(
                        constantsApp.SEND_SOCKET_API_EVENT,
                        constantsApp.API_CHANGE_ROOM_FAVORITE,
                        {
                          roomKey: item.rRoomKey,
                          isAdd: offFavorite
                        }
                      );
                    }}
                  >
                    <div className={classes.memuItem}>
                      <Typography variant="caption">
                        {offFavorite
                          ? t('Add to Favorites')
                          : t('Remove from Favorite')}
                      </Typography>
                    </div>
                  </MenuItem>
                ) : null
              ];
              this.context.onOpen(event, menuData);
            }}
          />
        );
      }

      if (item.groupId) {
        const openGroupMenu = () => {
          const menuData = [
            <MenuItem
              key="Talk"
              onClick={() => {
                this.context.onClose();
                this.onGroupTalk(item);
              }}
            >
              <div className={classes.memuItem}>
                <Typography variant="caption">{t('Talk')}</Typography>
              </div>
            </MenuItem>,
            <MenuItem
              key="Send Whisper"
              onClick={() => {
                this.context.onClose();
                this.onGroupWhisperCreate(item);
              }}
            >
              <div className={classes.memuItem}>
                <Typography variant="caption">{t('Send Whisper')}</Typography>
              </div>
            </MenuItem>
          ];
          this.context.onOpen(event, menuData);
        };

        return (
          <GroupSearch
            style={style}
            paddingTree={16}
            key={key}
            data={item}
            position={index}
            classes={classes}
            onClick={() => openGroupMenu()}
            onOpenMenu={event => {
              openGroupMenu();
            }}
          />
        );
      }

      const selectedUser =
        appUI.selected_room.tab_search?.users &&
        appUI.selected_room.tab_search?.users[0];
      return (
        <UserSearch
          style={style}
          data={item}
          selectedUser={selectedUser}
          key={key}
          classes={classes}
          org_time_card={org_time_card}
          onClick={this.onUserClick}
          onOpenMenu={event => {
            const isFav = this.isFavorite(item);
            const menuData = [
              commonSettings.use_new_tab && (
                <MenuItem
                  key="Open new Tab"
                  onClick={() => {
                    this.context.onClose();
                    this.onUserClick(item, true);
                  }}
                >
                  <div className={classes.memuItem}>
                    <Typography variant="caption">
                      {t('Open new Tab')}
                    </Typography>
                  </div>
                </MenuItem>
              ),
              <MenuItem
                key="Talk"
                onClick={() => {
                  this.context.onClose();
                  this.onUserClick(item, false);
                }}
              >
                <div className={classes.memuItem}>
                  <Typography variant="caption">{t('Talk')}</Typography>
                </div>
              </MenuItem>,
              <MenuItem
                key="View Profile"
                onClick={() => {
                  this.context.onClose();
                  this.context.onOpenProfile(item);
                }}
              >
                <div className={classes.memuItem}>
                  <Typography variant="caption">{t('View Profile')}</Typography>
                </div>
              </MenuItem>,
              <MenuItem
                key="Add to Favorites"
                onClick={() => {
                  this.context.onClose();
                  this.changeOrgFavorite(item);
                }}
              >
                <div className={classes.memuItem}>
                  <Typography variant="caption">
                    {isFav ? t('Remove from Favorite') : t('Add to Favorites')}
                  </Typography>
                </div>
              </MenuItem>,
              <MenuItem
                key="Send Whisper"
                onClick={() => {
                  this.context.onClose();
                  this.onContactWhisperCreate(item);
                  console.log('action menu for Send Whisper', item);
                }}
              >
                <div className={classes.memuItem}>
                  <Typography variant="caption">{t('Send Whisper')}</Typography>
                </div>
              </MenuItem>
            ];
            this.context.onOpen(event, menuData);
          }}
        />
      );
    };

    const itemSize = ({ index }) => {
      const value = allSeachResult[index];
      if (typeof value == 'string') {
        return value == 'no_result' ? 70 : 30;
      }
      return 70;
    };

    return (
      <div className={classes.main}>
        <AutoSizer>
          {({ height, width }) => {
            return (
              <div
                ref={this.parentRef}
                onScroll={this.onScroll}
                style={{
                  position: 'relative',
                  width: width,
                  height: height
                }}
              >
                <List
                  style={{
                    overflowX: false,
                    overflowY: false,
                    outline: 'none'
                  }}
                  ref={this.listRef}
                  height={height}
                  rowCount={allSeachResult.length}
                  rowHeight={itemSize}
                  width={width}
                  rowRenderer={Row}
                ></List>
              </div>
            );
          }}
        </AutoSizer>
        {changeRoomTitleDialog && (
          <UpdateRoomTitleDialog
            show={true}
            roomInfo={roomInfo}
            onUpdate={(obj, applyAll, title) => {
              ipcRenderer.send(
                constantsApp.SEND_SOCKET_API_EVENT,
                constantsApp.API_CHANGE_ROOM_NAME,
                {
                  roomKey: obj.rRoomKey,
                  isPublic: applyAll,
                  name: title
                }
              );
              this.setState({ changeRoomTitleDialog: false, roomInfo: {} });
            }}
            onClose={() => {
              this.setState({ changeRoomTitleDialog: false, roomInfo: {} });
            }}
          />
        )}
      </div>
    );
  }
}

class UserSearch extends Component {
  constructor(props) {
    super(props);
    this.onUserClick = this.onUserClick.bind(this);
  }

  onUserClick(newTab) {
    const { data, onClick } = this.props;
    if (onClick) {
      onClick(data, newTab);
    }
  }

  exportIconStatus = status => {
    const { classes } = this.props;
    switch (status) {
      case StatusMode.logout:
      case StatusMode.offline:
        return <div className={classes.circleOffline} />;
      case StatusMode.available:
        return <div className={classes.circleOnline} />;
      case StatusMode.away:
        return <TalkIcon name="idle" />;
      case StatusMode.busy:
        return <TalkIcon name="busy" />;
      case StatusMode.meeting:
        return <TalkIcon name="meeting" />;
      case StatusMode.meal:
        return <TalkIcon name="meal" />;
      case StatusMode.phone:
        return <TalkIcon name="call" />;
      case StatusMode.out:
        return <TalkIcon name="out" />;
      case StatusMode.business_trip:
        return <TalkIcon name="business_trip" />;

      default:
        return null;
    }
  };

  render() {
    const {
      data,
      org_time_card,
      classes,
      selectedUser,
      onOpenMenu,
      style
    } = this.props;
    const isDualLogin = contactUtils.isDualLogin(data);
    const isPCLogin = contactUtils.isPCLogin(data);
    const isMobileLogin = contactUtils.isMobileLogin(data);
    const iconStatus = this.exportIconStatus(data.userStatus);
    const isSelected = data.userKey == selectedUser;
    let renderDeviceList;
    let renderHoliday;
    let renderBirthDay;

    if (org_time_card && org_time_card[data.userKey]) {
      renderHoliday = org_time_card[data.userKey].holiday == 1;
      renderBirthDay = org_time_card[data.userKey].birthDay == 1;
    }

    if (isDualLogin) {
      renderDeviceList = (
        <div className={classes.contentDevice}>
          <div className={classes.contentBadge}>
            <div className={classes.badgeDevice}>
              <UserStatusPCIcon style={{ fontSize: 12 }} />
            </div>
          </div>
          <div style={{ position: 'absolute', left: 12 }}>
            <div className={classes.contentBadge}>
              <div className={classes.badgeDeviceGreen}>
                <UserStatusMobileIcon style={{ fontSize: 12 }} />
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      if (isPCLogin) {
        renderDeviceList = (
          <div className={classes.contentDevice}>
            <div className={classes.contentBadge}>
              <div className={classes.badgeDevice}>
                <UserStatusPCIcon style={{ fontSize: 12 }} />
              </div>
            </div>
          </div>
        );
      }

      if (isMobileLogin) {
        renderDeviceList = (
          <div className={classes.contentDevice}>
            <div className={classes.contentBadge}>
              <div className={classes.badgeDevice}>
                <UserStatusMobileIcon style={{ fontSize: 12 }} />
              </div>
            </div>
          </div>
        );
      }
    }

    const detail = data.fullLocation.result;
    const first = data.fullLocation.first;
    const tooltipText = `${detail}${first}`;

    return (
      <ListItem
        style={{
          ...style,
          paddingTop: 0,
          paddingBottom: 0,
          paddingRight: 0
        }}
        button
        // divider={true}
        onClick={() => this.onUserClick(false)}
        onContextMenu={event => {
          event.preventDefault();
          onOpenMenu(event);
        }}
        selected={isSelected}
      >
        <ListItemIcon style={{ height: 50, paddingRight: 15 }}>
          <div style={{ flexDirection: 'row', display: 'flex' }}>
            <div className={classes.contentStatus}>
              {renderHoliday ? (
                <div style={{ marginTop: 4 }}>
                  <TalkIcon name="holiday" />
                </div>
              ) : null}
              {renderBirthDay ? (
                <div style={{ marginTop: 4 }}>
                  <TalkIcon name="birthday" />
                </div>
              ) : null}
              {iconStatus}
            </div>
            <div>
              <UserPhotoView key={data.userKey} data={data} imgSize={50} />
              {renderDeviceList}
            </div>
          </div>
        </ListItemIcon>
        <ListItemText
          primary={
            <Typography variant="subtitle2" className={classes.textName}>
              {data.displayNameWithDuty}
            </Typography>
          }
          secondary={
            <Tooltip title={tooltipText} aria-label={tooltipText}>
              <Typography
                variant="caption"
                color="textSecondary"
                className={classes.textInfor}
              >
                {detail}
                <b>{first}</b>
              </Typography>
            </Tooltip>
          }
        />
        <div className={classes.contentSelect}>
          {isSelected && <div className={classes.selectedItem}></div>}
        </div>
      </ListItem>
    );
  }
}

class GroupSearch extends Component {
  constructor(props) {
    super(props);
    this.onGroupClick = this.onGroupClick.bind(this);
  }

  onGroupClick() {
    const { data, onClick, position } = this.props;
    if (onClick) {
      onClick(data, position);
    }
  }

  render() {
    const { data, onOpenMenu, style, paddingTree, classes } = this.props;

    return (
      <ListItem
        style={{ ...style, paddingLeft: paddingTree }}
        button
        onClick={this.onGroupClick}
        onContextMenu={event => {
          event.preventDefault();
          event.stopPropagation();
          onOpenMenu(event);
        }}
      >
        <ListItemIcon style={{ minWidth: 36 }}>
          <Folder />
        </ListItemIcon>
        <ListItemText
          primary={
            <Typography variant="subtitle1" className={classes.textName}>
              {data.displayName}
            </Typography>
          }
        />
        <div className={classes.contentCouter}>
          <Typography variant="caption" color="textSecondary">
            {data.numberOnline}/{data.numberAll}
          </Typography>
        </div>
      </ListItem>
    );
  }
}

SearchResult.propTypes = {
  data: PropTypes.object,
  org_time_card: PropTypes.object,
  classes: PropTypes.any,
  selectedUser: PropTypes.object,
  style: PropTypes.object,
  onOpenMenu: PropTypes.func
};

SearchResult.defaultProps = {
  data: {},
  org_time_card: {},
  classes: null,
  selectedUser: {},
  style: {},
  onOpenMenu: () => {}
};

export default withTranslation()(withStyles(SearchResult));
