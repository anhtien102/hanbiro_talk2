import React from 'react';
import PropTypes from 'prop-types';
import { Typography, ButtonBase } from '@material-ui/core';
import UserPhotoView from '../../../UserPhoto';

import { useStyles } from './styles';
import { useSelector } from 'react-redux';

export default function HistoryGroupUserPhoto(props) {
  const classes = useStyles();

  const userCached = useSelector(state => state.company.user_cached);

  const { userList, user, placeHolder, onPress } = props;
  if (userList) {
    const list = userList.filter(userKey => {
      return userKey != user.user_key;
    });

    switch (list.length) {
      case 0:
        return (
          <ButtonBase onClick={onPress} disabled={!onPress}>
            <div style={{ position: 'relative' }}>
              <UserPhotoView
                key={user.user_key}
                data={userCached[user.user_key]}
                className={classes.main}
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: '50%'
                }}
                userKeyData={user.user_key}
                imgSize={50}
                placeHolder={placeHolder}
              />
            </div>
          </ButtonBase>
        );
      case 1:
        const userKey = list[0];
        return (
          <ButtonBase onClick={onPress} disabled={!onPress}>
            <div style={{ position: 'relative' }}>
              <UserPhotoView
                key={userKey}
                className={classes.main}
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: '50%'
                }}
                data={userCached[userKey]}
                userKeyData={userKey}
                imgSize={50}
                placeHolder={placeHolder}
              />
            </div>
          </ButtonBase>
        );

      case 2:
        return (
          <ButtonBase onClick={onPress} disabled={!onPress}>
            <div
              style={{
                width: 50,
                height: 50,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-evenly'
              }}
            >
              <div
                style={{
                  width: '100%',
                  height: '50%',
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'flex-start'
                }}
              >
                <UserPhotoView
                  key={list[0]}
                  userKeyData={list[0]}
                  style={{
                    width: 28,
                    height: 28,
                    borderRadius: '50%'
                  }}
                  data={userCached[list[0]]}
                  imgSize={50}
                  placeHolder={placeHolder}
                />
              </div>
              <div
                style={{
                  width: '100%',
                  height: '50%',
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'flex-end'
                }}
              >
                <UserPhotoView
                  key={list[1]}
                  userKeyData={list[1]}
                  data={userCached[list[1]]}
                  style={{
                    width: 28,
                    height: 28,
                    borderRadius: '50%'
                  }}
                  imgSize={50}
                  placeHolder={placeHolder}
                />
              </div>
            </div>
          </ButtonBase>
        );

      case 3:
        return (
          <ButtonBase onClick={onPress} disabled={!onPress}>
            <div
              style={{
                width: 50,
                height: 50,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-evenly'
              }}
            >
              <div
                style={{
                  width: '100%',
                  height: '50%',
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'center'
                }}
              >
                <UserPhotoView
                  key={list[0]}
                  userKeyData={list[0]}
                  data={userCached[list[0]]}
                  style={{
                    width: 23,
                    height: 23,
                    borderRadius: '50%'
                  }}
                  imgSize={50}
                  placeHolder={placeHolder}
                />
              </div>
              <div
                style={{
                  width: '100%',
                  height: '50%',
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between'
                }}
              >
                <UserPhotoView
                  key={list[1]}
                  userKeyData={list[1]}
                  data={userCached[list[1]]}
                  style={{
                    width: 23,
                    height: 23,
                    borderRadius: '50%'
                  }}
                  imgSize={50}
                  placeHolder={placeHolder}
                />
                <UserPhotoView
                  key={list[2]}
                  userKeyData={list[2]}
                  data={userCached[list[2]]}
                  style={{
                    width: 23,
                    height: 23,
                    borderRadius: '50%',
                    marginLeft: 1,
                    marginLeft: 3
                  }}
                  imgSize={50}
                  placeHolder={placeHolder}
                />
              </div>
            </div>
          </ButtonBase>
        );
      default:
        return (
          <ButtonBase onClick={onPress} disabled={!onPress}>
            <div
              style={{
                width: 50,
                height: 50,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-evenly'
              }}
            >
              <div
                style={{
                  width: '100%',
                  height: '50%',
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-evenly'
                }}
              >
                <UserPhotoView
                  key={list[0]}
                  userKeyData={list[0]}
                  data={userCached[list[0]]}
                  style={{
                    width: 23,
                    height: 23,
                    borderRadius: '50%'
                  }}
                  imgSize={50}
                  placeHolder={placeHolder}
                />
                <UserPhotoView
                  key={list[1]}
                  userKeyData={list[1]}
                  data={userCached[list[1]]}
                  style={{
                    width: 23,
                    height: 23,
                    borderRadius: '50%'
                  }}
                  imgSize={50}
                  placeHolder={placeHolder}
                />
              </div>
              <div
                style={{
                  width: '100%',
                  height: '50%',
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-evenly'
                }}
              >
                <UserPhotoView
                  key={list[2]}
                  userKeyData={list[2]}
                  data={userCached[list[2]]}
                  style={{
                    width: 23,
                    height: 23,
                    borderRadius: '50%'
                  }}
                  imgSize={50}
                  placeHolder={placeHolder}
                />
                <div className={classes.summaryTotal}>
                  <Typography
                    variant="caption"
                    style={{
                      textAlign: 'center',
                      color: 'white',
                      fontWeight: 'bold',
                      fontSize: list.length > 13 ? 9 : 11
                    }}
                  >
                    +{list.length - 3}
                  </Typography>
                </div>
              </div>
            </div>
          </ButtonBase>
        );
    }
  }

  return (
    <UserPhotoView
      className={classes.main}
      style={{
        width: 50,
        height: 50,
        borderRadius: '50%'
      }}
      imgSize={50}
      placeHolder={placeHolder}
    />
  );
}

HistoryGroupUserPhoto.propTypes = {
  user: PropTypes.object,
  userList: PropTypes.any,
  imgSize: PropTypes.number,
  placeHolder: PropTypes.string,
  onPress: PropTypes.func
};

HistoryGroupUserPhoto.defaultProps = {
  user: {},
  userList: null,
  imgSize: 50,
  placeHolder: './images/user-default.jpg',
  onPress: null
};
