import React from 'react';
import { useStyles } from './styles';
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import HistoryGroupUserPhoto from '../HistoryGroupUserPhoto';
import UserPhotoView from '../../../UserPhoto';
import TimeAgo from 'javascript-time-ago';
import en from 'javascript-time-ago/locale/en';
import vi from 'javascript-time-ago/locale/vi';
import ko from 'javascript-time-ago/locale/ko';
import PropTypes from 'prop-types';

import { timeAgoStringWithTimeAgoLib } from '../../../../utils';

TimeAgo.addLocale(en);
TimeAgo.addLocale(vi);
TimeAgo.addLocale(ko);

export default function HistoryInfo(props) {
  const { t, i18n } = useTranslation();
  const classes = useStyles();
  const { data, style, selectedRoom, user, user_cached, onClick } = props;

  const timeAgo = new TimeAgo(i18n.language);
  const roomKeySelected = selectedRoom ? selectedRoom.ROOMKEY : '';
  const searchMessageTime = selectedRoom
    ? selectedRoom.SEARCH_MESSAGE_TIME
    : '';

  const selected =
    data.ROOMKEY == roomKeySelected &&
    data.SEARCH_MESSAGE_TIME == searchMessageTime;
  const messageTime = new Date(parseInt(data.SEARCH_MESSAGE_TIME));

  let userKeyList = data.USERS.split(',');
  userKeyList = userKeyList.filter(userKey => userKey != user.user_key);

  let lastMessage = data.SEARCH_MESSAGE;
  if (data.DELETED) {
    lastMessage = t('This message has been deleted');
  }
  const customerRoom = data.CHATTYPE == 'LIVECHAT';
  let roomName = data.ALIAS != null && data.ALIAS != '' ? data.ALIAS : null;

  if (!roomName) {
    if (userKeyList.length == 0) {
      roomName = t('My Room');
    } else {
      let tmpRoomName = '';
      const size = userKeyList.length > 10 ? 10 : userKeyList.length;
      for (let i = 0; i < size; i++) {
        const contact = user_cached[userKeyList[i]];
        tmpRoomName += contact ? contact.displayName : '-';
        if (i < size - 1) {
          tmpRoomName += ' ,';
        }
      }
      roomName = tmpRoomName;
    }
  }
  const timeDisplay = timeAgoStringWithTimeAgoLib(messageTime, timeAgo);

  return (
    <ListItem
      className={customerRoom ? classes.customerRoom : classes.item}
      button
      selected={selected}
      onClick={onClick}
      style={style}
    >
      <ListItemIcon>
        {customerRoom ? (
          <UserPhotoView
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              width: 50,
              height: 50,
              borderRadius: '50%',
              backgroundColor: 'white'
            }}
            imgSize={50}
            placeHolder={'./images/customter_avatar.png'}
          />
        ) : (
          <HistoryGroupUserPhoto userList={userKeyList} user={user} />
        )}
      </ListItemIcon>
      <ListItemText
        primary={
          <Typography variant="subtitle2" className={classes.textUser}>
            {roomName}
          </Typography>
        }
        secondary={
          <React.Fragment>
            <Typography
              variant="caption"
              color="textSecondary"
              className={classes.textMessage}
            >
              {lastMessage}
            </Typography>
          </React.Fragment>
        }
      />
      <div style={{ display: 'flex', flexDirection: 'row', height: '100%' }}>
        <div className={classes.contentRight}>
          <Typography
            variant="caption"
            color="textSecondary"
            className={classes.textTime}
          >
            {timeDisplay}
          </Typography>
        </div>
        <div className={classes.contentSelect}>
          {selected && <div className={classes.selectedItem}></div>}
        </div>
      </div>
    </ListItem>
  );
}

HistoryInfo.propTypes = {
  data: PropTypes.object,
  style: PropTypes.object,
  user: PropTypes.object,
  user_cached: PropTypes.object,
  selectedRoom: PropTypes.object,
  enableUnread: PropTypes.bool,
  onClick: PropTypes.func,
  onOpenMenu: PropTypes.func
};

HistoryInfo.defaultProps = {
  data: {},
  style: {},
  user: {},
  user_cached: {},
  selectedRoom: {},
  enableUnread: true,
  onClick: () => {},
  onOpenMenu: () => {}
};
