import React, { Fragment, Component, useEffect, Children } from 'react';
import Linkify from 'linkifyjs/react';
import { Typography, ButtonBase } from '@material-ui/core';
import { useStyles } from './styles';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import { messsageUtils } from '../../../../core/model/MessageUtils';
import LineStatus from '../../../MessageList/components/LineStatus';
import clsx from 'clsx';
import UserPhotoView from '../../../UserPhoto';
import {
  tickerEmojManager,
  isOnlyEmojis,
  convertFileSize,
  URL_WEB_REGEX
} from '../../../../utils';
import { getImg } from '../../../BaseFile';
import Highlighter from 'react-highlight-words';

import talkAPI from '../../../../core/service/talk.api.render';

import { openLink } from '../../../../utils/electron.utils';

export default function HistoryDetail(props) {
  const { t } = useTranslation();
  const classes = useStyles();
  const userCached = useSelector(state => state.company.user_cached);
  const currentKeyword = useSelector(state => state.chat_history.keyword);
  const { data, onSelectedMount } = props;

  let {
    isMe,
    isShowName,
    isShowTime,
    isShowDate,
    showTimePerHours,
    time
  } = data;

  useEffect(() => {
    if (onSelectedMount && data.isCenterMessage) {
      onSelectedMount();
    }
  }, []);

  const getUserInfo = (userKey, defaultName) => {
    if (userCached[userKey]) {
      return userCached[userKey];
    } else {
      return {
        primaryKey: '',
        userAlias: defaultName,
        displayName: defaultName,
        userKey: userKey,
        userPhotoTime: -1
      };
    }
  };

  const userInfo = getUserInfo(data.user, data.name);

  const getClass = () => {
    return {
      chatCont: isMe ? classes.chatContMy : classes.chatCont,
      chatWrap: isMe ? clsx(classes.chatWrap, classes.myChat) : classes.chatWrap
    };
  };

  const renderMessageDelete = () => {
    let { chatCont } = getClass();
    return (
      <div className={classes.chatContWrap}>
        <div className={chatCont}>
          <Typography
            variant="caption"
            className={isMe ? classes.descMessageMe : classes.descMessage}
          >
            {t('This message has been deleted')}
          </Typography>
        </div>
      </div>
    );
  };

  const getEmoticon = name => {
    return tickerEmojManager.filePathFromName(name);
  };

  const getEmoticonError = () => {
    return `./images/emoj/emoj_error.png`;
  };

  const findChunks = ({
    autoEscape,
    caseSensitive,
    sanitize,
    searchWords,
    textToHighlight
  }) => {
    const chunks = [];
    const textLow = textToHighlight.toLowerCase();
    const sep = /[\s]+/;

    const singleTextWords = textLow.split(sep);

    let fromIndex = 0;
    const singleTextWordsWithPos = singleTextWords.map(s => {
      const indexInWord = textLow.indexOf(s, fromIndex);
      fromIndex = indexInWord;
      return {
        word: s,
        index: indexInWord
      };
    });

    searchWords.forEach(sw => {
      const swLow = sw.toLowerCase();
      singleTextWordsWithPos.forEach(s => {
        if (s.word.match(URL_WEB_REGEX)) {
          const start = s.index;
          const end = s.index + s.word.length;
          chunks.push({
            start,
            end
          });
        } else if (s.word.includes(swLow)) {
          const sWordIndex = s.word.indexOf(swLow);
          if (sWordIndex >= 0) {
            const start = s.index + sWordIndex;
            const end = s.index + sWordIndex + swLow.length;
            chunks.push({
              start,
              end
            });
          }
        }
      });
    });

    return chunks;
  };

  const renderText = () => {
    let { chatCont } = getClass(isMe);

    const isEmojOnly = isOnlyEmojis(data.msg);

    return (
      <div className={classes.chatContWrap}>
        <div className={chatCont}>
          {currentKeyword ? (
            <Typography
              variant="body2"
              className={
                isEmojOnly
                  ? classes.textMessageEmojOnly
                  : isMe
                  ? classes.textMessageMe
                  : classes.textMessage
              }
            >
              <Highlighter
                searchWords={[currentKeyword]}
                textToHighlight={data.msg ?? ''}
                findChunks={findChunks}
                highlightTag={({ children, highlightIndex }) => {
                  if (children.match(URL_WEB_REGEX)) {
                    return (
                      <a
                        onClick={openLink}
                        href={children}
                        className={
                          isMe ? classes.colorLinkMe : classes.colorLinkOther
                        }
                        target="_blank"
                      >
                        <Highlighter
                          searchWords={[currentKeyword]}
                          textToHighlight={children ?? ''}
                        />
                      </a>
                    );
                  }
                  return <mark>{children}</mark>;
                }}
              />
            </Typography>
          ) : (
            <Linkify
              options={{
                attributes: {
                  onClick: openLink
                },
                className: isMe ? classes.colorLinkMe : classes.colorLinkOther
              }}
            >
              <Typography
                variant="body2"
                className={
                  isEmojOnly
                    ? classes.textMessageEmojOnly
                    : isMe
                    ? classes.textMessageMe
                    : classes.textMessage
                }
              >
                {data.msg}
              </Typography>
            </Linkify>
          )}
        </div>
      </div>
    );
  };

  const renderEmoticonIcon = () => {
    let { msg, msgEmotion } = data;
    let path = getEmoticon(msgEmotion);

    return (
      <div>
        <EmotionImage
          name={msgEmotion}
          failedPath={getEmoticonError()}
          path={path}
        />
        {msg ? renderText() : null}
      </div>
    );
  };

  const renderFileInfo = () => {
    return (
      <Fragment>
        <Typography
          variant="subtitle2"
          className={isMe ? classes.textFileNameMe : classes.textFileName}
        >
          {data.filename}
        </Typography>
        <Typography
          variant="caption"
          className={isMe ? classes.descMessageMe : classes.descMessage}
        >
          {convertFileSize(data.filesize)}
        </Typography>
      </Fragment>
    );
  };

  const renderFileOther = () => {
    let { chatCont } = getClass(isMe);

    return (
      <div className={classes.chatContWrap}>
        <div className={chatCont}>
          <Fragment>
            <div className={classes.chatFileWrap}>
              <img
                src={getImg(data.filename.split('.').pop())}
                alt=""
                className={classes.icoFile}
              />
              {renderFileInfo()}
            </div>
          </Fragment>
        </div>
      </div>
    );
  };

  const renderType = () => {
    if (messsageUtils.isMessageDeleted(data)) {
      return renderMessageDelete();
    }
    if (messsageUtils.isEmoticonIcon(data)) {
      return renderEmoticonIcon();
    }
    if (messsageUtils.isText(data)) {
      return renderText();
    }
    return renderFileOther();
  };

  const renderOther = () => {
    let { chatWrap } = getClass();
    return (
      <Fragment>
        <div className={chatWrap}>
          {isMe ? null : (
            <Fragment>
              {isShowName ? (
                <span className={classes.userPic} key={data.user}>
                  <ButtonBase onClick={() => {}}>
                    <UserPhotoView
                      style={{ width: 35, height: 35 }}
                      key={data.user}
                      userKeyData={data.user}
                      data={userInfo}
                      imgSize={50}
                    />
                  </ButtonBase>
                </span>
              ) : null}

              {isShowName ? (
                <Typography variant="subtitle2" color="textSecondary">
                  {userInfo.displayName}
                </Typography>
              ) : null}
            </Fragment>
          )}
          {renderType()}
          {isShowTime ? (
            <Typography variant="caption" color="textSecondary">
              {moment(time).format('HH:mm')}
            </Typography>
          ) : null}
        </div>
      </Fragment>
    );
  };

  const renderUI = () => {
    if (messsageUtils.isLeftRoom(data)) {
      return (
        <LineStatus
          message={`${userInfo.displayName} ${t('has left the chat room')}`}
        />
      );
    } else if (messsageUtils.isEnterRoom(data)) {
      let inviteUserInfo = data.cassa?.inviter
        ? getUserInfo(data.cassa?.inviter, data.cassa?.invitername)
        : null;
      if (inviteUserInfo && inviteUserInfo.displayName) {
        let patternF = t('invited $1 to room');
        patternF = patternF.replace('$1', userInfo.displayName);
        return (
          <LineStatus message={`${inviteUserInfo.displayName} ${patternF}`} />
        );
      } else {
        return (
          <LineStatus
            message={`${userInfo.displayName} ${t(
              'has entered the chat room'
            )}`}
          />
        );
      }
    } else if (messsageUtils.isRoomTitleChanged(data)) {
      let title = data.cassa?.alias ?? '';

      let textFormat = t('changed title to');
      textFormat = textFormat.replace('$1', title);
      return <LineStatus message={`${userInfo.displayName} ${textFormat}`} />;
    } else {
      return renderOther();
    }
  };

  return (
    <Fragment>
      <LineStatus
        visible={Boolean(isShowDate || showTimePerHours)}
        message={moment(time).format(talkAPI.dateTimeFormat())}
      />
      {renderUI()}
    </Fragment>
  );
}

HistoryDetail.propTypes = {
  data: PropTypes.object,
  onSelectedMount: PropTypes.func
};

HistoryDetail.defaultProps = {
  data: {},
  onSelectedMount: () => {}
};

const EMOTICON_HEIGHT = 100;

class EmotionImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      failed: false
    };
    this.onError = this.onError.bind(this);
  }
  onError() {
    this.setState({
      failed: true
    });
  }
  render() {
    const { path, failedPath, name } = this.props;
    const { failed } = this.state;

    const w =
      name == tickerEmojManager.likeTicker?.attributes?.name
        ? 50
        : EMOTICON_HEIGHT;
    const h =
      name == tickerEmojManager.likeTicker?.attributes?.name
        ? 50
        : EMOTICON_HEIGHT;
    return (
      <img
        style={{ width: w, height: h }}
        src={failed ? failedPath : path}
        onError={this.onError}
      />
    );
  }
}
