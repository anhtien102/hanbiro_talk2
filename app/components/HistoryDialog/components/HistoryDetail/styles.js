import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    chatCont: {
      cursor: 'pointer',
      whiteSpace: 'pre-wrap',
      position: 'relative',
      display: 'inline-block',
      maxWidth: 'calc(100% - 100px)',
      padding: '10px',
      backgroundColor: theme.palette.hanbiroColor.backgroundOther,
      borderRadius: '10px',
      verticalAlign: 'bottom',
      textAlign: 'left',
      wordBreak: 'break-word',
      wordWrap: 'break-word'
    },
    chatContMy: {
      cursor: 'pointer',
      whiteSpace: 'pre-wrap',
      position: 'relative',
      display: 'inline-block',
      maxWidth: 'calc(100% - 100px)',
      padding: '10px',
      backgroundColor:
        theme.palette.type == 'light'
          ? theme.palette.hanbiroColor.backgroundMe
          : '#303030',
      borderRadius: '10px',
      verticalAlign: 'bottom',
      textAlign: 'left',
      wordBreak: 'break-word',
      wordWrap: 'break-word'
    },
    chatContWrap: {
      display: 'block'
    },
    chatWrap: {
      position: 'relative',
      marginTop: '8px',
      paddingLeft: '50px'
    },
    chatFileWrap: {
      minHeight: '40px',
      position: 'relative',
      paddingLeft: '40px'
    },
    myChat: {
      paddingLeft: 0,
      textAlign: 'right'
    },
    descMessage: {
      color: 'rgba(0, 0, 0, 0.54)'
    },
    descMessageMe: {
      color: theme.palette.hanbiroColor.textDescMe
    },
    textFileName: {
      color: 'rgba(0, 0, 0, 0.87)',
      maxWidth: 200
    },
    textFileNameMe: {
      color:
        theme.palette.type == 'light'
          ? theme.palette.hanbiroColor.textMsgColorFileNameMe
          : '#ffffff',
      maxWidth: 200
    },
    userPic: {
      position: 'absolute',
      top: 0,
      left: 0,
      display: 'inline-block',
      overflow: 'hidden',
      width: '35px',
      height: '35px',
      borderRadius: '50%',
      textAlign: 'center'
    },
    colorLinkMe: {
      color:
        theme.palette.type == 'light'
          ? theme.palette.hanbiroColor.textMsgLinkMe
          : '#ffffff',
      textDecorationColor: 'rgba(255,255,255,0.5)',
      userDrag: 'none'
    },
    colorLinkOther: {
      color: theme.palette.hanbiroColor.textMsgLinkOther,
      userDrag: 'none'
    },
    textMessageEmojOnly: {
      fontSize: 40,
      color: 'rgba(0, 0, 0, 0.87)'
    },
    textMessage: {
      color: 'rgba(0, 0, 0, 0.87)'
    },
    textMessageMe: {
      color:
        theme.palette.type == 'light'
          ? theme.palette.hanbiroColor.textMsgColorMe
          : '#ffff'
    },
    icoFile: {
      position: 'absolute',
      top: 0,
      left: 0,
      borderColor: 'rgba(255,255,255,0.5)',
      borderRadius: 8,
      borderStyle: 'solid',
      borderWidth: 2,
      width: '30px'
    }
  })
);
