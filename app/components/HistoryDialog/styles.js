import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    dialog: {
      width: '100%',
      flexDirection: 'column',
      display: 'flex'
    },
    contentTitle: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      display: 'flex',
      padding: 4,
      paddingLeft: 8,
      color: 'white',
      backgroundColor:
        theme.palette.type == 'light'
          ? theme.palette.hanbiroColor.dialogTitle
          : theme.palette.divider
    },
    splitContentView: {
      position: 'absolute',
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      height: '100%'
    },
    mainContent: {
      width: '100%',
      height: 500,
      flexDirection: 'row',
      display: 'flex',
      borderTopColor: theme.palette.divider,
      borderBottomColor: theme.palette.divider,
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderTopStyle: 'solid',
      borderBottomStyle: 'solid'
    },
    leftPanel: {
      width: '100%',
      height: '100%',
      flexDirection: 'column',
      display: 'flex',
      borderRightColor: theme.palette.divider,
      borderRightWidth: 1,
      borderRightStyle: 'solid'
    },
    mainList: {
      width: '100%',
      height: '100%',
      position: 'relative',
      display: 'flex'
    },
    center: {
      width: '100%',
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex'
    },
    centerAbsolute: {
      top: 0,
      left: 0,
      position: 'absolute',
      width: '100%',
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex'
    },
    rightPanel: {
      width: '100%',
      height: '100%'
    },
    historyDetailList: {
      width: '100%',
      height: '100%',
      padding: 20
    },
    historyDetailListChildContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column'
    },
    contentSearch: {
      flexDirection: 'row',
      alignItems: 'center',
      display: 'flex',
      width: '100%',
      paddingLeft: 16,
      paddingRight: 8,
      paddingTop: 8,
      paddingBottom: 8
    },
    inputSearch: { fontSize: theme.typography.fontSize, width: '100%' },
    search: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-start',
      flexDirection: 'row',
      borderBottomColor: theme.palette.divider,
      borderBottomWidth: 1,
      borderBottomStyle: 'solid',
      padding: 4
    },
    searchIcon: {
      marginLeft: 4
    },
    searchIconHightLight: {
      marginLeft: 4,
      color: theme.palette.primary.main
    },
    clearIcon: {
      marginRight: 4
    },
    clearIconHightLight: {
      marginRight: 4,
      color: theme.palette.primary.main
    },
    actionIcon: {
      marginRight: 4
    },
    contentLoadingMore: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    searchOptionsInfo: {
      borderBottomColor: theme.palette.divider,
      borderBottomWidth: 1,
      borderBottomStyle: 'solid',
      alignItems: 'center',
      padding: 4,
      justifyContent: 'center',
      display: 'flex',
      flexDirection: 'row'
    },
    popupSearchOptions: {
      background: theme.palette.background.paper,
      outline: 'none',
      display: 'flex',
      flexDirection: 'column'
    },
    popupSearchOptionsDialogAction: {
      alignItems: 'right',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end'
    }
  })
);
