import React, { Component, useState, useEffect, Fragment } from 'react';
import {
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  ButtonBase,
  Typography,
  InputBase,
  IconButton,
  Menu,
  TextField
} from '@material-ui/core';
import ReplayIcon from '@material-ui/icons/Replay';
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
import ClearOutlined from '@material-ui/icons/ClearOutlined';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import Api from '../../core/service/api';
import talkAPI from '../../core/service/talk.api.render';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../actions';
import { AutoSizer, List } from 'react-virtualized';
import SimpleBarHanbiro from '../SimpleBarHanbiro';
import HistoryInfo from './components/HistoryInfo';
import SplitPane from 'react-split-pane';
import { useTheme } from '@material-ui/core/styles';
import HistoryDetail from './components/HistoryDetail';
import { SortAscIcon, SortDescIcon } from '../HanSVGIcon';

import { Updating } from '../MessageList/components';

import { StaticDateRangePicker } from '@material-ui/pickers';

import {
  STATE_REFRESH,
  STATE_BOTTOM,
  STATE_UP
} from '../../reducers/chat_history';

export default function HistoryDialog(props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const theme = useTheme();
  const classes = useStyles();
  const { onClose, roomKey } = props;

  useEffect(() => {
    return () => {
      dispatch(Actions.clearHistoryList());
    };
  }, []);

  return (
    <Dialog fullWidth={true} maxWidth="xl" open={true} onClose={onClose}>
      <DialogContent style={{ padding: 0, overflow: 'hidden' }}>
        <div className={classes.dialog}>
          <div className={classes.contentTitle}>
            <Typography variant="subtitle1"></Typography>
            <IconButton
              style={{ color: 'white' }}
              aria-label="delete"
              size="small"
              onClick={onClose}
            >
              <ClearOutlined fontSize="small" />
            </IconButton>
          </div>
          <div className={classes.mainContent}>
            <SplitPane
              minSize={250}
              maxSize={400}
              defaultSize={250}
              onChange={size => {
                // localStorage.setItem('splitPos', size);
                // const event = new Event('splitPannelChanged', { size: size });
                // window.dispatchEvent(event);
              }}
              style={{ position: 'relative' }}
              resizerStyle={{
                backgroundColor: theme.palette.divider,
                opacity: 1
              }}
            >
              <div className={classes.splitContentView}>
                <LeftPanel roomKey={roomKey} />
              </div>
              <div className={classes.splitContentView}>
                <RightPanel />
              </div>
            </SplitPane>
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>{t('Close')}</Button>
      </DialogActions>
    </Dialog>
  );
}

HistoryDialog.propTypes = {
  roomKey: PropTypes.string,
  onClose: PropTypes.func
};

HistoryDialog.defaultProps = {
  roomKey: null,
  onClose: () => {}
};

let uniqueKeyHistoryList;
let timerSearch;
function LeftPanel(props) {
  const { roomKey } = props;

  const { t } = useTranslation();
  const historyList = useSelector(state => state.chat_history.historyList);
  const selectedHistory = useSelector(
    state => state.chat_history.selected_history
  );

  const requestKeyword = useSelector(state => state.chat_history.keyword);

  const user = useSelector(state => state.auth.user.account_info);
  const user_cached = useSelector(state => state.company.user_cached);

  const classes = useStyles();
  const [searchFocused, setSearchFocused] = useState(false);
  const [keyword, setKeyword] = useState('');
  const [page, setPage] = useState(0);
  const [loadingMore, setLoadingMore] = useState(false);
  const [requesting, setRequesting] = useState(false);
  const dispatch = useDispatch();

  const [selectedDate, handleDateChange] = useState([null, null]);

  const initSearchState = () => {
    let now = Date.now();
    let week_1 = now - 1000 * 60 * 60 * 24 * 7;
    let endDate = new Date();
    let startDate = new Date(week_1);
    return [moment(startDate), moment(endDate)];
  };

  const [sortOptions, setSortOptions] = useState('desc');
  const [showSearchOptions, setShowSearchOptions] = useState(null);

  const [searchOptions, setSearchOptions] = useState(initSearchState);
  const [searchOptionsControl, setSearchOptionsControl] = useState(
    initSearchState
  );

  /**
   * Load List History
   * @param {*} page
   */
  const loadList = (page, search) => {
    uniqueKeyHistoryList = Date.now();
    if (search.length < 2) {
      setRequesting(false);
      setLoadingMore(false);
      return;
    }

    const startDate = searchOptions[0].format('YYYY-MM-DD');
    const endDate = searchOptions[1].format('YYYY-MM-DD');

    setRequesting(true);
    Api.get(
      talkAPI.historyList(
        startDate,
        endDate,
        page,
        search,
        roomKey,
        sortOptions
      ),
      null,
      null,
      uniqueKeyHistoryList
    )
      .then(response => {
        if (response.rows) {
          dispatch(Actions.saveHistoryList(response.rows, search, page > 0));
          setLoadingMore(response.rows.length >= 20);
        }
        setRequesting(false);
      })
      .catch(error => {
        if (Api.isCancel(error)) {
          return;
        }
        setRequesting(false);
      });
  };

  /**
   * On Change search keyword
   * @param {*} value
   */
  const onChange = value => {
    setKeyword(value);
    if (value.length >= 2) {
      if (timerSearch) {
        clearTimeout(timerSearch);
      }
      timerSearch = setTimeout(() => {
        Api.cancelRequest(uniqueKeyHistoryList);
        dispatch(Actions.clearHistoryList());
        loadList(page, value);
      }, 500);
    } else {
      if (timerSearch) {
        Api.cancelRequest(uniqueKeyHistoryList);
        setRequesting(false);
        setLoadingMore(false);
        clearTimeout(timerSearch);
        dispatch(Actions.clearHistoryList());
      }
    }
  };

  /**
   * Action Reload Whisper page
   * @param {*} page
   */
  const onReload = page => {
    Api.cancelRequest(uniqueKeyHistoryList);
    setPage(page);
    dispatch(Actions.saveHistoryList([]));
    loadList(page, keyword);
  };

  /**
   * onScroll Listener
   * @param {*} e
   */
  const onScroll = e => {
    const { scrollTop, scrollHeight, clientHeight } = e.target;
    if (scrollHeight && scrollHeight - scrollTop == clientHeight) {
      if (loadingMore) {
        const newPage = historyList.length;
        setPage(newPage);
        loadList(newPage, keyword);
      }
    }
  };

  /**
   * Render Row Items
   * @param {*} { index, key, style }
   * @returns
   */
  const Row = ({ index, key, style }) => {
    const loadMore = index == historyList.length;
    if (loadMore) {
      return (
        <div style={style} key={key} className={classes.contentLoadingMore}>
          <div className="han-loading mini" />
        </div>
      );
    }

    const item = historyList[index];

    return (
      <HistoryInfo
        style={style}
        key={key}
        data={item}
        user={user}
        user_cached={user_cached}
        selectedRoom={selectedHistory}
        onClick={() => {
          dispatch(Actions.openHistory(item));
        }}
      ></HistoryInfo>
    );
  };

  /**
   * Item Size
   * @param {*} { index }
   * @returns
   */
  const itemSize = ({ index }) => {
    return 70;
  };

  const handleSearchOptionsShow = e => {
    setSearchOptionsControl(searchOptions);
    setShowSearchOptions(e.currentTarget);
  };

  const handleSearchOptionsClose = () => {
    setShowSearchOptions(null);
  };

  useEffect(() => {
    return () => {
      Api.cancelRequest(uniqueKeyHistoryList);
    };
  }, []);

  useEffect(() => {
    onReload(0);
  }, [searchOptions]);

  useEffect(() => {
    onReload(0);
  }, [sortOptions]);

  const onClosePopup = () => {
    handleSearchOptionsClose();
  };

  const isDateValid = options => {
    const m1 = options[0];
    const m2 = options[1];
    return m1 && m2;
  };

  const onApplyPopup = () => {
    handleSearchOptionsClose();
    if (isDateValid(searchOptionsControl)) {
      setSearchOptions(searchOptionsControl);
    }
  };

  const onSortChange = () => {
    setSortOptions(sortOptions == 'asc' ? 'desc' : 'asc');
  };

  const loading = requesting && page == 0;
  const showNoData =
    requestKeyword?.length >= 2 && historyList?.length == 0 && !requesting;
  const showSearchInfo = keyword.length < 2;
  const stringFromSearchInfo = options => {
    const m1 = options[0];
    const m2 = options[1];
    const startDate = m1 ? m1.format(talkAPI.dateTimeFormat()) : '---';
    const endDate = m2 ? m2.format(talkAPI.dateTimeFormat()) : '---';
    return `${startDate} ~ ${endDate}`;
  };

  return (
    <div className={classes.leftPanel}>
      <div className={classes.search}>
        <IconButton
          onClick={handleSearchOptionsShow}
          size="small"
          className={
            searchFocused ? classes.searchIconHightLight : classes.searchIcon
          }
        >
          <SearchIcon fontSize="small" />
        </IconButton>
        <InputBase
          className={classes.inputSearch}
          style={{ height: 25 }}
          onFocus={() => {
            setSearchFocused(true);
          }}
          onBlur={() => {
            setSearchFocused(false);
          }}
          placeholder={searchFocused ? null : t('Search')}
          value={keyword}
          onChange={event => onChange(event.target.value)}
        />
        <IconButton
          size="small"
          className={
            searchFocused ? classes.clearIconHightLight : classes.clearIcon
          }
          onClick={() => onChange('')}
        >
          <ClearIcon fontSize="small" />
        </IconButton>

        <IconButton
          onClick={() => onReload(0)}
          size="small"
          className={classes.actionIcon}
        >
          <ReplayIcon fontSize="small" />
        </IconButton>

        <IconButton
          onClick={() => onSortChange()}
          size="small"
          className={classes.actionIcon}
        >
          {sortOptions == 'asc' ? (
            <SortAscIcon fontSize="small" />
          ) : (
            <SortDescIcon fontSize="small" />
          )}
        </IconButton>
      </div>
      <ButtonBase
        className={classes.searchOptionsInfo}
        onClick={handleSearchOptionsShow}
      >
        <Typography variant="body2">
          {stringFromSearchInfo(searchOptions)}
        </Typography>
      </ButtonBase>
      {showSearchInfo ? (
        <div className={classes.center}>
          <Typography variant="body2">{t('search_2')}</Typography>
        </div>
      ) : showNoData ? (
        <div className={classes.center}>
          <Typography variant="body2">{t('no_result')}</Typography>
        </div>
      ) : loading ? (
        <div className={classes.center}>
          <div className="han-loading mini" />
        </div>
      ) : (
        <div className={classes.mainList}>
          <HistoryListComponent
            Row={Row}
            onScroll={onScroll}
            itemSize={itemSize}
            historyList={historyList}
            loadingMore={loadingMore}
          ></HistoryListComponent>
        </div>
      )}

      <Menu
        id="simple-menu"
        anchorEl={showSearchOptions}
        open={Boolean(showSearchOptions)}
        onClose={handleSearchOptionsClose}
        PaperProps={{
          style: {
            overflow: 'hidden',
            transform: 'translateY(48%)'
          }
        }}
      >
        <div className={classes.popupSearchOptions}>
          <Typography className={classes.searchOptionsInfo} variant="body2">
            {stringFromSearchInfo(searchOptionsControl)}
          </Typography>

          <StaticDateRangePicker
            inputFormat={'YYYY-MM-DD'}
            showToolbar={false}
            value={searchOptionsControl}
            onChange={date => setSearchOptionsControl(date)}
          />
          <div className={classes.popupSearchOptionsDialogAction}>
            <Button onClick={onApplyPopup}>{t('Apply')}</Button>
            <Button onClick={onClosePopup}>{t('Close')}</Button>
          </div>
        </div>
      </Menu>
    </div>
  );
}

let uniqueKeyHistoryDetail;
function RightPanel(props) {
  const { t } = useTranslation();
  const theme = useTheme();
  const classes = useStyles();
  const dispatch = useDispatch();
  const [messageTimeBottom, setMessageTimeBottom] = useState('');
  const [messageTimeTop, setMessageTimeTop] = useState('');
  const [requestState, setRequestState] = useState(STATE_REFRESH);
  const [requesting, setRequesting] = useState(false);
  const user = useSelector(state => state.auth.user.account_info);
  const selectedHistory = useSelector(
    state => state.chat_history.selected_history
  );
  const historyDetailList = useSelector(
    state => state.chat_history.history_detail
  );
  const historyDetailListUI = useSelector(
    state => state.chat_history.history_detail_UI
  );

  const loadList = (roomKey, state) => {
    uniqueKeyHistoryDetail = Date.now();

    let downcount = 20;
    let upcount = 20;
    let searchTime = selectedHistory.SEARCH_MESSAGE_TIME;
    if (state == STATE_BOTTOM) {
      upcount = 0;
      searchTime = messageTimeBottom;
    } else if (state == STATE_UP) {
      downcount = 0;
      searchTime = messageTimeTop;
    }

    if (!selectedHistory.SEARCH_MESSAGE_TIME) {
      return;
    }

    setRequestState(state);
    setRequesting(true);
    Api.get(
      talkAPI.historyDetailList(roomKey, searchTime, downcount, upcount),
      null,
      null,
      uniqueKeyHistoryDetail
    )
      .then(response => {
        if (response.rows) {
          if (response.rows.length > 0) {
            if (state == STATE_REFRESH) {
              setMessageTimeTop(response.rows[0].time);
              setMessageTimeBottom(
                response.rows[response.rows.length - 1].time
              );
            } else if (state == STATE_UP) {
              setMessageTimeTop(response.rows[0].time);
            } else if (state == STATE_BOTTOM) {
              setMessageTimeBottom(
                response.rows[response.rows.length - 1].time
              );
            }
          }
          dispatch(
            Actions.saveHistoryDetail(response.rows, state, user.user_key)
          );
        }
        setRequesting(false);
      })
      .catch(error => {
        if (Api.isCancel(error)) {
          return;
        }
        setRequesting(false);
      });
  };

  useEffect(() => {
    return () => {
      Api.cancelRequest(uniqueKeyHistoryDetail);
    };
  }, []);

  useEffect(() => {
    Api.cancelRequest(uniqueKeyHistoryDetail);
    dispatch(Actions.saveHistoryDetail([], 0, user.user_key));
    loadList(selectedHistory.ROOMKEY, STATE_REFRESH);
  }, [selectedHistory]);

  const onScroll = e => {
    if (requesting) {
      return;
    }
    let element = e.target;
    if (element.scrollTop == 0) {
      loadList(selectedHistory.ROOMKEY, STATE_UP);
    } else {
      const diffScrollTop = element.scrollHeight - element.clientHeight;
      const scrollAtBottom = diffScrollTop - element.scrollTop <= 0;
      if (scrollAtBottom) {
        loadList(selectedHistory.ROOMKEY, STATE_BOTTOM);
      }
    }
  };

  const loading = requesting == true && requestState == STATE_REFRESH;

  return (
    <div className={classes.rightPanel}>
      {selectedHistory.SEARCH_MESSAGE_TIME ? (
        <Fragment>
          <Updating
            visible={requesting && requestState == STATE_UP}
            message={t('Updating conversation')}
          />
          <HistoryDetailListComponent
            onScroll={onScroll}
            historyDetailList={historyDetailListUI}
            classes={classes}
          />
          <Updating
            atBottom={true}
            visible={requesting && requestState == STATE_BOTTOM}
            message={t('Updating conversation')}
          />
          {loading && (
            <div className={classes.centerAbsolute}>
              <div className="han-loading mini" />
            </div>
          )}
        </Fragment>
      ) : (
        <div
          style={{
            flexDirection: 'column',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            height: '100%'
          }}
        >
          <img
            draggable="false"
            className={classes.logo}
            src={
              theme.palette.type == 'light'
                ? './images/talk_icon_v2.svg'
                : './images/talk_icon_dark_v2.svg'
            }
            style={{ width: 200, marginBottom: 50 }}
          />
        </div>
      )}
    </div>
  );
}

class HistoryListComponent extends Component {
  constructor(props) {
    super(props);

    this.simpleBar = null;
    this.parentRef = React.createRef();
    this.listRef = React.createRef();
    this.onScroll = this.onScroll.bind(this);
  }

  componentDidMount() {
    this.simpleBar = new SimpleBarHanbiro(this.parentRef.current);
  }

  onScroll(e) {
    const { onScroll } = this.props;
    const { scrollTop, scrollLeft } = e.target;
    const { Grid: grid } = this.listRef.current;
    grid.handleScrollEvent({ scrollTop, scrollLeft });
    if (onScroll) {
      onScroll(e);
    }
  }

  render() {
    const { Row, itemSize, historyList, loadingMore } = this.props;
    return (
      <AutoSizer>
        {({ height, width }) => {
          return (
            <div
              ref={this.parentRef}
              onScroll={this.onScroll}
              style={{
                position: 'relative',
                width: width,
                height: height
              }}
            >
              <List
                style={{ overflowX: false, overflowY: false, outline: 'none' }}
                ref={this.listRef}
                height={height}
                rowCount={historyList.length + (loadingMore ? 1 : 0)}
                rowHeight={itemSize}
                width={width}
                rowRenderer={Row}
              ></List>
            </div>
          );
        }}
      </AutoSizer>
    );
  }
}

class HistoryDetailListComponent extends Component {
  constructor(props) {
    super(props);

    this.simpleBar = null;
    this.listRef = React.createRef();
    this.selectedRef = React.createRef();
    this.onSelectedMount = this.onSelectedMount.bind(this);
  }

  onSelectedMount() {
    if (this.selectedRef.current) {
      this.simpleBar.scrollTo(this.selectedRef.current.offsetTop);
    }
  }

  componentDidMount() {
    this.simpleBar = new SimpleBarHanbiro(this.listRef.current);
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    // Are we adding new items to the list?
    // Capture the scroll position so we can adjust scroll later.
    if (
      prevProps.historyDetailList.length <
        this.props.historyDetailList.length &&
      prevProps.historyDetailList.length > 0
    ) {
      const prevMsg = prevProps.historyDetailList[0];
      const nextMsg = this.props.historyDetailList[0];
      if (
        prevMsg &&
        prevMsg.primaryKey != nextMsg.primaryKey &&
        this.simpleBar
      ) {
        const prev_scroll_top =
          this.simpleBar.getScrollHeight() - this.simpleBar.getScrollTop();
        return {
          prev_scroll_top: prev_scroll_top
        };
      }
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (snapshot !== null) {
      this.simpleBar.scrollTo(
        this.simpleBar.getScrollHeight() - snapshot.prev_scroll_top
      );
    }
  }

  render() {
    const { historyDetailList, classes, onScroll } = this.props;
    return (
      <div
        className={classes.historyDetailList}
        ref={this.listRef}
        onScroll={onScroll}
      >
        <div className={classes.historyDetailListChildContainer}>
          {historyDetailList.map((element, index) => {
            return (
              <div
                key={element.primaryKey}
                ref={element.isCenterMessage ? this.selectedRef : null}
                className={classes.historyDetailListChildContainer}
              >
                <HistoryDetail
                  data={element}
                  onSelectedMount={this.onSelectedMount}
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
