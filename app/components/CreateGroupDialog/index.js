import React, { useState } from 'react';
import {
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  Typography,
  IconButton,
  TextField
} from '@material-ui/core';
import ClearOutlined from '@material-ui/icons/ClearOutlined';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

export default function CreateGroupDialog(props) {
  const { t } = useTranslation();
  const classes = useStyles();
  const { onClose, item, onUpdate } = props;
  const isCreateNew = item == null;

  const [title, setTitle] = useState(isCreateNew ? '' : item.displayName);

  return (
    <Dialog fullWidth={true} maxWidth="sm" open={true} onClose={onClose}>
      <DialogContent style={{ padding: 0 }}>
        <div className={classes.dialog}>
          <div className={classes.contentTitle}>
            <Typography variant="subtitle1">
              {t(isCreateNew ? 'Create group' : 'Edit group')}
            </Typography>
            <IconButton
              style={{ color: 'white' }}
              aria-label="delete"
              size="small"
              onClick={onClose}
            >
              <ClearOutlined fontSize="small" />
            </IconButton>
          </div>
          <div className={classes.mainContent}>
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              label={t('Name')}
              error={false}
              value={title}
              onKeyPress={ev => {
                if (ev.key === 'Enter') {
                  onUpdate(item, title, {});
                }
              }}
              onChange={event => {
                setTitle(event.target.value);
              }}
            />
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>{t('Close')}</Button>
        <Button
          onClick={() => {
            onUpdate(item, title, {});
          }}
          color="primary"
        >
          {t(isCreateNew ? 'Create' : 'Update')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

CreateGroupDialog.propTypes = {
  open: PropTypes.bool,
  item: PropTypes.object,
  onClose: PropTypes.func,
  onUpdate: PropTypes.func
};

CreateGroupDialog.defaultProps = {
  open: false,
  item: {},
  onClose: () => {},
  onUpdate: (item, newTitle, extraInfo) => {}
};
