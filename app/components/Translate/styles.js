import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    main: {
      display: 'flex',
      minWidth: 50,
      minHeight: 50,
      padding: 8,
      borderRadius: 8,
      flexDirection: 'column',
      backgroundColor: theme.palette.background.paper,
      maxWidth: 350
    },
    row: {
      flexDirection: 'row',
      display: 'flex',
      alignItems: 'center'
    },
    formControl: {
      minWidth: 100,
      maxWidth: 150,
      fontSize: 6
    },
    outlined: {
      padding: 8,
      fontSize: 12
    },
    label: {
      fontSize: 13
    },
    item: {
      minWidth: 60,
      fontSize: 12,
      paddingTop: 3,
      paddingBottom: 3
    }
  })
);
