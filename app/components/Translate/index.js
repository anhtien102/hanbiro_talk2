import React, { useState, useEffect } from 'react';
import {
  FormControl,
  InputLabel,
  MenuItem,
  Typography,
  Select,
  IconButton
} from '@material-ui/core';
import RefreshIcon from '@material-ui/icons/Refresh';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { gql } from '@apollo/client';
import { remote } from 'electron';
import GraphQL from '../../core/service/graphql';
import { useSelector } from 'react-redux';
import * as Utils from '../../utils';
import * as constantsApp from '../../configs/constant';
export default function Translate(props) {
  const { t, i18n } = useTranslation();
  const classes = useStyles();
  const user = useSelector(state => state.auth.user);
  const selectedLanguage = Utils.getDataFromStorage(
    constantsApp.TRANSLATE_SELECTED
  );
  let retry = 3;
  let textTranslate = JSON.stringify(props?.text ?? '');
  textTranslate = textTranslate.substring(1, textTranslate.length - 1);

  const [language, setLanguage] = useState(selectedLanguage ?? i18n.language);
  const [loading, setLoading] = useState(true);
  const [message, setMessage] = useState(textTranslate);
  const [error, setError] = useState(false);

  const updateViewPosition = () => {
    setTimeout(() => {
      props.updatePosition();
    }, 50);
  };

  useEffect(() => {
    translate();
    Utils.saveDataToStorage(constantsApp.TRANSLATE_SELECTED, language);
  }, [language]);

  /**
   * reauthenticate then translate again
   *
   */
  const authenticate = () => {
    const { GROUPWARE_TOKEN } = remote.getGlobal('ShareKeepValue');
    const GET_TOKEN = gql`
      mutation create(
        $hanbiro_auth: String!
        $host: String!
        $category: String!
      ) {
        create(hanbiro_auth: $hanbiro_auth, host: $host, category: $category) {
          access_token
          refresh_token
        }
      }
    `;
    GraphQL.authClient
      .mutate({
        mutation: GET_TOKEN,
        variables: {
          hanbiro_auth: GROUPWARE_TOKEN,
          host: user.account_info.domain,
          category: 'translate'
        }
      })
      .then(result => {
        GraphQL.setToken({
          token: result.data?.create?.access_token,
          refresh_token: result.data?.create?.refresh_token
        });
        translate();
      })
      .catch(error => {
        updateViewPosition();
        setLoading(false);
        setMessage('Authenticate Error');
        console.log('authenticate error', error);
      });
  };

  /**
   * refresh token then translate again
   *
   */
  const refreshToken = () => {
    const REFRESH_TOKEN = gql`
      mutation refresh($access_token: String!, $refresh_token: String!) {
        refresh(access_token: $access_token, refresh_token: $refresh_token) {
          access_token
          refresh_token
        }
      }
    `;
    GraphQL.authClient
      .mutate({
        mutation: REFRESH_TOKEN,
        variables: {
          access_token: GraphQL.token,
          refresh_token: GraphQL.refreshToken
        }
      })
      .then(result => {
        GraphQL.setToken({
          token: result.data?.refresh?.access_token,
          refresh_token: result.data?.refresh?.refresh_token
        });
        translate();
      })
      .catch(error => {
        console.log('refreshToken error', error);
        authenticate();
      });
  };

  /**
   * fetch translate text
   */
  const translate = () => {
    if (retry == 0) return;
    const TRANSLATE_LANGUAGE = gql`
      query {
        translate(
          user_id: "${user.account_info?.user_id}"
          user_name: "${user.extra_login_info?.login?.username}"
          source_lang: "auto"
          target_lang: "${language}"
          text: "${textTranslate}"
        ) {
          source_text
          source_lang
          target_text
        }
      }
    `;
    GraphQL.translateClient
      .query({
        query: TRANSLATE_LANGUAGE
      })
      .then(result => {
        updateViewPosition();
        setLoading(false);
        setMessage(result.data?.translate?.target_text);
      })
      .catch(error => {
        retry = retry - 1;
        refreshToken();
      });
  };

  /**
   * onChange language
   * @param {*} event
   */
  const handleChange = event => {
    setLanguage(event.target.value);
  };

  return (
    <div className={classes.main}>
      {loading ? (
        <div style={{ padding: 4 }} className="han-loading mini" />
      ) : (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <div className={classes.row}>
            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel
                id="demo-simple-select-outlined-label"
                classes={{
                  root: classes.label
                }}
              >
                {t('Language')}
              </InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={language}
                onChange={handleChange}
                label={t('Language')}
                classes={{
                  outlined: classes.outlined
                }}
              >
                <MenuItem classes={{ root: classes.item }} value={'vi'}>
                  {t('Vietnamese')}
                </MenuItem>
                <MenuItem classes={{ root: classes.item }} value={'ko'}>
                  {t('Korean')}
                </MenuItem>
                <MenuItem classes={{ root: classes.item }} value={'en'}>
                  {t('English')}
                </MenuItem>
              </Select>
            </FormControl>
            {error && (
              <IconButton
                aria-label="refresh"
                size="small"
                style={{ marginLeft: 8 }}
                onClick={refreshToken}
              >
                <RefreshIcon fontSize="small" />
              </IconButton>
            )}
          </div>
          <Typography variant="body2" style={{ marginTop: 8 }}>
            {message}
          </Typography>
        </div>
      )}
    </div>
  );
}

Translate.propTypes = {
  text: PropTypes.string
};

Translate.defaultProps = {
  text: ''
};
