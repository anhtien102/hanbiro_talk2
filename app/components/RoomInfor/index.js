import React, { Component } from 'react';
import { useStyles } from './styles';
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  Badge,
  Typography
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import BookmarkIcon from '@material-ui/icons/Bookmark';
import UserGroupPhoto from '../../components/UserGroupPhoto';
import UserPhotoView from '../../components/UserPhoto';
import NotificationsOffIcon from '@material-ui/icons/NotificationsOff';
import {
  ROOM_TYPE_GUEST,
  ROOM_TYPE_LIVE,
  StatusMode
} from '../../core/service/talk-constants';
import TimeAgo from 'javascript-time-ago';
import StarIcon from '@material-ui/icons/Star';
import en from 'javascript-time-ago/locale/en';
import vi from 'javascript-time-ago/locale/vi';
import ko from 'javascript-time-ago/locale/ko';
import PropTypes from 'prop-types';
import TalkIcon from '../../components/TalkIcon';

import { ensureFilePath } from '../../configs/file_cheat';

import { timeAgoStringWithTimeAgoLib, tickerEmojManager } from '../../utils';

TimeAgo.addLocale(en);
TimeAgo.addLocale(vi);
TimeAgo.addLocale(ko);

export default function RoomInfor(props) {
  const { t, i18n } = useTranslation();
  const classes = useStyles();
  const {
    data,
    style,
    selectedRoom,
    user,
    user_cached,
    onClick,
    enableUnread,
    onOpenMenu
  } = props;

  const getEmoticon = name => {
    return tickerEmojManager.filePathFromName(name);
  };

  /**
   * return UI status icon
   * @param {*} status
   * @returns
   */
  const exportIconStatus = status => {
    switch (status) {
      case StatusMode.logout:
      case StatusMode.offline:
        return (
          <div className={classes.contentStatusSmall}>
            <div className={classes.circleOffline} />
          </div>
        );
      case StatusMode.available:
        return (
          <div className={classes.contentStatusSmall}>
            <div className={classes.circleOnline} />
          </div>
        );
      case StatusMode.away:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="idle" />
          </div>
        );
      case StatusMode.busy:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="busy" />
          </div>
        );
      case StatusMode.meeting:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="meeting" />
          </div>
        );
      case StatusMode.meal:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="meal" />
          </div>
        );
      case StatusMode.phone:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="call" />
          </div>
        );
      case StatusMode.out:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="out" />
          </div>
        );
      case StatusMode.business_trip:
        return (
          <div className={classes.contentStatusLarge}>
            <TalkIcon name="business_trip" />
          </div>
        );

      default:
        return null;
    }
  };

  const timeAgo = new TimeAgo(i18n.language);
  const roomKeySelected = selectedRoom ? selectedRoom.rRoomKey : '';
  const selected = data.rRoomKey == roomKeySelected;
  const unReadMessage = enableUnread && data.unReadMessage == 1 ? true : false;
  const messageTime = new Date(data.rRoomServerLastMsgTime);
  const enableRoomTag = data.rRoomTag && data.rRoomTag != '';

  const lastUserSenderMessage =
    data.rRoomServerLastSenderUserKey &&
    data.rRoomServerLastSenderUserKey != '' &&
    data.roomDetailds.length > 2
      ? user_cached[data.rRoomServerLastSenderUserKey]?.displayName
        ? user_cached[data.rRoomServerLastSenderUserKey]?.displayName + ': '
        : '- :'
      : null;
  let lastMessage = data.rRoomServerLastMsgBody;
  const lastMessageEmoticon = data.rRoomServerLastMsgEmoticon;

  if (lastMessageEmoticon && lastMessageEmoticon != '') {
    lastMessage = t('EMOTICON');
    // lastMessage = (
    //   <EmotionImage t={t} path={getEmoticon(lastMessageEmoticon)} />
    // );
  }
  if (data.rRoomServerLastMsgDeleted == 1) {
    lastMessage = t('This message has been deleted');
  }

  const enableFavorite = data.rRoomFavorite == 1;
  const disableNotification = data.rRoomPushAlert == 0;
  const customerRoom =
    data.rRoomType == ROOM_TYPE_GUEST || data.rRoomType == ROOM_TYPE_LIVE;

  let userStatus;
  const showStatus = data && data.roomDetailds.length < 3;
  if (showStatus) {
    const list = data.roomDetailds.filter(
      item => item.rdtUKey != user.user_key
    );
    let userShow = list.length > 0 ? list[0] : null;
    if (userShow?.rdtUKey && user_cached.hasOwnProperty(userShow.rdtUKey)) {
      userShow = user_cached[userShow.rdtUKey];
      userStatus = exportIconStatus(userShow?.userStatus);
    }
  }

  const timeDisplay = timeAgoStringWithTimeAgoLib(messageTime, timeAgo); // timeAgoString(messageTime) ?? timeAgo.format(messageTime);

  return (
    <ListItem
      className={customerRoom ? classes.customerRoom : classes.item}
      button
      // divider={true}
      selected={selected}
      onClick={onClick}
      style={style}
      onContextMenu={event => {
        event.preventDefault();
        onOpenMenu(event);
      }}
    >
      <ListItemIcon>
        {customerRoom ? (
          <UserPhotoView
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              width: 50,
              height: 50,
              borderRadius: '50%',
              backgroundColor: 'white'
            }}
            imgSize={50}
            placeHolder={'./images/customter_avatar.png'}
          />
        ) : (
          <UserGroupPhoto
            userList={data.roomDetailds}
            user={user}
            userStatus={userStatus}
          />
        )}
      </ListItemIcon>
      <ListItemText
        primary={
          <Typography variant="subtitle2" className={classes.textUser}>
            {data?.displayName}
          </Typography>
        }
        secondary={
          <React.Fragment>
            <Typography
              variant="caption"
              color="textSecondary"
              className={
                unReadMessage ? classes.textPrimary : classes.textMessage
              }
            >
              {lastUserSenderMessage}
              {lastMessage}
            </Typography>

            {enableRoomTag ? (
              <span
                style={{
                  flexDirection: 'row',
                  display: 'flex',
                  alignItems: 'center'
                }}
              >
                <BookmarkIcon className={classes.tagIcon} />
                <span className={classes.textTag}>{t(data.rRoomTag)}</span>
              </span>
            ) : null}
          </React.Fragment>
        }
      />
      <div style={{ display: 'flex', flexDirection: 'row', height: '100%' }}>
        <div className={classes.contentRight}>
          {unReadMessage ? (
            <Badge
              className={classes.badge}
              badgeContent={data.rRoomUnreadMsgServer}
              max={99}
              color="error"
            ></Badge>
          ) : null}
          {enableFavorite ? (
            <StarIcon style={{ fontSize: 12, color: 'orange' }} />
          ) : null}
          {disableNotification ? (
            <NotificationsOffIcon className={classes.tagIcon} />
          ) : null}
          <Typography
            variant="caption"
            color="textSecondary"
            className={classes.textTime}
          >
            {timeDisplay}
          </Typography>
        </div>
        <div className={classes.contentSelect}>
          {selected && <div className={classes.selectedItem}></div>}
        </div>
      </div>
    </ListItem>
  );
}

RoomInfor.propTypes = {
  data: PropTypes.object,
  style: PropTypes.object,
  user: PropTypes.object,
  user_cached: PropTypes.object,
  selectedRoom: PropTypes.object,
  enableUnread: PropTypes.bool,
  onClick: PropTypes.func,
  onOpenMenu: PropTypes.func
};

RoomInfor.defaultProps = {
  data: {},
  style: {},
  user: {},
  user_cached: {},
  selectedRoom: {},
  enableUnread: true,
  onClick: () => {},
  onOpenMenu: () => {}
};

class EmotionImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      failed: false
    };
    this.onError = this.onError.bind(this);
  }
  onError() {
    this.setState({
      failed: true
    });
  }

  render() {
    const { path, t } = this.props;
    console.log('WTF', path);
    const { failed } = this.state;

    const w = 20;
    const h = 20;

    return failed ? (
      t('EMOTICON')
    ) : (
      <img
        style={{ width: w, height: h }}
        src={ensureFilePath(path)}
        onError={this.onError}
      />
    );
  }
}
