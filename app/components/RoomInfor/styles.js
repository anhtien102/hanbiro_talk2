import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    textUser: {
      display: 'block',
      maxWidth: 'calc(100% - 20px)',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    textUserCustomer: {
      display: 'block',
      maxWidth: 'calc(100% - 20px)',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      fontWeight: 'bold',
      color: 'white'
    },
    item: {
      paddingTop: 0,
      paddingBottom: 0,
      paddingRight: 0
    },
    circleOffline: {
      backgroundColor: '#cbced1',
      width: 10,
      height: 10,
      borderRadius: '50%'
    },
    circleOnline: {
      backgroundColor: theme.palette.success.light,
      width: 10,
      height: 10,
      borderRadius: '50%'
    },
    contentStatusSmall: {
      position: 'absolute',
      bottom: 5,
      right: -3,
      width: 14,
      height: 14,
      backgroundColor: 'white',
      borderRadius: '50%',
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex'
    },
    contentStatusLarge: {
      position: 'absolute',
      bottom: 5,
      right: -3,
      width: 18,
      height: 18,
      backgroundColor: 'white',
      borderRadius: '50%',
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex'
    },
    customerRoom: {
      paddingTop: 0,
      paddingBottom: 0,
      paddingRight: 0,
      backgroundColor: '#8392a5'
    },
    contentSelect: {
      width: 8,
      height: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end'
    },
    selectedItem: {
      width: 4,
      height: '100%',
      backgroundColor: theme.palette.primary.main
    },
    textMessage: {
      display: 'block',
      maxWidth: 'calc(100% - 20px)',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    textMessageCustomer: {
      display: 'block',
      maxWidth: 'calc(100% - 20px)',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      color: 'white'
    },
    textPrimary: {
      color: theme.palette.primary.main,
      display: 'block',
      maxWidth: 'calc(100% - 20px)',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    },
    textTime: {
      display: 'block',
      whiteSpace: 'nowrap',
      fontSize: 11,
      paddingBottom: 4
    },
    textTimeCustomer: {
      display: 'block',
      whiteSpace: 'nowrap',
      fontSize: 10,
      paddingBottom: 4,
      color: 'white'
    },
    contentMessage: {
      display: 'flex',
      width: '100%',
      height: '100%',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'center',
      backgroundColor: 'red'
    },
    contentRight: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'flex-end',
      alignItems: 'flex-end',
      height: '100%'
    },
    contentTime: {
      display: 'flex',
      width: '30%',
      height: '100%',
      flexDirection: 'column',
      alignItems: 'flex-end',
      justifyContent: 'center',
      backgroundColor: 'blue'
    },
    tagIcon: {
      fontSize: 12,
      color: theme.palette.primary.main
    },
    textTag: {
      display: 'block',
      whiteSpace: 'nowrap',
      fontSize: 10,
      color: theme.palette.primary.main
    },
    badge: {
      right: 6,
      top: -10
    }
  })
);
