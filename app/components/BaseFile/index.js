const getImg = type => {
  switch (type) {
    case 'asp':
      return './images/ico-file-asp.svg';
    case 'avi':
      return './images/ico-file-avi.svg';
    case 'bmp':
      return './images/ico-file-bmp.svg';
    case 'chm':
      return './images/ico-file-chm.svg';
    case 'css':
      return './images/ico-file-css.svg';
    case 'doc':
      return './images/ico-file-doc.svg';
    case 'exe':
      return './images/ico-file-exe.svg';
    case 'gif':
      return './images/ico-file-gif.svg';
    case 'gz':
      return './images/ico-file-gz.svg';
    case 'htm':
      return './images/ico-file-htm.svg';
    case 'html':
      return './images/ico-file-html.svg';
    case 'hwp':
      return './images/ico-file-hwp.svg';
    case 'java':
      return './images/ico-file-java.svg';
    case 'js':
      return './images/ico-file-js.svg';
    case 'jsp':
      return './images/ico-file-jsp.svg';
    case 'mp3':
      return './images/ico-file-mp3.svg';
    case 'mp4':
      return './images/ico-file-mp4.svg';
    case 'pdf':
      return './images/ico-file-pdf.svg';
    case 'php':
      return './images/ico-file-php.svg';
    case 'png':
      return './images/ico-file-png.svg';
    case 'ppt':
      return './images/ico-file-ppt.svg';
    case 'rar':
      return './images/ico-file-rar.svg';
    case 'smi':
      return './images/ico-file-smi.svg';
    case 'swf':
      return './images/ico-file-swf.svg';
    case 'txt':
      return './images/ico-file-txt.svg';
    case 'wav':
      return './images/ico-file-wav.svg';
    case 'wmv':
      return './images/ico-file-wmv.svg';
    case 'xls':
      return './images/ico-file-xls.svg';
    case 'zip':
      return './images/ico-file-zip.svg';

    default:
      return './images/ico-file-default.svg';
  }
};

const imgExtension = ['png', 'jpg', 'jpeg', 'gif'];

export { getImg, imgExtension };
