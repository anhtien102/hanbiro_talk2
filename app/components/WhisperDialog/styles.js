import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>
  createStyles({
    dialog: {
      width: '100%',
      flexDirection: 'column',
      display: 'flex'
    },
    contentTitle: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      display: 'flex',
      padding: 4,
      paddingLeft: 8,
      color: 'white',
      backgroundColor:
        theme.palette.type == 'light'
          ? theme.palette.hanbiroColor.dialogTitle
          : theme.palette.divider
    },
    mainContent: {
      width: '100%',
      height: '100%',
      flexDirection: 'column',
      display: 'flex',
      // borderTopColor: theme.palette.divider,
      // borderBottomColor: theme.palette.divider,
      // borderTopWidth: 1,
      // borderBottomWidth: 1,
      // borderTopStyle: 'solid',
      // borderBottomStyle: 'solid',
      paddingLeft: 16,
      paddingRight: 16,
      paddingTop: 16
    },
    userSelectedContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingBottom: 8,
      width: '100%'
    },
    bodyContainer: {
      overflow: 'hidden',
      position: 'relative',
      display: 'flex',
      flexDirection: 'column'
    },
    listSelectedUser: {
      position: 'relative',
      display: 'flex',
      flexWrap: 'wrap',
      flexDirection: 'row',
      overflow: 'hidden'
    },
    listSelectedUserContainer: {
      maxHeight: 135,
      width: '100%'
    },
    userItem: {
      display: 'flex',
      position: 'relative',
      alignItems: 'center',
      justifyItems: 'start',
      borderRadius: 45,
      height: 30,
      background: 'rgba(0,0,0,0.1)',
      marginRight: 8,
      marginBottom: 8
    },
    iconUserItem: {
      display: 'flex',
      width: 30,
      height: 30,
      alignItems: 'center',
      justifyContent: 'center'
    },
    chatAction: {
      paddingTop: 8,
      paddingBottom: 8,
      borderLeft: 1,
      borderLeftStyle: 'solid',
      borderLeftColor: theme.palette.divider,
      borderRight: 1,
      borderRightStyle: 'solid',
      borderRightColor: theme.palette.divider,
      borderTop: 1,
      borderTopStyle: 'solid',
      borderTopColor: theme.palette.divider
    },
    chatButton: {
      marginLeft: 15,
      marginRight: 15,
      padding: '4px 0px 0px 0px',
      color: '#767676',
      background: 'none',
      border: 'none',
      borderRadius: '10px',
      cursor: 'pointer',
      outline: 'none',
      '&:hover': {
        color: theme.palette.primary.main
      }
    },
    contentUpload: {
      position: 'absolute',
      width: '100%',
      height: 200,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    }
  })
);
