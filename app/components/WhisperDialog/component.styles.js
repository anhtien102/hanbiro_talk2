import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
export const CSSButtonStyle = withStyles(theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.main
    },
    '& .MuiButton-label': {
      color: 'white'
    }
  }
}))(Button);
