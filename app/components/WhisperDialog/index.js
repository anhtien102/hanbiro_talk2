import React, { useState, useEffect, Component, useRef } from 'react';
import {
  Dialog,
  DialogContent,
  DialogActions,
  ButtonBase,
  Typography,
  IconButton,
  Snackbar,
  CircularProgress
} from '@material-ui/core';
import ClearOutlined from '@material-ui/icons/ClearOutlined';
import Clear from '@material-ui/icons/Clear';
import { useStyles } from './styles';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import WhisperDropFile from '../WhisperDropFile';
import UserPhotoView from '../UserPhoto';
import FileInput from '../FileInput';
import WhisperEditor from '../WhisperEditor';
import ImageOutlinedIcon from '@material-ui/icons/ImageOutlined';
import AttachFileOutlinedIcon from '@material-ui/icons/AttachFileOutlined';
import { userKeyToWhisperKey } from '../../screens/home/whisper/utils';
import Api from '../../core/service/api';
import talkAPI from '../../core/service/talk.api.render';
import fs from 'fs';
import { CSSButtonStyle } from './component.styles';
import { AddUserIcon } from '../HanSVGIcon';
import SimpleBarHanbiro from '../SimpleBarHanbiro';

let writeUniqueKey = null;
let whisperEditor = null;
let scrollBottom = true;
export default function WhisperDialog(props) {
  const { t } = useTranslation();
  const classes = useStyles();
  const { onClose, contacts, onSelectUser } = props;
  const [openSnackbar, setOpenSnackbar] = useState({ show: false, msg: '' });
  const [selectedUsers, setSelectedUsers] = useState(contacts);
  const [percentUpload, setPercentUpload] = useState(0);
  const scrollView = useRef(null);

  const deleteUserSelected = selected => {
    setSelectedUsers(
      selectedUsers.filter(item => item.userKey != selected.userKey)
    );
  };

  const handleDrop = e => {
    e.preventDefault();
    e.stopPropagation();
    if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
      if (whisperEditor) {
        let files = [];
        for (let f of e.dataTransfer.files) {
          const item = { name: f.name, path: f.path, size: f.size };
          files.push(item);
        }
        whisperEditor.addAttachment(files);
      }
      e.dataTransfer.clearData();
    }
  };

  const addFile = fileData => {
    if (whisperEditor) {
      let files = [];
      for (let f of fileData) {
        const item = { name: f.name, path: f.path, size: f.size };
        files.push(item);
      }
      whisperEditor.addAttachment(files);
    }
  };

  const onSendWhisper = (enterText, attachments) => {
    if (selectedUsers.length > 0 && (enterText || attachments.length > 0))
      sendWriteWhisperAPI(enterText, attachments, respone => {
        if (respone.success) {
          setOpenSnackbar({ show: true, msg: t('Send whisper success') });
          whisperEditor.onClear();
          whisperEditor.clearAttachment();
        } else {
          setOpenSnackbar({ show: true, msg: respone.msg });
        }
      });
  };

  const readFile = file => {
    return new Promise((resolve, reject) => {
      fs.readFile(file.path, (err, data) => {
        if (err) {
          resolve(null);
          return;
        }
        resolve(data);
      });
    });
  };

  const sendWriteWhisperAPI = async (enterText, attachments, callback) => {
    let listTo = [];
    for (let user of selectedUsers) {
      listTo.push(userKeyToWhisperKey(user.userKey));
    }
    let to = listTo.join(',');
    let body = new FormData();
    body.append('to', to);
    body.append('memo', enterText ?? '');
    body.append('bigdata_enabled', 'true');
    let i = 0;
    for (let att of attachments) {
      const data = await readFile(att);
      if (data) {
        i++;
        const file = new File([data], att.name);
        body.append('file' + i, file);
      }
    }
    writeUniqueKey = Date.now();
    Api.postWithPercent(
      talkAPI.whisperWrite(),
      body,
      null,
      writeUniqueKey,
      percent => {
        setPercentUpload(percent);
      }
    )
      .then(respone => {
        callback(respone);
        setPercentUpload(0);
      })
      .catch(error => {
        callback({ success: false, msg: error?.message ?? 'unknown' });
        setPercentUpload(0);
      });
  };

  useEffect(() => {
    whisperEditor = null;
    return () => {
      Api.cancelRequest(writeUniqueKey);
      whisperEditor = null;
      writeUniqueKey = null;
    };
  }, []);

  useEffect(() => {
    if (scrollBottom) {
      scrollView?.current?.scrollBottom();
      scrollBottom = false;
    }
  }, [selectedUsers]);

  const allowFileTransfer = talkAPI.allowFileTransfer();

  return (
    <Dialog fullWidth={true} maxWidth="md" open={true} onClose={onClose}>
      <DialogContent style={{ padding: 0, overflow: 'hidden' }}>
        <div className={classes.dialog}>
          <div className={classes.contentTitle}>
            <Typography variant="subtitle1">{t('Whisper Write')}</Typography>
            <IconButton
              style={{ color: 'white' }}
              aria-label="delete"
              size="small"
              onClick={onClose}
            >
              <ClearOutlined fontSize="small" />
            </IconButton>
          </div>
          <div className={classes.mainContent}>
            <div className={classes.userSelectedContainer}>
              <PerfectScrollSelectedUser
                ref={scrollView}
                classes={classes}
                selectedUsers={selectedUsers}
                onClickAdd={() => {
                  onSelectUser(
                    newSelected => {
                      scrollBottom = true;
                      setSelectedUsers(newSelected);
                    },
                    selectedUsers,
                    1000,
                    true
                  );
                }}
                deleteUserSelected={deleteUserSelected}
              />
            </div>
            <div className={classes.chatAction}>
              {allowFileTransfer && (
                <>
                  <FileInput
                    accept="image/*"
                    style={{ display: 'inline' }}
                    onChange={addFile}
                  >
                    <button type="button" className={classes.chatButton}>
                      <ImageOutlinedIcon />
                    </button>
                  </FileInput>
                  <FileInput style={{ display: 'inline' }} onChange={addFile}>
                    <button type="button" className={classes.chatButton}>
                      <AttachFileOutlinedIcon />
                    </button>
                  </FileInput>
                </>
              )}
            </div>
            <div className={classes.bodyContainer}>
              {allowFileTransfer && (
                <WhisperDropFile
                  contextId={'whisper_editor'}
                  onSend={handleDrop}
                />
              )}

              <div id="whisper_editor">
                <WhisperEditor
                  onRef={ref => {
                    whisperEditor = ref;
                  }}
                  showSendButton={false}
                  onSend={onSendWhisper}
                  allowFileTransfer={allowFileTransfer}
                />
              </div>
              {percentUpload == 0 ? null : (
                <div className={classes.contentUpload}>
                  <div style={{ position: 'relative', width: 24, height: 24 }}>
                    <CircularProgress
                      style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        color: '#eee'
                      }}
                      variant="determinate"
                      value={100}
                      size={24}
                    />
                    <CircularProgress
                      style={{ position: 'absolute', top: 0, left: 0 }}
                      variant="determinate"
                      value={percentUpload}
                      size={24}
                    />
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        <CSSButtonStyle
          style={{ marginRight: 10, width: 80 }}
          variant="contained"
          onClick={() => {
            onSendWhisper(
              whisperEditor.getSendText(),
              whisperEditor.getAttachments()
            );
          }}
        >
          {t('SEND')}
        </CSSButtonStyle>
      </DialogActions>
      <Snackbar
        autoHideDuration={2000}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={openSnackbar.show}
        onClose={() => setOpenSnackbar({ show: false, msg: '' })}
        message={openSnackbar.msg}
      />
    </Dialog>
  );
}

class PerfectScrollSelectedUser extends Component {
  constructor(props) {
    super(props);
    this.simpleBar = null;
    this.listRef = React.createRef();
    this.listScrollRef = React.createRef();
    this.scrollBottom = this.scrollBottom.bind(this);
  }

  componentDidMount() {
    this.simpleBar = new SimpleBarHanbiro(this.listScrollRef.current);
    this.scrollBottom();
  }

  scrollBottom() {
    this.simpleBar?.scrollToBottom();
  }

  render() {
    const {
      classes,
      selectedUsers,
      deleteUserSelected,
      onClickAdd
    } = this.props;
    return (
      <div
        className={classes.listSelectedUserContainer}
        ref={this.listScrollRef}
      >
        <div className={classes.listSelectedUser} ref={this.listRef}>
          {selectedUsers.map(user => (
            <div
              className={classes.userItem}
              key={user.userKey}
              onClick={() => deleteUserSelected(user)}
            >
              <UserPhotoView
                style={{ width: 30, height: 30 }}
                key={user.userKey}
                data={user}
                userKeyData={user.userKey}
                imgSize={50}
              />
              <Typography style={{ paddingLeft: 5 }} variant="inherit">
                {user.displayName}
              </Typography>
              <div
                onClick={() => deleteUserSelected(user)}
                className={classes.iconUserItem}
              >
                <Clear fontSize="small" />
              </div>
            </div>
          ))}
          <div key={'add'} onClick={onClickAdd}>
            <IconButton style={{ padding: 2 }}>
              <AddUserIcon
                style={{
                  width: 26,
                  height: 26
                }}
              />
            </IconButton>
          </div>
        </div>
      </div>
    );
  }
}

WhisperDialog.propTypes = {
  contacts: PropTypes.array,
  onClose: PropTypes.func,
  onSelectUser: PropTypes.func
};

WhisperDialog.defaultProps = {
  contacts: [],
  onClose: () => {},
  onSelectUser: (callbackSelected, selectedUser, maximum) => {}
};
