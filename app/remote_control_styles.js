import { withStyles } from '@material-ui/core/styles';
const styles = theme => ({
  main: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    height: '100%'
  }
});

export default withStyles(styles);
