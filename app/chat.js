import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from './actions';

import React, { Component } from 'react';
import { Menu } from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import { ipcRenderer, remote } from 'electron';
import * as constantsApp from './configs/constant';
import orgWorker from './core/worker/orgtree';
import roomListWorker from './core/worker/roomlist';
import downloadUploadManager from './core/service/filemanager/download-upload-manager';
import OrgDialog from './components/OrgDialog';
import ProfileDialog from './components/ProfileDialog';
import RoomUserChoiceDialog from './components/RoomUserChoiceDialog';
import HistoryDialog from './components/HistoryDialog';

import talkApi from './core/service/talk.api.render';

import ChatWindowContainer from './containers/ChatWindowContainer';

export const MenuOptionContext = React.createContext({
  onOpen: () => {},
  onClose: () => {},
  onOpenProfile: item => {},
  onOpenCreateGroupDialog: item => {},
  onOpenOrgDialog: () => {}
});

export const OrgContext = React.createContext({
  onOpen: () => {},
  onOpenProfile: item => {},
  onOpenRoomUserChoiceDialog: dataItem => {},
  onOpenSearchHistoryDialog: roomKey => {}
});

class ChatPage extends Component {
  constructor(props) {
    super(props);

    this.onSelectedUserOrg = null;

    this.delayShowDisconnect = null;

    this.state = {
      showOrgDialog: false,
      notAllowDisabledSelectedUser: false,
      changeOrgDialogTitleUpdate: false,

      historyDialogData: {
        show: false,
        item: null
      },
      profileDialogData: {
        show: false,
        item: null
      },
      roomUserDialogData: {
        show: false,
        data: null
      },
      openSnackbar: false,
      errorSnackbar: '',
      mouseY: 0,
      mouseX: 0,
      childrenOptionMenu: [],
      listSelectedUser: [],
      maximumOrg: 50,
      roomData: null
    };
  }

  handleClosed() {
    this.setState({
      openSnackbar: false
    });
  }

  showAlert(msg) {
    this.setState({
      errorSnackbar: msg,
      openSnackbar: true
    });
  }

  isCurrentWindowFocused() {
    return (
      remote.getCurrentWindow().isFocused() &&
      remote.getCurrentWindow().isVisible() &&
      !remote.getCurrentWindow().isMinimized()
    );
  }

  componentDidMount() {
    const { actions } = this.props;

    let paramQuery = global.location.search;
    paramQuery = paramQuery.split('?')[1];
    const roomKey = paramQuery.split('=')[1];
    const r = ipcRenderer.sendSync(
      constantsApp.COMMON_SYNC_ACTION_FROM_RENDER,
      constantsApp.ACTION_SYNC_GET_ROOM_BY_ROOM_KEY,
      { roomKey: roomKey }
    );
    this.setState({
      roomData: { room: r, primary_key: roomKey, users: null }
    });

    downloadUploadManager.initFunc();

    ipcRenderer.send(constantsApp.SEND_EVENT_LOAD_APP_SETTING, '');
    ipcRenderer.on(
      constantsApp.REPLY_EVENT_LOAD_APP_SETTING,
      (event, setting, alreadyLogin, languageMap) => {
        // update domain for API
        talkApi.transferSettingInfoFromElectronToBrowser();
        actions.requestInitAppSetting(setting, {}, alreadyLogin);
      }
    );

    // ORG
    ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ORG_TREE, '');
    ipcRenderer.on(constantsApp.REPLY_FULL_QUERY_ORG_TREE, (event, args) => {
      args.loggedUser = this.props.logged_user;
      orgWorker.getTreeOrg(args).then(result => {
        actions.saveOrgTree(
          result.main_tree,
          result.main_tree_favorite,
          result.group_cached,
          result.user_cached,
          result.listActualUserKeyFavourite
        );
        ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ROOM_LIST, '');
      });
    });

    ipcRenderer.on(
      constantsApp.REPLY_FULL_QUERY_ORG_TREE_FOR_STATE,
      (event, args) => {
        args.loggedUser = this.props.logged_user;
        orgWorker.getTreeOrg(args).then(result => {
          actions.saveOrgTree(
            result.main_tree,
            result.main_tree_favorite,
            result.group_cached,
            result.user_cached,
            result.listActualUserKeyFavourite
          );
        });
      }
    );

    //ROOM LIST
    ipcRenderer.on(constantsApp.REPLY_FULL_QUERY_ROOM_LIST, (event, args) => {
      let roomData = {
        listRoom: args,
        listUser: this.props.user_cached,
        loggedUser: this.props.logged_user,
        curRoomKey: '',
        window_visible: this.isCurrentWindowFocused()
      };
      roomListWorker.getRoomList(roomData).then(result => {
        actions.saveRoomList(result);
      });
    });

    ipcRenderer.on(constantsApp.MAIN_TO_RENDER_EVENT, (event, action, args) => {
      if (action == constantsApp.ACTION_SHOW_DISCONNECT_SOCKET) {
        this.showDisconnectSocket(args.force, args.show);
        return;
      }

      if (action == constantsApp.ACTION_EXTRA_LOGIN_CHANGED) {
        talkApi.updateAllAccountInfo();
        actions.requestUpdateExtraLoginInfo(args);
        return;
      }

      if (action == constantsApp.ACTION_ORG_CHANGED) {
        ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ORG_TREE, '');
        return;
      }

      if (action == constantsApp.ACTION_ORG_STATUS_CHANGED) {
        ipcRenderer.send(
          constantsApp.REQUEST_FULL_QUERY_ORG_TREE_FOR_STATE,
          ''
        );
        return;
      }

      if (action == constantsApp.ACTION_ORG_TIME_CARD_CHANGED) {
        actions.saveOrgTimeCard(args);
        return;
      }

      if (action == constantsApp.ACTION_ROOM_LIST_CHANGED) {
        if (args?.hasNewMessage) {
          if (Object.keys(args?.hasNewMessage).length > 0) {
            if (!this.isCurrentWindowFocused()) {
              this.setFlashBadgeIcon();
            }
          }
        }
        ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ROOM_LIST, '');
        return;
      }

      if (action == constantsApp.ACTION_GOT_FOCUSED_WINDOWS_AGAIN) {
        return;
      }

      if (action == constantsApp.ACTION_RELOAD_ROOM_AND_REMOVE) {
        const roomKey = args.roomKey;
        ipcRenderer.send(constantsApp.REQUEST_FULL_QUERY_ROOM_LIST, '');
        actions.checkAndClearRoomKey(roomKey);
      }

      if (action == constantsApp.ACTION_SHOW_MSG_INFO_TO_USER) {
        const msg = args;
        this.showAlert(msg);
        return;
      }

      if (action == constantsApp.ACTION_STOP_ALL_REQUEST) {
        downloadUploadManager.stopAllTask();

        return;
      }
    });
  }

  showDisconnectSocket(force, show) {
    const { actions } = this.props;
    clearTimeout(this.delayShowDisconnect);
    if (force) {
      actions.showDisconnectSocket(show);
    } else {
      this.delayShowDisconnect = setTimeout(() => {
        actions.showDisconnectSocket(show);
      }, 5000);
    }
  }

  setFlashBadgeIcon() {
    if (process.platform == 'win32') {
      remote.getCurrentWindow().flashFrame(true);
    }
  }

  componentWillUnmount() {
    clearTimeout(this.delayShowDisconnect);

    ipcRenderer.removeAllListeners(constantsApp.REPLY_EVENT_LOAD_APP_SETTING);
    ipcRenderer.removeAllListeners(constantsApp.REPLY_FULL_QUERY_ROOM_LIST);
    ipcRenderer.removeAllListeners(constantsApp.REPLY_FULL_QUERY_ORG_TREE);

    ipcRenderer.removeAllListeners(
      constantsApp.REPLY_FULL_QUERY_ORG_TREE_FOR_STATE
    );

    ipcRenderer.removeAllListeners(constantsApp.MAIN_TO_RENDER_EVENT);
    downloadUploadManager.destroy();
  }

  onOpenOrgDialog = (
    callbackSelected = () => {},
    listSelectedUser = [],
    maximumOrg = 50,
    notAllowDisabledSelectedUser,
    updateTitle = false
  ) => {
    this.setState({
      showOrgDialog: true,
      listSelectedUser,
      maximumOrg,
      notAllowDisabledSelectedUser: notAllowDisabledSelectedUser ?? false,
      changeOrgDialogTitleUpdate: updateTitle ?? false
    });
    this.onSelectedUserOrg = callbackSelected;
  };

  onCloseOrgDialog = user => {
    if (user) {
      this.onSelectedUserOrg(user);
    }
    this.setState({
      showOrgDialog: false,
      listSelectedUser: [],
      maximumOrg: 50,
      notAllowDisabledSelectedUser: false
    });
    this.onSelectedUserOrg = null;
  };

  onCloseRoomUserChoiceDialog = (selectedList, data) => {
    if (selectedList?.length > 0 && data) {
      ipcRenderer.send(
        constantsApp.REQUEST_COMMON_ACTION_FROM_RENDER,
        constantsApp.ACTION_QUERY_ROOM_OR_CREATE_ROOM_IF_NEED_AND_SHARE_MESSAGE,
        { targetData: selectedList, shareMsg: data }
      );
    }
    this.setState({
      roomUserDialogData: {
        show: false,
        data: null
      }
    });
  };

  onOpenMenu = (event, children) => {
    this.setState({
      childrenOptionMenu: children,
      mouseX: event.clientX - 2,
      mouseY: event.clientY - 4
    });
  };

  onCloseMenu = () => {
    this.setState({ mouseX: 0, mouseY: 0 });
  };

  onOpenProfile = item => {
    this.setState({
      profileDialogData: {
        show: true,
        item: item
      }
    });
  };

  onOpenRoomUserChoiceDialog = dataItem => {
    this.setState({
      roomUserDialogData: {
        show: true,
        data: dataItem
      }
    });
  };

  onOpenSearchHistoryDialog = roomKey => {
    this.setState({
      historyDialogData: { show: true, item: roomKey }
    });
  };

  render() {
    const { org, group_cached, user_cached, logged_user, rooms } = this.props;
    const {
      openSnackbar,
      errorSnackbar,
      showOrgDialog,
      notAllowDisabledSelectedUser,
      changeOrgDialogTitleUpdate,
      mouseX,
      mouseY,
      childrenOptionMenu,
      listSelectedUser,
      maximumOrg,
      profileDialogData,
      roomUserDialogData,
      historyDialogData,
      roomData
    } = this.state;

    return (
      <MenuOptionContext.Provider
        value={{
          onOpen: this.onOpenMenu,
          onClose: this.onCloseMenu,
          onOpenProfile: this.onOpenProfile,
          onOpenOrgDialog: this.onOpenOrgDialog
        }}
      >
        <OrgContext.Provider
          value={{
            onOpen: this.onOpenOrgDialog,
            onOpenProfile: this.onOpenProfile,
            onOpenRoomUserChoiceDialog: this.onOpenRoomUserChoiceDialog,
            onOpenSearchHistoryDialog: this.onOpenSearchHistoryDialog
          }}
        >
          <div
            style={{
              width: '100%',
              height: '100%'
            }}
          >
            {roomData ? (
              <div
                style={{
                  width: '100%',
                  height: '100%'
                }}
              >
                <ChatWindowContainer
                  style={{ width: '100%', height: '100%' }}
                  roomData={roomData}
                  roomKey={roomData.room.rRoomKey}
                />
              </div>
            ) : null}

            <Menu
              keepMounted
              open={mouseX && mouseY ? true : false}
              onClose={this.onCloseMenu}
              anchorReference="anchorPosition"
              anchorPosition={{ top: mouseY ?? 0, left: mouseX ?? 0 }}
            >
              {childrenOptionMenu}
            </Menu>

            {historyDialogData.show && (
              <HistoryDialog
                show={true}
                roomKey={historyDialogData.item}
                onClose={() =>
                  this.setState({
                    historyDialogData: { show: false, item: null }
                  })
                }
              />
            )}

            {profileDialogData.show && (
              <ProfileDialog
                show={true}
                item={profileDialogData.item}
                onClose={() =>
                  this.setState({
                    profileDialogData: {
                      show: false,
                      item: null
                    }
                  })
                }
              />
            )}

            {roomUserDialogData.show && (
              <RoomUserChoiceDialog
                rooms={rooms}
                org={org}
                groupCached={group_cached}
                user_cached={user_cached}
                user={logged_user?.account_info}
                loggedUser={logged_user}
                dataObj={roomUserDialogData.data}
                open={true}
                maximum={1}
                onSelect={this.onCloseRoomUserChoiceDialog}
                onClose={this.onCloseRoomUserChoiceDialog}
              />
            )}
            {showOrgDialog && (
              <OrgDialog
                org={org}
                groupCached={group_cached}
                loggedUser={logged_user}
                listSelectedUser={listSelectedUser}
                open={true}
                changeOrgDialogTitleUpdate={changeOrgDialogTitleUpdate}
                notAllowDisableSelected={notAllowDisabledSelectedUser}
                maximum={maximumOrg}
                onSelect={this.onCloseOrgDialog}
                onClose={this.onCloseOrgDialog}
              />
            )}
            <Snackbar
              autoHideDuration={2000}
              anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
              open={openSnackbar}
              onClose={() => this.handleClosed()}
              message={errorSnackbar}
            />
          </div>
        </OrgContext.Provider>
      </MenuOptionContext.Provider>
    );
  }
}

function mapStateToProps(state) {
  return {
    logged_user: state.auth.user,
    group_cached: state.company.group_cached,
    org: state.company.org,
    rooms: state.room_list.rooms,
    room_cached: state.room_list.room_cached,
    user_cached: state.company.user_cached,
    room_info: state.message_list.roomInfo
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ChatPage);
