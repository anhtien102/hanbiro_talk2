import { withStyles } from '@material-ui/core/styles';
const styles = theme => ({
  main: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    height: '100%'
  },
  videoLeft: {
    width: 'calc(100% - 450px)',
    height: '100%',
    borderRight: 1,
    borderRightStyle: 'solid',
    borderRightColor: theme.palette.divider
  },
  videoLeftFullScreen: {
    overflow: 'hidden',
    width: '100%',
    height: '100%',
    borderRight: 1,
    borderRightStyle: 'solid',
    borderRightColor: theme.palette.divider,
    zIndex: 2
  },
  videoRightFullScreen: {
    position: 'absolute',
    width: 0,
    opacity: 0,
    height: '100%',
    zIndex: 1
  },
  videoRight: {
    width: 450,
    height: '100%'
  },
  chatContainer: {
    width: '100%',
    height: '100%'
  },
  chatContainerParent: {
    width: '100%',
    height: '100%'
  }
});

export default withStyles(styles);
